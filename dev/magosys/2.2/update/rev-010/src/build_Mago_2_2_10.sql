SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.2.10 SRC
PROMPT file :  build_Mago_2_2_10.sql
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/SAR_ADMIN_PARAMETRI_GENERALI.sql


PROMPT _______________________________________________________________________________________
PROMPT Appl defaults - Crel
PROMPT
conn crel/crel&tns_arcdb1
@./InitTables/CREL_APPL_DEFAULTS.sql

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/GRUPPI_MISURA.sql
@./InitTables/METEO_JOB_STATIC_CONFIG.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/REGISTRO_RES_RITARDO.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_TRANSFER_RES.sql
@./Packages/PKG_MISURE.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MAGO_DGF.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_TRANSFER_RES.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MISURE.sql

PROMPT _______________________________________________________________________________________
PROMPT Procedures
PROMPT
@./Procedures/SP_STORICO_REGISTRO_INVIO.sql

PROMPT _______________________________________________________________________________________
PROMPT Schedulerjobs
PROMPT
@./Schedulerjobs/STORICO_REGISTRO_INVIO.sql



COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.2.10 SRC
PROMPT

spool off
