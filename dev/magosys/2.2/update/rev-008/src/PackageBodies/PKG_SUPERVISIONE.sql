PROMPT PACKAGE BODY: PKG_SUPERVISIONE

create or replace PACKAGE BODY PKG_SUPERVISIONE AS

/* ***********************************************************************************************************
   NAME:       PKG_SUPERVISIONE
   PURPOSE:    Modulo Supervisione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      17/11/2016  Favatella F.     Created this package.        -- Spec. Version 2.2.1
   2.2.1      11/01/2017  Favatella F.       create AddSpvCollectorMeasures e AddSpvCollectorInfos, gestione fileBlob,
                                             aggiornata procedura GetSpvDettaglioParametri  -- Spec. Version 2.2.1
   2.2.1      27/03/2017  Favatella F.      created AddSpvCollectorInfoWithPod
                                            F_getSpvDettaglioParametri
                                            F_getSpvSoglie                -- Spec. Version 2.2.1
   2.2.2    12/05/2017    Frosini     eliminate dbms_output
   2.2.3    12/05/2017    Favatella F.     MAGO-1166 modificata procedura DecrementSpvDettaglioParametri per evitare contatori negativi
   2.2.6    27/07/2017    Favatella F.     MAGO-1126 modificata procedura GetSpvSistema per ultima riga selezionabile
   2.2.7    03/08/2017    Forno S.         MAGO-1216  SPV -  notifiche mail         -- Spec. Version 2.2.7
   2.2.8    25/09/2017    Favatella F.     MAGO-1239 modificata gestione aggregazioni nella procedura GetSpvDettaglioParametri
*********************************************************************************************************** */

 PROCEDURE GetSpvSistema      (pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              ) AS
 BEGIN

    OPEN pRefCurs FOR
    with a as(
      select ss.id_sistema ID_SISTEMA
      , sa.id_livello id_livello
      , count(*) count_livello
      from spv_sistema ss
      join spv_rel_sistema_dizionario srsd on srsd.id_sistema = ss.id_sistema
      join spv_allarmi sa on sa.id_rel_sistema_dizionario = srsd.id_rel_sistema_dizionario
      group by ss.id_sistema, sa.id_livello
    ) , b as (
      select ss.*, soglie.valore, sla.id_livello
      from spv_sistema ss
      left join spv_soglie soglie  on soglie.id_sistema = ss.id_sistema
                                  and soglie.id_chiave  like '%.supervision_enabled',
      spv_livello_allarmi sla
    )
    select  b.id_sistema
          ,nvl(SUM(DECODE(b.id_livello, 2, a.count_livello)),0) YELLOW_ALARMS_NUMBER
          ,nvl(SUM(DECODE(b.id_livello, 3, a.count_livello)),0) RED_ALARMS_NUMBER
          ,b.DESCRIZIONE DESCRIZIONE
          , (
            case
              when nvl(SUM(DECODE(b.id_livello, 3, a.count_livello)),0) >0 then 3
              when nvl(SUM(DECODE(b.id_livello, 2, a.count_livello)),0) >0 then 2
              else 1
            end
          )ALARM_STATE
          , sysdate LAST_UPDATE_DATE
          , '' SUPERVISION_CATEGORY_ID
          , '' SISTEMI_COINVOLTI
          , '' DIRECTION
          , 1 FREQUENCY
          ,  case b.valore
              when 'true' then 1
              else 0
            end ENABLED
          , b.visible VISIBLE
    from b
    left join a on b.id_sistema = a.id_sistema and b.id_livello = a.id_livello
    group by  b.id_sistema, b.DESCRIZIONE, b.valore, b.visible
    order by b.id_sistema;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvSistema'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetSpvSistema;




 PROCEDURE GetSpvDizionarioAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                        , pID_SISTEMA number
                              )
AS
BEGIN
 OPEN pRefCurs FOR
    select  sda.id_dizionario_allarme,
            sda.descrizione
    from spv_dizionario_allarmi sda
    join spv_rel_sistema_dizionario srsd on srsd.id_dizionario_allarme = sda.id_dizionario_allarme
    join spv_sistema ss on ss.id_sistema = srsd.id_sistema
    where ss.id_sistema = pID_SISTEMA
    order by sda.id_dizionario_allarme
    ;


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvDizionarioAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvDizionarioAllarmi;

 PROCEDURE GetSpvAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              , pID_SISTEMA number
--                              , pID_DIZIONARIO_ALLARME varchar2
--                              , pID_LIVELLO_ALLARME varchar2
                              )AS
BEGIN
 OPEN pRefCurs FOR
  select
    sa.ID_ALLARME ID_ALLARME
			,srsd.id_dizionario_allarme ID_DIZIONARIO_ALLARME
			,sa.ID_LIVELLO ID_LIVELLO_ALLARME
			,sa.DATA_ALLARME DATA_ALLARME
			,sa.DESCRIZIONE_TIPO DESCRIZIONE_TIPO
			,sa.DESCRIZIONE_ELEMENTO DESCRIZIONE_ELEMENTO
			,sa.DESCRIZIONE_ALTRO1 DESCRIZIONE_ALTRO1
			,sa.DESCRIZIONE_ALTRO2 DESCRIZIONE_ALTRO2
			,sa.DESCRIZIONE_ALTRO3 DESCRIZIONE_ALTRO3
			,sa.INFO INFO
      ,sa.PARAMETRI_DESCRIZIONE
	from spv_allarmi sa
  join spv_rel_sistema_dizionario srsd on srsd.id_dizionario_allarme = sa.id_rel_sistema_dizionario
  where srsd.id_sistema = pID_SISTEMA
--  and (pID_DIZIONARIO_ALLARME is null or (pID_DIZIONARIO_ALLARME is not null and srsd.id_dizionario_allarme in (
--    select regexp_substr(pID_DIZIONARIO_ALLARME,'[^,]+', 1, level) from dual
--    connect by regexp_substr(pID_DIZIONARIO_ALLARME, '[^,]+', 1, level) is not null
--  )))
--  and (pID_LIVELLO_ALLARME is null or (pID_LIVELLO_ALLARME is not null and sa.ID_LIVELLO in (
--    select regexp_substr(pID_LIVELLO_ALLARME,'[^,]+', 1, level) from dual
--    connect by regexp_substr(pID_LIVELLO_ALLARME, '[^,]+', 1, level) is not null
--  )))

  order by sa.ID_LIVELLO desc, sa.data_allarme
  ;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvAllarmi;

PROCEDURE GetSpvDettaglioParametri ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                    , pID_SISTEMA number
                                    )
AS
  p_dal varchar2(20);
  p_al varchar2(20);
  p_inizio_elaborazione varchar2(20);
  p_fine_elaborazione varchar2(20);
  p_tempo_richiesto varchar2(20);
  p_stato_elaborazione varchar2(20);
  p_numAggregazioni varchar2(20);
BEGIN


   if(pID_SISTEMA = 2)then
    select
    --pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(dal) END ) dal
    --, pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(al) END ) al
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    --, CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0 THEN 'CONCLUSA'
    --ELSE 'IN CORSO'  END  stato_elaborazione
    , CASE 
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN null
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN 'IN CORSO'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN 'CONCLUSA'
      END  stato_elaborazione
    into p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    from (
    select
    ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    from session_statistics ss
    where ss.cod_tipo_misura in ('PAS')
    and ss.service_ref = (select max(service_ref) from session_statistics where cod_tipo_misura in ('PAS'))
    order by ss.cod_sessione
    );

    select count(*)
    into p_numAggregazioni
    from scheduled_jobs
    where cod_tipo_misura in ('PAS')
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, null );
  end if;
  if(pID_SISTEMA = 7)then
    select
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(dal) END ) dal
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(al) END ) al
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    ,  CASE 
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN null
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN 'IN CORSO'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN 'CONCLUSA'
      END  stato_elaborazione
    into p_dal, p_al, p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    from (
    select
    ss.date_from dal
    ,ss.date_to al

    ,ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    from session_statistics ss
    where ss.cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1')
    and ss.service_ref = (select max(service_ref) from session_statistics where cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1'))
    order by ss.cod_sessione
    )
    ;

    select count(*)
    into p_numAggregazioni
    from scheduled_jobs
    where cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1')
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoDal' 			, p_dal , null);
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoAl' 			, p_al , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, null );
  end if;

 OPEN pRefCurs FOR
   select sdp.id_sistema
        , sdp.id_chiave
        , sdp.valore
        , sdp.valore_blob
   from spv_dettaglio_parametri sdp
   where sdp.id_sistema = pID_SISTEMA;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvDettaglioParametri;



PROCEDURE AddSpvAllarmi(pID_SISTEMA number
                      , pID_DIZIONARIO_ALLARME number
											, pDATA_ALLARME date
											, pDESCRIZIONE_TIPO VARCHAR2
											, pDESCRIZIONE_ELEMENTO VARCHAR2
											, pDESCRIZIONE_ALTRO1 VARCHAR2
											, pDESCRIZIONE_ALTRO2 VARCHAR2
											, pDESCRIZIONE_ALTRO3 VARCHAR2
											, pID_LIVELLO NUMBER
											, pINFO VARCHAR2
                      , pPARAMETRI_DESCRIZIONE VARCHAR2)
AS
  pID_REL_SISTEMA_DIZIONARIO number;
BEGIN
  null; --dbms_output.put_line('AddSpvAllarmi');

  select srsd.id_rel_sistema_dizionario into pID_REL_SISTEMA_DIZIONARIO
  from spv_rel_sistema_dizionario srsd
  where srsd.id_sistema = pID_SISTEMA
  and srsd.id_dizionario_allarme = pID_DIZIONARIO_ALLARME
  ;

  Insert into SPV_ALLARMI (ID_ALLARME,ID_REL_SISTEMA_DIZIONARIO,DATA_ALLARME,DESCRIZIONE_TIPO,DESCRIZIONE_ELEMENTO,DESCRIZIONE_ALTRO1,DESCRIZIONE_ALTRO2,DESCRIZIONE_ALTRO3,ID_LIVELLO,INFO,PARAMETRI_DESCRIZIONE)
  values (SPV_ALLARMI_SEQ.nextval,pID_REL_SISTEMA_DIZIONARIO,pDATA_ALLARME,pDESCRIZIONE_TIPO,pDESCRIZIONE_ELEMENTO,pDESCRIZIONE_ALTRO1,pDESCRIZIONE_ALTRO2,pDESCRIZIONE_ALTRO3,pID_LIVELLO,pINFO,pPARAMETRI_DESCRIZIONE);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvAllarmi;

PROCEDURE CleanSpvAllarmi(pID_SISTEMA number)
AS
BEGIN
null; --dbms_output.put_line('CleanSpvAllarmi');
DELETE FROM SPV_ALLARMI
  WHERE id_rel_sistema_dizionario in (
    select srsd.id_rel_sistema_dizionario
    from spv_rel_sistema_dizionario srsd
    where srsd.id_sistema = pID_SISTEMA
  );

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.CleanSpvAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END CleanSpvAllarmi;

PROCEDURE SetSpvDettaglioParametri( pID_SISTEMA number
                                  , pID_CHIAVE VARCHAR2
                                  , pValore VARCHAR2
                                  , pVALORE_BLOB BLOB
                                  )
AS
BEGIN
null; --dbms_output.put_line('SetSpvDettaglioParametri');

  UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE = pVALORE
      , VALORE_BLOB = pVALORE_BLOB
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE
      ;

  IF ( sql%rowcount = 0 )
    THEN
        Insert into SPV_DETTAGLIO_PARAMETRI (ID_SISTEMA,ID_CHIAVE,VALORE)
        values (pID_SISTEMA,pID_CHIAVE,pVALORE);

  END IF;


EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.SetSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END SetSpvDettaglioParametri;

PROCEDURE DecrementSpvDettaglioParametri( pID_SISTEMA number
                                        , pID_CHIAVE VARCHAR2
                                        , pID_CHIAVE_ELENCO VARCHAR2
                                        , pCOD_GESTIONALE VARCHAR2
                                        )
AS
  blob_contains number;
BEGIN
null; --dbms_output.put_line('DecrementSpvDettaglioParametri');

  BEGIN
   -- verifico se il codGest � presente
     select
      CASE WHEN valore_blob IS NULL
      THEN 0
      ELSE dbms_lob.instr(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE)
      END
     into blob_contains
    from SPV_DETTAGLIO_PARAMETRI
    WHERE ID_SISTEMA = pID_SISTEMA
    AND ID_CHIAVE = pID_CHIAVE_ELENCO
    ;
    -- se il codGest � presente
    if blob_contains > 0 then

      -- decremento il contatore
      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE = VALORE-1
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE
      ;

      -- pulisco la lista
      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE||','))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
      ;

      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),','||pCOD_GESTIONALE))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
      ;

      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
      ;
    end if;



  EXCEPTION
        WHEN OTHERS
        THEN null; null; --dbms_output.put_line(SQLCODE);
  END;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.DecrementSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END DecrementSpvDettaglioParametri;

PROCEDURE GetSpvCollectorInfo      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                   , pID_SISTEMA        number)
AS
BEGIN
  null; --dbms_output.put_line('GetSpvCollectorInfo');
  OPEN pRefCurs FOR
    select sci.ID_INFO
    ,sci.COLLECTOR_NAME
    ,sci.DATA_INSERIMENTO
    ,srsd.ID_SISTEMA
    ,srsd.ID_DIZIONARIO_ALLARME
    ,sci.SUPPLIER_NAME
    ,sci.COD_GEST_ELEMENTO
    ,sci.COD_TIPO_MISURA
    ,sci.COD_TIPO_FONTE
    ,sci.VALORE
    ,sci.INFORMAZIONI
    from spv_collector_info sci
    join spv_rel_sistema_dizionario srsd on srsd.id_rel_sistema_dizionario = sci.id_rel_sistema_dizionario
    where srsd.id_sistema = pID_SISTEMA
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvCollectorInfo;

PROCEDURE CleanSpvCollectorInfo(pID_SISTEMA number)
AS
BEGIN
  null; --dbms_output.put_line('CleanSpvCollectorInfo');
  DELETE FROM SPV_COLLECTOR_INFO
  WHERE id_rel_sistema_dizionario in (
    select srsd.id_rel_sistema_dizionario
    from spv_rel_sistema_dizionario srsd
    where srsd.id_sistema = pID_SISTEMA
  );

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.CleanSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END CleanSpvCollectorInfo;


PROCEDURE AddSpvCollectorInfo(	pCOLLECTOR_NAME				        VARCHAR2
                              , pDATA_INSERIMENTO				      DATE
                              , pID_SISTEMA                   number
                              , pID_DIZIONARIO_ALLARME        number
                              , pSUPPLIER_NAME					      VARCHAR2
                              , pCOD_GEST_ELEMENTO			      VARCHAR2
                              , pCOD_TIPO_MISURA				      VARCHAR2
                              , pCOD_TIPO_FONTE				        VARCHAR2
                              , pVALORE						            NUMBER
                              , pINFORMAZIONI					        VARCHAR2
                              )
AS
  pID_REL_SISTEMA_DIZIONARIO number;
BEGIN
--  null; --dbms_output.put_line('AddSpvCollectorInfo');

  select srsd.id_rel_sistema_dizionario into pID_REL_SISTEMA_DIZIONARIO from spv_rel_sistema_dizionario srsd
  --join spv_sistema ss on ss.id_sistema = srsd.id_sistema
  --join spv_dizionario_allarmi sda on sda.id_dizionario_allarme = srsd.id_dizionario_allarme
  where srsd.id_sistema = pID_SISTEMA
  and srsd.id_dizionario_allarme = pID_DIZIONARIO_ALLARME
  ;

  Insert into SPV_COLLECTOR_INFO (ID_INFO,COLLECTOR_NAME,DATA_INSERIMENTO,ID_REL_SISTEMA_DIZIONARIO,SUPPLIER_NAME,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE,INFORMAZIONI)
  values (SPV_COLLECTOR_INFO_SEQ.nextval,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pID_REL_SISTEMA_DIZIONARIO,pSUPPLIER_NAME,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE,pINFORMAZIONI);


EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorInfo;

PROCEDURE AddSpvCollectorInfoWithPod(	pCOLLECTOR_NAME				        VARCHAR2
                              , pDATA_INSERIMENTO				      DATE
                              , pID_SISTEMA                   number
                              , pID_DIZIONARIO_ALLARME        number
                              , pSUPPLIER_NAME					      VARCHAR2
                              , pCOD_POD			                VARCHAR2
                              , pCOD_TIPO_MISURA				      VARCHAR2
                              , pCOD_TIPO_FONTE				        VARCHAR2
                              , pVALORE						            NUMBER
                              , pINFORMAZIONI					        VARCHAR2
                              )
AS
  pID_REL_SISTEMA_DIZIONARIO number;
  pGest varchar2(100);
  pResult number;
BEGIN
  null; --dbms_output.put_line('AddSpvCollectorInfoWithPod');

  pResult := pkg_elementi.f_GetGestFromPod(pCOD_POD,pGest);

  AddSpvCollectorInfo(	pCOLLECTOR_NAME, pDATA_INSERIMENTO, pID_SISTEMA, pID_DIZIONARIO_ALLARME, pSUPPLIER_NAME, pGest, pCOD_TIPO_MISURA, pCOD_TIPO_FONTE, pVALORE, pINFORMAZIONI);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfoWithPod'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorInfoWithPod;

PROCEDURE AddSpvCollectorInfos       (p_Array    IN T_SPVCOLLECTORINFO_ARRAY)
AS
  pID_REL_SISTEMA_DIZIONARIO number;
BEGIN
	null; --dbms_output.put_line('AddSpvCollectorInfos');
    IF p_Array.FIRST IS NULL THEN
      null; --dbms_output.put_line('AddSpvCollectorInfos: SpvCollectorInfos non trovate');
      RETURN;
    END IF;


    FOR i IN p_Array.FIRST .. p_Array.LAST LOOP
      select srsd.id_rel_sistema_dizionario into pID_REL_SISTEMA_DIZIONARIO from spv_rel_sistema_dizionario srsd
      --join spv_sistema ss on ss.id_sistema = srsd.id_sistema
      --join spv_dizionario_allarmi sda on sda.id_dizionario_allarme = srsd.id_dizionario_allarme
      where srsd.id_sistema = p_Array(i).ID_SISTEMA
      and srsd.id_dizionario_allarme = p_Array(i).ID_DIZIONARIO_ALLARME
      ;

      Insert into SPV_COLLECTOR_INFO (ID_INFO,COLLECTOR_NAME,DATA_INSERIMENTO,ID_REL_SISTEMA_DIZIONARIO,SUPPLIER_NAME,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE,INFORMAZIONI)
      values (SPV_COLLECTOR_INFO_SEQ.nextval,p_Array(i).COLLECTOR_NAME,p_Array(i).DATA_INSERIMENTO,pID_REL_SISTEMA_DIZIONARIO,p_Array(i).SUPPLIER_NAME,p_Array(i).COD_GEST_ELEMENTO,p_Array(i).COD_TIPO_MISURA,p_Array(i).COD_TIPO_FONTE,p_Array(i).VALORE,p_Array(i).INFORMAZIONI);

    END LOOP;
	null; --dbms_output.put_line('AddSpvCollectorInfos: SpvCollectorInfos inserite');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfos'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddSpvCollectorInfos;

PROCEDURE GetSpvSoglie      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                            , pID_SISTEMA        number)
AS
BEGIN
  null; --dbms_output.put_line('GetSpvSoglie');
  OPEN pRefCurs FOR
    select s.ID_SOGLIA
    ,s.ID_SISTEMA
    ,s.ID_CHIAVE
    ,s.VALORE
    from spv_soglie s
    where s.id_sistema = pID_SISTEMA
    order by s.id_chiave
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvSoglie'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvSoglie;

PROCEDURE AddSpvSoglie       (pMisureMeteo    IN T_SPVSOGLIA_ARRAY)
AS

BEGIN
	null; --dbms_output.put_line('AddSpvSoglie');
    IF pMisureMeteo.FIRST IS NULL THEN
      null; --dbms_output.put_line('AddSpvSoglie: soglie non trovate');
      RETURN;
    END IF;


    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP
	  UPDATE SPV_SOGLIE
		  SET VALORE = pMisureMeteo(i).VALORE
		  WHERE ID_SOGLIA = pMisureMeteo(i).ID_SOGLIA
		  AND ID_SISTEMA = pMisureMeteo(i).ID_SISTEMA
		  AND ID_CHIAVE = pMisureMeteo(i).ID_CHIAVE
		  ;
    END LOOP;
	null; --dbms_output.put_line('AddSpvSoglie: soglie inserite');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvSoglie'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddSpvSoglie;

PROCEDURE GetSpvCollectorMeasure      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                      , pCOLLECTOR_NAME		VARCHAR2
                                      , pSTART_DATE	DATE)
AS
BEGIN
  null; --dbms_output.put_line('GetSpvCollectorMeasure');
  OPEN pRefCurs FOR
    select m.ID_MEASURE
    ,m.COLLECTOR_NAME
    ,m.DATA_INSERIMENTO
    ,m.DATA_MISURA
    ,m.COD_GEST_ELEMENTO
    ,m.COD_TIPO_MISURA
    ,m.COD_TIPO_FONTE
    ,m.VALORE
    from spv_collector_measure m
    where m.collector_name = pCOLLECTOR_NAME
    and m.data_misura >= pSTART_DATE
    order by m.data_misura
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvCollectorMeasure'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvCollectorMeasure;

PROCEDURE AddSpvCollectorMeasure(	pCOLLECTOR_NAME				        VARCHAR2
                                , pDATA_INSERIMENTO				      DATE
                                , pDATA_MISURA    				      DATE
                                , pCOD_GEST_ELEMENTO			      VARCHAR2
                                , pCOD_TIPO_MISURA				      VARCHAR2
                                , pCOD_TIPO_FONTE				        VARCHAR2
                                , pVALORE						            NUMBER
                                )
AS
BEGIN
  null; --dbms_output.put_line('AddSpvCollectorMeasure');



--  UPDATE SPV_COLLECTOR_MEASURE
--      SET DATA_INSERIMENTO = pDATA_INSERIMENTO,
--          VALORE = pVALORE
--      WHERE COLLECTOR_NAME = pCOLLECTOR_NAME
--      AND DATA_MISURA = pDATA_MISURA
--      AND COD_GEST_ELEMENTO = pCOD_GEST_ELEMENTO
--      AND COD_TIPO_MISURA = pCOD_TIPO_MISURA
--      AND COD_TIPO_FONTE = pCOD_TIPO_FONTE
--      ;
--
--  IF ( sql%rowcount = 0 )
--    THEN
--        Insert into SPV_COLLECTOR_MEASURE (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
--        values (SPV_COLLECTOR_MEASURE_SEQ.nextval,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pDATA_MISURA,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE);
--
--  END IF;

  MERGE into SPV_COLLECTOR_MEASURE T
  USING (select pDATA_INSERIMENTO DATA_INSERIMENTO ,pVALORE VALORE,  pDATA_MISURA DATA_MISURA ,
                       pCOLLECTOR_NAME  COLLECTOR_NAME,pCOD_GEST_ELEMENTO  COD_GEST_ELEMENTO ,
                       pCOD_TIPO_MISURA COD_TIPO_MISURA, pCOD_TIPO_FONTE COD_TIPO_FONTE
                       from dual
          )  ORIG
  ON (           T.COLLECTOR_NAME = ORIG.COLLECTOR_NAME
                    AND T.DATA_MISURA = ORIG.DATA_MISURA
                    AND T.COD_GEST_ELEMENTO = ORIG.COD_GEST_ELEMENTO
                    AND T.COD_TIPO_MISURA = ORIG.COD_TIPO_MISURA
                    AND T.COD_TIPO_FONTE = ORIG.COD_TIPO_FONTE  )
  WHEN MATCHED THEN
                    UPDATE SET DATA_INSERIMENTO = pDATA_INSERIMENTO,  VALORE = pVALORE
  WHEN NOT MATCHED THEN
                    Insert  (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
                    values (SPV_COLLECTOR_MEASURE_SEQ.nextval,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pDATA_MISURA,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE);



EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorMeasure'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorMeasure;

PROCEDURE AddSpvCollectorMeasures(	p_Array    IN T_SPVCOLLECTORMEASURE_ARRAY)
AS
  ptmp varchar2(10);
BEGIN
	null; --dbms_output.put_line('AddSpvCollectorMeasures');
    IF p_Array.FIRST IS NULL THEN
      null; --dbms_output.put_line('AddSpvCollectorMeasures: SpvCollectorMeasures non trovate');
      RETURN;
    END IF;


    FOR i IN p_Array.FIRST .. p_Array.LAST LOOP

      UPDATE SPV_COLLECTOR_MEASURE
      SET DATA_INSERIMENTO = p_Array(i).DATA_INSERIMENTO,
          VALORE = p_Array(i).VALORE
      WHERE COLLECTOR_NAME = p_Array(i).COLLECTOR_NAME
      AND DATA_MISURA = p_Array(i).DATA_MISURA
      AND COD_GEST_ELEMENTO = p_Array(i).COD_GEST_ELEMENTO
      AND COD_TIPO_MISURA = p_Array(i).COD_TIPO_MISURA
      AND COD_TIPO_FONTE = p_Array(i).COD_TIPO_FONTE
      ;

      IF ( sql%rowcount = 0 )
        THEN
            Insert into SPV_COLLECTOR_MEASURE (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
            values (SPV_COLLECTOR_MEASURE_SEQ.nextval
            ,p_Array(i).COLLECTOR_NAME
            ,p_Array(i).DATA_INSERIMENTO
            ,p_Array(i).DATA_MISURA
            ,p_Array(i).COD_GEST_ELEMENTO
            ,p_Array(i).COD_TIPO_MISURA
            ,p_Array(i).COD_TIPO_FONTE
            ,p_Array(i).VALORE);

      END IF;

--select '1' into ptmp from dual;
    END LOOP;
	null; --dbms_output.put_line('AddSpvCollectorMeasures: SpvCollectorMeasures inserite');
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorMeasures'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorMeasures;

PROCEDURE getMeteoJobStaticConfig      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                    , pKEY               VARCHAR2)
AS
BEGIN
  null; --dbms_output.put_line('getMeteoJobStaticConfig');
  OPEN pRefCurs FOR
    select *
    from meteo_job_static_config
    where key = pKEY--'MFM.enabled.suppliers'
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.getMeteoJobStaticConfig'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END getMeteoJobStaticConfig;

FUNCTION getTimestamp(pDate IN DATE)
   RETURN NUMBER
   IS p_unix_time NUMBER;
   BEGIN
select trunc(extract(day    from (pDate - timestamp '1970-01-01 00:00:00')) * 86400000
     + extract(hour   from (pDate - timestamp '1970-01-01 00:00:00')) * 3600000
     + extract(minute from (pDate - timestamp '1970-01-01 00:00:00')) * 60000
     + extract(second from (pDate - timestamp '1970-01-01 00:00:00')) * 1000) unix_time

     into p_unix_time
from dual;

      RETURN(p_unix_time);
    END;

FUNCTION convert_to_clob(l_blob BLOB) RETURN CLOB IS
     l_clob         CLOB;
     l_dest_offset  NUMBER := 1;
     l_src_offset   NUMBER := 1;
     l_lang_context NUMBER := dbms_lob.default_lang_ctx;
     l_warning      NUMBER;
  BEGIN
     dbms_lob.createtemporary(l_clob, TRUE);
     dbms_lob.converttoclob(dest_lob     => l_clob,
                            src_blob     => l_blob,
                            amount       => dbms_lob.lobmaxsize,
                            dest_offset  => l_dest_offset,
                            src_offset   => l_src_offset,
                            blob_csid    => nls_charset_id('AL32UTF8'),
                            lang_context => l_lang_context,
                            warning      => l_warning);
     RETURN l_clob;
  END convert_to_clob;

FUNCTION convert_to_blob(l_clob CLOB) RETURN BLOB IS
   l_blob         BLOB;
   l_dest_offset  NUMBER := 1;
   l_src_offset   NUMBER := 1;
   l_lang_context NUMBER := dbms_lob.default_lang_ctx;
   l_warning      NUMBER;
BEGIN
   dbms_lob.createtemporary(l_blob, TRUE);
   dbms_lob.converttoblob(dest_lob     => l_blob,
                          src_clob     => l_clob,
                          amount       => dbms_lob.lobmaxsize,
                          dest_offset  => l_dest_offset,
                          src_offset   => l_src_offset,
                          blob_csid    => nls_charset_id('AL32UTF8'),
                          lang_context => l_lang_context,
                          warning      => l_warning);
   RETURN l_blob;
END convert_to_blob;

	-- Restituisce il campo valore della tabella SPV_DETTAGLIO_PARAMETRI
	function F_getSpvDettaglioParametri ( PID_SISTEMA IN spv_dettaglio_parametri.ID_SISTEMA%type
									   ,pID_CHIAVE IN spv_dettaglio_parametri.ID_CHIAVE%type
									   )
	return spv_dettaglio_parametri.VALORE%type
	is
	P_VALORE spv_dettaglio_parametri.VALORE%type;
	begin

	  P_VALORE := 'XXX';
	  begin
		 select sdp.valore into P_VALORE
		 --sdp.id_sistema, sdp.id_chiave, sdp.valore , sdp.valore_blob
	   from spv_dettaglio_parametri sdp
	   where sdp.id_sistema = PID_SISTEMA
	   and sdp.id_chiave = pID_CHIAVE;

		EXCEPTION
		WHEN OTHERS THEN
			null;null; --dbms_output.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	  end;
	return P_VALORE;
	end F_getSpvDettaglioParametri;

	-- Restituisce il campo valore della tabella SPV_SOGLIE
	function F_getSpvSoglie ( PID_SISTEMA IN spv_soglie.ID_SISTEMA%type
							 ,PID_CHIAVE IN spv_soglie.ID_CHIAVE%type
							)
	return spv_soglie.VALORE%type
	is
	P_VALORE spv_soglie.VALORE%type;
	begin
	P_VALORE := 'XXX';
	begin
	  select s.VALORE into P_VALORE
	  --s.ID_SOGLIA,s.ID_SISTEMA,s.ID_CHIAVE,s.VALORE
	from spv_soglie s
	where s.id_sistema = PID_SISTEMA
	and s.id_chiave = PID_CHIAVE
	;
	exception
	WHEN OTHERS THEN

		 null;null; --dbms_output.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	end;
	return P_VALORE;

	end F_getSpvSoglie;

-- NOTIFICHE MAIL 2.2.7
    PROCEDURE  AddSpvconfignotifica(pFormato IN CHAR ,pDestinatari IN VARCHAR2  ,pNome IN VARCHAR2  ,pOggetto IN VARCHAR2   , pTesto IN VARCHAR2 , pNotifiche IN T_SISLIV_ARRAY)
    AS
    IdNextConfigNotif integer;
    BEGIN

     select SPV_CONFIG_NOTIF_SEQ.nextval into IdNextConfigNotif from dual;

    -- SPV_CONFIG_NOTIFICA
    Insert into SPV_CONFIG_NOTIFICA  (ID_SPVCONFIGNOTIFICA, DATA_CREAZIONE, NOME, TIPO_ALLEGATO, OGGETTO_MAIL, TESTO_MAIL, MAIL_LIST)
    values (IdNextConfigNotif, sysdate, pNome, pFormato, pOggetto, pTesto, pDestinatari);


    -- SPV_CONFIG_NOTIFICA_ALARMLIST
    IF pNotifiche.FIRST IS NULL THEN
          RETURN;
    END IF;

    FOR i IN pNotifiche.FIRST .. pNotifiche.LAST LOOP
        Insert into SPV_CONFIG_NOTIFICA_ALARMLIST (ID_SPVCONFIGNOTIFICA,  ID_SISTEMA,  ID_LIVELLO)
        values (IdNextConfigNotif,   pNotifiche(i).ID_SISTEMA, pNotifiche(i).ID_LIVELLO);
    END LOOP;


    EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END AddSpvconfignotifica;


    PROCEDURE GetListSpvconfignotifica(pRefCurs OUT PKG_UtlGlb.t_query_cur)
    AS
        sql_query varchar2(3000) := 'select nome, ID_SPVCONFIGNOTIFICA , OGGETTO_MAIL, TESTO_MAIL , MAIL_LIST,TIPO_ALLEGATO ';

    begin
        for x in (select distinct ID_SISTEMA from SPV_SISTEMA) --  where visible =1) per estrarre solo i sistemi abilitati
        loop
            sql_query := sql_query ||
              ' , sum(case when ID_SISTEMA = '''||x.ID_SISTEMA||''' then id_livello else 0 end) as '||  'spv_sistema'|| x.ID_SISTEMA;--REPLACE(REPLACE(x.DESCRIZIONE,' ', '_'),'+','_') ;

               -- dbms_output.put_line(sql_query);
        end loop;

        sql_query := sql_query || ' from (SELECT NOME, SPV_SISTEMA.ID_SISTEMA,  SPV_CONFIG_NOTIFICA_ALARMLIST.ID_LIVELLO,  SPV_CONFIG_NOTIFICA.ID_SPVCONFIGNOTIFICA, SPV_CONFIG_NOTIFICA.OGGETTO_MAIL,SPV_CONFIG_NOTIFICA.TESTO_MAIL,SPV_CONFIG_NOTIFICA.MAIL_LIST,SPV_CONFIG_NOTIFICA.TIPO_ALLEGATO
          FROM SPV_CONFIG_NOTIFICA
               INNER JOIN SPV_CONFIG_NOTIFICA_ALARMLIST
                  ON (SPV_CONFIG_NOTIFICA.ID_SPVCONFIGNOTIFICA =
                         SPV_CONFIG_NOTIFICA_ALARMLIST.ID_SPVCONFIGNOTIFICA)
               INNER JOIN SPV_SISTEMA
                  ON (SPV_SISTEMA.ID_SISTEMA =
                         SPV_CONFIG_NOTIFICA_ALARMLIST.ID_SISTEMA)
         WHERE VISIBLE = 1) group by nome, ID_SPVCONFIGNOTIFICA, OGGETTO_MAIL, TESTO_MAIL , MAIL_LIST,TIPO_ALLEGATO';

        open pRefCurs for sql_query;
     EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetListSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END GetListSpvconfignotifica;

    PROCEDURE DeleteSpvconfignotifica(pid_confignotifica IN  number)
    AS
    BEGIN

    DELETE FROM SPV_CONFIG_NOTIFICA_ALARMLIST T
    WHERE T.ID_SPVCONFIGNOTIFICA = pid_confignotifica;

    DELETE FROM SPV_CONFIG_NOTIFICA T
    WHERE T.ID_SPVCONFIGNOTIFICA = pid_confignotifica;


    EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.DeleteSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END DeleteSpvconfignotifica;


END PKG_SUPERVISIONE;
/

show errors;