PROMPT Package PKG_TRANSFER_RES

CREATE OR REPLACE PACKAGE PKG_TRANSFER_RES AS

/********************************************************************************************************************
    2.2.9    18/09/2017     Zanini        Modificata procedura sp_transfer_mis_R per recupero dati anche 1/4 ora
    2.2.10   28/09/2017     Zanini        Modificata procedura sp_transfer_mis_R  es p_transfer_mis per ritardo invio
    2.2.11   16/10/2017     Zanini        Modificata procedura sp_transfer_mis_R  per prendere solo i tipi clienti attivi 
 ********************************************************************************************************************/


 PROCEDURE  sp_transfer_mis_R;
 PROCEDURE  sp_transfer_mis;
 PROCEDURE  sp_transfer_LOOP_mis_R (P_DATA_INIZIO IN date,
                                                                P_DATA_FINE IN date default sysdate,
                                                                P_intervallo IN number,
                                                                P_aggr  IN number,
                                                                P_ESITO OUT number,
                                                                P_MSG  OUT VARCHAR2);
                                                                
 PROCEDURE  sp_transfer_LOOP_mis    (P_DATA_INIZIO IN date,
                                                                P_DATA_FINE IN date default sysdate,
                                                                P_intervallo IN number,
                                                                P_aggr  IN number,
                                                                P_ESITO OUT number,
                                                                P_MSG  OUT VARCHAR2);                                                                
 
 procedure SP_STORICO_REGISTRO_INVIO;
 
 PROCEDURE  sp_transfer_ritardo_mis_R;

 PROCEDURE  sp_transfer_ritardo_mis;

 PROCEDURE GetElementForRES   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1);

 PROCEDURE GetTrasformatoriforRES     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pDisconnect IN NUMBER DEFAULT 0);



END PKG_TRANSFER_RES;
/