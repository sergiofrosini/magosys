PROMPT TYPE: T_SISLIV

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SISLIV_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_SISLIV_ARRAY non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SISLIV_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_SISLIV_OBJ non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

CREATE OR REPLACE TYPE T_SISLIV_OBJ AS OBJECT
(
  ID_LIVELLO NUMBER,
  ID_SISTEMA NUMBER
)
/

CREATE OR REPLACE TYPE T_SISLIV_ARRAY AS TABLE OF T_SISLIV_OBJ
/

