SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.2.7 SRC
PROMPT file :  build_Mago_2_2_7.sql
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT
@./Types/T_SISLIV.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/SPV_CONFIG_NOTIFICA.sql
@./Tables/SPV_CONFIG_NOTIFICA_ALARMLIST.sql

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT
@./Sequences/SPV_CONFIG_NOTIF_SEQ.sql

PROMPT _______________________________________________________________________________________
PROMPT Triggers
PROMPT
@./Triggers/AFT_IUR_ELEMENTI_DEF.sql

PROMPT _______________________________________________________________________________________
PROMPT DBLinks
PROMPT
@./DBLinks/PKG1_CREL.sql

PROMPT _______________________________________________________________________________________
PROMPT Synonyms
PROMPT
@./Synonyms/CREL_PKG_CREL.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_SUPERVISIONE.sql
@./Packages/PKG_MAGO.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_TRANSFER_RES.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_SUPERVISIONE.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_TRANSFER_RES.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.2.7 SRC
PROMPT

spool off
