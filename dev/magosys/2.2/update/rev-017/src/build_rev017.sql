SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.2 rev 17
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT

@./PackageBodies/PKG_REPORTS.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.2 rev 17
PROMPT =======================================================================================

SPOOL OFF
