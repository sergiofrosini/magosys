PROMPT PACKAGE: PKG_METEO

CREATE OR REPLACE PACKAGE "PKG_METEO" AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.4   
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */
 --Codici Tipo Coord
 cTipoCoord_Comuni          CONSTANT CHAR(1)  := 'C';
 cTipoCoord_Punti              CONSTANT CHAR(1)  := 'P';

 cTipoResetAll                     CONSTANT INTEGER  := 0;
 cTipoResetDelta                CONSTANT INTEGER  := 1;
 cTipoResetModel                CONSTANT INTEGER  := 2;

function gRegExpSepara RETURN VARCHAR2;

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetCitta_Cliente_TrasfMT(pCodGest      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                   pData         IN DATE) RETURN METEO_REL_ISTAT.COD_CITTA%TYPE;

 PROCEDURE GetCodiciMeteo       (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pFlagDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetProduttori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

PROCEDURE GetProduttori2        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C');

 PROCEDURE GetGeneratori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetGeneratori2        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C');

 PROCEDURE GetTrasformatori     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pDisconnect IN NUMBER DEFAULT 0);

  PROCEDURE GetTrasformatori2     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1);
 PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                  pTipoMeteo      IN INTEGER,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrev IN VARCHAR2 DEFAULT NULL);



 PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY);

 PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY);

 PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pTipiElemento   IN VARCHAR2,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrevMeteo IN VARCHAR2 DEFAULT '0');


 PROCEDURE ElaboraMisure        (pMisureMeteo    IN T_MISMETEO_ARRAY);


 PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                 pFinishTimestamp IN DATE);

 PROCEDURE GetMeteoDistribListCO(pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetMeteoDistribList  (pRefCurs       OUT PKG_UtlGlb.t_query_cur, pTipoGeo IN VARCHAR2 DEFAULT 'C');

 PROCEDURE SetForecastParamIntervTraining (pForecastParam  IN T_PARAM_INTERV_TRAINING_ARRAY);

 PROCEDURE GetAlgorithmParameter( pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                                      pValidDate IN DATE
                        );


/***       SUPERVISIONE FORECAST PARAMETER          **/

  PROCEDURE  ResetForecastParameter(pRefCurs       OUT PKG_UtlGlb.t_query_cur ,  --  -1 errore
                                                          pListaCodElemento  IN VARCHAR2,  -- Lista codici separati da |
                                                         pCodPrevMeteo IN INTEGER , -- 1 o 2
                                                         pTipoReset IN INTEGER --1 = Delta 2 = Model  0 = Entrambi
                                                          );

PROCEDURE GetAnagElementForecastParam(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestElem      IN VARCHAR2 ) ;     -- CODICE GESTIONALE

PROCEDURE GetDettaglioForecastParam(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestElem      IN VARCHAR2  ,    -- CODICE GESTIONALE
                               ptipoFonte           IN VARCHAR2 ,       -- TIPO_FONTE
                               pcod_prev_meteo IN VARCHAR2,
                               pcod_tipo_coord IN VARCHAR2
                               ) ;


PROCEDURE UpdateAlgorithmParameter(
                        pRefCurs       OUT PKG_UtlGlb.t_query_cur ,    -- 0 ok
                        pgg_misure    IN NUMBER,    -- Utilizza misure [giorni]
                        pgg_ogni    IN NUMBER,    -- Addestramento ogni [giorni]
                        pjmax        IN NUMBER,    -- parametro J MAX
                        palfamax    IN NUMBER,    -- parametro ALFA MAX
                        palfamin    IN NUMBER     -- parametro ALFA MIN
                        );




PROCEDURE Get_ListParameterChange (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE,
                        pcod_tipo_fonte IN VARCHAR2,
                        pcod_prec_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2
);


PROCEDURE Get_IntervalloAddestraParam (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE,
                        pcod_tipo_fonte IN VARCHAR2 ,
                        pcod_prev_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2
);


PROCEDURE Get_InfoTableParameter (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pcod_tipo_fonte IN VARCHAR2,
                        pcod_prec_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE
);

FUNCTION GetNumProduttori      ( pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pDisconnect     IN NUMBER DEFAULT 0) return number ;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Meteo;
/