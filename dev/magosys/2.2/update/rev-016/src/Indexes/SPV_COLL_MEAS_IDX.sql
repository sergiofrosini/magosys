Prompt INDEX  SPV_COLL_MEAS_IDX
-- LA CREAZIONE DELL'INDICE RICHIEDE TEMPI LUNGHI
-- SE ESISTE GIA' MEGLIO NON FARE DROP/CREATE
DECLARE 
  v_cnt NUMBER :=0;
BEGIN 
   SELECT COUNT(*)
     INTO v_cnt
	 FROM user_indexes
	WHERE index_name='SPV_COLL_MEAS_IDX'; 

  IF v_cnt= 0 THEN
   EXECUTE IMMEDIATE 'CREATE INDEX spv_coll_meas_idx '||
                             ' ON spv_collector_measure '||
                             ' (collector_name,cod_gest_elemento,data_misura,cod_tipo_misura) '||
                             ' tablespace mago_idx ';
   
  ELSE
    dbms_output.put_line('index already SPV_COLL_MEAS_IDX exists');
  END IF;   
-- EXCEPTION 
--     WHEN OTHERS THEN 
--             NULL;
--  
END;
/
 
