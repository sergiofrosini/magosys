SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.2 rev 16
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2

PROMPT ==========================================
PROMPT Indexes
PROMPT __________________________________________
@./Indexes/SPV_COLL_MEAS_IDX.sql

PROMPT ==========================================
PROMPT InitTables
PROMPT __________________________________________
@./InitTables/METEO_JOB_STATIC_CONFIG.sql

PROMPT ==========================================
PROMPT Packages
PROMPT __________________________________________
@./Packages/PKG_SUPERVISIONE.sql

PROMPT ==========================================
PROMPT PackageBodies
PROMPT __________________________________________
@./PackageBodies/PKG_PROFILI.sql
@./PackageBodies/PKG_SUPERVISIONE.sql

PROMPT ==========================================
PROMPT SchedulerJobs
PROMPT __________________________________________
@./ScheduledJobs/MAGO_SPV_CLEAN_COLL_MIS.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.2 rev 16
PROMPT =======================================================================================

SPOOL OFF
