PROMPT PACKAGE BODY: PKG_SPV_ANAG_AUI

CREATE OR REPLACE PACKAGE BODY PKG_SPV_ANAG_AUI AS


/* ***********************************************************************************************************
   NAME:       PKG_SPV_ANAG_AUI
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      10/04/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1
   2.2.2      12/05/2017  Frosini           Modifiche dopo test 			   -- Spec. Version 2.2.2
   NOTES:

*********************************************************************************************************** */



	g_CollectorName spv_collector_info.collector_name%type 	:='ANAGRAFICA_AUI';
	g_IdSistema spv_dettaglio_parametri.ID_SISTEMA%type 	:= 6;
	gID_CHIAVE_SPV_ENABLED VARCHAR2(100 BYTE) := 'aui.supervision_enabled';

    g_SogliaLinea NUMBER := 6000; -- 6MW

    gMis_PI     VARCHAR2(10)  := 'PI';
    gMis_PAS    VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT VARCHAR2(10)  := 'PMC';
    gMis_PRI    VARCHAR2(10)  := 'PRI';


    g_tit_CodGes constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.codGes';
    g_tit_TipCli constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipCli';
    g_tit_TipEle constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipEle';
    g_tit_PI     constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.PI';

    g_sep_CSV_excel constant VARCHAR2(1) := ';';

    --mago.supervision.detail.anagraficaAui.elenco.title


    -- 01 err 28,'Produttore senza generatori sottesi');
    -- 02 err 29,'Generatore MT con Potenza Installata non definita');
    -- 03 err 30,'Generatore MT o TR MT/BT con fonte d�energia definita ma con Potenza Installata minore od uguale a z');
    -- 04 err 31,'La PI del produttore � diverso dalla somma PI dei generatori sottesi');
    -- 05 err 32,'Gruppo con potenza superiore a 6MW senza linea MT dedicata');
    -- 06 err 33,'Fonte di energia per PRODUTTORI MT o TR MT/BT non definita in AUI');
    -- 07 err 34,'Fonte energia non associata ad un generatore MT');
    -- 08 err 35,'Produttori MT o TR MT/BT senza valorizzazione campo Tipo Produttore');
    -- 09 err 36,'POD0 ai quali corrispondono pi� di un Produttore MT');
    -- 10 err 37,'Clienti MT marcati come �non produttori� ma con generatori associati');
    -- 11 err 38,'Clienti MT o TR MT/BT con cos(phi)<0 che darebbero luogo a PI<=0');
    -- 12 err 39,'Montanti di linea MT marcati erroneamente come clienti MT');
    -- 13 err 40,'Sbarre MT di CS per le quali non sono disponibili le coordinate lat/log');
    -- 14 err 41,'Sbarre MT di CS non agganciate in tutte le gerarchie ');
    -- 15 err 42,'Produttori MT o TR MT/BT senza comune ISTAT associato');
    -- 16 err 43,'Produttori MT con PI>0 ed il campo Tipo Produttore non specificato');
    -- 17 err 44,'Produttori MT Puri probabilmente marcati in AUI in modo erroneo');
	pID_DIZIONARIO_ALLARME_01 spv_dizionario_allarmi.id_dizionario_allarme%type := 28;
	pID_DIZIONARIO_ALLARME_02 spv_dizionario_allarmi.id_dizionario_allarme%type := 29;
	pID_DIZIONARIO_ALLARME_03 spv_dizionario_allarmi.id_dizionario_allarme%type := 30;
	pID_DIZIONARIO_ALLARME_04 spv_dizionario_allarmi.id_dizionario_allarme%type := 31;
	pID_DIZIONARIO_ALLARME_05 spv_dizionario_allarmi.id_dizionario_allarme%type := 32;
	pID_DIZIONARIO_ALLARME_06 spv_dizionario_allarmi.id_dizionario_allarme%type := 33;
	pID_DIZIONARIO_ALLARME_07 spv_dizionario_allarmi.id_dizionario_allarme%type := 34;
	pID_DIZIONARIO_ALLARME_08 spv_dizionario_allarmi.id_dizionario_allarme%type := 35;
	pID_DIZIONARIO_ALLARME_09 spv_dizionario_allarmi.id_dizionario_allarme%type := 36;
	pID_DIZIONARIO_ALLARME_10 spv_dizionario_allarmi.id_dizionario_allarme%type := 37;
	pID_DIZIONARIO_ALLARME_11 spv_dizionario_allarmi.id_dizionario_allarme%type := 38;
	pID_DIZIONARIO_ALLARME_12 spv_dizionario_allarmi.id_dizionario_allarme%type := 39;
	pID_DIZIONARIO_ALLARME_13 spv_dizionario_allarmi.id_dizionario_allarme%type := 40;
	pID_DIZIONARIO_ALLARME_14 spv_dizionario_allarmi.id_dizionario_allarme%type := 41;
	pID_DIZIONARIO_ALLARME_15 spv_dizionario_allarmi.id_dizionario_allarme%type := 42;
	pID_DIZIONARIO_ALLARME_16 spv_dizionario_allarmi.id_dizionario_allarme%type := 43;
	pID_DIZIONARIO_ALLARME_17 spv_dizionario_allarmi.id_dizionario_allarme%type := 44;

    vPARAMETRI_DESCRIZIONE VARCHAR2(1000);

/*============================================================================*/
function f_Date2DettStr(pData in date) return varchar2 IS

BEGIN
 return to_char(pData,'dd/mm/yyyy')||' ore '||to_char(pData,'hh24:mi');
END;
/*============================================================================*/

PROCEDURE printa(pSTR IN VARCHAR2) IS

BEGIn
	PKG_Logs.TraceLog(psTR, PKG_UtlGlb.gcTrace_VRB);
END;

/*============================================================================*/

PROCEDURE sp_AddUnCollector(pErrore       IN INTEGER,
                            pGestElem     IN VARCHAR2,
                            pTipoElem     IN VARCHAR2 DEFAULT NULL,
                            pData         IN DATE DEFAULT SYSDATE) AS

BEGIN

   pkg_supervisione.AddSpvCollectorInfo(g_CollectorName,
                                        pData,
                                        g_IdSistema,
                                        pErrore,
                                        null,
                                        pGestElem,
                                        null,
                                        null,
                                        1,
                                        pTipoElem);

END sp_AddUnCollector;

/*============================================================================*/

PROCEDURE sp_CheckProdAUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vMsg        GTTD_VALORI_TEMP.ALF3%TYPE;
    vSql        VARCHAR2(4000) ;

    vNum INTEGER := 0;
    vContaGen  INTEGER;
    vPrd VARCHAR2(123);
    vGen VARCHAR2(50);

    vSommaxGruppo NUMBER;

    vPIgen NUMBER;


    /* ====================================================================== */

    PROCEDURE sp_CheckProdPAS_AUI(pCodGest IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pCodtipo IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE) IS

        vQuanti NUMBER;

    BEGIN
     
    SELECT COUNT(*)
        INTO vQuanti
        FROM (
            select precedente,successivo from (
            (
            SELECT m.valore attuale,
                LAG( m.valore ) OVER (partition by cod_trattamento_elem order by m.data) precedente,
                LEAD( m.valore ) OVER (partition by cod_trattamento_elem order by m.data) successivo,
                COD_ELEMENTO
            from (select * from misure_acquisite where trunc(data)
                 between trunc(pData-2) and trunc(pData-1)) m
            join (select * from trattamento_elementi 
                    where cod_elemento=pkg_elementi.GetCodElemento(pCodGest)) t
            using (cod_trattamento_elem)
            where t.cod_tipo_misura = 'PAS'
          )
        ) where attuale=0 )
        where precedente <> 0
        and successivo <> 0;   
        
        
        IF vQuanti > 0 THEN
            -- AUI 17 Produttori MT Puri probabilmente marcati in AUI in modo erroneo
            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_17, pCodGest, pCodTipo);
        END IF;

    END sp_CheckProdPAS_AUI;

    /* ====================================================================== */

    PROCEDURE sp_CheckLineaGruppo(pCodGest IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pCodtipo IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE) IS

        vQuanti NUMBER;

    BEGIN

        select count(e.cod_gest_elemento)
        into vQuanti
        from
        (select * from v_elementi where cod_gest_elemento=pCodGest) e
        join
        (select * from gerarchia_imp_sn
                where pData between data_attivazione and data_disattivazione) g
        on (e.cod_elemento=g.cod_elemento)
        join (select * from v_elementi where cod_tipo_elemento='LMT')  t
        on (
               g.l01 = t.cod_elemento
            or g.l02 = t.cod_elemento
            or g.l03 = t.cod_elemento
            or g.l04 = t.cod_elemento
            or g.l05 = t.cod_elemento
            or g.l06 = t.cod_elemento
            or g.l07 = t.cod_elemento
            or g.l08 = t.cod_elemento
            or g.l09 = t.cod_elemento
            or g.l10 = t.cod_elemento
            or g.l11 = t.cod_elemento
            or g.l12 = t.cod_elemento
            or g.l13 = t.cod_elemento
            or g.l14 = t.cod_elemento
            or g.l15 = t.cod_elemento
            or g.l16 = t.cod_elemento
            or g.l17 = t.cod_elemento
            or g.l18 = t.cod_elemento
            or g.l19 = t.cod_elemento
            or g.l20 = t.cod_elemento
            or g.l21 = t.cod_elemento
            or g.l22 = t.cod_elemento
            or g.l23 = t.cod_elemento
            or g.l24 = t.cod_elemento
            or g.l25 = t.cod_elemento
        );

        IF vQuanti = 0 THEN
            -- AUI 05 Gruppo con potenza superiore a 6MW senza linea MT dedicata
            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_05, pCodGest, pCodTipo);
        END IF;

    END;

    /* ====================================================================== */

BEGIN

-- CLIENT

    FOR iClient IN (WITH
            CLI_MAGO AS (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_CLIENTE,POTENZA_INSTALLATA
                              ,COD_TIPO_ELEMENTO
                              ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'COM', pData, 2, 1) COMUNE
                              ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'CPR', pData, 1, 1) CAB_PRI
                          FROM V_ELEMENTI
                         WHERE COD_TIPO_ELEMENTO IN ( 'CMT','TRM')
                           AND COD_TIPO_CLIENTE IN ('A','B')
                       )
           ,CLI_AUI AS (SELECT A.GEST_PROD, a.TIPO_ELEMENTO TIPO_AUI,
                               A.RAGIO_SOC   NOME_PROD,
                                A.TIPO_FORN   TIPO_FORN,
                               A.POT_GRUPPI
                          FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD,
                                       C.RAGIO_SOC,C.TIPO_FORN,C.POT_GRUPPI,C.TIPO_ELEMENTO
                                  FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  LEFT OUTER JOIN TRASFORMATORI_TLC@PKG1_STMAUI.IT t
                                  ON (t.COD_ORG_NODO||t.SER_NODO||t.NUM_NODO||t.TIPO_ELEMENTO||t.ID_TRASF=
                                            C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)
                                 WHERE --C.TIPO_FORN IN ('AP','PP','PD')
                                   --AND
                                   C.TRATTAMENTO = 0 AND C.STATO = 'E'
                               ) A
                         GROUP BY A.GEST_PROD,a.TIPO_ELEMENTO,A.RAGIO_SOC,A.TIPO_FORN,A.POT_GRUPPI
                       )
           ,CLI_PI  AS (SELECT COD_GEST_ELEMENTO,M.VALORE PI_MISURA
                          FROM CLI_MAGO E
                          LEFT OUTER JOIN TRATTAMENTO_ELEMENTI T
                                       ON T.COD_ELEMENTO = E.COD_ELEMENTO
                                      AND T.COD_TIPO_MISURA = 'PI'
                                      AND TIPO_AGGREGAZIONE = 1
                          LEFT OUTER JOIN MISURE_AGGREGATE_STATICHE M
                                       ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                      AND pData BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE
                       )
      SELECT distinct COD_GEST_ELEMENTO,
             COD_TIPO_ELEMENTO,
             NVL(COD_GEST_AUI,' ') COD_GEST_AUI,
             NVL(COD_GEST_MAGO,' ') COD_GEST_MAGO,
             TIPO_AUI,
             NVL(NOME_PROD,' ') NOME_PROD,
             NVL(CAB_PRI,' ') CAB_PRI,
             COMUNE,
             IS_PROD_AUI,
             NVL(IN_AUI,' ') IN_AUI,
             NVL(IN_MAGO,' ') IN_MAGO,
             TP_AUI,
             TP_MAGO,
             PI_AUI,
             PI_MAGO_ELE
        FROM (SELECT A.*,PI_MISURA PI_MAGO_MIS
                FROM (SELECT NVL(M.COD_GEST_ELEMENTO,A.GEST_PROD) COD_GEST_ELEMENTO,
                             M.COD_GEST_ELEMENTO COD_GEST_MAGO,M.COD_TIPO_ELEMENTO,
                             A.GEST_PROD COD_GEST_AUI,A.TIPO_AUI,
                             M.NOME_ELEMENTO NOME_PROD,
                             PKG_ELEMENTI.GETGESTELEMENTO(CAB_PRI) CAB_PRI,
                             C.NOME_ELEMENTO COMUNE,
                             CASE WHEN A.TIPO_FORN IN ('AP','PP','PD') THEN 1 ELSE 0 END IS_PROD_AUI,
                             CASE WHEN A.TIPO_FORN IS NULL THEN NULL ELSE '  si  ' END IN_AUI,
                             CASE WHEN M.COD_TIPO_CLIENTE IS NULL THEN NULL ELSE '  si   ' END IN_MAGO,
                             A.TIPO_FORN TP_AUI,
                             t.cod_raggr_fonte TP_MAGO,
                             A.POT_GRUPPI PI_AUI,
                             M.POTENZA_INSTALLATA PI_MAGO_ELE
                        FROM CLI_AUI A
                        FULL OUTER JOIN CLI_MAGO   M ON M.COD_GEST_ELEMENTO = A.GEST_PROD
                        LEFT OUTER JOIN (SELECT f.cod_raggr_fonte,f.cod_tipo_fonte  FROM RAGGRUPPAMENTO_FONTI r
                             left join TIPO_FONTI f
                               ON (r.cod_raggr_fonte = f.cod_raggr_fonte
                                  AND f.COD_RAGGR_FONTE_ALFA_RIF IS NULL)
                             ) T
                        ON M.COD_TIPO_CLIENTE = T.cod_tipo_fonte
                        LEFT OUTER JOIN V_ELEMENTI C ON C.COD_ELEMENTO      = COMUNE
                     ) A
                LEFT OUTER JOIN CLI_PI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
              ) A  ORDER BY CAB_PRI NULLS LAST, IN_MAGO NULLS LAST,IN_AUI NULLS LAST,A.COD_GEST_ELEMENTO
     ) LOOP


            IF iClient.TP_AUI = 'AP' AND iClient.TP_MAGO IN ('T','I') THEN
                -- se definito puro termico/idraulico controllo la PAS
                sp_CheckProdPAS_AUI(iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
            END IF;


			IF iClient.COMUNE IS NULL THEN
               -- AUI 15 comune non associato
			   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_15, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
			END IF;

			IF iClient.TP_AUI IS NULL THEN
               -- AUI 08 tipo prod non definito
			   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
			END IF;


            IF iClient.TP_MAGO IS NULL THEN

                if iClient.IS_PROD_AUI = 1 THEN
                    -- AUI 06 tipo fonte non definita per cliente produttore
                   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_06, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
                END IF;

            ELSE

                IF (iClient.COD_TIPO_ELEMENTO='TRM') AND NVL(iClient.PI_AUI,0)<=0 THEN
                    -- AUI 03 - Trasf. con fonte definita ma con PI<=0
                    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
                END IF;

            END IF;



           vNum := vNum + 1;
           vContaGen := 0;

           vSommaxGruppo := 0;

           FOR iGen IN (SELECT distinct G.COD_ORG_NODO,G.SER_NODO,G.NUM_NODO,G.TIPO_ELEMENTO,G.ID_GENERATORE,
                 G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE GEST_GEN,
                 G.TIPO_IMP,F.COD_TIPO_FONTE,
                 E.COD_TIPO_ELEMENTO,
                 G.P_APP_NOM,G.N_GEN_PAR,G.F_P_NOM
            FROM GENERATORI_TLC@PKG1_STMAUI.IT G
            JOIN V_ELEMENTI E
            ON (E.COD_GEST_ELEMENTO = G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE)
            LEFT OUTER JOIN TIPO_FONTI F ON F.COD_TIPO_FONTE = G.TIPO_IMP
           WHERE G.COD_ORG_NODO = SUBSTR(iClient.cod_gest_elemento,1,4)
             AND G.SER_NODO     = SUBSTR(iClient.cod_gest_elemento,5,1)
             AND G.NUM_NODO     = SUBSTR(iClient.cod_gest_elemento,6,6)
             AND G.ID_CL        = SUBSTR(iClient.cod_gest_elemento,13,2)
             AND G.TRATTAMENTO  = 0
             AND G.STATO        = 'E'
         ) LOOP


            vContaGen := vContaGen + 1;

            vPIgen := NVL(iGen.P_APP_NOM,0)*NVL(iGen.N_GEN_PAR,0)*NVL(iGen.F_P_NOM,0);

            vSommaxGruppo := vSommaxGruppo + vPIgen;


            IF NVL(iGen.F_P_NOM,0)<0 THEN
               -- AUI 11 cosphi < 0
               sp_AddUnCollector(pID_DIZIONARIO_ALLARME_11, iGen.gest_gen, iGen.cod_tipo_elemento);
            END IF;

            IF iGen.TIPO_IMP IS NULL OR iGen.COD_TIPO_FONTE IS NULL THEN
               -- AUI 07 tipo fonte non definita
               sp_AddUnCollector(pID_DIZIONARIO_ALLARME_07, iGen.gest_gen, iGen.cod_tipo_elemento);
            ELSE

                IF iGen.P_APP_NOM IS NULL OR iGen.N_GEN_PAR IS NULL OR iGen.F_P_NOM IS NULL THEN
                    -- AUI 02 pI non definita (con tipo fonte definita)
                   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_02, iGen.gest_gen, iGen.tipo_elemento);
                END IF;

                IF vPIGen<=0 THEN
                    -- AUI 03 pI <= 0 (con tipo fonte definita)
                   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, iGen.gest_gen, iGen.cod_tipo_elemento);
                END IF;

            END IF;

        END LOOP;


        IF vContaGen = 0  AND iClient.IS_PROD_AUI = 1 THEN
            -- AUI 01 cliente produttore senza generatori
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF vContaGen > 0 AND iClient.IS_PROD_AUI = 0 THEN
            -- AUI 10 cliente non produttore ma con generatori
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_10, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF (NOT iClient.TP_AUI IS NULL) AND NVL(iClient.PI_AUI,0) > 0 THEN
            -- AUI 16 PI senza definizione tipo produttore
            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_16, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
        END IF;

        IF vSommaxGruppo <> iClient.PI_AUI THEN
            -- AUI 04 somma PI dei gen <> PI del prod
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_04, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF vSommaxGruppo > g_sogliaLinea THEN
            sp_CheckLineaGruppo(iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;


    END LOOP;


END sp_CheckProdAUI;

/*============================================================================*/

PROCEDURE sp_CheckSbarreAUI (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga in (
            select cod_gest_elemento from
            (select * from v_elementi where cod_tipo_elemento = 'SCS') e
             JOIN AUI_NODI_TLC B
               ON     B.COD_ORG_NODO =
                         SUBSTR (e.COD_GEST_ELEMENTO, 1, 4)
                  AND B.SER_NODO = SUBSTR (e.COD_GEST_ELEMENTO, 5, 1)
                  AND B.NUM_NODO = SUBSTR (e.COD_GEST_ELEMENTO, 6, 6)
                  AND B.TRATTAMENTO = 0
                  AND B.STATO = 'E'
                  AND (NVL(B.gps_x,0)=0 or NVL(B.gps_y,0)=0)
    ) LOOP


            -- AUI 13 Sbarra CS senza coordinate
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_13, iRiga.COD_GEST_ELEMENTO, 'SCS');


    END LOOP;

END sp_CheckSbarreAUI;

/*============================================================================*/

PROCEDURE sp_CheckSbarreMAGO (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga in (
            select cod_elemento
            from
            (select cod_elemento from v_elementi where cod_tipo_elemento='SCS' ) e
            natural join
            (select cod_elemento, 'AMM' ger from gerarchia_amm where pData between data_attivazione and data_disattivazione
             union
             select cod_elemento, 'IMP' ger from gerarchia_imp_sn where pData between data_attivazione and data_disattivazione
             union
             select cod_elemento, 'GEO' ger from gerarchia_geo where pData between data_attivazione and data_disattivazione) g
            group by cod_elemento
            having count(g.ger)<3
    ) LOOP


            -- AUI 14 Sbarra CS non presente su tutte le gerarchie
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_14,
                              PKG_ELEMENTI.GetGestElemento(iRiga.COD_ELEMENTO),
                              'SCS');


    END LOOP;

END sp_CheckSbarreMAGO;

/*============================================================================*/

PROCEDURE sp_CheckMontanti_AUI  (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga in (
            WITH
            iClienti AS
                (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE COD_GEST_ELEMENTO
                    FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                    LEFT OUTER JOIN TRASFORMATORI_TLC@PKG1_STMAUI.IT t
                    ON (t.COD_ORG_NODO||t.SER_NODO||t.NUM_NODO||t.TIPO_ELEMENTO||t.ID_TRASF=
                    C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)
                    WHERE C.TIPO_FORN IN ('AP','PP','PD')
                    AND C.TRATTAMENTO = 0 AND C.STATO = 'E'),
            iMontanti AS
                (SELECT COD_GEST_ELEMENTO
                    FROM v_ELEMENTI
                    WHERE COD_TIPO_ELEMENTO = 'LMT')
            SELECT COD_GEST_ELEMENTO
            FROM iClienti
            NATURAL JOIN iMontanti
    ) LOOP

            -- AUI 12 Montante MT definito come Cliente
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_12, iRiga.COD_GEST_ELEMENTO,'LMT');

    END LOOP;

END sp_CheckMontanti_AUI;

/*============================================================================*/

PROCEDURE sp_CheckProdPod_AUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga in (SELECT C.POD
                    FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                    LEFT OUTER JOIN TRASFORMATORI_TLC@PKG1_STMAUI.IT t
                    ON (t.COD_ORG_NODO||t.SER_NODO||t.NUM_NODO||t.TIPO_ELEMENTO||t.ID_TRASF=
                        C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)
                    WHERE C.TIPO_FORN IN ('AP','PP','PD')
                    AND C.TRATTAMENTO = 0 AND C.STATO = 'E'
                    AND NOT C.POD IS NULL
                    GROUP BY C.POD
                    HAVING count(C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)>1
     ) LOOP

            -- AUI 09 POD associato a pi� codici gestionali
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_09, iRiga.POD);

     END LOOP;

END sp_CheckProdPod_AUI;

/*============================================================================*/

PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

 vOra TIMESTAMP;
BEGIN

vOra := current_timestamp;
printa(RPAD('in  sp_CheckProdAUI ',40,' '));
 sp_CheckProdAUI  (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckProdAUI ',40,' ')||to_char(current_timestamp-vOra));

vOra := current_timestamp;
printa(RPAD('in  sp_CheckSbarreAUI ',40,' '));
 sp_CheckSbarreAUI (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckSbarreAUI ',40,' ')||to_char(current_timestamp-vOra));

vOra := current_timestamp;
printa(RPAD('in  sp_CheckSbarreMAGO ',40,' '));
 sp_CheckSbarreMAGO (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckSbarreMAGO ',40,' ')||to_char(current_timestamp-vOra));

vOra := current_timestamp;
printa(RPAD('in  sp_CheckMontanti_AUI ',40,' '));
 sp_CheckMontanti_AUI  (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckMontanti_AUI ',40,' ')||to_char(current_timestamp-vOra));

vOra := current_timestamp;
printa(RPAD('in  sp_CheckProdPod_AUI ',40,' '));
 sp_CheckProdPod_AUI (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckProdPod_AUI ',40,' ')||to_char(current_timestamp-vOra));

END sp_CheckAll;

/*============================================================================*/

procedure auiSpvCollector
	is
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvCollector',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvCollector - Start';
		printa(vStrLog);

		sp_CheckAll;

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvCollector - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
		EXCEPTION
		WHEN OTHERS THEN
		   printa ('PKG_SPV_ANAG_AUI.auiSpvCollector error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));

end auiSpvCollector;

/*============================================================================*/

procedure auiSpvProcessor
	is
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento varchar2(100);
		p_cod_tipo_elemento varchar2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%type;
		pID_DIZIONARIO_ALLARME number;
		pDATA_ALLARME date := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := null;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := null;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := null;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := null;


		n_allarmi_1 	number;
		n_allarmi_2 	number;
		n_allarmi_3 	number;
		n_allarmi_3_GMT number;
		n_allarmi_3_TRM number;
		n_allarmi_4 	number;
		n_allarmi_5 	number;
		n_allarmi_6_CMT number;
		n_allarmi_6_TRM number;
		n_allarmi_7 	number;
		n_allarmi_8 	number;
		n_allarmi_9 	number;
		n_allarmi_10 	number;
		n_allarmi_11 	number;
		n_allarmi_12 	number;
		n_allarmi_13 	number;
		n_allarmi_14 	number;
		n_allarmi_15 	number;
		n_allarmi_16 	number;
		n_allarmi_17 	number;

		soglia_allarme_1 		number;
		soglia_allarme_2 		number;
		soglia_allarme_3_gmt 	number;
		soglia_allarme_3_trm 	number;
		soglia_allarme_4 		number;
		soglia_allarme_5 		number;
		soglia_allarme_6_cmt 	number;
		soglia_allarme_6_trm	number;
		soglia_allarme_7 		number;
		soglia_allarme_8 		number;
		soglia_allarme_9 		number;
		soglia_allarme_10 		number;
		soglia_allarme_11 		number;
		soglia_allarme_12 		number;
		soglia_allarme_13 		number;
		soglia_allarme_14 		number;
		soglia_allarme_15 		number;
		soglia_allarme_16 		number;
		soglia_allarme_17 		number;

		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvProcessor',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Start';
		printa(vStrLog);

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_gen');
		soglia_allarme_1 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_nd');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_lt_0');
		soglia_allarme_3_gmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_pi_lt_0');
		soglia_allarme_3_trm := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_pot_gruppi');
		soglia_allarme_4 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_fonte');
		soglia_allarme_6_cmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_no_fonte');
		soglia_allarme_6_trm := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_no_fonte');
		soglia_allarme_7 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_tr_tipo_prod');
		soglia_allarme_8 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_pod0');
		soglia_allarme_9 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_cmt_non_prod');
		soglia_allarme_10 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_cmt_tr_cos_phi_lt_0');
		soglia_allarme_11 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_sbarre_no_coordinate');
		soglia_allarme_13 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_sbarre_non_agganciate');
		soglia_allarme_14 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_tr_no_istat');
		soglia_allarme_15 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gruppi_no_linea');
		soglia_allarme_5 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_montanti');
		soglia_allarme_12 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_pi_gt_0');
		soglia_allarme_16 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_mal_identificati');
		soglia_allarme_17 := TO_NUMBER(P_VALORE, '9999');

		--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'smileGme.n_hour');

		--allarme 3:
		--			GENERATORI MT vs TR MT/bt
		--			GMT / TRM
		--allarme 6:
		--			PRODUTTORI MT vs TR MT/bt
		--			CMT / TRM


		with allarmi as (
		  select d.id_dizionario_allarme
		  , nvl(sum(i.valore), 0) n_allarmi
      , i.informazioni
		  from spv_rel_sistema_dizionario d
		  left join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = g_CollectorName
		  where d.id_sistema = g_IdSistema
		  and trunc(i.data_inserimento) = trunc(sysdate)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  order BY d.id_dizionario_allarme, i.informazioni
		)
		select
		 sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01  , a.n_allarmi, 0)) n_allarmi_1
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_02  , a.n_allarmi, 0)) n_allarmi_2
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_03  , a.n_allarmi, null)) n_allarmi_3
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 and a.informazioni = 'GMT') THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_GMT
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 and a.informazioni = 'TRM') THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_TRM
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_04  , a.n_allarmi, 0)) n_allarmi_4
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_05  , a.n_allarmi, 0)) n_allarmi_5
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_06  , a.n_allarmi, null)) n_allarmi_6
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 and a.informazioni = 'CMT') THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_CMT
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 and a.informazioni = 'TRM') THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_TRM
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_07  , a.n_allarmi, 0)) n_allarmi_7
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_08  , a.n_allarmi, 0)) n_allarmi_8
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_09  , a.n_allarmi, 0)) n_allarmi_9
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_10 , a.n_allarmi, 0)) n_allarmi_10
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_11 , a.n_allarmi, 0)) n_allarmi_11
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_12 , a.n_allarmi, 0)) n_allarmi_12
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_13 , a.n_allarmi, 0)) n_allarmi_13
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_14 , a.n_allarmi, 0)) n_allarmi_14
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_15 , a.n_allarmi, 0)) n_allarmi_15
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_16 , a.n_allarmi, 0)) n_allarmi_16
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_17 , a.n_allarmi, 0)) n_allarmi_17
		into n_allarmi_1,n_allarmi_2,n_allarmi_3_GMT,n_allarmi_3_TRM,n_allarmi_4,n_allarmi_5,n_allarmi_6_CMT,n_allarmi_6_TRM,n_allarmi_7,n_allarmi_8,n_allarmi_9,n_allarmi_10
		,n_allarmi_11,n_allarmi_12,n_allarmi_13,n_allarmi_14,n_allarmi_15,n_allarmi_16,n_allarmi_17
		from allarmi a
		;

		OPEN pRefCurs FOR
			select d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, e.cod_tipo_elemento, i.informazioni
			from spv_rel_sistema_dizionario d
			join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = g_CollectorName
			left join elementi e on e.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
			where d.id_sistema = g_IdSistema
			and trunc(i.data_inserimento) = trunc(sysdate)
			order BY d.id_dizionario_allarme
			;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);


			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 2;

			-- Gli allarmi 5-12-16-17 hanno una soglia di tipo GIALLO sotto la quale non vengono segnalati
			IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_05  and n_allarmi_5  >= soglia_allarme_5 )
				or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_12 and n_allarmi_12 >= soglia_allarme_12)
				or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_16 and n_allarmi_16 >= soglia_allarme_16)
				or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_17 and n_allarmi_17 >= soglia_allarme_17)
					)THEN
				vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'pPARAMETRI_DESCRIZIONE: ' || pPARAMETRI_DESCRIZIONE ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);

			ELSE
				-- tutti gli altri allarmi vengono gestiti cosi:
				-- da 0 a soglia --> allarme Giallo
				-- da soglia in poi --> allarme Rosso
				IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_01  and n_allarmi_1  >= soglia_allarme_1 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_02  and n_allarmi_2  >= soglia_allarme_2 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3  >= soglia_allarme_3 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3_GMT  >= soglia_allarme_3_gmt )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3_TRM  >= soglia_allarme_3_trm )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_04  and n_allarmi_4  >= soglia_allarme_4 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6  >= soglia_allarme_6 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6_CMT  >= soglia_allarme_6_cmt )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6_TRM  >= soglia_allarme_6_trm )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_07  and n_allarmi_7  >= soglia_allarme_7 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_08  and n_allarmi_8  >= soglia_allarme_8 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_09  and n_allarmi_9  >= soglia_allarme_9 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_10 and n_allarmi_10 >= soglia_allarme_10)
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_11 and n_allarmi_11 >= soglia_allarme_11)
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_13 and n_allarmi_13 >= soglia_allarme_13)
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_14 and n_allarmi_14 >= soglia_allarme_14)
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_15 and n_allarmi_15 >= soglia_allarme_15)
				)THEN
					pID_LIVELLO := 3;
				END IF;

				vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
			END IF;
		END LOOP;
		CLOSE PREFCURS;

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_ANAG_AUI.auiSpvProcessor error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end auiSpvProcessor;

/*============================================================================*/

function f_SetDettaglioLob(p_chiave  IN VARCHAR2,
                         p_query     IN VARCHAR2,
                         p_valforced IN NUMBER DEFAULT -1) return number IS

l_cursor integer;
l_cursor_status integer;
l_col_count number;
l_desc_tbl sys.dbms_sql.desc_tab2;
l_col_val varchar2(32767);

l_report blob;
l_raw raw(32767);

l_Quanti NUMBER;

/*----------------------------------------------------------------*/

begin

    -- open BLOB to store CSV file
    dbms_lob.createtemporary( l_report, FALSE );
    dbms_lob.open( l_report, dbms_lob.lob_readwrite );

    -- parse query
    l_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(l_cursor, p_query, dbms_sql.native);
    dbms_sql.describe_columns2(l_cursor, l_col_count, l_desc_tbl );

    -- define report columns
    for i in 1 .. l_col_count loop
        dbms_sql.define_column(l_cursor, i, l_col_val, 32767 );
    end loop;

    -- write column headings to CSV file
    for i in 1 .. l_col_count loop
        l_col_val := l_desc_tbl(i).col_name;
        IF upper(l_col_val) = upper('g_tit_CodGes') THEN
            l_col_val := g_tit_CodGes;
        ELSIF  upper(l_col_val) = upper('g_tit_TipCli') THEN
            l_col_val := g_tit_TipCli;
        ELSIF  upper(l_col_val) = upper('g_tit_TipEle') THEN
            l_col_val := g_tit_TipEle;
        END IF;

        if i = l_col_count then
--            l_col_val := '"'||l_col_val||'"'||chr(10);
            l_col_val := l_col_val||chr(10);
        else
--            l_col_val := '"'||l_col_val||'",';
            l_col_val := l_col_val||g_sep_CSV_excel;
        end if;
        l_raw := utl_raw.cast_to_raw( l_col_val );
        dbms_lob.writeappend( l_report, utl_raw.length( l_raw ), l_raw );
    end loop;

    l_cursor_status := sys.dbms_sql.execute(l_cursor);

    l_Quanti := 0;
    -- write result set to CSV file
    loop
    exit when dbms_sql.fetch_rows(l_cursor) <= 0;

        l_Quanti := l_Quanti + 1;

        for i in 1 .. l_col_count loop
            dbms_sql.column_value(l_cursor, i, l_col_val);
            if i = l_col_count then
         --       l_col_val := '"'||l_col_val||'"'||chr(10);
                l_col_val := l_col_val||chr(10);
            else
         --       l_col_val := '"'||l_col_val||'",';
                l_col_val := l_col_val||g_sep_CSV_excel;
            end if;
            l_raw := utl_raw.cast_to_raw( l_col_val );
            dbms_lob.writeappend( l_report, utl_raw.length( l_raw ), l_raw );
        end loop;
    end loop;

    dbms_sql.close_cursor(l_cursor);
    dbms_lob.close( l_report );

    IF p_valforced > 0 THEN
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   p_valforced,
                                                   l_report);
    ELSE
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   l_Quanti,
                                                   l_report);
    END IF;

    return l_Quanti;
exception
when no_data_found then
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               0,
                                               null);
    return 0;

end f_SetDettaglioLob;

/*============================================================================*/

function f_SetDettaglioNum(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN NUMBER DEFAULT -1) RETURN NUMBER IS

l_Quanti NUMBER;

/*----------------------------------------------------------------*/

begin

    IF p_valforced > 0 THEN
        l_quanti := p_valforced;
    ELSE
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    END IF;

    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               l_Quanti,
                                               NULL);
 
    return l_quanti;

end f_SetDettaglioNum;

/*============================================================================*/

procedure auiSpvDetail
	is


		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);

        v_sql_dummy VARCHAR(2000) := 'SELECT 0 QUANTI  FROM DUAL';
        v_sql_nihil VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL WHERE ROWNUM<1';

        -- RICORDARSI: nelle select per la PI dividere il valore per 1000
        -- che i dati sono in kW ma sul video li vogliono in MW
        
        v_sql_aui_TR_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) ';
        v_sql_aui_consTrMTBT_N  VARCHAR2(2000) := 'SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_TR_from;
        v_sql_aui_consTrMTBT_PI VARCHAR2(2000)  :='SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_TR_from;
                
        v_sql_mago_TR_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO = ''TRM''';
        v_sql_mago_TR_N  VARCHAR2(2000) := 'SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_mago_TR_from;
        v_sql_mago_TR_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_TR_from;
                
                
               
        v_sql_aui_TRprod_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'         AND POT_NOM_1>0 '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) '
                ||' WHERE POTENZA_INSTALLATA>0 ';
        v_sql_aui_TRprod_N  VARCHAR2(2000) := 'SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_TRprod_from;
        v_sql_aui_TRprod_PI VARCHAR2(2000)  := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_TRprod_from;

        v_sql_mago_TRprod_from  VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = ''TRM'') '||
                '    AND POTENZA_INSTALLATA>0 '||
                ' ORDER BY COD_GEST_ELEMENTO';
        v_sql_mago_TRprod_N  VARCHAR2(2000) :=  'SELECT '
                ||' COD_GEST_ELEMENTO g_tit_CodGes '
                ||v_sql_mago_TRprod_from;
        v_sql_mago_TRprod_PI VARCHAR2(2000) := 'SELECT  SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_TRprod_from;


       v_sql_aui_consCliMT_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN ''TRM'' ELSE ''CMT'' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN (''AP'',''PP'',''PD'') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN ) ';
        v_sql_aui_consCliMT_N VARCHAR2(2000) := 'SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_aui_consCliMT_from;
        v_sql_aui_consCliMT_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_consCliMT_from;
        
        v_sql_mago_cliMT_from VARCHAR2(2000) := '   FROM V_ELEMENTI  '||
                ' WHERE COD_TIPO_ELEMENTO=''CMT''  '||
                ' AND COD_TIPO_CLIENTE IN (''A'',''B'',''C'') '||
                ' ';
        v_sql_mago_cliMT_N VARCHAR2(2000) := 'SELECT  '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE g_tit_Tipele '
                ||v_sql_mago_cliMT_from;
        v_sql_mago_cliMT_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_cliMT_from;

        -- clienti BT non reperibili
        v_sql_aui_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_aui_cliBT_PI VARCHAR2(2000) := v_sql_dummy;

        v_sql_mago_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_mago_cliBT_PI VARCHAR2(2000) := v_sql_dummy;


        v_sql_aui_prodPuri_from VARCHAR2(2000) :=' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN (''PP'') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodPuri_N VARCHAR2(2000) :='SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodPuri_from;
        v_sql_aui_prodPuri_PI VARCHAR2(2000) :=  'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_prodPuri_from;

        v_sql_mago_prodPuri_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO=''CMT'' '||
                ' AND COD_TIPO_CLIENTE = ''A''';
        v_sql_mago_prodPuri_N VARCHAR2(2000) := 'SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodPuri_from;
        v_sql_mago_prodPuri_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_prodPuri_from;
                
        v_sql_aui_prodAuto_from VARCHAR2(2000) := ' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN (''AP'') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodAuto_N VARCHAR2(2000)  :='SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodAuto_from;
        v_sql_aui_prodAuto_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_prodAuto_from;

        v_sql_mago_prodAuto_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO=''CMT'' '||
                ' AND COD_TIPO_CLIENTE = ''B''';
        v_sql_mago_prodAuto_N VARCHAR2(2000) := 'SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodAuto_from;
        v_sql_mago_prodAuto_PI VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_prodAuto_from;

        v_sql_aui_ConsTot_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN ''TRM'' ELSE ''CMT'' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN (''AP'',''PP'',''PD'') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN ) ';
        v_sql_aui_ConsTot_N  VARCHAR2(2000) := 'SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_aui_ConsTot_from;
        v_sql_aui_ConsTot_PI  VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_ConsTot_from;
        
        v_sql_mago_ConsTot_from  VARCHAR2(2000) := 
                ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = ''TRM'') '||
                ' OR (COD_TIPO_ELEMENTO=''CMT'' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'')) ';
        v_sql_mago_ConsTot_N  VARCHAR2(2000) := 'SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_ConsTot_from;
        v_sql_mago_ConsTot_PI  VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI  '
                || v_sql_mago_ConsTot_from;

        v_sql_aui_GeneTot_from  VARCHAR2(2000) := ' FROM ( select gest_prod COD_GEST_ELEMENTO,'
                ||'  COD_TIPO_CLIENTE, COD_TIPO_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'                C.TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN ''TRM'' ELSE ''CMT'' END COD_TIPO_ELEMENTO,C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.COD_TIPO_ELEMENTO,A.COD_TIPO_CLIENTE ) '
                ||' WHERE POTENZA_INSTALLATA>0 ';
        v_sql_aui_GeneTot_N  VARCHAR2(2000) :=  'SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_aui_GeneTot_from;
        v_sql_aui_GeneTot_PI  VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI  '
                || v_sql_aui_GeneTot_from;

        v_sql_mago_GeneTot_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = ''TRM'') '||
                ' OR (COD_TIPO_ELEMENTO=''CMT'' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'')) ';
        v_sql_mago_GeneTot_N  VARCHAR2(2000) :=  'SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_GeneTot_from;
        v_sql_mago_GeneTot_PI  VARCHAR2(2000) := 'SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI  '
                || v_sql_mago_GeneTot_from;

        v_dummyN            NUMBER;
        
        v_totN_auiCons      NUMBER := 0;
        v_totN_magoCons     NUMBER := 0;
        v_totPI_auiCons     NUMBER := 0;
        v_totPI_magoCons    NUMBER := 0;
        v_totN_auiGene      NUMBER := 0;
        v_totN_magoGene     NUMBER := 0;
        v_totPI_auiGene     NUMBER := 0;
        v_totPI_magoGene    NUMBER := 0;
      
        v_dataAUI VARCHAR2(100);
        v_dataMago VARCHAR2(100);

/*----------------------------------------------------------------------------*/
begin

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvDetail',
                               pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvDetail - Start';
    printa(vStrLog);


    -- date ultimi aggiornamenti
    select f_Date2DettStr(max(data_import))
    into v_dataAUI
    from STORICO_IMPORT
    where TIPO = 'SN' AND ORIGINE = 'AUI';

   select f_Date2DettStr(max(data_import))
    into v_dataMago
    from STORICO_IMPORT
    where TIPO = 'SN' AND ORIGINE = 'CORELE';

    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.aui.ultimoAggiornamento',
                                               v_dataAUI,
                                               NULL);
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.mago.ultimoAggiornamento',
                                               v_dataMago,
                                               NULL);

    -- consistenza rete AUI
        v_totN_auiCons       := 0;
        v_totPI_auiCons      := 0;

    printa('consistenzaRete.aui.trMTBT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.trMTBT_N',
                                        v_sql_aui_consTrMTBT_N);
    printa('consistenzaRete.aui.trMTBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.trMTBT_PI',
                                        v_sql_aui_consTrMTBT_PI);

    printa('consistenzaRete.aui.clientiMT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiMT_N',
                                        v_sql_aui_consCliMT_N);
    printa('consistenzaRete.aui.clientiMT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiMT_PI',
                                        v_sql_aui_consCliMT_PI);
    -- clienti BT non reperibili
    printa('consistenzaRete.aui.clientiBT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiBT_N',
                                        v_sql_aui_cliBT_N);
    printa('consistenzaRete.aui.clientiBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiBT_PI',
                                        v_sql_aui_cliBT_PI);

    -- totali cons AUI
    printa('consistenzaRete.mago.totale_N');
    v_dummyN := v_dummyN + f_SetDettaglioLob('consistenzaRete.aui.totale_N',
                                        v_sql_aui_ConsTot_N,
                                        v_totN_auiCons);

    v_dummyN := v_dummyN + f_SetDettaglioNum('consistenzaRete.aui.totale_PI',
                                        v_sql_aui_ConsTot_PI,
                                        v_totPI_auiCons);


    -- consistenza rete Mago
        v_totN_magoCons      := 0;
        v_totPI_magoCons     := 0;

    printa('consistenzaRete.mago.trMTBT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.trMTBT_N',
                                        v_sql_mago_TR_N);
    printa('consistenzaRete.mago.trMTBT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.trMTBT_PI',
                                        v_sql_mago_TR_PI);

    printa('consistenzaRete.mago.clientiMT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiMT_N',
                                        v_sql_mago_cliMT_N);
    printa('consistenzaRete.mago.clientiMT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiMT_PI',
                                        v_sql_mago_cliMT_PI);

    -- clienti BT non reperibili
    printa('consistenzaRete.mago.clientiBT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiBT_N',
                                        v_sql_mago_cliBT_N);
    printa('consistenzaRete.mago.clientiBT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiBT_PI',
                                        v_sql_mago_cliBT_PI);

    -- totali cons Mago
    printa('consistenzaRete.mago.totale_N');
    v_dummyN := v_dummyN + f_SetDettaglioLob('consistenzaRete.mago.totale_N',
                                        v_sql_aui_ConsTot_N,
                                        v_totN_magoCons);

    v_dummyN := v_dummyN + f_SetDettaglioNum('consistenzaRete.mago.totale_PI',
                                        v_sql_aui_ConsTot_PI,
                                        v_totPI_magoCons);



    --  generazione distribuitaAUI
        v_totN_auiGene       := 0;
        v_totPI_auiGene      := 0;

    printa('genDistribuita.aui.trMTBT_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.trMTBT_N',
                                        v_sql_aui_TRprod_N);
    printa('genDistribuita.aui.trMTBT_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.trMTBT_PI',
                                        v_sql_aui_TRprod_PI);

    printa('genDistribuita.aui.prodMTPuri_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTPuri_N',
                                        v_sql_aui_prodPuri_N);
    printa('genDistribuita.aui.prodMTPuri_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTPuri_PI',
                                        v_sql_aui_prodPuri_PI);

    printa('genDistribuita.aui.prodMTAutoProduttori_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTAutoProduttori_N',
                                        v_sql_aui_prodAuto_N);
    printa('genDistribuita.aui.prodMTAutoProduttori_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTAutoProduttori_PI',
                                        v_sql_aui_prodAuto_PI);

    -- totali gene dist AUI
    printa('genDistribuita.aui.totale_N');
    v_dummyN := v_dummyN + f_SetDettaglioLob('genDistribuita.aui.totale_N',
                                        v_sql_aui_GeneTot_N,
                                        v_totN_auiGene);

    v_dummyN := v_dummyN + f_SetDettaglioNum('genDistribuita.aui.totale_PI',
                                        v_sql_aui_GeneTot_PI,
                                        v_totPI_auiGene);

                                   

    -- generazione distribuita Mago
        v_totN_magoGene      := 0;
        v_totPI_magoGene     := 0;

    printa('genDistribuita.mago.trMTBT_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.trMTBT_N',
                                        v_sql_mago_TRprod_N);
    printa('genDistribuita.mago.trMTBT_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.trMTBT_PI',
                                        v_sql_mago_TRprod_PI);

    printa('genDistribuita.mago.prodMTPuri_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTPuri_N',
                                        v_sql_mago_prodPuri_N);
    printa('genDistribuita.mago.prodMTPuri_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTPuri_PI',
                                        v_sql_mago_prodPuri_PI);

    printa('genDistribuita.mago.prodMTAutoproduttori_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTAutoproduttori_N',
                                        v_sql_mago_prodAuto_N);
    printa('genDistribuita.mago.prodMTPAutoproduttori_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTPAutoproduttori_PI',
                                        v_sql_mago_prodAuto_PI);



    -- totali gene dist Mago
    printa('genDistribuita.mago.totale_N');
    v_dummyN := v_dummyN + f_SetDettaglioNum('genDistribuita.mago.totale_N',
                                        v_sql_mago_GeneTot_N,
                                        v_totN_magoGene);
                                        
    v_dummyN := v_dummyN + f_SetDettaglioNum('genDistribuita.mago.totale_PI',
                                        v_sql_mago_GeneTot_PI,
                                        v_totPI_magoGene);





    vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvDetail - End';
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    printa(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

EXCEPTION
WHEN OTHERS THEN
    printa('PKG_SPV_ANAG_AUI.auiSpvDetail error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end auiSpvDetail;

/*============================================================================*/

procedure auiSpvMain(	 pDoDeleteOldSpvInfo in number default 0
							,pDoDeleteSpvAlarm in number default 1
							,pDoDeleteTodaySpvInfo in number default 1
							,pDoCollector in number default 1
							,pDoProcessor in number default 1
							,pDoDetail in number default 1
							)
	is
    vIsSupervisionEnabled varchar2(100);
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(1000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvMain',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Start';
		printa(vStrLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.supervision_enabled');
    	vStrLog :=  'PKG_SPV_ANAG_AUI --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
			'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
			'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;
     printa(vStrLog);

    if vIsSupervisionEnabled = 'true' or vIsSupervisionEnabled = 'TRUE' then
        -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
      if pDoDeleteTodaySpvInfo = 1 then
          vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - delete Today spv_collector_info ';
           printa(vStrLog);
        delete from spv_collector_info
        where collector_name = g_CollectorName
        and trunc(data_inserimento) = trunc(sysdate)
        ;
      end if;

      -- cancello TUTTE le info generate dal collector ANAGRAFICA_AUI prima di gc-48h
      if pDoDeleteOldSpvInfo = 1 then
          vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - delete Old spv_collector_info ';
          printa(vStrLog);
        delete from spv_collector_info
        where collector_name = g_CollectorName
        and data_inserimento < trunc(sysdate)
        and id_rel_sistema_dizionario in (
          select id_rel_sistema_dizionario
          from spv_rel_sistema_dizionario
          where id_sistema = g_IdSistema
        );
      end if;

      -- eseguo il collector e creo le spv_collector_info
      if pDoCollector = 1 then
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvCollector ';
        printa(vStrLog);
        auiSpvCollector();
      end if;

      -- cancello TUTTI gli allarmi generati dal collector ANAGRAFICA_AUI
      if pDoDeleteSpvAlarm = 1 then
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain -  delete from spv_allarmi ';
         printa(vStrLog);
        delete from spv_allarmi where id_rel_sistema_dizionario in (select id_rel_sistema_dizionario from spv_rel_sistema_dizionario where id_sistema = g_IdSistema);
      end if;

      -- eseguo il processor e creo le spv_allarmi
      if pDoProcessor = 1 then
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvProcessor ';
        printa(vStrLog);

        auiSpvProcessor();
      end if;

      -- valorizzo il pannello di dettaglio
      if pDoDetail = 1 then
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvDetail ';
        printa(vStrLog);

        auiSpvDetail();
      end if;
    else
      vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Supervision AUI is NOT enabled';
      printa(vStrLog);
    end if;



		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_ANAG_AUI.auiSpvMain error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end auiSpvMain;

/*============================================================================*/

END PKG_SPV_ANAG_AUI;
/
	