SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.2 rev 12
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2



PROMPT _______________________________________________________________________________________
PROMPT Triggers
PROMPT
@./Triggers/AFT_IUR_ELEMENTI_DEF.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT

@./PackageBodies/PKG_SUPERVISIONE.sql




disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.2 rev 12
PROMPT =======================================================================================

SPOOL OFF
