#!/usr/bin/ksh
#
#       scarica_gmepr_MAGO.ksh        (Siemens Italia SpA)
#
#       Data:           Giugno 2012 
#       Autore          E. Castrignano
#
#
#       Modifiche:
#            - Ottobre 2012: gestione scarico da siti esercizio. 
#            - Settembre 2017, unificazione script CO e CP.
# 
#       Descrizione:
#            Script che scarica dal server di scambio
#            i files GMEPR per l'utilizzo nell'applicativo MAGO
#
#       Parametri:
#            - Esistenza di un file che indica se i dati dell'esercizio di
#              competenza sono utilizzati anche come test per il centro prove.
#            - Esistenza di un file che indica se si e' al centro prove.
#
#       Modalita' di attivazione:
#            Da crontab
#
#----------------------------------------------------------------------------------
script_name=$(basename $0)
script_name_less_ext=$(basename $0 .ksh)

#impostazioni oracle
#variabile per db ARC
dbnum=2
if [[ -z "$ORACLE_HOME" ]] || [[ -z "$ORACLE_SID" ]]; then
  echo "Variabili di ambiente Oracle non configurate"
  #export ORACLE_HOME=/oracle_bases/ARCDB$dbnum
  #export ORACLE_SID=ARCDB$dbnum
  if [[  -d /oracle_bases/ARCDB$dbnum/oradata ]]
  then
  # SAR cluster
    export ORACLE_HOME=/oracle_bases/ARCDB$dbnum
    export ORACLE_SID=ARCDB$dbnum
  else
  # SAR std
    export ORACLE_HOME=/orakit/app/oracle/product/10.2.0/db_1
    export ORACLE_SID=ARCDB1
  fi
  echo "Variabili di ambiente Oracle ora configurate :"
  echo "ORACLE_HOME = $ORACLE_HOME"
  echo "ORACLE_SID  = $ORACLE_SID"
else
  echo "Variabili di ambiente Oracle gia' configurate :"
  echo "ORACLE_HOME = $ORACLE_HOME"
  echo "ORACLE_SID  = $ORACLE_SID"
fi

##
# Definizione delle directory di lavoro 
##
TMPDIR="/usr/NEW/magosys/tmp"
UTLDIR="/usr/NEW/magosys/utl"
CORDIR="/usr/NEW/magosys/corrupted/gme/MT"
BINDIR="/usr/NEW/magosys/bin"
LOGDIR="/usr/NEW/magosys/log"
GMEMAGODIR="/usr/NEW/magosys/fromGME/MT"
PCAUIMAGODIR="/MAGO"
##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"

##
# Definizione dei comandi di sistema utilizzati nello script 
##
AUI_HOST="ftpauitel.enel.com"
SFTP_PORT=11022
FTP_PORT=21
#AUTH="sar_admin/sar_admin_dba"
AUTH="mago/mago"
CAT="${USRDIR}/cat"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
GREP="${USRDIR}/grep"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
UNZIP="${USRDIR2}/unzip"
WC="${USRDIR2}/wc"
SFTP="${USRDIR2}/sftp"
CUT="${USRDIR2}/cut"
PS="${USRDIR}/ps"


##
# Definizione delle variabili di date-time 
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}

##
# Definizione nomi files
##
MAGOLOG="download_GMEPR_${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp.log"
ESECOMP_X_CP="${UTLDIR}/esecompTest.txt"
ESECP="${UTLDIR}/eseCP.txt"
SFTP_USR_PATH_FILE="${UTLDIR}/sftp_usr_path.csv"
SFTP_CERT_USR=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f1)
CERTIFICATO=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f2)

##
# Definizione variabili varie
##
MAX_TRY=3

#
# Scrittura su file di log
#
function SendLog
{
     dataora=$(date +'%Y.%m.%d %H:%M:%S')
     $ECHO "${dataora} - ${1}" >> ${LOGDIR}/${MAGOLOG}
     return 0
}

# Controllo per non far partire esecuzioni multiple.
esito=$($PS x -o pid,ppid,args | $GREP -vw $$ | $GREP -i "${script_name}" | $GREP -c "\/usr\/bin\/ksh")
if (( $esito > 0 )); then
     exit 0
fi

function sftp_cert
{
     L_SFTP_FILE=$1
     L_SFTP_CERT_USR=$2
     L_SFTP_HOST=$3
     L_SFTP_LOG=$4

     # Aggiungo questa istruzione in comune a tutte le trasmissioni.
     ${ECHO} "exit 100" >> $L_SFTP_FILE

     ${SFTP} -oPort=$SFTP_PORT -oIdentityFile="${CERTIFICATO}" -oKexAlgorithms=diffie-hellman-group14-sha1 -oUserKnownHostsFile=/dev/null -oStrictHostKeyChecking=no  -b $L_SFTP_FILE ${L_SFTP_CERT_USR}@${L_SFTP_HOST} 1>> $L_SFTP_LOG 2>&1

     # Si ricerca "exit 100" perche' faccio terminare i batch sftp con questa stringa.
     # Se sftp non termina correttamente uno dei comandi, la modalita' batchmode fa
     # abortire la sessione, cosi non troverei la stringa cercata.
     res=$(grep "exit 100" $L_SFTP_LOG|wc -l)
     #echo "res: $res "
     if (( $res == 1 )); then
          # sftp ok
          v_check=0
     else
          # sftp ko
          v_check=1
     fi

     return $v_check
}

function gest_file_integrity_GMEPR
{
     EXIST_FILE=$(${LS} -1a ${TMPDIR}/*_GMEPR_*.zip | ${WC} -l)
     if (( $EXIST_FILE != 0 )); then
          cd $TMPDIR
          ${LS} *_GMEPR_*.zip > ${TMPDIR}/test_integrity_gmepr.txt
          for filename in $(${CAT} ${TMPDIR}/test_integrity_gmepr.txt)
          do
               ${UNZIP} -t $filename > /dev/null
               case "${?}" in
               0 )           SendLog "FILE $filename INTEGRO (TRASMISSIONE COMPLETATA) [OK] al ${DATA}"
                               ;;
               1 )           SendLog "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [ERROR] al ${DATA}"
                             ${MV} $filename $CORDIR
                               ;;
               2 )           SendLog "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [WARNING] al ${DATA}"
                             ${MV} $filename $CORDIR
                               ;;
               * )           SendLog "FILE $filename NON INTEGRO (gestione di default del case) [ERROR] al ${DATA}"
                             ${MV} $filename $CORDIR
                               ;;
               esac
          done
          ${RM} -f ${TMPDIR}/test_integrity_gmepr.txt
     fi
     return 0
}

function send_file_to_cp
{
     s_attempt=1
     s_check=0

     cd ${TMPDIR}

     while (( $s_check == 0 && $s_attempt <= $MAX_TRY ))
     do
          SendLog "Inizio spedizione file(s) GMEPR ad utilizzo centro prove tentativo $s_attempt di ${MAX_TRY}"

          ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
          retping=$(${ECHO} ${?})

          if [[ "${retping}" = "0" ]]; then

               SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"

               SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
               ${ECHO} "cd ${PCAUIMAGODIR}\r" > $SFTP_FILE_PAR
               ${ECHO} "put ${ESECOMP}_GMEPR_*.zip\r" >> $SFTP_FILE_PAR

               sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then

                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GMEPR FALLITA"

                    if (( $s_attempt < 3 )); then 
                         n_random=$((${RANDOM}%15+1))
                         SendLog "sleep di $n_random min prima del prossimo tentativo..."
                         sleep ${n_random}m
                    fi    

                    s_attempt=$(($s_attempt+1))

               else

                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GMEPR COMPLETATA"

                    s_check=1
               fi

          else

               SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"

               if (( $s_attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    SendLog "sleep di $n_random min prima del prossimo tentativo..."
                    sleep ${n_random}m
               fi    

               s_attempt=$(($s_attempt+1))


          fi

     done

     if (( $s_attempt > $MAX_TRY )); then
       return 1
     fi

     ${RM} -f $SFTP_FILE_PAR
     SendLog "Fine spedizione file(s) GMEPR ad utilizzo centro prove"
     return 0
}

##
# Processo di attivazione FTP per ricezione file GMEPR
##
function ricevi_files_GMEPR
{ 
     attempt=1
     check=0

     cd ${TMPDIR}

     if [[ -a "${ESECP}" ]]; then
          WHEREAREGMEPR="${PCAUIMAGODIR}"
     else
          WHEREAREGMEPR="STM_${ESECOMP}/GME"
     fi

     while (( $check == 0 && $attempt <= $MAX_TRY ))
     do
          SendLog "Inizio fase ricezione file(s) GMEPR tentativo $attempt di ${MAX_TRY}"

          ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
          retping=$(${ECHO} ${?})

          if [[ "${retping}" = "0" ]]; then

               SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"

               SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
               ${ECHO} "cd ${WHEREAREGMEPR}\r" > $SFTP_FILE_PAR
               ${ECHO} "get ${ESECOMP}_GMEPR_*.zip\r" >> $SFTP_FILE_PAR

               sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then

                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GMEPR FALLITA"
            
                    if (( $attempt < 3 )); then 

                         n_random=$((${RANDOM}%15+1))
                         SendLog "sleep di $n_random min prima del prossimo tentativo..."
                         sleep ${n_random}m
                    fi    

                    attempt=$(($attempt+1))

               else

                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GMEPR COMPLETATA"

                    gest_file_integrity_GMEPR || return 1

                    if [[ -a "${ESECOMP_X_CP}" ]]; then

                         send_file_to_cp || SendLog "_____ATTENZIONE !!! ERRORE SPEDIZIONE FILES AL CP !!!_____"
                    fi

                    SendLog "Riposizionamento file(s) GMEPR nella cartella di lavoro ${GMEMAGODIR}."

                    ${MV} *_GMEPR_*.zip $GMEMAGODIR

                    SendLog "Cancellazione file(s) da pc_aui."

                    SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
                    ${ECHO} "cd ${WHEREAREGMEPR}\r" > $SFTP_FILE_PAR
                    ${ECHO} "rm ${ESECOMP}_GMEPR_*.zip\r" >> $SFTP_FILE_PAR

                    sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}

                   check=1
               fi

          else

               SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"
            
               if (( $attempt < 3 )); then 

                    n_random=$((${RANDOM}%15+1))
                    SendLog "sleep di $n_random min prima del prossimo tentativo..."
                    sleep ${n_random}m
               fi    

               attempt=$(($attempt+1))


          fi
     done

     if (( $attempt > $MAX_TRY )); then
       return 1
     fi

     ${RM} -f $SFTP_FILE_PAR
     SendLog "Fine fase ricezione file(s) GMEPR"

     return 0
}

function main
{

     SendLog "__________________________________________________________________________________________________________________________"
     SendLog "Controllo esistenza e ricezione file(s) GMEPR per MAGO ${GIORNO}/${MESE}/${ANNO} ${H}:${M}:${S}\n"


     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 50;
whenever oserror exit 120;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select pkg_mago.f_GetDefCO_ENEL as sg_ese_base from dual;
exit 140
!)

retcode=${?}

     if [[ "${retcode}" != "140" ]]; then

          SendLog "Errore recupero dati!"
          return 1
     fi

     export ESECOMP

     ricevi_files_GMEPR || return 1

     return 0
}

main || exit 1

exit 0
