PROMPT Package PKG_TRANSFER_RES

CREATE OR REPLACE PACKAGE PKG_TRANSFER_RES AS 

/********************************************************************************************************************
    2.2.9     18/09/2017     Zaninii        Modificata procedura sp_transfer_mis_R per recupero dati anche 1/4 ora
 ********************************************************************************************************************/


 PROCEDURE  sp_transfer_mis_R;
 PROCEDURE  sp_transfer_mis;
 procedure SP_STORICO_REGISTRO_INVIO;

 PROCEDURE GetElementForRES   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1);

 PROCEDURE GetTrasformatoriforRES     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pDisconnect IN NUMBER DEFAULT 0);



END PKG_TRANSFER_RES;
/
