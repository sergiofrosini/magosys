SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.2 rev 13
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2

----------------------------
PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT
@./Types/T_MISMETEO_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ANAGRAFICA_IMPIANTO.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./InitTables/SPV_DIZIONARIO_ALLARMI.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MISURE.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_PROFILI.sql
@./PackageBodies/PKG_SUPERVISIONE.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_SPV_SMILE_GME.sql
@./PackageBodies/PKG_MISURE.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.2 rev 13
PROMPT =======================================================================================

SPOOL OFF
