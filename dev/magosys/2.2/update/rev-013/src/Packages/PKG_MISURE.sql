PROMPT PACKAGE BODY: PKG_MISURE

create or replace PACKAGE PKG_MISURE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.1 rev 009
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/




/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_Misura      IS RECORD (COD_TIPO_MISURA      TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                               DATA                 DATE,
                               VALORE               NUMBER,
                               COD_TIPO_FONTE       RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE);

 TYPE t_Misure      IS TABLE OF t_Misura;

 gcModalitaKPI      CONSTANT NUMBER(1) := 3;        -- Modalita' KPI (usato per TIPO_AGGREGAZIONE in TRATTAMENTO_ELEMENTI

 cNoConversion      CONSTANT INTEGER := 0;
 cPotenzaToEnergia  CONSTANT INTEGER := 1;

 cUnSecondo         CONSTANT NUMBER  := (1 / 1440) / 60;

 cMisDettTot        CONSTANT NUMBER(1) := 1;
 cMisTot            CONSTANT NUMBER(1) := 0;
 cMisDett           CONSTANT NUMBER(1) := -1;

 gListaTipoMisura            VARCHAR2(200) := NULL;


 gTotTipFon  NUMBER(1)     := 0;  -- MAGO-733 bug fix - Aggregazione PI
-- ----------------------------------------------------------------------------------------------------------
-- codici Log
-- ----------------------------------------------------------------------------------------------------------

 gkInfo           CONSTANT NUMBER := 001; -- informazione
 gkWarning        CONSTANT NUMBER := 002; -- informazione
 gkAnagrNonPres   CONSTANT NUMBER := 101; -- non definito in anagrafica
 gkAnagrIncompl   CONSTANT NUMBER := 102; -- anagrafica incompleta
 gkAnagrIncong    CONSTANT NUMBER := 103; -- anagrafica incongruente

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetTrattamentoElemento(pCodEle      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipEle      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                 pTipMis      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pTipFon      IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                 pTipRet      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 pTipCli      IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                 pOrganizzaz  IN NUMBER,
                                 pTipoAggreg  IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE,pTemporalAggreg IN NUMBER) RETURN DATE;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetProduttoriGME     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodCo            IN VARCHAR2,
                                 pTipiFonte        IN VARCHAR2,
                                 pDate             IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GMEcompleted         (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pFinishTimestamp  IN DATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGme         (pMisure           IN T_MISURA_GME_ARRAY,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisure            (pMisure           IN T_MISURA_GME_ARRAY);
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE CalcMisureStatiche   (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE);

 PROCEDURE AddMisurePI          (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE,
                                 pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard);

-- PROCEDURE AddMisurePC          (pData             IN DATE DEFAULT SYSDATE,
--                                 pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard);

 PROCEDURE AddMisureNRI         (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE,
                                 pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureMeteo       (pMisureMeteo      IN T_MISMETEO_ARRAY,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE);



-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoFonti        (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;

 FUNCTION GetIdTipoReti         (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;

 FUNCTION GetIdTipoClienti      (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMisuraStatica       (pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE) RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisure            (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 15,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pdeltaT           IN INTEGER DEFAULT 7);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureCG          (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pGestElem         IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 15,
                                 pDisconnect       IN INTEGER DEFAULT 0);

-- FUNCTION  GetMisureTab         (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
--                                 pDataDa           IN DATE,
--                                 pDataA            IN DATE,
--                                 pOrganizzazione   IN INTEGER,
--                                 pStatoRete        IN INTEGER,
--                                 pGerarchiaECS     IN TIPI_ELEMENTO.GER_ECS%TYPE,
--                                 pTotTipFon        IN INTEGER,
--                                 pIntervallo       IN INTEGER,
--                                 pDiffDate         IN NUMBER,
--                                 pRaggrMis         IN CHAR) RETURN t_Misure PIPELINED;

 FUNCTION  GetMisureTabMisStat  (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAgrTemporale     IN INTEGER,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTable            IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pTipoAggreg1      IN INTEGER,
                                 pTipoAggreg2      IN INTEGER,
                                 pGerECS           IN NUMBER,
                                 pTotTipFon        IN NUMBER,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pOnlyDiff         IN NUMBER DEFAULT 1) RETURN t_Misure PIPELINED;

 FUNCTION  GetMisureTabMis      (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTotTipFon        IN INTEGER,
                                 pAgrTemporale     IN INTEGER,
                                 pDataAlVolo       IN DATE,
                                 pForceEmpyRow     IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOff,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pOnlyDiff         IN NUMBER DEFAULT 1) RETURN t_Misure PIPELINED;



PROCEDURE GetMisureMultiElem   (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                pCodElem          IN VARCHAR2,
                                pDataDa           IN DATE,
                                pDataA            IN DATE,
                                pTipiMisura       IN VARCHAR2,
                                pOrganizzazione   IN INTEGER,
                                pStatoRete        IN INTEGER,
                                pTipologiaRete    IN VARCHAR2,
                                pFonte            IN VARCHAR2,
                                pTipoClie         IN VARCHAR2,
                                pAgrTemporale     IN INTEGER DEFAULT 15,
                                pDisconnect       IN INTEGER DEFAULT 0,
                                pdeltaT           IN INTEGER DEFAULT 7);

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------
-- Implementazioni versione 1.6a
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureOfflineSessionID  (pSessionID     OUT NUMBER);

-- ----------------------------------------------------------------------------------------------------------


 FUNCTION GetMisuraConv (pOrigineMis    IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                            pTipele        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                            pTipMisIn      IN TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_IN%TYPE,
                            pTipMisOut     IN TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE
                            ) RETURN NUMBER ; 
 
 

                            

 FUNCTION f_MisureInCollector(pColl IN VARCHAR2, pMis In VARCHAR2) RETURN NUMBER;


                            
END PKG_MISURE;
/