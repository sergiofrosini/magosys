PROMPT SEQUENCE: SPV_COLLECTOR_INFO_SEQ


BEGIN

EXECUTE IMMEDIATE 'DROP SEQUENCE SPV_COLLECTOR_INFO_SEQ';
    
EXCEPTION 
 WHEN OTHERS THEN 
  NULL;
END;
/


CREATE SEQUENCE SPV_COLLECTOR_INFO_SEQ
  START WITH 1
  MAXVALUE 999999999999999999999999999
  MINVALUE 0
  NOCYCLE
  NOCACHE
  NOORDER
  /