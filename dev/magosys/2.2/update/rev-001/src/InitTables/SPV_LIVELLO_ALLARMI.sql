PROMPT TABELLA: SPV_LIVELLO_ALLARMI

MERGE INTO SPV_LIVELLO_ALLARMI A USING
 (SELECT
  2 as ID_LIVELLO,
  'MEDIO' as DESCRIZIONE,
  'Rilevate anomalie che richiedono una discreta attenzione ma non necessariamente richiedono un intervento' as NOTA
  FROM DUAL) B
ON (A.ID_LIVELLO = B.ID_LIVELLO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_LIVELLO, DESCRIZIONE, NOTA)
VALUES (
  B.ID_LIVELLO, B.DESCRIZIONE, B.NOTA)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE,
  A.NOTA = B.NOTA;

MERGE INTO SPV_LIVELLO_ALLARMI A USING
 (SELECT
  1 as ID_LIVELLO,
  'GRAVE' as DESCRIZIONE,
  'Rilevate delle anomalie bloccanti che richiedono un intervento urgente per chiarire e risolvere la situazione' as NOTA
  FROM DUAL) B
ON (A.ID_LIVELLO = B.ID_LIVELLO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_LIVELLO, DESCRIZIONE, NOTA)
VALUES (
  B.ID_LIVELLO, B.DESCRIZIONE, B.NOTA)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE,
  A.NOTA = B.NOTA;

MERGE INTO SPV_LIVELLO_ALLARMI A USING
 (SELECT
  3 as ID_LIVELLO,
  'NESSUNA' as DESCRIZIONE,
  'Non ci sono anomalie rilevate, tutto procede in modo regolare in funzione dei criteri di allarme definiti' as NOTA
  FROM DUAL) B
ON (A.ID_LIVELLO = B.ID_LIVELLO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_LIVELLO, DESCRIZIONE, NOTA)
VALUES (
  B.ID_LIVELLO, B.DESCRIZIONE, B.NOTA)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE,
  A.NOTA = B.NOTA;

COMMIT;
