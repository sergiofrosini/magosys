SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.2.1 SRC
PROMPT file :  build_Mago_2_2_1.sql
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 



PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE RUOLI <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/RUOLI.sql



PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2



PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/SPV_CATEGORIA.sql
@./Tables/SPV_SISTEMA.sql
@./Tables/SPV_SOGLIE.sql
@./Tables/SPV_DETTAGLIO_PARAMETRI.sql
@./Tables/SPV_DIZIONARIO_ALLARMI.sql
@./Tables/SPV_LIVELLO_ALLARMI.sql
@./Tables/SPV_MACRO.sql
@./Tables/SPV_REL_CATEGORIA_SISTEMA.sql
@./Tables/SPV_REL_MACRO_CATEGORIA.sql
@./Tables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./Tables/SPV_REL_SOGLIE_DIZIONARIO.sql
@./Tables/SPV_ALLARMI.sql
@./Tables/SPV_COLLECTOR_INFO.sql
@./Tables/SPV_COLLECTOR_MEASURE.sql

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT
@./Sequences/SPV_ALLARMI_SEQ.sql
@./Sequences/SPV_COLLECTOR_INFO_SEQ.sql
@./Sequences/SPV_COLLECTOR_MEASURE_SEQ.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/SPV_CATEGORIA.sql
@./InitTables/SPV_SISTEMA.sql
@./InitTables/SPV_DIZIONARIO_ALLARMI.sql
@./InitTables/SPV_LIVELLO_ALLARMI.sql
@./InitTables/SPV_MACRO.sql
@./InitTables/SPV_REL_CATEGORIA_SISTEMA.sql
@./InitTables/SPV_REL_MACRO_CATEGORIA.sql
@./InitTables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./InitTables/SPV_SOGLIE.sql
@./InitTables/SPV_REL_SOGLIE_DIZIONARIO.sql
@./InitTables/SPV_DETTAGLIO_PARAMETRI.sql

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT
@./Types/T_SPVSOGLIA.sql
@./Types/T_SPVCOLLECTORINFO.sql
@./Types/T_SPVCOLLECTORMEASURE.sql
@./Types/T_ROWGERARCHIA.sql
@./Types/T_SPVELEMENT.sql


PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_SUPEVISIONE.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_SPV_ANAG_AUI.sql
@./Packages/PKG_SPV_ELEMENT.sql
@./Packages/PKG_SPV_SMILE_GME.sql
@./Packages/PKG_STATS.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_METEO.sql


PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_SUPEVISIONE.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_SPV_ELEMENT.sql
@./PackageBodies/PKG_SPV_SMILE_GME.sql
@./PackageBodies/PKG_STATS.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_METEO.sql



PROMPT _______________________________________________________________________________________
PROMPT JOB
PROMPT
@./Job/MAGO_SPV_LOAD_MEASURE_SMILE.sql
@./Job/MAGO_SPV_LOAD_AUI.sql

COMMIT;


PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.2.1 SRC
PROMPT

spool off
