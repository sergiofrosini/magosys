PROMPT TYPE: T_SPVCOLLECTORMEASURE

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVCOLLECTORMEASURE_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVCOLLECTORMEASURE_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/


create or replace TYPE "T_SPVCOLLECTORMEASURE_OBJ" AS OBJECT(  
 ID_MEASURE        			NUMBER       
,COLLECTOR_NAME             VARCHAR2(20) 
,DATA_INSERIMENTO           DATE         
,DATA_MISURA                DATE         
,COD_GEST_ELEMENTO          VARCHAR2(20) 
,COD_TIPO_MISURA            VARCHAR2(6)  
,COD_TIPO_FONTE             VARCHAR2(20) 
,VALORE                     NUMBER(20,4) 
);
/
create or replace TYPE "T_SPVCOLLECTORMEASURE_ARRAY" IS TABLE OF T_SPVCOLLECTORMEASURE_OBJ;
/