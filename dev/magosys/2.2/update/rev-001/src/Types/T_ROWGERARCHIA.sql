PROMPT TYPE: T_ROWGERARCHIA

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE t_TabGerarchia';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto t_TabGerarchia non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE t_RowGerarchia';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto t_RowGerarchia non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

create or replace TYPE  t_RowGerarchia        IS object (COD_ELEMENTO         NUMBER,
                                       COD_GEST_ELEMENTO    VARCHAR2(20),
                                      COD_TIPO_ELEMENTO    VARCHAR2(3),
                                       LIVELLO              INTEGER,
                                       SEQUENZA             INTEGER);
/

create or replace TYPE t_TabGerarchia        IS TABLE OF t_RowGerarchia;
/

