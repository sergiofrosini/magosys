PROMPT TYPE: T_SPVCOLLECTORINFO 

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVCOLLECTORINFO_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVCOLLECTORINFO_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

create or replace TYPE "T_SPVCOLLECTORINFO_OBJ" AS OBJECT(  
 ID_INFO                   			NUMBER         
,COLLECTOR_NAME                     VARCHAR2(20)   
,DATA_INSERIMENTO                   DATE           
,ID_SISTEMA          				NUMBER         
,ID_DIZIONARIO_ALLARME          	NUMBER         
,SUPPLIER_NAME                      VARCHAR2(20)   
,COD_GEST_ELEMENTO                  VARCHAR2(20)   
,COD_TIPO_MISURA                    VARCHAR2(6)    
,COD_TIPO_FONTE                     VARCHAR2(2)    
,VALORE                             NUMBER         
,INFORMAZIONI                       VARCHAR2(1000) 
);
/
create or replace TYPE "T_SPVCOLLECTORINFO_ARRAY" IS TABLE OF T_SPVCOLLECTORINFO_OBJ; 
/