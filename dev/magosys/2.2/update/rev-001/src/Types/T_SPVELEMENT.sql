PROMPT TYPE: t_SPVElement

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE t_SPVTable';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto t_SPVTable non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE t_SPVElement';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto t_SPVElement non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

create or replace TYPE      t_SPVElement IS object (COD_GEST_ELEMENTO  VARCHAR2(20),
                              IS_SOLARE_OMOGENEO NUMBER);
/

create or replace TYPE t_SPVTable        IS TABLE OF t_SPVElement;
/

