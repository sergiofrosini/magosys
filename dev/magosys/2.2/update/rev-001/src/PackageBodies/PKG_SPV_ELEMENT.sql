PROMPT PACKAGE BODY: PKG_SPV_ELEMENT

CREATE OR REPLACE PACKAGE BODY PKG_SPV_ELEMENT AS

/* ***********************************************************************************************************
   NAME:       PKG_SPV_ELEMENT
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      10/04/2017  Frosini S.       Created this package .              -- Spec. Version 2.2.1

   NOTES:

*********************************************************************************************************** */

g_Fontesolare       VARCHAR2(1) := 'S';


g_DumDum_s          VARCHAR2(100); -- variabile generica stringa
g_DumDum_n          NUMBER; -- variabile generica numerica


-- =========================================================================================
PROCEDURE PRINTA(pTxt IN VARCHAR2 DEFAULT NULL, pLev IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF) AS
BEGIN
 PKG_Logs.TraceLog(pTxt,pLev);
END;
--===============================================================================

PROCEDURE GetProduttori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pFlagPI         IN NUMBER DEFAULT 1,
                              pDisconnect     IN NUMBER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN


   IF pDisconnect = 0 THEN
   
      OPEN pRefCurs FOR
      
            SELECT DISTINCT
                A.COD_ELEMENTO_CLIENTE,
                A.FONTE
            FROM
                (
                    SELECT
                        PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,
                                                      TIP_ELE_GEN,
                                                      pData,
                                                      PKG_Mago_utl.gcOrganizzazELE,
                                                      PKG_Mago_utl.gcStatoNormale)
                        COD_ELEMENTO_CLIENTE,
                        A.COD_ELEMENTO_GENERATORE,
                        A.COD_RAGGR_FONTE FONTE,
                        A.POTENZA_INSTALLATA,
                        A.COD_TIPO_CLIENTE,
                        A.COD_TIPO_RETE
                    FROM
                        (
                            SELECT
                                COD_ELEMENTO,
                                COD_ELEMENTO as COD_ELEMENTO_GENERATORE,
                                COD_RAGGR_FONTE,
                                COD_TIPO_FONTE,
                                POTENZA_INSTALLATA,
                                COD_TIPO_CLIENTE,
                                COD_TIPO_RETE,
                                TIP_ELE_GEN
                            FROM
                                (
                                    SELECT
                                        TR.COD_ELEMENTO,
                                        TR.COD_TIPO_FONTE,
                                        F.COD_RAGGR_FONTE,
                                        TR.COD_TIPO_CLIENTE,
                                        TR.COD_TIPO_RETE,
                                        MIS.VALORE POTENZA_INSTALLATA,
                                        CASE TR.COD_TIPO_ELEMENTO
                                            WHEN PKG_Mago_utl.gcGeneratoreMT
                                            THEN PKG_Mago_utl.gcClienteMT
                                            WHEN PKG_Mago_utl.gcGeneratoreBT
                                            THEN PKG_Mago_utl.gcClienteBT
                                            WHEN PKG_Mago_utl.gcGeneratoreAT
                                            THEN PKG_Mago_utl.gcClienteAT
                                        END TIP_ELE_GEN
                                    FROM TRATTAMENTO_ELEMENTI TR
                                    INNER JOIN MISURE_ACQUISITE_STATICHE MIS
                                    ON ( MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM )
                                    INNER JOIN ELEMENTI_DEF DEF
                                    ON ( DEF.COD_ELEMENTO = TR.COD_ELEMENTO )
                                    INNER JOIN
                                        (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,'[^\|]+', 1, level) 
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,'[^\|]+', 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F 
                                        ON ( F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE )
                                    INNER JOIN
                                        (
                                            SELECT
                                                regexp_substr(pTipologiaRete,'[^\|]+', 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, '[^\|]+', 1, level) IS NOT NULL
                                        ) RET
                                    ON ( RET.COD_TIPO_RETE=TR.COD_TIPO_RETE )
                                    INNER JOIN
                                        (
                                            SELECT
                                                regexp_substr(pTipoProd,'[^\|]+', 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,'[^\|]+', 1, level) IS NOT NULL
                                        ) PR
                                    ON ( PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE )
                                    WHERE
                                        TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                 PKG_Mago_utl.gcGeneratoreBT,
                                                                 PKG_Mago_utl.gcGeneratoreAT)
                                        AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                        AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE
                                )
                        ) A
                    LEFT OUTER JOIN ELEMENTI B
                    ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,
                                                                        PKG_Mago_utl.gcComune,pData, 
                                                                        PKG_Mago_utl.gcOrganizzazGEO,
                                                                        PKG_Mago_utl.gcStatoNormale)
                ) A
            INNER JOIN ELEMENTI_DEF E
            ON ( E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                 AND SYSDATE BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                 AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE )
            LEFT OUTER JOIN FORECAST_PARAMETRI B
            ON (B.COD_ELEMENTO       = A.COD_ELEMENTO_CLIENTE
                AND B.COD_TIPO_FONTE = A.FONTE
                AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE)
            INNER JOIN TIPI_CLIENTE TCL
            ON ( TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE )
            WHERE TCL.FORNITORE = PKG_Mago_utl.gcON 
            AND (pFlagPI <>1 OR A.POTENZA_INSTALLATA>0);
                 
   ELSE
   
      OPEN pRefCurs FOR
      
            WITH
                wgerarchia_ele AS( SELECT 
                        cod_elemento ,
                        L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25,
                        data_attivazione,
                        data_disattivazione,
                        NVL(lead(data_attivazione-1/(24*60*60)) 
                              OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione ) ,
                            TO_DATE('01013000', 'ddmmyyyy')) data_disattivazione_calc
                    FROM GERARCHIA_IMP_SN
                ),
                wgerarchia_geo AS( SELECT 
                        cod_elemento ,
                        L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25,
                        data_attivazione,
                        data_disattivazione ,
                        NVL(lead(data_attivazione-1/(24*60*60)) 
                              OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione ) ,
                            TO_DATE('01013000', 'ddmmyyyy')) data_disattivazione_calc
                        FROM GERARCHIA_GEO)
            SELECT DISTINCT A.COD_ELEMENTO_CLIENTE, A.FONTE
            FROM
                (
                    SELECT
                        A.COD_ELEMENTO_CLIENTE,
                        A.COD_ELEMENTO_GENERATORE,
                        A.COD_RAGGR_FONTE FONTE,
                        A.POTENZA_INSTALLATA,
                        COD_TIPO_CLIENTE,
                        COD_TIPO_RETE
                    FROM
                        (
                            SELECT
                                A.COD_ELEMENTO,
                                A.COD_ELEMENTO COD_ELEMENTO_GENERATORE,
                                A.COD_RAGGR_FONTE,
                                A.COD_TIPO_FONTE,
                                A.POTENZA_INSTALLATA,
                                A.COD_TIPO_CLIENTE,
                                A.COD_ELEMENTO_CLIENTE,
                                A.COD_TIPO_RETE,
                                A.TIP_ELE_GEN
                            FROM
                                (
                                    SELECT
                                        TR.COD_ELEMENTO,
                                        TR.COD_TIPO_FONTE,
                                        F.COD_RAGGR_FONTE,
                                        TR.COD_TIPO_CLIENTE,
                                        TR.COD_TIPO_RETE,
                                        MIS.VALORE POTENZA_INSTALLATA,
                                        CASE TR.COD_TIPO_ELEMENTO
                                            WHEN PKG_Mago_utl.gcGeneratoreMT
                                            THEN PKG_Mago_utl.gcClienteMT
                                            WHEN PKG_Mago_utl.gcGeneratoreBT
                                            THEN PKG_Mago_utl.gcClienteBT
                                            WHEN PKG_Mago_utl.gcGeneratoreAT
                                            THEN PKG_Mago_utl.gcClienteAT
                                        END TIP_ELE_GEN,
                                        COD_ELEMENTO_CLIENTE
                                    FROM TRATTAMENTO_ELEMENTI TR
                                    INNER JOIN
                                        (
                                            SELECT
                                                ele.cod_gest_elemento cod_gest_cliente,
                                                ele.cod_elemento cod_elemento_cliente,
                                                ele.cod_tipo_elemento,
                                                rel.cod_elemento
                                            FROM wgerarchia_ele rel
                                            INNER JOIN ELEMENTI ele
                                            ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                                            WHERE
                                                pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                AND rel.data_disattivazione != rel.data_disattivazione_calc
                                        ) CLI ON
                                        (   tr.cod_elemento           = cli.cod_elemento
                                            AND cli.cod_tipo_elemento =  CASE tr.cod_tipo_elemento
                                                                            WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                            WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                            WHEN PKG_Mago_utl.gcGeneratoreBT  THEN PKG_Mago_utl.gcClienteBT
                                                                        END
                                        )
                                    INNER JOIN
                                        (
                                            SELECT
                                                MIS.*,
                                                NVL(lead(MIS.data_attivazione-1/(24*60*60))
                                                    OVER (PARTITION BY MIS.cod_trattamento_elem
                                                          ORDER BY MIS.data_attivazione), TO_DATE('01013000','ddmmyyyy'))
                                                data_disattivazione_calc
                                            FROM MISURE_ACQUISITE_STATICHE MIS
                                        ) MIS
                                    ON ( MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM )
                                    INNER JOIN ELEMENTI_DEF DEF
                                    ON (  DEF.COD_ELEMENTO = TR.COD_ELEMENTO )
                                    INNER JOIN
                                        (
                                            SELECT
                                                COD_TIPO_FONTE,
                                                COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT regexp_substr(pFonte,'[^\|]+', 1, level) ALF1
                                                    FROM dual
                                                        CONNECT BY regexp_substr(pFonte, '[^\|]+', 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F 
                                        ON ( F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE )
                                    INNER JOIN
                                        (
                                            SELECT
                                                regexp_substr(pTipologiaRete,'[^\|]+', 1,  level) COD_TIPO_RETE
                                            FROM dual
                                                CONNECT BY regexp_substr(pTipologiaRete, '[^\|]+', 1, level) IS NOT NULL
                                        )
                                        RET
                                    ON ( RET.COD_TIPO_RETE=TR.COD_TIPO_RETE )
                                    INNER JOIN
                                        (
                                            SELECT
                                                regexp_substr(pTipoProd,'[^\|]+', 1, level)  COD_TIPO_CLIENTE
                                            FROM dual
                                                CONNECT BY regexp_substr(pTipoProd, '[^\|]+', 1, level) IS NOT NULL
                                        ) PR
                                    ON ( PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE  )
                                    WHERE
                                        TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                 PKG_Mago_utl.gcGeneratoreBT,
                                                                 PKG_Mago_utl.gcGeneratoreAT)
                                        AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                        AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                ) A
                        ) A
                    INNER JOIN
                        (
                            SELECT
                                ele.cod_gest_elemento cod_gest_comune,
                                ele.cod_elemento cod_elemento_comune,
                                rel.cod_elemento
                            FROM wgerarchia_geo rel
                            INNER JOIN ELEMENTI ele
                            ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                            WHERE
                                pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                AND cod_tipo_elemento        = PKG_Mago_utl.gcComune
                                AND rel.data_disattivazione != rel.data_disattivazione_calc
                        ) B 
                        ON ( b.cod_elemento = A.cod_elemento )
                )  A
            INNER JOIN ELEMENTI_DEF E
            ON  E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                AND SYSDATE BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE
            LEFT OUTER JOIN FORECAST_PARAMETRI B
            ON  B.COD_ELEMENTO       = A.COD_ELEMENTO_CLIENTE
                AND B.COD_TIPO_FONTE = A.FONTE
                AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE
            INNER JOIN TIPI_CLIENTE TCL
            ON ( TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE )
            WHERE  TCL.FORNITORE = PKG_Mago_utl.gcON 
            AND (pFlagPI <>1 OR A.POTENZA_INSTALLATA>0);
               
               
   END IF;
   
   
   PKG_Logs.TraceLog('Eseguito GetProduttori - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);


EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.GetProduttori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetProduttori;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneratori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pDisconnect IN NUMBER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vMagoDGF   VARCHAR2(1);
BEGIN

    vMagoDGF := CASE WHEN PKG_Mago_utl.IsMagoDGF THEN 'Y' ELSE 'N' END;

    IF pDisconnect = 0 THEN
        OPEN pRefCurs FOR
              SELECT distinct 
              FONTE, COD_GEST_CLIENTE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,
                                     PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoPadre(COD_ELEMENTO,
                                                                                                CASE TIP_ELE_GEN
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                                                                END,pdata,1,1)) COD_GEST_CLIENTE
                                FROM (SELECT     TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*,
                                                         NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND tr.cod_tipo_misura = PKG_Mago_utl.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI pgeo ON (DEF.cod_geo = pgeo.cod_geo)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI AGEO ON (DEF.cod_geo_a = ageo.cod_geo)
                                       INNER JOIN (SELECT cod_tipo_fonte, cod_raggr_fonte
                                                    FROM (select regexp_substr(pFonte,'[^\|]+', 1, level) ALF1 from dual
                                                      connect by regexp_substr(pFonte, '[^\|]+', 1, level) is not null )
                                                     INNER JOIN TIPO_FONTI ON alf1 = cod_raggr_fonte
                                                  ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (select regexp_substr(pTipologiaRete,'[^\|]+', 1, level) COD_TIPO_RETE from dual
                                                      connect by regexp_substr(pTipologiaRete, '[^\|]+', 1, level) is not null
                                                 )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
                                       INNER JOIN (select regexp_substr(pTipoProd,'[^\|]+', 1, level) COD_TIPO_CLIENTE from dual
                                                      connect by regexp_substr(pTipoProd, '[^\|]+', 1, level) is not null
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                      PKG_Mago_utl.gcGeneratoreBT,
                                                                      PKG_Mago_utl.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                        LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                                PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale)
                     ) A
                LEFT OUTER JOIN FORECAST_PARAMETRI B ON COD_ELEMENTO = COD_ELEMENTO_GENERATORE  AND B.COD_TIPO_FONTE = A.FONTE AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE ;
    ELSE
        OPEN pRefCurs FOR
        
            WITH
                wgerarchia_ele AS( SELECT 
                        cod_elemento ,
                        L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25,
                        data_attivazione,
                        data_disattivazione,
                        NVL(lead(data_attivazione-1/(24*60*60)) 
                              OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione ) ,
                            TO_DATE('01013000', 'ddmmyyyy')) data_disattivazione_calc
                    FROM GERARCHIA_IMP_SN
                ),
                wgerarchia_geo AS( SELECT 
                        cod_elemento ,
                        L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25,
                        data_attivazione,
                        data_disattivazione ,
                        NVL(lead(data_attivazione-1/(24*60*60)) 
                              OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione ) ,
                            TO_DATE('01013000', 'ddmmyyyy')) data_disattivazione_calc
                        FROM GERARCHIA_GEO)
              SELECT /*+ ORDERED USE_HASH(A,B,C)*/distinct 
              FONTE, COD_GEST_CLIENTE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,COD_GEST_CLIENTE
                                FROM (SELECT /*+ USE_HASH(TR,MIS,DEF,PGEO,AGEO,CLI,F,RET,PR,B ) */
                                        TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,cli.cod_gest_cliente,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*
                                                         ,NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND TR.cod_tipo_misura = PKG_Mago_utl.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN FORECAST_PARAMETRI B ON b.COD_ELEMENTO = tr.COD_ELEMENTO  AND tr.COD_TIPO_FONTE = b.cod_tipo_FONTE AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE
                                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_cliente, ele.cod_elemento cod_elemento_cliente,ele.cod_tipo_elemento, rel.cod_elemento
                                                     FROM wgerarchia_ele rel
                                                   INNER JOIN ELEMENTI ele 
                                                   ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                     AND rel.data_disattivazione != rel.data_disattivazione_calc
                                                  ) CLI ON (    tr.cod_elemento = cli.cod_elemento
                                                            AND cli.cod_tipo_elemento = CASE tr.cod_tipo_elemento
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                                                        END
                                                           )
                                       INNER JOIN (SELECT /*+ USE_NL(tmp tp) */
                                                          cod_tipo_fonte, cod_raggr_fonte
                                                   FROM (select regexp_substr(pFonte,'[^\|]+', 1, level) ALF1 from dual
                                                      connect by regexp_substr(pFonte, '[^\|]+', 1, level) is not null )
                                                    tmp
                                                    INNER JOIN TIPO_FONTI tp ON alf1 = cod_raggr_fonte
                                                  ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (select regexp_substr(pTipologiaRete,'[^\|]+', 1, level) COD_TIPO_RETE from dual
                                                      connect by regexp_substr(pTipologiaRete, '[^\|]+', 1, level) is not null
                                                 )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
                                       INNER JOIN (select regexp_substr(pTipoProd,'[^\|]+', 1, level) COD_TIPO_CLIENTE from dual
                                                      connect by regexp_substr(pTipoProd, '[^\|]+', 1, level) is not null
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                      PKG_Mago_utl.gcGeneratoreBT,
                                                                      PKG_Mago_utl.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                       INNER JOIN (SELECT /*+ USE_HASH(REL,ELE,I) */
                                         ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                    FROM wgerarchia_geo rel
                                        INNER JOIN ELEMENTI ele 
                                         ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                                         WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                         AND cod_tipo_elemento = PKG_Mago_utl.gcComune
                                         AND rel.data_disattivazione != rel.data_disattivazione_calc
                                         ) B ON B.COD_ELEMENTO = A.COD_ELEMENTO
                 ) A;
    END IF;
    PKG_Logs.TraceLog('Eseguito GetGeneratori - '||PKG_Mago_utl.StdOutDate(pData)||
                                   '   TipiRete='||pTipologiaRete||
                                       '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.GetGeneratori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetGeneratori;

-- ----------------------------------------------------------------------------------------------------------

--===============================================================================

PROCEDURE GetTrasformatori  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

 

    IF pDisconnect = 0 THEN
        OPEN pRefCurs FOR
            SELECT distinct COD_ELEMENTO_TRASFORMATORE,FONTE
              FROM (SELECT PKG_Elementi.GetElementoPadre (A.COD_ELEMENTO,PKG_Mago_utl.gcTrasformMtBt,pData,
                                                          PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale) COD_ELEMENTO_TRASFORMATORE,
                           COD_TIPO_FONTE FONTE, POTENZA_INSTALLATA,
                           COD_TIPO_CLIENTE, COD_TIPO_RETE
                      FROM (SELECT ELE.COD_ELEMENTO,
                                    tfonte.COD_RAGGR_FONTE COD_TIPO_FONTE
                                    ,NVL(TR.valore, 0) POTENZA_INSTALLATA,ELE.COD_TIPO_ELEMENTO,
                                   ELE.COD_TIPO_CLIENTE, ELE.COD_TIPO_RETE
                              FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                           NVL(COD_TIPO_FONTE, PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                           NVL(COD_TIPO_CLIENTE, PKG_Mago_utl.gcClienteAutoProd) COD_TIPO_CLIENTE
                                      FROM (SELECT ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                   PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                              FROM ELEMENTI_DEF DEF INNER JOIN ELEMENTI ELE ON (ELE.cod_elemento = DEF.cod_elemento)
                                             WHERE ELE.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                             GROUP BY ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente, 'B'
                                           ) ELE
                                    ) ELE
                              LEFT OUTER JOIN (SELECT TR.*, MIS.valore, MIS.data_attivazione, MIS.data_disattivazione
                                                 FROM TRATTAMENTO_ELEMENTI TR
                                                INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                              AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata)
                                              ) TR ON (    TR.cod_elemento = ELE.cod_elemento
                                                       AND TR.cod_tipo_fonte = ELE.cod_tipo_fonte
                                                       AND TR.cod_tipo_cliente = ELE.cod_tipo_cliente)
                            INNER JOIN (SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE
                                         FROM (select regexp_substr(pFonte,'[^\|]+', 1, level) ALF1 from dual
                                                      connect by regexp_substr(pFonte, '[^\|]+', 1, level) is not null 
                                            ) PF 
                                          INNER JOIN TIPO_FONTI T 
                                          ON PF.ALF1 = T.COD_RAGGR_FONTE
                                         ) tfonte ON (tfonte.cod_tipo_fonte = ELE.cod_tipo_fonte)
                           INNER JOIN (select regexp_substr(pTipologiaRete,'[^\|]+', 1, level) COD_TIPO_RETE from dual
                                          connect by regexp_substr(pTipologiaRete, '[^\|]+', 1, level) is not null
                                     )RET ON (RET.COD_TIPO_RETE=ele.COD_TIPO_RETE)
                           INNER JOIN (select regexp_substr(pTipoProd,'[^\|]+', 1, level) COD_TIPO_CLIENTE from dual
                                          connect by regexp_substr(pTipoProd, '[^\|]+', 1, level) is not null
                                      ) PR ON (PR.COD_TIPO_CLIENTE=ele.COD_TIPO_CLIENTE)
                             WHERE (  TR.cod_trattamento_elem IS NOT NULL
                                    OR pFlagPI = 0)
                               AND pData BETWEEN NVL (TR.DATA_ATTIVAZIONE, TO_DATE ('01011900', 'ddmmyyyy'))
                                             AND NVL (TR.DATA_DISATTIVAZIONE, TO_DATE ('01013000', 'ddmmyyyy'))
                           ) A
                      LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                           PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale)
                    ) A
              LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                       AND B.COD_TIPO_FONTE = A.FONTE
                                                       AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE)
                        ;
   ELSE
        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT 
                                             rel.cod_elemento
                                            ,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT 
                                             rel.cod_elemento
                                            ,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT distinct COD_ELEMENTO_TRASFORMATORE,FONTE     FROM (SELECT TR.cod_elemento_trasformatore,COD_TIPO_FONTE FONTE,POTENZA_INSTALLATA,
                              COD_TIPO_CLIENTE,COD_TIPO_RETE
                        FROM (SELECT ele.COD_ELEMENTO,ele.COD_TIPO_FONTE,NVL(tr.valore,0) POTENZA_INSTALLATA,ele.COD_TIPO_ELEMENTO,ele.COD_TIPO_CLIENTE,ele.COD_TIPO_RETE
                                FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                             NVL(COD_TIPO_FONTE,PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                             NVL(COD_TIPO_CLIENTE,PKG_Mago_utl.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                        FROM (SELECT ele.cod_elemento, DEF.cod_tipo_fonte,
                                                     ele.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                     PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                                FROM ELEMENTI_DEF DEF
                                              INNER JOIN ELEMENTI ELE ON (ele.cod_elemento = DEF.cod_elemento)
                                              WHERE ele.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                              GROUP BY ele.cod_elemento, DEF.cod_tipo_fonte, ele.cod_tipo_elemento, DEF.cod_tipo_cliente , 'B'
                                             ) ELE
                                     ) ELE
                                LEFT OUTER JOIN (SELECT tr.* , MIS.valore, MIS.data_attivazione, MIS.data_disattivazione,
                                                        NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                   FROM TRATTAMENTO_ELEMENTI TR
                                                  INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                               AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata
                                                                                               )
                                                  WHERE MIS.valore != 0
                                                ) TR ON (    TR.cod_elemento = ele.cod_elemento
                                                         AND tr.cod_tipo_fonte = ele.cod_tipo_fonte
                                                         AND tr.cod_tipo_cliente = ele.cod_tipo_cliente )
                               INNER JOIN (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE
                                             FROM (select regexp_substr(pFonte,'[^\|]+', 1, level) ALF1 from dual
                                                      connect by regexp_substr(pFonte, '[^\|]+', 1, level) is not null )
                                            INNER JOIN TIPO_FONTI ON ALF1 = COD_RAGGR_FONTE
                                            GROUP BY COD_RAGGR_FONTE
                                          ) tfonte ON (tfonte.cod_tipo_fonte = ele.cod_tipo_fonte)
                                                     INNER JOIN (select regexp_substr(pTipologiaRete,'[^\|]+', 1, level) COD_TIPO_RETE from dual
                                          connect by regexp_substr(pTipologiaRete, '[^\|]+', 1, level) is not null
                                     )RET ON (RET.COD_TIPO_RETE=ele.COD_TIPO_RETE)
                           INNER JOIN (select regexp_substr(pTipoProd,'[^\|]+', 1, level) COD_TIPO_CLIENTE from dual
                                          connect by regexp_substr(pTipoProd, '[^\|]+', 1, level) is not null
                                      ) PR ON (PR.COD_TIPO_CLIENTE=ele.COD_TIPO_CLIENTE)
                             WHERE (tr.cod_trattamento_elem IS NOT NULL OR pFlagPI = 0)
                                 AND pData BETWEEN NVL(tr.data_attivazione, TO_DATE('01011900','ddmmyyyy'))
                                               AND NVL(tr.data_disattivazione_calc,TO_DATE('01013000','ddmmyyyy'))
                             ) A
                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                     FROM wgerarchia_geo rel
                                    INNER JOIN ELEMENTI ele 
                                    ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                                     WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcComune
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) B ON (b.cod_elemento = A.cod_elemento)
                      INNER JOIN (SELECT ele.cod_elemento cod_elemento_trasformatore, rel.cod_elemento
                                     FROM wgerarchia_ele rel
                                    INNER JOIN ELEMENTI ele 
                                    ON ( ele.cod_elemento IN (rel.L01,rel.L02,rel.L03,rel.L04,rel.L05,rel.L06,rel.L07,rel.L08,rel.L09,rel.L10,rel.L11,rel.L12,rel.L13,rel.L14,rel.L15,rel.L16,rel.L17,rel.L18,rel.L19,rel.L20,rel.L21,rel.L22,rel.L23,rel.L24,rel.L25))
                                     WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcTrasformMtBt
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) TR ON (tr.cod_elemento = A.cod_elemento)
                        ) A
                   LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                            AND B.COD_TIPO_FONTE = A.FONTE
                                                            AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE )


                              ;
   END IF;

   PKG_Logs.TraceLog('Eseguito GetTrasformatoriforRES - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.GetTrasformatoriforRES'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformatori;

-------------------------------------------------------------------------------

FUNCTION f_SolareOmogeneo(pCOD_ELEMENTO IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN NUMBER IS

vEsito NUMBER;

BEGIN

 SELECT count(e.COD_ELEMENTO) 
 INTO vEsito
 FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaInf(pCOD_ELEMENTO,
                               1, 1, sysdate, 'TRM|CMT|GMT|CAT|CBT|GAT|GBT',1)) T
                        JOIN ELEMENTI_DEF  E 
                        ON (E.COD_ELEMENTO = T.COD_ELEMENTO) 
                        JOIN (select COD_TIPO_FONTE from TIPO_FONTI 
                              WHERE COD_RAGGR_FONTE <> g_Fontesolare) F
                        ON (F.COD_TIPO_FONTE = E.COD_TIPO_FONTE);

 return 1 - SIGN(vEsito);

END;

-------------------------------------------------------------------------------

FUNCTION f_GetElement   (pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2 DEFAULT 'M|B',
                                pFonte          IN VARCHAR2 DEFAULT 'S|E|I|T|R|C|1',
                                pTipoProd       IN VARCHAR2 DEFAULT 'A|B|X',
                                pTipoElement    IN VARCHAR2 DEFAULT 'TRM|CMT|GMT',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pdisconnect     IN NUMBER DEFAULT 1)  
RETURN t_SPVTable PIPELINED AS
PRAGMA AUTONOMOUS_TRANSACTION; -- da qualche parte nelle SPC npi� intorate ci sono delle insert in GTT
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante - Funzione clonata dalla PKG_METEO.GetElementForecast
-----------------------------------------------------------------------------------------------------------*/
    vNum        INTEGER;
    vFlgNull    NUMBER(1) := -1;

    vIdFonti    INTEGER := PKG_Mago_utl.GetIdTipoFonti(pFonte);
    vIdReti     INTEGER := PKG_Mago_utl.GetIdTipoReti(pTipologiaRete);
    vIdProd     INTEGER := PKG_Mago_utl.GetIdTipoClienti(pTipoProd);

    vElePadre   ELEMENTI.COD_ELEMENTO%TYPE;
    vGstPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vGstElem    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    VtIPelem    ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
    vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
    vFonte      TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
    vPotenza    ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
    vTipProd    TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
    vTipRet     TIPI_RETE.COD_TIPO_RETE%TYPE;
 
    vOmog  NUMBER;

    v_SPVElement t_SPVElement;
    
    v_indxFore     VARCHAR2(100);
    
--    g_SPVTable  t_SPVTable := t_SPVTable();
    v_SPVTable  t_SPVTable := t_SPVTable();


g_ForecastTable t_ForecastTable;

--------------------------------------------------------------------------------
-------------------------------------------------------------------------------
    
    PROCEDURE sp_CaricaTabellaElements   (pData           IN DATE,
                                    pTipologiaRete  IN VARCHAR2,
                                    pFonte          IN VARCHAR2,
                                    pTipoProd       IN VARCHAR2,
                                    pTipoElement    IN VARCHAR2,
                                    pFlagPI         IN NUMBER DEFAULT 1,
                                    pdisconnect     IN NUMBER DEFAULT 1) AS
    /*-----------------------------------------------------------------------------------------------------------
        Restituisce l'elenco dei produttori e relativi generatori in un determinato istante - Funzione clonata dalla PKG_METEO.GetElementForecast
    -----------------------------------------------------------------------------------------------------------*/
        vNum        INTEGER;
        vFlgNull    NUMBER(1) := -1;
        
        vRefCurs    PKG_UtlGlb.t_query_cur;
        vIdFonti    INTEGER := PKG_Mago_utl.GetIdTipoFonti(pFonte);
        vIdReti     INTEGER := PKG_Mago_utl.GetIdTipoReti(pTipologiaRete);
        vIdProd     INTEGER := PKG_Mago_utl.GetIdTipoClienti(pTipoProd);
    
        vElePadre   ELEMENTI.COD_ELEMENTO%TYPE;
        vGstPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;
        vGstElem    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
        vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
        vFonte      TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
        vPotenza    ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
        vTipProd    TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
        vTipRet     TIPI_RETE.COD_TIPO_RETE%TYPE;
    
            
    BEGIN
    
        SELECT COUNT(*)
          INTO vNum
          FROM (select regexp_substr(pTipoElement,'[^\|]+', 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, '[^\|]+', 1, level) is not null )                                         
         WHERE ALF1 IN (PKG_Mago_utl.gcClienteAT,PKG_Mago_utl.gcClienteMT,PKG_Mago_utl.gcClienteBT);
         
         IF vNum > 0 THEN
         
            FOR iDisco In 0..pDisconnect LOOP
                GetProduttori(vRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pFlagPI,iDisco/*ElemenDisconnessi o no*/);
                LOOP
                   FETCH vRefCurs INTO vElePadre,
                   vFonte  ;
                   EXIT WHEN vRefCurs%NOTFOUND;
                   
                   IF NOT velePadre IS NULL THEN
                   
                      g_ForecastTable( PKG_ELEMENTI.GetGestElemento(velePadre)).IS_SOLARE_OMOGENEO := 
                            CASE vFonte 
                                WHEN g_Fontesolare THEN f_SolareOmogeneo(vElePadre)
                                ELSE 0
                            END;

                   END IF;
                   
                END LOOP;
                CLOSE vRefCurs;
            END LOOP;
         END IF;
    
        SELECT COUNT(*)
          INTO vNum
          FROM (select regexp_substr(pTipoElement,'[^\|]+', 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, '[^\|]+', 1, level) is not null ) 
         WHERE ALF1 IN (PKG_Mago_utl.gcTrasformMtBt);
         IF vNum > 0 THEN
    
            FOR iDisco In 0..pDisconnect LOOP
                GetTrasformatori(vRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pFlagPI,iDisco /*ElemenNonDisconnessi*/);
                LOOP
                   FETCH vRefCurs INTO vCodEle,vFonte ;
        
                   EXIT WHEN vRefCurs%NOTFOUND;
                   
                   IF NOT vCodele IS NULL THEN
    
                      g_ForecastTable( PKG_ELEMENTI.GetGestElemento(vCodele)).IS_SOLARE_OMOGENEO := 
                            CASE vFonte 
                                WHEN g_Fontesolare THEN f_SolareOmogeneo(vCodele)
                                ELSE 0
                            END;
                                            
                    END IF;
                    
                 END LOOP;
                CLOSE vRefCurs;
                
            END LOOP;
         END IF;
    
        SELECT COUNT(*)
          INTO vNum
          FROM (select regexp_substr(pTipoElement,'[^\|]+', 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, '[^\|]+', 1, level) is not null ) 
         WHERE ALF1 IN (PKG_Mago_utl.gcGeneratoreAT,PKG_Mago_utl.gcGeneratoreMT,PKG_Mago_utl.gcGeneratoreBT);
         IF vNum > 0 THEN
    
            FOR iDisco In 0..pDisconnect LOOP
                GetGeneratori(vRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,iDisco);
                LOOP
                   FETCH vRefCurs INTO 
                   vFonte,vGstPadre
                                       ;
                   EXIT WHEN vRefCurs%NOTFOUND;
                   
                   IF NOT vGstPadre IS NULL THEN
                   
                      g_ForecastTable(vGstPadre).IS_SOLARE_OMOGENEO := 
                            CASE vFonte 
                                WHEN g_Fontesolare THEN f_SolareOmogeneo(PKG_Elementi.GetCodElemento(vGstPadre))
                                ELSE 0
                            END;
                                                
                    END IF;
                    
                END LOOP;
                CLOSE vRefCurs;
            END LOOP;
         END IF;
    
       PKG_Logs.TraceLog('Eseguito sp_CaricaTabellaElements - '||PKG_Mago_utl.StdOutDate(pData)||
                                           '   TipiRete='||pTipologiaRete||
                                              '   Fonti='||pFonte||
                                           '   TipiProd='||pTipoProd||
                                           '   TipiElem='||pTipoElement,PKG_UtlGlb.gcTrace_VRB);
    
    EXCEPTION
        WHEN OTHERS THEN
             PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'procedure PKG_SPV_ELEMENT.sp_CaricaTabellaElements'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
             PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
             RAISE;
    END sp_CaricaTabellaElements;
    
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

BEGIN


    sp_CaricaTabellaElements(pData,pTipologiaRete,pFonte,pTipoProd,pTipoElement,pFlagPI,pdisconnect); 
    
       v_SPVElement := NEW t_SPVElement( ' ',0 );  -- sfo init object  t_SPVElement
       v_indxFore := g_ForecastTable.FIRST;
       WHILE v_indxFore IS NOT NULL
       LOOP
       
            v_SPVElement.COD_GEST_ELEMENTO := v_indxFore;
            v_SPVElement.IS_SOLARE_OMOGENEO := g_ForecastTable(v_indxFore).IS_SOLARE_OMOGENEO;
            
            rollback;
            PIPE ROW (v_SPVElement) ;
            
                
            v_indxFore := g_ForecastTable.NEXT(v_indxFore);

       END LOOP;

    
    RETURN;
    
EXCEPTION
    when NO_DATA_NEEDED then -- 'per pulizia' della pipeline
          return;
    WHEN OTHERS THEN
    
    IF sqlcode = '-6502' then
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SPV_ELEMENT.F_GetElement'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE||CHR(10)||SQLCODE||CHR(10)||v_indxFore);
    ELSE
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SPV_ELEMENT.F_GetElement'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE_APPLICATION_ERROR(-20666,'Funzione PKG_SPV_ELEMENT.F_GetElement'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE||CHR(10)||SQLCODE||CHR(10)||v_indxFore);
   END IF;
END f_GetElement;

-- =========================================================================================

 PROCEDURE sp_GetElement   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2 DEFAULT 'M|B',
                                pFonte          IN VARCHAR2 DEFAULT 'S|E|I|T|R|C|1',
                                pTipoProd       IN VARCHAR2 DEFAULT 'A|B|X',
                                pTipoElement    IN VARCHAR2 DEFAULT 'TRM|CMT|GMT',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pdisconnect     IN NUMBER DEFAULT 1) IS
                                
BEGIN

       OPEN pRefCurs FOR
        SELECT COD_GEST_ELEMENTO, IS_SOLARE_OMOGENEO FROM TABLE(CAST(f_GetElement(pData,
                                pTipologiaRete,
                                pFonte,
                                pTipoProd,
                                pTipoElement,
                                pFlagPI,
                                pdisconnect ) AS t_SPVTable)); 


END sp_GetElement;

-- =========================================================================================

BEGIN
    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_SPV_ELEMENT',
                          pStoreOnFile => FALSE);
END PKG_SPV_ELEMENT;
/