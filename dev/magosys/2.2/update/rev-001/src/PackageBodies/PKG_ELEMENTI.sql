PROMPT PACKAGE BODY: PKG_ELEMENTI

CREATE OR REPLACE PACKAGE BODY "PKG_ELEMENTI" AS

/* ***********************************************************************************************************
   NAME:       PKG_Elementi
   PURPOSE:    Servizi per la gestione degli elementi

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.1.c      13/05/2013  Moretti C.       Visualizzazione codici Direttrici per Montanti di linea MT
   1.4.a      22/01/2014  Moretti C.       Implementazioni per versione 1.4a.p11
   1.6.a      13/01/2014  Moretti C.       Implementazioni per versione 1.6a
                                           - GetGestCodePerimeter
   1.1.h      14/03/2014  Moretti C.       In funzione GetElemInfo esclude da selezione la PI = 0
   1.7.a.2    04/04/2014  Moretti C.       parametro aggiuntivo per stato Rete in GetElementsRegistry
   1.8.a.1    22/04/2014  Moretti C.       Utilizzo Carattere jolly in procedura GetElementList
   1.8.a.5    26/08/2014  Moretti C.       Modifica gestione GetElementList
   1.9.a.0    05/08/2014  Moretti C.       Gestione Centri satellite primo step
   1.9.a.2    08/08/2014  Moretti C.       Implementazione GestCodePerimeter
   1.9.a.3    24/09/2014  Moretti C.       Gestione Centri satellite implementazione finale
   1.9.a.4    16/10/2014  Moretti C.       modificato PKG_ELEMENTI.GetElementList per soppressione dei
                                           Centri Satellite dalla selezione da Ricerca Testuale
   1.9.a.6    21/10/2014  Moretti C.       modificato PKG_ELEMENTI.GestCodePerimeter per nuovo filtro
                                           elementi con PI
   1.9.a.9    16/02/2015  Moretti C.       corretta funzione GetElemInfo.CheckDate per DATA_DISATTIVAZIONE_UNDISCONN
   1.9.a.14   27/04/2015  Moretti C.       corretta funzione GetElemInfo per attribuzione StatoRete in elementi di CS
   1.9.c.7    16/02/2016  Frosini S.       modificata GestCodePerimeter - pi� tipi elementi, aggiunto parametro fonti
   1.11        09706/2016  Roberto Z.    Modiicata per MAGONAZ
   2.0.2	  17/10/2016	Forno S.		Fix per ACEA - Sostituzione in mago di CONNECT BY PRIOR <condizione> con CONNECT BY NOCYCLE PRIOR <condizione>
   2.2.1     26/01/2017    Forno S.        Modifico GestCodePerimeter per mago-670
   2.2.1 	10/04/2017 	Frosini S.         Nuova funzione f_GetGestFromPod     			-- Spec. Version 2.2.1
      NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gElementoBase      ELEMENTI.COD_ELEMENTO%TYPE;
 gElementoEseDef    ELEMENTI.COD_ELEMENTO%TYPE;

 cGerarchiaELE      CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazELE;          -- gerarchia elettrica          generico
 cGerarchiaELE_CP   CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazELE * 10 + 1; -- gerarchia elettrica          di cabina primaria
 cGerarchiaELE_CS   CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazELE * 10 + 2; -- gerarchia elettrica          di cabina secondaria
 cGerarchiaGEO      CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazGEO;          -- gerarchia geografica (Istat) base
 cGerarchiaGEO_CS   CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazGEO * 10 + 2; -- gerarchia geografica (Istat) di cabina secondaria
 cGerarchiaAMM      CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazAMM;          -- gerarchia amministrativa     base
 cGerarchiaAMM_CS   CONSTANT INTEGER   := Pkg_Mago_utl.gcOrganizzazAMM * 10 + 2; -- gerarchia amministrativa     di cabina secondaria

 gIDReteInit        BOOLEAN := FALSE;
 gIDReteAT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete AT
 gIDReteMT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete MT
 gIDReteBT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete BT

 gIDProdInit        BOOLEAN := FALSE;
 gIDProdPuro        TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID CLIENTE puro
 gIDProdCliente     TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID CLIENTE/Cliente
 gIDProdNonDeterm   TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID Non determinato
 gIDProdNonApplic   TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID Non applicabile

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

PROCEDURE GetGerarchiaID    (pElemento       IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pGerarchia      IN INTEGER,
                             pGerarchiaBase OUT INTEGER,
                             pNextLevel     OUT INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna l'identificativo gerarchia in base all'organizzazione e all'elemento
-----------------------------------------------------------------------------------------------------------*/
    vLivEcp TIPI_ELEMENTO.GER_ECP%TYPE;
    vLivEcs TIPI_ELEMENTO.GER_ECS%TYPE;
    vLivAmm TIPI_ELEMENTO.GER_AMM%TYPE;
    vLivGeo TIPI_ELEMENTO.GER_GEO%TYPE;
    vtip TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
 BEGIN
    SELECT E.COD_TIPO_ELEMENTO ,NVL(GER_ECP,0), NVL(GER_ECS,0), NVL(GER_AMM,0), NVL(GER_GEO,0)
      INTO vTip, vLivEcp, vLivEcs, vLivAmm, vLivGeo
      FROM ELEMENTI E
     INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO)
     WHERE COD_ELEMENTO = pElemento;
    IF vLivEcs > 0 THEN -- assumo che la CS sia il punto di congiunzione ...
        CASE pGerarchia
            WHEN cGerarchiaELE THEN pGerarchiaBase := cGerarchiaELE_CS;
            WHEN cGerarchiaGEO THEN pGerarchiaBase := cGerarchiaGEO_CS;
            WHEN cGerarchiaAMM THEN pGerarchiaBase := cGerarchiaAMM_CS;
        END CASE;
    ELSE
        CASE pGerarchia
            WHEN cGerarchiaELE THEN pGerarchiaBase := cGerarchiaELE_CP;
            WHEN cGerarchiaGEO THEN pGerarchiaBase := pGerarchia;
            WHEN cGerarchiaAMM THEN pGerarchiaBase := pGerarchia;
        END CASE;
    END IF;
    CASE pGerarchiaBase
        WHEN cGerarchiaELE_CP  THEN pNextLevel := vLivEcp; -- gerarchia elettrica di cabina primaria
        WHEN cGerarchiaELE_CS  THEN pNextLevel := vLivEcs; -- gerarchia elettrica di cabina secondaria
        WHEN cGerarchiaGEO_CS  THEN pNextLevel := vLivEcs; -- gerarchia geografica (Istat) di cabina secondariacabina secondaria
        WHEN cGerarchiaAMM_CS  THEN pNextLevel := vLivEcs; -- gerarchia amministrativa di cabina secondaria
        WHEN cGerarchiaGEO     THEN pNextLevel := vLivGeo; -- gerarchia geografica (Istat)
        WHEN cGerarchiaAMM     THEN pNextLevel := vLivAmm; -- gerarchia amministrativa
    END CASE;
END GetGerarchiaID;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetFETreeRefDate (pDataDa      IN DATE,
                           pDataA       IN DATE) RETURN DATE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la data ti riferimento per la selezione dell'albero in FE
-----------------------------------------------------------------------------------------------------------*/
    vData DATE;
BEGIN
--    CASE
--        WHEN SYSDATE BETWEEN pDataDa AND pDataA THEN  vData := SYSDATE;
--        WHEN SYSDATE       > pDataA             THEN  vData := pDataA;
--        WHEN SYSDATE       < pDataDa            THEN  vData := pDataDa;
--    END CASE;
--    RETURN GetStartDate (vData);
   RETURN pDataA;
END GetFETreeRefDate;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitIdRete AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza codici numerici identificativi dell tipo rete
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF gIDReteInit = TRUE THEN
        RETURN;
    END IF;
    SELECT ID_RETE INTO gIDReteAT FROM TIPI_RETE WHERE COD_TIPO_RETE = Pkg_Mago_utl.gcTipReteAT;
    SELECT ID_RETE INTO gIDReteMT FROM TIPI_RETE WHERE COD_TIPO_RETE = Pkg_Mago_utl.gcTipReteMT;
    SELECT ID_RETE INTO gIDReteBT FROM TIPI_RETE WHERE COD_TIPO_RETE = Pkg_Mago_utl.gcTipReteBT;
    gIDReteInit := TRUE;
END InitIdRete;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitIdProd AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza codici numerici identificativi dell tipo CLIENTE
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF gIDProdInit = TRUE THEN
        RETURN;
    END IF;
    SELECT ID_CLIENTE INTO gIDProdPuro      FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = Pkg_Mago_utl.gcClienteProdPuro;
    SELECT ID_CLIENTE INTO gIDProdCliente   FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = Pkg_Mago_utl.gcClienteAutoProd;
    SELECT ID_CLIENTE INTO gIDProdNonDeterm FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = Pkg_Mago_utl.gcClientePrdNonDeterm;
    SELECT ID_CLIENTE INTO gIDProdNonApplic FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = Pkg_Mago_utl.gcClientePrdNonApplic;
    gIDProdInit := TRUE;
END InitIdProd;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetOrderByElemType (pTipoElem       IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pCodGestElem    IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la priorit� di ordimamento di default per il tipo elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    CASE pTipoElem
       WHEN Pkg_Mago_utl.gcCentroOperativo   THEN RETURN 0;
       WHEN Pkg_Mago_utl.gcEsercizio THEN
                  CASE INSTR(pCodGestElem,'_')
                      WHEN 0             THEN RETURN 1;
                                         ELSE RETURN 2;
                  END CASE;
       WHEN Pkg_Mago_utl.gcCabinaPrimaria    THEN RETURN 3;
       WHEN Pkg_Mago_utl.gcTrasfCabPrim      THEN RETURN 4;
       WHEN Pkg_Mago_utl.gcClienteAT         THEN RETURN 5;
       WHEN Pkg_Mago_utl.gcGeneratoreAT      THEN RETURN 6;
       WHEN Pkg_Mago_utl.gcSecondarioDiTrasf THEN RETURN 7;
       WHEN Pkg_Mago_utl.gcTerziarioDiTrasf  THEN RETURN 8;
       WHEN Pkg_Mago_utl.gcSbarraMT          THEN RETURN 9;
       WHEN Pkg_Mago_utl.gcLineaMT           THEN RETURN 10;
       WHEN Pkg_Mago_utl.gcSbarraCabSec      THEN RETURN 11;
       WHEN Pkg_Mago_utl.gcClienteMT         THEN RETURN 12;
       WHEN Pkg_Mago_utl.gcGeneratoreMT      THEN RETURN 13;
       WHEN Pkg_Mago_utl.gcTrasformMtBt      THEN RETURN 14;
       WHEN Pkg_Mago_utl.gcTrasformMtBtDett  THEN RETURN 15;
       WHEN Pkg_Mago_utl.gcClienteBT         THEN RETURN 16;
       WHEN Pkg_Mago_utl.gcGeneratoreBT      THEN RETURN 17;
       ELSE                                   RETURN 99;
    END CASE;
 END GetOrderByElemType;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetElemBaseEseDef (pElementoBase     ELEMENTI.COD_ELEMENTO%TYPE,
                             pElementoEseDef   ELEMENTI.COD_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Imposta le variabili
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    gElementoBase   := pElementoBase;
    gElementoEseDef := gElementoEseDef;
 END SetElemBaseEseDef;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoBase    RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'esercizio di default
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gElementoBase;
 END GetElementoBase;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoEseDef    RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elemento base
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gElementoEseDef;
 END GetElementoEseDef;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION DecodeTipElem      (pTipEle         IN VARCHAR2,
                             pCodeOnNotFound IN BOOLEAN DEFAULT TRUE)
                      RETURN TIPI_ELEMENTO.DESCRIZIONE%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    decodifica il tipo elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vDesc  TIPI_ELEMENTO.DESCRIZIONE%TYPE;
BEGIN

    SELECT DESCRIZIONE
      INTO vDesc
      FROM TIPI_ELEMENTO
     WHERE COD_TIPO_ELEMENTO = pTipEle;
    RETURN vDesc;

 EXCEPTION
    WHEN NO_DATA_FOUND THEN
        IF pCodeOnNotFound THEN
            RETURN pTipEle;
        ELSE
            RETURN NULL;
        END IF;
    WHEN OTHERS THEN RAISE;

END DecodeTipElem;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetDefaultCO      (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                             pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le informazioni relative al CO di default
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO
      INTO pElem, pGest, pNome
      FROM DEFAULT_CO
     INNER JOIN V_ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_CO
     WHERE DATA_DISATTIVAZIONE >= SYSDATE
       AND DATA_ATTIVAZIONE    <= SYSDATE;
END GetDefaultCO;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetDefaultESE     (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                             pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le informazioni relative alla Esercizio di default
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO
      INTO pElem, pGest, pNome
      FROM DEFAULT_CO
     INNER JOIN V_ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
     WHERE DATA_DISATTIVAZIONE >= SYSDATE
       AND DATA_ATTIVAZIONE   <= SYSDATE;
END GetDefaultESE;

-- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                             pDataA          IN DATE,
--                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elementoralativi ALL'ultima occorenza nel periodo richiesto
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT COD_ELEMENTO,
--                        COD_GEST_ELEMENTO,
--                        NOME_ELEMENTO,
--                        COD_TIPO_ELEMENTO,
--                        COD_TIPO_FONTE,
--                        COD_TIPO_CLIENTE,
--                        ID_ELEMENTO,
--                        RIF_ELEMENTO,
--                        COORDINATA_X,
--                        COORDINATA_Y,
--                        POTENZA_INSTALLATA,
--                        POTENZA_CONTRATTUALE
--                   FROM (SELECT E.COD_ELEMENTO,
--                                COD_GEST_ELEMENTO,
--                                NOME_ELEMENTO,
--                                E.COD_TIPO_ELEMENTO,
--                                COD_TIPO_FONTE,
--                                COD_TIPO_CLIENTE,
--                                ID_ELEMENTO,
--                                RIF_ELEMENTO,
--                                COORDINATA_X,
--                                COORDINATA_Y,
--                                POTENZA_INSTALLATA,
--                                POTENZA_CONTRATTUALE,
--                                ROW_NUMBER()OVER(PARTITION BY E.COD_ELEMENTO ORDER BY E.COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
--                           FROM ELEMENTI E
--                          INNER JOIN ELEMENTI_DEF D ON (D.COD_ELEMENTO = E.COD_ELEMENTO)
--                          WHERE COD_ELEMENTO = pCodElem
--                            AND (DATA_DISATTIVAZIONE >= pDataDa AND DATA_ATTIVAZIONE <= pDataA)
--                        )
--                  WHERE ORD=1
--                )
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pData           IN DATE,
--                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla DATA richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (Pkg_Mago_utl.GetElemDef(pData,pData,pCodElem)))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                             pDataA          IN DATE,
--                             pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla data richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (Pkg_Mago_utl.GetElemDef(pDataDa,pDataA,GetCodElemento(pGstElem))))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pData           IN DATE,
--                             pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla data richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (Pkg_Mago_utl.GetElemDef(pData,pData,GetCodElemento(pGstElem))))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetCodElemento    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNoNascosti     IN INTEGER DEFAULT 0)
                                                RETURN ELEMENTI.COD_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Converte il codice gestionale in codice elemento
-----------------------------------------------------------------------------------------------------------*/
    vElem     ELEMENTI.COD_ELEMENTO%TYPE := NULL;
    vNascosto TIPI_ELEMENTO.NASCOSTO%TYPE;
BEGIN
    BEGIN
        SELECT COD_ELEMENTO, NASCOSTO
          INTO vElem, vNascosto
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
         WHERE COD_GEST_ELEMENTO = pGestElem;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                SELECT COD_ELEMENTO
                  INTO vElem
                  FROM ELEMENTI_DEF
                 INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                 INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
                 WHERE RIF_ELEMENTO = pGestElem
                   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN vElem := NULL;
            END ;
    END;
    IF pNoNascosti <> 0 AND vNascosto = 1 THEN
        vElem := NULL;
    END IF;
    RETURN vElem;
END GetCodElemento;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetGestElemento   (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pNoNascosti     IN INTEGER DEFAULT 0)
                                                RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Converte il codice elemento in godice gestionale
-----------------------------------------------------------------------------------------------------------*/
    vGest     ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
    vGest2    ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
    vTipo     ELEMENTI.COD_TIPO_ELEMENTO%TYPE := NULL;
    vNascosto TIPI_ELEMENTO.NASCOSTO%TYPE;
BEGIN
    BEGIN
        SELECT COD_GEST_ELEMENTO, NASCOSTO, COD_TIPO_ELEMENTO
          INTO vGest, vNascosto, vTipo
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodElem;
    IF pNoNascosti <> 0 AND vNascosto = 1 THEN
        vGest := NULL;
    ELSE
        IF vTipo = Pkg_Mago_utl.gcLineaMT THEN
            BEGIN
                SELECT RIF_ELEMENTO
                  INTO vGest2
                  FROM ELEMENTI_DEF E
                 WHERE COD_ELEMENTO = pCodElem
                   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                IF vGest2 IS NOT NULL THEN
                    vGest := vGest2;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN NULL;
            END;
        END IF;
    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN vGest := NULL;
    END;
    RETURN vGest;
END GetGestElemento;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  CalcOutCodGest    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pData           IN DATE DEFAULT SYSDATE,
                             pTipoElemento   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                             pRifElemento    IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE DEFAULT NULL)
                                                RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il codice gestionale piu' opportuno da presentare scegliendo tra codice Gestionale e Rif_elemento
-----------------------------------------------------------------------------------------------------------*/
    vRetCode ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
BEGIN
    IF (pTipoElemento IS NOT NULL) THEN
        IF pTipoElemento <> Pkg_Mago_utl.gcLineaMT THEN
            RETURN pGestElem;
        ELSE
            IF pRifElemento IS NOT NULL THEN
                RETURN pRifElemento;
            END IF;
        END IF;
    END IF;
    SELECT CASE
              WHEN E.COD_TIPO_ELEMENTO = Pkg_Mago_utl.gcLineaMT
                  THEN CASE
                         WHEN D.RIF_ELEMENTO IS NULL
                             THEN E.COD_GEST_ELEMENTO
                             ELSE D.RIF_ELEMENTO
                       END
                  ELSE E.COD_GEST_ELEMENTO
           END
      INTO vRetCode
      FROM ELEMENTI E
     INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
     WHERE E.COD_GEST_ELEMENTO = pGestElem
       AND NVL(pData,SYSDATE) BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE;
    RETURN vRetCode;
EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN NULL;
    WHEN OTHERS THEN RAISE;
END CalcOutCodGest;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoPadre   (pElemFiglio     IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pTipoPadre      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                             pData           IN DATE,
                             pOrganizzazione IN NUMBER,
                             pStato          IN NUMBER,
                             pDisconnect     IN NUMBER DEFAULT 0)
                                                RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce il Codice Elemento padre dell'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vSelEle VARCHAR2(1000) := 'SELECT e.COD_ELEMENTO, e.COD_TIPO_ELEMENTO '                     ||
                               'FROM ( SELECT r.*, #DATAELECALC# data_disattivazione_calc '     ||
                                        'FROM #TAB# r '                                         ||
                                    ') r '                                                      ||
                              'INNER JOIN ELEMENTI e '                                          ||
                                 'ON (e.COD_ELEMENTO IN (r.l01,r.l02,r.l03,r.l04,r.l05,'        ||
                                                        'r.l06,r.l07,r.l08,r.l09,r.l10,'        ||
                                                        'r.l11,r.l12,r.l13,r.l14,r.l15,'        ||
                                                        'r.l16,r.l17,r.l18,r.l14,r.l20,'        ||
                                                        'r.l21,r.l22,r.l23,r.l24,r.l25) )'      ||
                              'WHERE r.COD_ELEMENTO = :el '                                     || -- vElem
                                ' AND e.COD_TIPO_ELEMENTO = :tipo '                             ||
                                ' AND :dt BETWEEN r.DATA_ATTIVAZIONE AND r.DATA_DISATTIVAZIONE_CALC ';       -- pData
    vElem       ELEMENTI.COD_ELEMENTO%TYPE;
    vTipo       TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipoPadre;
    vGerEcs     TIPI_ELEMENTO.GER_ECS%TYPE;
    vRelTab     VARCHAR2(500);
    vLetto      BOOLEAN := FALSE;
    vTrovato    BOOLEAN := FALSE;
    TYPE t_cur IS REF CURSOR;
    vCur t_cur;

BEGIN
    BEGIN
--        SELECT GER_ECS
--          INTO vGerEcs
--          FROM TABLE(Pkg_Mago_utl.GetElemDef(pData,pElemFiglio))
--         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO);
        SELECT GER_ECS
          INTO vGerEcs
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pElemFiglio;
    EXCEPTION
         WHEN OTHERS THEN RAISE;
    END;

    CASE pOrganizzazione
        WHEN Pkg_Mago_utl.gcOrganizzazELE THEN vRelTab := 'GERARCHIA_IMP#STA#';
        WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN vRelTab := 'GERARCHIA_GEO';
        WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN vRelTab := 'GERARCHIA_AMM';
    END CASE;

    CASE pStato
        WHEN Pkg_Mago_utl.gcStatoNormale  THEN vRelTab := REPLACE(vRelTab,'#STA#','_SN');
        WHEN Pkg_Mago_utl.gcStatoAttuale  THEN vRelTab := REPLACE(vRelTab,'#STA#','_SA');
    END CASE;

    vSelEle := REPLACE(vSelEle,'#TAB#',vRelTab);

    vElem := pElemFiglio;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSelEle := REPLACE (vSelEle, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSelEle := REPLACE (vSelEle, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    BEGIN
        OPEN vCur FOR vSelEle USING vElem, vTipo, pData;
        LOOP
            FETCH vCur INTO vElem, vTipo;
            EXIT WHEN vCur%NOTFOUND;
            vTrovato := TRUE;
        END LOOP;
        CLOSE vCur;
        IF NOT vTrovato THEN
            vElem := NULL;
        END IF;
        RETURN vElem;
    END;

END GetElementoPadre;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  InsertElement     (pCodGest        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) RETURN  ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Definisce, se non gia' presente l'elemento, ritorna il codice elemento definito o trovato
-----------------------------------------------------------------------------------------------------------*/
    vCodEle  ELEMENTI.COD_ELEMENTO%TYPE;
    vTipEle  TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
BEGIN

    BEGIN
        INSERT INTO ELEMENTI (COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO)
                      VALUES (pCodGest,pTipElem) RETURNING COD_ELEMENTO INTO vCodEle;
        PKG_Logs.TraceLog('Inserito elemento - Cod='||vCodEle||'  Gest='||pCodGest||'  Tipo='||pTipElem);
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            SELECT COD_ELEMENTO, COD_TIPO_ELEMENTO
              INTO vCodEle, vTipEle
              FROM ELEMENTI
             WHERE COD_GEST_ELEMENTO = pCodGest;
            IF vTipEle <> pTipElem THEN
                UPDATE ELEMENTI             SET COD_TIPO_ELEMENTO = pTipElem WHERE COD_ELEMENTO = vCodEle;
                UPDATE TRATTAMENTO_ELEMENTI SET COD_TIPO_ELEMENTO = pTipElem WHERE COD_ELEMENTO = vCodEle;
                PKG_Logs.TraceLog('Per elemento - Cod='||vCodEle||'  Gest='||pCodGest||'  il Tipo e'' cambiato: da'||vTipEle||' a '||pTipElem);
--                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,
--                                        'L''elemento con codice gestionale '||pCodGest||
--                                        ' e'' gia'' presente con codice '||vCodEle||' e tipo elemento '||vTipEle||
--                                        '. L''inserimento con tipo '||pTipElem||' non e'' ammesso!.');
            END IF;
        WHEN OTHERS THEN RAISE;
    END;
    RETURN vCodEle;

END InsertElement;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetElemInfo       (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataRif        IN DATE,
                             pTipologiaRete  IN VARCHAR2,
                             pTipiCliente    IN VARCHAR2,
                             pOrganizzaz     IN NUMBER,
                             pStatoRete      IN INTEGER) RETURN INTEGER AS -- DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna un integer che descrive le caratteristiche dell'elemento
        XXX
        |||
        ||+----> Tipo Generazione sottesa
        |+-----> 1=Elemento Foglia, 0=Elemento con Figli sottesi
        +------> Sbarra sottesa a Montante di linea di altra CP

    Tipo Generazione sottesa;
        0 = nessuna generazione sottesa
        1 = AT
        2 = MT
        3 = AT+MT
        4 = BT
        5 = AT+BT
        6 = MT+BT
        7 = AT+MT+BT
        9 = Generazione non specificata (Tipologia rete non specificata)

    Sbarra sottesa a Montante di linea di altra CP
        1 = Sbarra di Centro Satellite sottesa a Montante di linea di Cabina Primaria
        2 = Sbarra di Cabina Primaria sottesa a Montante di linea di altra Cabina Primaria

-----------------------------------------------------------------------------------------------------------*/
    vNum          INTEGER;
    vTab          INTEGER;
    vRet          INTEGER := 0;
    vSumRet       INTEGER := NULL;
    vSumPrd       INTEGER := NULL;
    vCS           TIPI_ELEMENTO.GER_ECS%TYPE;
    vGstEle       ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vRifEle       ELEMENTI_DEF.RIF_ELEMENTO%TYPE;
    vGstCP        ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vFlag         ELEMENTI_DEF.FLAG%TYPE;
    vTipEle       TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTipElePadre  TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vReteAT       NUMBER(1) := 0;
    vReteMT       NUMBER(1) := 0;
    vReteBT       NUMBER(1) := 0;
    vStatoRete    INTEGER := pStatoRete;
    vTabRel       VARCHAR2(35) := 'REL_ELEMENTI_#GER##STA#';

    vSqlFoglia    VARCHAR2(400) := 'SELECT COUNT(*) FROM DUAL '                                                               ||
                                  'WHERE EXISTS (SELECT 1 '                                                                   ||
                                                  'FROM #TABREL# R '                                                          ||
                                                 'WHERE COD_ELEMENTO_PADRE = :ce '                                            ||
                                                   'AND :dt BETWEEN R.DATA_ATTIVAZIONE '                                      ||
                                                               'AND R.DATA_DISATTIVAZIONE '                                   ||
                                                   '#COND_GEO_DGF# '                                                          ||
                                                ')';

    vSqlFoglia2   VARCHAR2(400) := 'SELECT COUNT(*) FROM DUAL '                                                               ||
                                    'WHERE EXISTS (SELECT 1 '                                                                 ||
                                                    'FROM #TABREL# R '                                                        ||
                                                   'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = R.COD_ELEMENTO_FIGLIO '         ||
                                                   'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO ' ||
                                                   'WHERE COD_ELEMENTO_PADRE = :ce '                                          ||
                                                     'AND :dt BETWEEN R.DATA_ATTIVAZIONE '                                    ||
                                                                 'AND R.DATA_DISATTIVAZIONE '                                 ||
                                                     'AND T.NASCOSTO = '||PKG_UtlGlb.gkFlagOFF||' '                           ||
                                                     '#COND_GEO_DGF# '                                                        ||
                                                  ')';

    vSqlTipoPadre VARCHAR2(300) := 'SELECT E.COD_TIPO_ELEMENTO '                                                              ||
                                     'FROM #TABREL# R '                                                                       ||
                                    'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = R.COD_ELEMENTO_PADRE '                         ||
                                    'WHERE R.COD_ELEMENTO_FIGLIO = :ele '                                                     ||
                                      'AND :dt BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE '                         ||
                                      'AND R.DATA_DISATTIVAZIONE_UNDISCONN IS NULL';

    FUNCTION CheckDate(pTab IN INTEGER) RETURN INTEGER AS
        vNum INTEGER;
        vSql VARCHAR2(1000) :=
                'SELECT COUNT(*) FROM DUAL '                                                                                ||
                 'WHERE EXISTS (SELECT 1 '                                                                                  ||
                                 'FROM MISURE_#TAB#_STATICHE M '                                                            ||
                                'INNER JOIN TRATTAMENTO_ELEMENTI T ON (M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM) '   ||
                                'INNER JOIN ' || vTabRel || ' R ON (R.COD_ELEMENTO_PADRE = T.COD_ELEMENTO) '                ||
                                '#JOIN_TIPO_RETE# '                                                                         ||
                                '#JOIN_TIPO_CLIENTE# '                                                                      ||
                                'WHERE :dt BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE '                           ||
                                  'AND :dt BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE '                           ||
                                  'AND T.COD_ELEMENTO = :ce '                                                               ||
                                  'AND VALORE <> 0 '                                                                        ||
                                  'AND TIPO_AGGREGAZIONE = :st '                                                            ||
                                  '#FILTRO_TIPO_RETE# '                                                                     ||
                                  '#FILTRO_TIPO_CLIENTE# '                                                                  ||
                                  'AND COD_TIPO_MISURA IN ('''||Pkg_Mago_utl.gcPotenzaInstallata||''') '                        ||
                              ') ';
      BEGIN
        IF pTab = 1 THEN
            vSql := REPLACE(vSql,'#TAB#','ACQUISITE');
        ELSE
            vSql := REPLACE(vSql,'#TAB#','AGGREGATE');
        END IF;

        IF vSumRet IS NOT NULL THEN
            vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#','INNER JOIN TIPI_RETE USING(COD_TIPO_RETE) ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND BITAND(:rte,ID_RETE) = ID_RETE ');
        ELSE
            vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#',' ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#',' ');
        END IF;
        IF vSumPrd IS NOT NULL THEN
            vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#','INNER JOIN TIPI_CLIENTE USING(COD_TIPO_CLIENTE) ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND BITAND(:prd,ID_CLIENTE) = ID_CLIENTE ');
        ELSE
            vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#',' ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#',' ');
        END IF;
        CASE
            WHEN (vSumRet IS NOT NULL) AND (vSumPrd IS NOT NULL) THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumRet, vSumPrd;
            WHEN vSumRet IS NOT NULL THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumRet;
            WHEN vSumPrd IS NOT NULL THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumPrd;
            ELSE
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete;
        END CASE;
        RETURN vNum;
      END;

BEGIN

    InitIdRete;
    InitIdProd;

    BEGIN
        SELECT CASE
                  WHEN R.COD_ELEMENTO IS NOT NULL THEN COUNT(*)
                  ELSE 0
               END CASE,
               MAX(E.COD_GEST_ELEMENTO),
               MAX(RETE_AT),MAX(RETE_MT),MAX(RETE_BT),
               MAX(TAB_MISURE),MAX(GER_ECS),MAX(E.COD_TIPO_ELEMENTO)
          INTO vNum, vGstEle, vReteAT, vReteMT, vReteBT, vTab, vCS, vTipEle
          FROM (SELECT COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO,GER_ECS
                  FROM ELEMENTI E
                 INNER JOIN TIPI_ELEMENTO T ON(E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
                 WHERE COD_ELEMENTO = pCodElem
               ) E
          LEFT OUTER JOIN
               (SELECT COD_ELEMENTO, COD_TIPO_MISURA,
                       CASE
                        WHEN MAX(ACQ) = 0 AND MAX(AGR) = 0 THEN 0
                        WHEN MAX(ACQ) = 1 AND MAX(AGR) = 0 THEN 1
                        WHEN MAX(ACQ) = 0 AND MAX(AGR) = 1 THEN 2
                        WHEN MAX(ACQ) = 1 AND MAX(AGR) = 1 THEN 3
                       END TAB_MISURE,
                       MAX(RETE_AT) RETE_AT,
                       MAX(RETE_MT) RETE_MT,
                       MAX(RETE_BT) RETE_BT
                  FROM (SELECT COD_ELEMENTO, COD_TIPO_MISURA,
                               CASE TIPO_AGGREGAZIONE WHEN 0 THEN 1 ELSE 0 END ACQ,
                               CASE TIPO_AGGREGAZIONE WHEN 0 THEN 0 ELSE 1 END AGR,
                               CASE COD_TIPO_RETE     WHEN Pkg_Mago_utl.gcTipReteAT THEN 1 ELSE 0 END RETE_AT,
                               CASE COD_TIPO_RETE     WHEN Pkg_Mago_utl.gcTipReteMT THEN 1 ELSE 0 END RETE_MT,
                               CASE COD_TIPO_RETE     WHEN Pkg_Mago_utl.gcTipReteBT THEN 1 ELSE 0 END RETE_BT,
                               TIPO_AGGREGAZIONE
                          FROM TRATTAMENTO_ELEMENTI
                         WHERE COD_ELEMENTO = pCodElem
                           AND COD_TIPO_MISURA = Pkg_Mago_utl.gcPotenzaInstallata
                        )
                 GROUP BY COD_ELEMENTO, COD_TIPO_MISURA
             ) R ON (E.COD_ELEMENTO = R.COD_ELEMENTO)
       GROUP BY R.COD_ELEMENTO;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN 0;
    END;

    IF pOrganizzaz = Pkg_Mago_utl.gcOrganizzazELE THEN
        vStatoRete := pStatoRete;
    ELSE
        vStatoRete := Pkg_Mago_utl.gcStatoAttuale;
    END IF;

     IF vCS = PKG_UtlGlb.gkFlagON AND pOrganizzaz != Pkg_Mago_utl.gcOrganizzazELE THEN
        -- per elementi in gerarchia di CS (solo AMM e GEO) uso solo lo stato attuale
        vStatoRete := Pkg_Mago_utl.gcStatoAttuale;
    END IF;

    IF vCS = PKG_UtlGlb.gkFlagON THEN
        vTabRel := REPLACE(vTabRel,'#GER#','ECS');
    ELSE
        CASE pOrganizzaz
            WHEN Pkg_Mago_utl.gcOrganizzazELE THEN vTabRel := REPLACE(vTabRel,'#GER#','ECP');
            WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN vTabRel := REPLACE(vTabRel,'#GER#','AMM');
                                               vTabRel := REPLACE(vTabRel,'#STA#','');
            WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN vTabRel := REPLACE(vTabRel,'#GER#','GEO');
                                               vTabRel := REPLACE(vTabRel,'#STA#','');
        END CASE;
    END IF;
    CASE vStatoRete
        WHEN Pkg_Mago_utl.gcStatoNormale  THEN vTabRel := REPLACE(vTabRel,'#STA#','_SN');
        WHEN Pkg_Mago_utl.gcStatoAttuale  THEN vTabRel := REPLACE(vTabRel,'#STA#','_SA');
    END CASE;

    IF vNum > 0 THEN
        IF NVL(pTipologiaRete,TO_CHAR(Pkg_Mago_utl.gcNullNumCode)) = TO_CHAR(Pkg_Mago_utl.gcNullNumCode) THEN
            vRet := 9;
            vSumRet := gIDReteAT + gIDReteMT + gIDReteBT;
        ELSE
            vRet := 0;
            IF vReteAT <> 0 THEN
                IF INSTR(pTipologiaRete,Pkg_Mago_utl.gcTipReteAT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteAT)) THEN
                    vRet := vRet + gIDReteAT;
                END IF;
            END IF;
            IF vReteMT <> 0 THEN
                IF INSTR(pTipologiaRete,Pkg_Mago_utl.gcTipReteMT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteMT)) THEN
                    vRet := vRet + gIDReteMT;
                END IF;
            END IF;
            IF vReteBT <> 0 THEN
                IF INSTR(pTipologiaRete,Pkg_Mago_utl.gcTipReteBT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteBT)) THEN
                    vRet := vRet +gIDReteBT;
                END IF;
            END IF;
            IF vRet > 0 THEN
                vSumRet := vRet;
            END IF;
        END IF;

        IF pTipiCliente IS NOT NULL THEN
            vSumPrd := 0;
            IF INSTR(pTipiCliente,Pkg_Mago_utl.gcClienteProdPuro) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdPuro;
            END IF;
            IF INSTR(pTipiCliente,Pkg_Mago_utl.gcClienteAutoProd) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdCliente;
            END IF;
            IF INSTR(pTipiCliente,Pkg_Mago_utl.gcClientePrdNonDeterm) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdNonDeterm;
            END IF;
            IF INSTR(pTipiCliente,Pkg_Mago_utl.gcClientePrdNonApplic) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdNonApplic;
            END IF;
        END IF;
        IF vTab = 1 THEN
            IF CheckDate(1) = 0 THEN
                vRet := 0;
            END IF;
        ELSE
            IF CheckDate(2) = 0 THEN
                vRet := 0;
            END IF;
        END IF;

    END IF;

    IF vTipEle = Pkg_Mago_utl.gcSbarraMT THEN
        vSqlTipoPadre := REPLACE(vSqlTipoPadre,'#TABREL#',vTabRel);
        EXECUTE IMMEDIATE vSqlTipoPadre INTO vTipElePadre USING pCodElem, pDataRif;
        IF vTipElePadre IN (Pkg_Mago_utl.gcSecondarioDiTrasf,Pkg_Mago_utl.gcTerziarioDiTrasf) THEN
            NULL;  -- vecchia modalita'. Non faccio nulla
        ELSE
            -- Sbarra MT di Centro Satellite o di altra CP ?
            SELECT RIF_ELEMENTO INTO vRifEle --In RIF_ELEMENTO per Sbarra MT c'e' il cod gest della CP fisica
              FROM ELEMENTI_DEF
             WHERE COD_ELEMENTO = pCodElem
               AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
            vGstCP := GetGestElemento(GetElementoPadre(pCodElem,Pkg_Mago_utl.gcCabinaPrimaria,pDataRif,pOrganizzaz,pStatoRete));
            IF vRifEle <> vGstCP THEN
                -- La Sbarra MT appartiene fisicamente ad alta Cabina Primaria o Centro Satellite
                -- verifico quindi se CP fisica � centro satellite (FLAG = 1)
                SELECT FLAG INTO vFlag
                  FROM ELEMENTI E
                 INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
                 WHERE E.COD_GEST_ELEMENTO = vRifEle
                   AND pDataRif BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE;
                IF NVL(vFlag,0) = PKG_UtlGlb.gkFlagON THEN
                    vNum := 1; -- sbarra di Centro Satellite associata a Montante di linea MT di altra CP
                ELSE
                    vNum := 2; -- sbarra di Cabina Primaria associata a Montante di linea MT di altra CP
                END IF;
                vRet := vRet + (vNum*100);  -- La Sbarra MT � fisicamente appartenente ad altra CP (vNum = 2) o Centro Satellite (vNum = 1)
            END IF;
        END IF;
    END IF;

    -- verifica se foglia

    IF vTipEle = Pkg_Mago_utl.gcTrasformMtBt THEN
        -- Il dettaglio di trasformatore Mt/Bt e' nascosto;
        -- uso query differente per eliminare eventuali tipi elementi da nascondere
        -- e visualizzare eventuali altre dipendenze
        vSqlFoglia := vSqlFoglia2;
    END IF;

    IF vCS = PKG_UtlGlb.gkFlagON THEN
        vSqlFoglia := REPLACE(vSqlFoglia,'#COND_GEO_DGF#','AND FLAG_GEO IN (CASE '||pOrganizzaz||' WHEN '||Pkg_Mago_utl.gcOrganizzazGEO||' THEN 1 ELSE 0 END, 2)');
    ELSE
        vSqlFoglia := REPLACE(vSqlFoglia,'#COND_GEO_DGF#', '');
    END IF;

    vSqlFoglia := REPLACE(vSqlFoglia,'#TABREL#', vTabRel);
    EXECUTE IMMEDIATE vSqlFoglia INTO vNum USING pCodElem, pDataRif;

    IF (vNum = 0) OR (vTipEle = Pkg_Mago_utl.gcTrasformMtBt) THEN
        vRet := vRet + 10;
    END IF;

    RETURN vRet;  -- ritorna il flag

END GetElemInfo;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetElemTooltip    (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataRif        IN DATE,
                             pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                             pElemInfo       IN INTEGER,
                             pTipologiaRete  IN VARCHAR2,
                             pTipiCliente    IN VARCHAR2,
                             pOrganizzaz     IN NUMBER,
                             pStatoRete      IN INTEGER) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la stringa di tooltip prevista per il tipo elementi
-----------------------------------------------------------------------------------------------------------*/
    vTipElem     TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipElem;
    vElemInfo    INTEGER := pElemInfo;
    vElemTooltip VARCHAR2(4000);
BEGIN
    IF vTipElem IS NULL THEN
        BEGIN
            SELECT COD_TIPO_ELEMENTO INTO vTipElem FROM ELEMENTI WHERE COD_ELEMENTO = pCodElem;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN RETURN NULL;
        END;
    END IF;
    CASE vTipElem
        WHEN Pkg_Mago_utl.gcSbarraMT THEN
            IF vElemInfo IS NULL THEN
                vElemInfo := GetElemInfo(pCodElem,pDataRif,pTipologiaRete,pTipiCliente,pOrganizzaz,pStatoRete);
            END IF;
            IF SUBSTR(LPAD(vElemInfo,20,'0'),-3,1) <> '0' THEN
                SELECT E.COD_GEST_ELEMENTO||'  '||D.NOME_ELEMENTO
                  INTO vElemTooltip
                  FROM ELEMENTI_DEF R
                 INNER JOIN ELEMENTI     E ON E.COD_GEST_ELEMENTO = R.RIF_ELEMENTO
                 INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO
                                          AND pDataRif BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                 WHERE R.COD_ELEMENTO = pCodElem
                   AND pDataRif BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE;
                 IF SUBSTR(LPAD(vElemInfo,20,'0'),-3,1) = '1' THEN
                    vElemTooltip := 'CSAT.  ' || vElemTooltip;
                 ELSE
                    vElemTooltip := 'CP.  ' || vElemTooltip;
                 END IF;
            END IF;
        ELSE NULL;
    END CASE;
    RETURN vElemTooltip;
END GetElemTooltip;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE,
                               pStatoRete     IN INTEGER DEFAULT Pkg_Mago_utl.gcStatoAttuale,
                               pDisconnect    IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con il dettaglio dei codici gestionali ricevuti
-----------------------------------------------------------------------------------------------------------*/

    vCodList  GTTD_VALORI_TEMP.TIP%TYPE := 'CG_LIST';
    vFlgNull  NUMBER(1) := -1;

    vSql      VARCHAR2(1000) :=
                'SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,'                              ||
                       'COD_TIPO_ELEMENTO,CODIFICA_ST_TIPO_ELEMENTO '                               ||
                  'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,RIF_ELEMENTO,NOME_ELEMENTO,'         ||
                               'COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO '           ||
                          'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,RIF_ELEMENTO,NOME_ELEMENTO,' ||
                                       'E.COD_TIPO_ELEMENTO,'                                       ||
                                       'ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO '              ||
                                                'ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD ' ||
                                  'FROM ELEMENTI E '                                                ||
                                 'INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO) '                   ||
                                 'WHERE COD_GEST_ELEMENTO IN (SELECT ALF1 COD_GEST_ELEMENTO '       ||
                                                               'FROM GTTD_VALORI_TEMP '             ||
                                                              'WHERE TIP = '''||vCodList||''')'     ||
                                ') '                                                                ||
                         'INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO) '                       ||
                          'WHERE ORD = 1 '                                                          ||
                       ') '                                                                         ||
                 'INNER JOIN (SELECT g.*, #DATAELECALC# data_disattivazione_calc FROM GERARCHIA_IMP_#STATO# G ) USING (COD_ELEMENTO) ' ||
                 'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                 ||
                   'ORDER BY COD_GEST_ELEMENTO' ;

BEGIN

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vCodList;

    Pkg_Mago_utl.TrattaListaCodici(pGestElem,vCodList, vFlgNull); -- non gestito il flag

    IF pStatoRete = Pkg_Mago_utl.gcStatoNormale THEN
        vSql := REPLACE(vSql,'#STATO#','SN');
    ELSE
        vSql := REPLACE(vSql,'#STATO#','SA');
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    OPEN pRefCurs FOR vSql USING NVL(pData,SYSDATE);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElementsRegistry'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetElementsRegistry;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL,
                             pElemTypes      IN VARCHAR2 DEFAULT NULL,
                             pCompilaInfo    IN INTEGER  DEFAULT Pkg_Mago_utl.gcON,
                             pCalcOutCodGest IN INTEGER  DEFAULT 1,
                             PDisconnect IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con la struttura degli elementi all'istante richiesto (da codice elemento )
-----------------------------------------------------------------------------------------------------------*/
    vTipTmp        GTTD_VALORI_TEMP.TIP%TYPE := 'TipEleLst0';
    vElemento      ELEMENTI.COD_ELEMENTO%TYPE;
    vGestionale    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vGerarchia     INTEGER;
    vLivello       INTEGER;
    vElemTypeLst   PKG_UtlGlb.t_SplitTbl := PKG_UtlGlb.t_SplitTbl();
    vTipologiaRete VARCHAR2(100) := pTipologiaRete;
    vDataRif       DATE := GetFETreeRefDate(pDataDa,pDataA);
    vSeparatore    CHAR(1);
    vStatoRete     INTEGER := pStatoRete;

    vSql1 VARCHAR2(2000) :=
          'SELECT COD_ELEMENTO,#FLAG_INFO# FLAGS,'                                                                                ||
                 'NVL(SUBSTR(A.COD_GEST_ELEMENTO,INSTR(A.COD_GEST_ELEMENTO,'''||vSeparatore||''')+1),'                            ||
                                                                              'A.COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,'           ||
                 'NVL(NOME_ELEMENTO,'' '') NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG '         ||
            'FROM (SELECT E.COD_ELEMENTO,E.NOME_ELEMENTO,COD_GEST_ELEMENTO,'                                                      ||
                         'E.COD_TIPO_ELEMENTO,E.CODIFICA_ST,LEVEL liv,E.FLAG '                                                    ||
                    'FROM (SELECT R.COD_ELEMENTO_PADRE,R.COD_ELEMENTO,E.COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,'                   ||
                                 'T.CODIFICA_ST,NOME_ELEMENTO,E.FLAG, '                                                           ||
                            'FROM (SELECT R.COD_ELEMENTO_PADRE,R.COD_ELEMENTO_FIGLIO COD_ELEMENTO '                               ||
                                    'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA# R) R ' ||
                                   'WHERE :pDa BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE_CALC '                        || --vDataRif
                                   '#COND_GEO_DGF# '                                                                              ||
                                  ') R'                                                                                           ||
                            'INNER JOIN '                                                                                         ||
                                 '(SELECT E.COD_ELEMENTO,D.NOME_ELEMENTO,'                                                        ||
                                         'CASE '||pCalcOutCodGest||' WHEN 1 '                                                     ||
                                                    'THEN PKG_Elementi.CalcOutCodGest(E.COD_GEST_ELEMENTO,'                       ||
                                                                                     ':dt,E.COD_TIPO_ELEMENTO) '                  || --vDataRif
                                                    'ELSE E.COD_GEST_ELEMENTO '                                                   ||
                                         'END COD_GEST_ELEMENTO '                                                                 ||
                                         'D.COD_TIPO_ELEMENTO,FLAG  '                                                             ||
                                    'FROM ELEMENTI_DEF D '                                                                        ||
                                   'INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO) '                                  ||
                                   'WHERE :pDa BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE '                             || --vDataRif
                                    'AND CASE WHEN E.COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcCabinaPrimaria||''' THEN '               ||
                                                'CASE WHEN D.FLAG = '||PKG_UtlGlb.gkFlagON||' '                                   ||
                                                        'THEN 0 '                                                                 ||
                                                        'ELSE 1 '                                                                 ||
                                                'END '                                                                            ||
                                             'ELSE 1 '                                                                            ||
                                        'END = 1 '                                                                                ||
                                 ') E ON (E.COD_ELEMENTO = R.COD_ELEMENTO) '                                                      ||
                           'INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO) '                           ||
                           'WHERE GER_#GER# <> 0 '                                                                                ||
                             'AND NASCOSTO <> 1 '                                                                                 ||
                         ') E START WITH COD_ELEMENTO_PADRE = :ele '                                                              || --vElemento
                           'CONNECT BY NOCYCLE PRIOR E.COD_ELEMENTO = COD_ELEMENTO_PADRE '                                                ||
                 ') A '                                                                                                           ||
           'WHERE liv = :liv ';                                                                                                      --vLivello
    vSql2 VARCHAR2(2000) :=
          'SELECT COD_ELEMENTO_FIGLIO COD_ELEMENTO,#FLAG_INFO# FLAGS,'                                                            ||
                 'NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,'''||vSeparatore||''')+ 1),'                               ||
                                                                                'COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,'           ||
                 'NVL(NOME_ELEMENTO,'' '') NOME_ELEMENTO,'                                                                        ||
                 'E.COD_TIPO_ELEMENTO,T.CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,E.FLAG '                                            ||
            'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA# R ) R '                        ||
           'INNER JOIN (SELECT D.COD_ELEMENTO,NOME_ELEMENTO,'                                                                     ||
                         'CASE '||pCalcOutCodGest||' WHEN 1 THEN PKG_Elementi.CalcOutCodGest(E.COD_GEST_ELEMENTO,'                ||
                                                                                            ':dt,E.COD_TIPO_ELEMENTO) '           || --vDataRif
                                                    'ELSE E.COD_GEST_ELEMENTO '                                                   ||
                         'END COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,D.FLAG '                                                      ||
                         'FROM ELEMENTI_DEF D '                                                                                   ||
                        'INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO) '                                             ||
                        'WHERE :pDa BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE '                                        || --vDataRif
                          'AND CASE WHEN E.COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcCabinaPrimaria||''' THEN '                         ||
                                      'CASE WHEN D.FLAG = '||PKG_UtlGlb.gkFlagON||' '                                             ||
                                              'THEN 0 '                                                                           ||
                                              'ELSE 1 '                                                                           ||
                                      'END '                                                                                      ||
                                   'ELSE 1 '                                                                                      ||
                              'END = 1 '                                                                                          ||
                       ') E ON E.COD_ELEMENTO=R.COD_ELEMENTO_FIGLIO '                                                             ||
           'INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO=T.COD_TIPO_ELEMENTO '                                               ||
           'WHERE R.COD_ELEMENTO_PADRE=:Ele '                                                                                     || --vElemento
             '#COND_GEO_DGF# '                                                                                                    ||
             'AND NASCOSTO <> 1 '                                                                                                 ||
             'AND :pDa BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE_CALC ';                                                  --vDataRif

        PROCEDURE NullCursor AS
            BEGIN
                OPEN pRefCurs FOR SELECT NULL COD_ELEMENTO,
                                         NULL FLAGS,
                                         NULL COD_GEST_ELEMENTO,
                                         NULL NOME_ELEMENTO,
                                         NULL COD_TIPO_ELEMENTO,
                                         NULL CODIFICA_ST_TIPO_ELEMENTO,
                                         NULL FLAG
                                    FROM DUAL WHERE DUMMY = '<NO SELECT>';
            END;
        FUNCTION CompletaSql(pSql VARCHAR2, pGerarchia INTEGER, pStato INTEGER, pTooltip BOOLEAN) RETURN VARCHAR2 AS
                vSql VARCHAR2(4000) := pSql;
                vGeoCond VARCHAR2(200) := '';
            BEGIN
                CASE pGerarchia
                    WHEN cGerarchiaELE_CP THEN
                         vSql := REPLACE(vSql,'#TAB#','ECP');
                    WHEN cGerarchiaELE_CS THEN
                         vSql := REPLACE(vSql,'#TAB#','ECS');
                         vGeoCond := 'AND FLAG_GEO IN ( CASE '||pOrganizzazione||' WHEN '||Pkg_Mago_utl.gcOrganizzazGEO||' THEN 1 ELSE 0 END, 2)';
                    WHEN cGerarchiaGEO    THEN vSql := REPLACE(vSql,'#TAB#','GEO');
                                               vSql := REPLACE(vSql,'#STA#','');
                    WHEN cGerarchiaAMM    THEN vSql := REPLACE(vSql,'#TAB#','AMM');
                                               vSql := REPLACE(vSql,'#STA#','');
                END CASE;
                IF (pGerarchia = cGerarchiaELE_CP) OR (vGerarchia = cGerarchiaELE_CS) THEN
                    CASE vStatoRete
                        WHEN Pkg_Mago_utl.gcStatoNormale    THEN vSql := REPLACE(vSql,'#STA#','_SN');
                        WHEN Pkg_Mago_utl.gcStatoAttuale    THEN vSql := REPLACE(vSql,'#STA#','_SA');
                    END CASE;
                END IF;
                vSql := REPLACE(vSql,'#COND_GEO_DGF#',vGeoCond);
                IF pTooltip THEN
                    vSql := 'SELECT COD_ELEMENTO,FLAGS,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,CODIFICA_ST_TIPO_ELEMENTO,' ||
                                   'PKG_Elementi.GetElemTooltip(COD_ELEMENTO,:dt,COD_TIPO_ELEMENTO,FLAGS,:tr,:tp,:org,:st) TOOLTIP,FLAG FROM ('||vSql||')';
                ELSE
                    vSql := 'SELECT COD_ELEMENTO,FLAGS,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,CODIFICA_ST_TIPO_ELEMENTO,' ||
                                   'NULL TOOLTIP,FLAG FROM ('||vSql||')';
                END IF;
                RETURN vSql;
            END;
BEGIN

    --InitMagoSession;
    IF Pkg_Mago_utl.IsMagoSTM AND pOrganizzazione <> Pkg_Mago_utl.gcOrganizzazELE THEN
        vStatoRete  := Pkg_Mago_utl.gcStatoAttuale;
    END IF;

    IF Pkg_Mago_utl.IsMagoDGF THEN
        vSeparatore := CHR(11);
    ELSE
        vSeparatore := Pkg_Mago_utl.cSeparatore;
    END IF;

    IF NVL(pCodElem,Pkg_Mago_utl.gcNullNumCode) = Pkg_Mago_utl.gcNullNumCode THEN
        vElemento := GetElementoBase;
        IF Pkg_Mago_utl.IsNazionale THEN
            --vCodEleTmp('GetElements - Nazionale ');
            OPEN pRefCurs FOR
                SELECT COD_ELEMENTO,FLAGS,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,CODIFICA_ST_TIPO_ELEMENTO,
                       PKG_Elementi.GetElemTooltip(COD_ELEMENTO,vDataRif,COD_TIPO_ELEMENTO,FLAGS,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete) TOOLTIP,
                       FLAG
                  FROM (SELECT COD_ELEMENTO,PKG_Elementi.GetElemInfo(COD_ELEMENTO,vDataRif,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete) FLAGS,
                               NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,vSeparatore) + 1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,NOME_ELEMENTO,
                               E.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG
                          FROM (SELECT D.COD_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,FLAG,NOME_ELEMENTO,COD_TIPO_FONTE
                                  FROM ELEMENTI_DEF D
                                 INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO)
                                WHERE vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                               ) E
                         INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO=T.COD_TIPO_ELEMENTO
                         WHERE COD_ELEMENTO = vElemento
                       );
            RETURN;
        ELSE
            --PRINT('GetElements - Non Nazionale ');
            vSql1 := 'SELECT A.COD_ELEMENTO,PKG_Elementi.GetElemInfo(A.COD_ELEMENTO,:dt,:tr,:tp,:org,:st) FLAGS,' ||
                            'NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,'''||vSeparatore||''')+1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,' ||
                            'NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG ' ||
                       'FROM (SELECT COD_ELEMENTO_FIGLIO COD_ELEMENTO, COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO ' ||
                               'FROM REL_ELEMENTI_#TAB##STA# R ' ||
                              'INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO ' ||
                              'WHERE :dt BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE ' ||
                                 'AND COD_ELEMENTO_PADRE = :el ' ||
                                 '#COND_GEO_DGF# ' ||
                            ') A ' ||
                      'INNER JOIN ELEMENTI_DEF D ON (A.COD_ELEMENTO = D.COD_ELEMENTO) ' ||
                      'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = A.COD_TIPO_ELEMENTO ' ||
                      'WHERE :dt BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE ' ||
                      'ORDER BY NVL(FLAG,-1) DESC, NLSSORT(NOME_ELEMENTO,''NLS_SORT=BINARY''), COD_GEST_ELEMENTO';
            GetGerarchiaID (vElemento, pOrganizzazione, vGerarchia, vLivello);
            vSql1 := CompletaSql(vSql1,vGerarchia,vStatoRete,TRUE);
            OPEN pRefCurs FOR vSql1
                        USING vDataRif, pTipologiaRete, pTipoClie, pOrganizzazione, vStatoRete,
                              vDataRif, pTipologiaRete, pTipoClie, pOrganizzazione, vStatoRete, vDataRif, vElemento, vDataRif;
            RETURN;
        END IF;
    ELSE
        vElemento := pCodElem;
    END IF;
    BEGIN
        GetGerarchiaID (vElemento, pOrganizzazione, vGerarchia, vLivello);
        --PRINT('GetElements - GetGerarchiaID = '||vGerarchia||'  -  Livello = '||vLivello||'  per elemento '||vElemento);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            --PRINT('GetElements - GetGerarchiaID non deterrminabile - Elemento = '||vElemento);
            NullCursor;
            OPEN pRefCurs FOR SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM DUAL WHERE DUMMY = '<NO SELECT>';
            RETURN;
    END;
    IF (vGerarchia = cGerarchiaGEO_CS) OR (vGerarchia = cGerarchiaAMM_CS) THEN
        vGerarchia := cGerarchiaELE_CS;
    END IF;
    IF pElemTypes IS NOT NULL THEN
        DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vTipTmp;
        vElemTypeLst := PKG_UtlGlb.SplitString(TRIM(pElemTypes),',');
        FOR i IN vElemTypeLst.FIRST .. vElemTypeLst.LAST LOOP
            INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1) VALUES (vTipTmp,vElemTypeLst(1));
        END LOOP;
    END IF;
    IF NVL(vLivello,1) = 1 THEN
        vSql1 := vSql2;
    END IF;

    IF pCompilaInfo = Pkg_Mago_utl.gcON THEN
        vSql1 := CompletaSql(vSql1,vGerarchia,vStatoRete,TRUE);
    ELSE
        vSql1 := CompletaSql(vSql1,vGerarchia,vStatoRete,FALSE);
    END IF;

    IF pElemTypes IS NOT NULL THEN
        vSql1 := 'SELECT A.* FROM (' || vSql1 ||
                                  ') A INNER JOIN GTTD_VALORI_TEMP ON ALF1 = COD_TIPO_ELEMENTO ' ||
                          'WHERE TIP = ''#TIP#'' ';
        vSql1 := REPLACE(vSql1,'#TIP#',vTipTmp);
    END IF;
    IF pCompilaInfo = Pkg_Mago_utl.gcON THEN
        vSql1 := REPLACE(vSql1,'#FLAG_INFO#','PKG_Elementi.GetElemInfo(COD_ELEMENTO,:dt,:tr,:tp,:org,:st)');
    ELSE
        vTipologiaRete := TO_CHAR(Pkg_Mago_utl.gcNullNumCode);
        vSql1 := REPLACE(vSql1,'#FLAG_INFO#',':TR');
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql1 := REPLACE (vSql1, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql1:= REPLACE (vSql1, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;
    --vSql1 := 'SELECT * FROM ('||vSql1||') ' ||
    --         ' WHERE CASE WHEN '||PKG_UtlGlb.BooleanToFlag(Pkg_Mago_utl.IsMagoSTM)||' = 0 THEN 1 '     ||
    --                     'WHEN COD_TIPO_ELEMENTO NOT IN ('''||Pkg_Mago_utl.gcTrasformMtBt||''','       ||
    --                                                    ''''||Pkg_Mago_utl.gcClienteAT||''','          ||
    --                                                    ''''||Pkg_Mago_utl.gcClienteMT||''','          ||
    --                                                    ''''||Pkg_Mago_utl.gcClienteBT||''')  THEN 1 ' ||
    --                     'ELSE CASE WHEN FLAGS >= 10 THEN FLAGS - 10 '                             ||
    --                               'ELSE FLAGS '                                                   ||
    --                          'END '                                                               ||
    --                'END <> 0 ';
    IF NVL(vLivello,1) = 1 THEN
        vSql1 := vSql1 || ' ORDER BY CASE NVL(COD_ELEMENTO,0)WHEN 0 THEN 1 ELSE 0 END, NLSSORT(UPPER(NOME_ELEMENTO),''NLS_SORT=BINARY''),COD_GEST_ELEMENTO ';
        IF pCompilaInfo = Pkg_Mago_utl.gcON THEN
            OPEN pRefCurs FOR vSql1 USING vDataRif,vTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete,
                                          vDataRif,vTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete,vDataRif,vDataRif,vElemento,vDataRif;
        ELSE
            OPEN pRefCurs FOR vSql1 USING TO_NUMBER(vTipologiaRete),vDataRif,vDataRif,vElemento,vDataRif;
        END IF;
    ELSE
        CASE vGerarchia
            WHEN cGerarchiaGEO    THEN vSql1 := REPLACE(vSql1,'#GER#','GEO');
            WHEN cGerarchiaAMM    THEN vSql1 := REPLACE(vSql1,'#GER#','AMM');
            WHEN cGerarchiaELE_CP THEN vSql1 := REPLACE(vSql1,'#GER#','ECP');
            WHEN cGerarchiaELE_CS THEN vSql1 := REPLACE(vSql1,'#GER#','ECS');
        END CASE;
        vSql1 := vSql1 || ' ORDER BY NLSSORT(NOME_ELEMENTO,''NLS_SORT=BINARY''),COD_GEST_ELEMENTO ';
        IF pCompilaInfo = Pkg_Mago_utl.gcON THEN
            OPEN pRefCurs FOR vSql1 USING vDataRif,vTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete,
                                          vDataRif,vTipologiaRete,pTipoClie,vDataRif,vDataRif,vDataRif,vElemento,vLivello;
        ELSE
            OPEN pRefCurs FOR vSql1 USING vTipologiaRete,vDataRif,vDataRif,vDataRif,vElemento,vLivello;
        END IF;
    END IF;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt('Codice Elemento: '||NVL(TO_CHAR(pCodElem),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Pariodo Selez. : '||Pkg_Mago_utl.StdOutDate(pDataDa)||' - '||Pkg_Mago_utl.StdOutDate(pDataA),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Data Riferim.  : '||Pkg_Mago_utl.StdOutDate(vDataRif),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Organizzazione : '||NVL(TO_CHAR(pOrganizzazione),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('StatoRete      : '||NVL(TO_CHAR(pStatoRete),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('TipologiaRete  : '||NVL(pTipologiaRete,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Tipi Produtt.  : '||NVL(pTipoClie,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Tipi Elemento  : '||NVL(pElemTypes,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElements'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         NullCursor;
         RETURN;
END GetElements;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pGestElem       IN VARCHAR2,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Localizza un codice gestionale nella gerarchia/stato indicate e restituisce la catena di elementi
    necessaria a raggiungere l'elemento stesso
-----------------------------------------------------------------------------------------------------------*/

    vSearchStr  VARCHAR2(100) := pGestElem;
    vDataRif    DATE := GetFETreeRefDate(pDataDa,pDataA);
    vTrovato    BOOLEAN := FALSE;
    vGerarchia  INTEGER;
    vLivello    INTEGER;
    vTipTmp     GTTD_VALORI_TEMP.TIP%TYPE := 'FindElem';
    vLiv        INTEGER;
    vNum        NUMBER;
    vTipEleRoot TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vCodEleRoot ELEMENTI.COD_ELEMENTO%TYPE;
    vCodInizio  ELEMENTI.COD_ELEMENTO%TYPE;
    vCodEleTmp  ELEMENTI.COD_ELEMENTO%TYPE;
    vFlg        INTEGER;
    vGst        ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNom        ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vRif        ELEMENTI_DEF.RIF_ELEMENTO%TYPE;
    vTip        TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTst        TIPI_ELEMENTO.CODIFICA_ST%TYPE;
    vSeq        INTEGER := 0;
    vTTp        VARCHAR2(4000);
    vFlagEse    ELEMENTI_DEF.FLAG%TYPE;
    vOrd        GTTD_VALORI_TEMP.NUM5%TYPE;
    vStatoRete  INTEGER := pStatoRete;
    vSeparatore CHAR(1);
    vRel1       VARCHAR2(200) := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_#TAB##STA1# '                                        ||
                                  'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE';              --vDataRif
    vRel2       VARCHAR2(400) := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_#TAB##STA1# '                                        ||
                                   'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '          || --vDataRif
                                  'UNION ALL '                                                             ||
                                 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_ECS#STA2# '                                          ||
                                  'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '           || --vDataRif
                                    'AND FLAG_GEO IN ( CASE '||pOrganizzazione||' '                        ||
                                                        'WHEN '||Pkg_Mago_utl.gcOrganizzazGEO||' THEN 1 '      ||
                                                        'ELSE 0 '                                          ||
                                                       'END, 2)';
    vSqlSelElem VARCHAR2(800) := 'SELECT A.COD_ELEMENTO_PADRE, A.LIV, E.COD_TIPO_ELEMENTO '                ||
                                   'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO, LEVEL liv '       ||
                                           'FROM (#REL#) '                                                 ||
                                                'START WITH COD_ELEMENTO_FIGLIO=:ele '                     || --vCodInizio
                                              'CONNECT BY NOCYCLE PRIOR COD_ELEMENTO_PADRE = COD_ELEMENTO_FIGLIO ' ||
                                        ') A '                                                             ||
                                  'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = COD_ELEMENTO_PADRE '          ||
                                  'INNER JOIN TIPI_ELEMENTO T ON(E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO) '; --||
                                  --' ORDER BY LIV ';
    vSqlInsTemp VARCHAR2(800) := 'INSERT INTO GTTD_VALORI_TEMP ( TIP, NUM1, NUM2) '                        ||
                                   'SELECT '''||vTipTmp||'2'','                                            ||
                                          'ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY NUM1 DESC) ORD,'     ||
                                          'NUM2 COD_ELEMENTO '                                             ||
                                     'FROM (SELECT TIP,NUM1,NUM2 FROM GTTD_VALORI_TEMP '                   ||
                                            'WHERE TIP = '''||vTipTmp||'1'')  A '                          ||
                                     'LEFT OUTER JOIN (SELECT * FROM REL_ELEMENTI_#GER##STA1# '            ||
                                                       'WHERE :pDa BETWEEN NVL(DATA_ATTIVAZIONE,:pDa) '    || --vDataRif,vDataRif
                                                                      'AND NVL(DATA_DISATTIVAZIONE,:pDa) ' || --vDataRif
                                                      ') ON COD_ELEMENTO_FIGLIO = NUM2 '                   ||
                                     'LEFT OUTER JOIN ELEMENTI E ON E.COD_ELEMENTO = COD_ELEMENTO_PADRE '  ||
                                 'ORDER BY TIP, NUM1 DESC, NUM2 ';

    vGeoCond VARCHAR2(200);

    PROCEDURE OpenCursor (pCurs OUT PKG_UtlGlb.t_query_cur, pTipTmp IN VARCHAR2) AS
            vSql VARCHAR2(700) := 'SELECT NUM2 COD_ELEMENTO, NUM3 FLAGS, '                                             ||
                                         'CASE '                                                                       ||
                                            'WHEN ALF5 IS NOT NULL THEN ALF5 '                                         ||
                                            'ELSE SUBSTR(ALF1,INSTR(ALF1,'''||vSeparatore||''')+1) '                   ||
                                         'END COD_GEST_ELEMENTO, '                                                     ||
                                         'ALF2 NOME_ELEMENTO,'                                                         ||
                                         'ALF3 COD_TIPO_ELEMENTO,'                                                     ||
                                         'ALF4 CODIFICA_ST_TIPO_ELEMENTO,'                                             ||
                                         'NUM1 - :liv LIVELLO, :org ORGANIZZAZIONE,'                                   || -- decr livello, pOrganizzazione
                                         'ALF6 TOOLTIP '                                                               ||
                                    'FROM (SELECT A.*,SUM(CASE ALF3 WHEN :te THEN 1 ELSE 0 END '                       || -- tipo elemento root
                                                        ') OVER (PARTITION BY TIP ) ROOT '                             ||
                                                'FROM (SELECT * FROM GTTD_VALORI_TEMP '                                ||
                                                      '#UNION_NAZ#) A '                                                || -- pTipTmp (per nazionale)
                                               'WHERE TIP = NVL(:tip,''<null>'')'                                      || -- pTipTmp
                                         ')'                                                                           ||
                                   'WHERE ROOT = 1 '                                                                   ||
                                   'ORDER BY CASE WHEN COD_TIPO_ELEMENTO = :te THEN 0 ELSE 1 END,NVL(NUM5,-1) DESC,NUM4' ;
        BEGIN
            IF Pkg_Mago_utl.isNazionale THEN
                -- aggiunge root fittizia che sara' tolta del FE
                vSql := REPLACE(vSql,'#UNION_NAZ#','UNION ALL SELECT :tip,0,0,''?'',''?'',''-'||vSeparatore||'-'',0,-1 FROM DUAL');
                --PRINT('Nazionale - org='|| pOrganizzazione||' - TipEleRoot='||vTipEleRoot||' - TipTmp='||pTipTmp||CHR(10)||vSql);
                OPEN pCurs FOR vSql USING 0, pOrganizzazione, vTipEleRoot, pTipTmp, pTipTmp, vTipEleRoot;
            ELSE
                vSql := REPLACE(vSql,'#UNION_NAZ#','');
                --PRINT('NON Nazionale - org='|| pOrganizzazione||' - TipEleRoot='||vTipEleRoot||' - TipTmp='||pTipTmp||CHR(10)||vSql);
                OPEN pCurs FOR vSql USING 1, pOrganizzazione, vTipEleRoot, pTipTmp, vTipEleRoot;
            END IF;
        END;
    PROCEDURE AddElem (pLiv NUMBER, pCod ELEMENTI.COD_ELEMENTO%TYPE) AS
        BEGIN
            INSERT INTO GTTD_VALORI_TEMP (TIP,NUM1,NUM2) VALUES (vTipTmp||'1',pLiv,pCod);
        END;

    PROCEDURE GetFigli (pLiv NUMBER, pCod ELEMENTI.COD_ELEMENTO%TYPE,pTmp GTTD_VALORI_TEMP.TIP%TYPE) AS
        vCur  PKG_UtlGlb.t_query_cur;
        vEle  ELEMENTI.COD_ELEMENTO%TYPE;
        vCod  ELEMENTI.COD_ELEMENTO%TYPE;
        vTmp  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
        BEGIN
            BEGIN
                --PRINT('GetFigli - tento di recuperare il livello '||pLiv||' per il cod '|| pCod||' - tmp='||pTmp);
                SELECT NUM2 INTO vCod FROM GTTD_VALORI_TEMP WHERE NUM1 = pLiv AND TIP = pTmp;
                --PRINT('GetFigli - a livello '||pLiv||' recupero il cod '|| vCod);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    --PRINT('GetFigli - non ho recuperato il livello '||pLiv||' per il cod '|| pCod||' - tmp='||pTmp);
                    RETURN;
            END;
            --PRINT('GetFigli - GetElements - Cod='||pCod);
            GetElements(vCur,pCod,vDataRif,vDataRif,pOrganizzazione,vStatoRete,pTipologiaRete,pTipoClie,NULL,Pkg_Mago_utl.gcON,0);
            --PRINT('GetFigli - GetElements - fine');
            LOOP
                FETCH vCur INTO vEle,vFlg,vGst,vNom,vTip,vTst,vTTp,vFlagEse;
                EXIT WHEN vCur%NOTFOUND;
                vSeq := vSeq + 1;
                IF vTip = Pkg_Mago_utl.gcEsercizio THEN
                    vOrd := vFlagEse;
                END IF;
                --PRINT(vSeq||' - ele='||vEle||' - tip='||vTip||' - tst='||vTst||' - gst='||vGst||' - pGestElem='||pGestElem);
                IF Pkg_Mago_utl.IsMagoDGF THEN
                    vTmp := NVL(SUBSTR(vSearchStr,INSTR(vSearchStr,vSeparatore)+1),vSearchStr);
                ELSE
                    vTmp := vSearchStr;
                END IF;
                IF vGst = vTmp THEN
                    vTrovato := TRUE;
                    --PRINT('>>>>> TROVATO <<<<<<<');
                END IF;
                IF vEle = vCod THEN
                   --PRINT('        cerco i figli di '||vEle);
                   UPDATE GTTD_VALORI_TEMP SET NUM3 = vFlg,
                                               ALF1 = vGst,
                                               ALF2 = vNom,
                                               ALF3 = vTip,
                                               ALF4 = vTst,
                                               ALF5 = PKG_Elementi.CalcOutCodGest(SUBSTR(vGst,INSTR(vGst,vSeparatore)+1),vDataRif,vTip),
                                               ALF6 = vTTp,
                                               NUM4 = vSeq,
                                               NUM5 = vOrd
                    WHERE TIP  = pTmp
                      AND NUM2 = vEle
                      AND NUM1 = pLiv;
                    GetFigli(pLiv + 1, vEle, pTmp);
                ELSE
                   INSERT INTO GTTD_VALORI_TEMP (TIP ,NUM1,NUM2,NUM3,NUM4,NUM5,
                                                      ALF1,ALF2,ALF3,ALF4,ALF6,ALF5)
                                         VALUES (pTmp,pLiv,vEle,vFlg,vSeq,vOrd,
                                                      vGst,vNom,vTip,vTst,vTTp,PKG_Elementi.CalcOutCodGest(SUBSTR(vGst,INSTR(vGst,vSeparatore)+1),vDataRif,vTip));
                END IF;
            END LOOP;
            CLOSE vCur;
        END;

BEGIN

    IF Pkg_Mago_utl.IsMagoSTM AND pOrganizzazione <> Pkg_Mago_utl.gcOrganizzazELE THEN
        vStatoRete  := Pkg_Mago_utl.gcStatoAttuale;
    END IF;

    IF Pkg_Mago_utl.IsMagoDGF THEN
        vSeparatore := CHR(11);
    ELSE
        vSeparatore := Pkg_Mago_utl.cSeparatore;
    END IF;

    IF Pkg_Mago_utl.isNazionale THEN
        vTipEleRoot := Pkg_Mago_utl.gcNazionale;
    ELSE
        vTipEleRoot := Pkg_Mago_utl.gcCentroOperativo;
    END IF;

    BEGIN
        BEGIN
            SELECT COD_ELEMENTO INTO vCodInizio
              FROM ELEMENTI
             WHERE COD_GEST_ELEMENTO = vSearchStr;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT COD_ELEMENTO,COD_GEST_ELEMENTO
                  INTO vCodInizio, vSearchStr
                  FROM ELEMENTI
                 INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                 WHERE D.RIF_ELEMENTO = vSearchStr
                    AND CASE WHEN vTipEleRoot <> PKG_Mago.gcCentroOperativo
                    		THEN 1
                    		ELSE CASE WHEN D.FLAG = 1 THEN 1
                                            ELSE 0
                         END
                      END = 1
                   AND vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                IF Pkg_Mago_utl.IsMagoDGF AND pOrganizzazione = Pkg_Mago_utl.gcOrganizzazGEO THEN
                    BEGIN
                        -- inizio forzatura per DGF
                        -- cerca il primo elemento con codice gestionale con 2^ parte del codice ricevuto corrispondente
                        SELECT MIN(COD_GEST_ELEMENTO) INTO vSearchStr
                          FROM ELEMENTI E
                         INNER JOIN ELEMENTI_DEF D ON (D.COD_ELEMENTO = E.COD_ELEMENTO)
                         WHERE SUBSTR(E.COD_GEST_ELEMENTO,INSTR(E.COD_GEST_ELEMENTO,Pkg_Mago_utl.cSeparatore)+1) = SUBSTR(vSearchStr,INSTR(vSearchStr,Pkg_Mago_utl.cSeparatore)+1)
                           AND INSTR(E.COD_GEST_ELEMENTO,Pkg_Mago_utl.cSeparatore) > 0
                           AND vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                        -- fine  forzatura per DGF
                        vCodInizio := GetCodElemento(vSearchStr);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN vCodInizio := NULL;
                    END;
                END IF;
                IF vCodInizio IS NULL THEN
                    SELECT MIN(COD_ELEMENTO)
                      INTO vCodInizio
                      FROM (SELECT DISTINCT COD_ELEMENTO
                              FROM V_SEARCH_ELEMENTS
                             WHERE SEARCH_STRING LIKE LOWER(vSearchStr||'%')
                           )
                     HAVING COUNT(*) = 1;
                    SELECT COD_GEST_ELEMENTO
                      INTO vSearchStr
                     FROM ELEMENTI
                    WHERE COD_ELEMENTO = vCodInizio;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     OpenCursor(pRefCurs,NULL);
                     --PRINT('GetFigli - Chiudo per NO_DATA_FOUND!');
                     RETURN;
                WHEN OTHERS THEN
                    IF SQLCODE = -1422 THEN
                       OpenCursor(pRefCurs,NULL);
                       --PRINT('GetFigli - Chiudo per SQLCODE = -1422');
                       RETURN;
                    ELSE
                       RAISE;
                    END IF;
            END;
    END;
    GetGerarchiaID (vCodInizio, pOrganizzazione, vGerarchia, vLivello);
    IF (vGerarchia = cGerarchiaELE) OR (vGerarchia = cGerarchiaGEO) OR (vGerarchia = cGerarchiaAMM) THEN
        vSqlSelElem := REPLACE(vSqlSelElem,'#REL#',vRel1);
    ELSE
        vSqlSelElem := REPLACE(vSqlSelElem,'#REL#',vRel2);
    END IF;
    CASE vGerarchia
         WHEN cGerarchiaELE_CP  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'ECP');
         WHEN cGerarchiaELE_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'ECP');
         WHEN cGerarchiaGEO     THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'GEO');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','');
         WHEN cGerarchiaAMM     THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'AMM');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','');
         WHEN cGerarchiaGEO_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'GEO');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
         WHEN cGerarchiaAMM_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'AMM');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
    END CASE;

    CASE pOrganizzazione
        WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','GEO');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','GEO');
        WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','AMM');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','AMM');
        WHEN Pkg_Mago_utl.gcOrganizzazELE THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','ECP');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','ECP');
    END CASE;

    IF vStatoRete = Pkg_Mago_utl.gcStatoAttuale THEN
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','_SA');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','_SA');
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','_SA');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','_SA');
    ELSE
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','_SN');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','_SN');
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','_SN');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','_SN');
    END IF;

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP LIKE vTipTmp||'%';

    IF (vGerarchia = cGerarchiaELE) OR (vGerarchia = cGerarchiaGEO) OR (vGerarchia = cGerarchiaAMM) THEN
        OPEN pRefCurs FOR vSqlSelElem USING vDataRif, vCodInizio;
    ELSE
       OPEN pRefCurs FOR vSqlSelElem USING vDataRif, vDataRif, vCodInizio;
    END IF;
    LOOP
       FETCH pRefCurs INTO vCodEleTmp, vLiv, vTip;
       EXIT WHEN pRefCurs%NOTFOUND;
       IF vTip = vTipEleRoot THEN
          vCodEleRoot := vCodEleTmp;
       END IF;
       AddElem(vLiv,vCodEleTmp);
    END LOOP;
    CLOSE pRefCurs;

    IF vCodInizio IS NOT NULL THEN
       AddElem(0,vCodInizio);
    END IF;

    EXECUTE IMMEDIATE vSqlInsTemp USING vDataRif, vDataRif, vDataRif;
    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vTipTmp||'1';

    BEGIN
        SELECT FLAGS,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,CODIFICA_ST,
               PKG_Elementi.GetElemTooltip(COD_ELEMENTO,vDataRif,COD_TIPO_ELEMENTO,FLAGS,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete)
          INTO vFlg,vGst,vNom,vTip,vTst,vTTp
          FROM (SELECT COD_ELEMENTO,
                       PKG_Elementi.GetElemInfo(COD_ELEMENTO,vDataRif,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete) FLAGS,
                       NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,vSeparatore)+1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,
                       NOME_ELEMENTO,T.COD_TIPO_ELEMENTO,CODIFICA_ST
                  FROM (SELECT D.COD_ELEMENTO,NOME_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO
                          FROM ELEMENTI_DEF D
                         INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO)
                         WHERE vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                       ) E
                 INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO)
                 WHERE COD_ELEMENTO = (SELECT NUM2 FROM GTTD_VALORI_TEMP WHERE NUM1 = 1 AND TIP = vTipTmp||'2')
               );
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
                --PRINT('>> Chiudo per NO_DATA_FOUND!');
                OpenCursor(pRefCurs,NULL);
                RETURN;
    END;
    --PRINT('aggiorno livello 1 con - NUM3='||vFlg||'  ALF1='||vGst||'  ALF2='||vNom||'  ALF3='||vTip||'  ALF4='||vTst||'  NUM4=0');
    UPDATE GTTD_VALORI_TEMP SET NUM3=vFlg,
                                ALF1=vGst,
                                ALF2=vNom,
                                ALF3=vTip,
                                ALF4=vTst,
                                ALF6=vTTp,
                                NUM4=0
     WHERE NUM1 = 1
       AND TIP = vTipTmp||'2';  -- livello 1

    IF Pkg_Mago_utl.IsNazionale THEN
        GetFigli(2, GetElementoBase, vTipTmp||'2');
    ELSE
        GetFigli(2, vCodEleRoot, vTipTmp||'2');
    END IF;

    IF vTrovato THEN
        --PRINT('apro il cursore');
        OpenCursor(pRefCurs,vTipTmp||'2');
    ELSE
        --PRINT('elemento non trovato');
        OpenCursor(pRefCurs,NULL);
    END IF;
    --PRINT('Fine');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.FindElement'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OpenCursor(pRefCurs,NULL);
         RETURN;

END FindElement;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementList     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pNumRows        IN INTEGER  DEFAULT 100) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna un cursore riportante gli elementi che soddisfano la richiesta in Stringa di ricerca
-----------------------------------------------------------------------------------------------------------*/

    vSearchString  VARCHAR2(200) := pSearchString;
    vTabGerarchia  VARCHAR2(35)  := 'GERARCHIA_#GER##STA#';

    vSql           VARCHAR2(5000) :=
                      'SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,DES_TIPO_ELEMENTO,'              ||
                             'PKG_Elementi.GetElemInfo (COD_ELEMENTO,:dt,:tr,:tc,:org,:sta) FLAG,'                            ||
                             'CASE '                                                                                          ||
                                'WHEN COD_TIPO_ELEMENTO IN ('''||Pkg_Mago_utl.gcClienteMT||''','''||Pkg_Mago_utl.gcLineaMT||''')'     ||
                                        'THEN RIFERIMENTO '                                                                   ||
                                        'ELSE NULL '                                                                          ||
                             'END RIFERIMENTO,'                                                                               ||
                             'TOTALI COUNT '                                                                                  ||
                        'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,T.COD_TIPO_ELEMENTO,'                      ||
                                     'T.DESCRIZIONE DES_TIPO_ELEMENTO,A.RIFERIMENTO,TOTALI '                                  ||
                                'FROM (SELECT E.COD_ELEMENTO,'                                                                ||
                                             'CASE '                                                                          ||
                                                 'WHEN E.COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcLineaMT||''' '                   ||
                                                  'AND S.RIF_ELEMENTO IS NOT NULL '                                           ||
                                                    'THEN S.RIF_ELEMENTO '                                                    ||
                                                 'ELSE E.COD_GEST_ELEMENTO '                                                  ||
                                             'END COD_GEST_ELEMENTO,'                                                         ||
                                             'S.NOME_ELEMENTO,'                                                               ||
                                             'E.COD_TIPO_ELEMENTO,'                                                           ||
                                             'CASE '                                                                          ||
                                                'WHEN E.COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcLineaMT||''' '                    ||
                                                    'THEN E.COD_GEST_ELEMENTO '                                               ||
                                                    'ELSE S.RIF_ELEMENTO '                                                    ||
                                             'END RIFERIMENTO,'                                                               ||
                                             'TOTALI '                                                                        ||
                                        'FROM ELEMENTI E '                                                                    ||
                                       'INNER JOIN (SELECT COD_ELEMENTO,TIPO,TOTALI,NOME_ELEMENTO,RIF_ELEMENTO '              ||
                                                     'FROM (SELECT A.*,'                                                      ||
                                                                  'D.NOME_ELEMENTO,D.RIF_ELEMENTO,'                           ||
                                                                  'COUNT (1) OVER (PARTITION BY 1 ORDER BY 1 DESC) TOTALI '   ||
                                                             'FROM V_SEARCH_ELEMENTS A '                                      ||
                                                            'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = A.COD_ELEMENTO '       ||
                                                            'INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO '   ||
                                                                                     'AND :dt BETWEEN D.DATA_ATTIVAZIONE '    ||
                                                                                                 'AND D.DATA_DISATTIVAZIONE ' ||
                                                            'WHERE SEARCH_STRING LIKE :src '                                  ||
                                                              'AND CASE '                                                     ||
                                                                     ' WHEN EXISTS (SELECT 1 FROM #GERARCHIA# X '             ||
                                                                                    'WHERE X.COD_ELEMENTO = A.COD_ELEMENTO '  ||
                                                                                      'AND :dt BETWEEN DATA_ATTIVAZIONE '     ||
                                                                                                  'AND DATA_DISATTIVAZIONE '  ||
                                                                                   ') '                                       ||
                                                                          'THEN 1 '                                           ||
                                                                          'ELSE 0 '                                           ||
                                                                      'END = 1 '                                              ||
                                                                      'AND CASE '                                             ||
                                                                              'WHEN E.COD_TIPO_ELEMENTO = '''                 ||
                                                                                        Pkg_Mago_utl.gcCabinaPrimaria||''' THEN ' ||
                                                                                  'CASE WHEN D.FLAG = 1 THEN 0 '              ||
                                                                                                       'ELSE 1 '              ||
                                                                                  'END '                                      ||
                                                                              'ELSE 1 '                                       ||
                                                                          'END = 1 '                                          ||
                                                            'ORDER BY PKG_Elementi.GetOrderByElemType(E.COD_TIPO_ELEMENTO,'   ||
                                                                                                     'E.COD_GEST_ELEMENTO)'   ||
                                                          ') '                                                                ||
                                                    'WHERE ROWNUM <= :num '                                                   ||
                                                  ') S ON (S.COD_ELEMENTO = E.COD_ELEMENTO) '                                 ||
                                     ') A '                                                                                   ||
                               'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = A.COD_TIPO_ELEMENTO) '                    ||
                               'ORDER BY PKG_Elementi.GetOrderByElemType (COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO)';

    PROCEDURE NullCursor(pCur OUT PKG_UtlGlb.t_query_cur) AS
        BEGIN
            OPEN pCur FOR
                SELECT NULL COD_ELEMENTO,
                       NULL COD_GEST_ELEMENTO,
                       NULL NOME_ELEMENTO,
                       NULL COD_TIPO_ELEMENTO,
                       NULL DES_TIPO_ELEMENTO,
                       NULL FLAG,
                       NULL RIFERIMENTO,
                       0 COUNT
                  FROM DUAL
                 WHERE 1=2;
        END;

BEGIN
    --PKG_Logs.TraceLog('GetElementList  >'||vSearchString||'< - '||TO_CHAR(pData,'dd/mm/yyyy hh24:mi:ss')||
    --                  ' - Org='||pOrganizzazione||' - Sta='||pStatoRete||' - '||
    --                  NVL(pTipologiaRete,'<null>')||' - '||NVL(pTipoClie,'<null>'));

    IF NVL(TRIM(pSearchString),'*') = '*' THEN
        NullCursor(pRefCurs);
        RETURN;
    END IF;

    CASE pOrganizzazione
        WHEN Pkg_Mago_utl.gcOrganizzazELE THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','IMP');
        WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','AMM');
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','');
        WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','GEO');
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','');
    END CASE;
    CASE pStatoRete
        WHEN Pkg_Mago_utl.gcStatoAttuale THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','_SA');
        WHEN Pkg_Mago_utl.gcStatoNormale THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','_SN');
    END CASE;

    vSql := REPLACE(vSql,'#GERARCHIA#',vTabGerarchia);

    IF SUBSTR(vSearchString,-1,1) <> '*' THEN
        vSearchString := pSearchString || '*';
    END IF;

    vSearchString := LOWER(TRIM(SUBSTR(vSearchString,1,200)));
    vSearchString := REPLACE(vSearchString,'*','%');
    vSearchString := REPLACE(vSearchString,'?','_');
    vSearchString := REGEXP_REPLACE(vSearchString,'[^[:alnum:]:%_:]');

    OPEN pRefCurs FOR vSql USING pData,pTipologiaRete,pTipoClie,pOrganizzazione,pStatoRete,
                                 pData,vSearchString,pData,pNumRows;

EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt('DATA           : '||Pkg_Mago_utl.StdOutDate(pData),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Search STRING  : '||NVL(pSearchString,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElementList'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         NullCursor(pRefCurs);
         RETURN;

END GetElementList;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementList      (pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL)
                       RETURN t_TabElemList PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna, in formato tabellare, gli elementi che soddisfano la richiesta in Stringa do ricerca
-----------------------------------------------------------------------------------------------------------*/
   vRow t_RowElemList;
   vCur PKG_UtlGlb.t_query_cur;
BEGIN
    GetElementList(vCur,pData,pSearchString,pOrganizzazione,pStatoRete,pTipologiaRete,pTipoClie);
    LOOP
        FETCH vCur INTO vRow;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vRow);
     END LOOP;
    CLOSE vCur;
END GetElementList;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaLineare (pDataDa         IN DATE,
                                pDataA          IN DATE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pDisconnect     IN INTEGER DEFAULT 0)
                       RETURN t_TabGerarchiaLineare PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) la gerarchia lineare dell'elemento richiesto
-----------------------------------------------------------------------------------------------------------*/

    vCurGer      PKG_UtlGlb.t_query_cur;
    vStatoRete   INTEGER := pStatoRete;
    vRow         GTTD_CALC_GERARCHIA%ROWTYPE;
    vSqlGer      VARCHAR2(9999) :=
                  'SELECT COD_ELEMENTO_FIGLIO '                                                                                                              ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,1)!=0 THEN INSTR(cod_el,''!'',1,1)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,2),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,2)-1)-(INSTR(cod_el,''!'',1,1)))) liv1'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,2)!=0 THEN INSTR(cod_el,''!'',1,2)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,3),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,3)-1)-(INSTR(cod_el,''!'',1,2)))) liv2'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,3)!=0 THEN INSTR(cod_el,''!'',1,3)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,4),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,4)-1)-(INSTR(cod_el,''!'',1,3)))) liv3'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,4)!=0 THEN INSTR(cod_el,''!'',1,4)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,5),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,5)-1)-(INSTR(cod_el,''!'',1,4)))) liv4'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,5)!=0 THEN INSTR(cod_el,''!'',1,5)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,6),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,6)-1)-(INSTR(cod_el,''!'',1,5)))) liv5'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,6)!=0 THEN INSTR(cod_el,''!'',1,6)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,7),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,7)-1)-(INSTR(cod_el,''!'',1,6)))) liv6'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,7)!=0 THEN INSTR(cod_el,''!'',1,7)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,8),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,8)-1)-(INSTR(cod_el,''!'',1,7)))) liv7'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,8)!=0 THEN INSTR(cod_el,''!'',1,8)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,9),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,9)-1)-(INSTR(cod_el,''!'',1,8)))) liv8'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,9)!=0 THEN INSTR(cod_el,''!'',1,9)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,10),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,10)-1)-(INSTR(cod_el,''!'',1,9)))) liv9'    ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,10)!=0 THEN INSTR(cod_el,''!'',1,10)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,11),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,11)-1)-(INSTR(cod_el,''!'',1,10)))) liv10'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,11)!=0 THEN INSTR(cod_el,''!'',1,11)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,12),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,12)-1)-(INSTR(cod_el,''!'',1,11)))) liv11'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,12)!=0 THEN INSTR(cod_el,''!'',1,12)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,13),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,13)-1)-(INSTR(cod_el,''!'',1,12)))) liv12'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,13)!=0 THEN INSTR(cod_el,''!'',1,13)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,14),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,14)-1)-(INSTR(cod_el,''!'',1,13)))) liv13 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,14)!=0 THEN INSTR(cod_el,''!'',1,14)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,15),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,15)-1)-(INSTR(cod_el,''!'',1,14)))) liv14 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,15)!=0 THEN INSTR(cod_el,''!'',1,15)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,16),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,16)-1)-(INSTR(cod_el,''!'',1,15)))) liv15 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,16)!=0 THEN INSTR(cod_el,''!'',1,16)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,17),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,17)-1)-(INSTR(cod_el,''!'',1,16)))) liv16 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,17)!=0 THEN INSTR(cod_el,''!'',1,17)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,18),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,18)-1)-(INSTR(cod_el,''!'',1,17)))) liv17 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,18)!=0 THEN INSTR(cod_el,''!'',1,18)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,19),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,19)-1)-(INSTR(cod_el,''!'',1,18)))) liv18 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,19)!=0 THEN INSTR(cod_el,''!'',1,19)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,20),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,20)-1)-(INSTR(cod_el,''!'',1,19)))) liv19 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,20)!=0 THEN INSTR(cod_el,''!'',1,20)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,21),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,21)-1)-(INSTR(cod_el,''!'',1,20)))) liv20 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,21)!=0 THEN INSTR(cod_el,''!'',1,21)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,22),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,22)-1)-(INSTR(cod_el,''!'',1,21)))) liv21 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,22)!=0 THEN INSTR(cod_el,''!'',1,22)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,23),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,23)-1)-(INSTR(cod_el,''!'',1,22)))) liv22 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,23)!=0 THEN INSTR(cod_el,''!'',1,23)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,24),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,24)-1)-(INSTR(cod_el,''!'',1,23)))) liv23 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,24)!=0 THEN INSTR(cod_el,''!'',1,24)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,25),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,25)-1)-(INSTR(cod_el,''!'',1,24)))) liv24 ' ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,25)!=0 THEN INSTR(cod_el,''!'',1,25)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,26),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,26)-1)-(INSTR(cod_el,''!'',1,25)))) liv25 ' ||
                    'FROM (    SELECT SYS_CONNECT_BY_PATH (COD_ELEMENTO_figlio, ''!'') cod_el, COD_ELEMENTO_FIGLIO, LEVEL LIV, ROWNUM SEQ '                  ||
                                'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM '                                                                 ||
                                       ' (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA1# r ) '                             ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                                                    ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM '                                                                 ||
                                      ' (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_ECS#STA2# r ) '                                ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                                                    ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT NULL, :el FROM DUAL '                                                                                          ||
                                     ')'                                                                                                                     ||
                          'START WITH COD_ELEMENTO_PADRE IS NULL '                                                                                           ||
                          'CONNECT BY NOCYCLE PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE)';

BEGIN
    IF Pkg_Mago_utl.IsMagoSTM AND pOrganizzazione <> Pkg_Mago_utl.gcOrganizzazELE THEN
        vStatoRete  := Pkg_Mago_utl.gcStatoAttuale;
    END IF;

    CASE pOrganizzazione
        WHEN Pkg_Mago_utl.gcOrganizzazELE THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','ECP');
                CASE vStatoRete
                    WHEN Pkg_Mago_utl.gcStatoAttuale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA1#','_SA');
                        vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
                    WHEN Pkg_Mago_utl.gcStatoNormale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA1#','_SN');
                        vSqlGer := REPLACE(vSqlGer,'#STA2#','_SN');
                END CASE;
        WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','GEO');
                vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
        WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','AMM');
                vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
    END CASE;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    OPEN vCurGer FOR vSqlGer USING pDataDa, pDataA, pCodEle;
    LOOP
        FETCH vCurGer INTO vRow;
        EXIT WHEN vCurGer%NOTFOUND;
        PIPE ROW(vRow);
    END LOOP;
    CLOSE vCurGer;
    RETURN;

END LeggiGerarchiaLineare;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaLineare_save(pDataDa         IN DATE,
                                    pDataA          IN DATE,
                                    pOrganizzazione IN INTEGER,
                                    pStatoRete      IN INTEGER,
                                    pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pDisconnect     IN INTEGER DEFAULT 0)
                             RETURN t_TabGerarchiaLineare PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) la gerarchia lineare dell'elemento richiesto
    - Funzione LeggiGerarchiaLineare precedentemente in uso (fino a 1.9.a.2) sostituita da nuova versione
      (tenere per il momento)
-----------------------------------------------------------------------------------------------------------*/

    vCurGer      PKG_UtlGlb.t_query_cur;
    vStatoRete   INTEGER := pStatoRete;
    vSqlGer      VARCHAR2(2000) := 'SELECT CASE WHEN NVL(L25,-1) > 0  THEN L25 '                      ||
                                               'WHEN NVL(L24,-1) > 0  THEN L24 '                      ||
                                               'WHEN NVL(L23,-1) > 0  THEN L23 '                      ||
                                               'WHEN NVL(L22,-1) > 0  THEN L22 '                      ||
                                               'WHEN NVL(L21,-1) > 0  THEN L21 '                      ||
                                               'WHEN NVL(L20,-1) > 0  THEN L20 '                      ||
                                               'WHEN NVL(L19,-1) > 0  THEN L19 '                      ||
                                               'WHEN NVL(L18,-1) > 0  THEN L18 '                      ||
                                               'WHEN NVL(L17,-1) > 0  THEN L17 '                      ||
                                               'WHEN NVL(L16,-1) > 0  THEN L16 '                      ||
                                               'WHEN NVL(L15,-1) > 0  THEN L15 '                      ||
                                               'WHEN NVL(L14,-1) > 0  THEN L14 '                      ||
                                               'WHEN NVL(L13,-1) > 0  THEN L13 '                      ||
                                               'WHEN NVL(L12,-1) > 0  THEN L12 '                      ||
                                               'WHEN NVL(L11,-1) > 0  THEN L11 '                      ||
                                               'WHEN NVL(L10,-1) > 0  THEN L10 '                      ||
                                               'WHEN NVL(L09,-1) > 0  THEN L09 '                      ||
                                               'WHEN NVL(L08,-1) > 0  THEN L08 '                      ||
                                               'WHEN NVL(L07,-1) > 0  THEN L07 '                      ||
                                               'WHEN NVL(L06,-1) > 0  THEN L06 '                      ||
                                               'WHEN NVL(L05,-1) > 0  THEN L05 '                      ||
                                               'WHEN NVL(L04,-1) > 0  THEN L04 '                      ||
                                               'WHEN NVL(L03,-1) > 0  THEN L03 '                      ||
                                               'WHEN NVL(L02,-1) > 0  THEN L02 '                      ||
                                               'WHEN NVL(L01,-1) > 0  THEN L01 '                      ||
                                          'END COD_ELEMENTO,'                                         ||
                                          'L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,'      ||
                                          'L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25 '          ||
                                    'FROM (SELECT g.*,#DATAELECALC# data_disattivazione_calc FROM GERARCHIA_#GER##STA# G) ' ||
                                   'WHERE :dA BETWEEN DATA_ATTIVAZIONE  AND DATA_DISATTIVAZIONE_CALC '     ||   -- pDataA
                                     'AND :ele IN (L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13,'   ||
                                                  'L14,L15,L16,L17,L18,L19,L20,L21,L22,L23,L24,L25)'; -- pCodEle
    vIn     GTTD_CALC_GERARCHIA%ROWTYPE;
    vOut    GTTD_CALC_GERARCHIA%ROWTYPE;
    PROCEDURE Compatta (pInd INTEGER) AS
          TYPE t_lev IS VARRAY(25) OF NUMBER;
          vArea1 t_lev;
          vArea2 t_lev := t_lev(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,
                                NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
        BEGIN
            vArea1 := t_lev(vIn.L01, vIn.L02, vIn.L03, vIn.L04, vIn.L05,
                            vIn.L06, vIn.L07, vIn.L08, vIn.L09, vIn.L10,
                            vIn.L11, vIn.L12, vIn.L13, vIn.L14, vIn.L15,
                            vIn.L16, vIn.L17, vIn.L18, vIn.L19, vIn.L20,
                            vIn.L21, vIn.L22, vIn.L23, vIn.L24, vIn.L25);

            FOR i IN pInd .. 25 LOOP
                vArea2(ABS(pInd-i)+1) :=  vArea1(i);
            END LOOP;
            vOut.L01 := vArea2(01);
            vOut.L02 := vArea2(02);
            vOut.L03 := vArea2(03);
            vOut.L04 := vArea2(04);
            vOut.L05 := vArea2(05);
            vOut.L06 := vArea2(06);
            vOut.L07 := vArea2(07);
            vOut.L08 := vArea2(08);
            vOut.L09 := vArea2(09);
            vOut.L10 := vArea2(10);
            vOut.L11 := vArea2(11);
            vOut.L12 := vArea2(12);
            vOut.L13 := vArea2(13);
            vOut.L14 := vArea2(14);
            vOut.L15 := vArea2(15);
            vOut.L16 := vArea2(16);
            vOut.L17 := vArea2(17);
            vOut.L18 := vArea2(18);
            vOut.L19 := vArea2(19);
            vOut.L20 := vArea2(20);
            vOut.L21 := vArea2(21);
            vOut.L22 := vArea2(22);
            vOut.L23 := vArea2(23);
            vOut.L24 := vArea2(24);
            vOut.L25 := vArea2(25);
        END;
BEGIN
    IF Pkg_Mago_utl.IsMagoSTM AND pOrganizzazione <> Pkg_Mago_utl.gcOrganizzazELE THEN
        vStatoRete  := Pkg_Mago_utl.gcStatoAttuale;
    END IF;
    CASE pOrganizzazione
        WHEN Pkg_Mago_utl.gcOrganizzazELE THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','IMP');
                CASE vStatoRete
                    WHEN Pkg_Mago_utl.gcStatoAttuale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA#','_SA');
                    WHEN Pkg_Mago_utl.gcStatoNormale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA#','_SN');
                END CASE;
        WHEN Pkg_Mago_utl.gcOrganizzazGEO THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','GEO');
        WHEN Pkg_Mago_utl.gcOrganizzazAMM THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','AMM');
    END CASE;
    vSqlGer := REPLACE(vSqlGer,'#STA#','');

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    --OPEN vCurGer FOR vSqlGer USING pDataDa, pDataA, pCodEle;
    OPEN vCurGer FOR vSqlGer USING pDataA, pCodEle;
    LOOP
        FETCH vCurGer INTO vIn;
        EXIT WHEN vCurGer%NOTFOUND;
        vOut := NULL;
        vOut.COD_ELEMENTO := vIn.COD_ELEMENTO;
        CASE pCodEle
            WHEN vIn.L01 THEN Compatta(1);
            WHEN vIn.L02 THEN Compatta(2);
            WHEN vIn.L03 THEN Compatta(3);
            WHEN vIn.L04 THEN Compatta(4);
            WHEN vIn.L05 THEN Compatta(5);
            WHEN vIn.L06 THEN Compatta(6);
            WHEN vIn.L07 THEN Compatta(7);
            WHEN vIn.L08 THEN Compatta(8);
            WHEN vIn.L09 THEN Compatta(9);
            WHEN vIn.L10 THEN Compatta(10);
            WHEN vIn.L11 THEN Compatta(11);
            WHEN vIn.L12 THEN Compatta(12);
            WHEN vIn.L13 THEN Compatta(13);
            WHEN vIn.L14 THEN Compatta(14);
            WHEN vIn.L15 THEN Compatta(15);
            WHEN vIn.L16 THEN Compatta(16);
            WHEN vIn.L17 THEN Compatta(17);
            WHEN vIn.L18 THEN Compatta(18);
            WHEN vIn.L19 THEN Compatta(19);
            WHEN vIn.L20 THEN Compatta(20);
            WHEN vIn.L21 THEN Compatta(21);
            WHEN vIn.L22 THEN Compatta(22);
            WHEN vIn.L23 THEN Compatta(23);
            WHEN vIn.L24 THEN Compatta(24);
            WHEN vIn.L25 THEN Compatta(25);
        END CASE;
        PIPE ROW(vOut);
    END LOOP;
    CLOSE vCurGer;
    RETURN;

END LeggiGerarchiaLineare_save;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaSup     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pData           IN DATE DEFAULT SYSDATE,
                                pDisconnect     IN INTEGER DEFAULT 0)
                         RETURN t_TabGerarchia PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) gli elementi gerarchicamente superiori all'elemento ricevuto
    - L'elemento ricevuto e' parte della gerarchia restituita (identificato con livello piu' alto)
-----------------------------------------------------------------------------------------------------------*/

    vECP    TIPI_ELEMENTO.GER_ECP%TYPE;
    vECS    TIPI_ELEMENTO.GER_ECS%TYPE;
    vGEO    TIPI_ELEMENTO.GER_GEO%TYPE;
    vAMM    TIPI_ELEMENTO.GER_AMM%TYPE;

    vEle    t_RowGerarchia;

    vFlag   INTEGER;

    vCur    PKG_UtlGlb.t_query_cur;

    vSql    VARCHAR2(1200) :=
               'SELECT COD_ELEMENTO,'                                                                  ||
                       'CASE '                                                                         ||
                         'WHEN COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcLineaMT||''' '                      ||
                            'THEN PKG_Elementi.GetGestElemento(COD_ELEMENTO) '                         ||
                            'ELSE COD_GEST_ELEMENTO '                                                  ||
                      'END COD_GEST_ELEMENTO,'                                                         ||
                      'COD_TIPO_ELEMENTO, LIV, ROWNUM '                                                ||
                 'FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, '                   ||
                              'MAX(LIV) OVER (PARTITION BY 1) - liv + 1 LIV '                          ||
                         'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,LEVEL LIV,ROWNUM SEQ '   ||
                                 'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM (SELECT r.*, '                                          ||
                                                      '#DATAELECALC# data_disattivazione_calc '        ||
                                                 'FROM REL_ELEMENTI_#TB1# r ) '                        ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE '                         ||
                                                       'AND DATA_DISATTIVAZIONE_CALC '                 ||
                                       '#ECS# '                                                        ||
                                      ') START WITH COD_ELEMENTO_FIGLIO = :ele '                       ||
                                      'CONNECT BY NOCYCLE PRIOR COD_ELEMENTO_PADRE = COD_ELEMENTO_FIGLIO '     ||
                              ') '                                                                     ||
                         'INNER JOIN ELEMENTI ON COD_ELEMENTO_FIGLIO = COD_ELEMENTO '                  ||
                         'ORDER BY SEQ DESC '                                                          ||
                      ') ';
    vSqlEcs VARCHAR2(500) :=
                    'UNION ALL '                                                                       ||
                   'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                                   ||
                     'FROM (SELECT r.*, '                                                              ||
                                  '#DATAELECALC# data_disattivazione_calc '                            ||
                             'FROM REL_ELEMENTI_#TB2# r )'                                             ||
                    'WHERE :dat BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ';
BEGIN

    BEGIN
        SELECT GER_ECP, GER_ECS, GER_GEO, GER_AMM
          INTO vECP,    vECS,    vGEO,    vAMM
          FROM ELEMENTI
         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodEle;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    IF vECS = 1 THEN
        IF vECP + vGEO + vAMM > 1 THEN
            vSql := REPLACE(vSql,'#ECS#','');
            vFlag := 1;
        ELSE
            vSql := REPLACE(vSql,'#ECS#',vSqlEcs);
            vFlag := 2;
        END IF;
    ELSE
        vSql := REPLACE(vSql,'#ECS#','');
        vFlag := 1;
    END IF;

    CASE pOrganizzazione
        WHEN 1 THEN CASE pStatoRete
                        WHEN 1 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SN');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SN');
                        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SA');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
                        ELSE RETURN;
                    END CASE;
        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','GEO');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        WHEN 3 THEN vSql := REPLACE(vSql,'#TB1#','AMM');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        ELSE RETURN;
    END CASE;
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    IF vFlag = 1 THEN
        OPEN vCur FOR vSql USING pData,        pCodEle;
    ELSE
        OPEN vCur FOR vSql USING pData, pData, pCodEle;
    END IF;

    LOOP
        FETCH vCur INTO vEle;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vEle);
    END LOOP;
    CLOSE vCur;
    RETURN;

END LeggiGerarchiaSup;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaInf     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pData           IN DATE DEFAULT SYSDATE,
                                pFiltroTipEle   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                                pDisconnect     IN INTEGER DEFAULT 0)
                         RETURN t_TabGerarchia PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) gli elementi gerarchicamente dipendenti dall'elemento ricevuto
    - L'elemento ricevuto e' parte della gerarchia restituita (identificato con livello 0) se non richiesto
      filtro su tipo elemento (pFiltroTipEle)
-----------------------------------------------------------------------------------------------------------*/

    vECP    TIPI_ELEMENTO.GER_ECP%TYPE;
    vECS    TIPI_ELEMENTO.GER_ECS%TYPE;
    vGEO    TIPI_ELEMENTO.GER_GEO%TYPE;
    vAMM    TIPI_ELEMENTO.GER_AMM%TYPE;
    vTip    TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;

    vIncSeq NUMBER(1);

    vEle    t_RowGerarchia;

    vCur    PKG_UtlGlb.t_query_cur;

    vSql    VARCHAR2(1200) :=
               'SELECT COD_ELEMENTO,'                                                                  ||
                       'CASE '                                                                         ||
                         'WHEN COD_TIPO_ELEMENTO = '''||Pkg_Mago_utl.gcLineaMT||''' '                      ||
                            'THEN PKG_Elementi.GetGestElemento(COD_ELEMENTO) '                         ||
                            'ELSE COD_GEST_ELEMENTO '                                                  ||
                      'END COD_GEST_ELEMENTO,'                                                         ||
                      'COD_TIPO_ELEMENTO, LIV, SEQ + :incr '                                           ||
                 'FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, SEQ, LIV '          ||
                         'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,LEVEL LIV,ROWNUM SEQ '   ||
                                 'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM ( SELECT r.*, '                                         ||
                                                       '#DATAELECALC# data_disattivazione_calc '       ||
                                                  'FROM REL_ELEMENTI_#TB1# r ) '                       ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE '                         ||
                                                       'AND DATA_DISATTIVAZIONE_CALC '                 ||
                                        'UNION ALL '                                                   ||
                                       'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM ( SELECT r.*, '                                         ||
                                                       '#DATAELECALC# data_disattivazione_calc '       ||
                                                  'FROM REL_ELEMENTI_#TB2# r ) '                       ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE '                         ||
                                                       'AND DATA_DISATTIVAZIONE_CALC '                 ||
                                      ') START WITH COD_ELEMENTO_PADRE = :ele '                        ||
                                      'CONNECT BY NOCYCLE PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE '     ||
                              ') '                                                                     ||
                         'INNER JOIN ELEMENTI ON COD_ELEMENTO_FIGLIO = COD_ELEMENTO '                  ||
                         'WHERE COD_TIPO_ELEMENTO LIKE :tip '                                          ||
                         'ORDER BY SEQ '                                                               ||
                      ') ';
BEGIN

    BEGIN
        SELECT GER_ECP, GER_ECS, GER_GEO, GER_AMM, COD_TIPO_ELEMENTO
          INTO vECP,    vECS,    vGEO,    vAMM,    vTip
          FROM ELEMENTI
         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodEle;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    CASE pOrganizzazione
        WHEN 1 THEN CASE pStatoRete
                        WHEN 1 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SN');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SN');
                        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SA');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
                        ELSE RETURN;
                    END CASE;
        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','GEO');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        WHEN 3 THEN vSql := REPLACE(vSql,'#TB1#','AMM');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        ELSE RETURN;
    END CASE;

    IF pFiltroTipEle IS NULL THEN
        vEle.COD_ELEMENTO := pCodEle;
        vEle.COD_GEST_ELEMENTO := GetGestElemento(pCodEle);
        vEle.COD_TIPO_ELEMENTO := vTip;
        vEle.LIVELLO := 0;
        vEle.SEQUENZA := 1;
        PIPE ROW(vEle);
        vIncSeq := 1;
    ELSE
        vIncSeq := 0;
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    OPEN vCur FOR vSql USING vIncSeq, pData, pData, pCodEle, NVL(pFiltroTipEle,'%');
    LOOP
        FETCH vCur INTO vEle;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vEle);
    END LOOP;
    CLOSE vCur;

END LeggiGerarchiaInf;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetClientType     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pPOD            IN INTEGER  DEFAULT 1,    -- Default 1 = POD altrimenti COD_GEST
                             pElem           IN VARCHAR2 DEFAULT NULL, -- se null ritorna tutti i clienti definiti
                             pData           IN DATE     DEFAULT SYSDATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna, un cursore che descrive, per ogni cliente MT richiesto
         (o per tutti i clienti MT se nessun codice indicato),
    il tipo di cliente definito in anafrafica.
    I codici possono essere indicati come
      - POD      : parametro pPOD  = 1
        COD GEST : parametro pPOD <> 1
    il cursore di ritorno e' composto da
        . codice elemento (POD o COD GEST come da valorizzazione del param pPOD)
        . Tipo cliente (vedere tab TIPI_CLIENTE per significato del tipo cliente)
        . flag di validita': impostato = 1 se elemento in anagrafica all'istante richiesto
                                       = 0 se elemento non in anagrafica all'istante richiesto
-----------------------------------------------------------------------------------------------------------*/
    vLista      PKG_UtlGlb.t_SplitTbl;
    vKeyTmp     GTTD_VALORI_TEMP.TIP%TYPE := 'TIPCLIE.';
BEGIN

    DELETE GTTD_VALORI_TEMP WHERE TIP = vKeyTmp;

    IF pElem IS NULL THEN
        OPEN pRefCurs FOR SELECT CODICE, COD_TIPO_CLIENTE, 1 VALIDO
                            FROM (SELECT CASE pPOD
                                            WHEN 1 THEN D.RIF_ELEMENTO
                                                   ELSE E.COD_GEST_ELEMENTO
                                            END CODICE,
                                         D.COD_TIPO_CLIENTE
                                    FROM ELEMENTI E
                                   INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                                            AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                                          AND D.DATA_DISATTIVAZIONE
                                   WHERE E.COD_TIPO_ELEMENTO = Pkg_Mago_utl.gcClienteMT
                                 )
                           WHERE CODICE IS NOT NULL
                           ORDER BY 1;
    ELSE
        vLista   := PKG_UtlGlb.SplitString(pElem,Pkg_Mago_utl.cSepCharLst);
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            INSERT INTO GTTD_VALORI_TEMP (TIP, ALF1,NUM1) VALUES (vKeyTmp,vLista(i),i);
        END LOOP;
        OPEN pRefCurs FOR SELECT T.ALF1 CODICE, X.COD_TIPO_CLIENTE,
                                 CASE
                                    WHEN X.RIFER IS NOT NULL THEN 1
                                                             ELSE 0
                                 END valido
                            FROM GTTD_VALORI_TEMP T
                            LEFT OUTER JOIN (SELECT D.RIF_ELEMENTO, E.COD_GEST_ELEMENTO, D.COD_TIPO_CLIENTE,
                                                    CASE pPOD
                                                       WHEN 1 THEN D.RIF_ELEMENTO
                                                              ELSE E.COD_GEST_ELEMENTO
                                                    END RIFER
                                               FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO
                                                       FROM ELEMENTI
                                                      WHERE COD_TIPO_ELEMENTO = Pkg_Mago_utl.gcClienteMT
                                                    ) E
                                              INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                                                       AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                                                     AND D.DATA_DISATTIVAZIONE
                                            ) X ON T.ALF1 = X.RIFER
                           ORDER BY NUM1;
    END IF;

END GetClientType;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE oldGestCodePerimeter  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pData           IN DATE,
                              pElemTypeReq    IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pDisconnect     IN INTEGER  DEFAULT 0,
                              pTipiCliente    IN VARCHAR2 DEFAULT NULL,
                              pElementiConPI  IN INTEGER  DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il perimetro degli elementi relativi al tipo richiesto per il Codice Gestionale ricevuto
   Se pElemTypeReq � cliente (AT/MT/BT) e pTipiCliente e' valorizzato estrae solo i clienti che
      soddisfano pTipiCliente
   Se pElemTypeReq � non cliente pTipiCliente viene ignorato
   Se pElementiConPI = 1 vengono selezionati solo gli elementi con PI valorizzata
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vFiltTipCli NUMBER(1) := 0;
    vSql        VARCHAR2(1000) :=
                 'SELECT DISTINCT A.COD_ELEMENTO,A.COD_GEST_ELEMENTO,B.NOME_ELEMENTO '                                      ||
                        'FROM TABLE(PKG_Elementi.LeggiGerarchiaInf(PKG_Elementi.GetCodElemento(:cg),'                       ||
                                                                  ':Org,:STA,:dt,:tip,:Dis) '                               ||
                                  ') A '                                                                                    ||
                       'LEFT OUTER JOIN ELEMENTI_DEF        B ON B.COD_ELEMENTO = A.COD_ELEMENTO '                          ||
                                                            'AND :dt BETWEEN B.DATA_ATTIVAZIONE '                           ||
                                                                        'AND B.DATA_DISATTIVAZIONE '                        ||
                      '#FILTRO_PI# '                                                                                        ||
                      'WHERE CASE WHEN :fc = 0 '                                                                            ||
                                'THEN 1 '                                                                                   ||
                                'ELSE CASE WHEN B.COD_TIPO_CLIENTE IN (SELECT ALF1 '                                        ||
                                                                        'FROM GTTD_VALORI_TEMP '                            ||
                                                                       'WHERE TIP = '''||Pkg_Mago_utl.gcTmpTipCliKey||''' '     ||
                                                                     ') '                                                   ||
                                        'THEN 1 '                                                                           ||
                                        'ELSE 0 '                                                                           ||
                                     'END '                                                                                 ||
                      'END = 1 '                                                                                            ||
                      'ORDER BY COD_GEST_ELEMENTO ';
    vSqlFiltPI  VARCHAR2(310) :=
                      'INNER JOIN TRATTAMENTO_ELEMENTI      T ON T.COD_ELEMENTO = A.COD_ELEMENTO '                          ||
                                                            'AND COD_TIPO_MISURA = '''||Pkg_Mago_utl.gcPotenzaInstallata||''' ' ||
                                                            'AND TIPO_AGGREGAZIONE IN (0,:STA) '                            ||
                      'INNER JOIN MISURE_AGGREGATE_STATICHE M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM '          ||
                                                            'AND :dt BETWEEN M.DATA_ATTIVAZIONE '                           ||
                                                                        'AND M.DATA_DISATTIVAZIONE '                        ||
                                                            'AND NVL(M.VALORE,0) > 0 ';

BEGIN
    IF pElemTypeReq IN (Pkg_Mago_utl.gcClienteAT,Pkg_Mago_utl.gcClienteMT,Pkg_Mago_utl.gcClienteBT) AND
       pTipiCliente IS NOT NULL THEN
        vFiltTipCli := 1;
        DELETE GTTD_VALORI_TEMP WHERE TIP = Pkg_Mago_utl.gcTmpTipCliKey;
        Pkg_Mago_utl.TrattaListaCodici(pTipiCliente, Pkg_Mago_utl.gcTmpTipCliKey, vFlgNull); -- non gestito il flag
    END IF;

    IF pElementiConPI <> 1 THEN
        vSql := REPLACE(vSql,'#FILTRO_PI#','');
        OPEN pRefCurs FOR vSql USING pGestElem,pOrganizzazione,pStatoRete,pData,pElemTypeReq,pDisconnect,pData,vFiltTipCli;
    ELSE
        vSql := REPLACE(vSql,'#FILTRO_PI#',vSqlFiltPI);
        OPEN pRefCurs FOR vSql USING pGestElem,pOrganizzazione,pStatoRete,pData,pElemTypeReq,pDisconnect,pData,pStatoRete,pData,vFiltTipCli;
    END IF;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GestCodePerimeter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END oldGestCodePerimeter;

-- ----------------------------------------------------------------------------------------------------------
PROCEDURE GestCodePerimeter(
    pRefCurs OUT PKG_UtlGlb.t_query_cur,
    pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
    pOrganizzazione IN INTEGER,
    pStatoRete      IN INTEGER,
    pData           IN DATE,
    pElemTypeReq    IN VARCHAR2,
    pDisconnect     IN INTEGER DEFAULT 0,
    pTipiCliente    IN VARCHAR2 DEFAULT NULL,
    pElementiConPI  IN INTEGER DEFAULT 0,
    pEleFonti       IN VARCHAR2 DEFAULT NULL)
AS
  /*-----------------------------------------------------------------------------------------------------------
  Restituisce il perimetro degli elementi relativi al tipo richiesto per il Codice Gestionale ricevuto
  Se pElemTypeReq � cliente (AT/MT/BT) e pTipiCliente e' valorizzato estrae solo i clienti che
  soddisfano pTipiCliente
  Se pElemTypeReq � non cliente pTipiCliente viene ignorato (filtro nella select con vListaClientABM)
  Se pElementiConPI = 1 vengono selezionati solo gli elementi con PI valorizzata
  -----------------------------------------------------------------------------------------------------------*/
  vFlgNull       NUMBER(1)      := -1;
  vFiltTipCli    NUMBER(1)      := 0;
  vFiltfonti     NUMBER(1)      := 0;

  -- preparo la lista codici Tipi elemento per renderla compatibile per SYS.odcivarchar2list
  vListaTipiElem VARCHAR2(1000)  := ''''||REPLACE(pElemTypeReq,'|',''',''')||'''';

  -- preparo la lista tipi elem. Client A/B/M per SYS.odcivarchar2list
  vListaClientABM VARCHAR2(1000) :=
                    ''''
                    ||Pkg_Mago_utl.gcClienteAT
                    ||''','''
                    ||Pkg_Mago_utl.gcClienteMT
                    ||''','''
                    ||Pkg_Mago_utl.gcClienteBT
                    ||'''';



  vSql           VARCHAR2(3000) :=
    'SELECT DISTINCT A.COD_ELEMENTO,A.COD_GEST_ELEMENTO,B.NOME_ELEMENTO ,' ||
    '(select round(ED.COORDINATA_Y,4)  from ELEMENTI_DEF ED where ED.cod_elemento =  (PKG_ELEMENTI.GetElementoPadre ( B.COD_ELEMENTO,''SCS'' , B.DATA_ATTIVAZIONE, 1,2, 0 ) ) and  B.DATA_ATTIVAZIONE between  ED.DATA_ATTIVAZIONE and ED.DATA_DISATTIVAZIONE) LAT, ' ||      
    '(select round(ED.COORDINATA_X,4)  from ELEMENTI_DEF ED where ED.cod_elemento =  (PKG_ELEMENTI.GetElementoPadre ( B.COD_ELEMENTO,''SCS'' , B.DATA_ATTIVAZIONE, 1,2, 0 ) ) and  B.DATA_ATTIVAZIONE between  ED.DATA_ATTIVAZIONE and ED.DATA_DISATTIVAZIONE) LON  ' ||   
    'FROM TABLE(pkg_elementi.LeggiGerarchiaInf(PKG_Elementi.GetCodElemento(:cg),:Org,:STA,:dt,null,:Dis))  A ' ||
    'LEFT OUTER JOIN ELEMENTI_DEF B ON ( B.COD_ELEMENTO = A.COD_ELEMENTO ' ||
                                        'AND :dt BETWEEN B.DATA_ATTIVAZIONE ' ||
                                        'AND B.DATA_DISATTIVAZIONE ) ' ||
    '#JoinFiltri#' ||
    'JOIN TABLE(SYS.odcivarchar2list('||vListaTipiElem||')) O ON O.COLUMN_VALUE = A.COD_TIPO_ELEMENTO     ' ||
    'WHERE CASE WHEN :fc = 0 ' ||
              'THEN 1 ' ||
            'ELSE CASE WHEN B.COD_TIPO_CLIENTE IN (SELECT ALF1 ' ||
                                                  'FROM GTTD_VALORI_TEMP ' ||
                                                  'WHERE TIP = '''||Pkg_Mago_utl.gcTmpTipCliKey||''' ' || ') ' ||
                            'OR NOT B.COD_TIPO_ELEMENTO IN (SELECT COLUMN_VALUE FROM TABLE(SYS.odcivarchar2list('||vListaClientABM||')))  ' ||
                    'THEN 1 ' ||
                    'ELSE 0 ' ||
                  'END ' ||
            'END = 1 ' ||
    'ORDER BY COD_GEST_ELEMENTO ';


  vSqlJoinFilt   VARCHAR2(5000) :=
    'INNER JOIN TRATTAMENTO_ELEMENTI T ON ( T.COD_ELEMENTO = A.COD_ELEMENTO ' ||
                                                'AND T.COD_TIPO_MISURA = '''||Pkg_Mago_utl.gcPotenzaInstallata||''' ' ||
                                                'AND T.TIPO_AGGREGAZIONE IN (0,:STA) ' ||
                                                '#WhereFonti# ) ' ||
    'INNER JOIN MISURE_AGGREGATE_STATICHE M ON ( M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM ' ||
                                                'AND :dt BETWEEN M.DATA_ATTIVAZIONE ' ||
                                                'AND M.DATA_DISATTIVAZIONE ' ||
                                                '#WherePI# ) ';


  vSqlFiltPI     VARCHAR2(5000) := 'AND NVL(M.VALORE,0) > 0 ';

  vSqlJoinFonti  VARCHAR2(500)  := 'AND T.COD_TIPO_FONTE IN (SELECT ALF1 FROM GTTD_VALORI_TEMP WHERE TIP = ''TMPFONTI'' ) ';

BEGIN

  IF NOT pEleFonti IS NULL THEN
    vFiltFonti     := 1;
    DELETE GTTD_VALORI_TEMP WHERE TIP = 'TMPFONTI';
    Pkg_Mago_utl.TrattaListaCodici(pEleFonti, 'TMPFONTI', vFlgNull); -- non gestito il flag
  END IF;

  IF pTipiCliente IS NOT NULL THEN
    vFiltTipCli    := 1;
    DELETE GTTD_VALORI_TEMP WHERE TIP = Pkg_Mago_utl.gcTmpTipCliKey;
    Pkg_Mago_utl.TrattaListaCodici(pTipiCliente, Pkg_Mago_utl.gcTmpTipCliKey, vFlgNull); -- non gestito il flag
  END IF;

  IF vFiltTipCli = 0 AND vFiltFonti = 0 THEN
    vSql        := REPLACE(vSql,'#JoinFiltri#','');
  ELSE
    vSql         := REPLACE(vSql,'#JoinFiltri#',vSqlJoinFilt);
    IF vFiltFonti = 0 THEN
      vSql       := REPLACE(vSql,'#WhereFonti#','');
    ELSE
      vSql := REPLACE(vSql,'#WhereFonti#',vSqlJoinFonti);
    END IF;
    IF vFiltTipCli = 0 THEN
      vSql        := REPLACE(vSql,'#WherePI#','');
    ELSE
      vSql := REPLACE(vSql,'#WherePI#',vSqlFiltPI);
    END IF;
  END IF;

  vSql := PKG_UtlGlb.CompattaSelect(vSql);

--  DBMS_OUTPUT.PUT_LINE('===================================');
--  DBMS_OUTPUT.PUT_LINE(vSql);
--  DBMS_OUTPUT.PUT_LINE('===================================');

  IF vFiltTipCli = 0 AND vFiltFonti = 0 THEN
    OPEN pRefCurs FOR vSql USING pGestElem,
                                  pOrganizzazione,
                                  pStatoRete,
                                  pData,
                                  pDisconnect,
                                  pData,
                                  vFiltTipCli;
  ELSE
    OPEN pRefCurs FOR vSql USING pGestElem,
                                  pOrganizzazione,
                                  pStatoRete,
                                  pData,
                                  pDisconnect,
                                  pData,
                                  pStatoRete,
                                  pData,
                                  vFiltTipCli;
  END IF;

--  DELETE zzz_GTTD_VALORI_TEMP;

--  INSERT INTO zzz_GTTD_VALORI_TEMP
--  SELECT * FROM GTTD_VALORI_TEMP;

EXCEPTION
WHEN OTHERS THEN
  ROLLBACK;
  PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione GestCodePerimeter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
  PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
  RAISE;
END GestCodePerimeter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetCOprevalente    (pCodElem       OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGestElem      OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNomeElem      OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il perimetro degli elementi relativi al tipo richiesto per il Codice Gestionale ricevuto
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT A.COD_ELEMENTO, B.COD_GEST_ELEMENTO, C.NOME_ELEMENTO
      INTO pCodElem, pGestElem, pNomeElem
      FROM (SELECT PKG_Elementi.GETELEMENTOESEDEF COD_ELEMENTO FROM DUAL) A
      LEFT OUTER JOIN ELEMENTI B  ON B.COD_ELEMENTO = A.COD_ELEMENTO
      LEFT OUTER JOIN ELEMENTI_DEF C ON C.COD_ELEMENTO = A.COD_ELEMENTO
                                    AND SYSDATE BETWEEN C.DATA_ATTIVAZIONE AND C.DATA_DISATTIVAZIONE;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GestCodePerimeter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetCOprevalente;

 FUNCTION f_GetGestFromPod(pPod      IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                           pGest IN OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN NUMBER IS
                           
/*-----------------------------------------------------------------------------------------------------------
  Converte il POD di un produttore nel corrispondente codice gestionale
-----------------------------------------------------------------------------------------------------------*/

    f_OK  CONSTANT NUMBER :=  0; -- trovato cod gestionale
    f_NO  CONSTANT NUMBER := -1; -- non trovato cod gestionale
    f_TOO CONSTANT NUMBER := -2; -- pod associato a pi� cod gestionali 

BEGIN

    pGest := NULL;
    
    SELECT E.COD_GEST_ELEMENTO
    INTO pGest
    FROM ELEMENTI E
    JOIN (SELECT COD_ELEMENTO FROM ELEMENTI_DEF
          WHERE SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
          AND RIF_ELEMENTO = pPod) D
    USING (COD_ELEMENTO);
    
    return f_OK;

EXCEPTION
    WHEN NO_DATA_FOUND THEN
         return f_NO;
    WHEN TOO_MANY_ROWS THEN
         return f_TOO;
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.f_GetGestFromPod '||pPod||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END f_GetGestFromPod;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => Pkg_Mago_utl.gcJobClassUTL,
                          pFunzione    => 'PKG_Elementi',
                          pStoreOnFile => FALSE);

    BEGIN
        SELECT COD_ELEMENTO_CO, COD_ELEMENTO_ESE
          INTO gElementoBase,   gElementoEseDef
          FROM DEFAULT_CO
         WHERE FLAG_PRIMARIO = 1;
    EXCEPTION
        WHEN OTHERS THEN IF Pkg_Mago_utl.gInizializzazione_In_Corso THEN
                            NULL;
                         ELSE
                            RAISE;
                         END IF;
    END;

-- ----------------------------------------------------------------------------------------------------------

END PKG_ELEMENTI;
/