PROMPT PACKAGE: PKG_SUPERVISIONE

CREATE OR REPLACE PACKAGE PKG_SUPERVISIONE AS 


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


  /* TODO enter package declarations (types, exceptions, methods etc) here */ 



 PROCEDURE GetSpvSistema      (pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              );
                              

 PROCEDURE GetSpvDizionarioAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                        , pID_SISTEMA number
                              );                            
 
 PROCEDURE GetSpvAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              , pID_SISTEMA number
--                              , pID_DIZIONARIO_ALLARME varchar2
--                              , pID_LIVELLO_ALLARME varchar2
                              );                            
 
 PROCEDURE GetSpvDettaglioParametri ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                    , pID_SISTEMA number
                                    );          
                                    
PROCEDURE AddSpvAllarmi(/*pResult out number
                      , */pID_SISTEMA number
                      , pID_DIZIONARIO_ALLARME number
											, pDATA_ALLARME date
											, pDESCRIZIONE_TIPO VARCHAR2 
											, pDESCRIZIONE_ELEMENTO VARCHAR2
											, pDESCRIZIONE_ALTRO1 VARCHAR2
											, pDESCRIZIONE_ALTRO2 VARCHAR2
											, pDESCRIZIONE_ALTRO3 VARCHAR2
											, pID_LIVELLO NUMBER
											, pINFO VARCHAR2
                      , pPARAMETRI_DESCRIZIONE VARCHAR2);            

PROCEDURE CleanSpvAllarmi(pID_SISTEMA number);  
                      
PROCEDURE SetSpvDettaglioParametri( pID_SISTEMA number
                                  , pID_CHIAVE VARCHAR2
                                  , pValore VARCHAR2
                                  , pVALORE_BLOB BLOB); 
                                  
PROCEDURE DecrementSpvDettaglioParametri( pID_SISTEMA number
                                        , pID_CHIAVE VARCHAR2
                                        , pID_CHIAVE_ELENCO VARCHAR2
                                        , pCOD_GESTIONALE VARCHAR2);                                  
                                  
PROCEDURE GetSpvCollectorInfo      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                   , pID_SISTEMA        number);                                      
PROCEDURE CleanSpvCollectorInfo(pID_SISTEMA number);                                  
PROCEDURE AddSpvCollectorInfo(	pCOLLECTOR_NAME				        VARCHAR2
                              , pDATA_INSERIMENTO				      DATE
                              , pID_SISTEMA                   number
                              , pID_DIZIONARIO_ALLARME        number
                              , pSUPPLIER_NAME					      VARCHAR2
                              , pCOD_GEST_ELEMENTO			      VARCHAR2
                              , pCOD_TIPO_MISURA				      VARCHAR2
                              , pCOD_TIPO_FONTE				        VARCHAR2
                              , pVALORE						            NUMBER
                              , pINFORMAZIONI					        VARCHAR2
                              );      
PROCEDURE AddSpvCollectorInfoWithPod(	pCOLLECTOR_NAME				        VARCHAR2
                                    , pDATA_INSERIMENTO				      DATE
                                    , pID_SISTEMA                   number
                                    , pID_DIZIONARIO_ALLARME        number
                                    , pSUPPLIER_NAME					      VARCHAR2
                                    , pCOD_POD              			  VARCHAR2
                                    , pCOD_TIPO_MISURA				      VARCHAR2
                                    , pCOD_TIPO_FONTE				        VARCHAR2
                                    , pVALORE						            NUMBER
                                    , pINFORMAZIONI					        VARCHAR2
                                    );                                   
PROCEDURE AddSpvCollectorInfos       (p_Array    IN T_SPVCOLLECTORINFO_ARRAY);                              
PROCEDURE GetSpvSoglie      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                            , pID_SISTEMA        number);                  
PROCEDURE AddSpvSoglie       (pMisureMeteo    IN T_SPVSOGLIA_ARRAY) ;                            
PROCEDURE GetSpvCollectorMeasure      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                      , pCOLLECTOR_NAME		VARCHAR2
                                      , pSTART_DATE	DATE);                                      

PROCEDURE AddSpvCollectorMeasure(	pCOLLECTOR_NAME				        VARCHAR2
                                , pDATA_INSERIMENTO				      DATE
                                , pDATA_MISURA    				      DATE
                                , pCOD_GEST_ELEMENTO			      VARCHAR2
                                , pCOD_TIPO_MISURA				      VARCHAR2
                                , pCOD_TIPO_FONTE				        VARCHAR2
                                , pVALORE						            NUMBER
                                );                                        

PROCEDURE AddSpvCollectorMeasures       (p_Array    IN T_SPVCOLLECTORMEASURE_ARRAY);                              

PROCEDURE getMeteoJobStaticConfig      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                    , pKEY               VARCHAR2);                                
                                    
FUNCTION getTimestamp(pDate IN DATE) 
   RETURN NUMBER;    
   
FUNCTION convert_to_clob(l_blob BLOB) RETURN CLOB;
FUNCTION convert_to_blob(l_clob CLOB) RETURN BLOB;

-- Restituisce il campo valore della tabella SPV_DETTAGLIO_PARAMETRI
	function F_getSpvDettaglioParametri ( pID_SISTEMA IN spv_dettaglio_parametri.ID_SISTEMA%type
										 ,pID_CHIAVE IN spv_dettaglio_parametri.ID_CHIAVE%type
							   )
	return spv_dettaglio_parametri.VALORE%type;
  
	-- Restituisce il campo valore della tabella SPV_SOGLIE
	function F_getSpvSoglie ( pID_SISTEMA IN spv_soglie.ID_SISTEMA%type
							 ,PID_CHIAVE IN spv_soglie.ID_CHIAVE%type
					)
	return spv_soglie.VALORE%type;
END PKG_SUPERVISIONE;
/