PROMPT PACKAGE: PKG_ELEMENTI

CREATE OR REPLACE PACKAGE PKG_ELEMENTI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_RowElemento         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       COD_TIPO_FONTE       ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                                       COD_TIPO_CLIENTE     ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE,
                                       ID_ELEMENTO          ELEMENTI_DEF.ID_ELEMENTO%TYPE,
                                       RIF_ELEMENTO         ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                                       COORDINATA_X         ELEMENTI_DEF.COORDINATA_X%TYPE,
                                       COORDINATA_Y         ELEMENTI_DEF.COORDINATA_Y%TYPE,
                                       POTENZA_INSTALLATA   ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE,
                                       POTENZA_CONTRATTUALE ELEMENTI_DEF.POTENZA_CONTRATTUALE%TYPE);
 TYPE t_DefElemento         IS TABLE OF t_RowElemento;

 TYPE t_RowElemList         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       DES_TIPO_ELEMENTO    TIPI_ELEMENTO.DESCRIZIONE%TYPE,
                                       FLAG                 NUMBER,
                                       RIFERIMENTO          ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                                       COUNT                NUMBER);
 TYPE t_TabElemList         IS TABLE OF t_RowElemList ;


 TYPE t_RowGerarchiaLineare IS RECORD (COD_ELEMENTO  NUMBER,
                                       L01           NUMBER,
                                       L02           NUMBER,
                                       L03           NUMBER,
                                       L04           NUMBER,
                                       L05           NUMBER,
                                       L06           NUMBER,
                                       L07           NUMBER,
                                       L08           NUMBER,
                                       L09           NUMBER,
                                       L10           NUMBER,
                                       L11           NUMBER,
                                       L12           NUMBER,
                                       L13           NUMBER,
                                       L14           NUMBER,
                                       L15           NUMBER,
                                       L16           NUMBER,
                                       L17           NUMBER,
                                       L18           NUMBER,
                                       L19           NUMBER,
                                       L20           NUMBER,
                                       L21           NUMBER,
                                       L22           NUMBER,
                                       L23           NUMBER,
                                       L24           NUMBER,
                                       L25           NUMBER);
 TYPE t_TabGerarchiaLineare IS TABLE OF t_RowGerarchiaLineare;

 
  TYPE t_RowGerarchia        IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       LIVELLO              INTEGER,
                                       SEQUENZA             INTEGER);
 TYPE t_TabGerarchia        IS TABLE OF t_RowGerarchia; 
/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetOrderByElemType (pTipoElem       IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pCodGestElem    IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetElemBaseEseDef (pElementoBase   IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoEseDef IN ELEMENTI.COD_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoBase    RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoEseDef  RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION DecodeTipElem      (pTipEle         IN VARCHAR2,
                              pCodeOnNotFound IN BOOLEAN DEFAULT TRUE)
                       RETURN TIPI_ELEMENTO.DESCRIZIONE%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultCO      (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultESE     (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetCodElemento    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetGestElemento   (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  CalcOutCodGest    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pData           IN DATE DEFAULT SYSDATE,
                              pTipoElemento   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                              pRifElemento    IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE DEFAULT NULL)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoPadre   (pElemFiglio     IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipoPadre      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pData           IN DATE,
                              pOrganizzazione IN NUMBER,
                              pStato          IN NUMBER,
                              pDisconnect     IN NUMBER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  InsertElement     (pCodGest        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) RETURN  ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetElemInfo       (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataRif        IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pTipiCliente    IN VARCHAR2,
                              pOrganizzaz     IN NUMBER,
                              pStatoRete      IN INTEGER) RETURN INTEGER; -- DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetElemTooltip    (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataRif        IN DATE,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pElemInfo       IN INTEGER,
                              pTipologiaRete  IN VARCHAR2,
                              pTipiCliente    IN VARCHAR2,
                              pOrganizzaz     IN NUMBER,
                              pStatoRete      IN INTEGER) RETURN VARCHAR2; -- DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE,
                               pStatoRete     IN INTEGER DEFAULT PKG_Mago_utl.gcStatoAttuale,
                               pDisconnect    IN INTEGER DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pElemTypes      IN VARCHAR2 DEFAULT NULL,
                              pCompilaInfo    IN INTEGER  DEFAULT PKG_Mago_utl.gcON,
                              pCalcOutCodGest IN INTEGER  DEFAULT 1,
                              pDisconnect     IN INTEGER  DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN VARCHAR2,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementList    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pNumRows        IN INTEGER  DEFAULT 100);

 FUNCTION GetElementList     (pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL)
                       RETURN t_TabElemList PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaLineare (pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchiaLineare PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaSup     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchia PIPELINED;

 FUNCTION LeggiGerarchiaInf     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pFiltroTipEle   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchia PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetClientType     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pPOD            IN INTEGER  DEFAULT 1,    -- Default 1 = POD altrimenti COD_GEST
                              pElem           IN VARCHAR2 DEFAULT NULL, -- se null ritorna tutti i clienti definiti
                              pData           IN DATE     DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE oldGestCodePerimeter (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pData           IN DATE,
                              pElemTypeReq    IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pDisconnect     IN INTEGER  DEFAULT 0,
                              pTipiCliente    IN VARCHAR2 DEFAULT NULL,
                              pElementiConPI  IN INTEGER  DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GestCodePerimeter(
    pRefCurs OUT PKG_UtlGlb.t_query_cur,
    pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
    pOrganizzazione IN INTEGER,
    pStatoRete      IN INTEGER,
    pData           IN DATE,
    pElemTypeReq    IN VARCHAR2,
    pDisconnect     IN INTEGER DEFAULT 0,
    pTipiCliente    IN VARCHAR2 DEFAULT NULL,
    pElementiConPI  IN INTEGER DEFAULT 0,
    pEleFonti       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetCOprevalente   (pCodElem       OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGestElem      OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNomeElem      OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------
 FUNCTION f_GetGestFromPod(pPod      IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                           pGest IN OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN NUMBER;
                           
-- ----------------------------------------------------------------------------------------------------------                           
END PKG_Elementi;
/