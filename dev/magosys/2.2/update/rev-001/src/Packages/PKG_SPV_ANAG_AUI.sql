PROMPT PACKAGE: PKG_SPV_ANAG_AUI

CREATE OR REPLACE PACKAGE PKG_SPV_ANAG_AUI AS 

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/



/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

function f_Date2DettStr(pData in date) return varchar2;


PROCEDURE sp_CheckProdAUI  (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);
                         

PROCEDURE sp_CheckSbarreAUI (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) ;

PROCEDURE sp_CheckSbarreMAGO (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) ;

                                     
PROCEDURE sp_CheckMontanti_AUI  (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);
                                     
PROCEDURE sp_CheckProdPod_AUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);
                                 
                                 
PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);
                                 

  procedure auiSpvCollector;
  procedure auiSpvProcessor;
  procedure auiSpvDetail;
  procedure auiSpvMain(	 pDoDeleteOldSpvInfo in number default 0
							,pDoDeleteSpvAlarm in number default 1
							,pDoDeleteTodaySpvInfo in number default 1
							,pDoCollector in number default 1
							,pDoProcessor in number default 1
							,pDoDetail in number default 1
							);
END PKG_SPV_ANAG_AUI;
/

/