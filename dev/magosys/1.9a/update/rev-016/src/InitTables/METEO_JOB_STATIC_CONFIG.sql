PROMPT METEO_JOB_STATIC_CONFIG


MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MFM.enabled.suppliers' key ,'1' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

