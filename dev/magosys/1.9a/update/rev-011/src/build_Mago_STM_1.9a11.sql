PROMPT =======================================================================================
PROMPT rel_19a11_STM
PROMPT =======================================================================================

PROMPT
PROMPT

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_STM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

WHENEVER SQLERROR CONTINUE

conn mago/mago

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MISURE.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT

@./InitTables/METEO_JOB_STATIC_CONFIG.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.11
PROMPT

SPOOL OFF

