PROMPT TYPE T_KPI_RICHIESTA_OBJ;
--
-- T_KPI_RICHIESTA_OBJ  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--

BEGIN
    EXECUTE IMMEDIATE 'DROP TYPE T_KPI_RICHIESTA_OBJ';
    DBMS_OUTPUT.PUT_LINE('Type T_KPI_RICHIESTA_OBJ droppato');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -04043 THEN DBMS_OUTPUT.PUT_LINE('Type T_KPI_RICHIESTA_OBJ non presente');
                            ELSE RAISE;
        END IF;
END;
/


CREATE OR REPLACE TYPE T_KPI_RICHIESTA_OBJ AS OBJECT      
      (
        ID_REQ_KPI                  NUMBER,
        DATA_RICHIESTA              DATE,
        DATA_SCADENZA               DATE,
        OPERATORE                   VARCHAR2(20),
        PERIODO_INIZIO              DATE,
        PERIODO_FINE                DATE,
        ORARIO_INIZIO               CHAR(5),
        ORARIO_FINE                 CHAR(5),
        RISOLUZIONE                 INTEGER,
        ORGANIZZAZIONE              INTEGER,
        STATO_RETE                  INTEGER,
        LISTA_FONTI                 VARCHAR2(20),
        LISTA_TIPI_RETE             VARCHAR2(20),
        LISTA_TIPI_CLIE             VARCHAR2(20),
        LISTA_INDICI_RICH           VARCHAR2(200),
        LISTA_TIPI_MISURA           VARCHAR2(100),
        LISTA_TIPI_MISURA_FORECAST  VARCHAR2(100),
        FREQ_ERR_VAL_INI            INTEGER,
        FREQ_ERR_VAL_INI_PERC       INTEGER,
        FREQ_ERR_VAL_FIN            INTEGER,
        FREQ_ERR_VAL_FIN_PERC       INTEGER,
        FREQ_ERR_PASSO              INTEGER,
        FREQ_ERR_PASSO_PERC         INTEGER,
        SOGLIA_COLORE               INTEGER,
        SOGLIA_COLORE_PERC          INTEGER,
        VALORE_SOGLIA               INTEGER,
        DELTA                       INTEGER,
        TIPO_NOTIFICA               INTEGER,
        ELAB_INIZIO                 DATE,
        ELAB_FINE                   DATE,
        ELAB_STATO                  INTEGER,
        DATA_STATO                  DATE,
        OPER_STATO                  VARCHAR2(20),
        ELAB_NOTE                   VARCHAR2(4000),
        AVANZAMENTO                 NUMBER         
      )
/
SHOW ERRORS;


