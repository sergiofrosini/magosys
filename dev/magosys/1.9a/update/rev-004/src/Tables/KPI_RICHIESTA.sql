PROMPT TABLE KPI_RICHIESTA;
--
-- KPI_RICHIESTA  (Table) 
--

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE KPI_RICHIESTA_SAV PURGE';
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE KPI_RICHIESTA_SAV AS SELECT * FROM KPI_RICHIESTA
/

ALTER TABLE KPI_ELEMENTI  DROP CONSTRAINT KPI_ELE_KPI_REQ
/
ALTER TABLE KPI_PARAMETRI DROP CONSTRAINT KPI_PARAM_KPI_REQ
/

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE KPI_RICHIESTA PURGE';
    DBMS_OUTPUT.PUT_LINE('Type KPI_RICHIESTA droppato');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN DBMS_OUTPUT.PUT_LINE('Type KPI_RICHIESTA non presente');
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE KPI_RICHIESTA
(
        ID_REQ_KPI                  NUMBER,
        DATA_RICHIESTA              DATE,
        DATA_SCADENZA               DATE,
        OPERATORE                   VARCHAR2(20),
        PERIODO_INIZIO              DATE,
        PERIODO_FINE                DATE,
        ORARIO_INIZIO               CHAR(5),
        ORARIO_FINE                 CHAR(5),
        RISOLUZIONE                 INTEGER,
        ORGANIZZAZIONE              INTEGER,
        STATO_RETE                  INTEGER,
        LISTA_FONTI                 VARCHAR2(20),
        LISTA_TIPI_RETE             VARCHAR2(20),
        LISTA_TIPI_CLIE             VARCHAR2(20),
        LISTA_INDICI_RICH           VARCHAR2(200),
        LISTA_TIPI_MISURA           VARCHAR2(100),
        LISTA_TIPI_MISURA_FORECAST  VARCHAR2(100),
        FREQ_ERR_VAL_INI            INTEGER,
        FREQ_ERR_VAL_INI_PERC       INTEGER,
        FREQ_ERR_VAL_FIN            INTEGER,
        FREQ_ERR_VAL_FIN_PERC       INTEGER,
        FREQ_ERR_PASSO              INTEGER,
        FREQ_ERR_PASSO_PERC         INTEGER,
        SOGLIA_COLORE               INTEGER,
        SOGLIA_COLORE_PERC          INTEGER,
        VALORE_SOGLIA               INTEGER,
        DELTA                       INTEGER,
        TIPO_NOTIFICA               INTEGER,
        ELAB_INIZIO                 DATE,
        ELAB_FINE                   DATE,
        ELAB_STATO                  INTEGER,
        DATA_STATO                  DATE,
        OPER_STATO                  VARCHAR2(20),
        ELAB_NOTE                   VARCHAR2(4000) 
)
TABLESPACE &TBS&DAT
NOCOMPRESS 
NOPARALLEL
/

INSERT INTO KPI_RICHIESTA (ID_REQ_KPI,DATA_RICHIESTA,OPERATORE,PERIODO_INIZIO,PERIODO_FINE,ORARIO_INIZIO,ORARIO_FINE,RISOLUZIONE,
                           ORGANIZZAZIONE,STATO_RETE,LISTA_FONTI,LISTA_TIPI_RETE,LISTA_TIPI_CLIE,LISTA_INDICI_RICH,LISTA_TIPI_MISURA,
                           LISTA_TIPI_MISURA_FORECAST,FREQ_ERR_VAL_INI,FREQ_ERR_VAL_FIN,FREQ_ERR_PASSO,VALORE_SOGLIA,DELTA,SOGLIA_COLORE,
                           ELAB_INIZIO,ELAB_FINE,ELAB_STATO,ELAB_NOTE,TIPO_NOTIFICA,DATA_SCADENZA)
                    SELECT ID_REQ_KPI,DATA_RICHIESTA,OPERATORE,PERIODO_INIZIO,PERIODO_FINE,ORARIO_INIZIO,ORARIO_FINE,RISOLUZIONE,
                           ORGANIZZAZIONE,STATO_RETE,LISTA_FONTI,LISTA_TIPI_RETE,LISTA_TIPI_CLIE,LISTA_INDICI_RICH,LISTA_TIPI_MISURA,
                           LISTA_TIPI_MISURA_FORECAST,FREQ_ERR_VAL_INI,FREQ_ERR_VAL_FIN,FREQ_ERR_PASSO,VALORE_SOGLIA,DELTA,SOGLIA_COLORE,
                           ELAB_INIZIO,ELAB_FINE,ELAB_STATO,ELAB_NOTE,TIPO_NOTIFICA,TRUNC(SYSDATE)+30
                      FROM KPI_RICHIESTA_SAV
/                      
DROP TABLE KPI_RICHIESTA_SAV PURGE
/                    


PROMPT INDEX KPI_RICHIESTA_PK;
--
-- KPI_RICHIESTA_PK  (Index) 
--
--  Dependencies: 
--   KPI_RICHIESTA (Table)
--
CREATE UNIQUE INDEX KPI_RICHIESTA_PK ON KPI_RICHIESTA
(ID_REQ_KPI)
TABLESPACE &TBS&IDX
NOPARALLEL
/


-- 
-- Non Foreign Key Constraints for Table KPI_RICHIESTA 
-- 
PROMPT Non-FOREIGN KEY CONSTRAINTS ON TABLE KPI_RICHIESTA;
ALTER TABLE KPI_RICHIESTA ADD (
  CONSTRAINT KPI_RICHIESTA_PK
  PRIMARY KEY
  (ID_REQ_KPI)
  USING INDEX KPI_RICHIESTA_PK
  ENABLE VALIDATE)
/

ALTER TABLE KPI_ELEMENTI  ADD (CONSTRAINT KPI_ELE_KPI_REQ  FOREIGN KEY (ID_REQ_KPI)  REFERENCES KPI_RICHIESTA (ID_REQ_KPI) ON DELETE CASCADE ENABLE VALIDATE)
/
ALTER TABLE KPI_PARAMETRI ADD (CONSTRAINT KPI_PARAM_KPI_REQ FOREIGN KEY (ID_REQ_KPI) REFERENCES KPI_RICHIESTA (ID_REQ_KPI) ON DELETE CASCADE ENABLE VALIDATE)
/


COMMENT ON TABLE KPI_RICHIESTA IS 'Testata richiesta KPI - indici di errore'
/

COMMENT ON COLUMN KPI_RICHIESTA.ID_REQ_KPI IS 'Codice identificativo della richiesta'
/

COMMENT ON COLUMN KPI_RICHIESTA.DATA_RICHIESTA IS 'Data/Ora della richiesta'
/

COMMENT ON COLUMN KPI_RICHIESTA.DATA_SCADENZA IS 'Data di scadenza delle richiesta'
/

COMMENT ON COLUMN KPI_RICHIESTA.OPERATORE IS 'Operatore richiedente'
/

COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_INIZIO IS 'Inizio periodo temporale selezionato'
/

COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_FINE IS 'Fine periodo temporale selezionato'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_INIZIO IS 'Orario di inizio (alba) - Formato HH:MI'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_FINE IS 'Orario di fine (tramonto) - Formato HH:MI'
/

COMMENT ON COLUMN KPI_RICHIESTA.RISOLUZIONE IS 'Risoluzione temporale dell''analisi (minuti)'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORGANIZZAZIONE IS 'Organizzazione selezionata'
/

COMMENT ON COLUMN KPI_RICHIESTA.STATO_RETE IS 'Stato della rete selezionata'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_FONTI IS 'Elenco delle fonti richieste (come ricevute in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_RETE IS 'Elenco dei tipi rete richiesti (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_CLIE IS 'Elenco dei tipi cliente richiesti (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_INDICI_RICH IS 'Elenco degli indici da calcolare (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA IS 'Elencio dei tipi Misura Base'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA_FORECAST IS 'Elencio dei tipi Misura di Forecast'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_INI IS 'Frequenza di errore - valore iniziale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_INI_PERC IS 'Frequenza di errore - valore iniziale - percentuale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_FIN IS 'Frequenza di errore - valore finale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_FIN_PERC IS 'Frequenza di errore - valore finale - percentuale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_PASSO IS 'Frequenza di errore - passo'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_PASSO_PERC IS 'Frequenza di errore - passo - percentuale'
/

COMMENT ON COLUMN KPI_RICHIESTA.SOGLIA_COLORE IS 'Soglia per Colorazione FE'
/

COMMENT ON COLUMN KPI_RICHIESTA.SOGLIA_COLORE_PERC IS 'Soglia per Colorazione FE - percentuale'
/

COMMENT ON COLUMN KPI_RICHIESTA.VALORE_SOGLIA IS 'Valore di soglia'
/

COMMENT ON COLUMN KPI_RICHIESTA.DELTA IS 'Delta'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_INIZIO IS 'Ora Inizio Elaborazione'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_FINE IS 'Ora Fine Elaborazione'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_STATO IS 'Stato avanzamento della richiesta; 0=da elaborare,1=elab. in corso,2=elaborazione conclusa, 3=elaborazione interrotta,-1=errore di elaborazione (dettaglio in ELAB_NOTE)'
/

COMMENT ON COLUMN KPI_RICHIESTA.DATA_STATO IS 'data del cambio di stato'
/

COMMENT ON COLUMN KPI_RICHIESTA.OPER_STATO IS 'Operatore/Funzione che ha determinato il cambio di stato'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_NOTE IS 'Note sull''elaborazione'
/


PROMPT TRIGGER BEF_IUR_KPI_RICHIESTA;
--
-- BEF_IUR_KPI_RICHIESTA  (Trigger) 
--
--  Dependencies: 
--   KPI_RICHIESTA (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_KPI_RICHIESTA 
BEFORE INSERT OR UPDATE
ON KPI_RICHIESTA REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_KPI_RICHIESTA
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      18/08/2014   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF UPDATING THEN
        IF :NEW.ID_REQ_KPI <> :NEW.ID_REQ_KPI THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice KPI in tabella ''KPI_RICHIESTA''');
        END IF;
    END IF;
    IF INSERTING THEN
        IF :NEW.ID_REQ_KPI IS NULL THEN
            SELECT KPI_PKSEQ.NEXTVAL INTO :NEW.ID_REQ_KPI FROM DUAL;
            :NEW.ELAB_STATO := NVL(:NEW.ELAB_STATO,PKG_KPI.gcStatoDaElaborare); 
            :NEW.DATA_RICHIESTA := NVL(:NEW.DATA_RICHIESTA,SYSDATE);
        END IF;
    END IF;
    IF :NEW.ELAB_STATO NOT IN (PKG_KPI.gcStatoDaElaborare,  
                               PKG_KPI.gcStatoInElaborazione,
                               PKG_KPI.gcStatoElabTerminata,
                               PKG_KPI.gcStatoElabInterrotta,
                               PKG_KPI.gcStatoErroreDiElab
                              ) THEN
        RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||:NEW.ELAB_STATO||') per richiesta n. '||:NEW.ID_REQ_KPI);
    END IF;
    IF :NEW.ELAB_STATO <> :OLD.ELAB_STATO THEN
        :NEW.DATA_STATO := NVL(:NEW.DATA_STATO,SYSDATE);
    END IF;
END BEF_IUR_KPI_RICHIESTA;
/

