SPOOL rel_19a10_NAZ.LOG

PROMPT =======================================================================================
PROMPT rel_19a10_NAZ
PROMPT =======================================================================================

--conn mago/mago@arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_DGF <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE


PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MISURE.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MISURE.sql


PROMPT _______________________________________________________________________________________
PROMPT SchedulerJobs
PROMPT

@./SchedulerJobs/GENERA_FILE_GEO.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.10
PROMPT

SPOOL OFF

