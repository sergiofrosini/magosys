PROMPT SCHEDULER JOB GENERA_FILES_GEO;
--
-- GENERA_FILES_GEO  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('GENERA_FILES_GEO', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'GENERA_FILES_GEO',
                 job_type             => 'stored_procedure',
                 job_action           => 'pkg_genera_file_geo.genera_files',
                 start_date           => TRUNC (SYSDATE) + (1/24 * 21),
                 repeat_interval      => 'FREQ=DAILY; INTERVAL=1',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Eseguo la generazione dei files di anagrafica punti e correlazione'
                );
END;
/
