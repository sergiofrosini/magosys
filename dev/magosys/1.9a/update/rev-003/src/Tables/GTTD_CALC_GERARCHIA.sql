PROMPT TABLE GTTD_CALC_GERARCHIA;
--
-- GTTD_CALC_GERARCHIA  (Table) 
--
--
BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE GTTD_CALC_GERARCHIA PURGE';
    DBMS_OUTPUT.PUT_LINE('Tabella GTTD_CALC_GERARCHIA droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE GLOBAL TEMPORARY TABLE GTTD_CALC_GERARCHIA
(
  COD_ELEMENTO  NUMBER,
  L01           NUMBER,
  L02           NUMBER,
  L03           NUMBER,
  L04           NUMBER,
  L05           NUMBER,
  L06           NUMBER,
  L07           NUMBER,
  L08           NUMBER,
  L09           NUMBER,
  L10           NUMBER,
  L11           NUMBER,
  L12           NUMBER,
  L13           NUMBER,
  L14           NUMBER,
  L15           NUMBER,
  L16           NUMBER,
  L17           NUMBER,
  L18           NUMBER,
  L19           NUMBER,
  L20           NUMBER,
  L21           NUMBER,
  L22           NUMBER,
  L23           NUMBER,
  L24           NUMBER,
  L25           NUMBER
)
ON COMMIT PRESERVE ROWS
/


PROMPT INDEX GTTD_CALC_GERARCHIA_PK;
--
-- GTTD_CALC_GERARCHIA_PK  (Index) 
--
--  Dependencies: 
--   GTTD_CALC_GERARCHIA (Table)
--
CREATE INDEX GTTD_CALC_GERARCHIA_PK ON GTTD_CALC_GERARCHIA (COD_ELEMENTO)
/
