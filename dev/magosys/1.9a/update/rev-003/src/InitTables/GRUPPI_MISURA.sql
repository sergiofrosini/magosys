PROMPT MERGE GRUPPI_MISURA;

MERGE INTO GRUPPI_MISURA A
     USING (
            SELECT 9999999999 AS COD_GRUPPO,
                   'ESE' AS COD_TIPO_ELEMENTO,
                   'PAS-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   2 AS SEQ_ORD,
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'ESE' AS COD_TIPO_ELEMENTO,
                   'PRI-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   9 AS SEQ_ORD,
                   0 AS DISATTIVATO
                   FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO, 
                   'CPR' AS COD_TIPO_ELEMENTO, 
                   'PAS-TR' AS COD_TIPO_MISURA, 
                   0 AS FLAG_ATTIVO,
                   2 AS SEQ_ORD, 
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                  'CPR' AS COD_TIPO_ELEMENTO,
                  'PRI-TR' AS COD_TIPO_MISURA,
                  0 AS FLAG_ATTIVO,
                  9 AS SEQ_ORD,
                  0 AS DISATTIVATO
             FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                  'TRF' AS COD_TIPO_ELEMENTO,
                  'PAS-TR' AS COD_TIPO_MISURA,
                  0 AS FLAG_ATTIVO,
                  2 AS SEQ_ORD,
                  0 AS DISATTIVATO
             FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'TRF' AS COD_TIPO_ELEMENTO,
                   'PRI-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   9 AS SEQ_ORD,
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'TRS' AS COD_TIPO_ELEMENTO,
                   'PAS-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   2 AS SEQ_ORD,
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'TRS' AS COD_TIPO_ELEMENTO,
                   'PRI-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   9 AS SEQ_ORD,
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'TRT' AS COD_TIPO_ELEMENTO,
                   'PAS-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   2 AS SEQ_ORD,
                   0 AS DISATTIVATO
              FROM DUAL
            UNION ALL
            SELECT 9999999999 AS COD_GRUPPO,
                   'TRT' AS COD_TIPO_ELEMENTO,
                   'PRI-TR' AS COD_TIPO_MISURA,
                   0 AS FLAG_ATTIVO,
                   9 AS SEQ_ORD,
                   0 AS DISATTIVATO
                   FROM DUAL
           ) B  ON (    A.COD_GRUPPO = B.COD_GRUPPO 
                    AND A.COD_TIPO_ELEMENTO = B.COD_TIPO_ELEMENTO 
                    AND A.COD_TIPO_MISURA = B.COD_TIPO_MISURA)
    WHEN NOT MATCHED THEN
        INSERT (COD_GRUPPO, COD_TIPO_ELEMENTO, COD_TIPO_MISURA, FLAG_ATTIVO, SEQ_ORD, DISATTIVATO)
        VALUES (B.COD_GRUPPO, B.COD_TIPO_ELEMENTO, B.COD_TIPO_MISURA, B.FLAG_ATTIVO, B.SEQ_ORD,B.DISATTIVATO)
    WHEN MATCHED THEN
        UPDATE SET A.FLAG_ATTIVO = B.FLAG_ATTIVO,
                   A.SEQ_ORD = B.SEQ_ORD,
                   A.DISATTIVATO = B.DISATTIVATO;

DECLARE
    ind INTEGER := 0;
BEGIN
    FOR i IN (SELECT DISTINCT COD_GRUPPO 
                FROM GRUPPI_MISURA 
               ORDER BY 1
             ) LOOP
        FOR x IN (SELECT DISTINCT COD_TIPO_ELEMENTO 
                    FROM GRUPPI_MISURA 
                   WHERE COD_GRUPPO = i.COD_GRUPPO 
                   ORDER BY 1
                 ) LOOP
            ind := 0;
            FOR y IN (SELECT DISTINCT COD_TIPO_MISURA , SEQ_ORD
                        FROM GRUPPI_MISURA 
                       WHERE COD_GRUPPO = i.COD_GRUPPO
                         AND COD_TIPO_ELEMENTO = x.COD_TIPO_ELEMENTO 
                       ORDER BY SEQ_ORD
                     ) LOOP
                ind := ind + 1;
                --DBMS_OUTPUT.PUT_LINE(i.COD_GRUPPO||' - '||x.COD_TIPO_ELEMENTO||' - '||y.COD_TIPO_MISURA||' > '||y.SEQ_ORD||' - '||ind);
                UPDATE GRUPPI_MISURA SET SEQ_ORD = ind
                 WHERE COD_GRUPPO = i.COD_GRUPPO 
                   AND COD_TIPO_ELEMENTO = x.COD_TIPO_ELEMENTO 
                   AND COD_TIPO_MISURA = y.COD_TIPO_MISURA;
            END LOOP;
        END LOOP;
    END LOOP;
END;
/
                   
                   
COMMIT;