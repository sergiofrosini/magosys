PROMPT MERGE TIPI_MISURA_CONV_UM;


MERGE INTO TIPI_MISURA_CONV_UM A
     USING (SELECT 'PAS-TR'  AS COD_TIPO_MISURA, 
                   'kW'      AS TIPO_MISURA_CONV, 
                   1000      AS FATTORE_MOLTIPLICATIVO, 
                   NULL      AS FORMULA 
              FROM DUAL  
            UNION ALL
            SELECT 'PAS-TR'  AS COD_TIPO_MISURA,
                   'W'       AS TIPO_MISURA_CONV,
                   1         AS FATTORE_MOLTIPLICATIVO,
                   NULL      AS FORMULA
              FROM DUAL
            UNION ALL
            SELECT 'PRI-TR'  AS COD_TIPO_MISURA,
                   'kVAr'    AS TIPO_MISURA_CONV,
                   1000      AS FATTORE_MOLTIPLICATIVO,
                   NULL      AS FORMULA
              FROM DUAL
            UNION ALL
            SELECT 'PRI-TR'  AS COD_TIPO_MISURA,
                   'VAr'     AS TIPO_MISURA_CONV,
                   1         AS FATTORE_MOLTIPLICATIVO,
                   NULL      AS FORMULA
             FROM DUAL
           ) B ON (    A.COD_TIPO_MISURA = B.COD_TIPO_MISURA 
                   AND A.TIPO_MISURA_CONV = B.TIPO_MISURA_CONV
                  )
    WHEN NOT MATCHED THEN
        INSERT (COD_TIPO_MISURA, TIPO_MISURA_CONV, FATTORE_MOLTIPLICATIVO, FORMULA)
        VALUES (B.COD_TIPO_MISURA, B.TIPO_MISURA_CONV, B.FATTORE_MOLTIPLICATIVO, B.FORMULA)
    WHEN MATCHED THEN
        UPDATE SET A.FATTORE_MOLTIPLICATIVO = B.FATTORE_MOLTIPLICATIVO,
                   A.FORMULA = B.FORMULA;

COMMIT;