PROMPT VIEW V_GERARCHIA_IMPIANTO_AT_MT;
--
-- V_GERARCHIA_IMPIANTO_AT_MT  (View) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--   V_MAGO_REL_IMP_AT_MT (View)
--   PKG_ELEMENTI (Package)
--   V_ESERCIZI (View)
--   ESERCIZI (Table)
--
CREATE OR REPLACE VIEW V_GERARCHIA_IMPIANTO_AT_MT
AS 
SELECT /*  
         ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                     NON eliminare prefissi di schema !!!! 
       */ 
       NVL(P.COD_TIPO_ELEMENTO,'COP')||'-'||F.COD_TIPO_ELEMENTO tipo,
       NVL(P.COD_ELEMENTO,PKG_ELEMENTI.GetElementoBase)         padre,
       F.COD_ELEMENTO                                           figlio
  FROM (WITH MyEsercizi AS (SELECT E.COD_GEST COD_GEST_BASE,
                                   CASE WHEN V.GST_ESE IS NULL THEN E.COD_GEST||'_'||E.ESE
                                                               ELSE V.GST_ESE
                                   END COD_GEST
                              FROM CORELE.ESERCIZI E
                              LEFT OUTER JOIN V_ESERCIZI V ON V.GST_ESE = E.COD_GEST
                             WHERE SYSDATE BETWEEN E.DATA_INIZIO AND E.DATA_FINE
                           )
        SELECT V.GEST_PADRE,
               V.TIPO_PADRE,
               CASE V.TIPO_FIGLIO
                    WHEN 'ESER' 
                        THEN (SELECT COD_GEST 
                                FROM MyEsercizi
                               WHERE COD_GEST_BASE = v.GEST_FIGLIO
                             )
                        ELSE v.GEST_FIGLIO
               END GEST_FIGLIO,
               V.TIPO_FIGLIO
          FROM CORELE.V_MAGO_REL_IMP_AT_MT V
       ) A
  LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.GEST_PADRE
 INNER JOIN       ELEMENTI F ON F.COD_GEST_ELEMENTO = A.GEST_FIGLIO
 WHERE CASE WHEN P.COD_ELEMENTO IS NOT NULL 
                THEN 1
                ELSE CASE WHEN F.COD_TIPO_ELEMENTO = 'ESE' 
                              THEN 1
                              ELSE 0
                     END
       END = 1
 ORDER BY PKG_ELEMENTI.GetOrderByElemType(F.COD_TIPO_ELEMENTO,F.COD_GEST_ELEMENTO),
          CASE SUBSTR(F.COD_GEST_ELEMENTO,1,1)
               WHEN 'D' THEN 1
               WHEN 'A' THEN 2
                        ELSE 3
          END, 
          F.COD_GEST_ELEMENTO
/
