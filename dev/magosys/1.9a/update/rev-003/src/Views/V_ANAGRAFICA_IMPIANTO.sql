PROMPT VIEW V_ANAGRAFICA_IMPIANTO;
--
-- V_ANAGRAFICA_IMPIANTO  (View) 
--
--  Dependencies: 
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   V_MAGO_ANAGRAFICA_BASE (View)
--   PKG_ELEMENTI (Package)
--   V_ESERCIZI (View)
--   ESERCIZI (Table)
--
CREATE OR REPLACE VIEW V_ANAGRAFICA_IMPIANTO
AS 
WITH 
     /*  
       ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                   NON eliminare prefissi di schema !!!! 
     */ 
     MyAngrBase AS (SELECT A.COD_GEST COD_GEST_ELEMENTO,
                           A.NOME NOME_ELEMENTO,
                           T.COD_TIPO_ELEMENTO  COD_TIPO_ELEMENTO,
                           CASE T.COD_TIPO_ELEMENTO
                             WHEN 'CPR' THEN CASE A.CENTROSATELLITE
                                                 WHEN 'SI' THEN 1
                                                           ELSE 0
                                             END
                             ELSE NULL
                           END FLAG,
                           TRIM (ID_ELEM) ID_ELEMENTO,
                           TRIM (RIF_ELEM) RIF_ELEMENTO
                      FROM CORELE.V_MAGO_ANAGRAFICA_BASE A
                     INNER JOIN TIPI_ELEMENTO T ON T.CODIFICA_ST = A.COD_ENTE
                     WHERE T.COD_TIPO_ELEMENTO <> 'ESE'
                   )
   , MyAngrAT   AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO IN ('CPR','TRF','TRS','TRT','LMT','SMT')
                   )
   , MyAngrSCS  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO = 'SCS'
                   )
   , MyAngrCLI  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO IN ('CMT','CAT','CBT')
                   )
   , MyAngrTRM  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO = 'TRM'
                   )
SELECT *
  FROM (/*CENTRO OPERATIVO */
        SELECT E.COD_GEST_ELEMENTO,
               D.NOME_ELEMENTO,
               D.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               NULL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               TO_NUMBER(NULL) FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y  
          FROM ELEMENTI E 
         INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
         WHERE COD_ELEMENTO = PKG_Elementi.GetElementoBase
        UNION ALL /* ESERCIZI */
        SELECT E.COD_ESE COD_GEST_ELEMENTO,
               E.NOME NOME_ELEMENTO,
               'ESE' COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               NULL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               V.ESE_PRIMARIO FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM (SELECT CASE 
                          WHEN SUBSTR(COD_GEST,3,2) = E.ESE THEN E.COD_GEST
                          ELSE E.COD_GEST || '_' || E.ESE
                       END COD_ESE,
                       E.NOME,
                       E.COD_ENTE,
                       E.ESE
                  FROM CORELE.ESERCIZI E 
                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
               ) E
         LEFT OUTER JOIN V_ESERCIZI V ON E.COD_ESE = V.GST_ESE
        UNION ALL /* ELEMENTI AT/MT ( CS Escluse )*/
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               A.RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y  
          FROM MyAngrAT A
        UNION ALL /* SBARRE DI CABINA SECONDARIA */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               'X' COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               B.COD_CFT RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               B.GPS_X COORDINATA_X,
               B.GPS_Y COORDINATA_Y
          FROM MyAngrSCS A
          LEFT OUTER JOIN NODI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                   AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                   AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                   AND B.TRATTAMENTO  = 0
                                                   AND B.STATO        = 'E' 
        UNION ALL /* CLIENTI */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               CASE TIPO_FORN
                    WHEN 'PP' THEN 'A'
                    WHEN 'AP' THEN 'B'
                    ELSE 'C'
               END COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               B.POD RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               B.POT_GRUPPI POTENZA_INSTALLATA,
               B.POT_DISP POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y  
          FROM MyAngrCLI A
          LEFT OUTER JOIN CLIENTI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO  = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                      AND B.SER_NODO      = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                      AND B.NUM_NODO      = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                      AND B.TIPO_ELEMENTO = SUBSTR(A.COD_GEST_ELEMENTO,12,1)
                                                      AND B.ID_CLIENTE    = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                      AND B.TRATTAMENTO   = 0
                                                      AND B.STATO         = 'E' 
        UNION ALL /* GENERATORI (clienti) */
        SELECT COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_GENERATORE COD_GEST_ELEMENTO,
               NULL NOME_ELEMENTO,
               CASE A.COD_TIPO_ELEMENTO
                    WHEN 'CAT' THEN 'GAT'
                    WHEN 'CMT' THEN 'GMT'
                    WHEN 'CBT' THEN 'GBT'
               END COD_TIPO_ELEMENTO,
               B.TIPO_IMP COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               B.COD_ORG_NODO||B.SER_NODO||B.NUM_NODO||'U'||B.ID_CL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               TO_NUMBER(NULL) FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               (B.P_APP_NOM * NVL(B.N_GEN_PAR,1)) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               B.F_P_NOM FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y  
          FROM MyAngrCLI A
          LEFT OUTER JOIN GENERATORI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                         AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                         AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                         AND B.ID_CL        = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                         AND B.TRATTAMENTO  = 0
                                                         AND B.STATO        = 'E'
        UNION ALL /* TRASFORMATORI MT/BT */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               'X' COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               SUM (B.POT_PROD) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y  
          FROM MyAngrTRM A
          LEFT OUTER JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                            AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                            AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                            AND B.TIPO_ELE     = SUBSTR(A.COD_GEST_ELEMENTO,12,1)
                                                            AND B.ID_TRASF     = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                            AND B.TRATTAMENTO  = 0
                                                            AND B.STATO        = 'E'
         GROUP BY A.COD_GEST_ELEMENTO,A.NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,A.ID_ELEMENTO,A.FLAG
        UNION ALL /* GENERATORI FITTIZI DI TRASFORMATORI MT/BT */
        SELECT A.COD_GEST_ELEMENTO||'_'|| NVL(TIPO_GEN,'3') COD_GEST_ELEMENTO,
               NULL NOME_ELEMENTO,
               'TRX' COD_TIPO_ELEMENTO,
               NVL(B.TIPO_GEN,'3') COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               A.COD_GEST_ELEMENTO ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               NULL FLAG,
               NVL(B.NUM_PROD,0) NUM_IMPIANTI,
               NVL(B.POT_PROD,0) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrTRM A 
          LEFT OUTER JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO ,1,4) 
                                                            AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO ,5,1) 
                                                            AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO ,6,6) 
                                                            AND B.TIPO_ELE     = SUBSTR(A.COD_GEST_ELEMENTO ,12,1)
                                                            AND B.ID_TRASF     = SUBSTR(A.COD_GEST_ELEMENTO ,13,2)
       )
 ORDER BY PKG_Elementi.GetOrderByElemType(COD_TIPO_ELEMENTO,COD_GEST_ELEMENTO),
       CASE SUBSTR(COD_GEST_ELEMENTO,1,1)
                               WHEN 'D' THEN 1
                               WHEN 'A' THEN 2
                                        ELSE 3
       END, 
       CASE COD_TIPO_ELEMENTO||FLAG 
            WHEN 'ESE1' THEN 0
            WHEN 'ESE0' THEN 1
            WHEN 'CPR0' THEN 0
            WHEN 'CPR1' THEN 1
       END,
       COD_GEST_ELEMENTO
/
