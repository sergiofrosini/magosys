PROMPT PACKAGE PKG_KPI;
--
-- PKG_KPI  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   PLITBLM (Synonym)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   PKG_ELEMENTI (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--   PLITBLM ()
--   KPI_INDICI (Table)
--   KPI_RICHIESTA (Table)
--   KPI_RICHIESTA (Table)
--   GTTD_VALORI_TEMP (Table)
--   T_COD_GEST_ARRAY (Type)
--   T_COD_GEST_ARRAY (Type)
--   T_KPI_MIS_ARRAY (Type)
--   T_KPI_MIS_ARRAY (Type)
--   T_KPI_RICHIESTA_OBJ (Type)
--   T_KPI_RICHIESTA_OBJ (Type)
--   GTTD_MISURE (Table)
--   KPI_PARAMETRI (Table)
--   KPI_PARAMETRI (Table)
--   KPI_ELEMENTI (Table)
--   KPI_ELEMENTI (Table)
--
CREATE OR REPLACE PACKAGE PKG_KPI AS

/* ***********************************************************************************************************
   NAME:       PKG_KPI
   PURPOSE:    Servizi per la gestione degli Indici di errore (KPI)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.9.a.1    18/08/2014  Moretti C.       Created this package.
   1.9.a.2    23/09/2014  Moretti C.       Procedura GetElementsByReq - Aggiunta colonna NOME a cursore
   1.9.a.3    06/10/2014  Moretti C.       Aggiunta colonna TIPO_NOTIFICA in KPI_RICHIESTA

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

-- Stati Elaborazione
    gcStatoDaElaborare              KPI_RICHIESTA.ELAB_STATO%TYPE := 0;  
    gcStatoInElaborazione           KPI_RICHIESTA.ELAB_STATO%TYPE := 1;  
    gcStatoElabTerminata            KPI_RICHIESTA.ELAB_STATO%TYPE := 2;  
    gcStatoErroreDiElab             KPI_RICHIESTA.ELAB_STATO%TYPE := -1;  

-- Tipi Parametro
    gcTipFonti                      KPI_PARAMETRI.TIPO%TYPE := 'TIPFTE';
    gcTipReti                       KPI_PARAMETRI.TIPO%TYPE := 'TIPRET';  
    gcTipClienti                    KPI_PARAMETRI.TIPO%TYPE := 'TIPCLI';  
    gcIndici                        KPI_PARAMETRI.TIPO%TYPE := 'TIPKPI';
    gcTipiMisura                    KPI_PARAMETRI.TIPO%TYPE := 'TIPMIS';
    gcTipiMisForecast               KPI_PARAMETRI.TIPO%TYPE := 'TIPMISFOR';

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertRichiesta   (pNewReq            IN T_KPI_RICHIESTA_OBJ,
                              pElementi          IN T_COD_GEST_ARRAY,
                              pID_REQ_KPI       OUT NUMBER);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRichieste      (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE DEFAULT NULL);

 PROCEDURE GetRichiesta      (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsByReq  (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetStatoRichiesta (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pELAB_STATO        IN KPI_RICHIESTA.ELAB_STATO%TYPE,
                              pELAB_NOTE         IN KPI_RICHIESTA.ELAB_NOTE%TYPE DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetStatoElemento  (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCOD_ELEMENTO      IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pSTATO             IN KPI_ELEMENTI.STATO%TYPE);

 PROCEDURE SetStatoElementoCG(pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCOD_GEST_ELEMENTO IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pSTATO             IN KPI_ELEMENTI.STATO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertValori      (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pTabValIndici      IN T_KPI_MIS_ARRAY);

-- ----------------------------------------------------------------------------------------------------------

 --PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
 --                             pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE,
 --                             pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
 --                             pDataDa            IN DATE,
 --                             pDataA             IN DATE,
 --                             pTipiIndice        IN VARCHAR2);

 PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE DEFAULT NULL,
                              pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------


END PKG_KPI;
/
SHOW ERRORS;


