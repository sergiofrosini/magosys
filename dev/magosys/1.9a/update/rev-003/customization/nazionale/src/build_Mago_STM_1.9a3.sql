SPOOL rel_19a3_NAZ.log

WHENEVER SQLERROR CONTINUE

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_NAZ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT

@./Types/T_KPI_RICHIESTA_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT

@./Sequences/FILEINFO_SEQUENCE.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

@./Tables/GERARCHIA_AMM.sql
@./Tables/GERARCHIA_GEO.sql
@./Tables/GERARCHIA_IMP_SA.sql
@./Tables/GERARCHIA_IMP_SN.sql
@./Tables/GTTD_CALC_GERARCHIA.sql
@./Tables/KPI_RICHIESTA.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT

@./Packages/PKG_MAGO.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_AGGREGAZIONI.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBody 
PROMPT

@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_AGGREGAZIONI.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT Update Tables
PROMPT

@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPI_MISURA_CONV_UM.sql
@./InitTables/TIPI_MISURA_CONV_ORIG.sql
@./InitTables/GRUPPI_MISURA.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.3
PROMPT

SPOOL OFF

