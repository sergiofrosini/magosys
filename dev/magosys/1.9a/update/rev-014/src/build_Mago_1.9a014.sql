SPOOL rel_19a14_STM.log


PROMPT =======================================================================================
PROMPT rel_19a15_STM
PROMPT =======================================================================================

conn mago/mago

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_STM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ESERCIZI.sql
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql
@./Views/V_GERARCHIA_GEOGRAFICA.sql
@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./Views/V_SCHEDULED_JOBS.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_SCHEDULER.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_SCHEDULER.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables

@./InitTables/V_PARAMETRI_APPLICATIVI.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9a.14
PROMPT
