PROMPT MERGE V_PARAMETRI_APPLICATIVI

MERGE INTO V_PARAMETRI_APPLICATIVI A USING
 (SELECT 'SCHED_WAIT' AS KEY, '6' AS VALORE, 'Secondi di attesa tra uno step di schedulazione (aggregazione) e l''altro' AS NOTA FROM DUAL) B
    ON (A.KEY = B.KEY)
        WHEN NOT MATCHED THEN INSERT (KEY, VALORE, NOTA)
                              VALUES (B.KEY, B.VALORE, B.NOTA)
        WHEN MATCHED THEN UPDATE SET A.VALORE = B.VALORE,
                                     A.NOTA = B.NOTA;

COMMIT;
