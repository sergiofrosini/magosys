PROMPT VIEW V_ESERCIZI;
--
-- V_ESERCIZI  (View)
--
--  Dependencies:
--   ELEMENTI (Table)
--   MACRO_AREE (Table)
--   UNITA_TERRITORIALI (Table)
--   ESERCIZI_ABILITATI (Table)
--   ESERCIZI (Table)
--

CREATE OR REPLACE VIEW V_ESERCIZI AS
    SELECT X.COD_MA,
           X.NOM_MA,
           X.ACR_MA,
           X.COD_UTR,
           X.GST_UTR, 
           X.NOM_UTR,
           X.ACR_UTR,X.COD_ESERCIZIO,
           X.GST_ESE, 
           UPPER(NVL(D.NOME,X.NOM_ESE)) NOM_ESE,
           X.ESE,
           X.ACR_ESE,
           X.ESE_PRIMARIO,
           E.COD_ELEMENTO COD_ESE
        FROM (SELECT M.COD_MACRO_AREA COD_MA,
                     M.NOME NOM_MA,
                     CASE M.COD_MACRO_AREA  /* Acroniomo Macro Area provvisoriamente valorizzato fittiziamente */
                             WHEN 1 THEN 'DANO' 
                             WHEN 2 THEN 'DANE' 
                             WHEN 3 THEN 'DAC' 
                             WHEN 4 THEN 'DAS'
                     END ACR_MA,
                     U.COD_UTR COD_UTR,
                     U.NOME NOM_UTR,
                     U.ACRONIMO ACR_UTR,
                     U.CODIFICA_STM GST_UTR,
                     E.COD_ESERCIZIO COD_ESE,
                     CODIFICA_UTR || CODIFICA_ESERCIZIO GST_ESE,
                     UPPER(E.NOME) NOM_ESE,
                     E.ACRONIMO ACR_ESE,
                     E.COD_ESERCIZIO,
                     CODIFICA_ESERCIZIO ESE,
                     CASE
                         WHEN A.COD_UTR = E.COD_UTR
                          AND A.COD_ESERCIZIO = E.COD_ESERCIZIO
                                THEN 1
                                ELSE 0
                     END ESE_PRIMARIO
                FROM SAR_ADMIN.ESERCIZI_ABILITATI A
                     INNER JOIN SAR_ADMIN.UNITA_TERRITORIALI U ON U.COD_UTR = A.COD_UTR
                     INNER JOIN SAR_ADMIN.MACRO_AREE M ON M.COD_MACRO_AREA = U.COD_MACRO_AREA
                     INNER JOIN SAR_ADMIN.ESERCIZI E ON E.COD_UTR = A.COD_UTR
               WHERE COD_APPLICAZIONE = 'MAGO'
                 AND E.CODIFICA_ESERCIZIO <> '00'
                 AND U.COD_UTR BETWEEN 1 AND 99
                 AND U.FLAG_DISATTIVATO = 0
                 AND E.COD_ESERCIZIO BETWEEN 1 AND 99
                 AND E.FLAG_DISATTIVATO = 0
             ) X
        LEFT OUTER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = X.GST_ESE
        LEFT OUTER JOIN CORELE.ESERCIZI D ON D.COD_GEST = X.GST_ESE
                                         AND SYSDATE BETWEEN D.DATA_INIZIO AND D.DATA_FINE     
       ORDER BY ESE_PRIMARIO DESC, NVL(D.NOME,X.NOM_ESE), GST_ESE
/

COMMENT ON COLUMN V_ESERCIZI.ACR_ESE IS 'Acronimo dell''Esercizio  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.ACR_MA IS 'Acronimo della MACRO AREA (calcolato nella vista)'
/

COMMENT ON COLUMN V_ESERCIZI.ACR_UTR IS 'Acronimo dell''Unit� Territoriale  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.COD_ESE IS 'COD_ELEMENTO dell esercizio (da  MAGO)'
/

COMMENT ON COLUMN V_ESERCIZI.COD_ESERCIZIO IS 'Codice dell''Esercizio  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.COD_MA IS 'Codice SAR_ADMIN della MACRO AREA (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.COD_UTR IS 'Codice dell''Unit� Territoriale  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.ESE IS 'Identificativo dell''Esercizio (da SR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.ESE_PRIMARIO IS '1=Esercizio Primario - 0=Esercizio Secondario'
/

COMMENT ON COLUMN V_ESERCIZI.GST_ESE IS 'Codice Gestionale dell''Esercizio  (calcolato: GST_UTR + ESE)'
/

COMMENT ON COLUMN V_ESERCIZI.GST_UTR IS 'Codice Gestionale dell''Unit� Territoriale  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.NOM_ESE IS 'Nome dell''Esercizio  (calcolato: Upper di nome esercizio SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.NOM_MA IS 'Nome della MACRO AREA  (da SAR_ADMIN)'
/

COMMENT ON COLUMN V_ESERCIZI.NOM_UTR IS 'Nome dell''Unit� Territoriale  (da SAR_ADMIN)'
/



