PROMPT VIEW V_GERARCHIA_AMMINISTRATIVA;
--
-- V_GERARCHIA_AMMINISTRATIVA  (View)
--
--  Dependencies:
--   CFT (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   ESERCIZI (Table)
--   GERARCHIA_IMP_SA (Table)
--   V_ESERCIZI (View)
--   ZONE (Table)
--

CREATE OR REPLACE VIEW V_GERARCHIA_AMMINISTRATIVA AS
    SELECT ESE_GST,
           UPPER(SUBSTR(NVL(D.NOME,A.ESE_NOM), 1, 60)) ESE_NOM,
           ZNA_GST,
           SUBSTR (ZNA_NOM, 1, 60) ZNA_NOM,
           ZNA_X,
           ZNA_Y,
           CFT_GST,
           SUBSTR (CFT_NOM, 1, 60) CFT_NOM,
           CFT_X,
           CFT_Y,
           COD_GEST_ELEMENTO SCS_GST,
           NOME_ELEMENTO SCS_NOM,
           COORDINATA_X SCS_X,
           COORDINATA_Y SCS_Y,
           COD_ELEMENTO SCS_COD
      FROM (SELECT E.COD_ESE ESE_COD,
                   E.GST_ESE ESE_GST,
                   E.NOM_ESE ESE_NOM,
                   CASE
                       WHEN COD_UTR = 5
                         AND COD_ESERCIZIO = 4
                        AND COD_ZONA = 4 THEN
                           'LG' || Z.CODIFICA_ZONA
                       ELSE
                           GST_UTR || Z.CODIFICA_ZONA
                   END ZNA_GST,
                   Z.NOME ZNA_NOM,
                   Z.LONGITUDINE_GPS ZNA_X,
                   Z.LATITUDINE_GPS ZNA_Y,
                   C.CODIFICA_COMPLETA_CFT CFT_GST,
                   C.NOME CFT_NOM,
                   C.LONGITUDINE_GPS CFT_X,
                   C.LATITUDINE_GPS CFT_Y,
                   ESE_PRIMARIO
              FROM V_ESERCIZI E
                   INNER JOIN (SELECT COD_UTR, COD_ESERCIZIO, COD_ZONA, CODIFICA_ZONA, NOME, LATITUDINE_GPS, LONGITUDINE_GPS
                                 FROM SAR_ADMIN.ZONE
                                WHERE COD_ZONA != 9999) Z
                       USING (COD_UTR, COD_ESERCIZIO)
                   INNER JOIN
                   (SELECT COD_UTR, COD_ESERCIZIO, COD_ZONA, COD_CFT, CODIFICA_CFT, CODIFICA_COMPLETA_CFT, NOME, LATITUDINE_GPS
                         , LONGITUDINE_GPS
                      FROM SAR_ADMIN.CFT) C
                       USING (COD_UTR, COD_ESERCIZIO, COD_ZONA)
           ) A
      LEFT OUTER JOIN (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, RIF_ELEMENTO, NOME_ELEMENTO, COORDINATA_X, COORDINATA_Y
                         FROM ELEMENTI E
                        INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
                        INNER JOIN GERARCHIA_IMP_SA R USING (COD_ELEMENTO)
                        WHERE E.COD_TIPO_ELEMENTO = 'SCS'
                          AND SYSDATE BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                          AND SYSDATE BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE
                      ) ON RIF_ELEMENTO = CFT_GST
      LEFT OUTER JOIN CORELE.ESERCIZI D ON D.COD_GEST = A.ESE_GST
                                       AND SYSDATE BETWEEN D.DATA_INIZIO AND D.DATA_FINE
     ORDER BY ESE_PRIMARIO DESC, ZNA_GST, CFT_GST, COD_ELEMENTO
/


