PROMPT =======================================================================================
PROMPT rel_19a12_NAZ
PROMPT =======================================================================================

--conn mago/mago@arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_NAZ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT
PROMPT
PROMPT ==============================================================
PROMPT === NESSUNA MODIFICA PER MAGO 1.9a rev-012 in ambiente NAZ ===
PROMPT ==============================================================
PROMPT
PROMPT

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.12
PROMPT


