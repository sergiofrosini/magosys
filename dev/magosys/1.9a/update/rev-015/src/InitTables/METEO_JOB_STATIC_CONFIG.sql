PROMPT METEO_JOB_STATIC_CONFIG


--ACCEPT name2   CHAR  FORMAT a16  DEFAULT 'I-EM'         PROMPT 'inserire nome convenzionale supplier2 (I-EM): '
--ACCEPT geotype2 CHAR  FORMAT a5  DEFAULT 'P'         PROMPT 'inserire geotype supplier2 in [A,P,C] (P): '
--ACCEPT ftp_yesno2 CHAR  FORMAT a5  DEFAULT 'true'         PROMPT 'inserire true o false per supplier2 use.ftp(true): '
--ACCEPT src_host2  CHAR  FORMAT a16 DEFAULT 'pkg_ARCDB2'   PROMPT 'inserire IP supplier2 ftp.source.host(pkg_ARCDB2): '
--ACCEPT usrftp2    CHAR  FORMAT a16 DEFAULT 'magosys'      PROMPT 'inserire username supplier2 per ftp(magosys): '
--ACCEPT paswwd2    CHAR  FORMAT a16 DEFAULT 'Magosys'      PROMPT 'inserire passwd supplier2 per ftp(Magosys): '
--ACCEPT src_path2  CHAR  FORMAT a64 DEFAULT '/usr/NEW/magosys/meteofile/iem' PROMPT 'inserire path ftp.source.path.2(/usr/NEW/magosys/meteofile/iem): '
--ACCEPT forecast2  CHAR  FORMAT a64 DEFAULT 'PMP.P1'  PROMPT 'inserire forecast curve name supplier1 (PMP.P1): '
--ACCEPT forecast_cs2  CHAR  FORMAT a64 DEFAULT 'PMC.P1'  PROMPT 'inserire forecast clearsky curve name supplier1 (PMC.P1): '

define name2='I-EM'
define geotype2='P'
define ftp_yesno2='true'
define dsftp_yesno2='false'
define usftp_yesno2='false'
define ftpport2='21'
define src_host2='pkg_ARCDB2'
define usrftp2='magosys'
define paswwd2='Magosys'
define src_path2='/usr/NEW/magosys/meteofile/iem'
define forecast2='PMP.P1'
define forecast_cs2='PMC.P1'

--Supplier FlyBy
MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MFM.supplier.conventional.name.2' key ,'&name2' value  FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useFtp.2','&ftp_yesno2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useSftp.2','&dsftp_yesno2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.upload.useSftp.2','&usftp_yesno2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.port.2','&ftpport2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.2','&src_host2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.2','&usrftp2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.2','&paswwd2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.useProxy.2','false' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyHost.2','' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyPort.2','' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyUser.2','' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyPass.2','' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.folder.source.path.2','&src_path2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.local.destination.path.2','./tmp2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.remote.mantaining.day.2','30' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.enabled.2','false' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.forward.hour.2','48' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.host.2','127.0.0.1' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.username.2','ftpuser' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.password.2','ftpuser' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.path.2','/2' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.useProxy.2','false' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.proxy.host.2','' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.proxy.port.2','' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.proxy.username.2','' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.ftp.proxy.password.2','' FROM dual
             UNION ALL
            SELECT 'MPS.supplier.prediction.export.daysToKeepZipFile.2','30' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.point.type.2','&geotype2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.forecast.curve.name.2','&forecast2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.forecast.clearsky.curve.name.2','&forecast_cs2' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.extension.2','.zip' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.match.first.2','_01_xml' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.match.second.2','_00_xml' FROM dual
             UNION ALL
            SELECT 'MFM.supplier.forecast.acquired.curve.name.2', '' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.ftp.destination.path.for.finaltrasfer.2', 'iem' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.ftp.download.URL.2', '' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.ftp.download.useURL.2', 'false' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.ftp.use.for.finaltrasfer.2', 'true' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.national.skip.elaboration.2', 'false' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.suffix.available.hour.first.2', '20' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.suffix.available.hour.second.2', '08' FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

--Key mancanti per fornitore 1
MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MFM.supplier.forecast.acquired.curve.name.1' key, '' value FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.destination.path.for.finaltrasfer.1' , 'ilmeteo' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.ftp.use.for.finaltrasfer.1', 'true' FROM dual
            UNION ALL
           SELECT 'MFM.supplier.national.skip.elaboration.1', 'false' FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MDS.prediction.timezone' key ,'Europe/Rome' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

