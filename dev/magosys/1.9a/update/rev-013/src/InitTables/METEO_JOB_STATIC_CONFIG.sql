PROMPT METEO_JOB_STATIC_CONFIG

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MFM.supplier.ftp.download.useFtp.1' key , 'true' value 
              FROM dual
             UNION ALL 
            SELECT 'MFM.supplier.ftp.download.useURL.1' key, 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.URL.1', ''
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.available.hour.first.1', '20'
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.available.hour.second.1', '08'
              FROM dual
             UNION ALL
            SELECT 'MDS.recovery.enabled' , 'true'
              FROM dual
             UNION ALL
            SELECT 'MPS.estimation.enable.producer', 'true'
              FROM dual
             UNION ALL
             SELECT 'MPS.estimation.enable.generator' , 'false'
              FROM dual
             UNION ALL
            SELECT 'MPS.types.eolic.generator', 'CAT|CMT|CBT|TRF|TRM'
              FROM dual
             UNION ALL
            SELECT 'MPS.estimation.involvedMeasureType', 'PAS'
              FROM dual
             UNION ALL
            SELECT 'MDS.prediction.start.hour.secondsuffix', '20'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.producer', 'true'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.transformator', 'true'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.generator' , 'false'
              FROM dual
          ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

