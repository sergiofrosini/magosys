PROMPT SCHEDULER JOB PERIODIC_MAINTENANCE;
--
-- PERIODIC_MAINTENANCE  (Scheduler Job) 
--

BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('PULIZIA_GIORNALIERA', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
END;
/

BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('PERIODIC_MAINTENANCE', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'PERIODIC_MAINTENANCE',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Manutenzione.PeriodicMaintenance',
                 start_date           => TRUNC (SYSDATE) + (1/24) * 4,
                 repeat_interval      => 'FREQ=HOURLY; INTERVAL=6',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Esegue manutenzioni periodiche'
                );
END;
/
