PROMPT PACKAGE PKG_MANUTENZIONE;
--
-- PKG_MANUTENZIONE  (Package) 
--
--  Dependencies: 
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_SCHEDULER ()
--   DBMS_SCHEDULER (Synonym)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   LOG_HISTORY (Table)
--   LOG_HISTORY_INFO (Table)
--   METEO_FILE_XML (Table)
--   METEO_FILE_ZIP (Table)
--   METEO_JOB_STATIC_CONFIG (Table)
--   PKG_KPI (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   STANDARD (Package)
--   USER_SCHEDULER_JOBS ()
--   USER_SCHEDULER_JOBS (Synonym)
--
CREATE OR REPLACE PACKAGE PKG_MANUTENZIONE AS

/* ***********************************************************************************************************
   NAME:       PKG_Manutenzione
   PURPOSE:    Servizi per la manutenzione dell'applicazione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.9.a.8    19/12/2014  Moretti C.       Pulizia Richieste e Indici KPI
                                           Lancio PKG_KPI.PuliziaRichieste
   1.9.a.13   10/03/2015  Moretti C.       Chiude e rilancia i Jobs Oracle in esecuzione da oltre 12 ore

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PeriodicMaintenance;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Manutenzione;
/
SHOW ERRORS;


