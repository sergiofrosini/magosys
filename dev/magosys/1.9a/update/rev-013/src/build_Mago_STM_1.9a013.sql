PROMPT =======================================================================================
PROMPT rel_19a13_STM
PROMPT =======================================================================================

PROMPT
PROMPT

WHENEVER SQLERROR CONTINUE

conn corele/corele

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI CORELE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_MAGO_ANAGRAFICA_BASE.sql
@./Views/V_MAGO_REL_IMP_AT_MT.sql

PROMPT _______________________________________________________________________________________
PROMPT Grants
PROMPT
@./Grants/V_MAGO_ANAGRAFICA_BASE_TO_MAGO.sql
@./Grants/V_MAGO_REL_IMP_AT_MT_TO_MAGO.sql

conn mago/mago

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_STM <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

PROMPT _______________________________________________________________________________________
PROMPT Scheduler Jobs
PROMPT
@./SchedulerJobs/PERIODIC_MAINTENANCE.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_MAGO_ANAGRAFICA_BASE.sql
@./Views/V_MAGO_REL_IMP_AT_MT.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_MANUTENZIONE.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_MANUTENZIONE.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables

@./InitTables/METEO_JOB_STATIC_CONFIG.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.13
PROMPT
