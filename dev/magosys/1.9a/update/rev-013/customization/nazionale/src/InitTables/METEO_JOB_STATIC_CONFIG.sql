PROMPT METEO_JOB_STATIC_CONFIG

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MFM.supplier.ftp.download.useFtp.1' key , 'false' value 
              FROM dual
             UNION ALL 
            SELECT 'MFM.supplier.ftp.download.useURL.1' key, 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.URL.1', 'http://www.ilmeteo.it/clienti/siemens/xml.zip'
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.available.hour.first.1', '20'
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.available.hour.second.1', '08'
              FROM dual
             UNION ALL
            SELECT 'MDS.recovery.enabled' , 'true'
              FROM dual
             UNION ALL
            SELECT 'MPS.estimation.enable.producer', 'false'
              FROM dual
             UNION ALL
             SELECT 'MPS.estimation.enable.generator' , 'true'
              FROM dual
             UNION ALL
            SELECT 'MPS.types.eolic.generator', 'CAT|CMT|CBT|GAT|GMT|GBT'
              FROM dual
             UNION ALL
            SELECT 'MPS.estimation.involvedMeasureType', 'PAG'
              FROM dual
             UNION ALL
            SELECT 'MDS.prediction.start.hour.secondsuffix', '08'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.producer', 'false'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.transformator', 'false'
              FROM dual
             UNION ALL
            SELECT 'MPS.prediction.enable.generator' , 'true'
              FROM dual
          ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

