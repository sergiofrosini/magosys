SPOOL rel_19a9_NAZ.log

PROMPT =======================================================================================
PROMPT rel_19a9_NAZ
PROMPT =======================================================================================

WHENEVER SQLERROR CONTINUE

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_NAZ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/EliminazioneVisteObsolete.sql
@./Views/V_ELEMENTI.sql
--@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_AGGREGAZIONI.sql
--@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_AGGREGAZIONI.sql
--@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_KPI.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.9
PROMPT

SPOOL OFF

