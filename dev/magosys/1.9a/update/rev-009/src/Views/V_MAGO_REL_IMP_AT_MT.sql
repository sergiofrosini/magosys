PROMPT VIEW V_MAGO_REL_IMP_AT_MT;
--
-- V_MAGO_REL_IMP_AT_MT  (View) 
--
--  Dependencies: 
--   ESERCIZI (Table)
--   AVVOLGIMENTI_SA (Table)
--   IMPIANTIAT_SA (Table)
--   MONTANTIMT_SA (Table)
--   SEZIONATORI_SA (Table)
--   TRASFORMATORIAT_SA (Table)
--   SBARRE_SA (Table)
--   IMPIANTIMT_SA (Table)
--
CREATE OR REPLACE VIEW V_MAGO_REL_IMP_AT_MT
AS 
WITH 
     /*  
     VISTA per generazione relationi per Schema MAGO.
     ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                 NON eliminare prefissi di schema !!!! 
     */ 
     MyEsercizi AS (SELECT CASE WHEN ESE <> SUBSTR(COD_GEST,3,2) 
                                    THEN COD_GEST||'_'||ESE
                                    ELSE COD_GEST
                           END       cg_ese,
                           COD_ENTE  tp_ese,
                           CODICE_ST ese_st
                      FROM CORELE.ESERCIZI
                   )
    ,MyCabPrim  AS (SELECT COD_GEST  cg_cpr,
                           COD_ENTE  tp_cpr,
                           CODICE_ST cpr_st,
                           COD_ESERCIZIO ese_st
                      FROM CORELE.IMPIANTIAT_SA
                     WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                   )
    ,MyTrasfAT  AS (SELECT cg_tra,
                           tp_tra,
                           tra_st,
                           cpr_st
                      FROM (SELECT CODICE_ST sat_st,CODICEST_IMPAT cpr_st
                              FROM CORELE.SBARRE_SA S
                             WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND TIPO = 'AT'
                             UNION ALL /* SELEZIONA EVENTUALI SBARRE MT CHE ALIMENTANO IL PRIMARIO DI UN TRASF.MT/MT */
                            SELECT CODICE_ST sat_st,CODICEST_IMPAT cpr_st
                              FROM CORELE.SBARRE_SA S
                             WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                               AND CODICE_ST IN (SELECT /*+ PUSH_SUBQ */ CODICEST_SBARRA
                                                   FROM CORELE.AVVOLGIMENTI_SA AVVOLGIMENTI_SA1
                                                  WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                                    AND COD_ENTE = 'PRIM'
                                                  GROUP BY CODICEST_SBARRA)
                           )
                     INNER JOIN
                          (SELECT CODICE_ST avv_prim_st,CODICEST_SBARRA sat_st
                             FROM CORELE.AVVOLGIMENTI_SA
                            WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND COD_ENTE = 'PRIM' /*AND STATO = 'CH'*/
                          ) TRP USING (sat_st)
                    INNER JOIN
                          (SELECT COD_GEST cg_tra,CODICE_ST tra_st,CODICEST_AVV_PRIM avv_prim_st, COD_ENTE tp_tra
                         FROM CORELE.TRASFORMATORIAT_SA
                            WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                          ) TRF USING (avv_prim_st)
                   )
    ,MySecTer   AS (SELECT AV.COD_GEST cg_sec,
                           AV.COD_ENTE tp_sec,
                           TR.CODICEST_AVV_SECN avv_sec_st,
                           TR.CODICE_ST tra_st
                      FROM CORELE.TRASFORMATORIAT_SA TR
                     INNER JOIN CORELE.AVVOLGIMENTI_SA AV ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                                         AND AV.CODICE_ST = TR.CODICEST_AVV_SECN AND AV.STATO = 'CH'
                     WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                    UNION
                    SELECT AV.COD_GEST cg_ter,
                           AV.COD_ENTE tp_ter,
                           TR.CODICEST_AVV_TERZ avv_ter_st,
                           TR.CODICE_ST tra_st
                      FROM CORELE.TRASFORMATORIAT_SA TR
                     INNER JOIN CORELE.AVVOLGIMENTI_SA AV ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                                         AND AV.CODICE_ST = TR.CODICEST_AVV_TERZ AND AV.STATO = 'CH'
                     WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                   )
    ,MySbarreMT AS (SELECT S.COD_GEST cg_smt,
                           S.COD_ENTE tp_smt,
                           S.CODICE_ST smt_st,
                           S.CODICEST_AVV avv_st,
                           S.CODICEST_MONT_ALIM mont_alim_st
                      FROM CORELE.SBARRE_SA S
                      LEFT OUTER JOIN (SELECT CODICEST_SBARRA1 CODICE_ST
                                         FROM CORELE.SEZIONATORI_SA Z
                                        INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA1 = S.CODICE_ST
                                        WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                          AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                       UNION
                                       SELECT CODICEST_SBARRA2 CODICE_ST
                                         FROM CORELE.SEZIONATORI_SA Z
                                        INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA2 = S.CODICE_ST
                                        WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                          AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                      ) Z ON (Z.CODICE_ST = S.CODICE_ST)
                     WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT' /* AND S.CODICEST_AVV <> 'ND' */
                   )
    ,MyLineeMT  AS (SELECT COD_GEST cg_lmt,
                           COD_ENTE tp_lmt,
                           CODICE_ST lmt_st,
                           CODICEST_SBARRA smt_st
                      FROM CORELE.MONTANTIMT_SA
                     WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND STATO = 'CH'
                   )
    ,MySbCabSec AS (SELECT COD_GEST_SBARRA cg_scs,
                           'SBCS' tp_scs,
                           CODICEST_MONTANTE lmt_st
                      FROM CORELE.IMPIANTIMT_SA
                     WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                   )
SELECT /* seleziono Esercizi */
       NULL   gest_padre,
       NULL   tipo_padre,
       cg_ese gest_figlio,
       tp_ese tipo_figlio
  FROM MyEsercizi
UNION ALL
SELECT /* associo CABINE PRIMARIE ad ESERCIZIO */
       cg_ese gest_padre,
       tp_ese tipo_padre,
       cg_cpr gest_figlio,
       tp_cpr tipo_figlio
  FROM MyCabPrim A
 INNER JOIN MyEsercizi B ON B.ese_st = A.ese_st
UNION ALL
SELECT /* associo TRASFORMATORI a CABINA PRIMARIA */
       cg_cpr gest_padre,
       tp_cpr tipo_padre,
       cg_tra gest_figlio,
       tp_tra tipo_figlio
  FROM MyTrasfAT A
 INNER JOIN MyCabPrim B ON B.cpr_st = A.cpr_st 
UNION ALL
SELECT /* associo SECONDARI/TERZIARI a TRASFORMATORE */
       cg_tra gest_padre,
       tp_tra tipo_padre,
       cg_sec gest_figlio,
       tp_sec tipo_figlio
  FROM MySecTer A
 INNER JOIN MyTrasfAT B ON B.tra_st = A.tra_st
UNION ALL
SELECT /* associo SBARRE MT a SECONDARIO/TERZIARIO */
       cg_sec gest_padre,
       tp_sec tipo_padre,
       cg_smt gest_figlio,
       tp_smt tipo_figlio
  FROM MySbarreMT A
 INNER JOIN MySecTer B ON B.avv_sec_st = A.avv_st
 WHERE A.mont_alim_st IS NULL /* esclude le SBARRE MT associate a MONTANTI di altre CP */
UNION ALL
SELECT /* associo LINEE MT a SBARRA MT */
       cg_smt gest_padre,
       tp_smt tipo_padre,
       cg_lmt gest_figlio,
       tp_lmt tipo_figlio
  FROM MyLineeMT A
 INNER JOIN MySbarreMT B ON B.smt_st = A.smt_st
UNION ALL
SELECT /* associo SBARRE MT a LINEE MT - SbarreMT alimentate da Linee di altra CP (es: Centro Satellite) */
       cg_lmt gest_padre,
       tp_lmt tipo_padre,
       cg_smt gest_figlio,
       tp_smt tipo_figlio
  FROM MySbarreMT A
 INNER JOIN MyLineeMT B ON B.lmt_st = A.mont_alim_st
 WHERE A.mont_alim_st IS NOT NULL /* seleziona solo le SBARRE MT associate a MONTANTI di altre CP */
UNION ALL
SELECT /* associo SBARRE DI CABINA SECONRARIA a LINEA MT */
       cg_lmt gest_padre,
       tp_lmt tipo_padre,
       cg_scs gest_figlio,
       tp_scs tipo_figlio
  FROM MySbCabSec A
 INNER JOIN MyLineeMT B ON B.lmt_st = A.lmt_st;


PROMPT Privs ON VIEW V_MAGO_REL_IMP_AT_MT TO MAGO TO MAGO;
GRANT SELECT ON V_MAGO_REL_IMP_AT_MT TO MAGO;

