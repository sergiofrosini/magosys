
PROMPT Eliminazione viste obsolete ....

BEGIN
    BEGIN
        EXECUTE IMMEDIATE 'DROP VIEW V_GERARCHIA_IMPIANTO_AT_MT_ALL';
        DBMS_OUTPUT.PUT_LINE('VISTA V_GERARCHIA_IMPIANTO_AT_MT_ALL   droppata');
    EXCEPTION
        WHEN OTHERS THEN IF SQLCODE = -942 THEN 
                             DBMS_OUTPUT.PUT_LINE('VISTA V_GERARCHIA_IMPIANTO_AT_MT_ALL   non presente ....');
                         ELSE
                             RAISE;
                         END IF; 
    END;
    BEGIN
        EXECUTE IMMEDIATE 'DROP VIEW V_GERARCHIA_IMPIANTO_MT_BT_ALL';
        DBMS_OUTPUT.PUT_LINE('VISTA V_GERARCHIA_IMPIANTO_MT_BT_ALL   droppata');
    EXCEPTION
        WHEN OTHERS THEN IF SQLCODE = -942 THEN 
                             DBMS_OUTPUT.PUT_LINE('VISTA V_GERARCHIA_IMPIANTO_MT_BT_ALL   non presente ....');
                         ELSE
                             RAISE;
                         END IF; 
    END;
END;
/