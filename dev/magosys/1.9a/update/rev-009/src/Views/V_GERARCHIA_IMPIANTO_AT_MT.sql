--
-- V_GERARCHIA_IMPIANTO_AT_MT  (View) 
--
--  Dependencies: 
--   ESERCIZI (Table)
--   ELEMENTI (Table)
--   PKG_ELEMENTI (Package)
--   V_MAGO_REL_IMP_AT_MT (View)
--
CREATE OR REPLACE VIEW V_GERARCHIA_IMPIANTO_AT_MT
AS 
SELECT /*  
         ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                     NON eliminare prefissi di schema !!!! 
       */ 
       NVL(P.COD_TIPO_ELEMENTO,'COP')||'-'||F.COD_TIPO_ELEMENTO tipo,
       NVL(P.COD_ELEMENTO,PKG_ELEMENTI.GetElementoBase)         padre,
       F.COD_ELEMENTO                                           figlio
  FROM (SELECT NULL GEST_PADRE, 
               NULL TIPO_PADRE, 
               CASE WHEN ESE <> SUBSTR(COD_GEST,3,2) 
                       THEN COD_GEST||'_'||ESE
                       ELSE COD_GEST
               END GEST_FIGLIO,
               'ESER' TIPO_FIGLIO
          FROM CORELE.ESERCIZI E
         WHERE SYSDATE BETWEEN E.DATA_INIZIO AND E.DATA_FINE
        UNION ALL
        SELECT V.GEST_PADRE,V.TIPO_PADRE,V.GEST_FIGLIO,V.TIPO_FIGLIO
          FROM CORELE.V_MAGO_REL_IMP_AT_MT V
         WHERE V.TIPO_FIGLIO <> 'ESER'
       ) A
  LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.GEST_PADRE
 INNER JOIN       ELEMENTI F ON F.COD_GEST_ELEMENTO = A.GEST_FIGLIO
 WHERE CASE WHEN P.COD_ELEMENTO IS NOT NULL 
                THEN 1
                ELSE CASE WHEN F.COD_TIPO_ELEMENTO = 'ESE' 
                              THEN 1
                              ELSE 0
                     END
       END = 1
 ORDER BY PKG_ELEMENTI.GetOrderByElemType(F.COD_TIPO_ELEMENTO,F.COD_GEST_ELEMENTO),
          CASE SUBSTR(F.COD_GEST_ELEMENTO,1,1)
               WHEN 'D' THEN 1
               WHEN 'A' THEN 2
                        ELSE 3
          END, 
          F.COD_GEST_ELEMENTO;
