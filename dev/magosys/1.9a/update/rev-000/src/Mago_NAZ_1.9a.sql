SPOOL rel_19a_NAZ.LOG

--conn mago/mago@arcdb2

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT =======================================================================================
PROMPT DETERMINA TIPO INSTALLAZIONE ORACLE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT
WHENEVER SQLERROR EXIT SQLCODE

var ORA CHAR(3);

DECLARE
  V_BNR VARCHAR2(128) := 'XX';
BEGIN
  BEGIN
     SELECT BANNER INTO V_BNR
       FROM V$VERSION
     WHERE INSTR(UPPER(BANNER),'ORACLE DATABASE')>0;
    EXCEPTION WHEN OTHERS THEN
    :ORA := 'xxx';
    RAISE_APPLICATION_ERROR (-20200, 'VERSION NOT FOUND '||CHR(10)||' STOP INSTALLAZIONE!');
  END;
  DBMS_OUTPUT.PUT_LINE('INSTALLAZIONE SU:'||CHR(10)||'    '||V_BNR);
  IF INSTR (UPPER(V_BNR), 'ENTERPRISE') > 0 THEN
    :ORA := 'ENT';
  ELSE
    :ORA := 'STD';
  END IF;
END;
/
COL DUMMY NEW_VALUE ORA
SELECT :ORA DUMMY FROM DUAL;


PROMPT =======================================================================================
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT

@./Sequences/KPI_PKSEQ.sql

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT

@./Types/DROP_TYPES.sql
@./Types/T_COD_GEST_OBJ.sql
@./Types/T_COD_GEST_ARRAY.sql
@./Types/T_KPI_MIS_OBJ.sql
@./Types/T_KPI_MIS_ARRAY.sql
@./Types/T_KPI_RICHIESTA_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

@./Tables/GTTD_VALORI_TEMP.sql
@./Tables/GTTD_MISURE.sql
@./Tables/TIPI_MISURA.sql
@./Tables/TIPO_FONTI.sql
@./Tables/ELEMENTI_DEF.sql
@./Tables/TRATTAMENTO_ELEMENTI.sql
@./Tables/KPI_RICHIESTA.sql
@./Tables/KPI_PARAMETRI.sql
@./Tables/KPI_ELEMENTI.sql
@./Tables/KPI_INDICI_&ORA.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT

@./Packages/PKG_MISURE.sql
@./Packages/PKG_MAGO.sql
@./Packages/PKG_ELEMENTI.sql
--@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ANAGRAFICA_IMPIANTO.sql
--@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./Views/V_TIPI_MISURA.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBody 
PROMPT

@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_ELEMENTI.sql
--@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPO_FONTI.sql
@./InitTables/VERSION.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a
PROMPT

SPOOL OFF

