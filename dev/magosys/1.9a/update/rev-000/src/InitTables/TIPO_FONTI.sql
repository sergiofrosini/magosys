PROMPT TIPO_FONTI

UPDATE tipo_fonti
   SET flg_apply_filter = 1
  WHERE COD_RAGGR_FONTE IN ('I','T', 'R', 'C');

UPDATE tipo_fonti
   SET cod_raggr_fonte_alfa_rif = UPPER(COD_RAGGR_FONTE)
  WHERE COD_RAGGR_FONTE IN ('i','t', 'r', 'c'); 
       
COMMIT;

