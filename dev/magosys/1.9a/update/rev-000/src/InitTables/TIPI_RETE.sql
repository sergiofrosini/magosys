PROMPT TIPO_FONTI

MERGE INTO TIPI_RETE A 
 USING (
        SELECT 'X' AS COD_TIPO_RETE, 'Non Disponibile' AS DESCRIZIONE,NULL AS ID_RETE FROM DUAL
       ) B ON (A.COD_TIPO_RETE = B.COD_TIPO_RETE)
  WHEN NOT MATCHED THEN INSERT (COD_TIPO_RETE, DESCRIZIONE, ID_RETE)
                        VALUES (B.COD_TIPO_RETE, B.DESCRIZIONE, B.ID_RETE)
  WHEN MATCHED THEN UPDATE SET A.DESCRIZIONE = B.DESCRIZIONE,
                               A.ID_RETE = B.ID_RETE;
COMMIT;
