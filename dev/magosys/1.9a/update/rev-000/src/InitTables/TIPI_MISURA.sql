PROMPT TIPI_MISURA APPLY FILTER

UPDATE TIPI_MISURA 
   SET flg_apply_filter = 1
 WHERE cod_tipo_misura LIKE 'PMP%' OR cod_tipo_misura LIKE 'PMC%';


PROMPT MERGING INTO TIPI_MISURA

MERGE INTO TIPI_MISURA A
     USING (SELECT 'MAE' AS COD_TIPO_MISURA, 'Media dell''errore assoluto' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL  
            UNION ALL
            SELECT 'NMAE1' AS COD_TIPO_MISURA, 'Media dell''errore assoluto rispetto a Pnom' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'NMAE2' AS COD_TIPO_MISURA, 'Media dell''errore assoluto rispetto alla Misura' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'NMAE3' AS COD_TIPO_MISURA, 'Media dell''errore assoluto rispetto alla Previsione' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'RMSE' AS COD_TIPO_MISURA, 'Root Mean Squared Error' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'NRMSE1' AS COD_TIPO_MISURA, 'Normalized Root Mean Squared Error Rispetto a Pnom' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL 
            UNION ALL
            SELECT 'NRMSE2' AS COD_TIPO_MISURA, 'Normalized Root Mean Squared Error Rispetto ai dati di targa' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'SD' AS COD_TIPO_MISURA, 'Standard Deviation sulla sequenza dell''errore' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'KPIMIS' AS COD_TIPO_MISURA, 'Misura "reale" utilizzata nel calcolo dei kpi' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'KPIFOR' AS COD_TIPO_MISURA, 'Misura di previsione utilizzata nel calcolo dei kpi' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
            UNION ALL
            SELECT 'KPIPI' AS COD_TIPO_MISURA, 'Componente Potenza Installata utilizzata nel calcolo dei kpi' AS DESCRIZIONE, 1 AS FLG_KPI FROM DUAL
           ) B
       ON (A.COD_TIPO_MISURA = B.COD_TIPO_MISURA)
     WHEN NOT MATCHED THEN INSERT (COD_TIPO_MISURA, DESCRIZIONE, FLG_KPI, FLG_APPLY_FILTER)
                           VALUES (B.COD_TIPO_MISURA, B.DESCRIZIONE, B.FLG_KPI, 0)
     WHEN MATCHED THEN UPDATE SET A.DESCRIZIONE = B.DESCRIZIONE,
                                  A.FLG_KPI = B.FLG_KPI,
                                  A.MIS_GENERAZIONE = NULL,
                                  A.RISOLUZIONE_FE = NULL,
                                  A.COD_UM_STANDARD = NULL,
                                  A.RILEVAZIONI_NAGATIVE = NULL,
                                  A.SPLIT_FONTE_ENERGIA = NULL,
                                  A.TIPO_INTERPOLAZIONE_FREQ_CAMP = NULL,
                                  A.TIPO_INTERPOLAZIONE_BUCHI_CAMP = NULL,
                                  A.FLG_MANUTENZIONE = NULL,
                                  A.PRIORITA_AGGR = NULL,
                                  A.FLG_MIS_CARICO = NULL,
                                  A.FLG_MIS_GENERAZIONE = NULL,
                                  A.FLG_APPLY_FILTER = 0;

COMMIT;

