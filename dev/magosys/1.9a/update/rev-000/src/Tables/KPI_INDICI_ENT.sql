PROMPT TABLE KPI_INDICI;
--
-- KPI_INDICI  (Table) 
--
--  Dependencies: 
--   TRATTAMENTO_ELEMENTI (Table)
--
CREATE TABLE KPI_INDICI
(
  COD_TRATTAMENTO_ELEM  NUMBER,
  RISOLUZIONE           INTEGER,
  DATA                  DATE,
  VALORE                NUMBER, 
  CONSTRAINT KPI_INDICI_PK
 PRIMARY KEY
 (COD_TRATTAMENTO_ELEM, RISOLUZIONE, DATA)
)
ORGANIZATION INDEX
  PARTITION BY RANGE (DATA)
(  
  PARTITION GEN2014 VALUES LESS THAN (TO_DATE(' 2014-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2014 VALUES LESS THAN (TO_DATE(' 2014-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2014 VALUES LESS THAN (TO_DATE(' 2014-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2014 VALUES LESS THAN (TO_DATE(' 2014-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2014 VALUES LESS THAN (TO_DATE(' 2014-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2014 VALUES LESS THAN (TO_DATE(' 2014-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2014 VALUES LESS THAN (TO_DATE(' 2014-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2014 VALUES LESS THAN (TO_DATE(' 2014-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2014 VALUES LESS THAN (TO_DATE(' 2014-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2014 VALUES LESS THAN (TO_DATE(' 2014-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2014 VALUES LESS THAN (TO_DATE(' 2014-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2014 VALUES LESS THAN (TO_DATE(' 2014-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2015 VALUES LESS THAN (TO_DATE(' 2015-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2015 VALUES LESS THAN (TO_DATE(' 2015-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2015 VALUES LESS THAN (TO_DATE(' 2015-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2015 VALUES LESS THAN (TO_DATE(' 2015-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2015 VALUES LESS THAN (TO_DATE(' 2015-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2015 VALUES LESS THAN (TO_DATE(' 2015-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2015 VALUES LESS THAN (TO_DATE(' 2015-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2015 VALUES LESS THAN (TO_DATE(' 2015-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2015 VALUES LESS THAN (TO_DATE(' 2015-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2015 VALUES LESS THAN (TO_DATE(' 2015-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2015 VALUES LESS THAN (TO_DATE(' 2015-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2015 VALUES LESS THAN (TO_DATE(' 2015-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2016 VALUES LESS THAN (TO_DATE(' 2016-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2016 VALUES LESS THAN (TO_DATE(' 2016-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2016 VALUES LESS THAN (TO_DATE(' 2016-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2016 VALUES LESS THAN (TO_DATE(' 2016-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2016 VALUES LESS THAN (TO_DATE(' 2016-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2016 VALUES LESS THAN (TO_DATE(' 2016-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2016 VALUES LESS THAN (TO_DATE(' 2016-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2016 VALUES LESS THAN (TO_DATE(' 2016-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2016 VALUES LESS THAN (TO_DATE(' 2016-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2016 VALUES LESS THAN (TO_DATE(' 2016-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2016 VALUES LESS THAN (TO_DATE(' 2016-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2016 VALUES LESS THAN (TO_DATE(' 2016-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2017 VALUES LESS THAN (TO_DATE(' 2017-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2017 VALUES LESS THAN (TO_DATE(' 2017-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2017 VALUES LESS THAN (TO_DATE(' 2017-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2017 VALUES LESS THAN (TO_DATE(' 2017-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2017 VALUES LESS THAN (TO_DATE(' 2017-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2017 VALUES LESS THAN (TO_DATE(' 2017-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2017 VALUES LESS THAN (TO_DATE(' 2017-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2017 VALUES LESS THAN (TO_DATE(' 2017-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2017 VALUES LESS THAN (TO_DATE(' 2017-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2017 VALUES LESS THAN (TO_DATE(' 2017-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2017 VALUES LESS THAN (TO_DATE(' 2017-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2017 VALUES LESS THAN (TO_DATE(' 2017-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2018 VALUES LESS THAN (TO_DATE(' 2018-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2018 VALUES LESS THAN (TO_DATE(' 2018-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2018 VALUES LESS THAN (TO_DATE(' 2018-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2018 VALUES LESS THAN (TO_DATE(' 2018-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2018 VALUES LESS THAN (TO_DATE(' 2018-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2018 VALUES LESS THAN (TO_DATE(' 2018-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2018 VALUES LESS THAN (TO_DATE(' 2018-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2018 VALUES LESS THAN (TO_DATE(' 2018-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2018 VALUES LESS THAN (TO_DATE(' 2018-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2018 VALUES LESS THAN (TO_DATE(' 2018-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2018 VALUES LESS THAN (TO_DATE(' 2018-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2018 VALUES LESS THAN (TO_DATE(' 2018-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2019 VALUES LESS THAN (TO_DATE(' 2019-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2019 VALUES LESS THAN (TO_DATE(' 2019-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2019 VALUES LESS THAN (TO_DATE(' 2019-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2019 VALUES LESS THAN (TO_DATE(' 2019-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2019 VALUES LESS THAN (TO_DATE(' 2019-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2019 VALUES LESS THAN (TO_DATE(' 2019-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2019 VALUES LESS THAN (TO_DATE(' 2019-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2019 VALUES LESS THAN (TO_DATE(' 2019-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2019 VALUES LESS THAN (TO_DATE(' 2019-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2019 VALUES LESS THAN (TO_DATE(' 2019-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2019 VALUES LESS THAN (TO_DATE(' 2019-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2019 VALUES LESS THAN (TO_DATE(' 2019-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GEN2020 VALUES LESS THAN (TO_DATE(' 2020-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION FEB2020 VALUES LESS THAN (TO_DATE(' 2020-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAR2020 VALUES LESS THAN (TO_DATE(' 2020-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION APR2020 VALUES LESS THAN (TO_DATE(' 2020-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MAG2020 VALUES LESS THAN (TO_DATE(' 2020-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION GIU2020 VALUES LESS THAN (TO_DATE(' 2020-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION LUG2020 VALUES LESS THAN (TO_DATE(' 2020-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION AGO2020 VALUES LESS THAN (TO_DATE(' 2020-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION SET2020 VALUES LESS THAN (TO_DATE(' 2020-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION OTT2020 VALUES LESS THAN (TO_DATE(' 2020-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION NOV2020 VALUES LESS THAN (TO_DATE(' 2020-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION DIC2020 VALUES LESS THAN (TO_DATE(' 2020-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT,  
  PARTITION MIS_NEXT_ALL VALUES LESS THAN (MAXVALUE)
    LOGGING
    NOCOMPRESS
    TABLESPACE &TBS&IOT
)
NOPARALLEL
MONITORING;




-- 
-- Foreign Key Constraints for Table KPI_INDICI 
-- 
PROMPT FOREIGN KEY CONSTRAINTS ON TABLE KPI_INDICI;
ALTER TABLE KPI_INDICI ADD (
  CONSTRAINT TRTELE_KPIVAL 
  FOREIGN KEY (COD_TRATTAMENTO_ELEM) 
  REFERENCES TRATTAMENTO_ELEMENTI (COD_TRATTAMENTO_ELEM)
  ENABLE VALIDATE)
/

