PROMPT TABLE TIPI_MISURA;
--
-- TIPI_MISURA  (Table) 
--
ALTER TABLE tipi_misura 
        ADD ( flg_apply_filter NUMBER(1) DEFAULT 0
              ,flg_kpi  NUMBER DEFAULT 0 );



COMMENT ON COLUMN TIPI_MISURA.flg_apply_filter IS '1=Alla misura si applicano i filtri per fonti diverse da Solare/Eolico'
/

COMMENT ON COLUMN TIPI_MISURA.FLG_KPI IS '1=La misura e'' un indice KPI'
/