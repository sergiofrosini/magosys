PROMPT TABLE KPI_RICHIESTA;
--
-- KPI_RICHIESTA  (Table) 
--
CREATE TABLE KPI_RICHIESTA
(
  ID_REQ_KPI                  NUMBER,
  DATA_RICHIESTA              DATE,
  OPERATORE                   VARCHAR2(20 BYTE),
  PERIODO_INIZIO              DATE,
  PERIODO_FINE                DATE,
  ORARIO_INIZIO               CHAR(5 BYTE),
  ORARIO_FINE                 CHAR(5 BYTE),
  RISOLUZIONE                 INTEGER,
  ORGANIZZAZIONE              INTEGER,
  STATO_RETE                  INTEGER,
  LISTA_FONTI                 VARCHAR2(20),
  LISTA_TIPI_RETE             VARCHAR2(20),
  LISTA_TIPI_CLIE             VARCHAR2(20),
  LISTA_INDICI_RICH           VARCHAR2(100),
  LISTA_TIPI_MISURA           VARCHAR2(100),
  LISTA_TIPI_MISURA_FORECAST  VARCHAR2(100),
  FREQ_ERR_VAL_INI            INTEGER,
  FREQ_ERR_VAL_FIN            INTEGER,
  FREQ_ERR_PASSO              INTEGER,
  VALORE_SOGLIA               INTEGER,
  DELTA                       INTEGER,
  SOGLIA_COLORE               INTEGER,
  ELAB_INIZIO                 DATE,
  ELAB_FINE                   DATE,
  ELAB_STATO                  INTEGER,
  ELAB_NOTE                   VARCHAR2(4000 BYTE)
)
TABLESPACE &TBS&DAT
NOCOMPRESS 
NOPARALLEL
/

COMMENT ON TABLE KPI_RICHIESTA IS 'Testata richiesta KPI - indici di errore'
/

COMMENT ON COLUMN KPI_RICHIESTA.ID_REQ_KPI IS 'Codice identificativo della richiesta'
/

COMMENT ON COLUMN KPI_RICHIESTA.DATA_RICHIESTA IS 'Data/Ora della richiesta'
/

COMMENT ON COLUMN KPI_RICHIESTA.OPERATORE IS 'Operatore richiedente'
/

COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_INIZIO IS 'Inizio periodo temporale selezionato'
/

COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_FINE IS 'Fine periodo temporale selezionato'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_INIZIO IS 'Orario di inizio (alba) - Formato HH:MI'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_FINE IS 'Orario di fine (tramonto) - Formato HH:MI'
/

COMMENT ON COLUMN KPI_RICHIESTA.RISOLUZIONE IS 'Risoluzione temporale dell''analisi (minuti)'
/

COMMENT ON COLUMN KPI_RICHIESTA.ORGANIZZAZIONE IS 'Organizzazione selezionata'
/

COMMENT ON COLUMN KPI_RICHIESTA.STATO_RETE IS 'Stato della rete selezionata'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_FONTI IS 'Elenco delle fonti richieste (come ricevute in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_RETE IS 'Elenco dei tipi rete richiesti (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_CLIE IS 'Elenco dei tipi cliente richiesti (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_INDICI_RICH IS 'Elenco degli indici da calcolare (come ricevuti in fase di inserimento)'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_INI IS 'Frequenza di errore - valore iniziale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_FIN IS 'Frequenza di errore - valore finale'
/

COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_PASSO IS 'Frequenza di errore - passo'
/

COMMENT ON COLUMN KPI_RICHIESTA.VALORE_SOGLIA IS 'Valore di soglia'
/

COMMENT ON COLUMN KPI_RICHIESTA.DELTA IS 'Delta'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_INIZIO IS 'Ora Inizio Elaborazione'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_FINE IS 'Ora Fine Elaborazione'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_STATO IS 'Stato avanzamento della richiesta; 0=da elaborare, 1=elab. in corso, 2=elaborazione conclusa, -1=errore di elaborazione (dettaglio in ELAB_NOTE)'
/

COMMENT ON COLUMN KPI_RICHIESTA.ELAB_NOTE IS 'Note sull''elaborazione'
/

COMMENT ON COLUMN KPI_RICHIESTA.SOGLIA_COLORE IS 'Soglia per Colorazione FE';
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA IS 'Elencio dei tipi Misura Base'
/

COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA_FORECAST IS 'Elencio dei tipi Misura di Forecast'
/




PROMPT INDEX KPI_RICHIESTA_PK;
--
-- KPI_RICHIESTA_PK  (Index) 
--
--  Dependencies: 
--   KPI_RICHIESTA (Table)
--
CREATE UNIQUE INDEX KPI_RICHIESTA_PK ON KPI_RICHIESTA
(ID_REQ_KPI)
TABLESPACE &TBS&IDX
NOPARALLEL
/


PROMPT TRIGGER BEF_IUR_KPI_RICHIESTA;
--
-- BEF_IUR_KPI_RICHIESTA  (Trigger) 
--
--  Dependencies: 
--   KPI_RICHIESTA (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_KPI_RICHIESTA 
BEFORE INSERT OR UPDATE
ON KPI_RICHIESTA REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_KPI_RICHIESTA
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      18/08/2014   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.ID_REQ_KPI IS NULL THEN
            SELECT KPI_PKSEQ.NEXTVAL INTO :NEW.ID_REQ_KPI FROM DUAL;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.ID_REQ_KPI <> :NEW.ID_REQ_KPI THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice KPI in tabella ''KPI_RICHIESTA''');
        END IF;
    END IF;

END BEF_UR_KPI_RICHIESTA;
/


-- 
-- Non Foreign Key Constraints for Table KPI_RICHIESTA 
-- 
PROMPT Non-FOREIGN KEY CONSTRAINTS ON TABLE KPI_RICHIESTA;
ALTER TABLE KPI_RICHIESTA ADD (
  CONSTRAINT KPI_RICHIESTA_PK
  PRIMARY KEY
  (ID_REQ_KPI)
  USING INDEX KPI_RICHIESTA_PK
  ENABLE VALIDATE)
/
