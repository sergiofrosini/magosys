SPOOL rel_19a_DGF.LOG

--conn mago/mago@arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_DGF <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

@./Tables/GTTD_VALORI_TEMP.sql
@./Tables/GTTD_MISURE.sql
@./Tables/TIPI_MISURA.sql
@./Tables/TIPO_FONTI.sql
@./Tables/ELEMENTI_DEF.sql
@./Tables/TRATTAMENTO_ELEMENTI.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT

@./Packages/PKG_MISURE.sql
@./Packages/PKG_MAGO.sql
@./Packages/PKG_ELEMENTI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBody 
PROMPT

@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_ELEMENTI.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPO_FONTI.sql
@./InitTables/VERSION.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.a.0
PROMPT

SPOOL OFF

