PROMPT PACKAGE BODY PKG_KPI;
--
-- PKG_KPI  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_KPI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9.a.2
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LeggiRichieste    (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste presenti   
 -----------------------------------------------------------------------------------------------------------*/
    
    -- ATTENZIONE: Il cursore DEVE corrispondere alla struttura del type T_KPI_RICHIESTA_OBJ !!
    
    vSql VARCHAR2(1800) := 'SELECT ID_REQ_KPI,R.DATA_RICHIESTA,R.OPERATORE,'                             ||
                                  'R.PERIODO_INIZIO,R.PERIODO_FINE,R.ORARIO_INIZIO,R.ORARIO_FINE,'       ||
                                  'R.RISOLUZIONE,R.ORGANIZZAZIONE,R.STATO_RETE,'                         ||
                                  'R.LISTA_FONTI,R.LISTA_TIPI_RETE,R.LISTA_TIPI_CLIE,'                   ||
                                  'R.LISTA_INDICI_RICH,R.LISTA_TIPI_MISURA,R.LISTA_TIPI_MISURA_FORECAST,'||
                                  'R.FREQ_ERR_VAL_INI,R.FREQ_ERR_VAL_FIN,'                               ||
                                  'R.FREQ_ERR_PASSO,R.VALORE_SOGLIA,R.DELTA,R.SOGLIA_COLORE,'            ||
                                  'R.ELAB_INIZIO,R.ELAB_FINE,R.ELAB_STATO,R.ELAB_NOTE,'                  ||
                                  '(100*NVL(B.daElab,0))/(NVL(A.Elab,0)+NVL(B.daElab,0)) AVANZAMENTO '   ||
                             'FROM KPI_RICHIESTA R '                                                     ||
                             'LEFT OUTER JOIN (SELECT ID_REQ_KPI,COUNT(*) Elab '                         ||
                                                'FROM KPI_RICHIESTA R '                                  ||
                                               'INNER JOIN KPI_ELEMENTI E USING(ID_REQ_KPI) '            ||
                                               'WHERE E.STATO = 0 #FILTRO_CODICE# #FILTRO_OPERATORE# '   ||
                                               'GROUP BY ID_REQ_KPI'                                     ||
                                             ') A USING(ID_REQ_KPI) '                                    ||
                             'LEFT OUTER JOIN (SELECT ID_REQ_KPI,COUNT(*) daElab '                       ||
                                                'FROM KPI_RICHIESTA R '                                  ||
                                               'INNER JOIN KPI_ELEMENTI E USING(ID_REQ_KPI)'             ||
                                               'WHERE E.STATO <> 0 #FILTRO_CODICE# #FILTRO_OPERATORE# '  ||
                                               'GROUP BY ID_REQ_KPI'                                     ||
                                             ') B USING(ID_REQ_KPI) '                                    ||
                            'WHERE 1=1 #FILTRO_CODICE# #FILTRO_OPERATORE# '                              ||
                           'ORDER BY ID_REQ_KPI DESC';

 BEGIN

    CASE 
        WHEN pID_REQ_KPI IS NOT NULL THEN
            vSql := REPLACE(vSql,'#FILTRO_CODICE#'   ,'AND ID_REQ_KPI = :cod');   
            vSql := REPLACE(vSql,'#FILTRO_OPERATORE#',''); 
            OPEN pRefCurs FOR vSql USING pID_REQ_KPI,pID_REQ_KPI,pID_REQ_KPI;
        WHEN pOPERATORE IS NOT NULL THEN
            vSql := REPLACE(vSql,'#FILTRO_CODICE#'   ,'');   
            vSql := REPLACE(vSql,'#FILTRO_OPERATORE#','AND R.OPERATORE = :ope'); 
            OPEN pRefCurs FOR vSql USING pOPERATORE,pOPERATORE,pOPERATORE;
        ELSE
            vSql := REPLACE(vSql,'#FILTRO_CODICE#'   ,'');   
            vSql := REPLACE(vSql,'#FILTRO_OPERATORE#',''); 
            OPEN pRefCurs FOR vSql;
    END CASE;

 END LeggiRichieste;

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertRichiesta      (pNewReq            IN T_KPI_RICHIESTA_OBJ,
                                 pElementi          IN T_COD_GEST_ARRAY,
                                 pID_REQ_KPI       OUT NUMBER) AS
 /*-----------------------------------------------------------------------------------------------------------
    Inserimento di una nuova richiesta 
 -----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1)     := -1;
      
 BEGIN

    DELETE GTTD_VALORI_TEMP; 
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_FONTI,                gcTipFonti,        vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_RETE,            gcTipReti,         vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_CLIE,            gcTipClienti,      vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_INDICI_RICH,          gcIndici,          vFlgNull);    -- non gestito il flag
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_MISURA,          gcTipiMisura,      vFlgNull);    -- non gestito il flag
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_MISURA_FORECAST, gcTipiMisForecast, vFlgNull);    -- non gestito il flag
    
    INSERT INTO KPI_RICHIESTA (OPERATORE,
                               DATA_RICHIESTA,
                               PERIODO_INIZIO,
                               PERIODO_FINE,
                               ORARIO_INIZIO,
                               ORARIO_FINE,
                               RISOLUZIONE,
                               ORGANIZZAZIONE,
                               STATO_RETE,
                               LISTA_FONTI,
                               LISTA_TIPI_RETE,
                               LISTA_TIPI_CLIE,
                               LISTA_INDICI_RICH,
                               LISTA_TIPI_MISURA, 
                               LISTA_TIPI_MISURA_FORECAST,
                               FREQ_ERR_VAL_INI,
                               FREQ_ERR_VAL_FIN,
                               FREQ_ERR_PASSO,
                               VALORE_SOGLIA,
                               DELTA,
                               SOGLIA_COLORE, 
                               ELAB_STATO)
                       VALUES (pNewReq.OPERATORE,
                               SYSDATE,
                               pNewReq.PERIODO_INIZIO,
                               pNewReq.PERIODO_FINE,
                               pNewReq.ORARIO_INIZIO,
                               pNewReq.ORARIO_FINE,
                               pNewReq.RISOLUZIONE,
                               pNewReq.ORGANIZZAZIONE,
                               pNewReq.STATO_RETE,
                               pNewReq.LISTA_FONTI,
                               pNewReq.LISTA_TIPI_RETE,
                               pNewReq.LISTA_TIPI_CLIE,
                               pNewReq.LISTA_INDICI_RICH,
                               pNewReq.LISTA_TIPI_MISURA, 
                               pNewReq.LISTA_TIPI_MISURA_FORECAST,
                               pNewReq.FREQ_ERR_VAL_INI,
                               pNewReq.FREQ_ERR_VAL_FIN,
                               pNewReq.FREQ_ERR_PASSO,
                               pNewReq.VALORE_SOGLIA,
                               pNewReq.DELTA,
                               pNewReq.SOGLIA_COLORE,
                               gcStatoDaElaborare
                              )
                    RETURNING ID_REQ_KPI INTO pID_REQ_KPI;
    
    INSERT INTO KPI_PARAMETRI (ID_REQ_KPI, TIPO, PARAMETRO)
        SELECT pID_REQ_KPI, TIP, ALF1 
          FROM GTTD_VALORI_TEMP;

    IF pElementi.LAST IS NOT NULL THEN
        INSERT INTO KPI_ELEMENTI (ID_REQ_KPI, COD_ELEMENTO)
            SELECT pID_REQ_KPI,
                   COD_ELEMENTO
              FROM TABLE (CAST(pElementi AS T_COD_GEST_ARRAY)) A
             INNER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = A.COD_GEST;
    END IF;

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.InsertRichiesta'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END InsertRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetRichieste         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                                 pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste definite 
      - per l'operatore ricevuto se non nullo
      - tutte le richieste se l'operatore e' nullo   
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    LeggiRichieste(pRefCurs,pOPERATORE,NULL);
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetRichieste'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRichieste;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetRichiesta         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                                 pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste presenti   
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    LeggiRichieste(pRefCurs,NULL,pID_REQ_KPI);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetRichiesta'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetElementsByReq  (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    ritorna gli elementi coinvolti nella richiesta indicata
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    OPEN pRefCurs FOR SELECT E.COD_ELEMENTO, E.COD_GEST_ELEMENTO, D.NOME_ELEMENTO, K.STATO 
                        FROM KPI_RICHIESTA R 
                       INNER JOIN KPI_ELEMENTI K ON K.ID_REQ_KPI = R.ID_REQ_KPI
                       INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
                        LEFT OUTER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = K.COD_ELEMENTO
                                                      AND R.PERIODO_FINE BETWEEN D.DATA_ATTIVAZIONE 
                                                                             AND D.DATA_DISATTIVAZIONE 
                       WHERE R.ID_REQ_KPI = pID_REQ_KPI;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetElementsByReq'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetElementsByReq;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoRichiesta    (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                                 pELAB_STATO        IN KPI_RICHIESTA.ELAB_STATO%TYPE,
                                 pELAB_NOTE         IN KPI_RICHIESTA.ELAB_NOTE%TYPE DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di una richiesta
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN
    CASE pELAB_STATO
        WHEN gcStatoDaElaborare THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_INIZIO = NULL,
                                     ELAB_FINE   = NULL,
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoInElaborazione THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_INIZIO = SYSDATE,
                                     ELAB_FINE   = NULL,
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoElabTerminata THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_FINE   = SYSDATE,
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoErroreDiElab THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        ELSE
            RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||pELAB_STATO||') per richiesta n. '||pID_REQ_KPI);
    END CASE;  

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.SetStatoRichiesta'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END SetStatoRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoElemento     (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                                 pCOD_ELEMENTO      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pSTATO             IN KPI_ELEMENTI.STATO%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di un elemento per la richiesta 
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    IF pStato IN (gcStatoDaElaborare,gcStatoInElaborazione) THEN
        UPDATE KPI_ELEMENTI SET STATO = pSTATO
         WHERE ID_REQ_KPI = pID_REQ_KPI
           AND COD_ELEMENTO = pCOD_ELEMENTO;  
    ELSE
        RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||pSTATO||')');
    END IF;

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.SetStatoElemento'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END SetStatoElemento;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoElementoCG   (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                                 pCOD_GEST_ELEMENTO IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pSTATO             IN KPI_ELEMENTI.STATO%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di un elemento per la richiesta 
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    SetStatoElemento(pID_REQ_KPI,PKG_ELEMENTI.GetCodElemento(pCOD_GEST_ELEMENTO),pSTATO);

 END SetStatoElementoCG;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertValori      (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pTabValIndici      IN T_KPI_MIS_ARRAY) AS
 /*-----------------------------------------------------------------------------------------------------------
    Inserisce i valori ricevuti in tabella KPI_INDICI relativi alla riciesta indicata
 -----------------------------------------------------------------------------------------------------------*/
 
    vRisoluzione KPI_RICHIESTA.RISOLUZIONE%TYPE;  
 
 BEGIN

--PKG_Logs.TraceLog('Misure ricevute '||NVL(pTabValIndici.LAST,0));

    IF pTabValIndici.LAST IS NULL THEN
        RETURN;
    END IF;

--FOR i IN pTabValIndici.FIRST .. pTabValIndici.LAST LOOP
--    PKG_Logs.TraceLog(i||' - COD_ELEMENTO      '||NVL(TO_CHAR(pTabValIndici(i).COD_ELEMENTO),'<CodNull>'));
--    PKG_Logs.TraceLog(i||' - COD_GEST_ELEMENTO '||NVL(TO_CHAR(pTabValIndici(i).COD_GEST_ELEMENTO),'<GestNull>'));
--    PKG_Logs.TraceLog(i||' - COD_INDICE        '||NVL(pTabValIndici(i).COD_INDICE,'<IndNull>'));
--    PKG_Logs.TraceLog(i||' - DATA              '||NVL(TO_CHAR(pTabValIndici(i).DATA,'dd/mm/yyyy hh24:mi:ss'),'<DataNull>'));
--    PKG_Logs.TraceLog(i||' - VALORE            '||NVL(TO_CHAR(pTabValIndici(i).VALORE),'<ValNull>'));
--    PKG_Logs.TraceLog(RPAD('-',30,'-'));
--END LOOP;

    SELECT RISOLUZIONE 
      INTO vRisoluzione
      FROM KPI_RICHIESTA
     WHERE ID_REQ_KPI = pID_REQ_KPI;
    
    DELETE GTTD_MISURE;
    INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                             COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM,DATA,VALORE)
          SELECT R.COD_TRATTAMENTO_ELEM,G.COD_ELEMENTO,G.COD_TIPO_MISURA,G.COD_TIPO_FONTE,
                 NVL(T.COD_TIPO_RETE,PKG_Mago.gcTipReteNonDisp) COD_TIPO_RETE,
                 NVL(D.COD_TIPO_CLIENTE,PKG_Mago.gcClientePrdNonApplic) COD_TIPO_CLIENTE,E.COD_TIPO_ELEMENTO,G.DATA,G.VALORE
            FROM (SELECT CASE 
                            WHEN COD_ELEMENTO IS NOT NULL 
                                THEN COD_ELEMENTO
                                ELSE PKG_Elementi.GetCodElemento(COD_GEST_ELEMENTO) 
                         END COD_ELEMENTO,
                         COD_INDICE COD_TIPO_MISURA,
                         NVL(COD_TIPO_FONTE,PKG_Mago.gcRaggrFonteNonAppl) COD_TIPO_FONTE,
                         DATA,
                         VALORE
                    FROM TABLE (CAST(pTabValIndici AS T_KPI_MIS_ARRAY))
                 ) G
           INNER JOIN ELEMENTI                   E ON E.COD_ELEMENTO      = G.COD_ELEMENTO
           INNER JOIN ELEMENTI_DEF               D ON D.COD_ELEMENTO      = G.COD_ELEMENTO
                                                  AND G.DATA        BETWEEN D.DATA_ATTIVAZIONE 
                                                                        AND D.DATA_DISATTIVAZIONE 
           INNER JOIN TIPI_ELEMENTO              T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO 
            LEFT OUTER JOIN TRATTAMENTO_ELEMENTI R ON R.COD_ELEMENTO      = G.COD_ELEMENTO
                                                  AND R.COD_TIPO_MISURA   = G.COD_TIPO_MISURA
                                                  AND R.COD_TIPO_FONTE    = G.COD_TIPO_FONTE
                                                  AND R.COD_TIPO_RETE     = T.COD_TIPO_RETE 
                                                  AND R.COD_TIPO_CLIENTE  = D.COD_TIPO_CLIENTE
                                                  AND R.ORGANIZZAZIONE    = PKG_Mago.gcOrganizzazELE
                                                  AND R.TIPO_AGGREGAZIONE = PKG_Misure.gcModalitaKPI;

--FOR i IN (SELECT * FROM GTTD_MISURE) LOOP
--    PKG_Logs.TraceLog(1||' - '||i.COD_TRATTAMENTO_ELEM||' - '||TO_CHAR(i.DATA,'dd/mm/yyyy hh24:mi:ss')||' - '||i.COD_ELEMENTO||' - '||i.valore);
--END LOOP;
--PKG_Logs.TraceLog(RPAD('-',30,'-'));
    FOR i IN (SELECT COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                     COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM 
                FROM GTTD_MISURE 
               WHERE COD_TRATTAMENTO_ELEM IS NULL
               GROUP BY COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                        COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM 
             ) LOOP
--PKG_Logs.TraceLog(i.cod_elemento||' - '||i.COD_TIPO_ELEM||' - mis='||i.COD_TIPO_MISURA||' - fte='||i.COD_TIPO_FONTE||' - Ret='||i.COD_TIPO_RETE||' - => '||i.COD_TIPO_CLIENTE);
        UPDATE GTTD_MISURE 
           SET COD_TRATTAMENTO_ELEM = PKG_Misure.GetTrattamentoElemento(i.COD_ELEMENTO,
                                                                        i.COD_TIPO_ELEM,
                                                                        i.COD_TIPO_MISURA,
                                                                        i.COD_TIPO_FONTE,
                                                                        i.COD_TIPO_RETE,
                                                                        i.COD_TIPO_CLIENTE,
                                                                        PKG_Mago.gcOrganizzazELE,
                                                                        PKG_Misure.gcModalitaKPI)
         WHERE COD_ELEMENTO     = i.COD_ELEMENTO
           AND COD_TIPO_MISURA  = i.COD_TIPO_MISURA
           AND COD_TIPO_FONTE   = i.COD_TIPO_FONTE
           AND COD_TIPO_RETE    = i.COD_TIPO_RETE 
           AND COD_TIPO_CLIENTE = i.COD_TIPO_CLIENTE;
    END LOOP;     
--PKG_Logs.TraceLog(RPAD('-',30,'-'));

--FOR i IN (SELECT * FROM GTTD_MISURE) LOOP
--    PKG_Logs.TraceLog(2||' - '||i.COD_TRATTAMENTO_ELEM||' - '||TO_CHAR(i.DATA,'dd/mm/yyyy hh24:mi:ss')||' - '||i.COD_ELEMENTO||' - '||i.valore);
--END LOOP;
--PKG_Logs.TraceLog(RPAD('-',30,'-'));

    MERGE INTO KPI_INDICI kpi
         USING (SELECT COD_TRATTAMENTO_ELEM,
                       vRisoluzione RISOLUZIONE,  
                       DATA, 
                       VALORE
                  FROM GTTD_MISURE 
                 WHERE COD_TRATTAMENTO_ELEM IS NOT NULL
               ) mis ON (    mis.COD_TRATTAMENTO_ELEM = kpi.COD_TRATTAMENTO_ELEM
                         AND mis.DATA                 = kpi.DATA
                         AND mis.RISOLUZIONE          = kpi.RISOLUZIONE
                        )
     WHEN MATCHED     THEN UPDATE SET kpi.VALORE           = mis.VALORE
                            WHERE kpi.VALORE              <> mis.VALORE
                              AND kpi.COD_TRATTAMENTO_ELEM = mis.COD_TRATTAMENTO_ELEM
                              AND kpi.DATA                 = mis.DATA
                              AND kpi.RISOLUZIONE          = mis.RISOLUZIONE
     WHEN NOT MATCHED THEN INSERT (kpi.COD_TRATTAMENTO_ELEM, kpi.RISOLUZIONE, kpi.DATA, kpi.VALORE)
                           VALUES (mis.COD_TRATTAMENTO_ELEM, mis.RISOLUZIONE, mis.DATA, mis.VALORE);

--PKG_Logs.TraceLog('Misure elaborate '||SQL%ROWCOUNT);

    COMMIT;
    
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.InsertValori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END InsertValori;

-- ----------------------------------------------------------------------------------------------------------
 
 --PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
 --                             pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE,
 --                             pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
 --                             pDataDa            IN DATE,
 --                             pDataA             IN DATE,
 --                             pTipiIndice        IN VARCHAR2) AS
 --/*-----------------------------------------------------------------------------------------------------------
 --   Ritorna i valori Indice per l'elemento richiesto (pCodElem o pCodGest) nel periodo di interesse
 --   per i tipi indice desiderati
 -------------------------------------------------------------------------------------------------------------*/
 --   vFlgNull    NUMBER(1)     := -1;
 --   vCodElem    ELEMENTI.COD_ELEMENTO%TYPE := NVL(pCodElem,PKG_Elementi.GetCodElemento(pCodGest));
 --     
 --BEGIN
 --
 --   DELETE GTTD_VALORI_TEMP; 
 --   PKG_Mago.TrattaListaCodici(pTipiIndice, gcIndici, vFlgNull);    -- non gestito il flag
 --   
 --   OPEN pRefCurs FOR  
 --       SELECT T.COD_TIPO_MISURA TIPO_INDICE,DATA,SUM(VALORE) VALORE
 --         FROM TRATTAMENTO_ELEMENTI T 
 --        INNER JOIN (SELECT ALF1 COD_TIPO_MISURA 
 --                      FROM GTTD_VALORI_TEMP 
 --                    WHERE TIP = gcIndici
 --                   ) I ON I.COD_TIPO_MISURA = T.COD_TIPO_MISURA
 --        INNER JOIN KPI_INDICI K ON K.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM 
 --                               AND K.DATA BETWEEN pDataDa AND pDataA
 --        WHERE T.COD_ELEMENTO = vCodElem
 --        GROUP BY T.COD_TIPO_MISURA,DATA
 --        ORDER BY T.COD_TIPO_MISURA,DATA;                
 --
 --EXCEPTION
 --   WHEN OTHERS THEN
 --        ROLLBACK;
 --        PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.InsertValori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
 --        PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
 --        RAISE;
 --
 --END GetValori;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE DEFAULT NULL,
                              pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna tutti i valori relativi ad una richiesta
 -----------------------------------------------------------------------------------------------------------*/

    vCodElem ELEMENTI.COD_ELEMENTO%TYPE := NVL(pCodElem,PKG_Elementi.GetCodElemento(pCodGest));
      
 BEGIN

    OPEN pRefCurs FOR 
              SELECT K.COD_ELEMENTO,
                     E.COD_GEST_ELEMENTO,
                     TIPO_INDICE,
                     DATA,
                     VALORE
                FROM (
                      SELECT T.COD_ELEMENTO, T.COD_TIPO_MISURA TIPO_INDICE,DATA,SUM(VALORE) VALORE
                        FROM KPI_RICHIESTA R
                       INNER JOIN KPI_PARAMETRI        P ON P.ID_REQ_KPI = R.ID_REQ_KPI
                                                        AND P.TIPO = gcIndici
                       INNER JOIN (SELECT E.*
                                     FROM KPI_ELEMENTI E
                                    WHERE ID_REQ_KPI = pID_REQ_KPI
                                      AND CASE 
                                             WHEN vCodElem IS NULL
                                                 THEN 1
                                                 ELSE CASE WHEN COD_ELEMENTO = vCodElem
                                                         THEN 1
                                                         ELSE 0
                                                      END
                                          END = 1
                                  ) E ON E.ID_REQ_KPI = R.ID_REQ_KPI
                       INNER JOIN TRATTAMENTO_ELEMENTI T ON T.COD_ELEMENTO = E.COD_ELEMENTO
                                                        AND T.COD_TIPO_MISURA = P.PARAMETRO
                                                        AND T.TIPO_AGGREGAZIONE = PKG_Misure.gcModalitaKPI
                       INNER JOIN KPI_INDICI K ON K.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                              AND K.RISOLUZIONE = R.RISOLUZIONE 
                                              AND K.DATA BETWEEN R.PERIODO_INIZIO  
                                                             AND R.PERIODO_FINE
                                              AND TO_CHAR(K.DATA,'hh24:mi') BETWEEN R.ORARIO_INIZIO  
                                                                                AND R.ORARIO_FINE 
                       WHERE ID_REQ_KPI = R.ID_REQ_KPI
                       GROUP BY T.COD_ELEMENTO,T.COD_TIPO_MISURA,DATA
                     ) K
               INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
               ORDER BY K.COD_ELEMENTO,K.DATA,K.TIPO_INDICE;

    RETURN;

/*
    vDatIni     DATE := NULL;  
    vDatFin     DATE := NULL;
    vOraIni     KPI_RICHIESTA.ORARIO_INIZIO%TYPE := NULL;  
    vOraFin     KPI_RICHIESTA.ORARIO_FINE%TYPE := NULL;

    BEGIN
        SELECT PERIODO_INIZIO,PERIODO_FINE,ORARIO_INIZIO,ORARIO_FINE
          INTO vDatIni,       vDatFin,     vOraIni,      vOraFin 
          FROM KPI_RICHIESTA 
         WHERE ID_REQ_KPI = pID_REQ_KPI;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN NULL; 
    END;

    OPEN pRefCurs FOR  
            WITH MyMisure AS (SELECT K.COD_ELEMENTO,
                                     E.COD_GEST_ELEMENTO,
                                     TIPO_INDICE,
                                     TO_CHAR(DATA,'YYYYMMDD') DATA,
                                     TO_CHAR(DATA,'HH24MI') ORA,
                                     VALORE
                                FROM (SELECT T.COD_ELEMENTO, T.COD_TIPO_MISURA TIPO_INDICE,DATA,SUM(VALORE) VALORE
                                        FROM TRATTAMENTO_ELEMENTI T 
                                       INNER JOIN KPI_PARAMETRI P ON P.ID_REQ_KPI = pID_REQ_KPI
                                                                 AND P.TIPO = 'TIPKPI'
                                       INNER JOIN KPI_ELEMENTI E ON E.COD_ELEMENTO = T.COD_ELEMENTO 
                                                                AND E.ID_REQ_KPI = pID_REQ_KPI
                                       INNER JOIN KPI_INDICI K ON K.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM 
                                                              AND K.DATA BETWEEN vDatIni AND vDatFin
                                                              AND TO_CHAR(K.DATA,'hh24:mi') BETWEEN vOraIni 
                                                                                                AND vOraFin 
                                       GROUP BY T.COD_ELEMENTO,T.COD_TIPO_MISURA,DATA
                                     ) K
                               INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
                             )
            SELECT TIPO_INDICE,TO_DATE(DATA,'yyyymmdd') DATA,
                   h0000.VALORE v0000, --h0015.VALORE v0015, h0030.VALORE v0030, h0045.VALORE v0045,
                   h0100.VALORE v0100, --h0115.VALORE v0115, h0130.VALORE v0130, h0145.VALORE v0145,
                   h0200.VALORE v0200, --h0215.VALORE v0215, h0230.VALORE v0230, h0245.VALORE v0245,
                   h0300.VALORE v0300, --h0315.VALORE v0315, h0330.VALORE v0330, h0345.VALORE v0345,
                   h0400.VALORE v0400, --h0415.VALORE v0415, h0430.VALORE v0430, h0445.VALORE v0445,
                   h0500.VALORE v0500, --h0515.VALORE v0515, h0530.VALORE v0530, h0545.VALORE v0545,
                   h0600.VALORE v0600, --h0615.VALORE v0615, h0630.VALORE v0630, h0645.VALORE v0645,
                   h0700.VALORE v0700, --h0715.VALORE v0715, h0730.VALORE v0730, h0745.VALORE v0745,
                   h0800.VALORE v0800, --h0815.VALORE v0815, h0830.VALORE v0830, h0845.VALORE v0845,
                   h0900.VALORE v0900, --h0915.VALORE v0915, h0930.VALORE v0930, h0945.VALORE v0945,
                   h1000.VALORE v1000, --h1015.VALORE v1015, h1030.VALORE v1030, h1045.VALORE v1045,
                   h1100.VALORE v1100, --h1115.VALORE v1115, h1130.VALORE v1130, h1145.VALORE v1145,
                   h1200.VALORE v1200, --h1215.VALORE v1215, h1230.VALORE v1230, h1245.VALORE v1245,
                   h1300.VALORE v1300, --h1315.VALORE v1315, h1330.VALORE v1330, h1345.VALORE v1345,
                   h1400.VALORE v1400, --h1415.VALORE v1415, h1430.VALORE v1430, h1445.VALORE v1445,
                   h1500.VALORE v1500, --h1515.VALORE v1515, h1530.VALORE v1530, h1545.VALORE v1545,
                   h1600.VALORE v1600, --h1615.VALORE v1615, h1630.VALORE v1630, h1645.VALORE v1645,
                   h1700.VALORE v1700, --h1715.VALORE v1715, h1730.VALORE v1730, h1745.VALORE v1745,
                   h1800.VALORE v1800, --h1815.VALORE v1815, h1830.VALORE v1830, h1845.VALORE v1845,
                   h1900.VALORE v1900, --h1915.VALORE v1915, h1930.VALORE v1930, h1945.VALORE v1945,
                   h2000.VALORE v2000, --h2015.VALORE v2015, h2030.VALORE v2030, h2045.VALORE v2045,
                   h2100.VALORE v2100, --h2115.VALORE v2115, h2130.VALORE v2130, h2145.VALORE v2145,
                   h2200.VALORE v2200, --h2215.VALORE v2215, h2230.VALORE v2230, h2245.VALORE v2245,
                   h2300.VALORE v2300  --h2315.VALORE v2315, h2330.VALORE v2330, h2345.VALORE v2345
              FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,DATA,TIPO_INDICE 
                      FROM MyMisure 
                     GROUP BY COD_ELEMENTO,COD_GEST_ELEMENTO,DATA,TIPO_INDICE
                   )
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0000') h0000 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0015') h0015 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0030') h0030 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0045') h0045 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0100') h0100 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0115') h0115 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0130') h0130 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0145') h0145 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0200') h0200 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0215') h0215 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0230') h0230 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0245') h0245 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0300') h0300 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0315') h0315 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0330') h0330 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0345') h0345 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0400') h0400 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0415') h0415 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0430') h0430 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0445') h0445 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0500') h0500 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0515') h0515 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0530') h0530 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0545') h0545 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0600') h0600 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0615') h0615 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0630') h0630 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0645') h0645 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0700') h0700 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0715') h0715 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0730') h0730 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0745') h0745 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0800') h0800 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0815') h0815 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0830') h0830 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0845') h0845 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0900') h0900 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0915') h0915 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0930') h0930 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '0945') h0945 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1000') h1000 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1015') h1015 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1030') h1030 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1045') h1045 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1100') h1100 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1115') h1115 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1130') h1130 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1145') h1145 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1200') h1200 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1215') h1215 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1230') h1230 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1245') h1245 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1300') h1300 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1315') h1315 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1330') h1330 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1345') h1345 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1400') h1400 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1415') h1415 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1430') h1430 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1445') h1445 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1500') h1500 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1515') h1515 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1530') h1530 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1545') h1545 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1600') h1600 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1615') h1615 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1630') h1630 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1645') h1645 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1700') h1700 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1715') h1715 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1730') h1730 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1745') h1745 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1800') h1800 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1815') h1815 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1830') h1830 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1845') h1845 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1900') h1900 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1915') h1915 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1930') h1930 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '1945') h1945 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2000') h2000 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2015') h2015 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2030') h2030 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2045') h2045 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2100') h2100 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2115') h2115 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2130') h2130 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2145') h2145 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2200') h2200 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2215') h2215 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2230') h2230 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2245') h2245 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2300') h2300 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2315') h2315 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2330') h2330 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
              --LEFT OUTER JOIN (SELECT * FROM MyMisure WHERE ORA = '2345') h2345 USING (COD_ELEMENTO,COD_GEST_ELEMENTO,TIPO_INDICE,DATA)
             ORDER BY DATA,TIPO_INDICE;
*/

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetValori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetValori;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_KPI;
/
SHOW ERRORS;


