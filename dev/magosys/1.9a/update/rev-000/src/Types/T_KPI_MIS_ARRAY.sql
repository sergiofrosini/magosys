PROMPT TYPE T_KPI_MIS_ARRAY;
--
-- T_KPI_MIS_ARRAY  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--   T_KPI_MIS_OBJ (Type)
--
CREATE OR REPLACE TYPE T_KPI_MIS_ARRAY  AS TABLE OF T_KPI_MIS_OBJ  ;
/
SHOW ERRORS;


