PROMPT VIEW V_GERARCHIA_IMPIANTO_AT_MT;
--
-- V_GERARCHIA_IMPIANTO_AT_MT  (View) 
--
--  Dependencies: 
--   PKG_ELEMENTI (Package)
--   ESERCIZI (Table)
--   AVVOLGIMENTI_SA (Table)
--   IMPIANTIAT_SA (Table)
--   MONTANTIMT_SA (Table)
--   SEZIONATORI_SA (Table)
--   TRASFORMATORIAT_SA (Table)
--   SBARRE_SA (Table)
--   IMPIANTIMT_SA (Table)
--   ELEMENTI (Table)
--
CREATE OR REPLACE FORCE VIEW V_GERARCHIA_IMPIANTO_AT_MT
AS 
WITH MyEsercizi AS (SELECT el.COD_ELEMENTO ese,CODICE_ST ese_st,COD_TIPO_ELEMENTO tipo
                      FROM CORELE.ESERCIZI es
                     INNER JOIN ELEMENTI el ON COD_GEST_ELEMENTO = es.COD_GEST
                     WHERE SYSDATE BETWEEN es.DATA_INIZIO AND es.DATA_FINE
                   )
    ,MyCabPrim  AS (SELECT COD_ELEMENTO cpr,CODICE_ST cpr_st,COD_ESERCIZIO ese_st,COD_TIPO_ELEMENTO tipo
                      FROM CORELE.IMPIANTIAT_SA
                     INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                     WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                   )
    ,MyTrasfAT  AS (SELECT tra,tra_st,cpr_st,tipo
                      FROM (SELECT CODICE_ST sat_st,CODICEST_IMPAT cpr_st
                              FROM CORELE.SBARRE_SA S
                             WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND TIPO = 'AT'
                             UNION ALL /* SELEZIONA EVENTUALI SBARRE MT CHE ALIMENTANO IL PRIMARIO DI UN TRASF.MT/MT */
                            SELECT CODICE_ST sat_st,CODICEST_IMPAT cpr_st
                              FROM CORELE.SBARRE_SA S
                             WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                               AND CODICE_ST IN (SELECT /*+ PUSH_SUBQ */ CODICEST_SBARRA
                                                   FROM CORELE.AVVOLGIMENTI_SA AVVOLGIMENTI_SA1
                                                  WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                                    AND COD_ENTE = 'PRIM'
                                                  GROUP BY CODICEST_SBARRA)
                           )
                     INNER JOIN
                          (SELECT CODICE_ST avv_prim_st,CODICEST_SBARRA sat_st
                             FROM CORELE.AVVOLGIMENTI_SA
                            WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND COD_ENTE = 'PRIM' /*AND STATO = 'CH'*/
                          ) TRP USING (sat_st)
                    INNER JOIN
                          (SELECT COD_ELEMENTO tra,CODICE_ST tra_st,CODICEST_AVV_PRIM avv_prim_st,COD_TIPO_ELEMENTO tipo
                             FROM CORELE.TRASFORMATORIAT_SA
                            INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                            WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                          ) TRF USING (avv_prim_st)
                   )
    ,MySecTer   AS (SELECT COD_ELEMENTO sec,TR.CODICEST_AVV_SECN avv_sec_st,TR.CODICE_ST tra_st,COD_TIPO_ELEMENTO tipo
                      FROM CORELE.TRASFORMATORIAT_SA TR
                     INNER JOIN CORELE.AVVOLGIMENTI_SA AV ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                                         AND AV.CODICE_ST = TR.CODICEST_AVV_SECN AND AV.STATO = 'CH'
                     INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = AV.COD_GEST
                     WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                    UNION
                    SELECT COD_ELEMENTO ter,TR.CODICEST_AVV_TERZ avv_ter_st,TR.CODICE_ST tra_st,COD_TIPO_ELEMENTO tipo
                      FROM CORELE.TRASFORMATORIAT_SA TR
                     INNER JOIN CORELE.AVVOLGIMENTI_SA AV ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                                         AND AV.CODICE_ST = TR.CODICEST_AVV_TERZ AND AV.STATO = 'CH'
                     INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = AV.COD_GEST
                     WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                   )
    ,MySbarreMT AS (SELECT COD_ELEMENTO smt,S.CODICE_ST smt_st,S.CODICEST_AVV alim_st,COD_TIPO_ELEMENTO tipo
                          FROM CORELE.SBARRE_SA S
                          INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = S.COD_GEST
                          LEFT OUTER JOIN (SELECT CODICEST_SBARRA1 CODICE_ST
                                             FROM CORELE.SEZIONATORI_SA Z
                                            INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA1 = S.CODICE_ST
                                            WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                              AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                           UNION
                                           SELECT CODICEST_SBARRA2 CODICE_ST
                                             FROM CORELE.SEZIONATORI_SA Z
                                            INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA2 = S.CODICE_ST
                                            WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                              AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                          ) Z ON (Z.CODICE_ST = S.CODICE_ST)
                         WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                           AND S.CODICEST_AVV <> 'ND'
                   )
    ,MyLineeMT  AS (SELECT COD_ELEMENTO lmt,CODICE_ST lmt_st,CODICEST_SBARRA smt_st,COD_TIPO_ELEMENTO tipo
                      FROM CORELE.MONTANTIMT_SA
                     INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                     WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND STATO = 'CH'
                   )
SELECT A.COD_TIPO_ELEMENTO||'-'||B.TIPO tipo,  /* associo ESERCIZI ad Elemento Base */
       A.COD_ELEMENTO   Padre,
       B.ese            Figlio
  FROM ELEMENTI A,
       MyEsercizi B
 WHERE A.COD_ELEMENTO = PKG_ELEMENTI.GetElementoBase
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo CABINE PRIMARIE ad ESERCIZIO */
       B.ese            Padre,
       A.cpr            Figlio
  FROM MyCabPrim A
 INNER JOIN MyEsercizi B ON B.ese_st = A.ese_st
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo TRASFORMATORI a CABINA PRIMARIA */
       B.cpr            Padre,
       A.tra            Figlio
  FROM MyTrasfAT A
 INNER JOIN MyCabPrim B ON B.cpr_st = A.cpr_st 
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo SECONDARI/TERZIARI a TRASFORMATORE */
       B.tra            Padre,
       A.sec            Figlio
  FROM MySecTer A
 INNER JOIN MyTrasfAT B ON B.tra_st = A.tra_st
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo SBARRE MT a SECONDARIO/TERZIARIO */
       B.sec            Padre,
       A.smt            Figlio
  FROM MySbarreMT A
 INNER JOIN MySecTer B ON B.avv_sec_st = A.alim_st
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo LINEE MT a SBARRA MT */
       B.smt            Padre,
       A.lmt            Figlio
  FROM MyLineeMT A
 INNER JOIN MySbarreMT B ON B.smt_st = A.smt_st
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo SBARRE MT a LINEE MT - SbarreMT alimentate da Linee di altra CP (es: Centro Satellite) */
       B.lmt            Padre,
       A.smt            Figlio
  FROM MySbarreMT A
 INNER JOIN MyLineeMT B ON B.lmt_st = A.alim_st
UNION ALL
SELECT B.TIPO||'-'||A.TIPO tipo,    /* associo SBARRE DI CABINA SECONRARIA a LINEA MT */
       B.lmt            Padre,
       A.scs            Figlio
  FROM (SELECT COD_ELEMENTO scs,CODICEST_MONTANTE lmt_st,COD_TIPO_ELEMENTO tipo
          FROM CORELE.IMPIANTIMT_SA
         INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST_SBARRA
         WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
       ) A
 INNER JOIN MyLineeMT B ON B.lmt_st = A.lmt_st
/
