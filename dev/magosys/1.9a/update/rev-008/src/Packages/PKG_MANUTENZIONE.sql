Prompt Package PKG_MANUTENZIONE;
--
-- PKG_MANUTENZIONE  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   LOG_HISTORY_INFO (Table)
--   METEO_JOB_STATIC_CONFIG (Table)
--   METEO_FILE_ZIP (Table)
--   METEO_FILE_XML (Table)
--   LOG_HISTORY (Table)
--   PKG_UTLGLB (Synonym)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_UTLGLB ()
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--   PKG_KPI (Package)
--
CREATE OR REPLACE PACKAGE PKG_MANUTENZIONE AS

/* ***********************************************************************************************************
   NAME:       PKG_Manutenzione
   PURPOSE:    Servizi per la manutenzione dell'applicazione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.9.a.8    19/12/2014  Moretti C.       Pulizia Richieste e Indici KPI
                                           Lancio PKG_KPI.PuliziaRichieste

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PuliziaGiornaliera;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Manutenzione;
/
SHOW ERRORS;


