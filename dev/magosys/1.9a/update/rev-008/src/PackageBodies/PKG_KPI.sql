PROMPT PACKAGE BODY PKG_KPI;
--
-- PKG_KPI  (Package Body) 
--
--  Dependencies: 
--   PKG_KPI (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_KPI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9.a.8
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LeggiRichieste    (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE,
                              pStati             IN VARCHAR2,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste presenti   
 -----------------------------------------------------------------------------------------------------------*/
    
    vFlgNull    NUMBER(1)     := -1;
    vTmpKey     GTTD_VALORI_TEMP.TIP%TYPE := 'KPISTATI';  
    vMaxNumRows  CONSTANT NUMBER := 250;
 
    -- ATTENZIONE: Il cursore DEVE corrispondere alla struttura del type T_KPI_RICHIESTA_OBJ !!
    
    vSql VARCHAR2(2500) := 
               'WITH MyKpiReq AS (SELECT * '                                                                                                      ||
                                   'FROM KPI_RICHIESTA R '                                                                                        ||
                                  'WHERE R.DATA_SCADENZA >= SYSDATE '                                                                             ||
                                        '#FILTRO_CODICE# #FILTRO_OPERATORE# #FILTRO_STATI# '                                                      ||
                            ') '                                                                                                                  ||
               'SELECT ID_REQ_KPI,'                                                                                                               ||
                      'R.DATA_RICHIESTA,'                                                                                                         ||
                      'R.DATA_SCADENZA,'                                                                                                          ||
                      'R.OPERATORE,'                                                                                                              ||
                      'R.PERIODO_INIZIO,'                                                                                                         ||
                      'R.PERIODO_FINE,'                                                                                                           ||
                      'R.ORARIO_INIZIO,'                                                                                                          ||
                      'R.ORARIO_FINE,'                                                                                                            ||
                      'R.RISOLUZIONE,'                                                                                                            ||
                      'R.ORGANIZZAZIONE,'                                                                                                         ||
                      'R.STATO_RETE,'                                                                                                             ||
                      'R.LISTA_FONTI,'                                                                                                            ||
                      'R.LISTA_TIPI_RETE,'                                                                                                        ||
                      'R.LISTA_TIPI_CLIE,'                                                                                                        ||
                      'R.LISTA_INDICI_RICH,'                                                                                                      ||
                      'R.LISTA_TIPI_MISURA,'                                                                                                      ||
                      'R.LISTA_TIPI_MISURA_FORECAST,'                                                                                             ||
                      'ELE.LISTA_COD_ELEMENTO,'                                                                                                   ||
                      'ELE.LISTA_COD_GEST_ELEMENTO,'                                                                                              ||
                      'R.FREQ_ERR_VAL_INI,'                                                                                                       ||
                      'R.FREQ_ERR_VAL_INI_PERC,'                                                                                                  ||
                      'R.FREQ_ERR_VAL_FIN,'                                                                                                       ||
                      'R.FREQ_ERR_VAL_FIN_PERC,'                                                                                                  ||
                      'R.FREQ_ERR_PASSO,'                                                                                                         ||
                      'R.FREQ_ERR_PASSO_PERC,'                                                                                                    ||
                      'R.SOGLIA_COLORE,'                                                                                                          ||
                      'R.SOGLIA_COLORE_PERC,'                                                                                                     ||
                      'R.VALORE_SOGLIA,'                                                                                                          ||
                      'R.DELTA,'                                                                                                                  ||
                      'R.TIPO_NOTIFICA,'                                                                                                          ||
                      'R.ELAB_INIZIO,'                                                                                                            ||
                      'R.ELAB_FINE,'                                                                                                              ||
                      'R.ELAB_STATO,'                                                                                                             ||
                      'R.DATA_STATO,'                                                                                                             ||
                      'R.OPER_STATO,'                                                                                                             ||
                      'R.ELAB_NOTE,'                                                                                                              ||
                      '(100*NVL(A.daElab,0))/(NVL(A.Elab,0)+NVL(A.daElab,0)) AVANZAMENTO '                                                        ||
                 'FROM MyKpiReq R '                                                                                                               ||
                 'LEFT OUTER JOIN (SELECT SUBSTR(LISTA_COD_ELEMENTO,2,LENGTH(LISTA_COD_ELEMENTO)) LISTA_COD_ELEMENTO,'                            ||
                                         'SUBSTR(LISTA_COD_GEST_ELEMENTO,2,LENGTH(LISTA_COD_GEST_ELEMENTO)) LISTA_COD_GEST_ELEMENTO,'             ||
                                         'ID_REQ_KPI '                                                                                            ||
                                   'FROM (SELECT ID_REQ_KPI,NUM, TOT,'                                                                            ||
                                                'SYS_CONNECT_BY_PATH(COD_ELEMENTO,''|'') LISTA_COD_ELEMENTO,'                                     ||
                                                'SYS_CONNECT_BY_PATH(COD_GEST_ELEMENTO,''|'') LISTA_COD_GEST_ELEMENTO '                           ||
                                           'FROM (SELECT * '                                                                                      ||
                                                   'FROM (SELECT E.COD_ELEMENTO,'                                                                 ||
                                                                'CASE '                                                                           ||
                                                                  'WHEN E.COD_TIPO_ELEMENTO = '''||PKG_Mago.gcLineaMT||''' '                      ||
                                                                      'THEN NVL(D.RIF_ELEMENTO,E.COD_GEST_ELEMENTO) '                             ||
                                                                      'ELSE E.COD_GEST_ELEMENTO '                                                 ||
                                                                'END COD_GEST_ELEMENTO,'                                                          ||
                                                                'K.ID_REQ_KPI,'                                                                   ||
                                                                'COUNT(*) OVER (PARTITION BY R.ID_REQ_KPI) TOT,'                                  ||
                                                                'ROW_NUMBER() OVER (PARTITION BY R.ID_REQ_KPI ORDER BY E.COD_GEST_ELEMENTO) NUM ' ||
                                                             'FROM MyKpiReq R '                                                                   ||
                                                            'INNER JOIN KPI_ELEMENTI K ON K.ID_REQ_KPI = R.ID_REQ_KPI '                           ||
                                                            'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO '                           ||
                                                                                 'AND K.KEYTYPE = ''' ||gcSelElem || ''' '                        ||
                                                             'LEFT OUTER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO '                 ||
                                                                                           'AND R.PERIODO_FINE BETWEEN D.DATA_ATTIVAZIONE '       ||
                                                                                                                  'AND D.DATA_DISATTIVAZIONE '    ||
                                                        ') A '                                                                                    ||
                                                  'WHERE NUM <= ' || vMaxNumRows || ' '                                                           ||
                                                ') A START WITH NUM = 1 CONNECT BY PRIOR NUM + 1 = NUM AND PRIOR ID_REQ_KPI = ID_REQ_KPI '        ||
                                        ') WHERE (NUM = TOT OR NUM = ' || vMaxNumRows || ' )'                                                     ||
                                 ') ELE ON (ELE.ID_REQ_KPI = R.ID_REQ_KPI) '                                                                      || 
                 'INNER JOIN (SELECT R.ID_REQ_KPI '                                                                                               ||
                                   ',COUNT(CASE WHEN E.STATO = 0 THEN 1 END) Elab, COUNT(CASE WHEN E.STATO <> 0 THEN 1 END) daElab '              ||
                                    'FROM MyKpiReq R '                                                                                            ||
                                   'INNER JOIN KPI_ELEMENTI E ON E.ID_REQ_KPI = R.ID_REQ_KPI '                                                    ||
                                   'WHERE E.KEYTYPE = ''' || gcPopElem || ''' '                                                                   ||
                                   'GROUP BY R.ID_REQ_KPI '                                                                                       ||
                                 ') A ON (A.ID_REQ_KPI = R.ID_REQ_KPI ) '                                                                         ||
                'WHERE R.DATA_SCADENZA >= SYSDATE  '                                                                                              ||
               'ORDER BY ID_REQ_KPI DESC';

 BEGIN

    DELETE GTTD_VALORI_TEMP WHERE TIP = vTmpKey;
    --PRINT('pOPERATORE '||pOPERATORE);
    --PRINT('pStati '||pStati);
    --PRINT('pID_REQ_KPI '||pID_REQ_KPI);
    IF pStati IS NOT NULL THEN
        PKG_Mago.TrattaListaCodici(pStati,vTmpKey,vFlgNull); -- non gestito il flag
    END IF;
    IF pID_REQ_KPI IS NOT NULL THEN
        vSql := REPLACE(vSql,'#FILTRO_CODICE#'   ,'AND R.ID_REQ_KPI = :cod');
        vSql := REPLACE(vSql,'#FILTRO_OPERATORE#',''); 
        vSql := REPLACE(vSql,'#FILTRO_STATI#'    ,'');
    ELSE 
        vSql := REPLACE(vSql,'#FILTRO_CODICE#'   ,'');
        IF pOPERATORE IS NOT NULL THEN
            vSql := REPLACE(vSql,'#FILTRO_OPERATORE#','AND R.OPERATORE = :ope');
        ELSE
            vSql := REPLACE(vSql,'#FILTRO_OPERATORE#','');
        END IF;
        IF pStati IS NOT NULL THEN
            vSql := REPLACE(vSql,'#FILTRO_STATI#','AND ELAB_STATO IN (SELECT TO_NUMBER(ALF1) FROM GTTD_VALORI_TEMP WHERE TIP = '''||vTmpKey||''')');
        ELSE
            vSql := REPLACE(vSql,'#FILTRO_STATI#','');
        END IF;
    END IF;

    vSql := PKG_UtlGlb.CompattaSelect(vSql);
    --DBMS_OUTPUT.PUT_LINE(vSql);
    CASE 
        WHEN pID_REQ_KPI IS NOT NULL THEN
            --PRINT('1 - '||vSql); 
            OPEN pRefCurs FOR vSql USING pID_REQ_KPI;
        WHEN pOPERATORE IS NOT NULL THEN
            --PRINT('2 - '||vSql); 
            OPEN pRefCurs FOR vSql USING pOPERATORE;
        ELSE
            --PRINT('3 - '||vSql); 
            OPEN pRefCurs FOR vSql;
    END CASE;

 END LeggiRichieste;

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertRichiesta      (pNewReq            IN T_KPI_RICHIESTA_OBJ,
                                 pElementi          IN T_COD_GEST_ARRAY,
                                 pID_REQ_KPI       OUT NUMBER) AS
 /*-----------------------------------------------------------------------------------------------------------
    Inserimento di una nuova richiesta 
 -----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1)     := -1;
      
 BEGIN

    DELETE GTTD_VALORI_TEMP; 
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_FONTI,                gcTipFonti,        vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_RETE,            gcTipReti,         vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_CLIE,            gcTipClienti,      vFlgNull);    -- non gestito il flag            
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_INDICI_RICH,          gcIndici,          vFlgNull);    -- non gestito il flag
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_MISURA,          gcTipiMisura,      vFlgNull);    -- non gestito il flag
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_TIPI_MISURA_FORECAST, gcTipiMisForecast, vFlgNull);    -- non gestito il flag
    PKG_Mago.TrattaListaCodici(pNewReq.LISTA_ELEMENTI,             gcElem,            vFlgNull);    -- non gestito il flag
        
    INSERT INTO KPI_RICHIESTA (DATA_SCADENZA,
                               OPERATORE,
                               PERIODO_INIZIO,
                               PERIODO_FINE,
                               ORARIO_INIZIO,
                               ORARIO_FINE,
                               RISOLUZIONE,
                               ORGANIZZAZIONE,
                               STATO_RETE,
                               LISTA_FONTI,
                               LISTA_TIPI_RETE,
                               LISTA_TIPI_CLIE,
                               LISTA_INDICI_RICH,
                               LISTA_TIPI_MISURA,
                               LISTA_TIPI_MISURA_FORECAST,
                               FREQ_ERR_VAL_INI,
                               FREQ_ERR_VAL_INI_PERC,
                               FREQ_ERR_VAL_FIN,
                               FREQ_ERR_VAL_FIN_PERC,
                               FREQ_ERR_PASSO,
                               FREQ_ERR_PASSO_PERC,
                               SOGLIA_COLORE,
                               SOGLIA_COLORE_PERC,
                               VALORE_SOGLIA,
                               DELTA,
                               TIPO_NOTIFICA,
                               ELAB_INIZIO,
                               ELAB_FINE,
                               ELAB_STATO,
                               DATA_STATO,
                               OPER_STATO,
                               ELAB_NOTE
                              )
                       VALUES (pNewReq.DATA_SCADENZA,
                               pNewReq.OPERATORE,
                               pNewReq.PERIODO_INIZIO,
                               pNewReq.PERIODO_FINE,
                               pNewReq.ORARIO_INIZIO,
                               pNewReq.ORARIO_FINE,
                               pNewReq.RISOLUZIONE,
                               pNewReq.ORGANIZZAZIONE,
                               pNewReq.STATO_RETE,
                               pNewReq.LISTA_FONTI,
                               pNewReq.LISTA_TIPI_RETE,
                               pNewReq.LISTA_TIPI_CLIE,
                               pNewReq.LISTA_INDICI_RICH,
                               pNewReq.LISTA_TIPI_MISURA,
                               pNewReq.LISTA_TIPI_MISURA_FORECAST,
                               pNewReq.FREQ_ERR_VAL_INI,
                               pNewReq.FREQ_ERR_VAL_INI_PERC,
                               pNewReq.FREQ_ERR_VAL_FIN,
                               pNewReq.FREQ_ERR_VAL_FIN_PERC,
                               pNewReq.FREQ_ERR_PASSO,
                               pNewReq.FREQ_ERR_PASSO_PERC,
                               pNewReq.SOGLIA_COLORE,
                               pNewReq.SOGLIA_COLORE_PERC,
                               pNewReq.VALORE_SOGLIA,
                               pNewReq.DELTA,
                               pNewReq.TIPO_NOTIFICA,
                               pNewReq.ELAB_INIZIO,
                               pNewReq.ELAB_FINE,
                               pNewReq.ELAB_STATO,
                               pNewReq.DATA_STATO,
                               pNewReq.OPER_STATO,
                               pNewReq.ELAB_NOTE
                              )
                    RETURNING ID_REQ_KPI INTO pID_REQ_KPI;
    
    INSERT INTO KPI_PARAMETRI (ID_REQ_KPI, TIPO, PARAMETRO)
        SELECT pID_REQ_KPI, TIP, ALF1 
          FROM GTTD_VALORI_TEMP
          WHERE TIP != gcElem;

    INSERT INTO KPI_ELEMENTI (ID_REQ_KPI, COD_ELEMENTO, KEYTYPE)
        SELECT pID_REQ_KPI, ALF1, PKG_KPI.gcSelElem 
          FROM GTTD_VALORI_TEMP
         WHERE TIP = gcElem;

    IF pElementi.LAST IS NOT NULL THEN
        INSERT INTO KPI_ELEMENTI (ID_REQ_KPI, COD_ELEMENTO, KEYTYPE)
            SELECT pID_REQ_KPI,
                   COD_ELEMENTO,
                   PKG_KPI.gcPopElem
              FROM TABLE (CAST(pElementi AS T_COD_GEST_ARRAY)) A
             INNER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = A.COD_GEST;
    END IF;

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.InsertRichiesta'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END InsertRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetRichieste      (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE DEFAULT NULL,
                              pStati             IN VARCHAR2 DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste definite        
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    LeggiRichieste(pRefCurs,pOPERATORE,pStati,NULL);
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetRichieste'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRichieste;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetRichiesta         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                                 pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna la definizione e lo stato delle richieste presenti   
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    LeggiRichieste(pRefCurs,NULL,NULL,pID_REQ_KPI);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetRichiesta'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetElementsByReq  (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    ritorna gli elementi coinvolti nella richiesta indicata
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    OPEN pRefCurs FOR SELECT E.COD_ELEMENTO, 
                             CASE 
                                 WHEN E.COD_TIPO_ELEMENTO = PKG_Mago.gcLineaMT
                                    THEN NVL(D.RIF_ELEMENTO,E.COD_GEST_ELEMENTO)
                                    ELSE E.COD_GEST_ELEMENTO
                             END COD_GEST_ELEMENTO, 
                             D.NOME_ELEMENTO, 
                             K.STATO 
                        FROM KPI_RICHIESTA R 
                       INNER JOIN KPI_ELEMENTI K ON K.ID_REQ_KPI = R.ID_REQ_KPI AND K.KEYTYPE = gcPopElem
                       INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
                        LEFT OUTER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = K.COD_ELEMENTO
                                                      AND R.PERIODO_FINE BETWEEN D.DATA_ATTIVAZIONE 
                                                                             AND D.DATA_DISATTIVAZIONE 
                       WHERE R.ID_REQ_KPI = pID_REQ_KPI;

/* 
    ---------------------------------------------------------------------------------------------------------------------------
    -- per uso futuro nel caso si debba associare la foglia al nodo
    ---------------------------------------------------------------------------------------------------------------------------

    OPEN pRefCurs FOR WITH MyElem     AS (SELECT E.COD_ELEMENTO, 
                                                 CASE 
                                                     WHEN E.COD_TIPO_ELEMENTO = PKG_Mago.gcLineaMT
                                                      THEN NVL(D.RIF_ELEMENTO,E.COD_GEST_ELEMENTO)
                                                      ELSE E.COD_GEST_ELEMENTO
                                                 END COD_GEST_ELEMENTO, 
                                                 D.NOME_ELEMENTO, 
                                                 K.STATO,
                                                 KEYTYPE
                                            FROM KPI_RICHIESTA R 
                                           INNER JOIN KPI_ELEMENTI K ON K.ID_REQ_KPI = R.ID_REQ_KPI 
                                           INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
                                            LEFT OUTER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = K.COD_ELEMENTO
                                                                          AND R.PERIODO_FINE BETWEEN D.DATA_ATTIVAZIONE 
                                                                                                 AND D.DATA_DISATTIVAZIONE 
                                            WHERE R.ID_REQ_KPI = pID_REQ_KPI
                                         ),
                           MySelElem  AS (SELECT * FROM MyElem WHERE KEYTYPE = gcSelElem),  
                           MyPopElem  AS (SELECT * FROM MyElem WHERE KEYTYPE = gcPopElem)
                      SELECT C.COD_ELEMENTO,
                             C.COD_GEST_ELEMENTO,
                             C.NOME_ELEMENTO,
                             C.STATO,
                             A.COD_ELEMENTO COD_NODO,
                             A.COD_GEST_ELEMENTO GEST_NODO,
                             A.NOME_ELEMENTO NOME_NODO
                        FROM (SELECT * FROM MySelElem) A
                       INNER JOIN TABLE(PKG_ELEMENTI.LEGGIGERARCHIAINF(A.COD_ELEMENTO, 1, 2, SYSDATE)) B 
                              ON B.COD_ELEMENTO IN (SELECT COD_ELEMENTO FROM MyPopElem)
                       INNER JOIN (SELECT * FROM MyPopElem) C ON C.COD_ELEMENTO = B.COD_ELEMENTO  
                       ORDER BY A.COD_GEST_ELEMENTO, B.COD_GEST_ELEMENTO;
*/

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetElementsByReq'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetElementsByReq;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoRichiesta (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pELAB_STATO        IN KPI_RICHIESTA.ELAB_STATO%TYPE,
                              pOPER_STATO        IN KPI_RICHIESTA.OPER_STATO%TYPE DEFAULT NULL, 
                              pDATA_STATO        IN KPI_RICHIESTA.DATA_STATO%TYPE DEFAULT SYSDATE, 
                              pELAB_NOTE         IN KPI_RICHIESTA.ELAB_NOTE%TYPE DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di una richiesta
 -----------------------------------------------------------------------------------------------------------*/
    vDATA_STATO KPI_RICHIESTA.DATA_STATO%TYPE := NVL(pDATA_STATO,SYSDATE);
 BEGIN
    CASE pELAB_STATO
        WHEN gcStatoDaElaborare THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_INIZIO = NULL,
                                     ELAB_FINE   = NULL,
                                     OPER_STATO  = pOPER_STATO,
                                     DATA_STATO  = NVL(vDATA_STATO,SYSDATE),
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoInElaborazione THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_INIZIO = SYSDATE,
                                     ELAB_FINE   = NULL,
                                     OPER_STATO  = pOPER_STATO,
                                     DATA_STATO  = NVL(vDATA_STATO,SYSDATE),
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoElabTerminata THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_FINE   = SYSDATE,
                                     OPER_STATO  = pOPER_STATO,
                                     DATA_STATO  = NVL(vDATA_STATO,SYSDATE),
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoElabInterrotta THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     ELAB_FINE   = SYSDATE,
                                     OPER_STATO  = pOPER_STATO,
                                     DATA_STATO  = NVL(vDATA_STATO,SYSDATE),
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        WHEN gcStatoErroreDiElab THEN
            UPDATE KPI_RICHIESTA SET ELAB_STATO  = pELAB_STATO,
                                     OPER_STATO  = pOPER_STATO,
                                     DATA_STATO  = NVL(vDATA_STATO,SYSDATE),
                                     ELAB_NOTE   = pELAB_NOTE
             WHERE ID_REQ_KPI = pID_REQ_KPI;
        ELSE
            RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||pELAB_STATO||') per richiesta n. '||pID_REQ_KPI);
    END CASE;  

    IF SQL%ROWCOUNT = 0 THEN
        RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Richiesta n. '||pID_REQ_KPI||' non presente !');
    END IF;

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.SetStatoRichiesta - richiesta n. '||pID_REQ_KPI||' - Stato '||pELAB_STATO||
                                        CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END SetStatoRichiesta;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoElemento     (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                                 pCOD_ELEMENTO      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pSTATO             IN KPI_ELEMENTI.STATO%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di un elemento per la richiesta 
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    IF pStato IN (gcStatoDaElaborare,gcStatoInElaborazione) THEN
        UPDATE KPI_ELEMENTI SET STATO = pSTATO
         WHERE ID_REQ_KPI = pID_REQ_KPI
           AND COD_ELEMENTO = pCOD_ELEMENTO
           AND KEYTYPE = gcPopElem;  
    ELSE
        RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||pSTATO||')');
    END IF;

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.SetStatoElemento - richiesta n. '||pID_REQ_KPI||' - Elem/Stato '||pCOD_ELEMENTO||'/'||pSTATO||
                                        CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END SetStatoElemento;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE SetStatoElementoCG   (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                                 pCOD_GEST_ELEMENTO IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pSTATO             IN KPI_ELEMENTI.STATO%TYPE) AS
 /*-----------------------------------------------------------------------------------------------------------
    aggiorna lo stato di un elemento per la richiesta 
 -----------------------------------------------------------------------------------------------------------*/
 BEGIN

    SetStatoElemento(pID_REQ_KPI,PKG_ELEMENTI.GetCodElemento(pCOD_GEST_ELEMENTO),pSTATO);

 END SetStatoElementoCG;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertValori      (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pTabValIndici      IN T_KPI_MIS_ARRAY) AS
 /*-----------------------------------------------------------------------------------------------------------
    Inserisce i valori ricevuti in tabella KPI_INDICI relativi alla riciesta indicata
 -----------------------------------------------------------------------------------------------------------*/
 
    vRisoluzione KPI_RICHIESTA.RISOLUZIONE%TYPE;  
 
 BEGIN

--PKG_Logs.TraceLog('Misure ricevute '||NVL(pTabValIndici.LAST,0));

    IF pTabValIndici.LAST IS NULL THEN
        RETURN;
    END IF;

--FOR i IN pTabValIndici.FIRST .. pTabValIndici.LAST LOOP
--    PKG_Logs.TraceLog(i||' - COD_ELEMENTO      '||NVL(TO_CHAR(pTabValIndici(i).COD_ELEMENTO),'<CodNull>'));
--    PKG_Logs.TraceLog(i||' - COD_GEST_ELEMENTO '||NVL(TO_CHAR(pTabValIndici(i).COD_GEST_ELEMENTO),'<GestNull>'));
--    PKG_Logs.TraceLog(i||' - COD_INDICE        '||NVL(pTabValIndici(i).COD_INDICE,'<IndNull>'));
--    PKG_Logs.TraceLog(i||' - DATA              '||NVL(TO_CHAR(pTabValIndici(i).DATA,'dd/mm/yyyy hh24:mi:ss'),'<DataNull>'));
--    PKG_Logs.TraceLog(i||' - VALORE            '||NVL(TO_CHAR(pTabValIndici(i).VALORE),'<ValNull>'));
--    PKG_Logs.TraceLog(RPAD('-',30,'-'));
--END LOOP;

    SELECT RISOLUZIONE 
      INTO vRisoluzione
      FROM KPI_RICHIESTA
     WHERE ID_REQ_KPI = pID_REQ_KPI;
    
    DELETE GTTD_MISURE;
    INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                             COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM,DATA,VALORE)
          SELECT R.COD_TRATTAMENTO_ELEM,G.COD_ELEMENTO,G.COD_TIPO_MISURA,G.COD_TIPO_FONTE,
                 NVL(T.COD_TIPO_RETE,PKG_Mago.gcTipReteNonDisp) COD_TIPO_RETE,
                 NVL(D.COD_TIPO_CLIENTE,PKG_Mago.gcClientePrdNonApplic) COD_TIPO_CLIENTE,E.COD_TIPO_ELEMENTO,G.DATA,G.VALORE
            FROM (SELECT CASE 
                            WHEN COD_ELEMENTO IS NOT NULL 
                                THEN COD_ELEMENTO
                                ELSE PKG_Elementi.GetCodElemento(COD_GEST_ELEMENTO) 
                         END COD_ELEMENTO,
                         COD_INDICE COD_TIPO_MISURA,
                         NVL(COD_TIPO_FONTE,PKG_Mago.gcRaggrFonteNonAppl) COD_TIPO_FONTE,
                         DATA,
                         VALORE
                    FROM TABLE (CAST(pTabValIndici AS T_KPI_MIS_ARRAY))
                 ) G
           INNER JOIN ELEMENTI                   E ON E.COD_ELEMENTO      = G.COD_ELEMENTO
           INNER JOIN ELEMENTI_DEF               D ON D.COD_ELEMENTO      = G.COD_ELEMENTO
                                                  AND G.DATA        BETWEEN D.DATA_ATTIVAZIONE 
                                                                        AND D.DATA_DISATTIVAZIONE 
           INNER JOIN TIPI_ELEMENTO              T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO 
            LEFT OUTER JOIN TRATTAMENTO_ELEMENTI R ON R.COD_ELEMENTO      = G.COD_ELEMENTO
                                                  AND R.COD_TIPO_MISURA   = G.COD_TIPO_MISURA
                                                  AND R.COD_TIPO_FONTE    = G.COD_TIPO_FONTE
                                                  AND R.COD_TIPO_RETE     = T.COD_TIPO_RETE 
                                                  AND R.COD_TIPO_CLIENTE  = D.COD_TIPO_CLIENTE
                                                  AND R.ORGANIZZAZIONE    = PKG_Mago.gcOrganizzazELE
                                                  AND R.TIPO_AGGREGAZIONE = PKG_Misure.gcModalitaKPI;

--FOR i IN (SELECT * FROM GTTD_MISURE) LOOP
--    PKG_Logs.TraceLog(1||' - '||i.COD_TRATTAMENTO_ELEM||' - '||TO_CHAR(i.DATA,'dd/mm/yyyy hh24:mi:ss')||' - '||i.COD_ELEMENTO||' - '||i.valore);
--END LOOP;
--PKG_Logs.TraceLog(RPAD('-',30,'-'));
    FOR i IN (SELECT COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                     COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM 
                FROM GTTD_MISURE 
               WHERE COD_TRATTAMENTO_ELEM IS NULL
               GROUP BY COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,
                        COD_TIPO_RETE,COD_TIPO_CLIENTE,COD_TIPO_ELEM 
             ) LOOP
--PKG_Logs.TraceLog(i.cod_elemento||' - '||i.COD_TIPO_ELEM||' - mis='||i.COD_TIPO_MISURA||' - fte='||i.COD_TIPO_FONTE||' - Ret='||i.COD_TIPO_RETE||' - => '||i.COD_TIPO_CLIENTE);
        UPDATE GTTD_MISURE 
           SET COD_TRATTAMENTO_ELEM = PKG_Misure.GetTrattamentoElemento(i.COD_ELEMENTO,
                                                                        i.COD_TIPO_ELEM,
                                                                        i.COD_TIPO_MISURA,
                                                                        i.COD_TIPO_FONTE,
                                                                        i.COD_TIPO_RETE,
                                                                        i.COD_TIPO_CLIENTE,
                                                                        PKG_Mago.gcOrganizzazELE,
                                                                        PKG_Misure.gcModalitaKPI)
         WHERE COD_ELEMENTO     = i.COD_ELEMENTO
           AND COD_TIPO_MISURA  = i.COD_TIPO_MISURA
           AND COD_TIPO_FONTE   = i.COD_TIPO_FONTE
           AND COD_TIPO_RETE    = i.COD_TIPO_RETE 
           AND COD_TIPO_CLIENTE = i.COD_TIPO_CLIENTE;
    END LOOP;     

--PKG_Logs.TraceLog(RPAD('-',30,'-'));
--FOR i IN (SELECT * FROM GTTD_MISURE) LOOP
--    PKG_Logs.TraceLog(2||' - '||i.COD_TRATTAMENTO_ELEM||' - '||TO_CHAR(i.DATA,'dd/mm/yyyy hh24:mi:ss')||' - '||i.COD_ELEMENTO||' - '||i.valore);
--END LOOP;
--PKG_Logs.TraceLog(RPAD('-',30,'-'));

    MERGE INTO KPI_INDICI kpi
         USING (SELECT COD_TRATTAMENTO_ELEM,
                       vRisoluzione RISOLUZIONE,  
                       DATA, 
                       VALORE
                  FROM GTTD_MISURE 
                 WHERE COD_TRATTAMENTO_ELEM IS NOT NULL
               ) mis ON (    mis.COD_TRATTAMENTO_ELEM = kpi.COD_TRATTAMENTO_ELEM
                         AND mis.DATA                 = kpi.DATA
                         AND mis.RISOLUZIONE          = kpi.RISOLUZIONE
                        )
     WHEN MATCHED     THEN UPDATE SET kpi.VALORE           = mis.VALORE
                            WHERE kpi.VALORE              <> mis.VALORE
                              AND kpi.COD_TRATTAMENTO_ELEM = mis.COD_TRATTAMENTO_ELEM
                              AND kpi.DATA                 = mis.DATA
                              AND kpi.RISOLUZIONE          = mis.RISOLUZIONE
     WHEN NOT MATCHED THEN INSERT (kpi.COD_TRATTAMENTO_ELEM, kpi.RISOLUZIONE, kpi.DATA, kpi.VALORE)
                           VALUES (mis.COD_TRATTAMENTO_ELEM, mis.RISOLUZIONE, mis.DATA, mis.VALORE);

--PKG_Logs.TraceLog('Misure elaborate '||SQL%ROWCOUNT);

    COMMIT;
    
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.InsertValori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END InsertValori;

-- ----------------------------------------------------------------------------------------------------------
 
 PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE DEFAULT NULL,
                              pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL) AS
 /*-----------------------------------------------------------------------------------------------------------
    Ritorna tutti i valori relativi ad una richiesta
 -----------------------------------------------------------------------------------------------------------*/

    vCodElem ELEMENTI.COD_ELEMENTO%TYPE := NVL(pCodElem,PKG_Elementi.GetCodElemento(pCodGest));
      
 BEGIN

    OPEN pRefCurs FOR 
              SELECT K.COD_ELEMENTO,
                     E.COD_GEST_ELEMENTO,
                     TIPO_INDICE,
                     DATA,
                     VALORE
                FROM (
                      SELECT T.COD_ELEMENTO, T.COD_TIPO_MISURA TIPO_INDICE,DATA,SUM(VALORE) VALORE
                        FROM KPI_RICHIESTA R
                       INNER JOIN KPI_PARAMETRI        P ON P.ID_REQ_KPI = R.ID_REQ_KPI
                                                        AND P.TIPO = gcIndici
                       INNER JOIN (SELECT E.*
                                     FROM KPI_ELEMENTI E
                                    WHERE ID_REQ_KPI = pID_REQ_KPI
                                      AND KEYTYPE = gcPopElem
                                      AND CASE 
                                             WHEN vCodElem IS NULL
                                                 THEN 1
                                                 ELSE CASE WHEN COD_ELEMENTO = vCodElem
                                                         THEN 1
                                                         ELSE 0
                                                      END
                                          END = 1
                                  ) E ON E.ID_REQ_KPI = R.ID_REQ_KPI
                       INNER JOIN TRATTAMENTO_ELEMENTI T ON T.COD_ELEMENTO = E.COD_ELEMENTO
                                                        AND T.COD_TIPO_MISURA = P.PARAMETRO
                                                        AND T.TIPO_AGGREGAZIONE = PKG_Misure.gcModalitaKPI
                       INNER JOIN KPI_INDICI K ON K.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                              AND K.RISOLUZIONE = R.RISOLUZIONE 
                                              AND K.DATA BETWEEN R.PERIODO_INIZIO  
                                                             AND R.PERIODO_FINE
                                              AND TO_CHAR(K.DATA,'hh24:mi') BETWEEN R.ORARIO_INIZIO  
                                                                                AND R.ORARIO_FINE 
                       WHERE ID_REQ_KPI = R.ID_REQ_KPI
                       GROUP BY T.COD_ELEMENTO,T.COD_TIPO_MISURA,DATA
                     ) K
               INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = K.COD_ELEMENTO
               ORDER BY K.COD_ELEMENTO,K.DATA,K.TIPO_INDICE;

    RETURN;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.GetValori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetValori;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PuliziaRichieste AS
 /*-----------------------------------------------------------------------------------------------------------
    Esegue la pulizia delle richieste e degli indici non pi� associati ad alcuna richiesta
 -----------------------------------------------------------------------------------------------------------*/
 
 BEGIN

    -- cancella le richieste scadute 
    -- KPI_ELEMENTI e KPI_PARAMETRI sono cancellati automaticamente per FOREIGN KEY ON DELETE CASCADE
    DELETE KPI_RICHIESTA WHERE DATA_SCADENZA < TRUNC(SYSDATE);
    
    -- cancella le Misure non piu' associate ad alcura richiesta  
    DELETE KPI_INDICI 
     WHERE (COD_TRATTAMENTO_ELEM, RISOLUZIONE, DATA)
           IN (SELECT COD_TRATTAMENTO_ELEM, RISOLUZIONE, DATA
                 FROM KPI_INDICI
               MINUS  
               SELECT K.COD_TRATTAMENTO_ELEM, K.RISOLUZIONE, K.DATA
                 FROM KPI_INDICI K
                INNER JOIN TRATTAMENTO_ELEMENTI T ON T.COD_TRATTAMENTO_ELEM = K.COD_TRATTAMENTO_ELEM
                 LEFT OUTER JOIN KPI_ELEMENTI E ON E.COD_ELEMENTO = T.COD_ELEMENTO
                WHERE E.COD_ELEMENTO IS NOT NULL
              );

    COMMIT;
    
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_KPI.PuliziaRichieste'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END PuliziaRichieste;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_KPI;
/

SHOW ERRORS;


