PROMPT PACKAGE PKG_KPI;
--
-- PKG_KPI  (Package) 
--
--  Dependencies: 
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_STANDARD (Package)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   GTTD_MISURE (Table)
--   GTTD_VALORI_TEMP (Table)
--   KPI_ELEMENTI (Table)
--   KPI_INDICI (Table)
--   KPI_PARAMETRI (Table)
--   KPI_RICHIESTA (Table)
--   PKG_ELEMENTI (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   PLITBLM ()
--   PLITBLM (Synonym)
--   STANDARD (Package)
--   TIPI_ELEMENTO (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   T_COD_GEST_ARRAY (Type)
--   T_KPI_MIS_ARRAY (Type)
--   T_KPI_RICHIESTA_OBJ (Type)
--
CREATE OR REPLACE PACKAGE PKG_KPI AS

/* ***********************************************************************************************************
   NAME:       PKG_KPI
   PURPOSE:    Servizi per la gestione degli Indici di errore (KPI)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.9.a.1    18/08/2014  Moretti C.       Created this package.
   1.9.a.2    23/09/2014  Moretti C.       Procedura GetElementsByReq - Aggiunta colonna NOME a cursore
   1.9.a.3    06/10/2014  Moretti C.       Aggiunta colonna TIPO_NOTIFICA in KPI_RICHIESTA
   1.9.a.4    17/10/2014  Moretti C.       implementata PKG_KPI.GetRichieste per selezione richieste 
                                           anche in base allo stato
              20/10/2014  Moretti C.       Aggiunta colonne in KPI_RICHIESTA
   1.9.a.5    18/11/2014  Campi P.         KPI Aggiunta gestione lista Elementi PAdri selezionati
   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

-- Stati Elaborazione
    gcStatoDaElaborare              KPI_RICHIESTA.ELAB_STATO%TYPE := 0;  
    gcStatoInElaborazione           KPI_RICHIESTA.ELAB_STATO%TYPE := 1;  
    gcStatoElabTerminata            KPI_RICHIESTA.ELAB_STATO%TYPE := 2;  
    gcStatoElabInterrotta           KPI_RICHIESTA.ELAB_STATO%TYPE := 3;  
    gcStatoErroreDiElab             KPI_RICHIESTA.ELAB_STATO%TYPE := -1;  

-- Tipi Parametro
    gcTipFonti                      KPI_PARAMETRI.TIPO%TYPE := 'TIPFTE';
    gcTipReti                       KPI_PARAMETRI.TIPO%TYPE := 'TIPRET';  
    gcTipClienti                    KPI_PARAMETRI.TIPO%TYPE := 'TIPCLI';  
    gcIndici                        KPI_PARAMETRI.TIPO%TYPE := 'TIPKPI';
    gcTipiMisura                    KPI_PARAMETRI.TIPO%TYPE := 'TIPMIS';
    gcTipiMisForecast               KPI_PARAMETRI.TIPO%TYPE := 'TIPMISFOR';
    gcElem                          KPI_PARAMETRI.TIPO%TYPE := 'ELE';
    
 -- Key Elemento 
    
    gcSelElem                       KPI_ELEMENTI.KEYTYPE%TYPE := 'SEL';
    gcPopElem                       KPI_ELEMENTI.KEYTYPE%TYPE := 'POP';
/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertRichiesta   (pNewReq            IN T_KPI_RICHIESTA_OBJ,
                              pElementi          IN T_COD_GEST_ARRAY,
                              pID_REQ_KPI       OUT NUMBER);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRichieste      (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pOPERATORE         IN KPI_RICHIESTA.OPERATORE%TYPE DEFAULT NULL,
                              pStati             IN VARCHAR2 DEFAULT NULL);

 PROCEDURE GetRichiesta      (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsByReq  (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetStatoRichiesta (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pELAB_STATO        IN KPI_RICHIESTA.ELAB_STATO%TYPE,
                              pOPER_STATO        IN KPI_RICHIESTA.OPER_STATO%TYPE DEFAULT NULL, 
                              pDATA_STATO        IN KPI_RICHIESTA.DATA_STATO%TYPE DEFAULT SYSDATE, 
                              pELAB_NOTE         IN KPI_RICHIESTA.ELAB_NOTE%TYPE DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetStatoElemento  (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCOD_ELEMENTO      IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pSTATO             IN KPI_ELEMENTI.STATO%TYPE);

 PROCEDURE SetStatoElementoCG(pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCOD_GEST_ELEMENTO IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pSTATO             IN KPI_ELEMENTI.STATO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InsertValori      (pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pTabValIndici      IN T_KPI_MIS_ARRAY);

-- ----------------------------------------------------------------------------------------------------------

 --PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
 --                             pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE,
 --                             pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
 --                             pDataDa            IN DATE,
 --                             pDataA             IN DATE,
 --                             pTipiIndice        IN VARCHAR2);

 PROCEDURE GetValori         (pRefCurs          OUT PKG_UtlGlb.t_query_cur,
                              pID_REQ_KPI        IN KPI_RICHIESTA.ID_REQ_KPI%TYPE,
                              pCodElem           IN ELEMENTI.COD_ELEMENTO%TYPE DEFAULT NULL,
                              pCodGest           IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------


END PKG_KPI;
/
SHOW ERRORS;


