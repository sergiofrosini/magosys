PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGONAZ   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS=&1;
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT =======================================================================================
PROMPT Directories
PROMPT _______________________________________________________________________________________
@./Directories/MAGO_LOGDIR_&3.sql;

PROMPT =======================================================================================
PROMPT Types
PROMPT _______________________________________________________________________________________
@./Types/T_FILE_INFO_OBJ.sql
@./Types/T_MISMETEO_OBJ.sql
@./Types/T_MISURA_GME_OBJ.sql
@./Types/T_PARAM_EOLIC_OBJ.sql
@./Types/T_PARAM_PREV_OBJ.sql
@./Types/T_MISREQ_OBJ.sql
@./Types/T_FILE_INFO_ARRAY.sql
@./Types/T_MISMETEO_ARRAY.sql
@./Types/T_MISURA_GME_ARRAY.sql
@./Types/T_PARAM_EOLIC_ARRAY.sql
@./Types/T_PARAM_PREV_ARRAY.sql
@./Types/T_MISREQ_ARRAY.sql
@./Types/T_TRTMISLV_OBJ.sql
@./Types/T_TRTMISLVPROFILE_OBJ.sql
@./Types/T_TRTMISMVCLI_OBJ.sql
@./Types/T_TRTMISMVTRD_OBJ.sql

PROMPT =======================================================================================
PROMPT Sequences
PROMPT _______________________________________________________________________________________
@./Sequences/ELEMENTI_PKSEQ.sql
@./Sequences/FILEINFO_SEQUENCE.sql
@./Sequences/LOG_SEQUENCE.sql
@./Sequences/METEO_FILE_XML_SEQ.sql
@./Sequences/METEO_FILE_ZIP_SEQ.sql
@./Sequences/METEO_JOB_SEQ.sql
@./Sequences/SCHEDULED_JOBS_PKSEQ.sql
@./Sequences/TRATTAMENTO_ELEMENTI_PKSEQ.sql

@./Sequences/OFFLINE_MEASURE_REQUEST_IDSEQ.sql
@./Sequences/SESSION_STATISTIC_IDSEQ.sql

PROMPT =======================================================================================
PROMPT Tabelle
PROMPT _______________________________________________________________________________________
@./Tables/TIPI_RETE.sql
@./Tables/TIPI_ELEMENTO.sql
@./Tables/TIPI_GRUPPI_MISURA.sql
@./Tables/TIPI_MISURA.sql
@./Tables/TIPI_MISURA_CONV_ORIG.sql
@./Tables/TIPI_MISURA_CONV_UM.sql
@./Tables/TIPI_CLIENTE.sql
@./Tables/RAGGRUPPAMENTO_FONTI.sql
@./Tables/TIPO_FONTI.sql
@./Tables/TIPO_TECNOLOGIA_SOLARE.sql
@./Tables/ANAGRAFICA_PUNTI.sql
@./Tables/APPLICATION_RUN.sql
@./Tables/ELEMENTI.sql
@./Tables/ELEMENTI_DEF.sql
@./Tables/DEFAULT_CO.sql
@./Tables/FILE_PROCESSED.sql
@./Tables/FORECAST_PARAMETRI.sql
@./Tables/GERARCHIA_AMM.sql
@./Tables/GERARCHIA_GEO.sql
@./Tables/GERARCHIA_IMP_SA.sql
@./Tables/GERARCHIA_IMP_SN.sql
@./Tables/GRUPPI_MISURA.sql
@./Tables/GTTD_CALC_GERARCHIA.sql
@./Tables/GTTD_FORECAST_ELEMENTS.sql
@./Tables/GTTD_IMPORT_GERARCHIA.sql
@./Tables/GTTD_MISURE.sql
@./Tables/GTTD_MOD_ASSETTO_RETE_SA.sql
@./Tables/GTTD_REP_ENERGIA_POTENZA_&2.sql
@./Tables/GTTD_VALORI_REP.sql
@./Tables/GTTD_VALORI_TEMP.sql
@./Tables/LOG_HISTORY.sql
@./Tables/LOG_HISTORY_INFO_&2.sql
@./Tables/MANUTENZIONE.sql
@./Tables/METEO_JOB.sql
@./Tables/METEO_JOB_RUNTIME_CONFIG.sql
@./Tables/METEO_JOB_STATIC_CONFIG.sql
@./Tables/METEO_CITTA.sql
@./Tables/METEO_FILE_LETTO.sql
@./Tables/METEO_FILE_ZIP.sql
@./Tables/METEO_FILE_XML.sql
@./Tables/METEO_PREVISIONE_&2.sql
@./Tables/METEO_REL_ISTAT.sql
@./Tables/TRATTAMENTO_ELEMENTI_&2.sql
@./Tables/MISURE_ACQUISITE_STATICHE.sql
@./Tables/MISURE_AGGREGATE_STATICHE.sql
@./Tables/MISURE_ACQUISITE_&2.sql
@./Tables/MISURE_AGGREGATE_&2.sql
@./Tables/REL_ELEMENTI_AMM.sql
@./Tables/REL_ELEMENTI_ECP_SA.sql
@./Tables/REL_ELEMENTI_ECP_SN.sql
@./Tables/REL_ELEMENTI_ECS_SA.sql
@./Tables/REL_ELEMENTI_ECS_SN.sql
@./Tables/REL_ELEMENTI_GEO.sql
@./Tables/REL_ELEMENTO_TIPMIS.sql
@./Tables/SCHEDULED_JOBS_DEF.sql
@./Tables/SCHEDULED_JOBS_&2.sql
@./Tables/SCHEDULED_TMP_GEN.sql
@./Tables/SCHEDULED_TMP_GME.sql
@./Tables/SCHEDULED_TMP_MET.sql
@./Tables/SERVIZIO_MAGO.sql
@./Tables/STORICO_IMPORT.sql
@./Tables/TMP_CONV_TRATT_ELEM.sql
@./Tables/TMP_MANUTENZIONE.sql

--Aggiunte in versione 1.7
@./Tables/MEASURE_OFFLINE_PARAMETERS.sql
@./Tables/SESSION_STATISTICS_&2.sql

@./Tables/TRT_MIS_LV.sql
@./Tables/TRT_MIS_LV_PROFILE.sql
@./Tables/TRT_MIS_MV_CLIENTI.sql
@./Tables/TRT_MIS_MV_TREND.sql

--Aggiunte in versione 1.8 
@./Tables/ELEMENTI_CFG.sql

@./Tables/VERSION.sql

PROMPT =======================================================================================
PROMPT Packages
PROMPT _______________________________________________________________________________________
@./Packages/PKG_MAGO.sql
@./Packages/PKG_AGGREGAZIONI.sql
--@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_INTEGRST.sql
@./Packages/PKG_LOGS.sql
@./Packages/PKG_MANUTENZIONE.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_REPORTS.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_TRT_MIS.sql

--Packages Aggiunti in versione 1.7
@./Packages/PKG_GENERA_FILE_GEO.sql
@./Packages/PKG_LOCALIZZA_GEO.sql
@./Packages/PKG_STATS.sql

PROMPT =======================================================================================
PROMPT Viste
PROMPT _______________________________________________________________________________________
@./Views/V_ESERCIZI.sql
@./Views/V_CURRENT_VERSION.sql
@./Views/V_ELEMENTI.sql
--@./Views/V_ANAGRAFICA_IMPIANTO.sql
--@./Views/V_GERARCHIA_AMMINISTRATIVA.sql
--@./Views/V_GERARCHIA_GEOGRAFICA.sql
--@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
--@./Views/V_GERARCHIA_IMPIANTO_MT_BT.sql
@./Views/V_LOG_HISTORY_MAGO.sql
--@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
--@./Views/V_MOD_ASSETTO_RETE_SA.sql
@./Views/V_PARAMETRI_APPLICATIVI.sql
@./Views/V_TIPI_MISURA.sql
@./Views/V_SEARCH_ELEMENTS.sql
@./Views/V_PUNTI_GEO.sql

PROMPT =======================================================================================
PROMPT Viste Materializzate
PROMPT _______________________________________________________________________________________
@./Views/V_SCHEDULED_JOBS.sql

PROMPT =======================================================================================
PROMPT Procedure
PROMPT _______________________________________________________________________________________
@./Procedures/GET_METEODISTRIBLIST.sql

PROMPT =======================================================================================
PROMPT Package Bodies
PROMPT _______________________________________________________________________________________
@./PackageBodies/PKG_AGGREGAZIONI.sql
--@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_INTEGRST.sql
@./PackageBodies/PKG_LOGS.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_TRT_MIS.sql

--Package aggiunti in versione 1.7

@./PackageBodies/PKG_GENERA_FILE_GEO.sql
@./PackageBodies/PKG_LOCALIZZA_GEO.sql
@./PackageBodies/PKG_STATS.sql

--PROMPT =======================================================================================
--PROMPT Scheduler Jobs
--PROMPT _______________________________________________________________________________________
--@./SchedulerJobs/ALLINEA_ANAGRAFICA.sql;
--@./SchedulerJobs/MAGO_SCHEDULER.sql;
--@./SchedulerJobs/MAGO_INS_REQ_AGGREG.sql;
--@./SchedulerJobs/MAGO_INS_REQ_AGG_METEO.sql;
--@./SchedulerJobs/MAGO_INS_REQ_AGG_GME.sql;
--@./SchedulerJobs/PULIZIA_GIORNALIERA.sql;
--@./SchedulerJobs/CHECK_SESSION_STATISTICS.sql;

PROMPT =======================================================================================
PROMPT Triggers
PROMPT _______________________________________________________________________________________
@./Triggers/AFTI_APPLICATION_RUN.sql
@./Triggers/BEF_IUR_ELEMENTI.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_AMM.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_ECP_SA.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_ECP_SN.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_ECS_SA.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_ECS_SN.sql
@./Triggers/BEF_IUR_REL_ELEMENTI_GEO.sql
@./Triggers/BEF_IUR_SCHEDULED_JOBS.sql
@./Triggers/BEF_IUR_TRATTAMENTO_ELEMENTI.sql
@./Triggers/BEF_SCHEDULED_TMP_GEN.sql
@./Triggers/BEF_SCHEDULED_TMP_GME.sql
@./Triggers/BEF_SCHEDULED_TMP_MET.sql
@./Triggers/FILEINFO_ID_PK.sql


PROMPT =======================================================================================
PROMPT
PROMPT Inizializazzione Tabelle ...
PROMPT Insert Table METEO_JOB_STATIC_CONFIG;
@./InitTables/METEO_JOB_STATIC_CONFIG.sql
PROMPT Insert Table RAGGRUPPAMENTO_FONTI;
@./InitTables/RAGGRUPPAMENTO_FONTI.sql
PROMPT Insert Table TIPO_FONTI;
@./InitTables/TIPO_FONTI.sql
PROMPT Insert Table TIPI_RETE;
@./InitTables/TIPI_RETE.sql
PROMPT Insert Table TIPI_ELEMENTO;
@./InitTables/TIPI_ELEMENTO.sql
PROMPT Insert Table TIPI_CLIENTE;
@./InitTables/TIPI_CLIENTE.sql
PROMPT Insert Table TIPI_MISURA;
@./InitTables/TIPI_MISURA.sql
PROMPT Insert Table TIPI_MISURA_CONV_ORIG;
@./InitTables/TIPI_MISURA_CONV_ORIG.sql
PROMPT Insert Table TIPI_MISURA_CONV_UM;
@./InitTables/TIPI_MISURA_CONV_UM.sql
PROMPT Insert Table TIPI_GRUPPI_MISURA;
@./InitTables/TIPI_GRUPPI_MISURA.sql
PROMPT Insert Table GRUPPI_MISURA;
@./InitTables/GRUPPI_MISURA.sql
PROMPT Insert Table TIPO_TECNOLOGIA_SOLARE;
@./InitTables/TIPO_TECNOLOGIA_SOLARE.sql
PROMPT Insert Table SCHEDULED_JOBS_DEF;
@./InitTables/SCHEDULED_JOBS_DEF.sql
PROMPT Insert Table METEO_REL_ISTAT;
@./InitTables/METEO_REL_ISTAT.sql
PROMPT Insert Table VERSION;
@./InitTables/VERSION.sql

BEGIN
   INSERT INTO V_PARAMETRI_APPLICATIVI (KEY, VALORE, NOTA) VALUES ('TRACELEVEL', '0', 'Livello di Trace per packages:   2=Solo Errori; 1=Errori+Info; 0=Errori+Info+Dettaglio');
   COMMIT;
EXCEPTION 
   WHEN DUP_VAL_ON_INDEX THEN NULL;
END;
/

MERGE INTO ELEMENTI A USING
 (SELECT 'NAZ' COD_GEST_ELEMENTO,'NAZ' COD_TIPO_ELEMENTO FROM DUAL) B
      ON (A.COD_GEST_ELEMENTO = B.COD_GEST_ELEMENTO)
WHEN NOT MATCHED THEN INSERT (COD_GEST_ELEMENTO,COD_TIPO_ELEMENTO)
                      VALUES (B.COD_GEST_ELEMENTO, B.COD_TIPO_ELEMENTO)
WHEN MATCHED     THEN UPDATE SET A.COD_TIPO_ELEMENTO = B.COD_TIPO_ELEMENTO;

MERGE INTO DEFAULT_CO A USING
 (SELECT 0 COD_ELEMENTO_CO,0 COD_ELEMENTO_ESE,3 TIPO_INST,1 FLAG_PRIMARIO,TRUNC(SYSDATE) START_DATE FROM DUAL) B
      ON (A.COD_ELEMENTO_CO = B.COD_ELEMENTO_CO AND A.COD_ELEMENTO_ESE = B.COD_ELEMENTO_ESE)
WHEN NOT MATCHED THEN INSERT ( COD_ELEMENTO_CO,COD_ELEMENTO_ESE,TIPO_INST,FLAG_PRIMARIO,START_DATE) 
                      VALUES ( B.COD_ELEMENTO_CO,B.COD_ELEMENTO_ESE,B.TIPO_INST,B.FLAG_PRIMARIO,B.START_DATE)
WHEN MATCHED     THEN UPDATE SET A.TIPO_INST = B.TIPO_INST,A.FLAG_PRIMARIO = B.FLAG_PRIMARIO,A.START_DATE = B.START_DATE;

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

