
spool MAGO_1.9_naz.log

PROMPT _______________________________________________________________________________________
PROMPT creazione strutture oracle per MAGONAZ
PROMPT

--parametri in ingresso
--$1 password di sys
--$2 azione (create_MAGONAZ | rebuild_MAGONAZ)

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sysdba as sysdba

COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;
SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;

@./00_tablespace_mago.sql
@./01_user_mago.sql sys_dba MAGONAZ
@./02_grant_mago.sql

connect magonaz/magonaz

@./04_mago_all.sql 'MAGONAZ' &ORATYPE NAZ 

PROMPT
PROMPT
PROMPT fine creazione strutture oracle per MAGONAZ
PROMPT

spool off

