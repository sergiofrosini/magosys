PROMPT METEO_JOB_STATIC_CONFIG

DELETE 
  FROM meteo_job_static_config
 WHERE key IN ('MFM.supplier.ftp.useftp.1','MFM.supplier.ftp.useftp.2'); 

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MFM.supplier.ftp.download.useFtp.1' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useSftp.1' key , 'true' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.upload.useSftp.1' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.port.1' key , '11022' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.1' key , '10.16.11.221' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.1' key , 'sftp_cp-mi' value           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.1' key , '' value
              FROM dual
               UNION ALL
            SELECT 'MFM.supplier.folder.source.path.1' key , '/MAGO/meteo' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useFtp.2' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useSftp.2' key , 'true' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.upload.useSftp.2' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.port.2' key , '11022' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.2' key , '10.16.11.221' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.2' key , 'sftp_cp-mi' value           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.2' key , '' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.folder.source.path.2' key , '/MAGO/meteo' value
              FROM dual
            ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

-- aggiunti in integrazione (mancavano originariamente)
MERGE INTO METEO_JOB_STATIC_CONFIG T
     USING (
            SELECT 'MDS.prediction.timezone' KEY , 'Europe/Bucharest' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.point.type.1' KEY , 'C' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.remote.mantaining.day.1' KEY , '30' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.local.destination.path.1' KEY , './tmp1' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.ftp.timeout.close' KEY , '300' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.ftp.timeout.connection' KEY , '300' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.ftp.timeout.read' KEY , '300' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.suffix.folder.elaboration' KEY , 'EL' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.suffix.folder.extraction' KEY , 'EX' VALUE
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyHost.1' KEY , '' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyPass.1' KEY , '' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyPort.1' KEY , '' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.proxyUser.1' KEY , '' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.useProxy.1' KEY , 'false' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.extension.1' KEY , '.zip' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.match.first.1' KEY , '_01_xml' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.suffix.zip.file.match.second.1' KEY , '_00_xml' VALUE           
              FROM dual
             UNION ALL
            SELECT 'MFM.local.download.retry' key , '3' value
              FROM dual
             UNION ALL
            SELECT 'MFM.local.mantaining.day' key , '30' value
              FROM dual
             UNION ALL
            SELECT 'MFM.local.recovery.days' key , '7' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.conventional.name.1' key , 'Ilmeteo' value
              FROM dual
            ) s
        ON (T.KEY = s.KEY)
      WHEN MATCHED THEN
    UPDATE SET 
              T.VALUE = s.VALUE
      WHEN NOT MATCHED THEN 
    INSERT   
          (KEY,VALUE) 
    VALUES (s.KEY, s.VALUE) 
;

COMMIT;



