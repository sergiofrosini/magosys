
PROMPT _______________________________________________________________________________________
PROMPT CREAZIONE UTENTE MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

set VER OFF SERVEROUTPUT ON

connect sys/&1 as sysdba
WHENEVER SQLERROR EXIT SQL.SQLCODE

declare
	vExists integer := 0;
begin
  select count(*) into vExists from all_users where username = &&3;
  if vExists > 0 then
    dbms_output.put_line ('Eliminazione user &&3');
    execute immediate 'drop user mago cascade';
  end if;
end;
/

Prompt User &&3;
--
-- MAGO  (User) 
--
CREATE USER &&3
  IDENTIFIED BY &&3
  DEFAULT TABLESPACE MAGO_DATA
  QUOTA UNLIMITED ON MAGO_DATA
  TEMPORARY TABLESPACE TEMP
  PROFILE DEFAULT
  ACCOUNT UNLOCK;

  -- 2 Roles for MAGO 
  GRANT RESOURCE TO &&3;
  GRANT JAVAUSERPRIV TO &&3;
  ALTER USER &&3 DEFAULT ROLE ALL;

  -- 19 System Privileges for MAGO 
  GRANT CREATE JOB TO &&3;
  GRANT ALTER SESSION TO &&3;
  GRANT CREATE DIMENSION TO &&3;
  GRANT DROP ANY DIRECTORY TO &&3;
  GRANT CREATE ANY DIRECTORY TO &&3;
  GRANT CREATE DATABASE LINK TO &&3;
  GRANT DROP PUBLIC SYNONYM TO &&3;
  GRANT CREATE LIBRARY TO &&3;
  GRANT QUERY REWRITE TO &&3;
  GRANT CREATE INDEXTYPE TO &&3;
  GRANT CREATE OPERATOR TO &&3;
  GRANT CREATE VIEW TO &&3;
  GRANT CREATE SYNONYM TO &&3;
  GRANT CREATE SESSION TO &&3;
  GRANT CREATE MATERIALIZED VIEW TO &&3;
  GRANT CREATE PUBLIC SYNONYM TO &&3;
  GRANT CREATE EXTERNAL JOB TO &&3;
  GRANT CREATE ANY TABLE TO &&3;
  GRANT UNLIMITED TABLESPACE TO &&3;

  --GRANT READ, WRITE ON DIRECTORY SYS.MAGO_LOGDIR TO &&3 WITH GRANT OPTION;
  GRANT EXECUTE ON SYS.DBMS_LOCK TO &&3;
  GRANT EXECUTE ON SYS.DBMS_SCHEDULER TO &&3;
  GRANT EXECUTE ON SYS.DBMS_SNAPSHOT TO &&3;
  
  GRANT SELECT ON SAR_ADMIN.CFT TO &&3;
  GRANT SELECT ON SAR_ADMIN.COMUNI TO &&3;
  GRANT SELECT ON SAR_ADMIN.ESERCIZI TO &&3;
  GRANT SELECT ON SAR_ADMIN.MACRO_AREE TO &&3;
  GRANT SELECT ON SAR_ADMIN.PROVINCE TO &&3;
  GRANT SELECT ON SAR_ADMIN.REGIONI TO &&3;
  GRANT SELECT ON SAR_ADMIN.REL_CFT_COMUNI TO &&3;
  GRANT SELECT ON SAR_ADMIN.UNITA_TERRITORIALI TO &&3;
  GRANT SELECT ON SAR_ADMIN.ZONE TO &&3;
  GRANT SELECT,INSERT,UPDATE,DELETE ON sar_admin.retesar_esercizi TO &&3;
  GRANT SELECT,INSERT,UPDATE,DELETE ON sar_admin.esercizi_abilitati TO &&3;
  
  --  GRANT SELECT ON STMAUI.AVVOLGIMENTI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.CAR_TRON_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.CAR_TRON_2_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.CLIENTI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.COLL_ELE_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.COLL_RAMO_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.COLL_TRASF_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.CONDENSATORI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.CONN_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.COPPIA_AVVO_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.GENERATORI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.HEADER_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.LINEE_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.MONTANTI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.NODI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.PROTEZIONI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.RAMI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.RTU_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.SBARRE_SEZ_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.SBARRE_SIS_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.TENS_NOM_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.TRASFORMATORI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.TRASF_PROD_BT_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.TRONCHI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.UPDATE_AUI_TLC TO MAGO;
--  GRANT SELECT ON STMAUI.UTILIZZO_TLC TO MAGO;

show error

