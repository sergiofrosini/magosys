DB
- MAGO 1.9  -->  /DRDossier/DR-database/dev/magosys/1.9a
		 (fino all�ultimo Update leggere /src/IntegrationNotes.txt, richiede se non istallate anche la versione MAGO 1.2a , 1.3a , 1.7a leggere /src/IntegrationNotes.txt ,1.8a /src/IntegrationNotes.txt)

- ADMIN 1.2.1 --> /DRDossier/DR-database/dev/sarsys/1.2/update/rev-001 (se non � gi� stata installato MAGO 1.8)
- ADMIN 1.2.2 --> /DRDossier/DR-database/dev/sarsys/1.2/update/rev-002
		( NOTA: per MAGO 1.9 � richiesto solo questo update)

		Contenuti:
		17/10/2014 1.0.9 rev 009 SAR_ADMIN 
  			- creata vista v_operatori_mago
  			- Inseriti parametri generali MAGO
  			- Aggiornamento Amministrazione Richiesto da MAGO 1.9a.3


- CORELE (info nella src/IntegrationNotes.txt di ogni versione)
		Dipende della versione di ST (che alimenta il flusso dati verso CORELE)
	�	2012.4       la versione di CORELE � 1.3a rev-003  (le versioni MAGO possono essere 1.8a o 1.9a) (leggere file Istruzioni.txt)
	�	2012.3x      la versione di CORELE � 1.3a rev-001  (la versione MAGO deve essere 1.8a)
	�	altrimenti    la versione di CORELE � 1.3a base (la versione MAGO deve essere 1.1h)

SCRIPT

- Abilitare Script Edoardo per flusso GME-TR su MAGO BE (/from_GME/AT)
- Abilitare Script Edoardo per flusso CCP/CCQ su MAGO BE (/STM_XXXX/GME/ )

PROCESSI

- STWebDatalaoder 1.9.a
	Defininire alias pc_dms

- MDS (rev. 3819)

- LFS (rev. 3820)

- MAGO BE 1.2.0 rev 1

JBOSS
- ricordarsi file mago-hibernate.cfg.xml aggiornato
