SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WRAPPED;
SET TIMING OFF;

--conn mago/mago

ALTER SESSION SET NLS_DATE_FORMAT='dd/mm/yyyy hh24.mi.ss';
ALTER SESSION SET NLS_NUMERIC_CHARACTERS = '.,';

CLEAR COLUMNS

COLUMN REPTIP     NEW_VALUE REPTIP
COLUMN REPTIT     NEW_VALUE REPTIT
COLUMN REPEXT     NEW_VALUE REPEXT
COLUMN SITENAME   NEW_VALUE SITENAME
COLUMN DATAREP    NEW_VALUE DATAREP
COLUMN ORAREP     NEW_VALUE ORAREP

SELECT TO_CHAR(SYSDATE,'DD/MM/YYYY')         DATAREP,
       TO_CHAR(SYSDATE,'HH24:MI')            ORAREP,
       COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO SITENAME,
       '_OccupazioneDBperMisure'             REPTIT,
       '.txt'                                REPEXT
  FROM DEFAULT_CO
 INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
 LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
 WHERE flag_primario = 1
   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

SET lines 124
SET pages 10000


COLUMN Tot_MB     NEW_VALUE Tot_MB     NOPRINT
COLUMN Tot_GB     NEW_VALUE Tot_GB     NOPRINT
COLUMN Tot_Misure NEW_VALUE Tot_Misure NOPRINT
COLUMN DATAREP    NEW_VALUE DATAREP    NOPRINT
COLUMN ORAREP     NEW_VALUE ORAREP     NOPRINT

CLEAR BREAKS
CLEAR COMPUTES

BREAK ON TABELLA SKIP 1
COMPUTE SUM LABEL "Tot. Tabella" OF MISURE ON TABELLA;
COMPUTE SUM LABEL "Tot. Tabella" OF MB     ON TABELLA;

TTITLE ================================================================================================================= SKIP 1 -
       SITENAME " - Occupazione DB per misure al " DATAREP " ore " ORAREP " - dati raggruppati per tabella" SKIP 1 -
       ================================================================================================================= SKIP 1 -
       "  Totale Mb     :  "  Tot_MB  "  ( " Tot_GB " Gb )" SKIP 1 -
       "  Totale Misure :  "  Tot_Misure                    SKIP 1 -
       ================================================================================================================= SKIP 2;

SELECT 'ByTable' REPTIP FROM DUAL;
SPOOL '&SITENAME&REPTIT&REPTIP&REPEXT'
@occupazioneDBperMisureByTable
SPOOL OFF

BREAK ON MESE SKIP 1
COMPUTE SUM LABEL "Tot. Mensile" OF MISURE ON MESE;
COMPUTE SUM LABEL "Tot. Mensile" OF MB     ON MESE;

TTITLE ================================================================================================================= SKIP 1 -
       SITENAME " - Occupazione DB per misure al " DATAREP " ore " ORAREP " - dati raggruppati per mese" SKIP 1 -
       ================================================================================================================= SKIP 1 -
       "  Totale Mb     :  "  Tot_MB  "  ( " Tot_GB " Gb )" SKIP 1 -
       "  Totale Misure :  "  Tot_Misure                    SKIP 1 -
       ================================================================================================================= SKIP 2;

SELECT 'ByMonth' REPTIP FROM DUAL;
SPOOL '&SITENAME&REPTIT&REPTIP&REPEXT'
@occupazioneDBperMisureByMonth
SPOOL OFF

CLEAR BREAKS
CLEAR COMPUTES

BREAK ON REPORT
COMPUTE SUM LABEL "Tot. Tabella" OF MISURE ON REPORT;
COMPUTE SUM LABEL "Tot. Tabella" OF MB     ON REPORT;

TTITLE ================================================================================================================= SKIP 1 -
       SITENAME " - Occupazione DB per misure al " DATAREP " ore " ORAREP " - totali mensili" SKIP 1 -
       ================================================================================================================= SKIP 2;

SELECT 'Totali' REPTIP FROM DUAL;
SPOOL '&SITENAME&REPTIT&REPTIP&REPEXT'
@occupazioneDBperMisureTotali
SPOOL OFF

EXIT;

