spool change_local_recovery_mds.log

conn mago/mago@arcdb2

select KEY,VALUE
from METEO_JOB_STATIC_CONFIG
WHERE KEY = 'MFM.local.recovery.days';

update METEO_JOB_STATIC_CONFIG set VALUE = 3
where KEY = 'MFM.local.recovery.days';

select KEY,VALUE
from METEO_JOB_STATIC_CONFIG
WHERE KEY = 'MFM.local.recovery.days';

commit;

spool off;

exit;