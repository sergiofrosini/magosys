SELECT INITCAP(TO_CHAR(MESE,'month yyyy', 'NLS_DATE_LANGUAGE=Italian'))  MESE,
       MB,
       PROGRESSIVO "PROGR.MB",
       MISURE,NOTA
  FROM (SELECT MESE,
               MB,
               SUM(MB) OVER (PARTITION BY 1 ORDER BY MESE ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) PROGRESSIVO,
               MISURE,
               NOTA
          FROM (SELECT MESE, 
                       ROUND(((BYTES/1024)/1024),2) MB, 
                       NUM_ROWS MISURE,
                       CASE 
                        WHEN LAST_DAY(MESE) <= LAST_ANALYZED THEN NULL
                                                             ELSE 'Misure stimate al '||TO_CHAR(LAST_ANALYZED,'dd/mm/yyyy hh24:mi')
                       END NOTA 
                  FROM (SELECT CASE US.PARTITION_NAME
                                 WHEN 'MIS_NEXT_ALL' THEN TO_DATE('01013000','ddmmyyyy')
                                 ELSE TO_DATE(US.PARTITION_NAME,'MONYYYY', 'NLS_DATE_LANGUAGE=Italian')
                               END MESE, 
                               SUM(UP.NUM_ROWS) NUM_ROWS,
                               SUM(BYTES) BYTES,
                               MIN(UP.LAST_ANALYZED) LAST_ANALYZED
                          FROM USER_SEGMENTS US
                         INNER JOIN USER_INDEXES UI ON UI.INDEX_NAME=US.SEGMENT_NAME
                         LEFT OUTER JOIN USER_TAB_PARTITIONS UP ON UP.TABLE_NAME=UI.TABLE_NAME AND UP.PARTITION_NAME=US.PARTITION_NAME
                         WHERE UI.TABLE_NAME IN ('MISURE_ACQUISITE','MISURE_AGGREGATE') 
                           AND UP.NUM_ROWS > 0
                         GROUP BY US.PARTITION_NAME
                       ) A
               )
       ) A
 ORDER BY A.MESE;
