SET SERVEROUTPUT ON SIZE UNLIMITED format WRAPPED
SET LINES 400
DECLARE
    vNum INTEGER := 0;
    vSw  INTEGER;
    vPrd VARCHAR2(123);
    vGen VARCHAR2(50);
BEGIN
    DBMS_OUTPUT.PUT_LINE(LPAD('Num',5,' ')||' '||
                         RPAD('Produttore',28,' ')||' '||
                         RPAD('Cab.Prim.',11,' ')||' '||
                         RPAD('Comune',25,' ')||' '||
                         RPAD('In AUI',6,' ')||' '||
                         RPAD('In MAGO',7,' ')||' '||
                         RPAD('TIPO',6,' ')||' '||
                         LPAD('PI_AUI',8,' ')||' '||
                         LPAD('PI_MAGO',10,' ')||'  '||
                         RPAD('Generatore',14,' ')||' '||
                         RPAD('Fonte',5,' ')||' '||
                         LPAD('PI_GEN',10,' ')||' '||
                         ' ');
    DBMS_OUTPUT.PUT_LINE(RPAD('-',5,'-')||' '||
                         RPAD('-',28,'-')||' '||
                         RPAD('-',11,'-')||' '||
                         RPAD('-',25,'-')||' '||
                         RPAD('-',6,'-')||' '||
                         RPAD('-',7,'-')||' '||
                         RPAD('-',6,'-')||' '||
                         RPAD('-',8,'-')||' '||
                         RPAD('-',10,'-')||'  '||
                         RPAD('-',14,'-')||' '||
                         RPAD('-',5,'-')||' '||
                         RPAD('-',10,'-')||' '||
                         ' ');
    FOR I IN (WITH CLI_MAGO AS (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_CLIENTE,POTENZA_INSTALLATA
                                      ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'COM', SYSDATE, 2, 1) COMUNE
                                      ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'CPR', SYSDATE, 1, 1) CAB_PRI 
                                  FROM V_ELEMENTI 
                                 WHERE COD_TIPO_ELEMENTO = 'CMT' 
                                   AND COD_TIPO_CLIENTE IN ('A','B')
                               )
                   ,CLI_AUI AS (SELECT A.GEST_PROD,
                                       A.RAGIO_SOC   NOME_PROD,
                                        A.TIPO_FORN   TIPO_FORN,
                                       A.POT_GRUPPI
                                  FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD,
                                               C.RAGIO_SOC,C.TIPO_FORN,C.POT_GRUPPI
                                          FROM CLIENTI_TLC@PKG1_STMAUI.IT C 
                                         WHERE C.TIPO_FORN IN ('AP','PP','PD')
                                           AND C.TRATTAMENTO = 0 AND C.STATO = 'E'
                                       ) A 
                                 GROUP BY A.GEST_PROD,A.RAGIO_SOC,A.TIPO_FORN,A.POT_GRUPPI 
                               )
                   ,CLI_PI  AS (SELECT COD_GEST_ELEMENTO,M.VALORE PI_MISURA 
                                  FROM CLI_MAGO E 
                                  LEFT OUTER JOIN TRATTAMENTO_ELEMENTI T 
                                               ON T.COD_ELEMENTO = E.COD_ELEMENTO   
                                              AND T.COD_TIPO_MISURA = 'PI'
                                              AND TIPO_AGGREGAZIONE = 1
                                  LEFT OUTER JOIN MISURE_AGGREGATE_STATICHE M 
                                               ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM 
                                              AND SYSDATE BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE 
                               )
              SELECT COD_GEST_ELEMENTO,
                     NVL(NOME_PROD,' ') NOME_PROD,
                     NVL(CAB_PRI,' ') CAB_PRI,
                     NVL(COMUNE,' ') COMUNE,
                     NVL(IN_AUI,' ') IN_AUI,
                     NVL(IN_MAGO,' ') IN_MAGO,
                     NVL(TP_MAGO,' ')||' ('||NVL(TP_AUI,'  ')||')' TIPO,
                     NVL(TO_CHAR(PI_AUI),' ') PI_AUI,
                     NVL(TO_CHAR(PI_MAGO_ELE),' ') PI_MAGO_ELE
                FROM (SELECT A.*,PI_MISURA PI_MAGO_MIS
                        FROM (SELECT NVL(M.COD_GEST_ELEMENTO,A.GEST_PROD) COD_GEST_ELEMENTO,
                                     M.NOME_ELEMENTO NOME_PROD,
                                     PKG_ELEMENTI.GETGESTELEMENTO(CAB_PRI) CAB_PRI,
                                     C.NOME_ELEMENTO COMUNE,
                                     CASE WHEN A.TIPO_FORN IS NULL THEN NULL ELSE '  si  ' END IN_AUI,
                                     CASE WHEN M.COD_TIPO_CLIENTE IS NULL THEN NULL ELSE '  si   ' END IN_MAGO,
                                     A.TIPO_FORN TP_AUI,
                                     M.COD_TIPO_CLIENTE TP_MAGO,
                                     A.POT_GRUPPI PI_AUI,
                                     M.POTENZA_INSTALLATA PI_MAGO_ELE
                                FROM CLI_AUI A
                                FULL OUTER JOIN CLI_MAGO   M ON M.COD_GEST_ELEMENTO = A.GEST_PROD
                                LEFT OUTER JOIN V_ELEMENTI C ON C.COD_ELEMENTO      = COMUNE
                             ) A
                        LEFT OUTER JOIN CLI_PI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO 
                       ORDER BY CAB_PRI NULLS LAST, IN_MAGO NULLS LAST,IN_AUI NULLS LAST,A.COD_GEST_ELEMENTO
                     ) A
--WHERE ROWNUM < 11                     
             ) LOOP
        vNum := vNum + 1;
        vPrd := LPAD(TO_CHAR(vNum),5,' ')||' '||
                RPAD(i.COD_GEST_ELEMENTO||' '||i.NOME_PROD,28,' ')||' '||
                RPAD(NVL(i.CAB_PRI,' '),11,' ')||' '||
                RPAD(NVL(i.COMUNE,' '),25,' ')||' '||
                RPAD(i.IN_AUI,6,' ')||' '||
                RPAD(i.IN_MAGO,7,' ')||' '||
                RPAD(i.TIPO,6,' ')||' '||
                LPAD(i.PI_AUI,8,' ')||' '||
                LPAD(i.PI_MAGO_ELE,10,' ')||' ';
        vSw := 0;
        FOR j IN (SELECT COD_ORG_NODO,SER_NODO,NUM_NODO,TIPO_ELEMENTO,ID_GENERATORE,
                         CASE
                            WHEN TIPO_IMP IS NULL
                                THEN 'null '
                            WHEN TIPO_IMP = ' '
                                THEN 'blank'
                            WHEN COD_RAGGR_FONTE = TIPO_IMP
                                THEN '  '||COD_RAGGR_FONTE||'  '
                            ELSE COD_RAGGR_FONTE||' ('||TIPO_IMP||')'
                         END TIPO_IMP,
                         P_APP_NOM,N_GEN_PAR,F_P_NOM
                    FROM GENERATORI_TLC@PKG1_STMAUI.IT 
                    LEFT OUTER JOIN TIPO_FONTI ON COD_TIPO_FONTE = TIPO_IMP
                   WHERE COD_ORG_NODO = SUBSTR(i.COD_GEST_ELEMENTO,1,4)
                     AND SER_NODO     = SUBSTR(i.COD_GEST_ELEMENTO,5,1)
                     AND NUM_NODO     = SUBSTR(i.COD_GEST_ELEMENTO,6,6)
                     AND ID_CL        = SUBSTR(i.COD_GEST_ELEMENTO,13,2)
                     AND TRATTAMENTO  = 0
                     AND STATO        = 'E'
                 ) LOOP
            vGen := j.COD_ORG_NODO||j.SER_NODO||j.NUM_NODO||j.TIPO_ELEMENTO||j.ID_GENERATORE||' '||
                    j.TIPO_IMP||' '||
                    LPAD(TO_CHAR((j.P_APP_NOM * NVL(j.N_GEN_PAR,1) * NVL(j.F_P_NOM,1))),10,' ');
            IF vSw = 0 THEN
                DBMS_OUTPUT.PUT_LINE(vPrd||' '||vGen);
                vSw := 1;
            ELSE
                DBMS_OUTPUT.PUT_LINE(RPAD(' ',LENGTH(vPrd),' ')||' '||vGen);
            END IF;
        END LOOP;
        IF vSw = 0 THEN
            DBMS_OUTPUT.PUT_LINE(vPrd||' '||vGen);
        END IF;
    END LOOP;
END;
/
       
