
conn mago/mago

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WRAPPED;
SET LINES 90;
SET TIMING ON;

DECLARE
    DT_START VARCHAR2(100);
    DT_END   VARCHAR2(100);
    DT       DATE;
    SP       VARCHAR2(100);
    TXT      VARCHAR2(100);
BEGIN
    DT_START := '&1';
    DT_END   := '&2';
    TXT       := 'Aggregazioni-';
    BEGIN
        DT := TO_DATE(DT_START,'dd/mm/yyyy');
    EXCEPTION
        WHEN OTHERS THEN 
            RAISE_APPLICATION_ERROR(-20000,'data START ('||DT_START||') errata'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    END;
    BEGIN
        IF DT_START IS NULL THEN
            DT_START := TO_CHAR(SYSDATE-1,'dd/mm/yyyy');
        END IF;
        IF DT_END IS NULL THEN
            DT_END := DT_START;
            TXT := TXT||DT_START||'.txt';
        ELSE
            DT := TO_DATE(DT_END,'dd/mm/yyyy');
            TXT := TXT||DT_START||'-'||DT_END||'.txt';
        END IF;
    EXCEPTION
        WHEN OTHERS THEN 
            RAISE_APPLICATION_ERROR(-20000,'data END ('||DT_END||') errata'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    END;
    DELETE GTTD_VALORI_TEMP WHERE TIP = 'SCRIPT';
    INSERT INTO GTTD_VALORI_TEMP (TIP, ALF1, ALF2, ALF3, ALF4, ALF5) 
        SELECT 'SCRIPT',COD_GEST_ELEMENTO,NOME_ELEMENTO,TXT,DT_START,DT_END
          FROM DEFAULT_CO
         INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
         LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
         WHERE FLAG_PRIMARIO = 1
           AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
END;
/

COL SPOOLNAME NEW_VALUE SPOOLNAME

COL SPOOLNAME FOR A80
SELECT ALF1||'-'||ALF2||'-'||ALF3 SPOOLNAME FROM GTTD_VALORI_TEMP WHERE TIP = 'SCRIPT';  

SPOOL '&SPOOLNAME'

SET LINE 300
COL TIPO FOR A65

WITH PERIODO AS (SELECT TO_DATE(alf4,'dd/mm/yyyy') DT_START, TO_DATE(alf5,'dd/mm/yyyy') DT_END FROM GTTD_VALORI_TEMP WHERE TIP = 'SCRIPT')
    ,STORICO AS (SELECT DATA,DATA_RIF,ORG,STATO,MISURE,ELAPSED,
                        (TO_NUMBER(SUBSTR(ELAPSED,1,2))*3600+TO_NUMBER(SUBSTR(ELAPSED,4,2))*60+TO_NUMBER(SUBSTR(ELAPSED,7,2)))*1000+TO_NUMBER(SUBSTR(ELAPSED,10)) MS
                   FROM (SELECT TRUNC(LOGDATE) DATA,
                                TO_DATE(SUBSTR(ELEMENTO,22,19),'dd/mm/yyyy hh24:mi:ss') DATA_RIF,
                                SUBSTR(ELEMENTO,54,1) ORG,
                                SUBSTR(ELEMENTO,60,1) STATO,
                                TRIM(SUBSTR(ELEMENTO,66,INSTR(ELEMENTO,'(')-1-66)) MISURE,
                                SUBSTR(ELEMENTO,INSTR(ELEMENTO,'(')+1,12) ELAPSED
                           FROM V_LOG_HISTORY_MAGO,PERIODO
                          WHERE TRUNC(LOGDATE-1) BETWEEN DT_START AND DT_END
                            AND CLASSE = 'Utility.SCH'
                       ) A
             )
SELECT CASE 
         WHEN SUBSTR(TIPO,1,1) = 1 
                THEN CASE
                       WHEN DT_START <> NVL(DT_END,DT_START) 
                            THEN 'totale del periodo '||SUBSTR(TIPO,3)||' '||TO_CHAR(DT_START,'dd/mm/yyyy')||' - '||TO_CHAR(DT_END,'dd/mm/yyyy')
                            ELSE 'totale del giorno '||SUBSTR(TIPO,3)||' '||TO_CHAR(DT_START,'dd/mm/yyyy')
                     END
                ELSE CASE
                       WHEN DT_START <> NVL(DT_END,DT_START) 
                            THEN 'totale del periodo'
                            ELSE 'totale del giorno'
                     END
       END ||  SUBSTR(TIPO,3)||' '||DECODE(TRIM(ORG_STATO),'/',NULL,ORG_STATO)||' '||MISURE TIPO,
       LPAD(TRUNC(TRUNC(TOT_SEC)/60),2,'0')||':'||LPAD(TRUNC(TOT_SEC - (TRUNC(TRUNC(TOT_SEC)/60)*60)),2,'0') AS TEMPO_TOTALE ,NUM,AVG_SEC
  FROM (SELECT TIPO,ORG,STATO,DT_START,DT_END,
               CASE ORG WHEN '1' THEN 'Elettrica' WHEN '2' THEN 'Amministrativa' WHEN '3' THEN 'Istat' ELSE ORG END 
               || ' / '||
               CASE STATO WHEN '1' THEN 'Normale' WHEN '2' THEN 'Attuale' ELSE STATO END 
               AS ORG_STATO ,
               MISURE,NUM,ROUND(TOT_MS/1000,3) TOT_SEC,ROUND(AVG_MS/1000,3) AVG_SEC
          FROM (SELECT '1. ' AS TIPO, NULL ORG, NULL STATO, NULL MISURE,SUM(MS) TOT_MS,AVG(MS) AVG_MS,COUNT(*) NUM FROM STORICO
                UNION ALL
                SELECT '2. per gerarchia' , ORG, STATO, NULL MISURE, SUM(MS) TOT_MS,AVG(MS) AVG_MS,COUNT(*) NUM FROM STORICO GROUP BY ORG,STATO
                UNION ALL
                SELECT '3. per tipi misura' , NULL ORG, NULL STATO, MISURE, SUM(MS) TOT_MS,AVG(MS) AVG_MS,COUNT(*) NUM FROM STORICO GROUP BY MISURE
               )
           JOIN PERIODO ON 1=1
       )
 ORDER BY TIPO,ORG_STATO;
 


SPOOL OFF

EXIT;
