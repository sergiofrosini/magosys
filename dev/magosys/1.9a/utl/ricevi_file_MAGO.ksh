#!/usr/bin/ksh
#
#       ricevi_file_MAGO.ksh        (Siemens Italia SpA)
#
#       Data:           Oct. 2011
#       Autore          G. Migliaccio, E. Castrignano
#       Aggiornamenti:
#
#       Modifiche da implementare :
#               - N.A.
#
#       Descrizione:
#            Script che scarica dal un server di scambio
#            i files MIS e GME di TMESX2 per
#            l'utilizzo dell'applicativo MAGO
#       Modalita' di attivazione:
#            Da crontab
#
#----------------------------------------------------------------------------------

##
# Definizione delle variabili di ambiente ORACLE
##
export ORACLE_HOME="/orakit/app/oracle/product/10.2.0/db_1"
export ORACLE_SID="ARCDB1"

##
# Definizione delle directory di lavoro
##
FILDIR="/usr/NEW/tmesx_2/filedati/from-AUI"
MAGODIR="/usr/NEW/tmesx_2/to_MAGO"
MAGODIRAUI="/MAGO"
GMEDIR="/usr/NEW/tmesx_2/filedati/to-GME"
OUTDIR="/usr/NEW/tmesx_2/filedati"
TMPDIR="/usr/NEW/tmesx_2/tmp"
REJDIR="/usr/NEW/tmesx_2/rejected"
CORDIR="/usr/NEW/tmesx_2/corrupted"
BINDIR="/usr/NEW/tmesx_2/bin"
LOGDIR="/usr/NEW/tmesx_2/log"
MISBCKDIR="/usr/NEW/tmesx_2/backup"
GMEBCKDIR="/usr/NEW/tmesx_2/backup_gme"
CESIMISDIR="/usr/NEW/tmesx_2/fromDF70"
CESIMISDIRTMP="/usr/NEW/tmesx_2/utl/tmpmis"
CESIGMEDIR="/usr/NEW/tmesx_2/to_MAGO"
CESIMAGODIR="/usr/NEW/magosys/fromGME/AT"
##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"
TOODIR="/bin/mtools"

##
# Definizione dei comandi di sistema utilizzati nello script
##

AUI_HOST="pc_aui"
AUI_PWD="chktele28"
AUI_USR="TELECHK"
MAGO_HOST="pkg_ARCDB2"
MAGO_PWD="Magosys"
MAGO_USR="magosys"
AUTH="tmesx_2/tmesx_2"
CAT="${USRDIR}/cat"
CP="${USRDIR}/cp"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
GREP="${USRDIR}/grep"
KSH="${USRDIR}/ksh"
LDR="${ORACLE_HOME}/bin/sqlldr"
LS1="${USRDIR}/ls -1"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
WC="${USRDIR2}/wc"

##
# Definizione delle variabili di date-time
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}
DATA_NEW=${ANNO}${MESE}${GIORNO}_${ORA}


##
# Definizione delle variabili di controllo
##
CHECKFILE="0"

##
# Definizione nomi files
##
MAGOLOG="mago_download__${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp.log"
FTPMAGOCHECKSEMAFORO="mago_chek_semaforo.log"
SEMAFORO_MIS="semaforo_mis.txt"
SEMAFORO_GME="semaforo_gme.txt"


# MAGO
##
# Check file semaforo
##
function CheckSemaforo
{
     ${ECHO} "Inizio controllo semaforo per ricezione file(s)" >> ${LOGDIR}/${MAGOLOG}

     CHECKFILE="0"

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile! (DOWNLOAD-MAGO)" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile! (DOWNLOAD-MAGO)" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
ls $semaforo
bye
eofftp

          ${GREP} $semaforo ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} > /dev/null
          retcode=$(${ECHO} $?)
          if [[ "${retcode}" = "0" ]]; then
               ${CAT} ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "Semaforo verde per ricezione!" >> ${LOGDIR}/${MAGOLOG}
          else
               ${CAT} ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "Semaforo rosso per ricezione!" >> ${LOGDIR}/${MAGOLOG}
               CHECKFILE="1"
          fi

          ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
     fi

     ${ECHO} "Fine controllo semaforo per ricezione file(s)" >> ${LOGDIR}/${MAGOLOG}

     return 0
}

# MAGO
##
# Processo di attivazione FTP per ricezione file MIS di tmesx
##
function ricevi_files_MIS
{
     ${ECHO} "Inizio fase ricezione file(s) MIS" >> ${LOGDIR}/${MAGOLOG}

     cd ${CESIMISDIRTMP}

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile! (DOWNLOAD MIS FOR MAGO)" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile! (DOWNLOAD MIS FOR MAGO IN TEMPORARY DIRECTORY)" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
mget STM*MIS.gz
bye
eofftp

          ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
          retcode=$(${ECHO} $?)
          if [[ "${retcode}" = "1" ]]; then
               ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "FTP file(s) MIS FALLITA" >> ${LOGDIR}/${MAGOLOG}
	       return 1
          else
               ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "FTP file(s) MIS COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

               ${ECHO} "Cancellazione file(s) MIS da server di scambio.." >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
mdelete STM*MIS.gz
delete $semaforo
bye
eofftp

               # Il passaggio da una cartella temporanea dove scaricare i files prima di posizionarli in quella di lavoro
               # si e' reso necessario in quanto durante il processo di ftp alcuni file cominciassero ad essere
               # elaborati prima dell'avvenuto trasferimento; conseguentemente il file veniva scartato come corrotto
               # perche' solitamente incompleto.

               ${ECHO} "Spostamento dei file(s) MIS dalla cartella temporanea ${CESIMISDIRTMP} a quella di lavoro ${CESIMISDIR}.">> ${LOGDIR}/${MAGOLOG}
               ${MV} STM*MIS.gz ${CESIMISDIR}
          fi

          ${ECHO} "Fine fase ricezione file(s) MIS" >> ${LOGDIR}/${MAGOLOG}
     fi

     return 0
}

# MAGO
##
# Processo di attivazione FTP per ricezione file GME
##
function ricevi_files_GME
{
     ${ECHO} "Inizio fase ricezione file(s) GME" >> ${LOGDIR}/${MAGOLOG}

     cd ${CESIGMEDIR}

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile! (DOWNLOAD GME FOR MAGO)" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile! (DOWNLOAD GME FOR MAGO)" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
mget ${ESECOMP}_GMETR_*.zip
bye
eofftp

          ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
          retcode=$(${ECHO} $?)
          if [[ "${retcode}" = "1" ]]; then
               ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "FTP file(s) GME FALLITA" >> ${LOGDIR}/${MAGOLOG}
	       return 1
          else
               ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
               ${ECHO} "FTP file(s) GME COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

               ${ECHO} "Cancellazione file(s) GME dal server di scambio.." >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
mdelete ${ESECOMP}_GMETR_*.zip
delete $semaforo
bye
eofftp

               ${ECHO} "Trasmissione file(s) GME al server MAGO" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $MAGO_HOST
user $MAGO_USR $MAGO_PWD
bin
prompt
cd $CESIMAGODIR
mput ${ESECOMP}_GMETR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} $?)
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
                    ${ECHO} "FTP file(s) GME VERSO MAGO FALLITA" >> ${LOGDIR}/${MAGOLOG}
		    return 1
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG}
                    ${ECHO} "FTP file(s) GME  VERSO MAGO COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

                    ${RM} -f ${ESECOMP}_GMETR_*.zip
               fi

          fi

          ${RM} -f ${TMPDIR}/${FTPMAGOLOG}

          ${ECHO} "Fine fase ricezione file(s) GME" >> ${LOGDIR}/${MAGOLOG}
     fi

     return 0
}

##
#
##
function main
{

     ${ECHO} "__________________________________________________________________________________________________________________________" >> ${LOGDIR}/${MAGOLOG}
     ${ECHO} "Controllo esistenza e trasmissione files MIS e GME per MAGO ${GIORNO}/${MESE}/${ANNO} ${H}:${M}:${S}\n" >> ${LOGDIR}/${MAGOLOG}

     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 1;
whenever oserror exit 1;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select e.COD_GEST_ELEMENTO
  from ELEMENTI e
 inner join DEFAULT_CO d on e.COD_ELEMENTO = d.COD_ELEMENTO_ESE
 where d.FLAG_PRIMARIO=1
   and e.COD_TIPO_ELEMENTO='W';
exit 140
!)
     if [[ "${?}" != "140" ]]; then
          echo "Errore recupero dati!" >> ${LOGDIR}/${MAGOLOG}
          return 1
     fi

     export ESECOMP

     semaforo=$SEMAFORO_MIS
     CheckSemaforo || return 1

     if [[ "${CHECKFILE}" = "0" ]]; then
          ricevi_files_MIS || return 1
     fi;

     semaforo=$SEMAFORO_GME
     CheckSemaforo || return 1

     if [[ "${CHECKFILE}" = "0" ]]; then
         ricevi_files_GME || return 1
     fi;

     return 0
}

main || exit 1

exit 0
