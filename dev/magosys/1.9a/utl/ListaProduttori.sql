
SET LINES 200
SET VERIFY OFF

COLUMN REP_ELEGST NEW_VALUE REP_ELEGST
COLUMN REP_ELENOM NEW_VALUE REP_ELENOM
COLUMN REP_ELETIP NEW_VALUE REP_ELETIP
COLUMN REP_ESEGST NEW_VALUE REP_ESEGST
COLUMN REP_ESENOM NEW_VALUE REP_ESENOM
COLUMN REP_STADES NEW_VALUE REP_STADES
COLUMN REP_STATO  NEW_VALUE REP_STATO

COLUMN SPOOLNAME1 NEW_VALUE SPOOLNAME1
COLUMN SPOOLNAME2 NEW_VALUE SPOOLNAME2

DEFINE TIPO_FONTE = "&3"

SELECT COD_GEST_ELEMENTO    REP_ELEGST, 
       NOME_ELEMENTO        REP_ELENOM, 
       DESCRIZIONE          REP_ELETIP, 
       GST_ESE              REP_ESEGST, 
       NOM_ESE              REP_ESENOM,
       CASE 
         WHEN STATO = 1 
           THEN 'Stato Normale'
           ELSE 'Stato Attuale'
       END                  REP_STADES,
       STATO                REP_STATO,
       COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_ListaProduttori.txt' SPOOLNAME1, 
       COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_ListaProduttori.csv' SPOOLNAME2 
  FROM (SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO, DESCRIZIONE, GST_ESE, NOM_ESE, &STATO_RETE STATO 
          FROM ELEMENTI E
         INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
         INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO
         INNER JOIN V_ESERCIZI V ON 1=1
         WHERE E.COD_GEST_ELEMENTO = '&COD_GEST'
           AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
           AND ESE_PRIMARIO = 1
       );


CREATE OR REPLACE FORCE VIEW TMP_LISTA_PRODUTTORI_VIEW
AS 
SELECT PRODUTTORE, NOME, IN_AUI, TIPO_FORN, FONTI, POT_GRUPPI, NUM_GEN, PI, 
       CASE 
          WHEN NUM_GEN IS NULL THEN 'Nessun generatore definito'
                               ELSE NOTA
       END NOTA
  FROM (SELECT SUBSTR(A.COD_GEST_ELEMENTO,1,15) PRODUTTORE, NOME_ELEMENTO NOME,
               CASE 
                  WHEN B.COD_ORG_NODO IS NOT NULL THEN ' Si    '
                                                  ELSE '    No '
               END IN_AUI,
               TIPO_FORN, POT_GRUPPI, FONTI, NUM_GEN, NOTA, PI
          FROM (SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO  
                  FROM TABLE(PKG_ELEMENTI.LEGGIGERARCHIAINF(PKG_ELEMENTI.GETCODELEMENTO('&REP_ELEGST'),1,&REP_STATO,SYSDATE)) A
                 INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                  LEFT OUTER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = 'PMT' 
                 WHERE A.COD_TIPO_ELEMENTO = CASE 
                                               WHEN T.COD_TIPO_ELEMENTO IS NULL THEN 'CMT'
                                                                                ELSE 'PMT'
                                              END
                   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                ) A
          LEFT OUTER JOIN 
                (SELECT COD_ORG_NODO,SER_NODO,NUM_NODO,TIPO_ELEMENTO,ID_CLIENTE,
                        COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_CLIENTE COD_GEST_ELEMENTO,
                        TIPO_FORN, POT_GRUPPI 
                   FROM CLIENTI_TLC@PKG1_STMAUI.IT 
                  WHERE STATO = 'E'
                    AND TRATTAMENTO = 0
                    AND TIPO_FORN IN ('AP','PP')
                ) B ON A.COD_GEST_ELEMENTO = B.COD_GEST_ELEMENTO
          LEFT OUTER JOIN 
                (SELECT COD_ORG_NODO,SER_NODO,NUM_NODO,ID_CL ID_CLIENTE,
                        COUNT(*) NUM_GEN,
                        SUM((P_APP_NOM * NVL(N_GEN_PAR,1) * NVL(F_P_NOM,1))) PI
                   FROM GENERATORI_TLC@PKG1_STMAUI.IT 
                  WHERE STATO = 'E'
                    AND TRATTAMENTO = 0
                  GROUP BY COD_ORG_NODO,SER_NODO,NUM_NODO,ID_CL
                ) C ON C.COD_ORG_NODO = B.COD_ORG_NODO
                   AND C.SER_NODO = B.SER_NODO
                   AND C.NUM_NODO = B.NUM_NODO
                   AND C.ID_CLIENTE = B.ID_CLIENTE
             INNER JOIN 
                (SELECT COD_GEST_ELEMENTO,SUBSTR(FONTI,1,LENGTH(FONTI) -1) FONTI, NOTA
                   FROM (SELECT COD_GEST_ELEMENTO,
                                CASE WHEN BITAND(BITMAP_FONTI,1)  =  1 THEN 'S ' ELSE '  ' END||
                                CASE WHEN BITAND(BITMAP_FONTI,2)  =  2 THEN 'E ' ELSE '  ' END||
                                CASE WHEN BITAND(BITMAP_FONTI,4)  =  4 THEN 'I ' ELSE '  ' END||
                                CASE WHEN BITAND(BITMAP_FONTI,8)  =  8 THEN 'T ' ELSE '  ' END||
                                CASE WHEN BITAND(BITMAP_FONTI,16) = 16 THEN 'R ' ELSE '  ' END||
                                CASE WHEN BITAND(BITMAP_FONTI,32) = 32 THEN 'C ' ELSE '  ' END FONTI,
                                CASE TIPO_FONTE
                                     WHEN '1' THEN 'Fonti non Omogenee'
                                     ELSE NULL
                                END NOTA
                           FROM (SELECT COD_GEST_ELEMENTO, 
                                        CASE
                                          WHEN MAX(COD_TIPO_FONTE) IS NULL OR
                                               MIN(COD_TIPO_FONTE) IS NULL THEN NULL
                                          WHEN MAX(COD_TIPO_FONTE) = MIN(COD_TIPO_FONTE) 
                                              THEN MAX(COD_TIPO_FONTE)
                                              ELSE '1'
                                        END TIPO_FONTE,
                                        COUNT(*) NUM_FONTI,
                                        BITMAP_FONTI
                                  FROM (SELECT COD_GEST_ELEMENTO, COD_TIPO_FONTE,
                                               SUM(ID_RAGGR_FONTE) OVER (PARTITION BY COD_GEST_ELEMENTO) BITMAP_FONTI
                                          FROM (SELECT DISTINCT F.COD_RAGGR_FONTE COD_TIPO_FONTE, ID_ELEMENTO COD_GEST_ELEMENTO
                                                  FROM ELEMENTI A
                                                 INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
                                                 INNER JOIN TIPO_FONTI F ON F.COD_TIPO_FONTE = D.COD_TIPO_FONTE
                                                 WHERE SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE 
                                                   AND A.COD_TIPO_ELEMENTO = 'GMT'
                                                   AND ( INSTR('&TIPO_FONTE',D.COD_TIPO_FONTE) > 0 OR '&TIPO_FONTE' = '*')
                                               )
                                         INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE) 
                                         INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE)
                                       )
                                  GROUP BY COD_GEST_ELEMENTO, BITMAP_FONTI
                                )
                        )
                ) D ON A.COD_GEST_ELEMENTO = D.COD_GEST_ELEMENTO
       )
 ORDER BY PRODUTTORE;

SET pages0

SPOOL '&SPOOLNAME2'

SELECT 'PRODUTTORE;NOME;IN_AUI;TIPO_FORN;FONTI;POT_GRUPPI;NUM_GEN;PI;NOTA' AS " "
  FROM DUAL 
UNION ALL
SELECT PRODUTTORE||';'||NOME||';'||TRIM(IN_AUI)||';'||TIPO_FORN||';'||TRIM(FONTI)||';'||POT_GRUPPI||';'||NUM_GEN||';'||PI||';'||NOTA 
  FROM TMP_LISTA_PRODUTTORI_VIEW;

SPOOL OFF
       
COL PRODUTTORE FOR A15
COL NOME       FOR A20
COL FONTI      FOR A13
COL TIPO_FORN  FOR A4      HEADING 'TIPO|FORN'
COL POT_GRUPPI FOR   99990 HEADING 'POT|GRUPPI'
COL NUM_GEN    FOR   99990 HEADING 'NUM|GEN'
COL PI         FOR 9999990 HEADING 'POT INST|KW'

SET PAGES 9999

SPOOL '&SPOOLNAME1'
      
TTITLE ================================================================================================================= SKIP 1 -
       REP_ESEGST "-" REP_ESENOM "      "                                                                                SKIP 1 -  
       ================================================================================================================= SKIP 1 -
       "Lista produttori sottesi a      " REP_ELETIP "  " REP_ELEGST " " REP_ELENOM "   " REP_STADES                     SKIP 1 -
       ================================================================================================================= SKIP 2;

BREAK ON REPORT;
COMPUTE SUM LABEL "Totale PI" OF PI ON REPORT;

SELECT * FROM TMP_LISTA_PRODUTTORI_VIEW;

SPOOL OFF
       
DROP VIEW TMP_LISTA_PRODUTTORI_VIEW;
 
EXIT;

        
        
        