SET pages 5000
SET lines 300
col NOME_CLIENTE FOR a20
col COD_GEST_ELEMENTO FOR a14
col COMUNE FOR a25
col CAB_PRI FOR a11
col TP_AUI  FOR a6
col TP_MAGO FOR a7
WITH CLI_MAGO AS (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_CLIENTE,POTENZA_INSTALLATA
                        ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'COM', SYSDATE, 2, 1) COM_SN
                        ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'CPR', SYSDATE, 1, 1) CP_SN 
                        --,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'COM', SYSDATE, 2, 2) COM_SA
                        --,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, 'CPR', SYSDATE, 1, 2) CP_SA 
                    FROM V_ELEMENTI 
                   WHERE COD_TIPO_ELEMENTO = 'CMT' 
                     AND COD_TIPO_CLIENTE IN ('A','B')
                 )
     ,CLI_AUI AS (SELECT A.GEST_PROD,
                         A.RAGIO_SOC   NOME_PROD,
                         A.TIPO_FORN   TIPO_FORN,
                         A.POT_GRUPPI,
                         SUM(A.POT_INST_GEN) POT_INST_GEN,
                         CASE
                             WHEN NVL(MIN(NVL(A.TIPO_IMP,'3')),'*') = '*' OR NVL(MAX(NVL(A.TIPO_IMP,'3')),'*') = '*' THEN '3'  -- 3=Fonte Non Disponibile
                             WHEN MAX(NVL(A.TIPO_IMP,'3')) <> MIN(NVL(A.TIPO_IMP,'3'))                               THEN '1'  -- 1=Fonte Non Omogenea
                                                                                                                     ELSE MAX(NVL(A.TIPO_IMP,'3'))
                         END TIPO_IMP,
                         COUNT(*) num_gen
                    FROM (SELECT c.COD_ORG_NODO||c.SER_NODO||c.NUM_NODO||c.TIPO_ELEMENTO||c.ID_CLIENTE GEST_PROD,
                                 C.RAGIO_SOC,C.TIPO_FORN,C.POT_GRUPPI,G.TIPO_IMP,
                                 (G.P_APP_NOM * NVL(G.N_GEN_PAR,1) * NVL(G.F_P_NOM,1)) POT_INST_GEN
                            FROM CLIENTI_TLC@PKG1_STMAUI.IT C 
                            LEFT OUTER JOIN GENERATORI_TLC@PKG1_STMAUI.IT G ON G.COD_ORG_NODO = C.COD_ORG_NODO
                                                                           AND G.SER_NODO     = C.SER_NODO
                                                                           AND G.NUM_NODO     = C.NUM_NODO
                                                                           AND G.ID_CL        = C.ID_CLIENTE
                                                                           AND G.TRATTAMENTO  = 0
                                                                           AND G.STATO        = 'E'
                           WHERE C.TIPO_FORN IN ('AP','PP','PD')
                             AND C.TRATTAMENTO = 0 AND C.STATO = 'E'
                         ) A 
                   GROUP BY A.GEST_PROD,A.RAGIO_SOC,A.TIPO_FORN,A.POT_GRUPPI 
                 )
     ,CLI_PI  AS (SELECT COD_GEST_ELEMENTO,M.VALORE PI_MISURA 
                    FROM CLI_MAGO E 
                    LEFT OUTER JOIN TRATTAMENTO_ELEMENTI T ON T.cod_elemento = E.cod_elemento   
                                                          AND T.COD_TIPO_MISURA = 'PI'
                                                          AND tipo_aggregazione = 1
                    LEFT OUTER JOIN MISURE_AGGREGATE_STATICHE  M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM 
                                                                AND  SYSDATE BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE 
                 )
SELECT ROWNUM num, A.*
  FROM (SELECT A.*,PI_MISURA PI_MAGO_MIS
          FROM (SELECT NVL(M.COD_GEST_ELEMENTO,A.GEST_PROD) COD_GEST_ELEMENTO,
                       M.NOME_ELEMENTO NOME_CLIENTE,
                       A.TIPO_IMP,
                       A.num_gen,
                       A.POT_INST_GEN PI_GEN,
                       PKG_ELEMENTI.GETGESTELEMENTO(CP_SN) CAB_PRI,
                       C.NOME_ELEMENTO COMUNE,
                       CASE WHEN A.TIPO_FORN IS NULL THEN NULL ELSE '  si  ' END IN_AUI,
                       CASE WHEN M.COD_TIPO_CLIENTE IS NULL THEN NULL ELSE '  si  ' END IN_MAGO,
                       A.TIPO_FORN TP_AUI,M.COD_TIPO_CLIENTE TP_MAGO,
                       A.POT_GRUPPI PI_AUI,M.POTENZA_INSTALLATA PI_MAGO_ELE
                  FROM CLI_AUI  A
                  FULL OUTER JOIN CLI_MAGO   M ON M.COD_GEST_ELEMENTO = A.GEST_PROD
                  LEFT OUTER JOIN V_ELEMENTI C ON C.COD_ELEMENTO      = COM_SN
               ) A
          LEFT OUTER JOIN CLI_PI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO 
         ORDER BY CAB_PRI NULLS LAST, IN_MAGO NULLS LAST,IN_AUI NULLS LAST,A.COD_GEST_ELEMENTO
       ) A;
       
       
