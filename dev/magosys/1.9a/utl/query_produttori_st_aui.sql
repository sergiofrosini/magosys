set lines 128
col nome for a25 
col cpr for a15
set pages 10000

select cor.cod_gest 
, cor.nome
,case WHEN (aui.cod_gest is not null ) THEN 1 else 0 END IN_AUI
,case WHEN (aui.tipo_forn in ('AP','PP') ) THEN 1 else 0 END PROD_OK
,tipo_forn 
,pkg_elementi.getgestelemento(pkg_elementi.getelementopadre(pkg_elementi.getcodelemento(cor.cod_gest),'CPR',sysdate,1,1)) cpr
,aui.pot_gruppi
FROM CORELE.CLIENTIMT_SA cor
,(select aui.*, trim(cod_org_nodo || ser_nodo || num_nodo || tipo_elemento || id_cliente)  cod_gest
from CLIENTI_TLC@PKG1_STMAUI.IT /*CLIENTI_TLC@PKG1_STMAUI.IT*/ aui
) aui
where sysdate between cor.data_inizio and cor.data_fine 
and cor.cod_gest = aui.cod_gest(+)
and aui.stato(+) = 'E' 
and aui.trattamento(+) = 0
--and aui.tipo_forn in ('AP','PP')
order by 3 desc ,4 desc ,5,1;