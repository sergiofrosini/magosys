
----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedura    PKG_KPI.InsertRichiesta
-- per definizione nuova richiesta
----------------------------------------------------------------------------------------------------------------
DECLARE
   vTabEle T_COD_GEST_ARRAY := T_COD_GEST_ARRAY(); 
   vEle    T_COD_GEST_OBJ := T_COD_GEST_OBJ(NULL);
   vNum    INTEGER := 0;
   vNewReq T_KPI_RICHIESTA_OBJ := T_KPI_RICHIESTA_OBJ(NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL,
                                                      NULL,NULL,NULL,NULL,NULL);
   vID_REQ_KPI NUMBER;
BEGIN  
    FOR i IN (SELECT cod_gest_elemento FROM ELEMENTI WHERE cod_tipo_elemento IN ('CMT')) LOOP
            vEle.COD_GEST := i.cod_gest_elemento;
            vTabEle.EXTEND;
            vTabEle(vTabEle.LAST) := vEle;
            vNum := vNum +1;
             
    END LOOP;

    vNewReq.OPERATORE          := 'operatore';
    vNewReq.DATA_SCADENZA      := SYSDATE + 60;
    vNewReq.PERIODO_INIZIO     := TO_DATE('21/10/2014 00:00','dd/mm/yyyy hh24:mi');
    vNewReq.PERIODO_FINE       := TO_DATE('30/10/2014 23:45','dd/mm/yyyy hh24:mi');
    vNewReq.ORARIO_INIZIO      := '08:30';
    vNewReq.ORARIO_FINE        := '15:30';
    vNewReq.RISOLUZIONE        := 60;
    vNewReq.ORGANIZZAZIONE     := 1;
    vNewReq.STATO_RETE         := 2;
    vNewReq.LISTA_FONTI        := 'S|E|I|T|R|C';
    vNewReq.LISTA_TIPI_RETE    := 'M|B';
    vNewReq.LISTA_TIPI_CLIE    := 'A|B|X';
    vNewReq.LISTA_INDICI_RICH  := 'MAE|NMAE2|SD';
    vNewReq.LISTA_TIPI_MISURA  := 'PAS';
    vNewReq.LISTA_TIPI_MISURA_FORECAST  := 'PI|PAS';
    vNewReq.FREQ_ERR_VAL_INI   := 80;
    vNewReq.FREQ_ERR_VAL_FIN   := 280;
    vNewReq.FREQ_ERR_PASSO     := 20;
    vNewReq.VALORE_SOGLIA      := 90;
    vNewReq.SOGLIA_COLORE      := 123;
    vNewReq.TIPO_NOTIFICA      := 1;
    vNewReq.DELTA              := 5;

    PKG_KPI.InsertRichiesta  (vNewReq,      -- dati della richiesta
                              vTabEle,      -- CG degli elementi coinvolti
                              vID_REQ_KPI   -- ID della richiesta (Parametro in OUTPUT) 
                             );
    
    DBMS_OUTPUT.PUT_LINE('inserita richiesta '||vID_REQ_KPI||'  con '||vNum||'  elementi');
END;
/


----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedure    
--      PKG_KPI.GetRichiesta  per lettura specifica richiesta tramite ID_Richiesta
--      PKG_KPI.GetRichieste  per lettura delle richieste di uno specifico operatore o di tutte le richieste definite
----------------------------------------------------------------------------------------------------------------
/*
UPDATE KPI_ELEMENTI SET STATO = 1 WHERE SUBSTR(TO_CHAR(COD_ELEMENTO),-1)  IN (0,2,4) AND ID_REQ_KPI = 9;
COMMIT; 
*/

DECLARE
   vLst PKG_UtlGlb.t_query_cur;
   vRec T_KPI_RICHIESTA_OBJ := T_KPI_RICHIESTA_OBJ(NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL,
                                                   NULL,NULL,NULL,NULL,NULL);  
BEGIN

   -- resituisce una specifica richiesta
   -- PKG_KPI.GetRichiesta(vLst,        -- cursore contenente i dati relativi alla richiesta 3689
   --                      1         -- identificativo della richiesta desiderata
   --                     );       

   -- resituisce le richieste di uno specifico operatore
   -- PKG_KPI.GetRichieste(vLst,        -- cursore contenente i dati relativi all'operatore PIPPO
   --                      'operatore'  -- operatore
   --                     );       

    -- resituisce tutte le richieste presenti
    PKG_KPI.GetRichieste(vLst);        -- cursore contenente tutte le richieste definite a sistema

   -- resituisce le richieste di uno specifico operatore per gli stati 1 e 0
   -- PKG_KPI.GetRichieste(vLst,        -- cursore contenente i dati relativi all'operatore PIPPO
   --                      'operatore'  -- operatore
   --                      '0|1'        -- Stati 0 e 1-> devono essere separati da | Pipe - attenzione caratteri non numerici causano errore
   --                     );       

   -- resituisce le richieste tutte le richieste con stato 1 e 0
   -- PKG_KPI.GetRichieste(vLst,        -- cursore contenente i dati relativi all'operatore PIPPO
   --                      'operatore'  -- operatore
   --                      '0|1'        -- Stati 0 e 1-> devono essere separati da | Pipe - attenzione caratteri non numerici causano errore
   --                     );       
   
   LOOP
      FETCH vLst INTO vRec.ID_REQ_KPI,
                      vRec.DATA_RICHIESTA,
                      vRec.DATA_SCADENZA,
                      vRec.OPERATORE,
                      vRec.PERIODO_INIZIO,
                      vRec.PERIODO_FINE,
                      vRec.ORARIO_INIZIO,
                      vRec.ORARIO_FINE,
                      vRec.RISOLUZIONE,
                      vRec.ORGANIZZAZIONE,
                      vRec.STATO_RETE,
                      vRec.LISTA_FONTI,
                      vRec.LISTA_TIPI_RETE,
                      vRec.LISTA_TIPI_CLIE,
                      vRec.LISTA_INDICI_RICH,
                      vRec.LISTA_TIPI_MISURA,
                      vRec.LISTA_TIPI_MISURA_FORECAST,
                      vRec.FREQ_ERR_VAL_INI,
                      vRec.FREQ_ERR_VAL_INI_PERC,
                      vRec.FREQ_ERR_VAL_FIN,
                      vRec.FREQ_ERR_VAL_FIN_PERC,
                      vRec.FREQ_ERR_PASSO,
                      vRec.FREQ_ERR_PASSO_PERC,
                      vRec.SOGLIA_COLORE,
                      vRec.SOGLIA_COLORE_PERC,
                      vRec.VALORE_SOGLIA,
                      vRec.DELTA,
                      vRec.TIPO_NOTIFICA,
                      vRec.ELAB_INIZIO,
                      vRec.ELAB_FINE,
                      vRec.ELAB_STATO,
                      vRec.DATA_STATO,
                      vRec.OPER_STATO,
                      vRec.ELAB_NOTE,
                      vRec.AVANZAMENTO;
      EXIT WHEN vLst%NOTFOUND;
      DBMS_OUTPUT.PUT_LINE (RPAD(TO_CHAR(vRec.ID_REQ_KPI),6,' ')||' - '||
                            TO_CHAR(vRec.DATA_RICHIESTA,'dd/mm/yyyy hh24:mi')||' - '||
                            RPAD(TO_CHAR(vRec.OPERATORE),20,' ')||' - '||
                            TO_CHAR(ROUND(vRec.AVANZAMENTO,2),'999.99')||' - '||
                            vRec.ELAB_STATO||' - '||
                            vRec.SOGLIA_COLORE||' - '||
                            vRec.TIPO_NOTIFICA
                            );
   END LOOP;
   CLOSE vLst;
END;
/


----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedura    
--      PKG_KPI.GetElementsByReq  per lettura degli elementi coinvolti in una richiesta
----------------------------------------------------------------------------------------------------------------

DECLARE
   vLst PKG_UtlGlb.t_query_cur;
   vCod ELEMENTI.COD_ELEMENTO%TYPE;
   vGst ELEMENTI.COD_GEST_ELEMENTO%TYPE;
   vNom ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
   vSta KPI_ELEMENTI.STATO%TYPE;
BEGIN
   PKG_KPI.GetElementsByReq(vLst,1);  
   LOOP
      FETCH vLst INTO vCod,vGst,vNom,vSta;
      EXIT WHEN vLst%NOTFOUND;
      DBMS_OUTPUT.PUT_LINE (RPAD(TO_CHAR(vCod),7,' ')||' - '||RPAD(TO_CHAR(vGst),16,' ')||' - '||RPAD(TO_CHAR(vNom),20,' ')||' - '||vSta);
   END LOOP;
   CLOSE vLst;
END;
/


----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedure    PKG_KPI.SetStatoElemento e PKG_KPI.SetStatoElementoCG 
-- per modifica stato di un elamento (CG) nell'abito di una richiesta
-- (*) Stato del'elemento; 0=da elaborare 
--                         1=elaborato 
----------------------------------------------------------------------------------------------------------------
BEGIN
    PKG_KPI.SetStatoElemento  (3691,                      -- identificativo della richiesta
                               23898,                     -- codice dell'elemento
                               1                          -- nuovo stato (*)
                              );                 
    PKG_KPI.SetStatoElementoCG(3691,                      -- identificativo della richiesta
                               'DQ102001076T01',          -- CG dell'elemento
                               1                          -- nuovo stato (*)
                              );                 
END;
/


----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedura    PKG_KPI.SetStatoRichiesta
-- (*) Stato avanzamento della richiesta; 0=da elaborare, 
--                                        1=elab. in corso, 
--                                        2=elaborazione conclusa, 
--                                        3=elaborazione interrotta, 
--                                       -1=errore di elaborazione (dettaglio in ELAB_NOTE) 
----------------------------------------------------------------------------------------------------------------
BEGIN
    PKG_KPI.SetStatoRichiesta(11,            -- identificativo della richiesta
                              3,               -- stato da attribuire alla richiesta (*)
                              'operatore1',    -- operatore/funzione che da origine al cambio stato
                              NULL,            -- data del cambio stato 
                              'iniziato !!!'   -- Nota
                             );
END;
/



----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedura    PKG_KPI.InsertValori
----------------------------------------------------------------------------------------------------------------
DECLARE
   vTabMis T_KPI_MIS_ARRAY := T_KPI_MIS_ARRAY(); 
   vMis    T_KPI_MIS_OBJ   := T_KPI_MIS_OBJ(NULL,NULL,NULL,NULL,NULL,NULL); 
   vRich   KPI_RICHIESTA.ID_REQ_KPI%TYPE := 15;  
BEGIN
 
    BEGIN

        FOR i IN (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO 
                    FROM KPI_ELEMENTI 
                   INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                  WHERE ID_REQ_KPI = vRich) LOOP
        
            vMis.COD_ELEMENTO := i.COD_ELEMENTO;
            vMis.COD_GEST_ELEMENTO := NULL; --i.COD_GEST_ELEMENTO;
            
            FOR DD IN 14 .. 15 LOOP
                FOR HH IN 11 .. 12 LOOP

                   vMis.DATA := TO_DATE(LPAD(DD,2,'0')||'/09/2014 '||LPAD(HH,2,'0')||':00','dd/mm/yyyy hh24:mi'); 

                   vMis.COD_INDICE := 'MAE';
                   vMis.VALORE := ROUND((((HH+3.14))/DD)+DBMS_RANDOM.VALUE(-1,4),6); 
                   vTabMis.EXTEND;
                   vTabMis(vTabMis.LAST) := vMis;

                   vMis.COD_INDICE := 'NMAE2';
                   vMis.VALORE := ROUND((((HH+3.14))/DD)*DBMS_RANDOM.VALUE(-5,3),6); 
                   vTabMis.EXTEND;
                   vTabMis(vTabMis.LAST) := vMis;

                   vMis.COD_INDICE := 'SD';
                   vMis.VALORE := ROUND((((HH+3.14))/DD)*DBMS_RANDOM.VALUE(-2,2),6); 
                   vTabMis.EXTEND;
                   vTabMis(vTabMis.LAST) := vMis;

                END LOOP;
            END LOOP;
        END LOOP;
    END;
   
   PKG_KPI.InsertValori(vRich,vTabMis);

END;
/


----------------------------------------------------------------------------------------------------------------
-- esempio utilizzo procedura    PKG_KPI.GetValori
----------------------------------------------------------------------------------------------------------------
DECLARE
   vLst PKG_UtlGlb.t_query_cur;
   vEle ELEMENTI.COD_ELEMENTO%TYPE;
   vGst ELEMENTI.COD_GEST_ELEMENTO%TYPE;
   vIdx TRATTAMENTO_ELEMENTI.COD_TIPO_MISURA%TYPE;
   vDat DATE;
   vVal KPI_INDICI.VALORE%TYPE;
BEGIN
-- Richiesta valori di un Elemento della richiesta
   --PKG_KPI.GetValori(vLst,             -- cursore
   --                  10,               -- id richiesta
   --                  NULL,             -- codice elemento (prioritario rispetto a codice gestionale)
   --                  'DQ102098518T01'  -- CG (usato se codice elemento nullo)
   --                 );  
-- Richiesta valori relativi a tutti gli elementi du uba richiesta
   PKG_KPI.GetValori(vLst,             -- cursore
                     10                -- id richiesta
                    );  
   LOOP
      FETCH vLst INTO vEle,vGst,vIdx,vDat,vVal;
      EXIT WHEN vLst%NOTFOUND;
      DBMS_OUTPUT.PUT_LINE (vEle||' - '||vGst||' - '||RPAD(TO_CHAR(vIdx),7,' ')||' - '||TO_CHAR(vDat,'dd/mm/yyyy hh24:mi:ss')||' - '||vVal);
   END LOOP;
   CLOSE vLst;
END;
/
