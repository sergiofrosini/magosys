-- Seleziona gli ultimi n meteo_job_id e cancella i relativi zip e xml, in modo da poter rieseguire MDS per i giorni già elaborati.
-- Impostare un opportuno valore di vNum tale da prendere gli ultimi vNum meteo_job id e cancellarne i relativi riferimenti ai file zip e xml, in cascata.

-- (Opzionale) Cancella eventuali job già schedulati per essere aggregati
--delete scheduled_jobs;
--delete scheduled_tmp_gen;
--delete scheduled_tmp_gme;
--delete scheduled_tmp_met;
-- commit;

-- seleziona gli ultimi n meteo_job_id e cancella i relativi zip e xml
DECLARE
vNum NUMBER := 30;

BEGIN
  delete 
    from meteo_file_xml 
    where meteo_file_zip_key in 
      (SELECT id 
        FROM meteo_file_zip 
        WHERE meteo_job_key
        IN (select ID 
            FROM 
              (SELECT ID 
                FROM meteo_job 
                ORDER BY ID DESC) 
            WHERE ROWNUM <= vNum)
      ); 
  delete FROM meteo_file_zip WHERE meteo_job_key
         IN (select ID 
            FROM 
              (SELECT ID 
                FROM meteo_job 
                ORDER BY ID DESC) 
            WHERE ROWNUM <= vNum);
  commit;

END;
/
