#!/usr/bin/ksh
#
#       scarica_gme_mensili_MAGO.ksh        (Siemens Italia SpA)
#
#       Data:           Febbraio 2015 
#       Autore          E. Castrignano
#       Aggiornamenti:  
#
#       Modifiche da implementare :
# 
#       Descrizione:
#            Script che scarica dal server di scambio
#            i files con i profili mensili GME 
#            per l'utilizzo nell'applicativo MAGO
#       Parametri:
#            - N_MAX_MESI: da passare a riga di comando: se presente indica il numero di mesi
#                          massimo del passato da scaricare.
#            - Esistenza di un file che indica se i dati dell'esercizio di
#              competenza sono utilizzati anche come test per il centro prove.
#
#       Modalita' di attivazione:
#            Da crontab
#
#----------------------------------------------------------------------------------
script_name_less_ext=$(basename $0 .ksh)

N_MAX_MESI="$1"
if [[ "${N_MAX_MESI}" = "" ]]; then N_MAX_MESI=0; fi

##
# Definizione delle variabili di ambiente ORACLE 
##
export ORACLE_HOME="/oracle_bases/ARCDB2"
export ORACLE_SID="ARCDB2"

##
# Definizione delle directory di lavoro 
##
TMPDIR="/usr/NEW/magosys/tmp"
UTLDIR="/usr/NEW/magosys/utl"
BINDIR="/usr/NEW/magosys/bin"
LOGDIR="/usr/NEW/magosys/log"
GME_MM_MAGODIR="/usr/NEW/magosys/STWebDataLoader/Runtime/fromGME/mensili"
GME_MM_BCKDIR="/usr/NEW/magosys/STWebDataLoader/Runtime/CCinput/backup"
PCAUIMAGODIR="/MAGO"
##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"

##
# Definizione dei comandi di sistema utilizzati nello script 
##
AUI_HOST="ftpauitel.enel.com"
SFTP_PORT=11022
AUTH="mago/mago"
CAT="${USRDIR}/cat"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
EGREP="${USRDIR}/egrep"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
UNZIP="${USRDIR2}/unzip"
WC="${USRDIR2}/wc"
SFTP="${USRDIR2}/sftp"
CUT="${USRDIR2}/cut"

##
# Definizione delle variabili di date-time 
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}

##
# Definizione nomi files
##
MAGOLOG="download_GMEMM_${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp_gme_mm.log"
GMEMMLIST="list_mago_gme_mm.log"
ESECOMP_X_CP="${UTLDIR}/esecompTestGME_mensili.txt"
lockfile="${UTLDIR}/${script_name_less_ext}"
tmp_lockfile="${UTLDIR}/${script_name_less_ext}.$$"
SFTP_USR_PATH_FILE="${UTLDIR}/sftp_usr_path.csv"
SFTP_CERT_USR=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f1)
CERTIFICATO=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f2)

##
# Definizione variabili varie
##
MAX_TRY=3

#
# Scrittura su file di log
#
function SendLog
{
     dataora=$(date +'%Y.%m.%d %H:%M:%S')
     $ECHO "${dataora} - ${1}" >> ${LOGDIR}/${MAGOLOG}
     return
}

# Controllo per non far partire esecuzioni multiple.
# Si utilizza un hard link in quanto e' un'operazione atomica.
# Oltre ad evitare l'esecuzione di un'altra istanza serve per evitare
# anche una race condition.
$ECHO $$ > $tmp_lockfile
if $(ln $tmp_lockfile $lockfile 2>&-); then
     SendLog "locked PID: $$ "
     trap "rm -f ${lockfile} ${tmp_lockfile} >/dev/null 2>&1" 0
     trap "exit 2" 1 2 3 13 15
else
     SendLog "Processo gia' in esecuzione con PID: $(<$lockfile) "
     trap "rm -f ${tmp_lockfile} >/dev/null 2>&1" 0
     trap "exit 2" 1 2 3 13 15
     exit 0
fi

function sftp_cert
{
     L_SFTP_FILE=$1
     L_SFTP_CERT_USR=$2
     L_SFTP_HOST=$3
     L_SFTP_LOG=$4

     # Aggiungo questa istruzione in comune a tutte le trasmissioni.
     ${ECHO} "exit 100" >> $L_SFTP_FILE

     ${SFTP} -oPort=$SFTP_PORT -oIdentityFile="${CERTIFICATO}" -b $L_SFTP_FILE ${L_SFTP_CERT_USR}@${L_SFTP_HOST} 1>> $L_SFTP_LOG 2>&1

     # Si ricerca "exit 100" perche' faccio terminare i batch sftp con questa stringa.
     # Se sftp non termina correttamente uno dei comandi, la modalita' batchmode fa
     # abortire la sessione, cosi non troverei la stringa cercata.
     res=$(grep "exit 100" $L_SFTP_LOG|wc -l)
     #echo "res: $res "
     if (( $res == 1 )); then
          # sftp ok
          v_check=0
     else
          # sftp ko
          v_check=1
     fi

     return $v_check
}

function gest_file_integrity_GME_MM
{
     EXIST_FILE=$(${LS} -1a ${TMPDIR}/${ESECOMP}_GME_*.zip | ${WC} -l)
     if (( $EXIST_FILE != 0 )); then
          cd $TMPDIR
          ${LS} ${ESECOMP}_GME_*.zip > ${TMPDIR}/test_integrity_gme_mm.txt
          for filename in $(${CAT} ${TMPDIR}/test_integrity_gme_mm.txt)
          do
               ${UNZIP} -t $filename > /dev/null
               case "${?}" in
               0 )           SendLog "FILE $filename INTEGRO (TRASMISSIONE COMPLETATA) [OK] al ${DATA}"
                             ;;
               1 )           SendLog "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [ERROR] al ${DATA}"
                             $RM -f $filename
                             ;;
               2 )           SendLog "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [WARNING] al ${DATA}"
                             $RM -f $filename
                             ;;
               * )           SendLog "FILE $filename NON INTEGRO (gestione di default del case) [ERROR] al ${DATA}"
                             $RM -f $filename
                             ;;
               esac
          done
          ${RM} -f ${TMPDIR}/test_integrity_gme_mm.txt
     fi
     return 0
}

# MAGO
##
# Processo di attivazione FTP per ricezione file GME con i profili mensili.
##
function ricevi_files_GME_MM
{ 
     attempt=1
     check=0
     count_downlad=0

     SendLog "Inizio fase ricezione file(s) GME mensili.. tentativo $attempt di ${MAX_TRY}"
     if (( $N_MAX_MESI > 0 )); then
          SendLog "Download dei profili mensili GME degli ultimi $N_MAX_MESI mesi."
     else
          SendLog "Download di tutti i profili mensili GME presenti."
     fi

     while (( $check == 0 && $attempt <= $MAX_TRY ))
     do
          SendLog "..tentativo $attempt di ${MAX_TRY}"

          cd ${TMPDIR}

          ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
          retping=$(${ECHO} ${?})

          if [[ "${retping}" != "0" ]]; then
               SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"

               if (( $attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    SendLog "sleep di $n_random min prima del prossimo tentativo..."
                    sleep ${n_random}m
               fi          

               attempt=$(($attempt+1))
          else
               SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"
          
               SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
               ${ECHO} "cd STM_${ESECOMP}/GME\r" > $SFTP_FILE_PAR
               ${ECHO} "ls -l1 ${ESECOMP}_GME_*.zip\r" >> $SFTP_FILE_PAR

               sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${GMEMMLIST}"

               $EGREP "^${ESECOMP}_GME_" ${TMPDIR}/${GMEMMLIST} > ${TMPDIR}/aaa_gme_mm.csv

               for file_gme_mm in $($CAT ${TMPDIR}/aaa_gme_mm.csv)
               do
                    EXISTSGMEMM=$($LS ${GME_MM_BCKDIR}/$file_gme_mm  | $WC -l)

                    if (( $EXISTSGMEMM == 0 )); then

                         if (( $N_MAX_MESI > 0 )); then

                              new_m=$MESE
                              new_y=$ANNO
                              i=$N_MAX_MESI

                              while (( $i > 0 ))
                              do
                                   new_m=$(($new_m-1))
                                   i=$(($i-1))

                                   if (( $new_m == 0 )); then
                                        new_m=12
                                        new_y=$(($new_y-1))
                                   fi
                              done
                         
                              anno_file=$(${ECHO} $file_gme_mm | cut -c13-16)
                              mese_file=$(${ECHO} $file_gme_mm | cut -c10-11)
                              data_file=${anno_file}${mese_file}

                              new_m=$(printf "%02d\n" ${new_m})

                              data_rif=${new_y}${new_m}

                              if (( $data_file > $data_rif )); then

                                   SendLog "Download file ${file_gme_mm}"

                                   SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
                                   ${ECHO} "cd STM_${ESECOMP}/GME\r" > $SFTP_FILE_PAR
                                   ${ECHO} "get ${file_gme_mm}\r" >> $SFTP_FILE_PAR

                                   sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

                                   retcode=$(${ECHO} ${?}) 
                                   if [[ "${retcode}" = "1" ]]; then
                                        while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                                        ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                                        SendLog "FTP file(s) GME FALLITA per ${file_gme_mm}"
                                   else
                                        while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                                        ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                                        SendLog "FTP file(s) GME COMPLETATA per ${file_gme_mm}."
                                        count_downlad=$(($count_downlad+1))
                                   fi
                              fi
                         else
                              SendLog "Download file ${file_gme_mm}"

                              SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
                              ${ECHO} "cd STM_${ESECOMP}/GME\r" > $SFTP_FILE_PAR
                              ${ECHO} "get ${file_gme_mm}\r" >> $SFTP_FILE_PAR

                              sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

                              retcode=$(${ECHO} ${?}) 
                              if [[ "${retcode}" = "1" ]]; then
                                   while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                                   ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                                   SendLog "FTP file(s) GME FALLITA per ${file_gme_mm}"
                              else
                                   while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                                   ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                                   SendLog "FTP file(s) GME COMPLETATA per ${file_gme_mm}."
                                   count_downlad=$(($count_downlad+1))
                              fi
                         fi
                    fi
               done

               SendLog "Sono stati scaricati $count_downlad file(s)."

               ${RM} -f ${TMPDIR}/${GMEMMLIST}
               ${RM} -f ${TMPDIR}/aaa_gme_mm.csv
                  
               gest_file_integrity_GME_MM || return 1

               # Spedizione files al centro prove nel caso in cui
               # il CO sia utilizzato per i test 
               if [[ -e "${ESECOMP_X_CP}" ]]; then

                    send_file_to_cp || return 1

               fi

               SendLog "Riposizionamento file(s) GME nella cartella di lavoro ${GME_MM_MAGODIR}."

               ${MV} ${ESECOMP}_GME_*.zip $GME_MM_MAGODIR

               check=1
          fi
     done

     if (( $attempt > $MAX_TRY )); then
          SendLog "Superato il numero massimo (${MAX_TRY}) di tentativi di connessione!"
          return 1
     fi

     ${RM} -f $SFTP_FILE_PAR
     SendLog "Fine fase ricezione file(s) GME"

     return 0
}

function send_file_to_cp
{
     s_attempt=1
     s_check=0

     while (( $s_check == 0 && $s_attempt <= $MAX_TRY ))
     do
          SendLog "Inizio spedizione file(s) profili mensili GME ad utilizzo centro prove tentativo $s_attempt di ${MAX_TRY}"

          cd ${TMPDIR}

          ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
          retping=$(${ECHO} ${?})

          if [[ "${retping}" != "0" ]]; then
               SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"

               if (( $s_attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    SendLog "sleep di $n_random min prima del prossimo tentativo..."
                    sleep ${n_random}m
               fi          

               s_attempt=$(($s_attempt+1))
          else
               SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"

               SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
               ${ECHO} "cd ${PCAUIMAGODIR}\r" > $SFTP_FILE_PAR
               ${ECHO} "put ${ESECOMP}_GME_*.zip\r" >> $SFTP_FILE_PAR

               sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then
                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GMEPR FALLITA"

                    if (( $s_attempt < 3 )); then 
                         n_random=$((${RANDOM}%15+1))
                         SendLog "sleep di $n_random min prima del prossimo tentativo..."
                         sleep ${n_random}m
                    fi     

                    s_attempt=$(($s_attempt+1))
               else
                    while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
                    ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
                    SendLog "FTP file(s) GME COMPLETATA"

                    s_check=1
               fi
          fi
     done

     if (( $s_attempt > $MAX_TRY )); then
          return 1
     fi

     ${RM} -f $SFTP_FILE_PAR
     SendLog "Fine spedizione file(s) profili mensili GME ad utilizzo centro prove"
     return 0
}

function main
{

     SendLog "_______________________________________________________________________"
     SendLog "Controllo esistenza e recupero file(s) profili mensili GME per MAGO \n"

     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 1;
whenever oserror exit 1;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select ut.CODIFICA_UTR||e.CODIFICA_ESERCIZIO
  from SAR_ADMIN.ESERCIZI_ABILITATI eab
 inner join SAR_ADMIN.ESERCIZI e on eab.COD_ESERCIZIO = e.COD_ESERCIZIO
                                and eab.COD_UTR = e.COD_UTR
 inner join SAR_ADMIN.UNITA_TERRITORIALI ut on ut.COD_UTR = e.COD_UTR
 where eab.COD_APPLICAZIONE='ADM';

exit 140
!)
     if [[ "${?}" != "140" ]]; then
          SendLog "Errore recupero dati amministrazione!"
          return 1
     fi

     export ESECOMP

     ricevi_files_GME_MM || return 1

     return 0
}

main || exit 1

exit 0
