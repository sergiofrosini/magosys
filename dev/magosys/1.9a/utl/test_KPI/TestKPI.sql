-- Test KPI su DB (CATANZARO) elementi:
-- DQ102034477U01
-- DQ102013233U01

/* PKG_Misure.GetMisure  PKG_Misure.GetMisureCG */
DECLARE
  tmdat           TIMESTAMP (4);
    vLst            PKG_UtlGlb.t_query_cur;
    pOrganizzazione INTEGER := 1;
    pStatoRete      INTEGER := 2;
    pGstElem        ELEMENTI.COD_GEST_ELEMENTO%TYPE :='DQ102034477U01';
    pCodElem        ELEMENTI.COD_ELEMENTO%TYPE := NULL;
    pDataDa         DATE := TO_DATE ('21/03/2014 00.00', 'dd/mm/yyyy hh24.mi');
    pDataA          DATE := TO_DATE ('21/03/2014 23.00', 'dd/mm/yyyy hh24.mi');
    pTipiMisura     VARCHAR2 (300) := 'PAS';
    pTipologiaRete  VARCHAR2 (300) := 'M|B';
   pFonte          VARCHAR2 (300) := '1!S|E|I|T|R|C|1|2|3';
    pTipoProd       VARCHAR2 (300) := 'A|B|X|Z|C';
    vAggr           INTEGER := 15;
    vCnt            INTEGER;
    vTip            V_ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
    vNom            V_ELEMENTI.NOME_ELEMENTO%TYPE;
    vGst            V_ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE;
    vData           DATE;
    vValore         NUMBER;
    vTipoFonte      TIPO_FONTI.COD_TIPO_FONTE%TYPE;
    vnum            INTEGER := 0;
    vinter number;
    v_outfile  UTL_FILE.FILE_TYPE;
    v_amount   INTEGER := 32767;
BEGIN
    tmdat := CURRENT_TIMESTAMP;
    IF pGstElem IS NOT NULL THEN
        pCodElem := PKG_elementi.GetCodElemento(pGstElem);
   END IF;

    SELECT COD_TIPO_ELEMENTO, NOME_ELEMENTO, COD_GEST_ELEMENTO
      INTO vTip, vNom, vGst
      FROM V_ELEMENTI
    WHERE COD_ELEMENTO = pCodElem;

    IF pGstElem IS NOT NULL THEN
        DBMS_OUTPUT.PUT_LINE ('PKG_Misure.GetMisureCG  ' || vGst || ' - ' || vTip || ' - ' || vNom || ' - ' || pCodElem);
        PKG_Misure.GetMisureCG (vLst, pGstElem, pDataDa, pDataA, pTipiMisura, pOrganizzazione, pStatoRete, pTipologiaRete, pFonte,pTipoProd, vAggr);
    ELSE
        DBMS_OUTPUT.PUT_LINE ('PKG_Misure.GetMisure    ' || vGst || ' - ' || vTip || ' - ' || vNom || ' - ' || pCodElem);
        PKG_Misure.GetMisure (vLst, pCodElem, pDataDa, pDataA, pTipiMisura, pOrganizzazione, pStatoRete, pTipologiaRete, pFonte,pTipoProd, vAggr);
    END IF;
    --v_outfile := UTL_FILE.FOPEN('MAGO_LOGDIR', vGst||'-'||REPLACE(vNom,' ','_')||'.csv', 'A', v_amount);
    DBMS_OUTPUT.PUT_LINE (TO_CHAR (CURRENT_TIMESTAMP - tmdat));
    LOOP
        FETCH vLst INTO vCodMis, vData, vValore, vTipoFonte, vinter;
        EXIT WHEN vLst%NOTFOUND;
        --UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);
        DBMS_OUTPUT.PUT_LINE(vCodMis||' - '||TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||' - '||vTipoFonte|| ' - '||vValore|| ' - Interpolato: '||vinter);
        vNum := vNum + 1;
    END LOOP;
    CLOSE vLst;
    --UTL_FILE.FCLOSE(v_outfile );
    
    DBMS_OUTPUT.PUT_LINE (TO_CHAR (CURRENT_TIMESTAMP - tmdat) || '    Linee lette: ' || vNum);
END;
