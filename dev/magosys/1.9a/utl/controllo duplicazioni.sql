SELECT TAB, NULL, NUM
  FROM(SELECT '1. GERARCHIA_IMP_SN' TAB, COUNT(*) NUM
          FROM GERARCHIA_IMP_SN
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. GERARCHIA_AMM' TAB, COUNT(*) NUM
          FROM GERARCHIA_AMM
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. GERARCHIA_GEO' TAB, COUNT(*) NUM
          FROM GERARCHIA_GEO
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. MISURE_ACQUISITE_STATICHE' TAB, COUNT(*) NUM
          FROM MISURE_ACQUISITE_STATICHE
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. MISURE_AGGREGATE_STATICHE' TAB, COUNT(*) NUM
          FROM MISURE_AGGREGATE_STATICHE
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. REL_ELEMENTI_ECP_SA' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_ECP_SA
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. REL_ELEMENTI_ECP_SN' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_ECP_SN
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. REL_ELEMENTI_ECS_SA' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_ECS_SA
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. REL_ELEMENTI_ECS_SN' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_ECS_SN
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. REL_ELEMENTI_AMM' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_AMM
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE
        UNION ALL
        SELECT '1. GERARCHIA_GEO' TAB, COUNT(*) NUM
          FROM REL_ELEMENTI_GEO
         WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE)
 WHERE NUM > 0
UNION ALL
  SELECT '2. GERARCHIA_IMP_SA' TAB, TO_CHAR(COD_ELEMENTO) KEY, COUNT(*) NUM
    FROM GERARCHIA_IMP_SA
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. GERARCHIA_IMP_SN' TAB, TO_CHAR(COD_ELEMENTO) KEY, COUNT(*) NUM
    FROM GERARCHIA_IMP_SN
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. GERARCHIA_AMM' TAB, TO_CHAR(COD_ELEMENTO) KEY, COUNT(*) NUM
    FROM GERARCHIA_AMM
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. GERARCHIA_GEO' TAB, TO_CHAR(COD_ELEMENTO) KEY, COUNT(*) NUM
    FROM GERARCHIA_GEO
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. MISURE_ACQUISITE_STATICHE' TAB, TO_CHAR(COD_TRATTAMENTO_ELEM) KEY, COUNT(*) NUM
    FROM MISURE_ACQUISITE_STATICHE
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_TRATTAMENTO_ELEM
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. MISURE_AGGREGATE_STATICHE' TAB, TO_CHAR(COD_TRATTAMENTO_ELEM) KEY, COUNT(*) NUM
    FROM MISURE_AGGREGATE_STATICHE
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_TRATTAMENTO_ELEM
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_ECP_SA' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_ECP_SA
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_ECP_SN' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_ECP_SN
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_ECS_SA' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_ECS_SA
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_ECS_SN' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_ECS_SN
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_AMM' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_AMM
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1
UNION ALL
  SELECT '2. REL_ELEMENTI_GEO' TAB, TO_CHAR(COD_ELEMENTO_PADRE)||'/'||TO_CHAR(COD_ELEMENTO_FIGLIO) KEY, COUNT(*) NUM
    FROM REL_ELEMENTI_GEO
   WHERE SYSDATE BETWEEN DATA_DISATTIVAZIONE AND DATA_ATTIVAZIONE
GROUP BY COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
  HAVING COUNT(*) > 1;  
  
---------------------------------


--DELETE GERARCHIA_IMP_SA WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--COMMIT;
--DELETE GERARCHIA_IMP_SN WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--COMMIT;
--DELETE GERARCHIA_AMM WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE GERARCHIA_GEO WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE MISURE_ACQUISITE_STATICHE WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE MISURE_AGGREGATE_STATICHE WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_ECP_SA WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_ECP_SN WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_ECS_SA WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_ECS_SN WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_AMM WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--DELETE REL_ELEMENTI_GEO WHERE DATA_DISATTIVAZIONE <= DATA_ATTIVAZIONE ;
--COMMIT;