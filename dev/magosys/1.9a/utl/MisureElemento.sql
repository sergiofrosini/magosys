DEFINE datafrom = "&1"
DEFINE datato = "&2"
DEFINE mis = "&3"
DEFINE cg = "&4"
DEFINE flag_aggr = &5

SET verify off
SET FEEDBACK off

COLUMN filename new_value v_filename

SELECT 'Misure' || '&MIS' || '_' || '&CG' || '_' || '&DATAFROM' || '_' || '&DATATO' || '.mis' filename
  FROM dual;
  
SPOOL &v_filename

select el.cod_gest_elemento, tr.cod_tipo_misura, to_char(mis.data,'dd/mm/yyyy hh24.mi.ss') data, mis.valore 
  from misure_acquisite mis
      ,trattamento_elementi tr
      ,elementi el
 where tr.cod_elemento = el.cod_elemento
   and tr.cod_trattamento_elem = mis.cod_trattamento_elem
   and mis.data >= trunc(to_date('&datafrom','yyyymmdd'))
   and mis.data < trunc(to_date('&datato','yyyymmdd'))
   and el.cod_gest_elemento = '&cg'
   and tr.cod_tipo_misura = '&mis' 
   and 0 = &flag_aggr
 UNION ALL
select el.cod_gest_elemento, tr.cod_tipo_misura, to_char(mis.data,'dd/mm/yyyy hh24.mi.ss'), mis.valore   from misure_aggregate mis
      ,trattamento_elementi tr
      ,elementi el
 where tr.cod_elemento = el.cod_elemento
   and tr.cod_trattamento_elem = mis.cod_trattamento_elem
   and mis.data >= trunc(to_date('&datafrom','yyyymmdd'))
   and mis.data < trunc(to_date('&datato','yyyymmdd'))
   and el.cod_gest_elemento = '&cg'
   and tr.cod_tipo_misura = '&mis' 
   and 1 = &flag_aggr;

SPOOL OFF

EXIT
