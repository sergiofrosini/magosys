#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
TOUCH="/bin/touch"
DF="/bin/df"
GREP="/bin/grep"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"
SSH="/usr/bin/ssh"
SCP="/usr/bin/scp"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  $ECHO "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi

RUN_DIR=`${PWD}`

#
# Aggiunta dell'utente per applicazione MAGONAZ
#
${MKDIR} /home/magonazsys
useradd -u 10325 -c "Utente applicazione MAGONAZ" -d /home/magonazsys -g dba -G nobody -s /bin/bash magonazsys  
# Creazione file .bashrc che definisce le variabili globali ORACLE_HOME e ORACLE_SID
${TOUCH} /home/magonazsys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magonazsys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magonazsys/.bashrc 
${ECHO} "cd /usr/NEW/magonazsys" >> /home/magonazsys/.bashrc
${CHOWN} -R magonazsys:dba /home/magonazsys
${CHMOD} 644 /home/magonazsys/.bashrc

path=`$PWD`
#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magonazsys/magonazsys
${CHOWN} -R magonazsys:dba /home/magonazsys/magonazsys
cd /home/magonazsys/magonazsys 
${LN} /usr/NEW/magonazsys Runtime
cd $path 

#
# Impostazione password
#
$ECHO "Impostazione password per utente magonazsys"
${PASWD} magonazsys


#
# Installazione ambiente Trasferimento misure per MAGONAZ 
#
${MKDIR} /usr/NEW
${CP} ambiente_magonazsys.tar.gz /usr/NEW
cd /usr/NEW
${GUNZIP} ambiente_magonazsys.tar.gz|${TAR} xvfp -
${RM} ambiente_magonazsys.tar*

#
# Chown sulle dir di magonazsys
#
${CHOWN} -R magonazsys:dba magonazsys
cd /usr/NEW/magonazsys
${CHMOD} 775 backup bin lib corrupted fromST fileMIS export install log tmp utl

#
# Attivazione scheduling processi MAGONAZ 
#
crontab -u magonazsys -r
crontab -u magonazsys $path/magonazsys_crontab 


