#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
TOUCH="/bin/touch"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  ${ECHO} "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi

PWD="/bin/pwd"

#
# aggiunta dell'utente per applicazione Trasferimento misure per MAGOnaz
#
${MKDIR} /home/magonazsys
useradd -u 10325 -c "Utente Gestione misure per Mon. Active Distr. Grid Op" -d /home/magonazsys -g dba -G nobody -s /bin/bash magonazsys  
${TOUCH} /home/magonazsys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magonazsys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magonazsys/.bashrc
${ECHO} "cd /usr/NEW/magonazsys" >> /home/magonazsys/.bashrc
${CHMOD} 644 /home/magonazsys/.bashrc
${CHOWN} -R magonazsys:dba /home/magonazsys

#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magonazsys/magonazsys
${CHOWN} -R magonazsys:dba /home/magonazsys/magonazsys
cd /home/magonazsys/magonazsys
${LN} /usr/NEW/magonazsys Runtime

#
# Impostazione password
#
$ECHO " impostazione password per utente magonazsys"
${PASWD} magonazsys

${MKDIR} /usr/NEW
cd /usr/NEW
${LN} /oracle_bases/ARCDB2/dataload/magonazsys magonazsys

