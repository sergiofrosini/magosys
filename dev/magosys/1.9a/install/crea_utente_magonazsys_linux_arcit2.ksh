#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
TOUCH="/bin/touch"
DF="/bin/df"
GREP="/bin/grep"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"
SSH="/usr/bin/ssh"
SCP="/usr/bin/scp"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  $ECHO "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi
# Controllo se ARCDBx e' montato
IS_MOUNTED_DB1=`${DF}|${GREP} ARCDB2 |${AWK} -F/ '{print $3}'`
if [ x${IS_MOUNTED_DB1} != "xARCDB2" ];then
  ${ECHO} "   ATTENZIONE eseguire il mount di /oracle_bases/ARCDB2"
  ${ECHO} "              eseguendo lo spostamento di pkg_ARCDB2"
  exit 1
fi

RUN_DIR=`${PWD}`

#
# Aggiunta dell'utente per applicazione MAGOnaz 
#
${MKDIR} /home/magonazsys
useradd -u 10325 -c "Utente Gestione misure per Mon. Active Distr. Grid Op" -d /home/magonazsys -g dba -G nobody -s /bin/bash magonazsys  
# Creazione file .bashrc che definisce le variabili globali ORACLE_HOME e ORACLE_SID
${TOUCH} /home/magonazsys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magonazsys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magonazsys/.bashrc 
${ECHO} "cd /usr/NEW/magonazsys" >> /home/magonazsys/.bashrc
${CHOWN} -R magonazsys:dba /home/magonazsys
${CHMOD} 644 /home/magonazsys/.bashrc

path=`$PWD`
#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magonazsys/magonazsys
${CHOWN} -R magonazsys:dba /home/magonazsys/magonazsys
cd /home/magonazsys/magonazsys 
${LN} /usr/NEW/magonazsys Runtime
cd $path 

#
# Impostazione password
#
$ECHO "Impostazione password per utente magonazsys"
${PASWD} magonazsys


#
# Installazione ambiente Trasferimento misure per MAGONAZ 
#
${MKDIR} /oracle_bases/ARCDB2/dataload 
${CHMOD} 775 /oracle_bases/ARCDB2/dataload
${CP} ambiente_magonazsys.tar.gz /oracle_bases/ARCDB2/dataload 
cd /oracle_bases/ARCDB2/dataload 
${GUNZIP} ambiente_magonazsys.tar.gz|${TAR} xvfp -
${RM} ambiente_magonazsys.tar.gz

#
# Chown sulle dir di magonazsys
#
cd /oracle_bases/ARCDB2/dataload 
${CHOWN} -R magonazsys:dba magonazsys
cd /oracle_bases/ARCDB2/dataload/magonazsys
${CHMOD} -R  775 backup bin lib corrupted fromST fromGME install log tmp utl
${MKDIR} /usr/NEW
cd /usr/NEW
${LN} /oracle_bases/ARCDB2/dataload/magonazsys magonazsys

#
# Attivazione scheduling processi MAGOnaz 
# da sistemare con script di cluster
# crontab -u magonazsys -r
# crontab -u magonazsys $path/magonazsys_crontab 

#
# Creazione utente su arcit1 
#
cd ${RUN_DIR}
${ECHO} "Creazione utente su Arcit1"
${ECHO} "Inserire password di root"
${SCP}  $path/crea_utente_magonazsys_linux_arcit1.ksh ArcIt1:/tmp
${ECHO} "Inserire password di root"
${SSH} ArcIt1 /tmp/crea_utente_magonazsys_linux_arcit1.ksh

