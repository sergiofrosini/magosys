#!/usr/bin/csh

if ( $# != 3 ) then
   echo "Inserire Password utente SYS"
   echo "Inserire tipologia di lancio        ( create_MAGO_DGF, rebuild_MAGO_DGF ) "
   echo "Inserire utente DGF "
   echo ""
   echo "usage : ./BUILD_MAGODGF [ sys_password ] [ create_MAGO_DGF | rebuild_MAGO_DGF ] "
   echo ""
else
   cd ../src
   switch($2)
     case create_MAGO_DGF :
         setenv SPOOL ../install/create_MAGO_DGF.log
         breaksw
      case rebuild_MAGO_DGF :
         setenv SPOOL ../install/rebuild_MAGO_DGF.log
         breaksw
      case * :
           echo ""
           echo "usage : ./BUILD_MAGODGF [ create_MAGO_DGF | rebuild_MAGO_DGF ] [UTENTE DGF]"
           echo ""
           exit
   endsw
   set OraInst = ""
   $ORACLE_HOME/bin/sqlplus -s sys/$1 as sysdba @../src/test_oracle_edition.sql > $SPOOL
   set esito = $?
   if ( $esito == 10 ) then
      set OraInst = "ENT"
   else if ( $esito == 20 ) then
      set OraInst = "STD"
   else
      echo ""
      echo ">>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!">>$SPOOL
      echo "\a\a>>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!"
      echo ""
      exit
   endif
   echo "Esecuzione in corso - attendere prego ..."
   switch($2)
      case create_MAGO_DGF :
         #$ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./create_mago.sql>>$SPOOL
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_mago_DGF.sql $1 $2 $3>>$SPOOL
         breaksw
      case rebuild_MAGO_DGF :
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_mago_DGF.sql $1 $2 $3>>$SPOOL
         breaksw
      case nothing :
         breaksw
      case * :
         echo "usage : ./BUILD_MAGODGF [ create_MAGO_DGF | rebuild_MAGO_DGF ] [UTENTE DGF]"
         exit
   endsw
   echo ""
   echo ""
#  grep "ERROR" $SPOOL 
   echo "--------------------------------------------------------------------------"
   grep -E '(ERROR|unable to open file)' $SPOOL 
   if ( $? == 0 ) then
      echo "Riscontrati probabili errori, consultare file di log $SPOOL"
   else
      echo "Elaborazione terminata regolarmente"
      echo "--------------------------------------------------------------------------"
      echo "eseguire ./definisci_CO_DGF.sh per la definizione del CO di default"
      echo "eseguire lo script SQL /usr/NEW/magosys/src/Conf/meteo_valcfg_insert_LOC_DGF.sql"
   endif
   echo "--------------------------------------------------------------------------"
   echo ""
   echo ""
endif


