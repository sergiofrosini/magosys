PROMPT TYPES T_MISMETEO_OBJ/ARRAY

create or replace
TYPE        T_MISMETEO_OBJ                                          AS OBJECT      
      (COD_ELEMENTO NUMBER
       ,COD_TIPO_MISURA  VARCHAR2(6)
       ,DATA             DATE
       ,VALORE           NUMBER
       ,COD_TIPO_FONTE   VARCHAR2(2)
      )
/
