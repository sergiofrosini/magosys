--------------------------------------------------------
--  DDL for Type T_TRTMISLVPROFILE_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_TRTMISLVPROFILE_OBJ as object
  (
    STAGIONE   NUMBER,
    GIORNO     NUMBER,
    CAMPIONE   NUMBER,
    CTYPE      VARCHAR2(3 BYTE),
    VALORE     NUMBER(20,9)
  )
/

--------------------------------------------------------
--  DDL for Type T_T_TRTMISLVPROFILE_ARRAY
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_TRTMISLVPROFILE_ARRAY as table of T_TRTMISLVPROFILE_OBJ
/

