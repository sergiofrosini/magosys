--------------------------------------------------------
Prompt  Type T_FILE_INFO_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_FILE_INFO_OBJ as object
  (
    ID_FILE        NUMBER,
    NOME_FILE      VARCHAR2(200),
    TIPO_FILE      VARCHAR2(200),
    DATA_ELAB      DATE,
    PROCESSO       VARCHAR2(500) 
  );

/

