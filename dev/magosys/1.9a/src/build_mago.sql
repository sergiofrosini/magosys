
spool MAGO_1.9_src.log

PROMPT _______________________________________________________________________________________
PROMPT creazione strutture oracle per MAGO
PROMPT

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sysdba as sysdba

COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;
SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;

@./00_tablespace_mago.sql
@./01_user_mago.sql sys_dba MAGO
@./02_grant_mago.sql

connect mago/mago

@./src/03_synonym_mago.sql

@./04_mago_all.sql MAGO &ORATYPE LOC 

PROMPT
PROMPT
PROMPT fine creazione strutture oracle per MAGO
PROMPT

spool off

