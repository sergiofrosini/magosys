Prompt Table GERARCHIA_IMP_SA;
--
-- GERARCHIA_IMP_SA  (Table) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE TABLE GERARCHIA_IMP_SA
(
  COD_ELEMENTO         NUMBER                   NOT NULL,
  DATA_ATTIVAZIONE     DATE                     NOT NULL,
  DATA_DISATTIVAZIONE  DATE                     NOT NULL,
  L01                  NUMBER                   NOT NULL,
  L02                  NUMBER,
  L03                  NUMBER,
  L04                  NUMBER,
  L05                  NUMBER,
  L06                  NUMBER,
  L07                  NUMBER,
  L08                  NUMBER,
  L09                  NUMBER,
  L10                  NUMBER,
  L11                  NUMBER,
  L12                  NUMBER,
  L13                  NUMBER,
  data_disattivazione_undisconn DATE
)
TABLESPACE &TBS&DAT
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.COD_ELEMENTO IS 'Codice dell''elemento'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.DATA_ATTIVAZIONE IS 'data di inizio validita'''
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.DATA_DISATTIVAZIONE IS 'data di fine validita'''
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L01 IS 'livello geararchico 1'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L02 IS 'livello geararchico 2'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L03 IS 'livello geararchico 3'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L04 IS 'livello geararchico 4'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L05 IS 'livello geararchico 5'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L06 IS 'livello geararchico 6'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L07 IS 'livello geararchico 7'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L08 IS 'livello geararchico 8'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L09 IS 'livello geararchico 9'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L10 IS 'livello geararchico 10'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L11 IS 'livello geararchico 11'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L12 IS 'livello geararchico 12'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.L13 IS 'livello geararchico 13'
/

COMMENT ON COLUMN GERARCHIA_IMP_SA.data_disattivazione_undisconn is 'Data di disattivazione senza buchi di inattivita'''
/

Prompt Index GERARCHIA_IMP_SA_PK;
--
-- GERARCHIA_IMP_SA_PK  (Index) 
--
--  Dependencies: 
--   GERARCHIA_IMP_SA (Table)
--
CREATE UNIQUE INDEX GERARCHIA_IMP_SA_PK ON GERARCHIA_IMP_SA
(COD_ELEMENTO, DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


Prompt Index GERARCHIA_IMP_SA_SN_DT1;
--
-- GERARCHIA_IMP_SA_SN_DT1  (Index) 
--
--  Dependencies: 
--   GERARCHIA_IMP_SA (Table)
--
CREATE INDEX GERARCHIA_IMP_SA_SN_DT1 ON GERARCHIA_IMP_SA
(DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


-- 
-- Non Foreign Key Constraints for Table GERARCHIA_IMP_SA 
-- 
Prompt Non-Foreign Key Constraints on Table GERARCHIA_IMP_SA;
ALTER TABLE GERARCHIA_IMP_SA ADD (
  CONSTRAINT GERARCHIA_IMP_SA_PK
  PRIMARY KEY
  (COD_ELEMENTO, DATA_ATTIVAZIONE)
  USING INDEX GERARCHIA_IMP_SA_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table GERARCHIA_IMP_SA 
-- 
Prompt Foreign Key Constraints on Table GERARCHIA_IMP_SA;
ALTER TABLE GERARCHIA_IMP_SA ADD (
  CONSTRAINT ELE_GERIMPSA 
  FOREIGN KEY (COD_ELEMENTO) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/
