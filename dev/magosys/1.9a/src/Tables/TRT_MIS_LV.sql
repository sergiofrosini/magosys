--------------------------------------------------------
--  File creato - venerd�-agosto-02-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TRT_MIS_LV
--------------------------------------------------------

CREATE TABLE  TRT_MIS_LV
  (
    COLNUMBER           NUMBER,
    AUINUMBER           NUMBER,
    TRANSFNUMBERNODE    NUMBER,
    ACTTRANSFNUMBERNODE NUMBER,
    AUICODE             VARCHAR2(20),
    VAL_POTENZA         NUMBER(20,9),
    VAL_CLIENTI         NUMBER(20,9),
    CTYPE_INDEX         NUMBER(2,0)
  ) ;