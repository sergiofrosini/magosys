Prompt Table GERARCHIA_AMM;
--
-- GERARCHIA_AMM  (Table) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE TABLE GERARCHIA_AMM
(
  COD_ELEMENTO         NUMBER                   NOT NULL,
  DATA_ATTIVAZIONE     DATE                     NOT NULL,
  DATA_DISATTIVAZIONE  DATE                     NOT NULL,
  L01                  NUMBER                   NOT NULL,
  L02                  NUMBER,
  L03                  NUMBER,
  L04                  NUMBER,
  L05                  NUMBER,
  L06                  NUMBER,
  L07                  NUMBER,
  L08                  NUMBER,
  L09                  NUMBER,
  L10                  NUMBER,
  L11                  NUMBER,
  L12                  NUMBER,
  L13                  NUMBER,
  data_disattivazione_undisconn date
)
TABLESPACE &TBS&DAT
/

COMMENT ON COLUMN GERARCHIA_AMM.COD_ELEMENTO IS 'Codice dell''elemento'
/

COMMENT ON COLUMN GERARCHIA_AMM.DATA_ATTIVAZIONE IS 'data di inizio validita'''
/

COMMENT ON COLUMN GERARCHIA_AMM.DATA_DISATTIVAZIONE IS 'data di fine validita'''
/

COMMENT ON COLUMN GERARCHIA_AMM.L01 IS 'livello geararchico 1'
/

COMMENT ON COLUMN GERARCHIA_AMM.L02 IS 'livello geararchico 2'
/

COMMENT ON COLUMN GERARCHIA_AMM.L03 IS 'livello geararchico 3'
/

COMMENT ON COLUMN GERARCHIA_AMM.L04 IS 'livello geararchico 4'
/

COMMENT ON COLUMN GERARCHIA_AMM.L05 IS 'livello geararchico 5'
/

COMMENT ON COLUMN GERARCHIA_AMM.L06 IS 'livello geararchico 6'
/

COMMENT ON COLUMN GERARCHIA_AMM.L07 IS 'livello geararchico 7'
/

COMMENT ON COLUMN GERARCHIA_AMM.L08 IS 'livello geararchico 8'
/

COMMENT ON COLUMN GERARCHIA_AMM.L09 IS 'livello geararchico 9'
/

COMMENT ON COLUMN GERARCHIA_AMM.L10 IS 'livello geararchico 10'
/

COMMENT ON COLUMN GERARCHIA_AMM.L11 IS 'livello geararchico 11'
/

COMMENT ON COLUMN GERARCHIA_AMM.L12 IS 'livello geararchico 12'
/

COMMENT ON COLUMN GERARCHIA_AMM.L13 IS 'livello geararchico 13'
/

COMMENT ON COLUMN GERARCHIA_AMM.data_disattivazione_undisconn is 'Data di disattivazione senza buchi di inattivita'''
/
Prompt Index GERARCHIA_AMM_PK;
--
-- GERARCHIA_AMM_PK  (Index) 
--
--  Dependencies: 
--   GERARCHIA_AMM (Table)
--
CREATE UNIQUE INDEX GERARCHIA_AMM_PK ON GERARCHIA_AMM
(COD_ELEMENTO, DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


Prompt Index GERARCHIA_AMM_SN_DT1;
--
-- GERARCHIA_AMM_SN_DT1  (Index) 
--
--  Dependencies: 
--   GERARCHIA_AMM (Table)
--
CREATE INDEX GERARCHIA_AMM_SN_DT1 ON GERARCHIA_AMM
(DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


-- 
-- Non Foreign Key Constraints for Table GERARCHIA_AMM 
-- 
Prompt Non-Foreign Key Constraints on Table GERARCHIA_AMM;
ALTER TABLE GERARCHIA_AMM ADD (
  CONSTRAINT GERARCHIA_AMM_PK
  PRIMARY KEY
  (COD_ELEMENTO, DATA_ATTIVAZIONE)
  USING INDEX GERARCHIA_AMM_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table GERARCHIA_AMM 
-- 
Prompt Foreign Key Constraints on Table GERARCHIA_AMM;
ALTER TABLE GERARCHIA_AMM ADD (
  CONSTRAINT ELE_GERAMM 
  FOREIGN KEY (COD_ELEMENTO) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/
