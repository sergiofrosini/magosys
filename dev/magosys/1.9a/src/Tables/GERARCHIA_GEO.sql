Prompt Table GERARCHIA_GEO;
--
-- GERARCHIA_GEO  (Table) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE TABLE GERARCHIA_GEO
(
  COD_ELEMENTO         NUMBER                   NOT NULL,
  DATA_ATTIVAZIONE     DATE                     NOT NULL,
  DATA_DISATTIVAZIONE  DATE                     NOT NULL,
  L01                  NUMBER                   NOT NULL,
  L02                  NUMBER,
  L03                  NUMBER,
  L04                  NUMBER,
  L05                  NUMBER,
  L06                  NUMBER,
  L07                  NUMBER,
  L08                  NUMBER,
  L09                  NUMBER,
  L10                  NUMBER,
  L11                  NUMBER,
  L12                  NUMBER,
  L13                  NUMBER,
  data_disattivazione_undisconn DATE
)
TABLESPACE &TBS&DAT
/

COMMENT ON COLUMN GERARCHIA_GEO.COD_ELEMENTO IS 'Codice dell''elemento'
/

COMMENT ON COLUMN GERARCHIA_GEO.DATA_ATTIVAZIONE IS 'data di inizio validita'''
/

COMMENT ON COLUMN GERARCHIA_GEO.DATA_DISATTIVAZIONE IS 'data di fine validita'''
/

COMMENT ON COLUMN GERARCHIA_GEO.L01 IS 'livello geararchico 1'
/

COMMENT ON COLUMN GERARCHIA_GEO.L02 IS 'livello geararchico 2'
/

COMMENT ON COLUMN GERARCHIA_GEO.L03 IS 'livello geararchico 3'
/

COMMENT ON COLUMN GERARCHIA_GEO.L04 IS 'livello geararchico 4'
/

COMMENT ON COLUMN GERARCHIA_GEO.L05 IS 'livello geararchico 5'
/

COMMENT ON COLUMN GERARCHIA_GEO.L06 IS 'livello geararchico 6'
/

COMMENT ON COLUMN GERARCHIA_GEO.L07 IS 'livello geararchico 7'
/

COMMENT ON COLUMN GERARCHIA_GEO.L08 IS 'livello geararchico 8'
/

COMMENT ON COLUMN GERARCHIA_GEO.L09 IS 'livello geararchico 9'
/

COMMENT ON COLUMN GERARCHIA_GEO.L10 IS 'livello geararchico 10'
/

COMMENT ON COLUMN GERARCHIA_GEO.L11 IS 'livello geararchico 11'
/

COMMENT ON COLUMN GERARCHIA_GEO.L12 IS 'livello geararchico 12'
/

COMMENT ON COLUMN GERARCHIA_GEO.L13 IS 'livello geararchico 13'
/

COMMENT ON COLUMN GERARCHIA_GEO.data_disattivazione_undisconn is 'Data di disattivazione senza buchi di inattivita'''
/

Prompt Index GERARCHIA_GEO_PK;
--
-- GERARCHIA_GEO_PK  (Index) 
--
--  Dependencies: 
--   GERARCHIA_GEO (Table)
--
CREATE UNIQUE INDEX GERARCHIA_GEO_PK ON GERARCHIA_GEO
(COD_ELEMENTO, DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


Prompt Index GERARCHIA_GEO_SN_DT1;
--
-- GERARCHIA_GEO_SN_DT1  (Index) 
--
--  Dependencies: 
--   GERARCHIA_GEO (Table)
--
CREATE INDEX GERARCHIA_GEO_SN_DT1 ON GERARCHIA_GEO
(DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


-- 
-- Non Foreign Key Constraints for Table GERARCHIA_GEO 
-- 
Prompt Non-Foreign Key Constraints on Table GERARCHIA_GEO;
ALTER TABLE GERARCHIA_GEO ADD (
  CONSTRAINT GERARCHIA_GEO_PK
  PRIMARY KEY
  (COD_ELEMENTO, DATA_ATTIVAZIONE)
  USING INDEX GERARCHIA_GEO_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table GERARCHIA_GEO 
-- 
Prompt Foreign Key Constraints on Table GERARCHIA_GEO;
ALTER TABLE GERARCHIA_GEO ADD (
  CONSTRAINT ELE_GERGEO 
  FOREIGN KEY (COD_ELEMENTO) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/
