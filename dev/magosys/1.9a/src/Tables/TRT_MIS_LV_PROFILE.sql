--------------------------------------------------------
--  DDL for Table TRT_MIS_LV_PROFILE
--------------------------------------------------------

  CREATE TABLE TRT_MIS_LV_PROFILE 
   (	STAGIONE     NUMBER, 
	    GIORNO       NUMBER, 
	    CAMPIONE     NUMBER, 
      CTYPE        VARCHAR2(3),
	    VALORE       NUMBER(20,9)
   );
--------------------------------------------------------
--  Constraints for Table TRT_MIS_LV_PROFILE
--------------------------------------------------------

  ALTER TABLE TRT_MIS_LV_PROFILE MODIFY (STAGIONE NOT NULL ENABLE);

 
  ALTER TABLE TRT_MIS_LV_PROFILE MODIFY (GIORNO NOT NULL ENABLE);

 
  ALTER TABLE TRT_MIS_LV_PROFILE MODIFY (CAMPIONE NOT NULL ENABLE);

  
  ALTER TABLE TRT_MIS_LV_PROFILE MODIFY (CTYPE NOT NULL ENABLE);
