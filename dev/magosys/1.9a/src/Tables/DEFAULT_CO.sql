Prompt Table DEFAULT_CO;
--
-- DEFAULT_CO  (Table) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE TABLE DEFAULT_CO
(
  COD_ELEMENTO_CO   NUMBER,
  COD_ELEMENTO_ESE  NUMBER,
  TIPO_INST         NUMBER(3)                   NOT NULL,
  FLAG_PRIMARIO     NUMBER(1)                   DEFAULT 0,
  START_DATE        DATE
)
TABLESPACE &TBS&DAT
/

COMMENT ON TABLE DEFAULT_CO IS 'Defaults Applicativi'
/

COMMENT ON COLUMN DEFAULT_CO.COD_ELEMENTO_CO IS 'Centro operarivo'
/

COMMENT ON COLUMN DEFAULT_CO.COD_ELEMENTO_ESE IS 'Esercizio'
/

COMMENT ON COLUMN DEFAULT_CO.TIPO_INST IS 'Utilizzare la logica a Bit: 1=Nazionale, 2=STM, 3=STMnaz, 4=DGF, 5=DGFnaz'
/

COMMENT ON COLUMN DEFAULT_CO.FLAG_PRIMARIO IS '1=Esercizio Primario - Valorizzare anche se presente un solo esercizio'
/

COMMENT ON COLUMN DEFAULT_CO.START_DATE IS 'Data prima di riverimento del promo caricamente anagrafiche'
/



-- 
-- Non Foreign Key Constraints for Table DEFAULT_CO 
-- 
Prompt Non-Foreign Key Constraints on Table DEFAULT_CO;
ALTER TABLE DEFAULT_CO ADD (
  CONSTRAINT FLAG_PRIMARIO_CHK
  CHECK (FLAG_PRIMARIO IN (0,1))
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table DEFAULT_CO 
-- 
Prompt Foreign Key Constraints on Table DEFAULT_CO;
ALTER TABLE DEFAULT_CO ADD (
  CONSTRAINT ELE_DEFCO 
  FOREIGN KEY (COD_ELEMENTO_CO) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/

ALTER TABLE DEFAULT_CO ADD (
  CONSTRAINT ELE_DEFESE 
  FOREIGN KEY (COD_ELEMENTO_ESE) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/
