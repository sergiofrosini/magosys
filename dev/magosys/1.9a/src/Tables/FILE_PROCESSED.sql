--------------------------------------------------------
--  File creato - venerd�-novembre-15-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table FILE_PROCESSED
--------------------------------------------------------

  CREATE TABLE FILE_PROCESSED 
   (ID_FILE NUMBER, 
    NOME_FILE VARCHAR2(200 BYTE), 
    TIPO_FILE VARCHAR2(200 BYTE), 
    DATA_ELAB DATE, 
    PROCESSO VARCHAR2(500 BYTE)
   ) ;
--------------------------------------------------------
--  DDL for Index FILE_PROCESSED_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX FILE_PROCESSED_PK ON FILE_PROCESSED (NOME_FILE, TIPO_FILE) 
  ;
--------------------------------------------------------
--  Constraints for Table FILE_PROCESSED
--------------------------------------------------------

  ALTER TABLE FILE_PROCESSED ADD CONSTRAINT FILE_PROCESSED_PK PRIMARY KEY (NOME_FILE, TIPO_FILE) ENABLE;
 
  ALTER TABLE FILE_PROCESSED MODIFY (NOME_FILE NOT NULL ENABLE);
 
  ALTER TABLE FILE_PROCESSED MODIFY (TIPO_FILE NOT NULL ENABLE);
 
  ALTER TABLE FILE_PROCESSED MODIFY (DATA_ELAB NOT NULL ENABLE);
 
  ALTER TABLE FILE_PROCESSED MODIFY (PROCESSO NOT NULL ENABLE);
