PROMPT PACKAGE BODY PKG_INTEGRST;
--
-- PKG_INTEGRST  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_INTEGRST AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.8.a.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureReq        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pMisReq         IN T_MISREQ_ARRAY,
                                 pDisconnect     IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce le misure relative agli elementi richiesti
-----------------------------------------------------------------------------------------------------------*/
    cKeyTmp     CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'REQ_ST';
    cKeyTmpMis  CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := cKeyTmp||'_MIS';
    vLista      PKG_UtlGlb.t_SplitTbl;
    vIdFonti    NUMBER      := 0;
    vTotTipFon  NUMBER(1)   := -1;
    vData       DATE;
    vMinData    DATE := PKG_UtlGlb.gkDataTappo;
    vMaxData    DATE := TO_DATE('01011900','ddmmyyyy');
    vNumOk      NUMBER := 0;
    vNumTot     NUMBER := 0;
    vInfo       VARCHAR2(4000);

 BEGIN

    PKG_Logs.ResetLogCommonArea;
    PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                        pFunzione     => 'PKG_IntegrST.GetMeasureReq',
                        pStoreOnFile  => FALSE);

    --PKG_Logs.TraceLog(vLog,'Inizio - GetMeasureReq',PKG_UtlGlb.gcTrace_VRB);

    IF pMisReq.LAST IS NULL THEN
        OPEN pRefCurs FOR SELECT NULL REQ_ID,     NULL COD_GEST_ELEMENTO, NULL COD_TIPO_MISURA_ST,
                                 NULL TIPI_FONTE, NULL DATA,              NULL VALORE, NULL STATUS
                            FROM DUAL;
        PKG_Logs.StdLogInfo('Array non valorizzato');
        PKG_Logs.StdLogPrint;
        --PKG_Logs.TraceLog(vLog,'Fine   - GetMeasureReq',PKG_UtlGlb.gcTrace_VRB);
        RETURN;
    END IF;

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP LIKE cKeyTmp||'%';

    FOR i IN pMisReq.FIRST .. pMisReq.LAST LOOP

        IF pMisReq(i).DATA_DA < vMinData THEN
            vMinData :=  pMisReq(i).DATA_DA;
        END IF;
        IF pMisReq(i).DATA_A > vMaxData THEN
            vMaxData :=  pMisReq(i).DATA_A;
        END IF;

        INSERT INTO GTTD_VALORI_TEMP (TIP,
                                      NUM1,
                                      ALF1,
                                      ALF2,
                                      ALF3,
                                      DAT1,
                                      DAT2)
                              VALUES (cKeyTmp,
                                      pMisReq(i).REQ_ID,
                                      pMisReq(i).COD_GEST_ELEMENTO,
                                      pMisReq(i).COD_TIPO_MISURA_ST,
                                      TRIM(pMisReq(i).TIPI_FONTE),
                                      pMisReq(i).DATA_DA,
                                      NVL(pMisReq(i).DATA_A,pMisReq(i).DATA_DA));
    END LOOP;

    PKG_Logs.ResetLogCommonArea;
    PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassBE,
                        pFunzione     => 'PKG_IntegrST.GetMeasureReq',
                        pStoreOnFile  => FALSE,
                        pDataRif      => vMinData,
                        pDataRif_fine => vMaxData);

    FOR i IN (SELECT COD_GEST_ELEMENTO, NVL(COD_TIPO_ELEMENTO,'non definito') COD_TIPO_ELEMENTO
                FROM (SELECT DISTINCT alf1 COD_GEST_ELEMENTO FROM GTTD_VALORI_TEMP WHERE TIP = cKeyTmp)
                LEFT OUTER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
               ORDER BY COD_TIPO_ELEMENTO NULLS LAST, COD_GEST_ELEMENTO
             ) LOOP
        PKG_Logs.StdLogAddTxt(i.COD_TIPO_ELEMENTO,i.COD_GEST_ELEMENTO,NULL);
    END LOOP;

    vInfo := RPAD('Fonti Richieste',20,' ')||': ';
    FOR i IN (SELECT ALF3 TipFon FROM GTTD_VALORI_TEMP WHERE TIP = cKeyTmp GROUP BY ALF3 ORDER BY 1) LOOP
        vInfo := SUBSTR(vInfo||NVL(i.TipFon,'<null>')||' - ',1,4000);
    END LOOP;
    PKG_Logs.StdLogAddTxt(vInfo,TRUE,NULL);

    vInfo := RPAD('TipMis. Richieste',20,' ')||': ';
    FOR i IN (SELECT ALF2 TipMis, COUNT(*) NUM FROM GTTD_VALORI_TEMP WHERE TIP = cKeyTmp GROUP BY ALF2 ORDER BY 1) LOOP
        vInfo := SUBSTR(vInfo||NVL(i.TipMis,'<null>')||'('||i.NUM||') - ',1,4000);
        PKG_Misure.gListaTipoMisura := PKG_Misure.gListaTipoMisura || '|' || i.TipMis || '|';
    END LOOP;
    PKG_Logs.StdLogAddTxt(vInfo,TRUE,NULL);

    vInfo := RPAD('TipEle. Richiesti',20,' ')||': ';
    FOR i IN (SELECT tip, COUNT(*) num
                FROM (SELECT COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO tip
                        FROM (SELECT DISTINCT alf1 COD_GEST_ELEMENTO, dat1, dat2 FROM GTTD_VALORI_TEMP)
                       INNER JOIN ELEMENTI E USING(COD_GEST_ELEMENTO)
                       INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                       WHERE (DATA_DISATTIVAZIONE >= DAT1 AND DATA_ATTIVAZIONE <= DAT2)
                       GROUP BY COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO
                     )
               GROUP BY TIP
             ) LOOP
        vInfo := SUBSTR(vInfo||NVL(i.tip,'<null>')||'='||i.num||' - ',1,4000);
        vNumOk := vNumOk + i.num;
    END LOOP;
    SELECT COUNT(*) INTO vNumTot FROM (SELECT DISTINCT ALF1 FROM GTTD_VALORI_TEMP);
    PKG_Logs.StdLogAddTxt(vInfo||'Err='||TO_CHAR(vNumTot - vNumOk),TRUE,NULL);

    -- Tipi Fonte - Calcola l'ID totale delle fonti richieste
    FOR i IN (SELECT DISTINCT ALF3 TIPI_FONTE FROM GTTD_VALORI_TEMP WHERE TIP = cKeyTmp) LOOP
        IF INSTR(i.TIPI_FONTE,'!') <> 0 THEN
            vIdFonti := PKG_Misure.GetIdTipoFonti(SUBSTR(i.TIPI_FONTE,3));
            CASE SUBSTR(i.TIPI_FONTE,1,1)
                WHEN '0' THEN vTotTipFon := PKG_Misure.cMisTot;
                WHEN '1' THEN vTotTipFon := PKG_Misure.cMisDettTot;
            END CASE;
        ELSE
            vIdFonti := PKG_Misure.GetIdTipoFonti(i.TIPI_FONTE);
            vTotTipFon := PKG_Misure.cMisDett;
        END IF;
        UPDATE GTTD_VALORI_TEMP SET NUM2 = NVL(vIdFonti,0),
                                    NUM3 = vTotTipFon
         WHERE NVL(ALF3,'<null>') = NVL(i.TIPI_FONTE,'<null>');
    END LOOP;

    -- Tipi Misura - Recupera la data per il calcolo dell'aggregata al volo
    FOR i IN (SELECT DISTINCT CODIFICA_ST, PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) MISURA_STATICA, COD_TIPO_MISURA
                FROM GTTD_VALORI_TEMP
               INNER JOIN TIPI_MISURA ON ALF2 IN (CODIFICA_ST,COD_TIPO_MISURA)
               WHERE TIP = cKeyTmp) LOOP
        vData := NULL;
        IF i.MISURA_STATICA = PKG_UtlGlb.gkFlagOFF THEN
            SELECT MIN(DATARIF)
              INTO vData
              FROM SCHEDULED_JOBS
             WHERE (    vMaxData  >= DATARIF
                    AND vMinData  <= DATARIF)
               AND ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
               AND STATO = PKG_Mago.gcStatoAttuale
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;
        UPDATE GTTD_VALORI_TEMP SET DAT3 = vData,
                                    NUM4 = i.MISURA_STATICA
         WHERE ALF2 = i.CODIFICA_ST;
    END LOOP;

    -- CURSORE ----------------------------------

    IF vTotTipFon = PKG_Misure.cMisDettTot THEN
        -- Dettaglio + Totale Fonti
        -- calcola prima il dettaglio e lo inserisce in GTTD e quindi calcola il totale dal dettaglio
        INSERT INTO GTTD_VALORI_TEMP (TIP,NUM1,ALF1,ALF2,ALF3,DAT1,NUM2,NUM3)
              SELECT DISTINCT
                     cKeyTmpMis,
                     A.REQ_ID, A.COD_GEST_ELEMENTO, A.COD_TIPO_MISURA_ST,
                     NVL(B.COD_TIPO_FONTE,' ') TIPI_FONTE, B.DATA, B.VALORE,
                     CASE
                        WHEN B.VALORE IS NULL THEN CASE WHEN STATUS = 0 THEN -2
                                                                        ELSE STATUS
                                                   END
                        ELSE STATUS
                     END STATUS
                FROM (SELECT E.COD_ELEMENTO,
                             G.NUM1 REQ_ID,
                             G.ALF1 COD_GEST_ELEMENTO,
                             NVL(T.CODIFICA_ST,T.COD_TIPO_MISURA) COD_TIPO_MISURA_ST,
                             T.COD_TIPO_MISURA COD_TIPO_MISURA,
                             G.ALF3 TIPI_FONTE,
                             G.NUM2 ID_FONTI,
                             PKG_Misure.cMisDett TOTALIZZAZIONE,
                             G.DAT1 DATA_DA,
                             G.DAT2 DATA_A,
                             T.RISOLUZIONE_FE RISOLUZIONE_FE,
                             G.DAT3 DATA_AL_VOLO,
                             CASE
                                WHEN E.COD_GEST_ELEMENTO IS NULL THEN -1 -- NOT FOUND
                                ELSE 0 -- OK
                             END STATUS
                        FROM GTTD_VALORI_TEMP   G
                       LEFT OUTER JOIN ELEMENTI E ON G.ALF1 = E.COD_GEST_ELEMENTO
                       INNER JOIN TIPI_MISURA   T ON G.ALF2 IN (T.CODIFICA_ST,T.COD_TIPO_MISURA)
                       WHERE TIP = cKeyTmp
                      ) A,
                      TABLE (PKG_Misure.GetMisureTabMis(COD_ELEMENTO,
                                                        COD_TIPO_MISURA,
                                                        ID_FONTI,
                                                        0,
                                                        0,
                                                        --PKG_UtlGlb.TruncDate(DATA_DA,RISOLUZIONE_FE),
                                                        --PKG_UtlGlb.TruncDate(DATA_A, RISOLUZIONE_FE),
                                                        DATA_DA,
                                                        DATA_A,
                                                        PKG_Mago.gcOrganizzazELE,
                                                        PKG_Mago.gcStatoAttuale,
                                                        TOTALIZZAZIONE,
                                                        RISOLUZIONE_FE,
                                                        DATA_AL_VOLO,
                                                        PKG_UtlGlb.gkFlagON,
                                                        pDisconnect
                                                       )
                           ) B
               WHERE A.COD_TIPO_MISURA = B.COD_TIPO_MISURA;
        -- restiruisce il corsore con totale + dettaglio
        OPEN pRefCurs FOR SELECT NUM1 REQ_ID, 
                                 ALF1 COD_GEST_ELEMENTO, 
                                 ALF2 COD_TIPO_MISURA_ST, 
                                 ALF3 TIPI_FONTE, 
                                 DAT1 DATA, 
                                 NUM2 VALORE, 
                                 NUM3 STATUS
                            FROM (SELECT NUM1,ALF1,ALF2,ALF3,DAT1,NUM2,NUM3
                                    FROM GTTD_VALORI_TEMP
                                   WHERE TIP = cKeyTmpMis
                                  UNION ALL
                                  SELECT NUM1,ALF1,ALF2,'0',DAT1,SUM(NUM2),NUM3
                                    FROM GTTD_VALORI_TEMP
                                   WHERE TIP = cKeyTmpMis
                                   GROUP BY NUM1,ALF1,ALF2,DAT1,NUM3
                                 )
                           ORDER BY NUM1, ALF1, ALF2,
                                    CASE ALF3
                                       WHEN '0' THEN 0
                                       ELSE          1
                                    END,
                                    DAT1,
                                    NUM3;
    ELSE
        -- Dettaglio o Totale Fonti
        OPEN pRefCurs FOR SELECT DISTINCT *
                            FROM (SELECT A.REQ_ID, A.COD_GEST_ELEMENTO, A.COD_TIPO_MISURA_ST,
                                         NVL(B.COD_TIPO_FONTE,' ') TIPI_FONTE, B.DATA, B.VALORE,
                                         CASE
                                            WHEN B.VALORE IS NULL THEN CASE WHEN STATUS = 0 THEN -2
                                                                                            ELSE STATUS
                                                                       END
                                            ELSE STATUS
                                         END STATUS
                                    FROM (SELECT E.COD_ELEMENTO,
                                                 G.NUM1 REQ_ID,
                                                 G.ALF1 COD_GEST_ELEMENTO,
                                                 NVL(T.CODIFICA_ST,T.COD_TIPO_MISURA) COD_TIPO_MISURA_ST,
                                                 T.COD_TIPO_MISURA COD_TIPO_MISURA,
                                                 G.ALF3 TIPI_FONTE,
                                                 G.NUM2 ID_FONTI,
                                                 G.NUM3  TOTALIZZAZIONE,
                                                 G.DAT1 DATA_DA,
                                                 G.DAT2 DATA_A,
                                                 T.RISOLUZIONE_FE RISOLUZIONE_FE,
                                                 G.DAT3 DATA_AL_VOLO,
                                                 CASE
                                                    WHEN E.COD_GEST_ELEMENTO IS NULL THEN -1 -- NOT FOUND
                                                    ELSE 0 -- OK
                                                 END STATUS
                                            FROM GTTD_VALORI_TEMP   G
                                           LEFT OUTER JOIN ELEMENTI E ON G.ALF1 = E.COD_GEST_ELEMENTO
                                           INNER JOIN TIPI_MISURA   T ON G.ALF2  IN (T.CODIFICA_ST,T.COD_TIPO_MISURA )
                                           WHERE TIP = cKeyTmp
                                          ) A,
                                          TABLE (PKG_Misure.GetMisureTabMis(COD_ELEMENTO,
                                                                            COD_TIPO_MISURA,
                                                                            ID_FONTI,
                                                                            0,
                                                                            0,
                                                                            --PKG_UtlGlb.TruncDate(DATA_DA,RISOLUZIONE_FE),
                                                                            --PKG_UtlGlb.TruncDate(DATA_A, RISOLUZIONE_FE),
                                                                            DATA_DA,
                                                                            DATA_A,
                                                                            PKG_Mago.gcOrganizzazELE,
                                                                            PKG_Mago.gcStatoAttuale,
                                                                            TOTALIZZAZIONE,
                                                                            RISOLUZIONE_FE,
                                                                            DATA_AL_VOLO,
                                                                            PKG_UtlGlb.gkFlagON,
                                                                            pDisconnect
                                                                           )
                                               ) B
                                   WHERE A.COD_TIPO_MISURA = B.COD_TIPO_MISURA(+)
                                     AND B.DATA IS NOT NULL
                                   ORDER BY  A.REQ_ID, A.COD_GEST_ELEMENTO, A.COD_TIPO_MISURA_ST,
                                             CASE B.COD_TIPO_FONTE
                                                WHEN '0' THEN 0
                                                ELSE          1
                                             END,
                                             B.DATA, B.COD_TIPO_FONTE
                                 );
                                             
    END IF;

    PKG_Logs.StdLogPrint;

    --PKG_Logs.TraceLog(vLog,'Fine   - GetMeasureReq',PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetMeasureReq;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassBE,
                          pFunzione    => 'PKG_IntegrST',
                          pStoreOnFile => FALSE);

END PKG_IntegrST;
/
SHOW ERRORS;


