Prompt Package Body PKG_MANUTENZIONE;
--
-- PKG_MANUTENZIONE  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY "PKG_MANUTENZIONE" AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.i
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */


/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaLogs AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce i log piu' vecchi
-----------------------------------------------------------------------------------------------------------*/
    vData DATE := (TRUNC(SYSDATE) - PKG_UtlGlb.GetParamGenNum('LOGDAYS',20));
BEGIN

    DELETE LOG_HISTORY_INFO WHERE RUN_ID IN (SELECT RUN_ID FROM LOG_HISTORY WHERE LOGDATE < vData);
    DELETE LOG_HISTORY WHERE LOGDATE < vData;
    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Manutenzione.PuliziaLogs'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END PuliziaLogs;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaMeteoDaily AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce le tabelle METEO_FILE_xxx
-----------------------------------------------------------------------------------------------------------*/
    vData DATE;
    vNum  INTEGER;
BEGIN

    SELECT TO_NUMBER(VALUE) INTO  vNum
      FROM METEO_JOB_STATIC_CONFIG WHERE KEY = 'MFM.local.mantaining.day';

    vData := TRUNC(SYSDATE) - vNum;

    DELETE METEO_FILE_XML WHERE CREATION_DATE < vData;
    DELETE METEO_FILE_ZIP WHERE CREATION_DATE < vData;
    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Manutenzione.PuliziaMeteoDaily'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END PuliziaMeteoDaily;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaGiornaliera AS
/*-----------------------------------------------------------------------------------------------------------
    Pulizia Giornaliera delle tabella applicative
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PuliziaLogs;
    PuliziaMeteoDaily;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Manutenzione.PuliziaGiornaliera'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END PuliziaGiornaliera;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Manutenzione',
                          pStoreOnFile => FALSE);

END PKG_Manutenzione;
/

SHOW ERRORS;


