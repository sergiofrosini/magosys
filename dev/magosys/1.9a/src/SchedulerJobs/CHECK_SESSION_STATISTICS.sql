PROMPT SCHEDULER JOB CHECK_SESSION_STATISTICS;
--
-- CHECK_SESSION_STATISTICS  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('CHECK_SESSION_STATISTICS', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'CHECK_SESSION_STATISTICS',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Stats.CheckSessions',
                 start_date           => TRUNC(SYSDATE),
                 --repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00,05,10,15,20,25,30,35,40,45,50,55',
                 repeat_interval      => 'FREQ=MINUTELY;INTERVAL=1',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Chiude le statistiche di sessione per le quali non sono piu'' presenti elaborazioni'
                );
END;
/

