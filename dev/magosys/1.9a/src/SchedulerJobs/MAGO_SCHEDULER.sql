PROMPT SCHEDULER JOB MAGO_SCHEDULER;
--
-- MAGO_SCHEDULER  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('MAGO_SCHEDULER', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'MAGO_SCHEDULER',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Scheduler.ElaboraJobs',
                 start_date           => TRUNC(SYSDATE),
                 --repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00,05,10,15,20,25,30,35,40,45,50,55',
                 repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00,10,20,30,40,50',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Esegue i jobs schedulati'
                );
END;
/

