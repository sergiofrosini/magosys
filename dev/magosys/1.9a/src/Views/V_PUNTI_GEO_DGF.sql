CREATE OR REPLACE VIEW v_punti_geo AS
SELECT cod_geo
      ,tipo_punto
      ,tipo_previsione
      ,coord_lat
      ,coord_long
      ,alt
      ,altezza_sul_terreno
  FROM 
      (
       SELECT 
              ROW_NUMBER() OVER ( PARTITION BY an.cod_geo, cod_tipo_fonte ORDER BY an.cod_geo) num
             ,an.cod_geo
             ,an.tipo_coord tipo_punto
             ,def.cod_tipo_fonte tipo_previsione
             ,TRIM(TO_CHAR(TRUNC(an.coordinata_x) ,'999') || ' ' 
              || TRUNC((an.coordinata_x - TRUNC(an.coordinata_x)) * 60) || ' ' 
              || ROUND(((((an.coordinata_x - TRUNC(an.coordinata_x)) * 60) - TRUNC((an.coordinata_x - TRUNC(an.coordinata_x)) * 60)) *60),2)) coord_lat
             ,TRIM(TO_CHAR(TRUNC(an.coordinata_y) ,'999') || ' ' 
              || TRUNC((an.coordinata_y - TRUNC(an.coordinata_y)) * 60) || ' ' 
              || ROUND(((((an.coordinata_y - TRUNC(an.coordinata_y)) * 60) - TRUNC((an.coordinata_y - TRUNC(an.coordinata_y)) * 60)) *60),2)) coord_long
             ,an.coordinata_h alt
             ,CASE WHEN cod_tipo_fonte ='E' THEN NVL(def.h_anem,CASE WHEN an.tipo_coord = 'A' THEN ROUND(AVG(eol.h_mozzo) OVER ( PARTITION BY ger.cod_elemento_padre )) ELSE eol.h_mozzo END) ELSE 3 END altezza_sul_terreno
         FROM anagrafica_punti an 
             ,elementi_def def 
             ,elementi_gdf_eolico eol
             ,rel_elementi_ecs_sn ger
        WHERE an.cod_geo IN (def.cod_geo, def.cod_geo_a)
          AND eol.cod_elemento (+) = def.cod_elemento
          AND ger.cod_elemento_figlio = def.cod_elemento
          AND def.data_disattivazione = to_date('01013000','ddmmyyyy')
          AND ger.data_disattivazione = to_date('01013000','ddmmyyyy')
          AND eol.data_disattivazione(+) = to_date('01013000','ddmmyyyy')          
      ) 
 WHERE num = 1
/

GRANT SELECT ON V_PUNTI_GEO TO MAGONAZ;
