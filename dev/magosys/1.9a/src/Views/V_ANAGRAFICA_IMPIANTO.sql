   Prompt View V_ANAGRAFICA_IMPIANTO;
   --
   -- V_ANAGRAFICA_IMPIANTO  (View) 
   --
   --  Dependencies: 
   --   ESERCIZI (Table)
   --   AVVOLGIMENTI_SA (Table)
   --   IMPIANTIAT_SA (Table)
   --   MONTANTIMT_SA (Table)
   --   TRASFORMATORIAT_SA (Table)
   --   SBARRE_SA (Table)
   --   CLIENTIMT_SA (Table)
   --   IMPIANTIMT_SA (Table)
   --   TRASFORMATORIBT_SA (Table)
   --   ELEMENTI_DEF (Table)
   --   TIPI_ELEMENTO (Table)
   --   ELEMENTI (Table)
   --   PKG_ELEMENTI (Package)
   --   V_ESERCIZI (View)
   --
   CREATE OR REPLACE VIEW V_ANAGRAFICA_IMPIANTO
   AS 
   SELECT CASE SUBSTR (COD_GEST_ELEMENTO, 1, 5)
           WHEN '?MONT' THEN COD_GEST_ELEMENTO || '_' || COD_TIPO_ELEMENTO
           ELSE COD_GEST_ELEMENTO
          END COD_GEST_ELEMENTO
        , NOME_ELEMENTO
        , COD_TIPO_ELEMENTO
        , COD_TIPO_FONTE
        , COD_TIPO_CLIENTE
        , TRIM (ID_ELEMENTO) ID_ELEMENTO
        , TRIM (RIF_ELEMENTO) RIF_ELEMENTO
        , FLAG
        , NUM_IMPIANTI
        , POTENZA_INSTALLATA
        , POTENZA_CONTRATTUALE
        , FATTORE
        , COORDINATA_X
        , COORDINATA_Y
     FROM (SELECT A.*
                , ROW_NUMBER () OVER (PARTITION BY A.COD_GEST_ELEMENTO ORDER BY A.COD_GEST_ELEMENTO, NOME_ELEMENTO) ORD
             FROM (SELECT COD_GEST_ELEMENTO
                        , NOME_ELEMENTO
                        , E.COD_TIPO_ELEMENTO
                        , COD_TIPO_FONTE
                        , COD_TIPO_CLIENTE
                        , ID_ELEMENTO
                        , RIF_ELEMENTO
                        , FLAG
                        , NUM_IMPIANTI
                        , POTENZA_INSTALLATA
                        , POTENZA_CONTRATTUALE
                        , FATTORE
                        , COORDINATA_X
                        , COORDINATA_Y
                        , NULL TIPO_FORN
                     FROM ELEMENTI E INNER JOIN ELEMENTI_DEF USING (COD_ELEMENTO)
                    WHERE COD_ELEMENTO = PKG_Elementi.GetElementoBase
                   UNION ALL
                   SELECT A.COD_GEST_ELEMENTO
                        , NOME_ELEMENTO
                        , A.COD_TIPO_ELEMENTO
                        , NULL COD_TIPO_FONTE
                        , NULL COD_TIPO_CLIENTE
                        , NULL ID_ELEMENTO
                        , RIF_ELEMENTO
                        , CASE COD_TIPO_ELEMENTO
                              WHEN 'ESE' THEN TO_NUMBER (PARAM)
                              WHEN 'CPR' THEN NVL (P.CENTROSATELLITE, 0)
                              ELSE NULL
                          END FLAG
                        , TO_NUMBER (NULL) NUM_IMPIANTI
                        , TO_NUMBER (NULL) POTENZA_INSTALLATA
                        , TO_NUMBER (NULL) POTENZA_CONTRATTUALE
                        , TO_NUMBER (NULL) FATTORE
                        , TO_NUMBER (NULL) COORDINATA_X
                        , TO_NUMBER (NULL) COORDINATA_Y
                        , NULL TIPO_FORN
                     FROM (SELECT COD_TIPO_ELEMENTO
                                , COD_GEST COD_GEST_ELEMENTO
                                , NOME NOME_ELEMENTO
                                , PARAM
                                , RIF_ELEMENTO
                             FROM (SELECT E.COD_ESE COD_GEST
                                        , E.NOME
                                        , E.COD_ENTE
                                        , TO_CHAR (ESE_PRIMARIO) PARAM
                                        , NULL RIF_ELEMENTO
                                    FROM (SELECT CASE 
                                                    WHEN SUBSTR(COD_GEST,3,2) = E.ESE THEN E.COD_GEST
                                                    ELSE E.COD_GEST || '_' || E.ESE
                                                 END COD_ESE
                                               , E.NOME
                                               , E.COD_ENTE
                                               , E.ESE
                                            FROM CORELE.ESERCIZI E 
                                           WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                         ) E
                                   LEFT OUTER JOIN V_ESERCIZI X ON E.COD_ESE = X.GST_ESE
                                   UNION ALL
                                   SELECT DISTINCT
                                          COD_GEST
                                        , CPR_NOM
                                        , COD_ENTE
                                        , NULL PARAM
                                        , NULL RIF_ELEMENTO
                                     FROM CORELE.IMPIANTIAT_SA
                                        , (SELECT COD_GEST CPR_COD
                                                , NOME CPR_NOM
                                             FROM (SELECT I.*
                                                        , ROW_NUMBER () OVER (PARTITION BY COD_GEST ORDER BY COD_GEST, LENGTH(NOME) DESC) ORD
                                                     FROM CORELE.IMPIANTIAT_SA I
                                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE)
                                            WHERE ORD = 1) CPN
                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                      AND CPN.CPR_COD = IMPIANTIAT_SA.COD_GEST
                                   UNION ALL
                                   SELECT COD_GEST
                                        , NOME
                                        , COD_ENTE
                                        , NULL PARAM
                                        , NULL RIF_ELEMENTO
                                     FROM CORELE.TRASFORMATORIAT_SA
                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                   UNION ALL
                                   SELECT COD_GEST
                                        , NOME_TRASF
                                        , COD_ENTE
                                        , NULL PARAM
                                        , NULL RIF_ELEMENTO
                                     FROM CORELE.AVVOLGIMENTI_SA
                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                      AND COD_ENTE != 'PRIM'
                                   UNION ALL
                                   SELECT COD_GEST
                                        , NOME
                                        , COD_ENTE
                                        , TIPO PARAM
                                        , NULL RIF_ELEMENTO
                                     FROM CORELE.SBARRE_SA
                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                   UNION ALL
                                   SELECT COD_GEST
                                        , NOME
                                        , COD_ENTE
                                        , NULL PARAM
                                        , CASE 
                                           WHEN COD_GEST_DIRMT <> 'ND' THEN COD_GEST_DIRMT
                                                                       ELSE NULL
                                          END RIF_ELEMENTO
                                     FROM CORELE.MONTANTIMT_SA
                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE) Q1
                                  INNER JOIN TIPI_ELEMENTO T
                                      ON COD_ENTE = CODIFICA_ST
                            WHERE Q1.COD_ENTE = T.CODIFICA_ST) A
                          LEFT OUTER JOIN (SELECT COD_ORG_NODO || SER_NODO || NUM_NODO CPR_COD
                                                , CASE TIPO_NODO
                                                   WHEN 'SA' THEN 1
                                                   ELSE 0
                                                  END CENTROSATELLITE
                                             FROM NODI_TLC@PKG1_STMAUI.IT
                                            WHERE TRATTAMENTO = 0
                                              AND STATO = 'E'
                                              AND SER_NODO = '1') P
                              ON P.CPR_COD = A.COD_GEST_ELEMENTO
                   UNION ALL
                   SELECT A.COD_GEST_ELEMENTO
                        , NOME_ELEMENTO
                        , A.COD_TIPO_ELEMENTO
                        , NULL COD_TIPO_FONTE
                        , CASE COD_TIPO_ELEMENTO
                              WHEN 'TRM' THEN NVL (T.TRM_TIPO_CLIENTE, 'X')
                              WHEN 'CMT' THEN NVL (C.PRD_TIPO_CLIENTE, 'X')
                              ELSE NULL
                          END COD_TIPO_CLIENTE
                        , ELE_ID_ELEMENTO ID_ELEMENTO
                        , CASE COD_TIPO_ELEMENTO
                           WHEN 'CMT' THEN c.POD
                           ELSE NULL
                          END RIF_ELEMENTO
                        , TO_NUMBER (NULL) FLAG
                        , NULL NUM_IMPIANTI
                        , CASE COD_TIPO_ELEMENTO
                              WHEN 'TRM' THEN T.TRM_POTENZA_INSTALLATA
                              WHEN 'CMT' THEN C.PRD_POTENZA_INSTALLATA
                              ELSE TO_NUMBER (NULL)
                          END POTENZA_INSTALLATA
                        , CASE COD_TIPO_ELEMENTO
                              WHEN 'TRM' THEN T.TRM_POTENZA_CONTRATTUALE
                              WHEN 'CMT' THEN C.PRD_POTENZA_CONTRATTUALE
                              ELSE TO_NUMBER (NULL)
                          END POTENZA_CONTRATTUALE
                        , CASE COD_TIPO_ELEMENTO
                              WHEN 'TRM' THEN T.TRM_FATTORE
                              ELSE TO_NUMBER (NULL)
                          END FATTORE
                        , NULL COORDINATA_X
                        , NULL COORDINATA_Y
                        , TIPO_FORN
                     FROM (SELECT COD_TIPO_ELEMENTO
                                , COD_GEST COD_GEST_ELEMENTO
                                , NOME NOME_ELEMENTO
                                , ELE_ID_ELEMENTO
                             FROM (SELECT C.COD_GEST
                                        , C.NOME
                                        , C.COD_ENTE
                                        , I.COD_GEST_SBARRA ELE_ID_ELEMENTO
                                     FROM CORELE.CLIENTIMT_SA C LEFT OUTER JOIN CORELE.IMPIANTIMT_SA I ON C.CODICEST_PADRE = I.CODICE_ST
                                    WHERE SYSDATE BETWEEN C.DATA_INIZIO AND C.DATA_FINE
                                      AND SYSDATE BETWEEN I.DATA_INIZIO AND I.DATA_FINE
                                   UNION ALL
                                   SELECT T.COD_GEST
                                        , T.NOME
                                        , T.COD_ENTE
                                        , C.COD_GEST_SBARRA ELE_ID_ELEMENTO
                                     FROM CORELE.TRASFORMATORIBT_SA T INNER JOIN CORELE.IMPIANTIMT_SA C ON T.CODICEST_IMPMT = C.CODICE_ST
                                    WHERE SYSDATE BETWEEN T.DATA_INIZIO AND T.DATA_FINE
                                      AND SYSDATE BETWEEN C.DATA_INIZIO AND C.DATA_FINE)
                                  INNER JOIN TIPI_ELEMENTO
                                      ON COD_ENTE = CODIFICA_ST
                          ) A
                     LEFT OUTER JOIN 
                          (SELECT COD_ORG_NODO || SER_NODO || NUM_NODO || TIPO_ELE || ID_TRASF TRM_GEST
                                , SUM (POT_PROD) TRM_POTENZA_INSTALLATA
                                , NULL TRM_TIPO_CLIENTE
                                , NULL TRM_POTENZA_CONTRATTUALE
                                , AVG (1) TRM_FATTORE
                            FROM TRASF_PROD_BT_TLC@PKG1_STMAUI.IT A
                            GROUP BY COD_ORG_NODO, SER_NODO, NUM_NODO, TIPO_ELE, ID_TRASF
                          ) T ON T.TRM_GEST = A.COD_GEST_ELEMENTO
                     LEFT OUTER JOIN 
                          (SELECT COD_ORG_NODO || SER_NODO || NUM_NODO || TIPO_ELEMENTO || ID_CLIENTE ELE_COD
                                , POD
                                , POT_GRUPPI PRD_POTENZA_INSTALLATA
                                , POT_DISP PRD_POTENZA_CONTRATTUALE
                                , RAGIO_SOC
                                , PRD_FATTORE
                                , PRD_TIPO_CLIENTE
                                , TIPO_FORN
                             FROM (SELECT COD_ORG_NODO
                                        , SER_NODO
                                        , NUM_NODO
                                        , ID_CLIENTE
                                        , TIPO_ELEMENTO
                                        , TIPO_FORN
                                        , 1 PRD_FATTORE
                                        , RAGIO_SOC
                                        , POT_DISP
                                        , POT_GRUPPI
                                        , POD
                                        , CASE TIPO_FORN
                                               WHEN 'PP' THEN 'A'
                                               WHEN 'AP' THEN 'B'
                                                         ELSE 'C'
                                          END PRD_TIPO_CLIENTE
                                     FROM CLIENTI_TLC@PKG1_STMAUI.IT
                                    WHERE TRATTAMENTO = 0
                                      AND STATO = 'E'
                                      AND SER_NODO = '2'
                                  )
                          ) C ON ELE_COD = COD_GEST_ELEMENTO
                   UNION ALL
                   SELECT SCS_COD COD_GEST_FIGLIO
                        , NOME NOME_ELEMENTO
                        , COD_TIPO_ELEMENTO
                        , NULL COD_TIPO_FONTE
                        , NULL COD_TIPO_CLIENTE
                        , A.CSE_COD ID_ELEMENTO
                        , COD_CFT RIF_ELEMENTO
                        , NULL FLAG
                        , NULL NUM_IMPIANTI
                        , NULL POTENZA_INSTALLATA
                        , NULL POTENZA_CONTRATTUALE
                        , NULL FATTORE
                        , GPS_X COORDINATA_X
                        , GPS_Y COORDINATA_Y
                        , NULL TIPO_FORN
                     FROM (SELECT COD_GEST CSE_COD
                                , COD_ENTE CSE_TIP
                                , NOME
                                , COD_GEST_SBARRA SCS_COD
                                , 'SCS' COD_TIPO_ELEMENTO
                                , COD_CFT
                                , GPS_X
                                , GPS_Y
                             FROM CORELE.IMPIANTIMT_SA
                            INNER JOIN (SELECT COD_CFT
                                             , GPS_X
                                             , GPS_Y
                                             , COD_ORG_NODO || SER_NODO || NUM_NODO CSE_COD
                                          FROM NODI_TLC@PKG1_STMAUI.IT
                                         WHERE TRATTAMENTO = 0
                                           AND STATO = 'E'
                                           AND SER_NODO = '2'
                                       ) ON COD_GEST = CSE_COD
                            WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                          ) A
                   UNION ALL
                   SELECT GMT_COD COD_GEST_FIGLIO
                        , NULL NOME_ELEMENTO
                        , 'GMT' COD_TIPO_ELEMENTO
                        , COD_TIPO_FONTE
                        , NULL COD_TIPO_CLIENTE
                        , GMT_ID_ELEMENTO ID_ELEMENTO
                        , NULL RIF_ELEMENTO
                        , NULL FLAG
                        , NULL NUM_IMPIANTI
                        , POTENZA_INSTALLATA
                        , NULL POTENZA_CONTRATTUALE
                        , FATTORE
                        , NULL COORDINATA_X
                        , NULL COORDINATA_Y
                        , NULL TIPO_FORN
                     FROM (SELECT GMT_COD
                                , TIPO_IMP COD_TIPO_FONTE
                                , POTENZA_INSTALLATA
                                , FATTORE
                                , CMT_COD GMT_ID_ELEMENTO
                                , COD_TIPO_CLIENTE
                             FROM (SELECT COD_ORG_NODO || SER_NODO || NUM_NODO || 'U' || ID_CL CMT_COD
                                        , COD_ORG_NODO || SER_NODO || NUM_NODO || TIPO_ELEMENTO || ID_GENERATORE GMT_COD
                                        , COD_ORG_NODO
                                        , SER_NODO
                                        , NUM_NODO
                                        , TIPO_ELEMENTO
                                        , ID_GENERATORE
                                        , NULL COD_TIPO_CLIENTE
                                        , ID_CL
                                        , TIPO_IMP
                                        , (P_APP_NOM * NVL(N_GEN_PAR,1)) POTENZA_INSTALLATA
                                        , F_P_NOM FATTORE
                                     FROM GENERATORI_TLC@PKG1_STMAUI.IT
                                    WHERE TRATTAMENTO = 0
                                      AND STATO = 'E'
                                      AND SER_NODO = '2'
                                  )
                             INNER JOIN 
                                 (SELECT COD_GEST
                                       , NOME
                                       , COD_ENTE
                                    FROM CORELE.CLIENTIMT_SA
                                   WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                 ) ON COD_GEST = CMT_COD
                          )
                   UNION ALL
                   SELECT TRX_COD COD_GEST_FIGLIO
                        , NULL NOME_ELEMENTO
                        , COD_TIPO_ELEMENTO
                        , COD_TIPO_FONTE
                        , NULL COD_TIPO_CLIENTE
                        , ID_ELEMENTO
                        , NULL RIF_ELEMENTO
                        , NULL FLAG
                        , NUM_IMPIANTI
                        , POTENZA_INSTALLATA
                        , NULL POTENZA_CONTRATTUALE
                        , FATTORE
                        , NULL COORDINATA_X
                        , NULL COORDINATA_Y
                        , NULL TIPO_FORN
                     FROM (SELECT t.cod_gest || '_' || NVL(tipo_gen,3) TRX_COD
                                , t.cod_gest ID_ELEMENTO, 'TRX' COD_TIPO_ELEMENTO
                                , NVL(tipo_gen,3) COD_TIPO_FONTE, NULL COD_TIPO_CLIENTE, NVL(NUM_PROD,0) NUM_IMPIANTI
                                , NVL(POT_PROD,0) POTENZA_INSTALLATA, 1 FATTORE
                             FROM CORELE.TRASFORMATORIBT_SA T 
                            INNER JOIN CORELE.IMPIANTIMT_SA C ON T.CODICEST_IMPMT = C.CODICE_ST
                            LEFT OUTER JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT A                         
                                            ON A.COD_ORG_NODO = SUBSTR(T.COD_GEST,1,4) AND a.SER_NODO = SUBSTR(T.COD_GEST,5,1) 
                                           AND a.NUM_NODO = SUBSTR(T.COD_GEST,6,6) AND a.TIPO_ELE = SUBSTR(T.COD_GEST,12,1)
                                           AND a.id_trasf = SUBSTR(T.COD_GEST,13,2)
                                     WHERE SYSDATE BETWEEN T.DATA_INIZIO AND T.DATA_FINE
                                      AND SYSDATE BETWEEN C.DATA_INIZIO AND C.DATA_FINE
                          )
                  ) A
          )
    WHERE ORD = 1
    ORDER BY CASE
               WHEN SUBSTR(COD_GEST_ELEMENTO,1,1) = '?'   THEN 99
               WHEN COD_TIPO_ELEMENTO             = 'COP' THEN  0             
               WHEN COD_TIPO_ELEMENTO             = 'ESE' THEN CASE FLAG
                                                                   WHEN 1 THEN 1
                                                                   WHEN 0 THEN 2
                                                                          ELSE 3
                                                               END 
               WHEN COD_TIPO_ELEMENTO             = 'CPR' THEN 20
               WHEN COD_TIPO_ELEMENTO             = 'TRF' THEN 25
               WHEN COD_TIPO_ELEMENTO             = 'TRS' THEN 30
               WHEN COD_TIPO_ELEMENTO             = 'TRT' THEN 35
               WHEN COD_TIPO_ELEMENTO             = 'SMT' THEN 40
               WHEN COD_TIPO_ELEMENTO             = 'LMT' THEN 45
               WHEN COD_TIPO_ELEMENTO             = 'SCS' THEN 50
               WHEN COD_TIPO_ELEMENTO             = 'PMT' THEN 55
               WHEN COD_TIPO_ELEMENTO             = 'TRM' THEN 60
               WHEN COD_TIPO_ELEMENTO             = 'GMT' THEN 65
               WHEN COD_TIPO_ELEMENTO             = 'TRX' THEN 70
               ELSE 98
             END,
             CASE SUBSTR(COD_GEST_ELEMENTO,1,1)
               WHEN 'D' THEN 1
               WHEN 'A' THEN 2
                        ELSE 3
             END, 
             COD_GEST_ELEMENTO
/



