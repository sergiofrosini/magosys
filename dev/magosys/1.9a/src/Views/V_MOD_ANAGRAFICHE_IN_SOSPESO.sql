Prompt View V_MOD_ANAGRAFICHE_IN_SOSPESO;
--
-- V_MOD_ANAGRAFICHE_IN_SOSPESO  (View) 
--
--  Dependencies: 
--   DATE_IMPORT_CONSISTENZA (Table)
--   STORICO_IMPORT (Table)
--   PKG_MAGO (Package)
--
CREATE OR REPLACE VIEW V_MOD_ANAGRAFICHE_IN_SOSPESO
AS 
SELECT DATA_RIF, ORIGINE, TIPO
    FROM (SELECT DATA_RIF, 'AUI' ORIGINE, 'SN' TIPO
          FROM (SELECT MAX(DATA_AGG) DATA_RIF
                  FROM UPDATE_AUI_TLC@pkg1_stmaui.it
                 WHERE DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('30000101','yyyymmdd'))
                   AND DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                   AND DATA_AGG > (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                              FROM STORICO_IMPORT
                                             WHERE ORIGINE = 'AUI' 
                                               AND TIPO = 'SN'
                                     )
                  )
         WHERE DATA_RIF IS NOT NULL
        UNION
        SELECT DATA_RIF, 'AUI' ORIGINE, 'SA' TIPO
          FROM (SELECT MAX(DATA_AGG) DATA_RIF
                  FROM UPDATE_AUI_TLC@pkg1_stmaui.it
                 WHERE DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('30000101','yyyymmdd'))
                   AND DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                   AND DATA_AGG > (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                              FROM STORICO_IMPORT
                                             WHERE ORIGINE = 'AUI' 
                                               AND TIPO = 'SA'
                                     )
                  )
         WHERE DATA_RIF IS NOT NULL
        UNION
        SELECT DATA_LAST_IMPORT DATA_RIF, 'CORELE' ORIGINE, 'SN' TIPO
          FROM CORELE.DATE_IMPORT_CONSISTENZA 
         WHERE TIPO_IMPORT = 'SN'
           --AND NVL(NUM_REC,1) > 0
           AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
           AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                      FROM STORICO_IMPORT
                                     WHERE ORIGINE = 'CORELE'
                                       AND TIPO = 'SN'
                                   )
        UNION
        SELECT DATA_LAST_IMPORT DATA_RIF, 'CORELE' ORIGINE, 'SA' TIPO
          FROM CORELE.DATE_IMPORT_CONSISTENZA 
         WHERE TIPO_IMPORT = 'SA'
           --AND NVL(NUM_REC,1) > 0
           AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
           AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                      FROM STORICO_IMPORT
                                     WHERE ORIGINE = 'CORELE'
                                       AND TIPO = 'SA'
                                   )
        UNION
        SELECT DATA_LAST_IMPORT DATA_RIF, 'CORELE' ORIGINE, 'NEA' TIPO
          FROM CORELE.DATE_IMPORT_CONSISTENZA A
         --INNER JOIN CORELE.HIST_NEA_ISFILE B ON A.DATA_LAST_IMPORT = B.DATARIF 
         WHERE TIPO_IMPORT = 'NEA'
           AND NUM_REC > 0
         --  AND TPTRASM <> 'GAST' /* GAST = Spadizione globale */
           AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
           AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                      FROM STORICO_IMPORT
                                     WHERE ORIGINE = 'CORELE'
                                       AND TIPO = 'NEA'
                                   )
           AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                      FROM STORICO_IMPORT
                                     WHERE ORIGINE = 'CORELE'
                                       AND TIPO = 'SA'
                                   )
         )
   ORDER BY DATA_RIF,
            CASE TIPO
              WHEN 'SA'  THEN 1
              WHEN 'NEA' THEN 2
              WHEN 'SN'  THEN 3
              ELSE            9
            END;


