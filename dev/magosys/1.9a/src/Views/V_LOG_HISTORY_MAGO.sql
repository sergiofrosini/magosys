Prompt View V_LOG_HISTORY_MAGO;
--
-- V_LOG_HISTORY_MAGO  (View) 
--
--  Dependencies: 
--   LOG_HISTORY (Table)
--   LOG_HISTORY_INFO (Table)
--   PKG_ELEMENTI (Package)
--
CREATE OR REPLACE VIEW V_LOG_HISTORY_MAGO
AS 
SELECT LOGDATE, 
       ELAPSED, 
       DATA_RIF, 
       DATA_RIF_TO, 
       CLASSE, 
       PROC_NAME,
       CASE
            WHEN ERROR_NUM IS NULL THEN MESSAGE
            ELSE 'ERR: '||TO_CHAR(ERROR_NUM)||' - '||MESSAGE
       END MSG,
       CASE 
            WHEN CODICE IS NULL THEN NULL
            ELSE  PKG_Elementi.DecodeTipElem(TIPO) || ': ' || CASE
                                                            WHEN NOME IS NULL THEN CODICE
                                                            ELSE  CODICE || '  (' || NOME || ')'
                                                          END
       END elemento
  FROM LOG_HISTORY 
  LEFT OUTER JOIN LOG_HISTORY_INFO USING(RUN_ID)   
 ORDER BY TRUNC(logdate) DESC, LOGDATE, RUN_ID, SEQ;


