PROMPT MATERIALIZED VIEW V_SEARCH_ELEMENTS;
--
-- V_SEARCH_ELEMENTS  (Materialized View) 
--
--  Dependencies: 
--   GERARCHIA_IMP_SA (Table)
--   GERARCHIA_IMP_SN (Table)
--   GERARCHIA_GEO (Table)
--   GERARCHIA_AMM (Table)
--   ELEMENTI_DEF (Table)
--   ELEMENTI (Table)
--   TIPI_ELEMENTO (Table)
--


BEGIN
    EXECUTE IMMEDIATE 'DROP MATERIALIZED VIEW V_SEARCH_ELEMENTS';
    DBMS_OUTPUT.PUT_LINE('Vista Materializzata V_SEARCH_ELEMENTS droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -12003 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/


CREATE MATERIALIZED VIEW V_SEARCH_ELEMENTS 
TABLESPACE MAGO_DATA
NOCOMPRESS
NOPARALLEL
BUILD IMMEDIATE
REFRESH FORCE ON DEMAND
WITH PRIMARY KEY
AS 
 SELECT COD_ELEMENTO,
        SEARCH_STRING,
        TIPO
  FROM (SELECT A.COD_ELEMENTO,
               DATA_ATTIVAZIONE,
               SEARCH_STRING,
               TIPO,
               MAX(DATA_ATTIVAZIONE) OVER(PARTITION BY E.COD_ELEMENTO ORDER BY E.COD_ELEMENTO,DATA_ATTIVAZIONE DESC) MAXDATA,
               T.COD_TIPO_ELEMENTO
          FROM (SELECT COD_ELEMENTO,
                       MIN (DATA_ATTIVAZIONE)    DATA_ATTIVAZIONE,
                       LOWER (COD_GEST_ELEMENTO) SEARCH_STRING,
                       'GST' TIPO
                  FROM ELEMENTI
                 INNER JOIN ELEMENTI_DEF USING (COD_ELEMENTO)
                 WHERE INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                 GROUP BY COD_ELEMENTO, COD_GEST_ELEMENTO
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       REGEXP_REPLACE(TRIM(LOWER(NOME_ELEMENTO)),'[^[:alnum:]]') SEARCH_STRING,
                       'NOM' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI USING (cod_elemento)
                 WHERE (NVL(NOME_ELEMENTO, '.')) <> '.'
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       TRIM(LOWER(RIF_ELEMENTO)) SEARCH_STRING,
                       'DIR' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI E USING (cod_elemento)
                 WHERE RIF_ELEMENTO IS NOT NULL
                   AND E.COD_TIPO_ELEMENTO = ('LMT')  -- LINEAMT
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       TRIM(LOWER(RIF_ELEMENTO)) SEARCH_STRING,
                       'POD' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI E USING (cod_elemento)
                 WHERE RIF_ELEMENTO IS NOT NULL
                   AND E.COD_TIPO_ELEMENTO IN ('CMT','CBT','CAT')  -- POD0
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
               ) A
         INNER JOIN ELEMENTI E ON A.COD_ELEMENTO = E.COD_ELEMENTO
         INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
         WHERE T.NASCOSTO <> 1
           AND E.COD_TIPO_ELEMENTO <> 'CFT'
       )
 WHERE CASE 
          WHEN TIPO = 'GST' THEN 1
          ELSE CASE 
                  WHEN DATA_ATTIVAZIONE = MAXDATA
                    THEN 1
                    ELSE 0
               END 
       END = 1
/


COMMENT ON MATERIALIZED VIEW V_SEARCH_ELEMENTS IS 'snapshot table for snapshot V_SEARCH_ELEMENTS'
/

PROMPT INDEX V_SEARCH_ELEMENTS_K1;
--
-- V_SEARCH_ELEMENTS_K1  (Index) 
--
--  Dependencies: 
--   V_SEARCH_ELEMENTS (Table)
--   V_SEARCH_ELEMENTS (Materialized View)
--
CREATE INDEX V_SEARCH_ELEMENTS_K1 ON V_SEARCH_ELEMENTS
(SEARCH_STRING)
TABLESPACE MAGO_IDX
NOPARALLEL
/
