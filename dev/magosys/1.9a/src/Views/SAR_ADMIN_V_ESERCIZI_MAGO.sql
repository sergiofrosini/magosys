--
-- V_ESERCIZI_MAGO  (View) 
--
--  Dependencies: 
--   ESERCIZI_ABILITATI (Table)
--   UNITA_TERRITORIALI (Table)
--   ESERCIZI (Table)
--
CREATE OR REPLACE VIEW V_ESERCIZI_MAGO
AS 
SELECT cod_utr, cod_esercizio, codifica_utr, codifica_esercizio,
       nome_esercizio, nome_utr
  FROM ((SELECT cod_utr, cod_esercizio, codifica_esercizio,
                nome AS nome_esercizio
          FROM esercizi )
     INNER JOIN
       ( SELECT cod_utr,codifica_utr, nome AS nome_utr
           FROM unita_territoriali )
      USING (cod_utr)
     INNER JOIN
      (SELECT * FROM esercizi_abilitati WHERE cod_applicazione  = 'MAGO')
     USING (cod_utr,cod_esercizio))
/


GRANT SELECT ON V_ESERCIZI_MAGO TO MAGO
/
