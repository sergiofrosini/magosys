PROMPT VIEW V_CURRENT_VERSION;

CREATE OR REPLACE VIEW v_current_version AS
SELECT version
      ,data
      ,content
      ,description
  FROM
      (SELECT 'DB=MAGO_' || major || '.' || minor || '/' || patch || ' (' || to_char(data,'dd/mm/yyyy') ||')' version 
             ,data
             ,content
             ,description
             ,ROW_NUMBER() OVER( ORDER BY data DESC) num 
         FROM version v
      ) 
 WHERE num = 1
 /
 