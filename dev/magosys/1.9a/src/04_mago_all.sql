PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI SAR_ADMIN per MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

PROMPT _______________________________________________________________________________________
PROMPT definizioni in schema SAR_ADMIN
PROMPT - gli aggiornamenti sono fatto in DB1 e allineati automaticamente in DB2
PROMPT

conn sar_admin/sar_admin_dba&4

@./src/InitTables/definizioni_amministrazione.sql

conn sar_admin/sar_admin_dba&5

@./src/Views/SAR_ADMIN_V_ESERCIZI_MAGO.sql

GRANT SELECT ON SAR_ADMIN.V_ESERCIZI_MAGO        TO MAGO;

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&5

UNDEF TBS;
DEFINE TBS=&1;
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT =======================================================================================
PROMPT Dblinks
PROMPT _______________________________________________________________________________________
@./src/Dblinks/PKG1_STMAUI.IT.sql

PROMPT =======================================================================================
PROMPT Directories
PROMPT _______________________________________________________________________________________
@./src/Directories/MAGO_LOGDIR_&3.sql;

PROMPT =======================================================================================
PROMPT Types
PROMPT _______________________________________________________________________________________
@./src/Types/T_FILE_INFO_OBJ.sql
@./src/Types/T_MISMETEO_OBJ.sql
@./src/Types/T_MISURA_GME_OBJ.sql
@./src/Types/T_PARAM_EOLIC_OBJ.sql
@./src/Types/T_PARAM_PREV_OBJ.sql
@./src/Types/T_MISREQ_OBJ.sql
@./src/Types/T_FILE_INFO_ARRAY.sql
@./src/Types/T_MISMETEO_ARRAY.sql
@./src/Types/T_MISURA_GME_ARRAY.sql
@./src/Types/T_PARAM_EOLIC_ARRAY.sql
@./src/Types/T_PARAM_PREV_ARRAY.sql
@./src/Types/T_MISREQ_ARRAY.sql
@./src/Types/T_TRTMISLV_OBJ.sql
@./src/Types/T_TRTMISLVPROFILE_OBJ.sql
@./src/Types/T_TRTMISMVCLI_OBJ.sql
@./src/Types/T_TRTMISMVTRD_OBJ.sql

PROMPT =======================================================================================
PROMPT Sequences
PROMPT _______________________________________________________________________________________
@./src/Sequences/ELEMENTI_PKSEQ.sql
@./src/Sequences/FILEINFO_SEQUENCE.sql
@./src/Sequences/LOG_SEQUENCE.sql
@./src/Sequences/METEO_FILE_XML_SEQ.sql
@./src/Sequences/METEO_FILE_ZIP_SEQ.sql
@./src/Sequences/METEO_JOB_SEQ.sql
@./src/Sequences/SCHEDULED_JOBS_PKSEQ.sql
@./src/Sequences/TRATTAMENTO_ELEMENTI_PKSEQ.sql

@./src/Sequences/OFFLINE_MEASURE_REQUEST_IDSEQ.sql
@./src/Sequences/SESSION_STATISTIC_IDSEQ.sql

PROMPT =======================================================================================
PROMPT Tabelle
PROMPT _______________________________________________________________________________________
@./src/Tables/TIPI_RETE.sql
@./src/Tables/TIPI_ELEMENTO.sql
@./src/Tables/TIPI_GRUPPI_MISURA.sql
@./src/Tables/TIPI_MISURA.sql
@./src/Tables/TIPI_MISURA_CONV_ORIG.sql
@./src/Tables/TIPI_MISURA_CONV_UM.sql
@./src/Tables/TIPI_CLIENTE.sql
@./src/Tables/RAGGRUPPAMENTO_FONTI.sql
@./src/Tables/TIPO_FONTI.sql
@./src/Tables/TIPO_TECNOLOGIA_SOLARE.sql
@./src/Tables/ANAGRAFICA_PUNTI.sql
@./src/Tables/APPLICATION_RUN.sql
@./src/Tables/ELEMENTI.sql
@./src/Tables/ELEMENTI_DEF.sql
@./src/Tables/DEFAULT_CO.sql
@./src/Tables/FILE_PROCESSED.sql
@./src/Tables/FORECAST_PARAMETRI.sql
@./src/Tables/GERARCHIA_AMM.sql
@./src/Tables/GERARCHIA_GEO.sql
@./src/Tables/GERARCHIA_IMP_SA.sql
@./src/Tables/GERARCHIA_IMP_SN.sql
@./src/Tables/GRUPPI_MISURA.sql
@./src/Tables/GTTD_CALC_GERARCHIA.sql
@./src/Tables/GTTD_FORECAST_ELEMENTS.sql
@./src/Tables/GTTD_IMPORT_GERARCHIA.sql
@./src/Tables/GTTD_MISURE.sql
@./src/Tables/GTTD_MOD_ASSETTO_RETE_SA.sql
@./src/Tables/GTTD_REP_ENERGIA_POTENZA_&2.sql
@./src/Tables/GTTD_VALORI_REP.sql
@./src/Tables/GTTD_VALORI_TEMP.sql
@./src/Tables/LOG_HISTORY.sql
@./src/Tables/LOG_HISTORY_INFO_&2.sql
@./src/Tables/MANUTENZIONE.sql
@./src/Tables/METEO_JOB.sql
@./src/Tables/METEO_JOB_RUNTIME_CONFIG.sql
@./src/Tables/METEO_JOB_STATIC_CONFIG.sql
@./src/Tables/METEO_CITTA.sql
@./src/Tables/METEO_FILE_LETTO.sql
@./src/Tables/METEO_FILE_ZIP.sql
@./src/Tables/METEO_FILE_XML.sql
@./src/Tables/METEO_PREVISIONE_&2.sql
@./src/Tables/METEO_REL_ISTAT.sql
@./src/Tables/TRATTAMENTO_ELEMENTI_&2.sql
@./src/Tables/MISURE_ACQUISITE_STATICHE.sql
@./src/Tables/MISURE_AGGREGATE_STATICHE.sql
@./src/Tables/MISURE_ACQUISITE_&2.sql
@./src/Tables/MISURE_AGGREGATE_&2.sql
@./src/Tables/REL_ELEMENTI_AMM.sql
@./src/Tables/REL_ELEMENTI_ECP_SA.sql
@./src/Tables/REL_ELEMENTI_ECP_SN.sql
@./src/Tables/REL_ELEMENTI_ECS_SA.sql
@./src/Tables/REL_ELEMENTI_ECS_SN.sql
@./src/Tables/REL_ELEMENTI_GEO.sql
@./src/Tables/REL_ELEMENTO_TIPMIS.sql
@./src/Tables/SCHEDULED_JOBS_DEF.sql
@./src/Tables/SCHEDULED_JOBS_&2.sql
@./src/Tables/SCHEDULED_TMP_GEN.sql
@./src/Tables/SCHEDULED_TMP_GME.sql
@./src/Tables/SCHEDULED_TMP_MET.sql
@./src/Tables/SERVIZIO_MAGO.sql
@./src/Tables/STORICO_IMPORT.sql
@./src/Tables/TMP_CONV_TRATT_ELEM.sql
@./src/Tables/TMP_MANUTENZIONE.sql

--Aggiunte in versione 1.7
@./src/Tables/MEASURE_OFFLINE_PARAMETERS.sql
@./src/Tables/SESSION_STATISTICS_&2.sql

@./src/Tables/TRT_MIS_LV.sql
@./src/Tables/TRT_MIS_LV_PROFILE.sql
@./src/Tables/TRT_MIS_MV_CLIENTI.sql
@./src/Tables/TRT_MIS_MV_TREND.sql

--Aggiunte in versione 1.8 
@./src/Tables/ELEMENTI_CFG.sql

@./src/Tables/VERSION.sql

PROMPT =======================================================================================
PROMPT Packages
PROMPT _______________________________________________________________________________________
@./src/Packages/PKG_MAGO.sql
@./src/Packages/PKG_AGGREGAZIONI.sql
@./src/Packages/PKG_ANAGRAFICHE.sql
@./src/Packages/PKG_ELEMENTI.sql
@./src/Packages/PKG_INTEGRST.sql
@./src/Packages/PKG_LOGS.sql
@./src/Packages/PKG_MANUTENZIONE.sql
@./src/Packages/PKG_METEO.sql
@./src/Packages/PKG_MISURE.sql
@./src/Packages/PKG_REPORTS.sql
@./src/Packages/PKG_SCHEDULER.sql
@./src/Packages/PKG_TRT_MIS.sql

--Packages Aggiunti in versione 1.7
@./src/Packages/PKG_GENERA_FILE_GEO.sql
@./src/Packages/PKG_LOCALIZZA_GEO.sql
@./src/Packages/PKG_STATS.sql

PROMPT =======================================================================================
PROMPT Viste
PROMPT _______________________________________________________________________________________
@./src/Views/V_ESERCIZI.sql
@./src/Views/V_CURRENT_VERSION.sql
@./src/Views/V_ELEMENTI.sql
@./src/Views/V_ANAGRAFICA_IMPIANTO.sql
@./src/Views/V_GERARCHIA_AMMINISTRATIVA.sql
@./src/Views/V_GERARCHIA_GEOGRAFICA.sql
@./src/Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./src/Views/V_GERARCHIA_IMPIANTO_MT_BT.sql
@./src/Views/V_LOG_HISTORY_MAGO.sql
@./src/Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./src/Views/V_MOD_ASSETTO_RETE_SA.sql
@./src/Views/V_PARAMETRI_APPLICATIVI.sql
@./src/Views/V_TIPI_MISURA.sql
@./src/Views/V_SEARCH_ELEMENTS.sql
@./src/Views/V_PUNTI_GEO.sql

PROMPT =======================================================================================
PROMPT Viste Materializzate
PROMPT _______________________________________________________________________________________
@./src/Views/V_SCHEDULED_JOBS.sql

PROMPT =======================================================================================
PROMPT Procedure
PROMPT _______________________________________________________________________________________
@./src/Procedures/GET_METEODISTRIBLIST.sql

PROMPT =======================================================================================
PROMPT Package Bodies
PROMPT _______________________________________________________________________________________
@./src/PackageBodies/PKG_AGGREGAZIONI.sql
@./src/PackageBodies/PKG_ANAGRAFICHE.sql
@./src/PackageBodies/PKG_ELEMENTI.sql
@./src/PackageBodies/PKG_INTEGRST.sql
@./src/PackageBodies/PKG_LOGS.sql
@./src/PackageBodies/PKG_MAGO.sql
@./src/PackageBodies/PKG_MANUTENZIONE.sql
@./src/PackageBodies/PKG_METEO.sql
@./src/PackageBodies/PKG_MISURE.sql
@./src/PackageBodies/PKG_REPORTS.sql
@./src/PackageBodies/PKG_SCHEDULER.sql
@./src/PackageBodies/PKG_TRT_MIS.sql

--Package aggiunti in versione 1.7

@./src/PackageBodies/PKG_GENERA_FILE_GEO.sql
@./src/PackageBodies/PKG_LOCALIZZA_GEO.sql
@./src/PackageBodies/PKG_STATS.sql

PROMPT =======================================================================================
PROMPT Scheduler Jobs
PROMPT _______________________________________________________________________________________
@./src/SchedulerJobs/ALLINEA_ANAGRAFICA.sql;
@./src/SchedulerJobs/MAGO_SCHEDULER.sql;
@./src/SchedulerJobs/MAGO_INS_REQ_AGGREG.sql;
@./src/SchedulerJobs/MAGO_INS_REQ_AGG_METEO.sql;
@./src/SchedulerJobs/MAGO_INS_REQ_AGG_GME.sql;
@./src/SchedulerJobs/PULIZIA_GIORNALIERA.sql;
@./src/SchedulerJobs/CHECK_SESSION_STATISTICS.sql;

PROMPT =======================================================================================
PROMPT Triggers
PROMPT _______________________________________________________________________________________


@./src/Triggers/AFTI_APPLICATION_RUN.sql
@./src/Triggers/BEF_IUR_ELEMENTI.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_AMM.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_ECP_SA.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_ECP_SN.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_ECS_SA.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_ECS_SN.sql
@./src/Triggers/BEF_IUR_REL_ELEMENTI_GEO.sql
@./src/Triggers/BEF_IUR_SCHEDULED_JOBS.sql
@./src/Triggers/BEF_IUR_TRATTAMENTO_ELEMENTI.sql
@./src/Triggers/BEF_SCHEDULED_TMP_GEN.sql
@./src/Triggers/BEF_SCHEDULED_TMP_GME.sql
@./src/Triggers/BEF_SCHEDULED_TMP_MET.sql
@./src/Triggers/FILEINFO_ID_PK.sql


PROMPT =======================================================================================
PROMPT Inizializzazione Tabelle
PROMPT _______________________________________________________________________________________


PROMPT Insert Table METEO_JOB_RUNTIME_CONFIG;
@./src/InitTables/METEO_JOB_RUNTIME_CONFIG.sql
PROMPT Insert Table METEO_JOB_STATIC_CONFIG;
@./src/InitTables/METEO_JOB_STATIC_CONFIG.sql
PROMPT Insert Table METEO_REL_ISTAT;
@./src/InitTables/METEO_REL_ISTAT.sql
PROMPT Insert Table RAGGRUPPAMENTO_FONTI;
@./src/InitTables/RAGGRUPPAMENTO_FONTI.sql
PROMPT Insert Table SCHEDULED_JOBS_DEF;
@./src/InitTables/SCHEDULED_JOBS_DEF.sql
PROMPT Insert Table TIPI_RETE;
@./src/InitTables/TIPI_RETE.sql
PROMPT Insert Table TIPI_ELEMENTO;
@./src/InitTables/TIPI_ELEMENTO.sql
PROMPT Insert Table TIPI_GRUPPI_MISURA;
@./src/InitTables/TIPI_GRUPPI_MISURA.sql
PROMPT Insert Table TIPI_MISURA;
@./src/InitTables/TIPI_MISURA.sql
PROMPT Insert Table TIPI_MISURA_CONV_ORIG;
@./src/InitTables/TIPI_MISURA_CONV_ORIG.sql
PROMPT Insert Table TIPI_MISURA_CONV_UM;
@./src/InitTables/TIPI_MISURA_CONV_UM.sql
PROMPT Insert Table TIPI_CLIENTE;
@./src/InitTables/TIPI_CLIENTE.sql
PROMPT Insert Table TIPO_FONTI;
@./src/InitTables/TIPO_FONTI.sql
PROMPT Insert Table TIPO_TECNOLOGIA_SOLARE;
@./src/InitTables/TIPO_TECNOLOGIA_SOLARE.sql
PROMPT Insert Table GRUPPI_MISURA;
@./src/InitTables/GRUPPI_MISURA.sql
PROMPT Insert Table MEASURE_OFFLINE_PARAMETERS;
@./src/InitTables/MEASURE_OFFLINE_PARAMETERS.sql

@./src/InitTables/VERSION.sql

BEGIN
   INSERT INTO V_PARAMETRI_APPLICATIVI (KEY, VALORE, NOTA) VALUES ('TRACELEVEL', '0', 'Livello di Trace per packages:   2=Solo Errori; 1=Errori+Info; 0=Errori+Info+Dettaglio');
   COMMIT;
EXCEPTION 
   WHEN DUP_VAL_ON_INDEX THEN NULL;
END;
/

/*
PROMPT =======================================================================================
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
PROMPT _______________________________________________________________________________________
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT =======================================================================================
PROMPT OGGETTI INVALIDI
PROMPT _______________________________________________________________________________________
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';
*/

PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9a
PROMPT
