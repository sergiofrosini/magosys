MERGE INTO TIPI_CLIENTE
 T USING (
SELECT 'A' COD_TIPO_CLIENTE,'Produttore Puro' DESCRIZIONE,4 ID_CLIENTE,1 FORNITORE
 FROM dual
 UNION ALL
 SELECT 'B' COD_TIPO_CLIENTE,'AutoProduttore' DESCRIZIONE,8 ID_CLIENTE,1 FORNITORE
 FROM dual
 UNION ALL
 SELECT 'C' COD_TIPO_CLIENTE,'Cliente' DESCRIZIONE,16 ID_CLIENTE,0 FORNITORE
 FROM dual
 UNION ALL
 SELECT 'X' COD_TIPO_CLIENTE,'Non Determinato' DESCRIZIONE,1 ID_CLIENTE,1 FORNITORE
 FROM dual
 UNION ALL
 SELECT 'Z' COD_TIPO_CLIENTE,'Non Applicabile' DESCRIZIONE,2 ID_CLIENTE,1 FORNITORE
 FROM dual
) S ON (
 s.cod_tipo_cliente = t.cod_tipo_cliente
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione , t.fornitore=s.fornitore , t.id_cliente=s.id_cliente
 WHEN NOT MATCHED THEN 
 INSERT (
 cod_tipo_cliente , descrizione , id_cliente , fornitore
) VALUES (
 s.cod_tipo_cliente , s.descrizione , s.id_cliente , s.fornitore
); 

COMMIT;
