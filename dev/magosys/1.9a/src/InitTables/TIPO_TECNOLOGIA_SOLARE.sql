MERGE INTO TIPO_TECNOLOGIA_SOLARE
 T USING (
SELECT 'C' ID_TECNOLOGIA,'Cell' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 'G' ID_TECNOLOGIA,'Glass' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 'L' ID_TECNOLOGIA,'Linear concentrator' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 'P' ID_TECNOLOGIA,'Polymer sheet' DESCRIZIONE
 FROM dual
) S ON (
 s.id_tecnologia = t.id_tecnologia
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione
 WHEN NOT MATCHED THEN 
 INSERT (
 id_tecnologia , descrizione
) VALUES (
 s.id_tecnologia , s.descrizione
); 

COMMIT;
