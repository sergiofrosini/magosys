MERGE INTO TIPI_RETE
 T USING (
SELECT 'A' COD_TIPO_RETE,'AT' DESCRIZIONE,1 ID_RETE
 FROM dual
 UNION ALL
 SELECT 'B' COD_TIPO_RETE,'BT' DESCRIZIONE,4 ID_RETE
 FROM dual
 UNION ALL
 SELECT 'M' COD_TIPO_RETE,'MT' DESCRIZIONE,2 ID_RETE
 FROM dual
) S ON (
 s.cod_tipo_rete = t.cod_tipo_rete
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione , t.id_rete=s.id_rete
 WHEN NOT MATCHED THEN 
 INSERT (
 cod_tipo_rete , descrizione , id_rete
) VALUES (
 s.cod_tipo_rete , s.descrizione , s.id_rete
); 

COMMIT;
