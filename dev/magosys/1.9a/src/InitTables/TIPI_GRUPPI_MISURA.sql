MERGE INTO TIPI_GRUPPI_MISURA
 T USING (
SELECT 1 COD_GRUPPO,'Default' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 9999999999 COD_GRUPPO,'Custom' DESCRIZIONE
 FROM dual
) S ON (
 s.cod_gruppo = t.cod_gruppo
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione
 WHEN NOT MATCHED THEN 
 INSERT (
 cod_gruppo , descrizione
) VALUES (
 s.cod_gruppo , s.descrizione
); 

COMMIT;
