PROMPT METEO_JOB_STATIC_CONFIG

DELETE 
  FROM meteo_job_static_config
 WHERE key IN ('MFM.supplier.ftp.useftp.1','MFM.supplier.ftp.useftp.2'); 

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MFM.supplier.ftp.download.useFtp.1' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useSftp.1' key , 'true' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.upload.useSftp.1' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.port.1' key , '11022' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.1' key , '10.16.11.221' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.1' key , 'sftp_cp-mi' value           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.1' key , '' value
              FROM dual
               UNION ALL
            SELECT 'MFM.supplier.folder.source.path.1' key , '/MAGO/meteo' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useFtp.2' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.download.useSftp.2' key , 'true' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.upload.useSftp.2' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.port.2' key , '11022' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.2' key , '10.16.11.221' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.2' key , 'sftp_cp-mi' value           
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.2' key , '' value
              FROM dual
               UNION ALL
            SELECT 'MFM.supplier.folder.source.path.2' key , '/MAGO/meteo' value
              FROM dual
            ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;



