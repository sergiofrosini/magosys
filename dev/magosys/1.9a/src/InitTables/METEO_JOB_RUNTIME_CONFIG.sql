MERGE INTO METEO_JOB_RUNTIME_CONFIG
 T USING (
SELECT 'LF-service' JOB_ID, to_date('30/04/2015 00.00.00','dd/mm/yyyy hh24.mi.ss') START_DATE, to_date('','dd/mm/yyyy hh24.mi.ss') END_DATE,1 ENABLED,1 FORCE_INSTALL, to_date('','dd/mm/yyyy hh24.mi.ss') LAST_FOREC_DATE
 FROM dual
 UNION ALL
 SELECT 'MDS-service' JOB_ID, to_date('30/04/2015 00.00.00','dd/mm/yyyy hh24.mi.ss') START_DATE, to_date('','dd/mm/yyyy hh24.mi.ss') END_DATE,1 ENABLED,0 FORCE_INSTALL, to_date('','dd/mm/yyyy hh24.mi.ss') LAST_FOREC_DATE
 FROM dual
) S ON (
 s.job_id = t.job_id
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.enabled=s.enabled , t.end_date=s.end_date , t.force_install=s.force_install , t.last_forec_date=s.last_forec_date , t.start_date=s.start_date
 WHEN NOT MATCHED THEN 
 INSERT (
 job_id , start_date , end_date , enabled , force_install , last_forec_date
) VALUES (
 s.job_id , s.start_date , s.end_date , s.enabled , s.force_install , s.last_forec_date
); 

COMMIT;
