--------------------------------------------------------
--  File creato - mercoledý-luglio-23-2014   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package PKG_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE "PKG_REPORTS" AS

/* ***********************************************************************************************************
   NAME:       PKG_Reports
   PURPOSE:    Servizi per la gestione dei Reports

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      17/07/2013  Frosini S.       Created this package (or a similar one...).

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------
 FUNCTION GetMisStima              (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipPrd2        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER;

 FUNCTION GetMisStimaFnte          (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pCodRaggrFonte  IN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipPrd2        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitVarGlob     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pDataDa         IN DATE,
                            pDataA          IN DATE,
                            pOrganizzazione IN NUMBER,
                            pStatoRete      IN NUMBER,
                            pTipologiaRete  IN VARCHAR2,
                            pTipologiaProd  IN VARCHAR2);

 PROCEDURE InitGerarchia     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipEleFiglio   IN VARCHAR2);

 PROCEDURE InitRepStimaConsi     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                  pDataDa         IN DATE,
                                  pDataA          IN DATE,
                                  pOrganizzazione IN NUMBER,
                                  pStatoRete      IN NUMBER,
                                  pTipologiaRete  IN VARCHAR2,
                                  pTipologiaProd  IN VARCHAR2,
                                  pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pTipEleFiglio   IN VARCHAR2);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetRepStimaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur);
 PROCEDURE GetRepStimaTot (pRefCurs       OUT PKG_UtlGlb.t_query_cur);


 PROCEDURE GetRepConsiBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetRepConsiFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur);
 PROCEDURE GetRepConsiTot (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetAggregDaPerim(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE);

 PROCEDURE GetDatiPerimHeader(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pElePerim IN ELEMENTI.COD_ELEMENTO%TYPE);



END PKG_REPORTS;
/
SHOW ERRORS;