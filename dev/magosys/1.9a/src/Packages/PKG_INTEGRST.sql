PROMPT PACKAGE PKG_INTEGRST;
--
-- PKG_INTEGRST  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   ELEMENTI_DEF (Table)
--   ELEMENTI (Table)
--   TIPI_MISURA (Table)
--   SCHEDULED_JOBS (Table)
--   GTTD_VALORI_TEMP (Table)
--   T_MISREQ_ARRAY (Type)
--   T_MISREQ_ARRAY (Type)
--   T_MISREQ_OBJ (Type)
--   PKG_UTLGLB (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--   DUAL ()
--   DBMS_OUTPUT ()
--   PLITBLM ()
--   DBMS_UTILITY ()
--
CREATE OR REPLACE PACKAGE PKG_INTEGRST AS

/* ***********************************************************************************************************
   NAME:       PKG_IntegrST
   PURPOSE:    Servizi di integrazione specifici con sistema ST

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.f      24/07/2012  Moretti C.       Created this package.
   1.0.g      07/09/2012  Moretti C.       Standardizzazione param pMisReq.TIPI_FONTE (GetMeasureReq)
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8.a.1    22/04/2014  Moretti C.       GetMisure - Ottimizzazione query ed eliminata modulazione su PI

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureReq        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pMisReq         IN T_MISREQ_ARRAY,
                                 pDisconnect     IN INTEGER DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

END PKG_IntegrST;
/
SHOW ERRORS;


