Prompt Package PKG_LOCALIZZA_GEO;
--
CREATE OR REPLACE PACKAGE PKG_LOCALIZZA_GEO AS

/* ***********************************************************************************************************
   NAME:       PKG_LOCALIZZA_GEO
   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      04/03/2013  Campi P.         Created this package.

   NOTES:

*********************************************************************************************************** */


-- ----------------------------------------------------------------------------------------------------------
 FUNCTION coord_conversion ( coord_value IN VARCHAR2, coord_type IN VARCHAR2, decimal_point IN VARCHAR2 DEFAULT '.' ) RETURN NUMBER;
 PROCEDURE load_punti;
 PROCEDURE associa_punti(coord_type IN VARCHAR2);

-- ----------------------------------------------------------------------------------------------------------

END PKG_LOCALIZZA_GEO;
/

SHOW ERRORS;


