var exit_val number;
execute :exit_val := 0;

whenever sqlerror exit :exit_val
set line 132
set serveroutput on size 1000000

DECLARE
  v_bnr varchar2(128) := 'xx';
BEGIN
  begin
     select BANNER
       into v_bnr
       from v$version
     where instr(LOWER(BANNER),'oracle database')>0;
    exception when others then
    :exit_val := 0;
    raise_application_error (-20200, 'VERSION NOT FOUND '||chr(10)||' Stop installazione!');
  end;
  dbms_output.put_line('Installazione su:'||chr(10)||'    '||v_bnr);
  if instr (LOWER(v_bnr), 'enterprise') > 0 then
    :exit_val := 10;  -- ENT ED.
  else
    :exit_val := 20;  -- STD ED
  end if;
END;
/
exit :exit_val
