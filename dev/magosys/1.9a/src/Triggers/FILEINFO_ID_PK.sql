--------------------------------------------------------
Prompt  Trigger FILEINFO_ID_PK
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER FILEINFO_ID_PK  
BEFORE INSERT ON file_processed
FOR EACH ROW
BEGIN
IF INSERTING THEN
   IF :NEW.id_file IS NULL THEN
      SELECT fileinfo_sequence.nextval
        INTO :NEW.id_file
        FROM DUAL;
   END IF;
END IF;
END;

/
ALTER TRIGGER FILEINFO_ID_PK ENABLE;
