#!/usr/bin/csh

cd /usr/NEW/magosys/backup/gme/AT
foreach I ( `find *txt.gz -print` )
        set FILE=`basename ${I}`
        rm $FILE
end
foreach I ( `find D???_GMETR_??_??_20??*.zip -print` )
    set FILE=`basename ${I}`
    set ANNO = `expr substr $FILE 18 4`
    mkdir -p $ANNO
    mv $FILE $ANNO
end

cd /usr/NEW/magosys/backup/gme/MT
foreach I ( `find *txt.gz -print` )
        set FILE=`basename ${I}`
        rm $FILE
end
foreach I ( `find D???_GMETR_??_??_20??*.zip -print` )
    set FILE=`basename ${I}`
    set ANNO = `expr substr $FILE 18 4`
    mkdir -p $ANNO
    mv $FILE $ANNO
end
foreach I ( `find D???_GMEPR_*.zip -print` )
    set FILE=`basename ${I}`
    set ANNO = `expr substr $FILE 18 4`
    mkdir -p $ANNO
    mv $FILE $ANNO
end

