#!/usr/bin/ksh
#
#       invia_file_GEOLOC.ksh
#
#       Data:           Maggio. 2014
#       Autore          Paolo Campi
#       Aggiornamenti:  Febbraio 2015 (E. Castrignano - passaggio a sftp)
#
#       Descrizione:
#            tool per ftp dei files di geolocalizzazione per fornitori meteo
#            verso server di scambio
#            Si utilizza un semaforo per far si che chi voglia scaricare i files
#            dal server di interscambio non esegua il download durante il
#            trasferimento degli stessi.
#
#----------------------------------------------------------------------------------
script_name_less_ext=$(basename $0 .ksh)

##
# Definizione delle variabili di ambiente ORACLE
##
export ORACLE_HOME="/oracle_bases/ARCDB2"
export ORACLE_SID="ARCDB2"
export TWO_TASK="ARCDB2"

##
# Definizione delle directory di lavoro
##
TMPDIR="/usr/NEW/magosys/tmp"
UTLDIR="/usr/NEW/magosys/utl"
BINDIR="/usr/NEW/magosys/bin"
LOGDIR="/usr/NEW/magosys/log"
FILDIR="/usr/NEW/magosys/toMeteo"

##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"
TOODIR="/bin/mtools"
FTPDIR="/METEO-I-EM/Anagraf"

##
# Definizione dei comandi di sistema utilizzati nello script
##
AUI_HOST="ftpauitel.enel.com"
SFTP_PORT=11022
FTP_PORT=21
AUTH="mago/mago"
MAGO_HOST="pkg_arcdb2"
MAGO_USR="magosys"
MAGO_PWD="Magosys"
CAT="${USRDIR}/cat"
CP="${USRDIR}/cp"
CUT="${USRDIR2}/cut"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
GREP="${USRDIR}/grep"
KSH="${USRDIR}/ksh"
LDR="${ORACLE_HOME}/bin/sqlldr"
LS1="${USRDIR}/ls -1"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
WC="${USRDIR2}/wc"
SFTP="${USRDIR2}/sftp"

##
# Definizione delle variabili di date-time
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}
DATA_NEW=${ANNO}${MESE}${GIORNO}_${ORA}

##
# Definizione nomi files
##
MAGOLOG="Mag_upload_geo_${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp_geoloc.log"
FTPMAGOCHECKSEMAFORO="mago_chek_semaforo_geoloc.log"
SEMAFORO_GEO="semaforo_geoloc.txt"
ANAGFILE="Anagrafica-punti-geo-localizzati.csv"
CORRFILE="Corrispondenza-punti-P-A.csv"

lockfile="${UTLDIR}/${script_name_less_ext}"
tmp_lockfile="${UTLDIR}/${script_name_less_ext}.$$"
SFTP_USR_PATH_FILE="${UTLDIR}/sftp_usr_path.csv"
SFTP_CERT_USR=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f1)
CERTIFICATO=$($CAT $SFTP_USR_PATH_FILE | $CUT -d";" -f2)

##
# Definizione delle variabili di controllo
##
CHECKFILE="0"
ROW_TO_GREP="\-rw"

#
# Scrittura su file di log
#
function SendLog
{
     dataora=$(date +'%Y.%m.%d %H:%M:%S')
     $ECHO "${dataora} - ${1}" >> ${LOGDIR}/${MAGOLOG}
     return
}

# Controllo per non far partire esecuzioni multiple.
# Si utilizza un hard link in quanto e' un'operazione atomica.
# Oltre ad evitare l'esecuzione di un'altra istanza serve per evitare
# anche una race condition.
$ECHO $$ > $tmp_lockfile
if $(ln $tmp_lockfile $lockfile 2>&-); then
     SendLog "locked PID: $$ "
     trap "rm -f ${lockfile} ${tmp_lockfile} >/dev/null 2>&1" 0
     trap "exit 2" 1 2 3 13 15
else
     SendLog "Processo gia' in esecuzione con PID: $(<$lockfile) "
     trap "rm -f ${tmp_lockfile} >/dev/null 2>&1" 0
     trap "exit 2" 1 2 3 13 15
     exit 0
fi

function sftp_cert
{
     L_SFTP_FILE=$1
     L_SFTP_CERT_USR=$2
     L_SFTP_HOST=$3
     L_SFTP_LOG=$4

     # Aggiungo questa istruzione in comune a tutte le trasmissioni.
     $ECHO "exit 100" >> $L_SFTP_FILE

     # Inizializzaione file di log sftp
     $ECHO "Inizio file log sftp." > $L_SFTP_LOG

     ${SFTP} -oPort=$SFTP_PORT -oIdentityFile="${CERTIFICATO}" -b $L_SFTP_FILE ${L_SFTP_CERT_USR}@${L_SFTP_HOST} 1>> $L_SFTP_LOG 2>&1

     # Si ricerca "exit 100" perche' faccio terminare i batch sftp con questa stringa.
     # Se sftp non termina correttamente uno dei comandi, la modalita' batchmode fa
     # abortire la sessione, cosi non troverei la stringa cercata.
     res=$(${GREP} "exit 100" $L_SFTP_LOG | $WC -l)
     #echo "res: $res "
     if (( $res == 1 )); then
          # sftp ok
          v_check=0
     else
          # sftp ko
          v_check=1
     fi

     return $v_check
}

# MAGO
##
# Accensione semaforo
##
function AccendiSemaforo
{

     SendLog "Posizionamento semaforo per invio files."

     CHECKFILE=0

     ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"
          return 1
     else
          SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"

          SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
          ${ECHO} "cd ${FTPDIR}\r" > $SFTP_FILE_PAR
          ${ECHO} "put ${FILDIR}/${semaforo}\r" >> $SFTP_FILE_PAR
          ${ECHO} "ls -l ${semaforo}\r" >> $SFTP_FILE_PAR

          sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOCHECKSEMAFORO}"

          while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}

          # E' contorto lo so, ma l'sftp mi restituisc una diagnostica per la
          # quale devo fare questo giro di controlli.
          $GREP "${semaforo}" ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} | $GREP "$ROW_TO_GREP" > /dev/null
          retcode=$(${ECHO} $?) 
          if [[ "${retcode}" = "0" ]]; then
               SendLog "Semaforo attivato!"
               ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
          else
               SendLog "Semaforo NON attivato!"
               ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
               CHECKFILE=1
          fi;

          ${RM} -f ${SFTP_FILE_PAR}

     fi

     SendLog "Fine controllo semaforo per invio files"

     return $CHECKFILE
}


# MAGO
##
# Processo di attivazione SFTP per trasferimento file GEO per Fornitori Meteo
##
function invia_files_GEO
{
     SendLog "Inizio fase trasmissione files GEO \n"

     cd ${FILDIR}

     ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile! \n"
          return 1
     else
          SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile! \n"

          SendLog "Trasmissione files GEO per Fornitori Meteo" > $semaforo

          EXIST_FILE_GEO=$(${LS1} "${ESECOMP}-${ANAGFILE}" | ${WC} -l)

          if [[ "${EXIST_FILE_GEO}" != "0" ]]; then

               SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
               ${ECHO} "cd ${FTPDIR}\r" > $SFTP_FILE_PAR
               ${ECHO} "put ${ESECOMP}-${ANAGFILE}\r" >> $SFTP_FILE_PAR
#               ${ECHO} "put ${ESECOMP}-${CORRFILE}\r" >> $SFTP_FILE_PAR

               sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOLOG}"

               retcode=$(${ECHO} $?) 
	       if [[ "${retcode}" = "1" ]]; then
	            while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
	            ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
	            SendLog "FTP files GEO FALLITA"
		    return 1
	       else
	            while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOLOG}
	            ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
	            SendLog "FTP files GEO COMPLETATA"
	       fi

               ${CP} "${ESECOMP}-${ANAGFILE}" "${ESECOMP}-${ANAGFILE}.${DATA}"
               ${CP} "${ESECOMP}-${CORRFILE}" "${ESECOMP}-${CORRFILE}.${DATA}"
          else
               SendLog "Nessun files GEO da trasferire."
          fi

          SendLog "Fine fase trasmissione files GEO per Fornitori Meteo. \n"
     fi;

     return 0
}

# MAGO
##
# Spegnimento semaforo
##
function SpegniSemaforo
{

     SendLog "Rimozione semaforo per invio files."

     CHECKFILE=0

     ${NETCAT} -vw 5 -z $AUI_HOST $SFTP_PORT
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          SendLog "Nodo server AUI (${AUI_HOST}) NON raggiungibile!"
          return 1
     else
          SendLog "Nodo server AUI (${AUI_HOST}) raggiungibile!"

          SFTP_FILE_PAR="${TMPDIR}/sftp_par.txt"
          ${ECHO} "cd ${FTPDIR}\r" > $SFTP_FILE_PAR
          ${ECHO} "rm ${semaforo}\r" >> $SFTP_FILE_PAR
          ${ECHO} "ls -l ${semaforo}\r" >> $SFTP_FILE_PAR

          sftp_cert $SFTP_FILE_PAR $SFTP_CERT_USR $AUI_HOST "${TMPDIR}/${FTPMAGOCHECKSEMAFORO}"

          while read riga; do SendLog "${riga}"; done < ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}

          # E' contorto lo so, ma l'sftp mi restituisc una diagnostica per la
          # quale devo fare questo giro di controlli.
          $GREP "${semaforo}" ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} | $GREP "$ROW_TO_GREP" > /dev/null
          retcode=$(${ECHO} $?) 
          if [[ "${retcode}" = "0" ]]; then
               SendLog "Semaforo rimosso correttamente."
               ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
          else
               SendLog "Semaforo ANCORA ATTIVO!"
               ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
               CHECKFILE=1
          fi;

          ${RM} -f ${SFTP_FILE_PAR}
          ${RM} -f ${FILDIR}/$semaforo

     fi

     SendLog "Fine controllo semaforo per invio files."

     return $CHECKFILE
}

##
#
##
function main
{

     SendLog "__________________________________________________________________"
     SendLog "Controllo esistenza e trasmissione files GEO per Fornitori Meteo"

     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 1;
whenever oserror exit 1;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select e.COD_GEST_ELEMENTO
  from ELEMENTI e
 inner join DEFAULT_CO d on e.COD_ELEMENTO = d.COD_ELEMENTO_ESE
 where d.FLAG_PRIMARIO=1
   and e.COD_TIPO_ELEMENTO='ESE';
exit 140
!)
     if [[ "${?}" != "140" ]]; then
          echo "Errore recupero dati!"
          return 1
     fi

     export ESECOMP

     semaforo="${ESECOMP}_${SEMAFORO_GEO}"
     $ECHO "Semaforo invio files GEOLOC" > ${FILDIR}/$semaforo

     AccendiSemaforo || return 1

     invia_files_GEO || return 1

     SpegniSemaforo || return 1

     return 0
}

main || exit 1

exit 0
