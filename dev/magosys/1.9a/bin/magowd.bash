#!/bin/bash

MAGO_USER="magosys"
log_file=process_status.log
if [[  -d /oracle_bases/ARCDB2/oradata ]]
then
# SAR cluster
  export ORACLE_HOME=/oracle_bases/ARCDB2
else
# SAR std
  export ORACLE_HOME=/orakit/app/oracle/product/10.2.0/db_2
fi
export ORACLE_SID=ARCDB2

cd /usr/NEW/$MAGO_USER/bin

rm -f $log_file

relaunch=0

declare -a MONITOR_PROCESSES
MONITOR_PROCESSES[0]="Dispatcher"
MONITOR_PROCESSES[1]="ConfMgr"
MONITOR_PROCESSES[2]="DBMgr"
MONITOR_PROCESSES[3]="DataLoader"

  typeset -i num_procs=${#MONITOR_PROCESSES[@]}
echo "=======================================================" >> $log_file

  echo " ${0}: " >> $log_file
  log_msg="`date` - check  ${num_procs} procesess  ${MAGO_USER}"
  echo $log_msg >> $log_file
  echo "" >> $log_file

  for i in ${MONITOR_PROCESSES[@]}
  do
# echo $i
   log_prc="${i} ......... "
   id=`pgrep  -u ${MAGO_USER} -l ${i} | awk '{print $1}' `
      if [[ ${id} != "" ]]
      then
          log_msg="\t process  ${log_prc} \t is up "
      else
          log_msg="\t process  ${log_prc} \t IS DOWN "
          relaunch=1
      fi
      echo -e $log_msg >> $log_file
  done

ores=`date | awk '{print $4}' |cut -c1-2`
mins=`date | awk '{print $4}' |cut -c4-5`

echo "ore ${ores}:${mins}">> $log_file

typeset -i ore=$ores
typeset -i min=$mins

if   [[ $ore == 23 ]]  ; then
  if [[ $min == 20 ]] ; then
    echo "Riavvio automatico" >> $log_file
    relaunch=1
    echo "backup directory reorganization" >> $log_file
    ./reorg_backup.sh
  fi
fi
echo "" >> $log_file
echo "relaunch = $relaunch" >> $log_file

if [ $relaunch == 1 ] ; then
  echo "relaunching..." >> $log_file
  ./kill_mago.bash
  sleep 90
  ./launch_mago.bash
else
  echo "all processes are up" >> $log_file
fi
echo "=======================================================" >> $log_file