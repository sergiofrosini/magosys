#!/usr/bin/csh
set BASEDIR="/usr/NEW/magosys"
cd $BASEDIR/fromEXT
set cf=0
set FILEDEST="misure.csv"
touch $FILEDEST
foreach I ( `find *Potenza-Attiva-Generata*.csv -print` )
        set FILE=`basename ${I}`
        set cf=`expr $cf + 1`
        if ($cf == 1) then
           cat $FILE > $FILEDEST
        else
           sed '1d' $FILE >> $FILEDEST
        endif
set datet=`date +%Y%m%d`
mv $FILE ./backup/$FILE.$datet
end
