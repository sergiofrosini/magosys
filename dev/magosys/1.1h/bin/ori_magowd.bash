#!/bin/bash

LD_LIBRARY_PATH=//home/magosys/magosys/Runtime/bin:/home/magosys/magosys/Runtime/lib:/orakit/app/oracle/product/10.2.0/db_2/lib:/usr/local/lib;

export LD_LIBRARY_PATH;

ORACLE_HOME=/oracle_bases/ARCDB2;
ORACLE_SID=ARCDB12

export ORACLE_HOME;
export ORACLE_SID;

cd /usr/NEW/magosys/bin
pDBMgr=/proc/`cat DBMgr.pid`/cmdline
pConfMgr=/proc/`cat ConfMgr.pid`/cmdline
pDataMgr=/proc/`cat DataMgr.pid`/cmdline
pDataLoader=/proc/`cat DataLoader.pid`/cmdline
pDispatcher=/proc/`cat Dispatcher.pid`/cmdline
pQueryEngine=/proc/`cat QueryEngine.pid`/cmdline
pReceiver=/proc/`cat Receiver.pid`/cmdline
pExporter=/proc/`cat Exporter.pid`/cmdline

log_file=./process_status.log
rm -f $log_file
date > $log_file

relaunch=0

if [ -f DBMgr.pid ] ; then
if [ -f $pDBMgr ] ; then
  echo "DBMgr is up" >> $log_file
else
  echo "DBMgr is down" >> $log_file
  relaunch=1
fi
else
  echo "DBMgr is down" >> $log_file
  relaunch=1
fi

if [ -f ConfMgr.pid ] ; then
if [ -f $pConfMgr ] ; then
  echo "ConfMgr is up" >> $log_file
else
  echo "ConfMgr is down" >> $log_file
  relaunch=1
fi
else
  echo "ConfMgr is down" >> $log_file
  relaunch=1
fi

if [ -f DataMgr.pid ] ; then
if [ -f $pDataMgr ] ; then
  echo "DataMgr is up" >> $log_file
else
  echo "DataMgr is down" >> $log_file
  relaunch=1
fi
else
  echo "DataMgr is down" >> $log_file
  relaunch=1
fi

if [ -f DataLoader.pid ] ; then
if [ -f $pDataLoader ] ; then
  echo "DataLoader is up" >> $log_file
else
  echo "DataLoader is down" >> $log_file
  relaunch=1
fi
else
  echo "DataLoader is down" >> $log_file
  relaunch=1
fi

if [ -f Dispatcher.pid ] ; then
if [ -f $pDispatcher ] ; then
  echo "Dispatcher is up" >> $log_file
else
  echo "Dispatcher is down" >> $log_file
  relaunch=1
fi
else
  echo "Dispatcher is down" >> $log_file
  relaunch=1
fi

if [ -f QueryEngine.pid ] ; then
if [ -f $pQueryEngine ] ; then
  echo "QueryEngine is up" >> $log_file
else
  echo "QueryEngine is down" >> $log_file
  relaunch=1
fi
else
  echo "QueryEngine is down" >> $log_file
  relaunch=1
fi

if [ -f Receiver.pid ] ; then
if [ -f $pReceiver ] ; then
  echo "Receiver is up" >> $log_file
else
  echo "Receiver is down" >> $log_file
  relaunch=1
fi
else
  echo "Receiver is down" >> $log_file
  relaunch=1
fi

if [ -f Exporter.pid ] ; then
if [ -f $pExporter ] ; then
  echo "Exporter is up" >> $log_file
else
  echo "Exporter is down" >> $log_file
  relaunch=1
fi
else
  echo "Exporter is down" >> $log_file
  relaunch=1
fi

echo $relaunch >> $log_file

if [ $relaunch == 1 ] ; then
  echo "relaunch" >> $log_file
  ./kill_tmesx.bash
#  sleep 90
  ./launch_tmesx.bash
else
  echo "all processes are up" >> $log_file
fi
