LD_LIBRARY_PATH=/home/magosys/magosys/Runtime/bin:/home/magosys/magosys/Runtime/lib:/orakit/app/oracle/product/10.2.0/db_1/lib:/usr/local/lib;

export LD_LIBRARY_PATH;

 clear

echo "****** Starting MAGO BE ******"

echo ""

echo "Starting Dispatcher..."
./Dispatcher -d
echo "Dispatcher started"
sleep 2

echo ""

echo "Starting DBMgr..."
./DBMgr -d
echo "DBMgr started"
sleep 2

echo ""

echo "Starting ConfMgr..."
./ConfMgr -d
echo "ConfMgr started"
sleep 2

echo ""

echo "Starting DataLoader..."
./DataLoader -d
echo "DataLoader started"
sleep 2

echo ""

echo "****** MAGO BE Started ******"

ps ux
