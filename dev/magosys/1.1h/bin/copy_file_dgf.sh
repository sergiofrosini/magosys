#!/usr/bin/csh
set BASEDIR="/usr/NEW/magosys"
cd $BASEDIR/fromEXT
set cf=0
set FILEDEST="produttori.csv"
touch $FILEDEST
foreach I ( `find *Produt-Generatori*.csv -print` )
        set FILE="`basename ${I}`"
        set cf=`expr $cf + 1`
        if ($cf == 1) then
           cat $FILE > $FILEDEST
        else
           sed '1,14d' $FILE >> $FILEDEST
        endif
set datet=`date +%Y%m%d`
mv $FILE ./backup/$FILE.$datet
end
set cf=0
set FILEDEST="eolico.csv"
touch $FILEDEST
foreach I ( `find *Eolici*.csv -print` )
        set FILE="`basename ${I}`"
        set cf=`expr $cf + 1`
        if ($cf == 1) then
           cat $FILE > $FILEDEST
        else
           sed '1,9d' $FILE >> $FILEDEST
        endif
set datet=`date +%Y%m%d`
mv $FILE ./backup/$FILE.$datet
end
set cf=0
set FILEDEST="solare.csv"
touch $FILEDEST
foreach I ( `find *Solari*.csv -print` )
        set FILE="`basename ${I}`"
        set cf=`expr $cf + 1`
        if ($cf == 1) then
           cat $FILE > $FILEDEST
        else
           sed '1,11d' $FILE >> $FILEDEST
        endif
set datet=`date +%Y%m%d`
mv $FILE ./backup/$FILE.$datet
end
$BASEDIR/bin/measure_file_aggr.sh
chmod 777 *.csv
