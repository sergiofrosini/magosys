#!/bin/bash
#    sleep ${TIME_OUT}

MAGO_USER="magosys"
LOG_KILL=safeKillMago.log

declare -a MONITOR_PROCESSES
MONITOR_PROCESSES[0]="Dispatcher"
MONITOR_PROCESSES[1]="ConfMgr"
MONITOR_PROCESSES[2]="DBMgr"
MONITOR_PROCESSES[3]="DataLoader"

  typeset -i num_procs=${#MONITOR_PROCESSES[@]}

  log_msg="`date` - kill ${num_procs} processi  ${MAGO_USER}"
  echo $log_msg >> $LOG_KILL

  for i in ${MONITOR_PROCESSES[@]}
  do
# echo $i
   log_prc="${i} ...... "
   id=`pgrep  -u ${MAGO_USER} -l ${i} | awk '{print $1}' `
      if [[ ${id} != "" ]]
      then
          kill -9 ${id}
          check_status=$?
          if [[ $check_status != 0 ]]
          then
            log_msg="  ${0}: processo  ${log_prc} \t gia' fermo "
          else
            log_msg="  ${0}: processo  ${log_prc} \t Inviato SIGKILL  per pid ${id} "
          fi
      else
          log_msg="  ${0}: processo  ${log_prc} \t NON ATTIVO!!! "
      fi
      echo -e $log_msg >> $LOG_KILL
  done
