--------------------------------------------------------
Prompt  Package Body PKG_ANAGRAFICHE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY PKG_ANAGRAFICHE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.1.h
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

    cStatoNormale              CONSTANT CHAR(2)    := 'SN';
    cStatoAttuale              CONSTANT CHAR(2)    := 'SA';

    cStatoNormaleSuffisso      CONSTANT CHAR(3)    := '_'||cStatoNormale;
    cStatoAttualeSuffisso      CONSTANT CHAR(3)    := '_'||cStatoAttuale;

    cTipTmpChk                 CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'CHK_ANAGR';

    cGiorniMantenimentoStorico CONSTANT NUMBER(2) := 60;

    cErrBlockCodGestDupl       CONSTANT NUMBER(2) := 01;
    cErrBlockCodGestNDef       CONSTANT NUMBER(2) := 02;
    cWarningLowerCodGest       CONSTANT NUMBER(2) := 16;
    cWarningBadCodGest         CONSTANT NUMBER(2) := 32;

    cCreaRelazione             CONSTANT NUMBER(1) := 1;
    cChiudiRelazione           CONSTANT NUMBER(1) := 2;

    gSqlView   LONG;
    gSqlStmt   VARCHAR2(1000);

/* ***********************************************************************************************************
 Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ElaboraGerarchiaIMP      (pData         IN DATE,
                                     pStato        IN INTEGER,
                                     pModAssetto   IN BOOLEAN,
                                     pDiffAnagr   OUT BOOLEAN,
                                     pDiffRelCP   OUT BOOLEAN,
                                     pDiffRelCS   OUT BOOLEAN) AS
/* ----------------------------------------------------------------------------------------------------------
    genera la gerarchia di impianto per l'istante richiesto prelevando le informazioni
    dalla Consistenza Elettica (CORELE) ed integrando IN dati mancanti prelevandoli da AUI.
    L'operazione viene eseguita per lo Stato Richiesto: pStatoAttuale = 1 -> Stato attuale
                                                                        0 -> Stato Normale
-----------------------------------------------------------------------------------------------------------*/

    TYPE t_RowRel IS RECORD  (COD_PADRE             ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_FIGLIO            ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_TIPO_ELEMENTO     ELEMENTI.COD_TIPO_ELEMENTO%TYPE
                             );
    vRowRel     t_RowRel;

    vTipGer     GTTD_VALORI_TEMP.TIP%TYPE := 'GER1';

    vCur        PKG_UtlGlb.t_query_cur;
    vELE        PKG_GestAnagr.t_DefAnagr;
    vRCP        PKG_GestAnagr.t_DefAnagr;
    vRCS        PKG_GestAnagr.t_DefAnagr;

    vStatoAlf   CHAR(2);
    vStatoDes   VARCHAR2(20);

    vCount1     INTEGER;
    vCount2     INTEGER;
    vInd        INTEGER := 0;
    vNum        INTEGER := 0;

    vTxt        VARCHAR2(200);

    vDate       DATE;

    vTipElem    TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vCodElem    ELEMENTI.COD_ELEMENTO%TYPE;

    vCodL01     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL02     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL03     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL04     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL05     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL06     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL07     ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL08     ELEMENTI.COD_ELEMENTO%TYPE;

    vCodL01p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL02p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL03p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL04p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL05p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL06p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL07p    ELEMENTI.COD_ELEMENTO%TYPE;
    vCodL08p    ELEMENTI.COD_ELEMENTO%TYPE;

    TYPE tCod   IS VARRAY(8) OF ELEMENTI.COD_ELEMENTO%TYPE;
    vCod        tCod := tCod(NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);

 BEGIN

    PKG_Logs.StdLogAddTxt('Gerarchia di Impianto ---------------',TRUE,NULL);

    /* INIZIALIZZAZIONE -------------------------------------------------------------------------*/

    pDiffAnagr := FALSE;
    pDiffRelCP := FALSE;
    pDiffRelCS := FALSE;

    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN vStatoAlf := cStatoAttuale;
                                          vStatoDes := 'Attuale';
        WHEN PKG_Mago.gcStatoNormale THEN vStatoAlf := cStatoNormale;
                                          vStatoDes := 'Normale';
    END CASE;

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Stato '||vStatoDes||'  Data '||PKG_Mago.StdOutDate(pData),PKG_UtlGlb.gcTRACE_VRB);

    --SetStartDate(pData);

    -- Inizializza le aree - anagrafica elementi
    PKG_GestAnagr.InitTab  (vELE,pData,USER,'ELEMENTI_DEF','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vELE,'COD_ELEMENTO',        PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vELE,'NOME_ELEMENTO',       PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_ELEMENTO',   PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_FONTE',      PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_PRODUTTORE',    PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'ID_ELEMENTO',         PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'RIF_ELEMENTO',        PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'FLAG',                PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'POTENZA_INSTALLATA',  PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'POTENZA_CONTRATTUALE',PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'FATTORE',             PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'NUM_IMPIANTI',        PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_X',        PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_Y',        PKG_GestAnagr.cColAttributo);

    -- Inizializza le aree - relazioni di CP
    PKG_GestAnagr.InitTab  (vRCP,pData,USER,'REL_ELEMENTI_ECP_'||vStatoAlf,'DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vRCP,'COD_ELEMENTO_PADRE',  PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vRCP,'COD_ELEMENTO_FIGLIO', PKG_GestAnagr.cColChiave);

    -- Inizializza le aree - relazioni di CS
    PKG_GestAnagr.InitTab  (vRCS,pData,USER,'REL_ELEMENTI_ECS_'||vStatoAlf,'DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vRCS,'COD_ELEMENTO_PADRE',  PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vRCS,'COD_ELEMENTO_FIGLIO', PKG_GestAnagr.cColChiave);

    /* ANAGRAFICA IMPIANTO ------------------------------------------------------------------------------------- */

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Controllo Codici Gestionali',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Anagrafiche.CheckCodGestCorele(pStato,TRUE,pData);
    SELECT COUNT(*) INTO vCount1
      FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
              FROM GTTD_VALORI_TEMP
             WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
               AND TIP = cTipTmpChk
           );
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - '||vCount1||' elementi con errori potenzialmente bloccanti.',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Elem. con errori bloccanti : '||vCount1,TRUE,NULL);

    IF pModAssetto = FALSE THEN

        /* CARICAMENTO ANAGRAFICA E MEMORIZZAZIONE DIFFERENZE ANAGRAFICHE ---------------------------*/

        -- Inizializza la tabella di lavoro ANAGRAFICA
        DELETE GTTD_IMPORT_GERARCHIA;
        PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - lettura Anagrafica impianto di CP e CS - inizio',PKG_UtlGlb.gcTRACE_VRB);

        -- recupera la query per la selezione dei dati anagrafici di CP e CS

        SELECT TEXT INTO gSqlView FROM USER_VIEWS WHERE VIEW_NAME = 'V_ANAGRAFICA_IMPIANTO';

        -- sostituisce le stringhe di default con quelle necessarie all'elaborazione
        IF pStato = PKG_Mago.gcStatoNormale THEN
            gSqlView := REPLACE(gSqlView,cStatoAttualeSuffisso,cStatoNormaleSuffisso);
        ELSE
            gSqlView := REPLACE(gSqlView,cStatoNormaleSuffisso,cStatoAttualeSuffisso);
        END IF;
        gSqlView := REPLACE(UPPER(gSqlView),'SYSDATE',':dt');

        -- inserisce in tabella di lavoro i dati anagrafici di CP e CS
        EXECUTE IMMEDIATE 'INSERT INTO GTTD_IMPORT_GERARCHIA ' ||
                            'SELECT COD_GEST_ELEMENTO,NULL,NOME_ELEMENTO,COD_TIPO_ELEMENTO,COD_TIPO_FONTE,COD_TIPO_PRODUTTORE,'||
                                  'ID_ELEMENTO,RIF_ELEMENTO,FLAG,NUM_IMPIANTI,POTENZA_INSTALLATA,POTENZA_CONTRATTUALE,FATTORE,'||
                                  'COORDINATA_X,COORDINATA_Y FROM ('||gSqlView ||')'
                USING pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData; --,pData,pData;

        DELETE GTTD_IMPORT_GERARCHIA
         WHERE COD_GEST_ELEMENTO IN (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                                        FROM GTTD_VALORI_TEMP
                                       WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                                         AND TIP = cTipTmpChk
                                    );

        UPDATE ELEMENTI_DEF SET DATA_DISATTIVAZIONE = pData - 1/86400
         WHERE COD_ELEMENTO IN
               (SELECT COD_ELEMENTO
                  FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                          FROM GTTD_VALORI_TEMP
                         WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                           AND TIP = cTipTmpChk
                       )
                 INNER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
               )
           AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

        PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - lettura Anagrafica impianto di CP e CS - Fine ('||SQL%ROWCOUNT||')',PKG_UtlGlb.gcTRACE_VRB);
        PKG_Logs.StdLogAddTxt('IMP - Elementi di CP e CS Totali : '||SQL%ROWCOUNT,TRUE,NULL);

        -- determina se presenti differenze anagrafiche che comportano il ricalcolo
        -- delle misure relative a POTENZA (Installata e Contrattuale) e NUMERO IMPIANTI
        SELECT COUNT(*) INTO vCount1
          FROM (SELECT COD_GEST_ELEMENTO, NUM_IMPIANTI, POTENZA_INSTALLATA, POTENZA_CONTRATTUALE, FATTORE
                  FROM GTTD_IMPORT_GERARCHIA
                MINUS
                SELECT COD_GEST_ELEMENTO, NUM_IMPIANTI, POTENZA_INSTALLATA, POTENZA_CONTRATTUALE, FATTORE
                  FROM ELEMENTI
                 INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                 WHERE pData BETWEEN   DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
               );
        IF vCount1 > 0 THEN
            pDiffAnagr := TRUE;
        END IF;

        -- lettura differenze anagrafiche con ultimo caricamento
        PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Differenze anagrafiche - inizio',PKG_UtlGlb.gcTRACE_VRB);
        vCount1 := 0;
        vCount2 := 0;
        FOR cRow IN (SELECT E.COD_ELEMENTO, A.*
                         FROM (SELECT * FROM GTTD_IMPORT_GERARCHIA
                               MINUS
                               SELECT COD_GEST_ELEMENTO, NULL, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO,
                                      COD_TIPO_FONTE, COD_TIPO_PRODUTTORE, ID_ELEMENTO, RIF_ELEMENTO, FLAG,
                                      NUM_IMPIANTI, POTENZA_INSTALLATA, POTENZA_CONTRATTUALE, FATTORE, COORDINATA_X, COORDINATA_Y
                                 FROM ELEMENTI E
                                INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                                WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                              ) A
                        LEFT OUTER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                       ORDER BY CASE A.COD_TIPO_ELEMENTO
                                   WHEN PKG_Mago.gcCentroOperativo   THEN 0
                                   WHEN PKG_Mago.gcEsercizio THEN
                                              CASE INSTR(A.COD_GEST_ELEMENTO,'_')
                                                  WHEN 0             THEN 1
                                                                     ELSE 2
                                              END
                                   WHEN PKG_Mago.gcCabinaPrimaria    THEN 3
                                   WHEN PKG_Mago.gcTrasfCabPrim      THEN 4
                                   WHEN PKG_Mago.gcProduttoreAT         THEN 5
                                   WHEN PKG_Mago.gcGeneratoreAT      THEN 6
                                   WHEN PKG_Mago.gcSecondarioDiTrasf THEN 7
                                   WHEN PKG_Mago.gcTerziarioDiTrasf  THEN 8
                                   WHEN PKG_Mago.gcSbarraMT          THEN 9
                                   WHEN PKG_Mago.gcLineaMT           THEN 10
                                   WHEN PKG_Mago.gcSbarraCabSec      THEN 11
                                   WHEN PKG_Mago.gcProduttoreMT         THEN 12
                                   WHEN PKG_Mago.gcGeneratoreMT      THEN 13
                                   WHEN PKG_Mago.gcTrasformMtBt      THEN 14
                                   WHEN PKG_Mago.gcTrasformMtBtDett  THEN 15
                                   WHEN PKG_Mago.gcProduttoreBT          THEN 16
                                   WHEN PKG_Mago.gcGeneratoreBT      THEN 17
                                   ELSE                                   99
                                END,
                                A.COD_GEST_ELEMENTO)
        LOOP
            BEGIN
                IF cRow.COD_ELEMENTO  IS NULL THEN
                    vCount1 := vCount1 + 1;
                    cRow.COD_ELEMENTO := PKG_Elementi.InsertElement(cRow.COD_GEST_ELEMENTO,cRow.COD_TIPO_ELEMENTO);
                    vTxt := 'Ins';
                ELSE
                    vCount2 := vCount2 + 1;
                    -- Viene chiamata la funzione PKG_Elementi.InsertElement per gestire l'eventuale cambio di tipo elemento
                    cRow.COD_ELEMENTO := PKG_Elementi.InsertElement(cRow.COD_GEST_ELEMENTO,cRow.COD_TIPO_ELEMENTO);
                    vTxt := 'Mod';
                END IF;
                IF PKG_Logs.gVerboseLog THEN
                    PKG_Logs.TraceLog('  '||vTxt||' - Cod:'||NVL(TO_CHAR(cRow.COD_ELEMENTO),'<null>')||
                                                  ' - Gst:'||NVL(cRow.COD_GEST_ELEMENTO,'<null>')||
                                                  ' - Nom:'||NVL(cRow.NOME_ELEMENTO,'<null>')||
                                                  ' - Tip:'||NVL(cRow.COD_TIPO_ELEMENTO,'<null>')||
                                                  ' - Fon:'||NVL(cRow.COD_TIPO_FONTE,'<null>')||
                                                  ' - Cli:'||NVL(cRow.COD_TIPO_PRODUTTORE,'<null>')||
                                                  ' - Id:'||NVL(cRow.ID_ELEMENTO,'<null>')||
                                                  ' - Rif:'||NVL(cRow.RIF_ELEMENTO,'<null>')||
                                                  ' - Flg:'||NVL(TO_CHAR(cRow.FLAG),'<null>')||
                                                  ' - PI:'||NVL(TO_CHAR(cRow.POTENZA_INSTALLATA),'<null>')||
                                                  ' - PC:'||NVL(TO_CHAR(cRow.POTENZA_CONTRATTUALE),'<null>')||
                                                  ' - Fat:'||NVL(TO_CHAR(cRow.FATTORE),'<null>')||
                                                  ' - NI:'||NVL(TO_CHAR(cRow.NUM_IMPIANTI),'<null>')||
                                                  ' - X:'||NVL(TO_CHAR(cRow.COORDINATA_X),'<null>')||
                                                  ' - Y:'||NVL(TO_CHAR(cRow.COORDINATA_Y),'<null>')
                                     ,PKG_UtlGlb.gcTRACE_INF);
                END IF;
                PKG_GestAnagr.InitRow (vELE);
                PKG_GestAnagr.AddVal  (vELE,'COD_ELEMENTO',        cRow.COD_ELEMENTO);
                PKG_GestAnagr.AddVal  (vELE,'NOME_ELEMENTO',       cRow.NOME_ELEMENTO);
                PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_ELEMENTO',   cRow.COD_TIPO_ELEMENTO);
                PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_FONTE',      cRow.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_PRODUTTORE',    cRow.COD_TIPO_PRODUTTORE);
                PKG_GestAnagr.AddVal  (vELE,'ID_ELEMENTO',         cRow.ID_ELEMENTO);
                PKG_GestAnagr.AddVal  (vELE,'RIF_ELEMENTO',        cRow.RIF_ELEMENTO);
                PKG_GestAnagr.AddVal  (vELE,'FLAG',                cRow.FLAG);
                PKG_GestAnagr.AddVal  (vELE,'POTENZA_INSTALLATA',  cRow.POTENZA_INSTALLATA);
                PKG_GestAnagr.AddVal  (vELE,'POTENZA_CONTRATTUALE',cRow.POTENZA_CONTRATTUALE);
                PKG_GestAnagr.AddVal  (vELE,'FATTORE',             cRow.FATTORE);
                PKG_GestAnagr.AddVal  (vELE,'NUM_IMPIANTI',        cRow.NUM_IMPIANTI);
                PKG_GestAnagr.AddVal  (vELE,'COORDINATA_X',        cRow.COORDINATA_X);
                PKG_GestAnagr.AddVal  (vELE,'COORDINATA_Y',        cRow.COORDINATA_Y);
                PKG_GestAnagr.Elabora (vELE);
            EXCEPTION
                WHEN OTHERS THEN
                     RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione elemento - Cod:'||cRow.COD_ELEMENTO||' - Gst:'||cRow.COD_GEST_ELEMENTO ||' - '||cRow.NOME_ELEMENTO);
            END;
        END LOOP;
        PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Differenze anagrafiche - Fine ('||TO_CHAR(vCount1 + vCount2)||')',PKG_UtlGlb.gcTRACE_VRB);
        PKG_Logs.StdLogAddTxt('IMP - Elementi Inseriti          : '||vCount1,TRUE,NULL);
        PKG_Logs.StdLogAddTxt('IMP - Elementi Modificati        : '||vCount2,TRUE,NULL);
        IF vCount1 + vCount2 > 0 THEN
            pDiffAnagr := TRUE;
        END IF;

--        -- Disattivazione elementi anagrafici non piu' attivi in anagrafica CORELE
--        vCount1 := 0;
--        FOR i IN (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, DATA_ATTIVAZIONE
--                    FROM (SELECT COD_GEST_ELEMENTO
--                            FROM (SELECT COD_ELEMENTO,DATA_ATTIVAZIONE,E.COD_TIPO_ELEMENTO,COD_GEST_ELEMENTO
--                                    FROM ELEMENTI E
--                                   INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
--                                   INNER JOIN
--                                         (SELECT COD_TIPO_ELEMENTO
--                                            FROM TIPI_ELEMENTO
--                                           WHERE (GER_ECP + GER_ECS ) > 0
--                                         ) T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
--                                   WHERE SYSDATE BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
--                                 ) E
--                           MINUS
--                           SELECT COD_GEST_ELEMENTO FROM GTTD_IMPORT_GERARCHIA A
--                         )
--                   INNER JOIN ELEMENTI E USING(COD_GEST_ELEMENTO)
--                   INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
--                 ) LOOP
--            IF PKG_Logs.gVerboseLog THEN
--                PKG_Logs.TraceLog('  Del Elemento - '||i.COD_ELEMENTO||'  -  ( '||i.COD_GEST_ELEMENTO||' )',PKG_UtlGlb.gcTRACE_INF);
--            END IF;
--            UPDATE ELEMENTI_DEF
--               SET DATA_DISATTIVAZIONE = pData - PKG_Misure.cUnSecondo
--             WHERE COD_ELEMENTO = i.COD_ELEMENTO
--               AND DATA_ATTIVAZIONE = i.DATA_ATTIVAZIONE;
--            vCount1 := vCount1 + 1;
--        END LOOP;
--        IF vCount1 > 0 THEN
--            pDiffAnagr := TRUE;
--        END IF;
--        PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Disattivazione Elementi anagrafici - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
--        PKG_Logs.StdLogAddTxt('IMP - Elementi disattivati       : '||vCount1,TRUE,NULL);

    END IF;

    --COMMIT;

    /* GERARCHIA E RELAZIONI DI CABINA PRIMARIA -------------------------------------------------*/

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Lettura Relazioni di CP - inizio',PKG_UtlGlb.gcTRACE_VRB);

    vCount1 := 0;
    -- Inizializza la tabella di lavoro RELAZIONI
    DELETE GTTD_VALORI_TEMP WHERE TIP = vTipGer;

    IF pModAssetto = FALSE THEN
        -- recupera la query per la selezione dei dati gerarchici di CP
        SELECT TEXT INTO gSqlView FROM USER_VIEWS WHERE VIEW_NAME = 'V_GERARCHIA_IMPIANTO_AT_MT';
        -- sostituisce le stringhe di default con quelle necessarie all'elaborazione
        IF pStato = PKG_Mago.gcStatoNormale THEN
            gSqlView := REPLACE(gSqlView,cStatoAttualeSuffisso,cStatoNormaleSuffisso);
        ELSE
            gSqlView := REPLACE(gSqlView,cStatoNormaleSuffisso,cStatoAttualeSuffisso);
        END IF;
        gSqlView := REPLACE(gSqlView,'SYSDATE',':dt');
        OPEN vCur FOR gSqlView USING pData,pData,pData,pData,pData,pData,pData,pData,
                                     pData,pData,pData,pData,pData,pData,pData,pData,
                                     pData,pData,pData,pData,pData,pData,pData;

        LOOP
            FETCH vCur INTO vCod(01), vCod(02), vCod(03), vCod(04), vCod(05), vCod(06), vCod(07), vCod(08);
            EXIT WHEN vCur%NOTFOUND;
            vCodL01 := NULL;
            vCodL02 := NULL;
            vCodL03 := NULL;
            vCodL04 := NULL;
            vCodL05 := NULL;
            vCodL06 := NULL;
            vCodL07 := NULL;
            vCodL08 := NULL;
            vInd    := 0;
            FOR x IN 1 .. 8 LOOP
                IF vCod(x) IS NOT NULL THEN
                    vInd := vInd + 1;
                    CASE vInd
                        WHEN 1 THEN vCodL01 := vCod(x);
                        WHEN 2 THEN vCodL02 := vCod(x);
                        WHEN 3 THEN vCodL03 := vCod(x);
                        WHEN 4 THEN vCodL04 := vCod(x);
                        WHEN 5 THEN vCodL05 := vCod(x);
                        WHEN 6 THEN vCodL06 := vCod(x);
                        WHEN 7 THEN vCodL07 := vCod(x);
                        WHEN 8 THEN vCodL08 := vCod(x);
                    END CASE;
                END IF;
            END LOOP;
            IF vCodL01 IS NOT NULL AND vCodL02 IS NOT NULL THEN
                IF (vCodL01 <> NVL(vCodL01p,-1)) OR (vCodL02 <> NVL(vCodL02p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL01,vCodL02);
                    vCount1 := vCount1 + 1;
                    vCodL01p := vCodL01;
                    vCodL02p := vCodL02;
                END IF;
            END IF;
            IF vCodL02 IS NOT NULL AND vCodL03 IS NOT NULL THEN
                IF (vCodL02 <> NVL(vCodL02p,-1)) OR (vCodL03 <> NVL(vCodL03p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL02,vCodL03);
                    vCount1 := vCount1 + 1;
                    vCodL02p := vCodL02;
                    vCodL03p := vCodL03;
                END IF;
            END IF;
            IF vCodL03 IS NOT NULL AND vCodL04 IS NOT NULL THEN
                IF (vCodL03 <> NVL(vCodL03p,-1)) OR (vCodL04 <> NVL(vCodL04p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL03,vCodL04);
                    vCount1 := vCount1 + 1;
                    vCodL03p := vCodL03;
                    vCodL04p := vCodL04;
                END IF;
            END IF;
            IF vCodL04 IS NOT NULL AND vCodL05 IS NOT NULL THEN
                IF (vCodL04 <> NVL(vCodL04p,-1)) OR (vCodL05 <> NVL(vCodL05p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL04,vCodL05);
                    vCount1 := vCount1 + 1;
                    vCodL04p := vCodL04;
                    vCodL05p := vCodL05;
                END IF;
            END IF;
            IF vCodL05 IS NOT NULL AND vCodL06 IS NOT NULL THEN
                IF (vCodL05 <> NVL(vCodL05p,-1)) OR (vCodL06 <> NVL(vCodL06p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL05,vCodL06);
                    vCount1 := vCount1 + 1;
                    vCodL05p := vCodL05;
                    vCodL06p := vCodL06;
                END IF;
            END IF;
            IF vCodL06 IS NOT NULL AND vCodL07 IS NOT NULL THEN
                IF (vCodL06 <> NVL(vCodL06p,-1)) OR (vCodL07 <> NVL(vCodL07p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL06,vCodL07);
                    vCount1 := vCount1 + 1;
                    vCodL06p := vCodL06;
                    vCodL07p := vCodL07;
                END IF;
            END IF;
            IF vCodL07 IS NOT NULL AND vCodL08 IS NOT NULL THEN
                IF (vCodL07 <> NVL(vCodL07p,-1)) OR (vCodL08 <> NVL(vCodL08p,-1)) THEN
                    INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL07,vCodL08);
                    vCount1 := vCount1 + 1;
                    vCodL07p := vCodL07;
                    vCodL08p := vCodL08;
                END IF;
            END IF;
        END LOOP;
        CLOSE vCur;
    END IF;
    IF pModAssetto = TRUE THEN
        /*
        ATTENZIONE: La modifica di assetto ha EFFETTO SOLO sullo Stato Attuale
        */
        -- Come base prende la situazione corrente
        INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2)
            SELECT vTipGer,COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO
              FROM REL_ELEMENTI_ECP_SA
             WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        vCount1 := vCount1 + SQL%ROWCOUNT;
        -- aggiunge all'elenco le relazioni di CP aggiunte dalla modifica di assetto rete SA
        INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2)
            SELECT vTipGer,COD_PADRE,COD_ELEMENTO
              FROM GTTD_MOD_ASSETTO_RETE_SA
             WHERE GER_ECP_PADRE = PKG_UtlGlb.gkFlagON
               AND GER_ECP_ELE   = PKG_UtlGlb.gkFlagON
               AND STATO = 'CH';
        --vCount1 := vCount1 + SQL%ROWCOUNT;
        -- toglie dall'elenco le relazioni di CP eliminate dalla modifica di assetto rete SA
        DELETE GTTD_VALORI_TEMP
         WHERE (TIP, NUM1, NUM2) IN (SELECT vTipGer,COD_PADRE,COD_ELEMENTO
                                       FROM GTTD_MOD_ASSETTO_RETE_SA
                                      WHERE GER_ECP_PADRE = PKG_UtlGlb.gkFlagON
                                        AND GER_ECP_ELE   = PKG_UtlGlb.gkFlagON
                                        AND STATO = 'AP'
                                     );
        --vCount1 := vCount1 - SQL%ROWCOUNT;
    END IF;

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Lettura Relazioni di CP - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CP Iniziali   : '||vCount1,TRUE,NULL);

    DELETE GTTD_VALORI_TEMP
     WHERE (TIP,NUM1) IN
                   (SELECT vTipGer,COD_ELEMENTO
                      FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                              FROM GTTD_VALORI_TEMP WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                                                      AND tip = cTipTmpChk
                           )
                     INNER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
                   );
    vCount1 := SQL%ROWCOUNT;
    DELETE GTTD_VALORI_TEMP
     WHERE (TIP,NUM2) IN
                   (SELECT vTipGer,COD_ELEMENTO
                      FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                              FROM GTTD_VALORI_TEMP WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                                                      AND tip = cTipTmpChk
                           )
                     INNER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
                   );
    vCount1 := vCount1 + SQL%ROWCOUNT;
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - '||vCount1||' relazioni CP eliminate a causa di errori potenzialmente bloccanti.',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Rel.CP.Eliminate Err.Block : '||vCount1,TRUE,NULL);

    -- inserimento nuove relazioni di CP
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Inserimento nuove Relazioni di CP - Inizio',PKG_UtlGlb.gcTRACE_VRB);
    vCount1 := 0;

    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                      ||
                      ',COD_TIPO_ELEMENTO COD_TIPO_PADRE '                             ||
                  'FROM (SELECT DISTINCT NUM1 COD_ELEMENTO_PADRE, '                    ||
                                        'NUM2 COD_ELEMENTO_FIGLIO '                    ||
                          'FROM GTTD_VALORI_TEMP '                                     ||
                         'WHERE TIP = '''||vTipGer||''' '                              ||
                        'MINUS '                                                       ||
                        'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '              ||
                          'FROM REL_ELEMENTI_ECP_'||vStatoAlf||' '                     ||
                         'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE ' ||
                       ') '                                                            ||
                 'INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_PADRE ';

    OPEN vCur FOR gSqlStmt USING pData;
    LOOP
        FETCH vCur INTO vCodL01, vCodL02, vTipElem;
        EXIT WHEN vCur%NOTFOUND;

--/*
--inizio ----------  Da togliere (diventa inutile) quando *.nea implementati
--In attesa dell'estensione del Set files nea anche con gestione Secondari/Terziari
--se ricevo una alimentazione su un secondario/terziario e questo non � in gerarchia con un trasformatore,
--cerco il trasformatore e creo il legame trasformatore -> sec./terz.
--*/
--        IF pModAssetto = TRUE THEN
--            IF vTipElem IN (PKG_Mago.gcSecondarioDiTrasf,PKG_Mago.gcTerziarioDiTrasf) THEN
--                -- Verifico che l'elemento Padre (vCodL01) sia figlio di qualcuno (trasformatore)
--                SELECT COUNT(*) INTO vNum FROM REL_ELEMENTI_ECP_SA
--                 WHERE COD_ELEMENTO_FIGLIO = vCodL01
--                   AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
--                IF vNum = 0 THEN
--                    -- se Padre (vCodL01) orfano cerco il suo trasformatore
--                    SELECT PKG_Elementi.GetCodElemento(T.COD_GEST) COD_ELEMENTO
--                      INTO vCodElem
--                      FROM CORELE.AVVOLGIMENTI_SA A
--                     INNER JOIN CORELE.TRASFORMATORIAT_SA T
--                            ON (   A.CODICE_ST = T.CODICEST_AVV_SECN
--                                OR A.CODICE_ST = T.CODICEST_AVV_TERZ
--                               )
--                     WHERE A.COD_GEST = PKG_Elementi.GetGestElemento(vCodL01)
--                       AND SYSDATE BETWEEN A.DATA_INIZIO AND A.DATA_FINE
--                       AND SYSDATE BETWEEN T.DATA_INIZIO AND T.DATA_FINE;
--                     IF vCodElem IS NOT NULL THEN
--                        -- Definisco il trasformatpre come padre di vCodL01
--                        IF PKG_Logs.gVerboseLog THEN
--                            PKG_Logs.TraceLog('  Ins Rel CP  - '||vCodElem||'/'||vCodL01||'  -  ( '||
--                                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodElem)),'<null>')||' / '||
--                                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL01)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
--                        END IF;
--                        vCount1 := vCount1 + 1;
--                        PKG_GestAnagr.InitRow (vRCP);
--                        PKG_GestAnagr.AddVal  (vRCP, 'COD_ELEMENTO_PADRE',  vCodElem);
--                        PKG_GestAnagr.AddVal  (vRCP, 'COD_ELEMENTO_FIGLIO', vCodL01);
--                        BEGIN
--                            PKG_GestAnagr.Elabora (vRCP);
--                        EXCEPTION
--                            WHEN DUP_VAL_ON_INDEX THEN NULL;
--                            WHEN OTHERS THEN
--                                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CP - Padre: '||vCodElem||' - Figlio: '||vCodL01);
--                        END;
--                        --inserisco la ralazione in GTTD altrimenti verr� annullata nello Step di Disattivazione relazioni di CP non piu' presenti
--                        INSERT INTO GTTD_VALORI_TEMP (TIP,NUM1,NUM2) VALUES (vTipGer,vCodElem,vCodL01);
--                     END IF;
--                END IF;
--            END IF;
--        END IF;
--/*
--fine ----------
--*/

        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Ins Rel CP  - '||vCodL01||'/'||vCodL02||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL01)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL02)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        PKG_GestAnagr.InitRow (vRCP);
        PKG_GestAnagr.AddVal  (vRCP, 'COD_ELEMENTO_PADRE',  vCodL01);
        PKG_GestAnagr.AddVal  (vRCP, 'COD_ELEMENTO_FIGLIO', vCodL02);
        BEGIN
            PKG_GestAnagr.Elabora (vRCP);
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN NULL;
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CP - Padre: '||vCodL01||' - Figlio: '||vCodL02);
        END;
    END LOOP;
    IF vCount1 > 0 THEN
        pDiffRelCP := TRUE;
    END IF;
    CLOSE vCur;

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Inserimento nuove Relazioni di CP - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CP attivate   : '||vCount1,TRUE,NULL);

    -- Disattivazione relazioni di CP non piu' presenti

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Disattivazione Relazioni di CP - Inizio',PKG_UtlGlb.gcTRACE_VRB);
    vCount1 := 0;
--    --gSqlStmt := 'SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,DATA_ATTIVAZIONE ' ||
--    --              'FROM REL_ELEMENTI_ECP_'||vStatoAlf||' '                        ||
--    --             'WHERE :dt BETWEEN DATA_ATTIVAZIONE '                            ||
--    --                           'AND DATA_DISATTIVAZIONE '                         ||
--    --               'AND NOT EXISTS (SELECT 1 FROM GTTD_VALORI_TEMP '              ||
--    --                                'WHERE TIP = '''||vTipGer||''' '              ||
--    --                                  'AND NUM1 = COD_ELEMENTO_PADRE '            ||
--    --                                  'AND NUM2 = COD_ELEMENTO_FIGLIO)';
--    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,DATA_ATTIVAZIONE ' ||
--                   'FROM (SELECT * FROM REL_ELEMENTI_ECP_'||vStatoAlf||' '        ||
--                          'WHERE :dt BETWEEN DATA_ATTIVAZIONE '                   ||
--                                            'AND DATA_DISATTIVAZIONE '            ||
--                        ') '                                                      ||
--                   'FULL OUTER JOIN (SELECT NUM1,NUM2 FROM GTTD_VALORI_TEMP '     ||
--                                     'WHERE TIP = '''||vTipGer||''' '             ||
--                                    ') '                                          ||
--                            'ON (    NUM1 = COD_ELEMENTO_PADRE '                  ||
--                                'AND NUM2 = COD_ELEMENTO_FIGLIO) '                ||
--                  'WHERE NUM1 IS NULL ';
    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '  ||
                  'FROM REL_ELEMENTI_ECP_'||vStatoAlf||' '         ||
                 'WHERE :dt BETWEEN DATA_ATTIVAZIONE '             ||
                               'AND DATA_DISATTIVAZIONE '          ||
                'MINUS '                                           ||
                'SELECT NUM1, NUM2 FROM GTTD_VALORI_TEMP '         ||
                 'WHERE TIP = '''||vTipGer||''' ';
    OPEN vCur FOR gSqlStmt USING pData;
    LOOP
        FETCH vCur INTO vCodL01, vCodL02;
        EXIT WHEN vCur%NOTFOUND;
        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Del Rel CP  - '||vCodL01||'/'||vCodL02||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL01)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL02)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        BEGIN
             EXECUTE IMMEDIATE 'UPDATE REL_ELEMENTI_ECP_'||vStatoAlf||' '  ||
                                 'SET DATA_DISATTIVAZIONE = :dt - 1/86400' ||
                                'WHERE COD_ELEMENTO_PADRE = :e1 '          ||
                                  'AND COD_ELEMENTO_FIGLIO = :e2 '         ||
                                  'AND :d1 BETWEEN DATA_ATTIVAZIONE '      ||
                                              'AND DATA_DISATTIVAZIONE '
                         USING pData, vCodL01, vCodL02, pData;
        EXCEPTION
            WHEN OTHERS THEN
                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CP - Padre: '||vCodL01||' - Figlio: '||vCodL02);
        END;
    END LOOP;
    IF vCount1 > 0 THEN
        pDiffRelCP := TRUE;
    END IF;
    CLOSE vCur;

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Disattivazione Relazioni di CP - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CP disattivate: '||vCount1,TRUE,NULL);

    /* GERARCHIA E RELAZIONI DI CABINA SECONDARIA -----------------------------------------------*/

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Lettura Relazioni di CS - inizio',PKG_UtlGlb.gcTRACE_VRB);

    vCount1 := 0;
    -- Inizializza la tabella di lavoro RELAZIONI
    DELETE GTTD_VALORI_TEMP WHERE TIP = vTipGer;

    IF pModAssetto = FALSE THEN
        -- recupera la query per la selezione dei dati geratchici di CS
        SELECT TEXT INTO gSqlView FROM USER_VIEWS WHERE VIEW_NAME = 'V_GERARCHIA_IMPIANTO_MT_BT';
        -- sostituisce le stringhe di default con quelle necessarie all'elaborazione
        IF pStato = PKG_Mago.gcStatoNormale THEN
            gSqlView := REPLACE(gSqlView,cStatoAttualeSuffisso,cStatoNormaleSuffisso);
        ELSE
            gSqlView := REPLACE(gSqlView,cStatoNormaleSuffisso,cStatoAttualeSuffisso);
        END IF;
        gSqlView := REPLACE(gSqlView,'SYSDATE',':dt');
        OPEN vCur FOR gSqlView USING pData,pData,pData,pData;
        LOOP
            FETCH vCur INTO vCodL01, vCodL02, vCodL03;
            EXIT WHEN vCur%NOTFOUND;
            vCount1 := vCount1 + 1;
            IF vCodL01 IS NOT NULL AND
               vCodL02 IS NOT NULL THEN
                INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL01, vCodL02);
            END IF;
            IF vCodL02 IS NOT NULL AND
               vCodL03 IS NOT NULL THEN
                INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2) VALUES (vTipGer, vCodL02, vCodL03);
            END IF;
        END LOOP;
        CLOSE vCur;
    END IF;
    IF pModAssetto = TRUE THEN
        /*
        ATTENZIONE: La modifica di assetto ha SEMPRE effetto solo sullo Stato Attuale
        */
        -- Come base prende la situazione corrente
        INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2)
            SELECT vTipGer,COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO
              FROM REL_ELEMENTI_ECS_SA
             WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        vCount1 := vCount1 + SQL%ROWCOUNT;
        -- aggiunge all'elenco le relazioni di CS aggiunte dalla modifica di assetto rete SA
        INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, NUM2)
            SELECT vTipGer,COD_PADRE,COD_ELEMENTO
              FROM GTTD_MOD_ASSETTO_RETE_SA
             WHERE GER_ECS_PADRE = PKG_UtlGlb.gkFlagON
               AND GER_ECS_ELE   = PKG_UtlGlb.gkFlagON
               AND STATO = 'CH';
        --vCount1 := vCount1 + SQL%ROWCOUNT;
        -- toglie dall'elenco le relazioni di CP eliminate dalla modifica di assetto rete SA
        DELETE GTTD_VALORI_TEMP
         WHERE (TIP, NUM1, NUM2) IN (SELECT vTipGer,COD_PADRE,COD_ELEMENTO
                                       FROM GTTD_MOD_ASSETTO_RETE_SA
                                      WHERE GER_ECS_PADRE = PKG_UtlGlb.gkFlagON
                                        AND GER_ECS_ELE   = PKG_UtlGlb.gkFlagON
                                        AND STATO = 'AP'
                                     );
        --vCount1 := vCount1 - SQL%ROWCOUNT;
    END IF;

    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Lettura Relazioni di CS - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CS Iniziali   : '||vCount1,TRUE,NULL);

    DELETE GTTD_VALORI_TEMP
     WHERE (TIP,NUM1) IN
                   (SELECT vTipGer,COD_ELEMENTO
                      FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                              FROM GTTD_VALORI_TEMP WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                                                      AND tip = cTipTmpChk
                           )
                     INNER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
                   );
    vCount1 := SQL%ROWCOUNT;
    DELETE GTTD_VALORI_TEMP
     WHERE (TIP,NUM2) IN
                   (SELECT vTipGer,COD_ELEMENTO
                      FROM (SELECT DISTINCT ALF1 COD_GEST_ELEMENTO
                              FROM GTTD_VALORI_TEMP WHERE NVL(NUM2,0) IN (cErrBlockCodGestDupl,cErrBlockCodGestNDef)
                                                      AND tip = cTipTmpChk
                           )
                     INNER JOIN ELEMENTI USING(COD_GEST_ELEMENTO)
                   );
    vCount1 := vCount1 + SQL%ROWCOUNT;
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - '||vCount1||' relazioni CS eliminate a causa di errori potenzialmente bloccanti.',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Rel.CS.Eliminate Err.Block : '||vCount1,TRUE,NULL);

    -- inserimento nuove relazioni di CS
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Inserimento nuove Relazioni di CS - Inizio',PKG_UtlGlb.gcTRACE_VRB);
    vCount1 := 0;

    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                      ||
                      ',COD_TIPO_ELEMENTO COD_TIPO_FIGLIO '                            ||
                  'FROM (SELECT DISTINCT NUM1 COD_ELEMENTO_PADRE, '                    ||
                                        'NUM2 COD_ELEMENTO_FIGLIO '                    ||
                          'FROM GTTD_VALORI_TEMP '                                     ||
                         'WHERE TIP = '''||vTipGer||''' '                              ||
                        'MINUS '                                                       ||
                        'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '              ||
                          'FROM REL_ELEMENTI_ECS_'||vStatoAlf||' '                     ||
                         'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE ' ||
                       ') '                                                            ||
                 'INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO ';
    OPEN vCur FOR gSqlStmt USING pData;
    LOOP
        FETCH vCur INTO vCodL01, vCodL02, vTipElem;
        EXIT WHEN vCur%NOTFOUND;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Ins Rel CS  - '||vCodL01||'/'||vCodL02||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL01)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL02)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        vCount1 := vCount1 + 1;
        PKG_GestAnagr.InitRow (vRCS);
        PKG_GestAnagr.AddVal  (vRCS, 'COD_ELEMENTO_PADRE',  vCodL01);
        PKG_GestAnagr.AddVal  (vRCS, 'COD_ELEMENTO_FIGLIO', vCodL02);
        BEGIN
            PKG_GestAnagr.Elabora (vRCS);
        EXCEPTION
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CS - Padre: '||vCodL01||' - Figlio: '||vCodL02);
        END;
        IF pModAssetto = TRUE AND pStato = PKG_Mago.gcStatoAttuale  THEN
            IF vTipElem IN (PKG_Mago.gcProduttoreMT,PKG_Mago.gcProduttoreBT,PKG_Mago.gcTrasformMtBt) THEN
                FOR gen IN (--SELECT COD_ELEMENTO
                            --  FROM ELEMENTI_DEF
                            -- WHERE ID_ELEMENTO = PKG_Elementi.GetGestElemento(vCodL02)
                            --   AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                            SELECT PKG_Elementi.GetCodElemento(COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_GENERATORE) COD_ELEMENTO
                              FROM GENERATORI_TLC@PKG1_STMAUI.IT
                             WHERE COD_ORG_NODO||SER_NODO||NUM_NODO||'U'||ID_CL = PKG_Elementi.GetGestElemento(vCodL02)
                               AND TRATTAMENTO = 0 AND STATO = 'E' AND SER_NODO = '2'
                            UNION ALL
                            SELECT PKG_Elementi.GetCodElemento(COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELE||ID_TRASF||'_'||TIPO_GEN) COD_ELEMENTO
                              FROM TRASF_PROD_BT_TLC@PKG1_STMAUI.IT
                             WHERE COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELE||ID_TRASF = PKG_Elementi.GetGestElemento(vCodL02)
                           ) LOOP
                    SELECT COUNT(*) INTO vNum FROM REL_ELEMENTI_ECS_SA
                     WHERE COD_ELEMENTO_PADRE = vCodL02
                       AND COD_ELEMENTO_FIGLIO = gen.COD_ELEMENTO
                       AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                    IF vNum = 0 THEN
                        vCount1 := vCount1 + 1;
                        IF PKG_Logs.gVerboseLog THEN
                            PKG_Logs.TraceLog('  Ins Rel CS  - '||vCodL02||'/'||gen.COD_ELEMENTO||'  -  ( '||
                                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL02)),'<null>')||' / '||
                                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(gen.COD_ELEMENTO)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
                        END IF;
                        PKG_GestAnagr.InitRow (vRCS);
                        PKG_GestAnagr.AddVal  (vRCS, 'COD_ELEMENTO_PADRE',  vCodL02);
                        PKG_GestAnagr.AddVal  (vRCS, 'COD_ELEMENTO_FIGLIO', gen.COD_ELEMENTO);
                        BEGIN
                            PKG_GestAnagr.Elabora (vRCS);
                        EXCEPTION
                            WHEN DUP_VAL_ON_INDEX THEN NULL;
                            WHEN OTHERS THEN
                                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CS - Padre: '||vCodL01||' - Figlio: '||vCodL02);
                        END;
                        --inserisco la ralazione in GTTD altrimenti verr� annullata nello Step di Disattivazione relazioni di CS non piu' presenti
                        INSERT INTO GTTD_VALORI_TEMP (TIP,NUM1,NUM2) VALUES (vTipGer,vCodL02,gen.COD_ELEMENTO);
                    END IF;
                END LOOP;
            END IF;
        END IF;
    END LOOP;
    IF vCount1 > 0 THEN
        pDiffRelCS := TRUE;
    END IF;
    CLOSE vCur;
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Inserimento nuove Relazioni di CS - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CS inserite   : '||vCount1,TRUE,NULL);

    -- Disattivazione relazioni di CS non piu' presenti
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Disattivazione Relazioni di CS - Inizio',PKG_UtlGlb.gcTRACE_VRB);
    vCount1 := 0;
--    --gSqlStmt := 'SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,DATA_ATTIVAZIONE ' ||
--    --              'FROM REL_ELEMENTI_ECS_'||vStatoAlf||' '                        ||
--    --             'WHERE :dt BETWEEN DATA_ATTIVAZIONE '                            ||
--    --                           'AND DATA_DISATTIVAZIONE '                         ||
--    --               'AND NOT EXISTS (SELECT 1 FROM GTTD_VALORI_TEMP '              ||
--    --                                'WHERE TIP = '''||vTipGer||''' '              ||
--    --                                  'AND NUM1 = COD_ELEMENTO_PADRE '            ||
--    --                                  'AND NUM2 = COD_ELEMENTO_FIGLIO)';
--    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,DATA_ATTIVAZIONE ' ||
--                   'FROM (SELECT * FROM REL_ELEMENTI_ECS_'||vStatoAlf||' '        ||
--                          'WHERE :dt BETWEEN DATA_ATTIVAZIONE '                   ||
--                                            'AND DATA_DISATTIVAZIONE '            ||
--                        ') '                                                      ||
--                   'FULL OUTER JOIN (SELECT NUM1,NUM2 FROM GTTD_VALORI_TEMP '     ||
--                                     'WHERE TIP = '''||vTipGer||''' '             ||
--                                    ') '                                          ||
--                            'ON (    NUM1 = COD_ELEMENTO_PADRE '                  ||
--                                'AND NUM2 = COD_ELEMENTO_FIGLIO) '                ||
--                  'WHERE NUM1 IS NULL ';
    gSqlStmt := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '  ||
                  'FROM REL_ELEMENTI_ECS_'||vStatoAlf||' '         ||
                 'WHERE :dt BETWEEN DATA_ATTIVAZIONE '             ||
                               'AND DATA_DISATTIVAZIONE '          ||
                'MINUS '                                           ||
                'SELECT NUM1, NUM2 FROM GTTD_VALORI_TEMP '         ||
                 'WHERE TIP = '''||vTipGer||''' ';
    OPEN vCur FOR gSqlStmt USING pData;
    LOOP
        FETCH vCur INTO vCodL01, vCodL02;
        EXIT WHEN vCur%NOTFOUND;
        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Del Rel CS  - '||vCodL01||'/'||vCodL02||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL01)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vCodL02)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        BEGIN
             EXECUTE IMMEDIATE 'UPDATE REL_ELEMENTI_ECS_'||vStatoAlf||' '  ||
                                 'SET DATA_DISATTIVAZIONE = :dt - 1/86400' ||
                                'WHERE COD_ELEMENTO_PADRE = :e1 '          ||
                                  'AND COD_ELEMENTO_FIGLIO = :e2 '         ||
                                  'AND :d1 BETWEEN DATA_ATTIVAZIONE '      ||
                                              'AND DATA_DISATTIVAZIONE '
                         USING pData, vCodL01, vCodL02, pData;
        EXCEPTION
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione relazione di CP - Padre: '||vCodL01||' - Figlio: '||vCodL02);
        END;
    END LOOP;
    CLOSE vCur;
    IF vCount1 > 0 THEN
        pDiffRelCS := TRUE;
    END IF;
    PKG_Logs.TraceLog('PKG_Anagrafiche.ElaboraGerarchiaIMP - Disattivazione Relazioni di CS - Fine ('||vCount1||')',PKG_UtlGlb.gcTRACE_VRB);
    PKG_Logs.StdLogAddTxt('IMP - Relazioni di CS disattivate: '||vCount1,TRUE,NULL);

    -- Pulisce la tabella di lavoro RELAZIONI
    DELETE GTTD_VALORI_TEMP;

    -- Pulisce tabella di lavoro ANAGRAFICA
    DELETE GTTD_IMPORT_GERARCHIA;

 END ElaboraGerarchiaIMP;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ElaboraGerarchiaGEO    (pData         IN DATE) AS
/* ----------------------------------------------------------------------------------------------------------
    genera la gerarchia geografica (ISTAT) per l'istante richiesto completando i dati con le informazioni
    geografiche di SAR_ADMIN
-----------------------------------------------------------------------------------------------------------*/

    TYPE t_RowRel IS RECORD  (COD_PADRE             ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_FIGLIO            ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_TIPO_ELEMENTO     ELEMENTI.COD_TIPO_ELEMENTO%TYPE
                             );
    vRowRel     t_RowRel;

    vCur        PKG_UtlGlb.t_query_cur;
    vRow        V_GERARCHIA_GEOGRAFICA%ROWTYPE;
    vELE        PKG_GestAnagr.t_DefAnagr;
    vGEO        PKG_GestAnagr.t_DefAnagr;
    vX          NUMBER;
    vY          NUMBER;
    vSeq        INTEGER := 0;
    vCodPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;

    vReg        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vPrv        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vCom        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vScs        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';

    vCount1     INTEGER;
    vCount2     INTEGER;

    vTxt        VARCHAR2(200);

 BEGIN

    PKG_Logs.StdLogAddTxt('Gerarchia Istat ---------------------',TRUE,NULL);

    --SetStartDate(pData);

    -- Inizializza le aree - anagrafica elementi
    PKG_GestAnagr.InitTab  (vELE,pData,USER,'ELEMENTI_DEF','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vELE,'COD_ELEMENTO',        PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_ELEMENTO',   PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'NOME_ELEMENTO',       PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_X',        PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_Y',        PKG_GestAnagr.cColAttributo);

    -- Inizializza le aree - relazioni di Geografiche (ISTAT)
    PKG_GestAnagr.InitTab  (vGEO,pData,USER,'REL_ELEMENTI_GEO','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vGEO,'COD_ELEMENTO_PADRE',  PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vGEO,'COD_ELEMENTO_FIGLIO', PKG_GestAnagr.cColChiave);

    -- recupera la query com il completamento dati da AUI
    SELECT TEXT INTO gSqlView FROM USER_VIEWS WHERE VIEW_NAME = 'V_GERARCHIA_GEOGRAFICA';

    -- sostituisce le stringhe di default con quelle necessarie all'elaborazione
    gSqlView := REPLACE(gSqlView,'SYSDATE',':dt');

    -- Inizializza la tabella di lavoro
    DELETE GTTD_IMPORT_GERARCHIA;

    -- lettura gerarchia elettrica (completa dei dati AUI)

    vCount1 := 0;

    OPEN vCur FOR gSqlView USING pData, pData;
    LOOP
        FETCH vCur INTO vRow;
        EXIT WHEN vCur%NOTFOUND;

        IF vRow.SCS_GST IS NOT NULL THEN

            vCount1 := vCount1 + 1;

--            IF vRow.REG_GST IS NOT NULL THEN
--                IF vReg <> PKG_Mago.gcRegione||PKG_Mago.cSeparatore||vRow.REG_GST THEN
--                    vReg := PKG_Mago.gcRegione||PKG_Mago.cSeparatore||vRow.REG_GST;
--                    BEGIN
--                        INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
--                                                           COD_GEST_FIGLIO,
--                                                           COD_TIPO_ELEMENTO,
--                                                           NOME_ELEMENTO,
--                                                           COORDINATA_X,
--                                                           COORDINATA_Y)
--                                                   VALUES (PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoBase),
--                                                           vReg,
--                                                           PKG_Mago.gcRegione,
--                                                           vRow.REG_NOM,
--                                                           vRow.REG_X,
--                                                           vRow.REG_Y);
--                        vSeq := vSeq + 1;
--                    EXCEPTION
--                        WHEN DUP_VAL_ON_INDEX THEN NULL;
--                        WHEN OTHERS THEN RAISE;
--                    END;
--                END IF;
--                vCodPadre := PKG_Mago.gcRegione||PKG_Mago.cSeparatore||vRow.REG_GST;
--            END IF;
            IF vRow.PRV_GST IS NOT NULL THEN
                IF vPrv <> vRow.PRV_GST THEN
                    vPrv := vRow.PRV_GST;
                    BEGIN
                        INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                           COD_GEST_FIGLIO,
                                                           COD_TIPO_ELEMENTO,
                                                           NOME_ELEMENTO,
                                                           COORDINATA_X,
                                                           COORDINATA_Y)
                                                   VALUES (--vCodPadre,
                                                           PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoBase),
                                                           vPrv,
                                                           PKG_Mago.gcProvincia,
                                                           vRow.PRV_NOM,
                                                           vRow.PRV_X,
                                                           vRow.PRV_Y);
                        vSeq := vSeq + 1;
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN NULL;
                        WHEN OTHERS THEN RAISE;
                    END;
                END IF;
                vCodPadre := vRow.PRV_GST;
            END IF;
            IF vRow.COM_GST IS NOT NULL THEN
                IF vCom <> vRow.COM_GST THEN
                    vCom := vRow.COM_GST;
                    BEGIN
                        INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                           COD_GEST_FIGLIO,
                                                           COD_TIPO_ELEMENTO,
                                                           NOME_ELEMENTO,
                                                           COORDINATA_X,
                                                           COORDINATA_Y)
                                                   VALUES (vCodPadre,
                                                           vCom,
                                                           PKG_Mago.gcComune,
                                                           vRow.COM_NOM,
                                                           vRow.COM_X,
                                                           vRow.COM_Y);
                        vSeq := vSeq + 1;
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN NULL;
                        WHEN OTHERS THEN RAISE;
                    END;
                END IF;
                vCodPadre := vRow.COM_GST;
            END IF;
            IF vRow.SCS_GST IS NOT  NULL THEN
                IF vScs <> vRow.SCS_GST THEN
                    vScs := vRow.SCS_GST;
                    BEGIN
                        INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                           COD_GEST_FIGLIO,
                                                           COD_TIPO_ELEMENTO,
                                                           NOME_ELEMENTO,
                                                           COORDINATA_X,
                                                           COORDINATA_Y)
                                                   VALUES (vCodPadre,
                                                           vScs,
                                                           PKG_Mago.gcSbarraCabSec,
                                                           vRow.SCS_NOM,
                                                           vRow.SCS_X,
                                                           vRow.SCS_Y);
                        vSeq := vSeq + 1;
                    EXCEPTION
                        WHEN DUP_VAL_ON_INDEX THEN NULL;
                        WHEN OTHERS THEN RAISE;
                    END;
                END IF;
                vCodPadre := vRow.SCS_GST;
            END IF;

        END IF;

    END LOOP;
    CLOSE vCur;

    PKG_Logs.StdLogAddTxt('GEO - Sbarre di CS attive        : '||vCount1,TRUE,NULL);

    -- lettura differenze anagrafiche con ultimo caricamento
    vCount1 := 0;
    FOR cAnagr IN (SELECT *
                     FROM (SELECT COD_ELEMENTO,COD_GEST_FIGLIO COD_GEST_ELEMENTO,A.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                             FROM (SELECT COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                     FROM GTTD_IMPORT_GERARCHIA
                                    GROUP BY COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                    MINUS
                                   SELECT COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                     FROM (SELECT COD_GEST_ELEMENTO COD_GEST_FIGLIO,E.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y,
                                                  ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                             FROM ELEMENTI E
                                            INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                                            WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                          )
                                    WHERE ORD = 1
                                  ) A
                            LEFT OUTER JOIN ELEMENTI B ON A.COD_GEST_FIGLIO = B.COD_GEST_ELEMENTO
                                                      AND A.COD_TIPO_ELEMENTO = B.COD_TIPO_ELEMENTO
                            MINUS
                            SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                             WHERE pData BETWEEN  DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                          )
                    ORDER BY CASE COD_TIPO_ELEMENTO
                                  WHEN PKG_Mago.gcRegione   THEN 1
                                  WHEN PKG_Mago.gcProvincia THEN 2
                                  WHEN PKG_Mago.gcComune    THEN 3
                                  ELSE                           9
                             END,
                             COD_ELEMENTO NULLS FIRST,COD_GEST_ELEMENTO
                  )
    LOOP
        BEGIN
            vCount1 := vCount1 + 1;
            IF cAnagr.COD_ELEMENTO  IS NULL THEN
                cAnagr.COD_ELEMENTO := PKG_Elementi.InsertElement(cAnagr.COD_GEST_ELEMENTO,cAnagr.COD_TIPO_ELEMENTO);
                vTxt := 'Ins';
            ELSE
                vTxt := 'Mod';
            END IF;
            IF PKG_Logs.gVerboseLog THEN
                PKG_Logs.TraceLog('  '||vTxt||' - Cod:'||NVL(TO_CHAR(cAnagr.COD_ELEMENTO),'<null>')||
                                              ' - Gst:'||NVL(cAnagr.COD_GEST_ELEMENTO,'<null>')||
                                              ' - Nom:'||NVL(cAnagr.NOME_ELEMENTO,'<null>')||
                                              ' - Tip:'||NVL(cAnagr.COD_TIPO_ELEMENTO,'<null>')||
                                              ' - X:'||NVL(TO_CHAR(cAnagr.COORDINATA_X),'<null>')||
                                              ' - Y:'||NVL(TO_CHAR(cAnagr.COORDINATA_Y),'<null>')
                                 ,PKG_UtlGlb.gcTRACE_INF);
            END IF;
            PKG_GestAnagr.InitRow (vELE);
            PKG_GestAnagr.AddVal  (vELE,'COD_ELEMENTO',        cAnagr.COD_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_ELEMENTO',   cAnagr.COD_TIPO_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'NOME_ELEMENTO',       cAnagr.NOME_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'COORDINATA_X',        cAnagr.COORDINATA_X);
            PKG_GestAnagr.AddVal  (vELE,'COORDINATA_Y',        cAnagr.COORDINATA_Y);
            PKG_GestAnagr.Elabora (vELE);
        EXCEPTION
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione elemento - Cod:'||cAnagr.COD_ELEMENTO||' - Gst:'||cAnagr.COD_GEST_ELEMENTO||' - '||cAnagr.NOME_ELEMENTO);
        END;
    END LOOP;
    PKG_Logs.StdLogAddTxt('GEO - Differenze anagrafiche     : '||vCount1,TRUE,NULL);

    -- inserimento nuove relazioni
    vCount1 := 0;
    FOR vRow IN (SELECT *
                   FROM (SELECT NVL (P.COD_ELEMENTO, PKG_Elementi.GetElementoBase) COD_PADRE,
                                F.COD_ELEMENTO COD_FIGLIO, F.COD_TIPO_ELEMENTO
                           FROM (SELECT DISTINCT COD_GEST_ELEMENTO, COD_GEST_FIGLIO
                                   FROM GTTD_IMPORT_GERARCHIA
                                ) A
                           LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                          INNER JOIN ELEMENTI F ON F.COD_GEST_ELEMENTO = COD_GEST_FIGLIO
                         MINUS
                         SELECT COD_ELEMENTO_PADRE COD_FIGLIO, COD_ELEMENTO_FIGLIO COD_FIGLIO, COD_TIPO_ELEMENTO
                           FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO
                                   FROM REL_ELEMENTI_GEO
                                  WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                )
                          INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO
                        )
                  ORDER BY 1 NULLS FIRST, 2
                 )
    LOOP
        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Ins Rel GEO - '||vRow.COD_PADRE||'/'||vRow.COD_FIGLIO||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_PADRE)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_FIGLIO)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        PKG_GestAnagr.InitRow (vGEO);
        PKG_GestAnagr.AddVal  (vGEO, 'COD_ELEMENTO_PADRE',  vRow.COD_PADRE);
        PKG_GestAnagr.AddVal  (vGEO, 'COD_ELEMENTO_FIGLIO', vRow.COD_FIGLIO);
        PKG_GestAnagr.Elabora (vGEO);
    END LOOP;
    PKG_Logs.StdLogAddTxt('GEO - Relazioni inserite         : '||vCount1,TRUE,NULL);

    -- Disattivazione relazioni non piu' presenti
    vCount1 := 0;
    FOR vRow IN (SELECT *
                  FROM (SELECT COD_ELEMENTO_PADRE COD_PADRE,COD_ELEMENTO_FIGLIO COD_FIGLIO,COD_TIPO_ELEMENTO
                          FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO
                                  FROM REL_ELEMENTI_GEO
                                 WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                               )
                         INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO
                        MINUS
                        SELECT DISTINCT NVL(P.COD_ELEMENTO,PKG_Elementi.GetElementoBase) COD_PADRE,
                                F.COD_ELEMENTO COD_FIGLIO,F.COD_TIPO_ELEMENTO
                          FROM GTTD_IMPORT_GERARCHIA A
                          LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                         INNER JOIN ELEMENTI F ON F.COD_GEST_ELEMENTO = COD_GEST_FIGLIO
                       )
                 ORDER BY 1 NULLS FIRST,2
                )
    LOOP
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Del Rel GEO - '||vRow.COD_PADRE||'/'||vRow.COD_FIGLIO||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_PADRE)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_FIGLIO)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);

        END IF;
        UPDATE REL_ELEMENTI_GEO
           SET DATA_DISATTIVAZIONE = pData - PKG_Misure.cUnSecondo
         WHERE COD_ELEMENTO_PADRE = vRow.COD_PADRE
           AND COD_ELEMENTO_FIGLIO = vRow.COD_FIGLIO
           AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        vCount1 := vCount1 + 1;
    END LOOP;
    PKG_Logs.StdLogAddTxt('GEO - Relazioni disattivate      : '||vCount1,TRUE,NULL);

 END ElaboraGerarchiaGEO;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ElaboraGerarchiaAMM    (pData         IN DATE) AS
/* ----------------------------------------------------------------------------------------------------------
    genera la gerarchia amministrativa (ZONA) per l'istante completando i dati con le informazioni
    dalle informazioni amministrative di SAR_ADMIN
-----------------------------------------------------------------------------------------------------------*/

    TYPE t_RowRel IS RECORD  (COD_PADRE             ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_FIGLIO            ELEMENTI.COD_ELEMENTO%TYPE,
                              COD_TIPO_ELEMENTO     ELEMENTI.COD_TIPO_ELEMENTO%TYPE
                             );
    vRowRel     t_RowRel;

    vCur        PKG_UtlGlb.t_query_cur;
    vRow        V_GERARCHIA_AMMINISTRATIVA%ROWTYPE;
    vELE        PKG_GestAnagr.t_DefAnagr;
    vAMM        PKG_GestAnagr.t_DefAnagr;
    vX          NUMBER;
    vY          NUMBER;
    vSeq        INTEGER := 0;
    vCodPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;

    vCount1     INTEGER;
    vCount2     INTEGER;

    vEse        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vZna        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vCft        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';
    vScs        ELEMENTI.COD_GEST_ELEMENTO%TYPE := '_';

    vTxt        VARCHAR2(200);

 BEGIN

    PKG_Logs.StdLogAddTxt('Gerarchia Amministrativa ------------',TRUE,NULL);

    --SetStartDate(pData);

    -- Inizializza le aree - anagrafica elementi
    PKG_GestAnagr.InitTab  (vELE,pData,USER,'ELEMENTI_DEF','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vELE,'COD_ELEMENTO',        PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_ELEMENTO',   PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'NOME_ELEMENTO',       PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_X',        PKG_GestAnagr.cColAttributo);
    PKG_GestAnagr.AddCol   (vELE,'COORDINATA_Y',        PKG_GestAnagr.cColAttributo);

    -- Inizializza le aree - relazioni Amministrative
    PKG_GestAnagr.InitTab  (vAMM,pData,USER,'REL_ELEMENTI_AMM','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vAMM,'COD_ELEMENTO_PADRE',  PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vAMM,'COD_ELEMENTO_FIGLIO', PKG_GestAnagr.cColChiave);

    -- recupera la query com il completamento dati da AUI
    SELECT TEXT INTO gSqlView FROM USER_VIEWS WHERE VIEW_NAME = 'V_GERARCHIA_AMMINISTRATIVA';

    -- sostituisce le stringhe di default con quelle necessarie all'elaborazione
    gSqlView := REPLACE(gSqlView,'SYSDATE',':dt');

    -- Inizializza la tabella di lavoro
    DELETE GTTD_IMPORT_GERARCHIA;

    -- lettura gerarchia elettrica (completa dei dati AUI)

    vCount1 := 0;

    OPEN vCur FOR gSqlView USING pData, pData;
    LOOP
        FETCH vCur INTO vRow;
        EXIT WHEN vCur%NOTFOUND;

        vCount1 := vCount1 + 1;

        IF vRow.ESE_GST IS NOT NULL THEN
            IF vEse <> vRow.ESE_GST THEN
                vEse := vRow.ESE_GST;
                INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                   COD_GEST_FIGLIO,
                                                   COD_TIPO_ELEMENTO,
                                                   NOME_ELEMENTO)
                                           VALUES (PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoBase),
                                                   vEse,
                                                   PKG_Mago.gcEsercizio,
                                                   vRow.ESE_NOM);
                vSeq := vSeq + 1;
            END IF;
            vCodPadre := vRow.ESE_GST;
        END IF;
        IF vRow.ZNA_GST IS NOT NULL THEN
            IF vZna <> vRow.ZNA_GST THEN
                vZna := vRow.ZNA_GST;
                INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                   COD_GEST_FIGLIO,
                                                   COD_TIPO_ELEMENTO,
                                                   NOME_ELEMENTO,
                                                   COORDINATA_X,
                                                   COORDINATA_Y)
                                           VALUES (vCodPadre,
                                                   vZna,
                                                   PKG_Mago.gcZona,
                                                   vRow.ZNA_NOM,
                                                   vRow.ZNA_X,
                                                   vRow.ZNA_Y);
                vSeq := vSeq + 1;
            END IF;
            vCodPadre := vRow.ZNA_GST;
        END IF;
        IF vRow.CFT_GST IS NOT NULL THEN
            IF vCft <> vRow.CFT_GST THEN
                vCft := vRow.CFT_GST;
                INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                   COD_GEST_FIGLIO,
                                                   COD_TIPO_ELEMENTO,
                                                   NOME_ELEMENTO,
                                                   COORDINATA_X,
                                                   COORDINATA_Y)
                                           VALUES (vCodPadre,
                                                   vCft,
                                                   PKG_Mago.gcCFT,
                                                   vRow.CFT_NOM,
                                                   vRow.CFT_X,
                                                   vRow.CFT_Y);
                vSeq := vSeq + 1;
            END IF;
            vCodPadre := vRow.CFT_GST;
        END IF;
        IF vRow.SCS_GST IS NOT  NULL THEN
            IF vScs <> vRow.SCS_GST THEN
                vScs := vRow.SCS_GST;
                INSERT INTO GTTD_IMPORT_GERARCHIA (COD_GEST_ELEMENTO,
                                                   COD_GEST_FIGLIO,
                                                   COD_TIPO_ELEMENTO,
                                                   NOME_ELEMENTO,
                                                   COORDINATA_X,
                                                   COORDINATA_Y)
                                           VALUES (vCodPadre,
                                                   vScs,
                                                   PKG_Mago.gcSbarraCabSec,
                                                   vRow.SCS_NOM,
                                                   vRow.SCS_X,
                                                   vRow.SCS_Y);
                vSeq := vSeq + 1;
            END IF;
            vCodPadre := vRow.SCS_GST;
        END IF;
    END LOOP;
    CLOSE vCur;
    PKG_Logs.StdLogAddTxt('AMM - Sbarre di CS attive        : '||vCount1,TRUE,NULL);

    -- lettura differenze anagrafiche con ultimo caricamento
    vCount1 := 0;
    FOR cAnagr IN (SELECT *
                     FROM (SELECT COD_ELEMENTO,COD_GEST_FIGLIO COD_GEST_ELEMENTO,A.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                             FROM (SELECT COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                     FROM GTTD_IMPORT_GERARCHIA
                                    GROUP BY COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                    MINUS
                                   SELECT COD_GEST_FIGLIO,COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                                     FROM (SELECT COD_GEST_ELEMENTO COD_GEST_FIGLIO,E.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y,
                                                  ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                             FROM ELEMENTI E
                                            INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                                            WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                          )
                                    WHERE ORD = 1
                                  ) A
                            LEFT OUTER JOIN ELEMENTI B ON A.COD_GEST_FIGLIO = B.COD_GEST_ELEMENTO
                                                      AND A.COD_TIPO_ELEMENTO = B.COD_TIPO_ELEMENTO
                            MINUS
                            SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                             WHERE pData BETWEEN  DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                          )
                    ORDER BY CASE COD_TIPO_ELEMENTO
                                  WHEN PKG_Mago.gcEsercizio THEN 1
                                  WHEN PKG_Mago.gcZona      THEN 2
                                  WHEN PKG_Mago.gcCFT       THEN 3
                                  ELSE                           9
                             END,
                             COD_ELEMENTO NULLS FIRST,COD_GEST_ELEMENTO
                  )
    LOOP
        BEGIN
            vCount1 := vCount1 + 1;
            IF cAnagr.COD_ELEMENTO  IS NULL THEN
                cAnagr.COD_ELEMENTO := PKG_Elementi.InsertElement(cAnagr.COD_GEST_ELEMENTO,cAnagr.COD_TIPO_ELEMENTO);
                vTxt := 'Ins';
            ELSE
                vTxt := 'Mod';
            END IF;
            IF PKG_Logs.gVerboseLog THEN
                PKG_Logs.TraceLog('  '||vTxt||' - Cod:'||NVL(TO_CHAR(cAnagr.COD_ELEMENTO),'<null>')||
                                              ' - Gst:'||NVL(cAnagr.COD_GEST_ELEMENTO,'<null>')||
                                              ' - Nom:'||NVL(cAnagr.NOME_ELEMENTO,'<null>')||
                                              ' - Tip:'||NVL(cAnagr.COD_TIPO_ELEMENTO,'<null>')||
                                              ' - X:'||NVL(TO_CHAR(cAnagr.COORDINATA_X),'<null>')||
                                              ' - Y:'||NVL(TO_CHAR(cAnagr.COORDINATA_Y),'<null>')
                                 ,PKG_UtlGlb.gcTRACE_INF);
            END IF;
            PKG_GestAnagr.InitRow (vELE);
            PKG_GestAnagr.AddVal  (vELE,'COD_ELEMENTO',        cAnagr.COD_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_ELEMENTO',   cAnagr.COD_TIPO_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'NOME_ELEMENTO',       cAnagr.NOME_ELEMENTO);
            PKG_GestAnagr.AddVal  (vELE,'COORDINATA_X',        cAnagr.COORDINATA_X);
            PKG_GestAnagr.AddVal  (vELE,'COORDINATA_Y',        cAnagr.COORDINATA_Y);
            PKG_GestAnagr.Elabora (vELE);
        EXCEPTION
            WHEN OTHERS THEN
                 RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,SQLERRM||CHR(10)||'Errore in elaborazione elemento - Cod:'||cAnagr.COD_ELEMENTO||' - Gst:'||cAnagr.COD_GEST_ELEMENTO||' - '||cAnagr.NOME_ELEMENTO);
        END;
    END LOOP;
    PKG_Logs.StdLogAddTxt('AMM - Differenze anagrafiche     : '||vCount1,TRUE,NULL);

    -- inserimento nuove relazioni
    vCount1 := 0;
    FOR vRow IN (SELECT *
                   FROM (SELECT NVL(P.COD_ELEMENTO,PKG_Elementi.GetElementoBase) COD_PADRE,
                                F.COD_ELEMENTO COD_FIGLIO ,F.COD_TIPO_ELEMENTO
                           FROM (SELECT DISTINCT COD_GEST_ELEMENTO, COD_GEST_FIGLIO FROM GTTD_IMPORT_GERARCHIA) A
                           LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                          INNER JOIN ELEMENTI F ON F.COD_GEST_ELEMENTO = COD_GEST_FIGLIO
                          MINUS
                         SELECT COD_ELEMENTO_PADRE COD_FIGLIO,COD_ELEMENTO_FIGLIO COD_FIGLIO,COD_TIPO_ELEMENTO
                           FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO
                                   FROM REL_ELEMENTI_AMM
                                  WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                )
                          INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO
                        )
                  ORDER BY 1 NULLS FIRST,2
                )
    LOOP
        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Ins Rel AMM - '||vRow.COD_PADRE||'/'||vRow.COD_FIGLIO||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_PADRE)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_FIGLIO)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        PKG_GestAnagr.InitRow (vAMM);
        PKG_GestAnagr.AddVal  (vAMM, 'COD_ELEMENTO_PADRE',  vRow.COD_PADRE);
        PKG_GestAnagr.AddVal  (vAMM, 'COD_ELEMENTO_FIGLIO', vRow.COD_FIGLIO);
        PKG_GestAnagr.Elabora (vAMM);
    END LOOP;
    PKG_Logs.StdLogAddTxt('AMM - Relazioni inserite         : '||vCount1,TRUE,NULL);

    -- Disattivazione relazioni non piu' presenti
    vCount1 := 0;
    FOR vRow IN (SELECT *
                  FROM (SELECT COD_ELEMENTO_PADRE COD_PADRE,COD_ELEMENTO_FIGLIO COD_FIGLIO,COD_TIPO_ELEMENTO
                          FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO
                                  FROM REL_ELEMENTI_AMM
                                 WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                               )
                         INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO
                        MINUS
                        SELECT DISTINCT NVL(P.COD_ELEMENTO,PKG_Elementi.GetElementoBase) COD_PADRE,
                                F.COD_ELEMENTO COD_FIGLIO,F.COD_TIPO_ELEMENTO
                          FROM GTTD_IMPORT_GERARCHIA A
                          LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                         INNER JOIN ELEMENTI F ON F.COD_GEST_ELEMENTO = COD_GEST_FIGLIO
                       )
                 ORDER BY 1 NULLS FIRST,2
                )
    LOOP
        vCount1 := vCount1 + 1;
        IF PKG_Logs.gVerboseLog THEN
            PKG_Logs.TraceLog('  Del Rel AMM - '||vRow.COD_PADRE||'/'||vRow.COD_FIGLIO||'  -  ( '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_PADRE)),'<null>')||' / '||
                               NVL(TO_CHAR(PKG_Elementi.GetGestElemento(vRow.COD_FIGLIO)),'<null>')||' )',PKG_UtlGlb.gcTRACE_INF);
        END IF;
        UPDATE REL_ELEMENTI_AMM
           SET DATA_DISATTIVAZIONE = pData - PKG_Misure.cUnSecondo
         WHERE COD_ELEMENTO_PADRE = vRow.COD_PADRE
           AND COD_ELEMENTO_FIGLIO = vRow.COD_FIGLIO
           AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

    END LOOP;
    PKG_Logs.StdLogAddTxt('AMM - Relazioni disattivate      : '||vCount1,TRUE,NULL);

 END ElaboraGerarchiaAMM;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ElaboraGerarchia         (pData          IN DATE,
                                     pStato         IN INTEGER,
                                     pModAssetto    IN BOOLEAN,
                                     pAUI           IN BOOLEAN,
                                     pElabSincrona  IN BOOLEAN,
                                     pForzaCompleta IN BOOLEAN) AS
/*-----------------------------------------------------------------------------------------------------------
    Pilota l'elaborazione della gerarchia
-----------------------------------------------------------------------------------------------------------*/

    vStatoDes   VARCHAR2(10);
    vDescr      VARCHAR2(200);
    vNum        INTEGER;

    vDiffAnagr  BOOLEAN;
    vDiffRelCP  BOOLEAN;
    vDiffRelCS  BOOLEAN;

 BEGIN

    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN vStatoDes := 'Attuale';
        WHEN PKG_Mago.gcStatoNormale THEN vStatoDes := 'Normale';
    END CASE;

    vDescr := vStatoDes ;

    IF pModAssetto THEN
        vDescr := vDescr || ' - Modifiche assetto rete (NEA)';
    ELSE
        vDescr := vDescr || ' - Files DAT';
    END IF;

    IF pAUI THEN
        vDescr := vDescr || ' - Modifiche AUI';
    END IF;

    PKG_Logs.Tracelog('Inizio elaborazione:  Stato '||vDescr||' - data rif: '||pkg_Mago.StdOutDate(pData));

    PKG_Logs.StdLogAddTxt(SUBSTR(vDescr||' '||RPAD('-',50,'-'),1,50),FALSE,NULL);

    IF pAUI OR pForzaCompleta THEN
        -- forza l'elaborazione completa
        PKG_Logs.StdLogAddTxt('Forzata elaborazione completa',FALSE,NULL);
    END IF;

    ElaboraGerarchiaIMP(pData, pStato, pModAssetto, vDiffAnagr, vDiffRelCP, vDiffRelCS);

    IF pAUI OR pForzaCompleta THEN
        -- forza l'elaborazione completa
        vDiffAnagr := TRUE;
        vDiffRelCP := TRUE;
        vDiffRelCS := TRUE;
    END IF;

    IF (pModAssetto = FALSE) AND (vDiffAnagr = FALSE) THEN
        -- verifica se in un 'giro' precedente ma di pari data (es SA se stato corrente � SN)
        -- ci sono state variazioni sui dati anagrafici
         SELECT COUNT(*) INTO vNum
           FROM ELEMENTI_DEF
          WHERE DATA_ATTIVAZIONE = pData
             OR DATA_DISATTIVAZIONE = pData - PKG_Misure.cUnSecondo;
         IF vNum > 0 THEN
            vDiffAnagr := TRUE;
         END IF;
    END IF;

    IF (NOT vDiffAnagr) AND
       (NOT vDiffRelCP) AND
       (NOT vDiffRelCS) THEN
        PKG_Logs.StdLogAddTxt('Nessuna azione da intraprendere su Relazioni o Aggregazione Misure',FALSE,NULL);
    ELSE
        IF (vDiffAnagr = TRUE) AND (pModAssetto = FALSE) THEN
            IF pElabSincrona THEN
                PKG_Logs.StdLogAddTxt(' > Calcolo misure statiche stato '||vStatoDes,FALSE,NULL);
                PKG_Misure.CalcMisureStatiche(pData,PKG_Mago.gcOrganizzazELE,pStato,FALSE);
            ELSE
                PKG_Logs.StdLogAddTxt('Richiedo calcolo misure statiche',FALSE,NULL);
                PKG_Scheduler.AddJobCalcMisStatiche(pData);
            END IF;
        END IF;
        IF (vDiffRelCP = TRUE) OR (vDiffRelCS = TRUE) THEN
            -- forzo sempre la linearizzazione sincrona per org Elettrica
            -- poiche' l'elaborazione delle gerarchie AMM e GEO richiede la compilazione della gerarchia IMP linearizzata
            PKG_Logs.StdLogAddTxt(' > linearizzazione org Elettrica stato '||vStatoDes,FALSE,NULL);
            PKG_Aggregazioni.LinearizzaGerarchia(NULL,pData,PKG_Mago.gcOrganizzazELE,pStato);
            IF pStato = PKG_Mago.gcStatoAttuale THEN
                ElaboraGerarchiaGEO(pData);
                IF pElabSincrona THEN
                    PKG_Logs.StdLogAddTxt(' > linearizzazione org Istat stato '||vStatoDes,FALSE,NULL);
                    PKG_Aggregazioni.LinearizzaGerarchia(NULL,pData,PKG_Mago.gcOrganizzazGEO,pStato);
                ELSE
                    PKG_Logs.StdLogAddTxt('Richiedo linearizzazione org Istat stato '||vStatoDes,FALSE,NULL);
                    PKG_Scheduler.AddJobLinearizzazione(pData, PKG_Mago.gcOrganizzazGEO, pStato);
                END IF;
                ElaboraGerarchiaAMM(pData);
                IF pElabSincrona THEN
                    PKG_Logs.StdLogAddTxt(' > linearizzazione org Amministrativa stato '||vStatoDes,FALSE,NULL);
                    PKG_Aggregazioni.LinearizzaGerarchia(NULL,pData,PKG_Mago.gcOrganizzazAMM,pStato);
                ELSE
                    PKG_Logs.StdLogAddTxt('Richiedo linearizzazione org Amministrativa stato '||vStatoDes,FALSE,NULL);
                    PKG_Scheduler.AddJobLinearizzazione(pData, PKG_Mago.gcOrganizzazAMM, pStato);
                END IF;
            END IF;
        END IF;
        IF pElabSincrona THEN
            -- esegue il ri-calcolo delle aggregate statiche data pari o successiva alla data di modifica gerarchia
            DELETE FROM GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey;
            INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1)
                     SELECT DISTINCT PKG_Mago.gcTmpTipMisKey, COD_TIPO_MISURA
                       FROM TIPI_MISURA
                      WHERE PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = PKG_UtlGlb.gkFlagON;
            PKG_Logs.StdLogAddTxt(' > aggregazione misure statiche org Elettrica stato '||vStatoDes,FALSE,NULL);
            PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazELE,pStato,pData,PKG_UtlGlb.gkFlagON);
            IF pStato = PKG_Mago.gcStatoAttuale THEN
                PKG_Logs.StdLogAddTxt(' > aggregazione misure statiche org Istat stato '||vStatoDes,FALSE,NULL);
                PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazGEO,pStato,pData,PKG_UtlGlb.gkFlagON);
                PKG_Logs.StdLogAddTxt(' > aggregazione misure statiche org Amministrativa stato '||vStatoDes,FALSE,NULL);
                PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazAMM,pStato,pData,PKG_UtlGlb.gkFlagON);
            END IF;
        ELSE
            -- richiede il ri-calcolo delle aggregate statiche data pari o successiva alla data di modifica gerarchia
            FOR i IN (SELECT COD_TIPO_MISURA
                        FROM TIPI_MISURA
                       WHERE PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = PKG_UtlGlb.gkFlagOn) LOOP
                PKG_Logs.StdLogAddTxt(' > richiesta aggregazione misure statiche org Elettrica stato '||vStatoDes,FALSE,NULL);
                PKG_Scheduler.AddJobCalcAggregazione(NULL,pData,PKG_Mago.gcOrganizzazELE,pStato,PKG_Mago.gcTipReteMT,i.COD_TIPO_MISURA);
                IF pStato = PKG_Mago.gcStatoAttuale THEN
                    PKG_Logs.StdLogAddTxt(' > richiesta aggregazione misure statiche org Istat stato '||vStatoDes,FALSE,NULL);
                    PKG_Scheduler.AddJobCalcAggregazione(NULL,pData,PKG_Mago.gcOrganizzazGEO,pStato,PKG_Mago.gcTipReteBT,i.COD_TIPO_MISURA);
                    PKG_Logs.StdLogAddTxt(' > richiesta aggregazione misure statiche org Amministrativa stato '||vStatoDes,FALSE,NULL);
                    PKG_Scheduler.AddJobCalcAggregazione(NULL,pData,PKG_Mago.gcOrganizzazAMM,pStato,PKG_Mago.gcTipReteBT,i.COD_TIPO_MISURA);
                END IF;
            END LOOP;
        END IF;
        IF vDiffAnagr THEN
            EXECUTE IMMEDIATE 'TRUNCATE TABLE V_SEARCH_ELEMENTS DROP STORAGE';
            DBMS_MVIEW.REFRESH('V_SEARCH_ELEMENTS', 'CF');
        END IF;
        -- richiede il ri-calcolo delle aggregate standard, gia' presenti nel sistema,
        -- con data pari o successiva alla data di modifica gerarchia
        MERGE INTO SCHEDULED_TMP_GEN B
             USING (SELECT DATA, ORGANIZZAZIONE, TIPO_AGGREGAZIONE STATO, ID_RETE, COD_TIPO_MISURA
                      FROM MISURE_AGGREGATE
                     INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                     INNER JOIN TIPI_RETE USING (COD_TIPO_RETE)
                     WHERE DATA >= pData
                       AND TIPO_AGGREGAZIONE = pStato
                     GROUP BY DATA, ORGANIZZAZIONE, TIPO_AGGREGAZIONE, ID_RETE, COD_TIPO_MISURA
                   ) A
               ON (B.DATARIF = A.DATA
              AND B.ORGANIZZAZIONE = A.ORGANIZZAZIONE
              AND B.STATO = A.STATO
              AND B.ID_RETE = A.ID_RETE
              AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
             WHEN NOT MATCHED THEN
                    INSERT (DATARIF, ORGANIZZAZIONE,   STATO,   ID_RETE,   COD_TIPO_MISURA)
                    VALUES (A.DATA,  A.ORGANIZZAZIONE, A.STATO, A.ID_RETE, A.COD_TIPO_MISURA);
        IF SQL%ROWCOUNT > 0 THEN
            PKG_Logs.StdLogAddTxt('Stato '||vStatoDes||' -  Richiedo ri-aggregazione delle misure successive  ('||SQL%ROWCOUNT||')',FALSE,NULL);
        ELSE
            PKG_Logs.StdLogAddTxt('Stato '||vStatoDes||' -  Nessuna ri-aggregazione da richiedere',FALSE,NULL);
        END IF;
    END IF;

    --PKG_Logs.Tracelog('Fine  elaborazione:  Stato '||vDescr||' - data rif: '||pkg_Mago.StdOutDate(pData));

 END ElaboraGerarchia;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitApplMagoSTM    (pCodGestESE    IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pData          IN DATE DEFAULT TRUNC(SYSDATE)) AS
  vCodGestESE   ELEMENTI.COD_GEST_ELEMENTO%TYPE := UPPER(pCodGestESE);
  vCodCO        ELEMENTI.COD_ELEMENTO%TYPE := NULL;
  vCodESE       ELEMENTI.COD_ELEMENTO%TYPE := NULL;
  vELE          PKG_GestAnagr.t_DefAnagr;
  vInserito     BOOLEAN := FALSE;
  vNum          INTEGER;
BEGIN

 SELECT COUNT(*) INTO vNum FROM DEFAULT_CO WHERE FLAG_PRIMARIO = 1;
 IF vNum > 0 THEN
    RETURN;  -- DEFAULT_CO gi� inizializzato
 END IF;

 PKG_Mago.gInizializzazione_In_Corso := TRUE;

 DELETE DEFAULT_CO;

 PKG_GestAnagr.InitTab  (vELE,pData,USER,'ELEMENTI_DEF','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
 PKG_GestAnagr.AddCol   (vELE,'COD_ELEMENTO',        PKG_GestAnagr.cColChiave);
 PKG_GestAnagr.AddCol   (vELE,'NOME_ELEMENTO',       PKG_GestAnagr.cColAttributo);
 PKG_GestAnagr.AddCol   (vELE,'COD_TIPO_ELEMENTO',   PKG_GestAnagr.cColAttributo);
 PKG_GestAnagr.AddCol   (vELE,'FLAG',                PKG_GestAnagr.cColAttributo);

 IF vCodGestESE IS NULL THEN
    RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,'Parametro obbligatorio - Codice gestionale esercizio principale');
 END IF;

 FOR I IN (SELECT *
             FROM (SELECT PKG_Mago.gcCentroOperativo||PKG_Mago.cSeparatore||U.CODIFICA_STM GST_CO, UPPER(U.NOME) NOME_CO,
                          U.CODIFICA_STM||E.CODIFICA_ESERCIZIO GST_ESE, UPPER(E.NOME) NOME_ESE,
                          CASE U.CODIFICA_STM||E.CODIFICA_ESERCIZIO
                            WHEN vCodGestESE THEN 1
                                             ELSE 0
                          END FLAG
                     FROM SAR_ADMIN.ESERCIZI E
                    INNER JOIN SAR_ADMIN.UNITA_TERRITORIALI U ON(U.COD_UTR = E.COD_UTR)
                    INNER JOIN (SELECT E.COD_UTR
                                  FROM SAR_ADMIN.ESERCIZI E
                                 INNER JOIN SAR_ADMIN.UNITA_TERRITORIALI U ON (E.COD_UTR = U.COD_UTR)
                                 WHERE E.COD_UTR BETWEEN 1 AND 99
                                   AND E.COD_ESERCIZIO BETWEEN 1 AND 99
                                   AND U.CODIFICA_STM||E.CODIFICA_ESERCIZIO = vCodGestESE
                               ) EU ON (E.COD_UTR = EU.COD_UTR)
                    WHERE COD_ESERCIZIO BETWEEN 1 AND 99
                  )
            ORDER BY FLAG DESC, GST_ESE
          )
 LOOP

    IF vCodCO IS NULL THEN
        vCodCO  := PKG_Elementi.InsertElement(I.GST_CO,PKG_Mago.gcCentroOperativo);
        PKG_GestAnagr.InitRow (vELE);
        PKG_GestAnagr.AddVal  (vELE,'COD_ELEMENTO',        vCodCO);
        PKG_GestAnagr.AddVal  (vELE,'NOME_ELEMENTO',       i.NOME_CO);
        PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_ELEMENTO',   PKG_Mago.gcCentroOperativo);
        PKG_GestAnagr.Elabora (vELE);
    END IF;

    vCodESE := PKG_Elementi.InsertElement(I.GST_ESE,PKG_Mago.gcEsercizio);
    PKG_GestAnagr.InitRow (vELE);
    PKG_GestAnagr.AddVal  (vELE,'COD_ELEMENTO',        vCodESE);
    PKG_GestAnagr.AddVal  (vELE,'NOME_ELEMENTO',       i.NOME_ESE);
    PKG_GestAnagr.AddVal  (vELE,'COD_TIPO_ELEMENTO',   PKG_Mago.gcEsercizio);
    PKG_GestAnagr.AddVal  (vELE,'FLAG',                i.FLAG);
    PKG_GestAnagr.Elabora (vELE);

    IF i.FLAG = 1 THEN
        vInserito := TRUE;
        INSERT INTO DEFAULT_CO (COD_ELEMENTO_CO, COD_ELEMENTO_ESE, TIPO_INST,          FLAG_PRIMARIO, START_DATE )
                        VALUES (vCodCO,          vCodESE,          PKG_Mago.gcMagoSTM, i.FLAG,        pData);
        PKG_Elementi.SetElemBaseEseDef (vCodCO,vCodESE);
    ELSE
        INSERT INTO DEFAULT_CO (COD_ELEMENTO_CO, COD_ELEMENTO_ESE, TIPO_INST,          FLAG_PRIMARIO)
                        VALUES (vCodCO,          vCodESE,          PKG_Mago.gcMagoSTM, i.FLAG);
    END IF;

 END LOOP;

 IF NOT vInserito THEN
    RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkErrElaborazione,'Esercizio : '''||vCodGestESE||''' non definito IN SAR_ADMIN! - Inizializzazione fallita');
 END IF;

 COMMIT;

 PKG_Mago.gInizializzazione_In_Corso := FALSE;

END InitApplMagoSTM;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetListaAttualizzazioni (pRefCurs   OUT PKG_UtlGlb.t_query_cur) AS
BEGIN
    OPEN pRefCurs FOR
           SELECT st.DATA_IMPORT,
                  SN.TIPO sn,
                  Sa.TIPO sa
             FROM (SELECT DISTINCT DATA_IMPORT
                     FROM STORICO_IMPORT
                     WHERE DATA_IMPORT >= TRUNC(SYSDATE) - cGiorniMantenimentoStorico
                  ) ST
             LEFT OUTER JOIN (SELECT DATA_IMPORT, cStatoNormale TIPO
                                FROM STORICO_IMPORT
                               WHERE DATA_IMPORT >= TRUNC(SYSDATE) - cGiorniMantenimentoStorico
                                 AND TIPO = cStatoNormale
                             ) SN ON SN.DATA_IMPORT = ST.DATA_IMPORT
             LEFT OUTER JOIN (SELECT DATA_IMPORT, cStatoAttuale TIPO
                                FROM STORICO_IMPORT
                               WHERE DATA_IMPORT >= TRUNC(SYSDATE) - cGiorniMantenimentoStorico
                                 AND TIPO = cStatoAttuale
                             ) SA ON SA.DATA_IMPORT = ST.DATA_IMPORT;
END GetListaAttualizzazioni;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckCodGestCorele  (pStato          IN INTEGER,
                               pBlockOnly      IN BOOLEAN,
                               pData           IN DATE DEFAULT SYSDATE) AS

    vMsg        GTTD_VALORI_TEMP.ALF3%TYPE;
    vSql        VARCHAR2(4000) :=
                   'INSERT INTO GTTD_VALORI_TEMP (TIP, NUM1, ALF3, ALF2, ALF1, ALF4, ALF5) '                                                                  ||
                    'SELECT :tip, E.COD_ELEMENTO, COD_ENTE, CODICE_ST, COD_GEST, COD_TIPO_ELEMENTO, NOME '                                                    ||
                      'FROM (SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME_TRASF nome FROM CORELE.AVVOLGIMENTI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '     ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.IMPIANTIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.CLIENTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.CONGIUNTORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.MONTANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.PARALLELI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.RIFASATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.SBARRE_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '            ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.SEZIONATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.TRASFORMATORIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.TRASFORMATORIBT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                                         'AND INSTR(UPPER(COD_GEST),''FIT'') = 0 AND INSTR(UPPER(NOME),''FIT'') = 0 '                                         ||
                            'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.TRASLATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT DISTINCT COD_ENTE,SUBSTR(CODICE_ST,1,LENGTH(CODICE_ST)-2)CODICE_ST,COD_GEST,NOME FROM CORELE.IMPIANTIMT_SA WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT ''SBCS'' COD_ENTE,CODICE_ST,COD_GEST_SBARRA,NOME FROM CORELE.IMPIANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE ' ||
                            --'UNION ALL SELECT COD_ENTE,CODICE_ST,COD_GEST,NOME FROM CORELE.IMPIANTIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '      ||
                           ') A '                                                                                                                                 ||
                      'LEFT OUTER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = A.COD_GEST';
BEGIN

    DELETE GTTD_VALORI_TEMP WHERE TIP = cTipTmpChk;

    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN vSql := REPLACE(vSql,'_##',cStatoAttualeSuffisso);
        WHEN PKG_Mago.gcStatoNormale THEN vSql := REPLACE(vSql,'_##',cStatoNormaleSuffisso);
    END CASE;

    EXECUTE IMMEDIATE vSql USING cTipTmpChk,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData,pData; --,pData;

    /*
    Errori bloccanti';
    */

    MERGE INTO GTTD_VALORI_TEMP T
         USING (SELECT SUM(TIPO) TIPO,ALF1,ALF2,ALF3
                  FROM (SELECT cErrBlockCodGestDupl TIPO,ALF1,ALF2,ALF3
                          FROM GTTD_VALORI_TEMP A
                         INNER JOIN (SELECT ALF1
                                       FROM GTTD_VALORI_TEMP
                                      WHERE TIP = cTipTmpChk
                                        AND ALF3 <> 'CPAT'
                                        --AND NUM1 IS NOT NULL
                                      GROUP BY ALF1 HAVING COUNT(*) > 1
                                    ) USING (ALF1)
                         WHERE TIP = cTipTmpChk
                        UNION ALL
                        SELECT cErrBlockCodGestNDef TIPO,ALF1,ALF2,ALF3
                          FROM GTTD_VALORI_TEMP A
                         WHERE SUBSTR(ALF1,1,1) = '?'
                           AND TIP = cTipTmpChk
                       )
                GROUP BY ALF1,ALF2,ALF3
               ) V
            ON (T.ALF1=V.ALF1 AND T.ALF2=V.ALF2 AND T.ALF3=V.ALF3 AND T.TIP=cTipTmpChk)
    WHEN MATCHED THEN
        UPDATE SET T.NUM2 = V.TIPO;

    IF NOT pBlockOnly THEN
        /*
        Errori non bloccanti';
        */
        MERGE INTO GTTD_VALORI_TEMP T
             USING (
                    SELECT SUM(TIPO) TIPO,ALF1,ALF2,ALF3
                      FROM (SELECT cWarningLowerCodGest TIPO,ALF1,ALF2,ALF3
                              FROM GTTD_VALORI_TEMP A
                             WHERE UPPER( ALF1) !=  ALF1
                               AND SUBSTR(ALF1,1,1) != '?'
                               AND TIP = cTipTmpChk
                            UNION ALL
                            SELECT cWarningBadCodGest TIPO, ALF1,ALF2,ALF3
                              FROM (SELECT ALF1,ALF2,ALF3
                                      FROM GTTD_VALORI_TEMP A
                                     WHERE ALF4 = PKG_Mago.gcSbarraCabSec
                                       AND UPPER (SUBSTR (ALF1, -2, 1)) NOT IN ('A', 'B', 'C', 'D', 'E', 'F', 'G')
                                       AND SUBSTR (ALF1, 1, 1) != '?'
                                       AND UPPER (SUBSTR (ALF1, -2, 2)) != 'RO'
                                       AND UPPER (SUBSTR (ALF1, -2, 2)) != 'VE'
                                       AND UPPER (SUBSTR (ALF1, -2, 2)) != 'NE'
                                       AND TIP = cTipTmpChk
                                    UNION ALL
                                    SELECT ALF1,ALF2,ALF3
                                      FROM GTTD_VALORI_TEMP A
                                     WHERE ALF4 IN (PKG_Mago.gcProduttoreAT,PKG_Mago.gcProduttoreMT,PKG_Mago.gcProduttoreBT)
                                       AND UPPER (SUBSTR (ALF1, -3, 1)) != 'U'
                                       AND SUBSTR(ALF1,1,1) != '?'
                                       AND TIP = cTipTmpChk
                                    UNION ALL
                                    SELECT ALF1,ALF2,ALF3
                                      FROM GTTD_VALORI_TEMP A
                                     WHERE ALF4 = PKG_Mago.gcTrasformMtBt
                                       AND UPPER (SUBSTR (ALF1, -3, 1)) != 'T'
                                       AND SUBSTR(ALF1,1,1) != '?'
                                       AND TIP = cTipTmpChk
                                   ) A
                           )
                    GROUP BY ALF1,ALF2,ALF3
                   ) V
                ON (T.ALF1=V.ALF1 AND T.ALF2=V.ALF2 AND T.ALF3=V.ALF3 AND T.TIP=cTipTmpChk)
        WHEN MATCHED THEN
            UPDATE SET T.NUM2 = NVL(T.NUM2,0) + V.TIPO;
    END IF;

    DELETE GTTD_VALORI_TEMP WHERE NUM2 IS NULL AND TIP = cTipTmpChk;

END CheckCodGestCorele;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElementsCheckReport (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pStato          IN INTEGER,
                               pData           IN DATE) AS
BEGIN

    PKG_Anagrafiche.CheckCodGestCorele(pStato,FALSE,pData);

    OPEN pRefCurs FOR
        SELECT ERR_BLOK,
               ERR_TIPO,
               COD_SG,
               COD_ST,
               TIP_ELE,
               NOM_ELE,
               SORT1 TIPO
          FROM (SELECT 1 SORT1,
                       1 ERR_BLOK,
                       'Codice gestionale duplicato' ERR_TIPO,
                       ALF1 COD_SG,
                       ALF2 COD_ST,
                       ALF3 TIP_ELE,
                       ALF5 NOM_ELE
                  FROM GTTD_VALORI_TEMP
                 WHERE BITAND (NUM2,1) = 1
                UNION ALL
                SELECT 2 SORT1,
                       1 ERR_BLOK,
                       'Codice Gestionale non definito' ERR_TIPO,
                       ALF1 COD_SG,
                       ALF2 COD_ST,
                       ALF3 TIP_ELE,
                       ALF5 NOM_ELE
                  FROM GTTD_VALORI_TEMP
                 WHERE BITAND (NUM2,2) = 2
                UNION ALL
                SELECT 3 SORT1,
                       0 ERR_BLOK,
                       'Caratteri minuscoli in codice gestionale' ERR_TIPO,
                       ALF1 COD_SG,
                       ALF2 COD_ST,
                       ALF3 TIP_ELE,
                       ALF5 NOM_ELE
                  FROM GTTD_VALORI_TEMP
                 WHERE BITAND (NUM2,16) = 16
                UNION ALL
                SELECT 4 SORT1,
                       0 ERR_BLOK,
                       'Codice Gestionale non standard' ERR_TIPO,
                       ALF1 COD_SG,
                       ALF2 COD_ST,
                       ALF3 TIP_ELE,
                       ALF5 NOM_ELE
                  FROM GTTD_VALORI_TEMP
                 WHERE BITAND (NUM2,32) = 32
               )
        ORDER BY SORT1,
                 CASE SORT1
                    WHEN 1 THEN 0
                    ELSE CASE TIP_ELE
                           WHEN 'ESER'      THEN 1
                           WHEN 'CPAT'      THEN 2
                           WHEN 'SBAT'      THEN 3
                           WHEN 'PRIM'      THEN 4
                           WHEN 'TRASF'     THEN 5
                           WHEN 'SECN'      THEN 6
                           WHEN 'TERZ'      THEN 7
                           WHEN 'SBMT'      THEN 8
                           WHEN 'LINMT'     THEN 9
                           WHEN 'SBCS'      THEN 10
                           WHEN 'CLMT'      THEN 11
                           WHEN 'TRBT'      THEN 12
                           ELSE 99
                         END
                 END,
                 COD_SG,COD_ST;

END ElementsCheckReport;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AllineaAnagrafica  (pElabSincrona  IN INTEGER DEFAULT PKG_UtlGlb.gkFlagON,
                              pForzaCompleta IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOFF) AS

    vCntElaborazioni  INTEGER := 0;
    vDataInizio       DATE := SYSDATE;
    vTimestampInizio  TIMESTAMP(4);
    vTrovato          BOOLEAN := FALSE;
    vWait             BOOLEAN := FALSE;
    vInizializzato    INTEGER := 0;
    vCodEseDefault    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    cOrigineAUI       CONSTANT STORICO_IMPORT.ORIGINE%TYPE := 'AUI';
    cStatoAUI         CONSTANT STORICO_IMPORT.TIPO%TYPE    := 'AUI';
    cOrigineCORELE    CONSTANT STORICO_IMPORT.ORIGINE%TYPE := 'CORELE';
    cModificheAssetto CONSTANT STORICO_IMPORT.TIPO%TYPE    := 'NEA';

    vElabSincrona     BOOLEAN := PKG_UtlGlb.FlagToBoolean(pElabSincrona);
    vForzaCompleta    BOOLEAN := PKG_UtlGlb.FlagToBoolean(pForzaCompleta);

    vNum              INTEGER;
BEGIN
--vForzaCompleta := true;
    SELECT COUNT(*) INTO vInizializzato FROM DEFAULT_CO WHERE FLAG_PRIMARIO = 1;

    IF vInizializzato = 0 THEN
        SELECT MIN(DATA_INIZIO) INTO vDataInizio FROM CORELE.ESERCIZI;
        IF vDataInizio IS NULL THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'PKG_Anagrafiche.AllineaAnagrafica - '||
                                    'Nessun Esercizio definito in CORELE - Verificare se Corele e'' inizializzato');
        END IF;
--        SELECT CODIFICA_UTR||CODIFICA_ESERCIZIO INTO vCodEseDefault
--          FROM SAR_ADMIN.V_ESERCIZI_MAGO
--         INNER JOIN SAR_ADMIN.ESERCIZI USING(COD_UTR,COD_ESERCIZIO,CODIFICA_ESERCIZIO)
--         INNER JOIN SAR_ADMIN.UNITA_TERRITORIALI USING(COD_UTR);
        BEGIN
            --SELECT U.CODIFICA_UTR||E.CODIFICA_ESERCIZIO INTO vCodEseDefault
            --  FROM UNITA_TERRITORIALI@PKG1_STMAUI.IT U,
            --       ESERCIZI@PKG1_STMAUI.IT E
            -- WHERE U.COD_UTR=E.COD_UTR
            --   AND FLAG_COMPETENZA_AUI=1
            --   AND E.CODIFICA_ESERCIZIO <> '00';
            SELECT ESECOMP INTO vCodEseDefault
              FROM UPDATE_AUI_TLC@PKG1_STMAUI.IT
              WHERE TIPO = 'T';
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'PKG_Anagrafiche.AllineaAnagrafica - '||
                                        'AUI non presente o FLAG_COMPETENZA_AUI=1 non valorizzato in tabella ESERCIZI@PKG1_STMAUI.IT ');
            WHEN OTHERS THEN RAISE;
        END;
        InitApplMagoSTM(vCodEseDefault,vDataInizio);
        vInizializzato := 1;
    END IF;

    vDataInizio := SYSDATE;

    LOOP

        vTrovato := FALSE;

        FOR i IN (SELECT NVL(STATO_AUI,0)    STATO_AUI,
                         NVL(STATO_CORELE,0) STATO_CORELE
                    FROM (SELECT SUM(STATO)  STATO_AUI
                            FROM UTILIZZO_TLC@pkg1_stmaui.it
                           WHERE UTENTE = 'TLC'
                         )
                    FULL OUTER JOIN
                         (SELECT SUM(VALORE) STATO_CORELE
                            FROM CORELE.SEMAFORO) ON 1=1
                 )
        LOOP

            IF i.STATO_AUI + i.STATO_CORELE > 0 THEN
                vWait := TRUE;
                DBMS_LOCK.SLEEP(10);
                EXIT;
            ELSE
                IF vWait = TRUE THEN
                    vWait := FALSE;
                END IF;
            END IF;
        END LOOP;

        FOR i IN (SELECT DATA_RIF, ORIGINE, TIPO
                    FROM (SELECT DATA_RIF, cOrigineAUI ORIGINE, cStatoNormale TIPO
                            FROM (SELECT MAX(DATA_AGG) DATA_RIF
                                    FROM UPDATE_AUI_TLC@pkg1_stmaui.it
                                   WHERE DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('30000101','yyyymmdd'))
                                     AND DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                                     AND DATA_AGG > (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                                FROM STORICO_IMPORT
                                                               WHERE ORIGINE = cOrigineAUI
                                                                 AND TIPO = cStatoNormale
                                                       )
                                    )
                           WHERE DATA_RIF IS NOT NULL
                          UNION
                          SELECT DATA_RIF, cOrigineAUI ORIGINE, cStatoAttuale TIPO
                            FROM (SELECT MAX(DATA_AGG) DATA_RIF
                                    FROM UPDATE_AUI_TLC@pkg1_stmaui.it
                                   WHERE DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('30000101','yyyymmdd'))
                                     AND DATA_AGG > NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                                     AND DATA_AGG > (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                                FROM STORICO_IMPORT
                                                               WHERE ORIGINE = cOrigineAUI
                                                                 AND TIPO = cStatoAttuale
                                                       )
                                    )
                           WHERE DATA_RIF IS NOT NULL
                          UNION
                          SELECT DATA_LAST_IMPORT DATA_RIF, cOrigineCORELE ORIGINE, cStatoNormale TIPO
                            FROM CORELE.DATE_IMPORT_CONSISTENZA
                           WHERE TIPO_IMPORT = cStatoNormale
                             --AND NVL(NUM_REC,1) > 0
                             AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                             AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                        FROM STORICO_IMPORT
                                                       WHERE ORIGINE = cOrigineCORELE
                                                         AND TIPO = cStatoNormale
                                                     )
                          UNION
                          SELECT DATA_LAST_IMPORT DATA_RIF, cOrigineCORELE ORIGINE, cStatoAttuale TIPO
                            FROM CORELE.DATE_IMPORT_CONSISTENZA
                           WHERE TIPO_IMPORT = cStatoAttuale
                             --AND NVL(NUM_REC,1) > 0
                             AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                             AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                        FROM STORICO_IMPORT
                                                       WHERE ORIGINE = cOrigineCORELE
                                                         AND TIPO = cStatoAttuale
                                                     )
                          UNION
                          SELECT DATA_LAST_IMPORT DATA_RIF, cOrigineCORELE ORIGINE, cModificheAssetto TIPO
                            FROM CORELE.DATE_IMPORT_CONSISTENZA A
                           --INNER JOIN CORELE.HIST_NEA_ISFILE B ON A.DATA_LAST_IMPORT = B.DATARIF
                           WHERE TIPO_IMPORT = cModificheAssetto
                             AND NUM_REC > 0
                             --AND TPTRASM <> 'GAST' /* GAST = Spadizione globale */
                             AND DATA_LAST_IMPORT >= NVL(PKG_Mago.GetStartDate,TO_DATE('19000101','yyyymmdd'))
                             AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                        FROM STORICO_IMPORT
                                                       WHERE ORIGINE = cOrigineCORELE
                                                         AND TIPO = cModificheAssetto
                                                     )
                             AND DATA_LAST_IMPORT >  (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd'))
                                                        FROM STORICO_IMPORT
                                                       WHERE ORIGINE = cOrigineCORELE
                                                         AND TIPO = cStatoAttuale
                                                     )
                         )
--WHERE DATA_RIF > (SELECT NVL(MAX(DATA_IMPORT),TO_DATE('19000101','yyyymmdd')) FROM STORICO_IMPORT)
                  ORDER BY DATA_RIF,
                           CASE TIPO
                             WHEN cStatoAttuale     THEN 1
                             WHEN cModificheAssetto THEN 2
                             WHEN cStatoNormale     THEN 3
                             ELSE                        9
                           END
                 )
        LOOP

            PKG_Logs.ResetLogCommonArea;

            PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassANA,
                                  pFunzione    => 'PKG_Anagrafiche.AllineaAnagrafica',
                                  pDataRif     => i.DATA_RIF,
                                  pTipo        => 'Origine',
                                  pCodice      => i.ORIGINE||' / '||i.TIPO,
                                  pStoreOnFile => FALSE);

            vCntElaborazioni := vCntElaborazioni + 1;

            vTrovato := TRUE;

            vTimestampInizio := CURRENT_TIMESTAMP;

            CASE
                WHEN i.ORIGINE = cOrigineAUI AND i.TIPO = cStatoNormale THEN
                     ElaboraGerarchia(i.DATA_RIF,PKG_Mago.gcStatoNormale,FALSE,TRUE,vElabSincrona,vForzaCompleta);
                     vTrovato := TRUE;
                WHEN i.ORIGINE = cOrigineAUI AND i.TIPO = cStatoAttuale THEN
                     ElaboraGerarchia(i.DATA_RIF,PKG_Mago.gcStatoAttuale,FALSE,TRUE,vElabSincrona,vForzaCompleta);
                     vTrovato := TRUE;
                WHEN i.ORIGINE = cOrigineCORELE AND i.TIPO = cStatoNormale THEN
                     ElaboraGerarchia(i.DATA_RIF,PKG_Mago.gcStatoNormale,FALSE,FALSE,vElabSincrona,vForzaCompleta);
                     vTrovato := TRUE;
                WHEN i.ORIGINE = cOrigineCORELE AND i.TIPO = cStatoAttuale THEN
                     ElaboraGerarchia(i.DATA_RIF,PKG_Mago.gcStatoAttuale,FALSE,FALSE,vElabSincrona,vForzaCompleta);
                     vTrovato := TRUE;
                WHEN i.ORIGINE = cOrigineCORELE AND i.TIPO = cModificheAssetto THEN
                     SELECT COUNT(*) INTO vNum
                       FROM STORICO_IMPORT
                      WHERE ORIGINE = cOrigineCORELE
                        AND TIPO  = cStatoAttuale
                        AND DATA_IMPORT >= i.DATA_RIF;
                     IF vNum = 0 THEN
                         INSERT INTO GTTD_MOD_ASSETTO_RETE_SA
                            SELECT *
                              FROM V_MOD_ASSETTO_RETE_SA
                             WHERE DATARIF = i.DATA_RIF;
                         ElaboraGerarchia(i.DATA_RIF,PKG_Mago.gcStatoAttuale,TRUE,FALSE,vElabSincrona,vForzaCompleta); --ElaboraModAssettoRete(i.DATA_RIF);
                         DELETE GTTD_MOD_ASSETTO_RETE_SA;
                     ELSE
                         PKG_Logs.Tracelog('File NEA delle '||pkg_Mago.StdOutDate(i.DATA_RIF)||' non elaborato.'||CHR(10)||
                                           'Un set files DAT di Stato Attuale con data pari o superiore gia'' elaborato.');
                     END IF;
                     vTrovato := TRUE;
                ELSE vTrovato := FALSE;
            END CASE;

            BEGIN
              INSERT INTO STORICO_IMPORT (ORIGINE,   DATA_IMPORT, TIPO,   DATA_ELAB_INI,    DATA_ELAB_FIN)
                                  VALUES (i.ORIGINE, i.DATA_RIF,  i.TIPO, vTimestampInizio, CURRENT_TIMESTAMP);
            EXCEPTION
              WHEN DUP_VAL_ON_INDEX THEN
                  UPDATE STORICO_IMPORT SET DATA_ELAB_INI = vTimestampInizio,
                                            DATA_ELAB_FIN = CURRENT_TIMESTAMP
                   WHERE ORIGINE     = i.ORIGINE
                     AND DATA_IMPORT = i.DATA_RIF
                     AND TIPO        = i.TIPO;
              WHEN OTHERS THEN
                                RAISE;
            END;

            PKG_Scheduler.ConsolidaJobAggregazione(NULL,TRUE);

            COMMIT;

            -- -- richiede l'elaborazione immediata del JOB di aggregazione;
            -- -- se gia' in corso la richiesta e' presa in carico dall'elaborazione in corso.
            -- BEGIN
            --     DBMS_SCHEDULER.RUN_JOB('MAGO_SCHEDULER',FALSE);
            -- EXCEPTION
            --     WHEN OTHERS THEN NULL;
            -- END;

            PKG_Logs.StdLogPrint;

            EXIT;

        END LOOP;

        IF vTrovato = FALSE THEN
            EXIT;
        END IF;

    END LOOP;

    -- richiede l'elaborazione immediata del JOB di aggregazione;
    -- se gia' in corso la richiesta e' presa in carico dall'elaborazione in corso.
    BEGIN
        DBMS_SCHEDULER.RUN_JOB('MAGO_SCHEDULER',FALSE);
    EXCEPTION
        WHEN OTHERS THEN NULL;
    END;

    -- esegue la pulizia dello storico mantenendo gli ultimi 60 gg di storia
    -- non elemina mai l'ultima registrazione relativa ad ogni ORTGINE/TIPO
    DELETE STORICO_IMPORT
     WHERE DATA_IMPORT  < TRUNC(SYSDATE) - cGiorniMantenimentoStorico
       AND (ORIGINE,TIPO,DATA_IMPORT) NOT IN (SELECT ORIGINE, TIPO, MAX(DATA_IMPORT)
                                               FROM STORICO_IMPORT
                                              GROUP BY ORIGINE, TIPO
                                             );

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         -- annulla gli aggiornamenti non ancora committati.
         ROLLBACK;
         -- richiede l'elaborazione immediata del JOB di aggregazione per le richieste gi� committate;
         -- se gia' in corso la richiesta e' presa in carico dall'elaborazione in corso.
         BEGIN
             DBMS_SCHEDULER.RUN_JOB('MAGO_SCHEDULER',FALSE);
         EXCEPTION
             WHEN OTHERS THEN NULL;
         END;
         COMMIT;
         PKG_Logs.StdLogInit (pClasseFunz => PKG_Mago.gcJobClassANA,
                              pFunzione   => 'PKG_Anagrafiche.AllineaAnagrafica');
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AllineaAnagrafica;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Anagrafiche;
/
