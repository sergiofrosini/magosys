--------------------------------------------------------
Prompt  Package Body PKG_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY PKG_REPORTS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.h
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gcEngr             CONSTANT NUMBER(1) := 1;
 gcPtnz             CONSTANT NUMBER(1) := 2;

 gcFatt_kW          CONSTANT NUMBER := 1 / 1000;
 --gcFatt_kVA         CONSTANT NUMBER := 1 / 0.9 / 1000;
 gcFatt_MVh         NUMBER;

 gcRepBody          CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'RepBody';

 gOraDesc           VARCHAR2(10);
 gDeltaTime         NUMBER;
 gDataDa            DATE;
 gDataA             DATE;

 gTipRetA           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetM           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetB           TIPI_RETE.COD_TIPO_RETE%TYPE;

 gOrganizzazione    NUMBER(1);
 gStatoRete         NUMBER(1);

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION DateDiff(pMask  IN VARCHAR2,
                   pD1    IN DATE,
                   pD2    IN DATE ) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce la differenza tra due date in ore o minuti
-----------------------------------------------------------------------------------------------------------*/
     vRes    NUMBER;
 BEGIN
    SELECT (pD2 - pD1) * CASE UPPER(pMask)
                           WHEN 'MI' THEN 24*60
                           WHEN 'HH' THEN 24
                         END
      INTO vRes
      FROM dual;
    RETURN vRes;
 END;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisElem               (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per l'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
 BEGIN
    SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT          = pFlagTab
       AND COD_ELEMENTO         = pCodElemento
       AND COD_TIPO_MISURA      = pTipMisura
       AND COD_TIPO_RETE       IN (gTipRetA,gTipRetM,gTipRetB)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN NULL;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisElem;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisFnte               (pFlagTab        IN INTEGER,
                                    pCodTipoFonte   IN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per la fonte ricevuta
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
 BEGIN
    SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT          = pFlagTab
       AND COD_RAGGR_FONTE      = pCodTipoFonte
       AND COD_TIPO_MISURA      = pTipMisura
       AND COD_TIPO_RETE       IN (gTipRetA,gTipRetM,gTipRetB)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN NULL;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisFnte;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitRepConsEnergia      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pDataDa         IN DATE,
                                    pDataA          IN DATE,
                                    pOrganizzazione IN NUMBER,
                                    pStatoRete      IN NUMBER,
                                    pTipologiaRete  IN VARCHAR2,
                                    pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipEleFiglio   IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;
    vCod            ELEMENTI.COD_ELEMENTO%TYPE;
    vGst            ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vFlg            INTEGER;
    vFlg2           INTEGER;
    vNom            ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vTip            TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTSt            TIPI_ELEMENTO.CODIFICA_ST%TYPE;
    vOre            NUMBER;
    vMin            NUMBER;

    vFlgNull        INTEGER;

    vElemento       ELEMENTI.COD_ELEMENTO%TYPE := pElementoPadre;
    vGestionale     ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome           ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

    vTmp           PKG_UtlGlb.t_SplitTbl;

    TYPE t_Fonti    IS TABLE OF RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE;
    vFonti          t_Fonti;

 BEGIN

    gOrganizzazione := pOrganizzazione;
    gStatoRete      := pStatoRete;

    OPEN pRefCurs FOR SELECT COUNT(*) fake FROM DUAL; -- cursore fasullo ma necessario al FE

    DELETE GTTD_VALORI_TEMP;

    gTipRetA := NULL;
    gTipRetM := NULL;
    gTipRetB := NULL;
    vTmp := PKG_UtlGlb.SplitString(pTipologiaRete,PKG_Mago.cSepCharLst);
    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcTipReteAT THEN
            gTipRetA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteMT THEN
            gTipRetM := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteBT THEN
            gTipRetB := vTmp(i);
        END IF;
    END LOOP;

    IF NVL(pElementoFiglio,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
        IF NVL(vElemento,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
            PKG_Elementi.GetDefaultCO (vElemento, vGestionale, vNome);
        END IF;
        PKG_Elementi.GetElements(vLst,
                                 vElemento,
                                 pDataDa,
                                 pDataA,
                                 gOrganizzazione,
                                 gStatoRete,
                                 NULL,NULL,NULL,
                                 PKG_Mago.gcOFF);
    ELSE
        OPEN vLst FOR
             SELECT pElementoFiglio,PKG_Mago.gcNullNumCode,COD_GEST_ELEMENTO,NULL,COD_TIPO_ELEMENTO,NULL,NULL
               FROM ELEMENTI
              WHERE COD_ELEMENTO = pElementoFiglio;
    END IF;

    IF pDataDa <> pDataA THEN
        gDeltaTime := DateDiff('HH', pDataDa, pDataA);
        vOre := TRUNC(gDeltaTime);
        vMin := (gDeltaTime - vOre) * 60;
        gcFatt_MVh := 1 / gDeltaTime / 1000000;
    ELSE
        gDeltaTime := 0;
        vOre := 0;
        vMin := 0;
        gcFatt_MVh := 1;
    END IF;

    gOraDesc   := TO_CHAR(vOre)||':'||TO_CHAR(vMin,'fm09');
    gDataDa    := pDataDa;
    gDataA     := pDataA + ((1/96) - (1/86400));
    --SELECT dataval - 1/86400 INTO gDataA FROM TABLE(PKG_UtlGlb.GetCalendario(pDataA,pDataA+1/96,96));

    LOOP
      FETCH vLst INTO vCod,vFlg,vGst,vNom,vTip,vTst,vFlg;
      EXIT WHEN vLst%NOTFOUND;
      INSERT INTO GTTD_VALORI_TEMP(TIP,NUM1,ALF1,ALF2)
                            VALUES(gcRepBody,
                                   vCod,
                                   SUBSTR(vGst,INSTR(vGst,PKG_Mago.cSeparatore)+1),
                                   vTip);
    END LOOP;
    CLOSE vLst;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.InitRepConsEnergia',
                                     pDataRif      => pDataDa,
                                     pDataRif_fine => pDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Org:'||NVL(pOrganizzazione,'<null>')||'-'||
                                                      'Sta:'||NVL(pStatoRete,'<null>')||'-'||
                                                      'Ret:'||NVL(pTipologiaRete,'<null>')||'-'||
                                                      'Padre:'||pElementoPadre||'-'||
                                                      'Figlio:'||pElementoFiglio);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END InitRepConsEnergia;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsEnergiaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Energia del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
 BEGIN
    DELETE FROM GTTD_REP_ENERGIA_POTENZA WHERE TIPO_REPORT = gcEngr;
    INSERT INTO GTTD_REP_ENERGIA_POTENZA (TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO,VALORE)
              SELECT TIPO_REPORT, COD_ELEMENTO, COD_RAGGR_FONTE, COD_TIPO_RETE, COD_TIPO_PRODUTTORE, COD_TIPO_MISURA, SEGNO, SUM(VALORE) VALORE
                FROM (SELECT gcEngr TIPO_REPORT ,NUM1 COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,
                             CASE
                                WHEN VALORE < 0 THEN '-'
                                ELSE                 '+'
                             END SEGNO,ABS(VALORE) VALORE
                        FROM (SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcPotenzaAttGenerata)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcPotenzaAttGenerata)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                            WHEN 1 THEN PKG_Mago.gcStatoNormale
                                                            ELSE gStatoRete
                                                         END
                             )
                       RIGHT OUTER JOIN GTTD_VALORI_TEMP  ON NUM1 = COD_ELEMENTO
                     )
               GROUP BY TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO;
    OPEN pRefCurs FOR
        SELECT NUM1 COD_ELEMENTO,
               ALF1 COD_GEST_ELEMENTO,
               ALF2 COD_TIPO_ELEMENTO,
               GetMisElem(gcEngr,NUM1,PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente,'+') * gcFatt_MVh E_prelevata,
               gOraDesc ORE_prelevata,
               GetMisElem(gcEngr,NUM1,PKG_Mago.gcPotenzaAttGenerata, PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente)     * gcFatt_MVh +
               GetMisElem(gcEngr,NUM1,PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente,'-') * gcFatt_MVh E_immessa,
               gOraDesc ORE_immessa,
               GetMisElem(gcEngr,NUM1,PKG_Mago.gcPotenzaAttGenerata, PKG_Mago.gcProduttorePuro,   NULL)                         * gcFatt_MVh E_gen,
               GetMisElem(gcEngr,NUM1,PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcProduttoreCliente,NULL)                         * gcFatt_MVh E_scamb
          FROM GTTD_VALORI_TEMP
         WHERE TIP = gcRepBody;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsEnergiaBody');
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsEnergiaBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsEnergiaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Energia del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
 BEGIN
    OPEN pRefCurs FOR
        SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
               GetMisFnte(gcEngr,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttGenerata,PKG_Mago.gcProduttorePuro,NULL)      * gcFatt_MVh  E_gen,
               GetMisFnte(gcEngr,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_MVh  E_scamb
          FROM RAGGRUPPAMENTO_FONTI
         ORDER BY ID_RAGGR_FONTE;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz  => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione    => 'PKG_Reports.GetRepConsEnergiaFooter');
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsEnergiaFooter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsPotenzaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pData           IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vDataA          DATE   := pData + ((1/96) - (1/86400));
 BEGIN
    DELETE FROM GTTD_REP_ENERGIA_POTENZA WHERE TIPO_REPORT = gcPtnz;
    INSERT INTO GTTD_REP_ENERGIA_POTENZA (TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO,VALORE)
              SELECT TIPO_REPORT, COD_ELEMENTO, COD_RAGGR_FONTE, COD_TIPO_RETE, COD_TIPO_PRODUTTORE, COD_TIPO_MISURA, SEGNO, SUM(VALORE) VALORE
                FROM (SELECT gcPtnz TIPO_REPORT ,NUM1 COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,
                             CASE
                                WHEN VALORE < 0 THEN '-'
                                ELSE                 '+'
                             END SEGNO,
                             ABS(VALORE) VALORE
                        FROM (SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               WHERE mis.DATA BETWEEN pData AND vDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcPotenzaAttGenerata,PKG_Mago.gcPotenzaMeteoPrevisto)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE mis.DATA BETWEEN pData AND vDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcPotenzaAttScambiata,PKG_Mago.gcPotenzaAttGenerata,PKG_Mago.gcPotenzaMeteoPrevisto)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                                WHEN 1 THEN PKG_Mago.gcStatoNormale
                                                                ELSE gStatoRete
                                                            END
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE_STATICHE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               WHERE pData BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE_STATICHE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP gttd ON gttd.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE pData BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                                WHEN 1 THEN PKG_Mago.gcStatoNormale
                                                                ELSE gStatoRete
                                                            END
                             )
                       RIGHT OUTER JOIN GTTD_VALORI_TEMP  ON NUM1 = COD_ELEMENTO
                     )
               GROUP BY TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO;
    OPEN pRefCurs FOR
        SELECT COD_ELEMENTO,
               COD_GEST_ELEMENTO,
               COD_TIPO_ELEMENTO,
               P_gen1,
               P_scamb1,
               P_prev_gen1,
               N_imp1,
               P_inst1,
               CASE NVL(P_inst1,0)
                 WHEN 0 THEN NULL
                 ELSE        P_gen1 / P_inst1
               END H_eq_ut1,
               P_gen2,
               P_scamb2,
               P_prev_gen2,
               N_imp2,
               P_inst2,
               P_contr2
          FROM (SELECT NUM1 COD_ELEMENTO,
                       ALF1 COD_GEST_ELEMENTO,
                       ALF2 COD_TIPO_ELEMENTO,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaAttGenerata,  PKG_Mago.gcProduttorePuro,NULL)     * gcFatt_kW  P_gen1,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaAttScambiata, PKG_Mago.gcProduttorePuro,NULL)     * gcFatt_kW  P_scamb1,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,NULL)     * gcFatt_kW  P_prev_gen1,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcNumeroImpianti,      PKG_Mago.gcProduttorePuro,NULL)                  N_imp1,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaInstallata,   PKG_Mago.gcProduttorePuro,NULL)     * gcFatt_kW /*gcFatt_kVA*/ P_inst1,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaAttGenerata,  PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_gen2,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaAttScambiata, PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_scamb2,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_prev_gen2,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcNumeroImpianti,      PKG_Mago.gcProduttoreCliente,NULL)              N_imp2,
                       GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaInstallata,   PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW /*gcFatt_kVA*/ P_inst2,
                       --GetMisElem(gcPtnz,NUM1,PKG_Mago.gcPotenzaContrattuale, PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_contr2
                       NULL  P_contr2
                  FROM GTTD_VALORI_TEMP
                 WHERE TIP = gcRepBody
               );
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody');
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsPotenzaBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsPotenzaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pData           IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vDataA          DATE   := pData + ((1/96) - (1/86400));
BEGIN
    OPEN pRefCurs FOR
        SELECT COD_TIPO_FONTE,
               P_gen1,
               P_scamb1,
               P_prev_gen1,
               N_imp1,
               P_inst1,
               CASE NVL(P_inst1,0)
                 WHEN 0 THEN NULL
                 ELSE        P_gen1 / P_inst1
               END H_eq_ut1,
               P_gen2,
               P_scamb2,
               P_prev_gen2,
               N_imp2,
               P_inst2,
               P_contr2
          FROM (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
                       ID_RAGGR_FONTE,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttGenerata,  PKG_Mago.gcProduttorePuro,NULL)    * gcFatt_kW  P_gen1,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttScambiata, PKG_Mago.gcProduttorePuro,NULL)    * gcFatt_kW  P_scamb1,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,NULL)    * gcFatt_kW  P_prev_gen1,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,      PKG_Mago.gcProduttorePuro,NULL)                 N_imp1,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,   PKG_Mago.gcProduttorePuro,NULL)    * gcFatt_kW /*gcFatt_kVA*/ P_inst1,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttGenerata,  PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_gen2,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaAttScambiata, PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_scamb2,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_prev_gen2,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,      PKG_Mago.gcProduttoreCliente,NULL)              N_imp2,
                       GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,   PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW /*gcFatt_kVA*/ P_inst2,
                       --GetMisFnte(gcPtnz,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaContrattuale, PKG_Mago.gcProduttoreCliente,NULL) * gcFatt_kW  P_contr2
                       NULL  P_contr2
                  FROM RAGGRUPPAMENTO_FONTI
               ) ORDER BY ID_RAGGR_FONTE;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody');
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsPotenzaFooter;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Reports;

/
