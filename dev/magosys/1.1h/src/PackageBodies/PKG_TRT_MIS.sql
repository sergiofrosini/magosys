--------------------------------------------------------
Prompt  Package Body PKG_TRT_MIS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY PKG_TRT_MIS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.i
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gStartDate         DATE;

 gTipoInstallazione DEFAULT_CO.TIPO_INST%TYPE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetFileElaborated(pFileinfo T_FILE_INFO_OBJ) IS

BEGIN

  INSERT INTO FILE_PROCESSED
  (
    NOME_FILE,
    TIPO_FILE,
    DATA_ELAB,
    PROCESSO
  )
  VALUES
  (
    pFileinfo.NOME_FILE,
    pFileinfo.TIPO_FILE,
    pFileinfo.DATA_ELAB,
    pFileinfo.PROCESSO
  );

  COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.SetFileElaborated'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetFileElaborated(pRefCurs  OUT PKG_UtlGlb.t_query_cur,
                           pNomefile   IN VARCHAR2 DEFAULT NULL,
                           pTipofile   IN VARCHAR2 DEFAULT NULL) IS

BEGIN

    OPEN pRefCurs FOR
        SELECT *
        FROM FILE_PROCESSED
        WHERE NOME_FILE = NVL(pNomefile,NOME_FILE)
        AND TIPO_FILE = NVL(pTipofile,TIPO_FILE)
        ORDER BY NOME_FILE, TIPO_FILE;

END;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN

PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Misure',
                          pStoreOnFile => FALSE);


END PKG_TRT_MIS;

/
