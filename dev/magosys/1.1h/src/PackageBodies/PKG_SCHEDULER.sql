--------------------------------------------------------
Prompt  Package Body PKG_SCHEDULER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY PKG_SCHEDULER AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.i
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

  --gcCaricaAnagrafiche     SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 0;
    gcLinearizzazione       SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 1;
    gcCalcolaMisureStat     SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 2;
    gcAggregMisureStat      SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 3; -- aggregazione misure statiche
    gcAggregMisureStd       SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 4; -- aggregazione misure standard (Prepara)
    -- gcAggregMisureStdCalc non piu' usato
--  gcAggregMisureStdCalc   SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 5; -- aggregazione misure standard (Calcola)

    gReuseJobNumber         NUMBER := NULL;


/* ***********************************************************************************************************
 Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

-- PROCEDURE AddJobLoadAnagr          (pDataRif         DATE,
--                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
--                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE) AS
--/*-----------------------------------------------------------------------------------------------------------
--    Inserisce una richiesta di elaborazione relativa al caricamento anagragica
-------------------------------------------------------------------------------------------------------------*/
-- BEGIN
--    -- Inserisco la richiesta per Data, Organizzazione e Stato Rete
--    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,            DATARIF,   ORGANIZZAZIONE,  STATO)
--                        VALUES (gcCaricaAnagrafiche, pDataRif,  pOrganizzazione, pStato);
-- EXCEPTION
--    WHEN DUP_VAL_ON_INDEX THEN NULL;
--    WHEN OTHERS THEN RAISE;

-- END AddJobLoadAnagr;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobLinearizzazione    (pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di elaborazione di linearizzazione gerarchia
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    -- Inserisco la richiesta per Data, Organizzazione e Stato Rete
    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,          DATARIF,   ORGANIZZAZIONE,  STATO)
                        VALUES (gcLinearizzazione, pDataRif,  pOrganizzazione, pStato);
 EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS THEN RAISE;

 END AddJobLinearizzazione;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobCalcMisStatiche    (pDataRif         DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di calcolo misura statica
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    -- Inserisco la richiesta per Data
    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,        DATARIF)
                        VALUES (gcCalcolaMisureStat, pDataRif);
 EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS THEN RAISE;

 END AddJobCalcMisStatiche;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggregazione (pTipo            VARCHAR2 DEFAULT NULL,
                                     pUseLog          BOOLEAN DEFAULT FALSE) AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabelle SCHEDULED_TMP_xxx
    - se parametro pTipo Null considera tabelle SCHEDULED_TMP_GEN e MIS
    - se parametro pTipo valorizzato considera la tabella relativa al valore ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vLog    PKG_Logs.t_StandardLog;
    vTipo   VARCHAR2(3) := NVL(pTipo,'GEN');
    vCur    PKG_UtlGlb.t_query_cur;
    vDatIns DATE;
    vAgrNum NUMBER := 0;
    vMaxNum NUMBER;
    vStart  DATE;
    vMinDat DATE;
    vMaxDat DATE;

    vJob    NUMBER;
    vDat    DATE;
    vOrg    NUMBER(1);
    vSta    NUMBER(1);
    vRet    TIPI_RETE.COD_TIPO_RETE%TYPE;
    vMis    TIPI_MISURA.COD_TIPO_MISURA%TYPE;

    vSql    VARCHAR2(1000) :=
              'SELECT JOB_NUMBER, DATARIF, ORGANIZZAZIONE, STATO, COD_TIPO_RETE, COD_TIPO_MISURA ' ||
                'FROM (SELECT DATARIF, ORGANIZZAZIONE, STATO, COD_TIPO_MISURA, '                   ||
                             'MIN(JOB_NUMBER) JOB_NUMBER, MAX(ID_RETE) ID_RETE '                   ||
                        'FROM SCHEDULED_TMP_#TAB# '                                                ||
                       'WHERE JOB_NUMBER <= :num '                                                 ||
                       'GROUP BY DATARIF, ORGANIZZAZIONE, STATO, COD_TIPO_MISURA '                 ||
                      ') '                                                                         ||
               'INNER JOIN TIPI_RETE USING(ID_RETE) '                                              ||
               'ORDER BY 1 ';

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.ConsolidaJobAggregazione',
                                pCodice       => NVL(pTipo,'<null>'),
                                pTipo         => 'Tipo misure',
                                pForceLogFile => TRUE);

    IF vTipo NOT IN ('MET','GME','GEN') THEN
        vTipo := 'GEN';
    END IF;

    BEGIN
        SELECT SYSDATE - (1 / 24 * RITARDO_HH) INTO vDatIns FROM SCHEDULED_JOBS_DEF WHERE TIPO_JOB = gcAggregMisureStd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    EXECUTE IMMEDIATE 'SELECT MAX(JOB_NUMBER) FROM SCHEDULED_TMP_'||vTipo INTO vMaxNum;

    OPEN vCur FOR REPLACE(vSql,'#TAB#',vTipo) USING vMaxNum;
    LOOP
        FETCH vCur INTO vJob, vDat, vOrg, vSta, vRet, vMis;
        EXIT WHEN vCur%NOTFOUND;
        gReuseJobNumber := vJob;
        AddJobCalcAggregazione(NULL,vDat,vOrg,vSta,vRet,vMis,vDatIns);
        vAgrNum := vAgrNum + 1;
    END LOOP;
    CLOSE vCur;

    IF pUseLog THEN
        IF vAgrNum > 0 THEN
            EXECUTE IMMEDIATE 'SELECT MIN(DATARIF), MAX(DATARIF), MIN(DATAINS) FROM SCHEDULED_TMP_'||vTipo||' '||
                               'WHERE JOB_NUMBER <= :num '
                         INTO vMinDat, vMaxDat, vStart USING vMaxNum ;
            vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                        pFunzione     => 'PKG_Scheduler.ConsolidaJobAggregazione',
                                        pTipo         => 'Tipo misure',
                                        pCodice       => vTipo,
                                        pNome         => 'num: '||TO_CHAR(vAgrNum),
                                        pDataRif      => vMinDat,
                                        pDataRif_fine => vMaxDat,
                                        pForceLogFile => TRUE);
            PKG_Logs.StdLogAddTxt('Inizio immissione',PKG_Mago.StdOutDate(vStart),NULL,vLog);
            PKG_Logs.StdLogPrint (vLog);
        END IF;
    END IF;

    EXECUTE IMMEDIATE 'DELETE FROM SCHEDULED_TMP_'||vTipo||' WHERE JOB_NUMBER <= :num' USING vMaxNum;

    --COMMIT;

 EXCEPTION
    WHEN OTHERS THEN RAISE;
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END ConsolidaJobAggregazione;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggrMETEO AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabella SCHEDULED_TMP_MET
-----------------------------------------------------------------------------------------------------------*/
 BEGIN

    ConsolidaJobAggregazione('MET',TRUE);
    COMMIT;

 END ConsolidaJobAggrMETEO;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggrGME AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabella SCHEDULED_TMP_GME
-----------------------------------------------------------------------------------------------------------*/
 BEGIN

    ConsolidaJobAggregazione('GME',TRUE);
    COMMIT;

 END ConsolidaJobAggrGME;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobCalcAggregazione   (pTipoProcesso    VARCHAR2,
                                     pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.STATO%TYPE,
                                     pTipoRete        TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     pTipoMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     pDataIns         DATE DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di calcolo aggregazione
-----------------------------------------------------------------------------------------------------------*/
    vLog       PKG_Logs.t_StandardLog;
    vIdReteTip TIPI_RETE.ID_RETE%TYPE;
    vTipoJob   SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE;
    vTrovato   BOOLEAN := FALSE;
    vTmpTable  VARCHAR2(30);
    vNum       INTEGER;
 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.AddJobCalcAggregazione',
                                pDataRif      => pDataRif,
                                pTipo         => 'Organizzazione/Stato',
                                pCodice       => NVL(TO_CHAR(pOrganizzazione),'<NULL>')||' / '||NVL(TO_CHAR(pStato),'<NULL>'),
                                pForceLogFile => TRUE);

    SELECT ID_RETE
      INTO vIdReteTip
      FROM TIPI_RETE
     WHERE COD_TIPO_RETE = pTipoRete;

    IF PKG_Misure.IsMisuraStatica(pTipoMis) = PKG_UtlGlb.gkFlagON THEN
        vTipoJob := gcAggregMisureStat;
    ELSE
        vTipoJob := gcAggregMisureStd;
    END IF;

    --IF NOT Pkg_Mago.IsMagoDGF THEN
    --    IF vTipoJob = gcAggregMisureStd AND pStato = PKG_Mago.gcStatoNormale THEN
    --        RETURN;  -- per STM/NAZ le misure Standard di stato Normale non si aggregano ma si calcolano al volo
    --    END IF;
    --END IF;

    IF (pTipoProcesso IS NULL) OR (vTipoJob = gcAggregMisureStat) THEN
        -- Aggiungo il tipo rete da elaborare alla richiesta
        UPDATE SCHEDULED_JOBS
          SET ID_RETE = ID_RETE + vIdReteTip
        WHERE TIPO_JOB = vTipoJob
          AND DATARIF = pDataRif
          AND ORGANIZZAZIONE = pOrganizzazione
          AND STATO = pStato
          AND COD_TIPO_MISURA = pTipoMis
          AND BITAND(ID_RETE,vIdReteTip) <> vIdReteTip;
        IF SQL%ROWCOUNT = 0 THEN
            BEGIN
                IF gReuseJobNumber IS NOT NULL THEN
                    SELECT COUNT(*) INTO vNum FROM SCHEDULED_JOBS WHERE JOB_NUMBER = gReuseJobNumber;
                    IF vNum <> 0 THEN
                        gReuseJobNumber := NULL;
                    END IF;
                END IF;
                -- Inserisco la richiesta
                INSERT INTO SCHEDULED_JOBS (JOB_NUMBER,      TIPO_JOB, ORGANIZZAZIONE,  STATO,  DATARIF,  ID_RETE,    COD_TIPO_MISURA, DATAINS)
                                    VALUES (gReuseJobNumber, vTipoJob, pOrganizzazione, pStato, pDataRif, vIdReteTip, pTipoMis,        pDataIns);
                --PKG_Logs.TraceLog(vLog,'Inserita richiesta aggregazione: Tip='||vTipoJob||' - Org='||pOrganizzazione||
                --                                                     ' - Sta='||pStato  ||' - Rte='||vIdReteTip||' - Mis='||pTipoMis||
                --                                                     ' - Dat='||Pkg_Mago.StdOutDate(pDataRif),PKG_UtlGlb.gcTrace_VRB);
            END;
        END IF;
    ELSE
        CASE pTipoProcesso
            WHEN 'GME' THEN vTmpTable := 'SCHEDULED_TMP_GME';
            WHEN 'MET' THEN vTmpTable := 'SCHEDULED_TMP_MET';
            ELSE            vTmpTable := 'SCHEDULED_TMP_GEN';
        END CASE;
        EXECUTE IMMEDIATE 'SELECT COUNT(*) '                ||
                            'FROM '||vTmpTable||' '         ||
                           'WHERE DATARIF = :dt '           ||
                             'AND ORGANIZZAZIONE = :org '   ||
                             'AND STATO = :sta '            ||
                             'AND ID_RETE = :ret '          ||
                             'AND COD_TIPO_MISURA = :mis '
                      INTO vNum USING pDataRif, pOrganizzazione, pStato, vIdReteTip, pTipoMis;
        IF vNum = 0 THEN
            EXECUTE IMMEDIATE 'INSERT INTO '||vTmpTable||'(DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, COD_TIPO_MISURA) '||
                                                   'VALUES(:dt,     :org,           :sta,  :ret,    :mis)'
                        USING pDataRif, pOrganizzazione, pStato, vIdReteTip, pTipoMis;
        END IF;
    END IF;
    gReuseJobNumber := NULL;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            gReuseJobNumber := NULL;
        WHEN OTHERS THEN
            gReuseJobNumber := NULL;
            PKG_Logs.StdLogAddTxt('Tipo Rete',NVL(TO_CHAR(pTipoRete),'<NULL>'),NULL,vLog);
            PKG_Logs.StdLogAddTxt('Tipo Misura',NVL(TO_CHAR(pTipoMis),'<NULL>'),NULL,vLog);
            PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
            PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
            RAISE;
 END AddJobCalcAggregazione;

-- ----------------------------------------------------------------------------------------------------------

  PROCEDURE ElaboraJobs AS
/*-----------------------------------------------------------------------------------------------------------
    Elabora le richieste di elaborazione presenti
-----------------------------------------------------------------------------------------------------------*/
    vDataInizio       DATE := SYSDATE;
    vTimestampInizio  TIMESTAMP;
    vLog              PKG_Logs.t_StandardLog;
    vTrovato          BOOLEAN;
    vTxt              VARCHAR2(200);
    vCntElaborazioni  NUMBER := 0;

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.ElaboraJobs',
                                pForceLogFile => TRUE);

    --PKG_Logs.TraceLog('Inizio - ElaboraJobs',PKG_UtlGlb.gcTrace_VRB);

    LOOP

        vTrovato := FALSE;

        FOR vRow IN (SELECT *
                       FROM (SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                    CASE
                                       WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                       ELSE DATARIF + (1 / 24 * RITARDO_HH)
                                    END data_elab, NULL DATAINS,
                                    ID_RETE, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                               FROM SCHEDULED_JOBS
                              INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                               LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                              WHERE TIPO_JOB IN (gcLinearizzazione, gcCalcolaMisureStat)
                                AND DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH ))
                              GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, MISURA_STATICA, RITARDO_HH
                             UNION ALL
                             SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                    CASE
                                       WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                       ELSE DATARIF + (1 / 24 * RITARDO_HH)
                                    END data_elab, NULL DATAINS,
                                    NULL, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                               FROM SCHEDULED_JOBS
                              INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                               LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                              WHERE TIPO_JOB = gcAggregMisureStat
                                AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                              GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH
                             UNION ALL
                             SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                    CASE
                                       WHEN MAX(DATAINS)  < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                       ELSE MAX(DATAINS)  + (1 / 24 * RITARDO_HH)
                                    END data_elab, MAX(DATAINS),
                                    NULL, NULL, MISURA_STATICA, COUNT(*) num
                               FROM SCHEDULED_JOBS
                              INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                               LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                              WHERE TIPO_JOB = gcAggregMisureStd
                                AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                              GROUP BY TIPO_JOB, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH, DATARIF
                            )
                      ORDER BY CASE
                                  WHEN TIPO_JOB  = 4 THEN CASE
                                                              WHEN TRUNC(SYSDATE,'HH24') = TRUNC(DATARIF,'HH24') THEN TIPO_JOB + 10
                                                              WHEN TRUNC(SYSDATE + 1/24,'HH') = TRUNC(DATARIF,'HH') THEN TIPO_JOB + 10
                                                              ELSE TIPO_JOB + 100
                                                          END
                                  ELSE TIPO_JOB
                               END,
                               CASE
                                  WHEN DATA_ELAB IS NULL THEN 0
                                  ELSE 1
                               END,
                               DATARIF, STATO DESC, ORGANIZZAZIONE, ID_RETE, MISURA_STATICA DESC, COD_TIPO_MISURA
                    )
        LOOP

            vCntElaborazioni := vCntElaborazioni + 1;

            vTimestampInizio := CURRENT_TIMESTAMP;
            vTxt := ' ';

            --PKG_Logs.TraceLog('Inizio - ElaboraJobs - '||
            --                '  JOB='||vRow.TIPO_JOB||
            --               '  Data='||TO_CHAR(vRow.DATARIF,'dd/mm/yyyy hh24:mi.ss')||
            --                '  Org='||NVL(TO_CHAR(vRow.ORGANIZZAZIONE),' ')||
            --                '  Sta='||NVL(TO_CHAR(vRow.STATO),' ')||
            --                '  Ret='||NVL(TO_CHAR(vRow.ID_RETE),' ')||
            --                '  Mis='||NVL(vRow.COD_TIPO_MISURA,' '),PKG_UtlGlb.gcTrace_VRB);

            CASE

                WHEN vRow.TIPO_JOB = gcLinearizzazione THEN
                    vTimestampInizio := CURRENT_TIMESTAMP;
                    PKG_Aggregazioni.LinearizzaGerarchia(NULL,vRow.DATARIF,vRow.ORGANIZZAZIONE,vRow.STATO);
                    DELETE FROM SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                                 AND DATARIF = vRow.DATARIF
                                                 AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                                 AND STATO = vRow.STATO;
                    PKG_Logs.StdLogAddTxt('Linearizza Gerarchia'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - vTimestampInizio,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB = gcCalcolaMisureStat THEN
                    vTimestampInizio := CURRENT_TIMESTAMP;
                    PKG_Misure.CalcMisureStatiche(vRow.DATARIF,vRow.ORGANIZZAZIONE,vRow.STATO);
                    DELETE FROM SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                                 AND DATARIF = vRow.DATARIF;
                    PKG_Logs.StdLogAddTxt('Calc.Mis.Statiche'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - vTimestampInizio,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB = gcAggregMisureStat THEN
                    vTimestampInizio := CURRENT_TIMESTAMP;
                    -- esecue il calcolo delle aggregate per i soli tipi misura interessati
                    DELETE GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey;
                    INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1)
                                    SELECT DISTINCT PKG_Mago.gcTmpTipMisKey, COD_TIPO_MISURA
                                      FROM SCHEDULED_JOBS
                                     INNER JOIN TIPI_MISURA USING (COD_TIPO_MISURA)
                                     WHERE TIPO_JOB = vRow.TIPO_JOB
                                       AND DATARIF = vRow.DATARIF
                                       AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                       AND STATO = vRow.STATO
                                       AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                       AND PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = vRow.MISURA_STATICA;
                    PKG_Aggregazioni.EseguiAggregazione(vRow.ORGANIZZAZIONE,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
--                    PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazELE,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
--                    IF (--BITAND(vRow.ID_RETE,PKG_Mago.gcReteMT) = PKG_Mago.gcReteMT   OR
--                        BITAND(vRow.ID_RETE,PKG_Mago.gcReteBT) = PKG_Mago.gcReteBT) AND
--                       vRow.STATO = PKG_Mago.gcStatoAttuale THEN
--                        PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazGEO,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
--                        PKG_Aggregazioni.EseguiAggregazione(PKG_Mago.gcOrganizzazAMM,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
--                    END IF;
                    FOR i IN (SELECT ALF1 COD_TIPO_MISURA FROM GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey) LOOP
                        vTxt := vTxt || i.COD_TIPO_MISURA||' ';
                    END LOOP;
                    DELETE SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                            AND DATARIF = vRow.DATARIF
                                            AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                            AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                            AND STATO = vRow.STATO
                                            AND COD_TIPO_MISURA IN (SELECT ALF1
                                                                      FROM GTTD_VALORI_TEMP
                                                                     WHERE TIP = PKG_Mago.gcTmpTipMisKey);
                    PKG_Logs.StdLogAddTxt('Aggr.Mis.Statiche'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - vTimestampInizio,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB = gcAggregMisureStd THEN
                    vTimestampInizio := CURRENT_TIMESTAMP;
                    -- esegue il calcolo delle aggregate per i soli tipi misura interessati
                    DELETE GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey;
                    INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1)
                                    SELECT DISTINCT PKG_Mago.gcTmpTipMisKey, COD_TIPO_MISURA ALF1
                                      FROM SCHEDULED_JOBS
                                     INNER JOIN TIPI_MISURA USING (COD_TIPO_MISURA)
                                     WHERE TIPO_JOB = vRow.TIPO_JOB
                                       AND DATARIF = vRow.DATARIF
                                       AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                       AND STATO = vRow.STATO
                                       AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                       AND PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = vRow.MISURA_STATICA;
                    FOR i IN (SELECT ALF1 COD_TIPO_MISURA FROM GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey) LOOP
                        vTxt := vTxt || i.COD_TIPO_MISURA||' ';
                    END LOOP;
                    PKG_Aggregazioni.EseguiAggregazione(vRow.ORGANIZZAZIONE,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
                    DELETE SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                            AND DATARIF = vRow.DATARIF
                                            AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                            AND STATO = vRow.STATO
                                            AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                            AND COD_TIPO_MISURA IN (SELECT ALF1
                                                                      FROM GTTD_VALORI_TEMP
                                                                     WHERE TIP = PKG_Mago.gcTmpTipMisKey);
                    PKG_Logs.StdLogAddTxt('Aggr.Misure'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        , SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - vTimestampInizio,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
            END CASE;

            --PKG_Logs.TraceLog('Fine   - ElaboraJobs - '||
            --                '  JOB='||vRow.TIPO_JOB||
            --                '  Data='||TO_CHAR(vRow.DATARIF,'dd/mm/yyyy hh24:mi.ss')||
            --                '  Org='||NVL(TO_CHAR(vRow.ORGANIZZAZIONE),' ')||
            --                '  Sta='||NVL(TO_CHAR(vRow.STATO),' ')||
            --                '  Ret='||NVL(TO_CHAR(vRow.ID_RETE),' ')||
            --                '  Mis='||NVL(vRow.COD_TIPO_MISURA,' '),PKG_UtlGlb.gcTrace_VRB);

            COMMIT;

            vTrovato := TRUE;

            EXIT;  -- esco da LOOP FOR per vedere se nel frattempo sono arrivare richieste con priorita' piu' alta.

        END LOOP;

        IF NOT vTrovato THEN
            EXIT;  -- il ciclo LOOP FOR non ha rilevato elaborazioni. Esco!
--        ELSE
--            DBMS_LOCK.SLEEP (2);
        END IF;

    END LOOP;

    IF vCntElaborazioni > 0 THEN
        PKG_Logs.StdLogAddTxt('Fine Elaborazione - '||'iniziata alle '||TO_CHAR(vDataInizio,'hh24:mi:ss')||' del '||TO_CHAR(vDataInizio,'dd/mm/yyyy')||
                                                 '    terminata alle '||TO_CHAR(SYSDATE,'hh24:mi:ss')||' del '||TO_CHAR(SYSDATE,'dd/mm/yyyy'),FALSE,NULL,vLog);
        PKG_Logs.StdLogPrint (vLog);
    END IF;

    --PKG_Logs.TraceLog('Fine   - ElaboraJobs - eseguite '||TO_CHAR(vCntElaborazioni)||' elaborazioni',PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END ElaboraJobs;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Scheduler;

/
