PROMPT SCHEDULER JOB MAGO_INS_REQ_AGGREG;
--
-- MAGO_INS_REQ_AGGREG  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('MAGO_INS_REQ_AGGREG', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'MAGO_INS_REQ_AGGREG',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Scheduler.ConsolidaJobAggregazione',
                 --start_date           => TRUNC(SYSDATE),
                 --repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00,05,10,15,20,25,30,35,40,45,50,55',
                 --repeat_interval      => 'FREQ=HOURLY; BYMINUTE=09,19,29,39,49,59',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Da tabelle temporanee SCHEDULED_TMP_GEN inserisce le richieste di aggregazione'
                );
END;
/

