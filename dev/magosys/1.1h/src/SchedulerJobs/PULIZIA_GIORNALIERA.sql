PROMPT SCHEDULER JOB PULIZIA_GIORNALIERA;
--
-- PULIZIA_GIORNALIERA  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('PULIZIA_GIORNALIERA', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'PULIZIA_GIORNALIERA',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Manutenzione.PuliziaGiornaliera',
                 start_date           => TRUNC (SYSDATE) + (1/24 * 20),
                 repeat_interval      => 'FREQ=DAILY; INTERVAL=1',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Eseguo la pulizia dei log'
                );
END;
/
