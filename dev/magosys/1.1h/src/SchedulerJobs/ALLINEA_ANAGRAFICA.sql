PROMPT SCHEDULER JOB ALLINEA_ANAGRAFICA;
--
-- ALLINEA_ANAGRAFICA  (Scheduler Job) 
--
BEGIN
   BEGIN
    SYS.DBMS_SCHEDULER.DROP_JOB('ALLINEA_ANAGRAFICA', TRUE);
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'ALLINEA_ANAGRAFICA',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_Anagrafiche.AllineaAnagrafica',
                 --start_date           => TRUNC(SYSDATE),
                 --repeat_interval      => 'FREQ=HOURLY; BYMINUTE=03,08,13,18,23,28,33,38,43,48,53,58;',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Esegue allineamento anagrafica mago se ci sono stati modifiche su quelle di AUI o di CORELE'
                );
END;
/

