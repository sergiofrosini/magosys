/*****************************************************************
****                                                          ****
****                       DML TEMPLATE                       ****
****                                                          ****
******************************************************************

SCRIPT NAME      : crea_00_tablespace_sql
AUTHOR           : Moretti C.
RESPONSIBLE      : F.Corti
JO/BUG/OTHER     : Creating Schema
SYSTEM           : MAGO
MODULE           : 
VERSION          : 1.0 - 21/06/2011
DESCRIPTION	 : 
CONSTRAINT       :
WARNING          :
DATABASE         : ARCDB2
SCHEMA           : MAGONAZ
         
*****************************************************************/

spool crea_00_tablespace_log

SET AUTOCOMMIT OFF 
SET TIMING ON ECHO ON FEEDBACK ON TERMOUT ON SERVEROUTPUT ON

whenever oserror exit SQL.OSCODE rollback
whenever sqlerror exit SQL.SQLCODE rollback

SELECT global_name ||'  ' || TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS')
FROM global_name;

SELECT USERNAME from user_users;

prompt Create TABLESPACES for data and indexes (  MAGONAZ_data, MAGONAZ_idx, MAGONAZ_iot )

CREATE TABLESPACE MAGONAZ_data EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 150M
       AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;

CREATE TABLESPACE MAGONAZ_idx EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 150M   
       AUTOEXTEND ON NEXT 50M MAXSIZE UNLIMITED;
	   
CREATE BIGFILE TABLESPACE MAGONAZ_iot EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 100M 
       AUTOEXTEND ON NEXT 20M MAXSIZE UNLIMITED;

show error

spool off