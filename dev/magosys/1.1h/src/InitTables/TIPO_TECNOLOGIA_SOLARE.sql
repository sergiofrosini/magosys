PROMPT INSERTING into TIPO_TECNOLOGIA_SOLARE

Insert into TIPO_TECNOLOGIA_SOLARE (ID_TECNOLOGIA,DESCRIZIONE) values ('G','Glass');
Insert into TIPO_TECNOLOGIA_SOLARE (ID_TECNOLOGIA,DESCRIZIONE) values ('C','Cell');
Insert into TIPO_TECNOLOGIA_SOLARE (ID_TECNOLOGIA,DESCRIZIONE) values ('P','Polymer sheet');
Insert into TIPO_TECNOLOGIA_SOLARE (ID_TECNOLOGIA,DESCRIZIONE) values ('L','Linear concentrator');
commit;
