--------------------------------------------------------
--  File creato - venerd�-luglio-25-2014   
--------------------------------------------------------
PROMPT INSERTING into RAGGRUPPAMENTO_FONTI

Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('i','Prod. Solare/Eolico + Idraulico','512');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('t','Prod. Solare/Eolico + Termico','1024');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('r','Prod. Solare/Eolico + Altra Rinnovabile','2048');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('c','Prod. Solare/Eolico + Altra Convenzionale','4096');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('S','Solare','1');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('E','Eolica','2');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('I','Idraulica','4');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('T','Termica','8');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('R','Altra Fonte Rinnovabile','16');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('C','Altra Fonte Convenzionale','32');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('1','Non Omogenea','64');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('2','Non Applicabile','128');
Insert into RAGGRUPPAMENTO_FONTI (COD_RAGGR_FONTE,DESCRIZIONE,ID_RAGGR_FONTE) values ('3','Non Disponibile','256');

COMMIT;
