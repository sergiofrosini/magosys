PROMPT INSERTING into TIPO_FONTI

Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('i','Prod. Solare/Eolico + Idraulico','i');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('t','Prod. Solare/Eolico + Termico','t');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('r','Prod. Solare/Eolico + Altra Rinnovabile','r');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('c','Prod. Solare/Eolico + Altra Convenzionale','c');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('H','Storage','3');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('E','Eolico','E');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('S','Solare','S');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('I','Idraulica','I');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('O','Del moto ondoso','I');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('M','Mare motrice','I');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('F','Idrocarburi fossili','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('G','Biogas','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('B','Biomasse','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('T','Geotermica','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('D','Gas di discarica','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('R','Gas da processo di depurazione','T');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('A','Altra fonte rinnovabile','R');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('C','Altra fonte convenzionale','C');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('1','Non Omogenea','1');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('2','Non Applicabile','2');
Insert into TIPO_FONTI (COD_TIPO_FONTE,DESCRIZIONE,COD_RAGGR_FONTE) values ('3','Non Disponibile','3');
commit;
