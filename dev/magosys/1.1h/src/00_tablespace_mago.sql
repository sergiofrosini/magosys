/*****************************************************************
****                                                          ****
****                       DML TEMPLATE                       ****
****                                                          ****
******************************************************************

SCRIPT NAME      : crea_00_tablespace_sql
AUTHOR           : Moretti C.
RESPONSIBLE      : F.Corti
JO/BUG/OTHER     : Creating Schema
SYSTEM           : MAGO
MODULE           : 
VERSION          : 1.0 - 21/06/2011
DESCRIPTION	 : 
CONSTRAINT       :
WARNING          :
DATABASE         : ARCDB2
SCHEMA           : MAGO 
         
*****************************************************************/

spool crea_00_tablespace_log

SET AUTOCOMMIT OFF 
SET TIMING ON ECHO ON FEEDBACK ON TERMOUT ON SERVEROUTPUT ON

whenever oserror exit SQL.OSCODE rollback
whenever sqlerror exit SQL.SQLCODE rollback

SELECT global_name ||'  ' || TO_CHAR(SYSDATE, 'YYYY/MM/DD HH24:MI:SS')
FROM global_name;

SELECT USERNAME from user_users;

prompt Create TABLESPACES for data and indexes (  MAGO_data, MAGO_idx, MAGO_iot )

CREATE TABLESPACE MAGO_data EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 1500M
       AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;

CREATE TABLESPACE MAGO_idx EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 1500M   
       AUTOEXTEND ON NEXT 100M MAXSIZE UNLIMITED;
	   
CREATE BIGFILE TABLESPACE MAGO_iot EXTENT MANAGEMENT LOCAL AUTOALLOCATE DATAFILE SIZE 10000M 
       AUTOEXTEND ON NEXT 500M MAXSIZE UNLIMITED;

show error

spool off