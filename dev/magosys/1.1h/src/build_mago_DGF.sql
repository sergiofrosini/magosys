
PROMPT _______________________________________________________________________________________
PROMPT creazione strutture oracle per MAGO
PROMPT

--parametri in ingresso
--$1 password di sys
--$2 azione (create_MAGO | rebuild_MAGO)
--$3 Tipo Installazione Oracle

WHENEVER SQLERROR EXIT SQL.SQLCODE

@./01_user_mago_DGF.sql &1 &2 &3

connect &3/&3

@./04_mago_DGF_build_all.sql 'MAGO'

PROMPT
PROMPT
PROMPT fine creazione strutture oracle per MAGO
PROMPT

exit
