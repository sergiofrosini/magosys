--------------------------------------------------------
Prompt  Type T_PARAM_EOLIC_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_PARAM_EOLIC_OBJ AS OBJECT (COD_ELEMENTO     NUMBER,
                                                    DATA_ULTIMO_AGG  DATE,
                                                    V_CUT_IN         NUMBER,
                                                    V_CUT_OFF        NUMBER,
                                                    V_MAX_POWER      NUMBER
                                                   );
/
