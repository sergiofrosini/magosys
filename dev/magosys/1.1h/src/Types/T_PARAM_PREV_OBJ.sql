--------------------------------------------------------
Prompt  Type T_PARAM_PREV_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_PARAM_PREV_OBJ AS OBJECT
      (COD_ELEMENTO     NUMBER,
       COD_TIPO_FONTE   VARCHAR2(2 BYTE),
       DATA_ULTIMO_AGG  DATE,
       PARAMETRO1       NUMBER,
       PARAMETRO2       NUMBER,
       PARAMETRO3       NUMBER,
       PARAMETRO4       NUMBER,
       PARAMETRO5       NUMBER,
       PARAMETRO6       NUMBER,
       PARAMETRO7       NUMBER,
       PARAMETRO8       NUMBER,
       PARAMETRO9       NUMBER,
       PARAMETRO10      NUMBER
      );
/
