--------------------------------------------------------
Prompt  Type T_MISURA_GME_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_MISURA_GME_OBJ IS OBJECT
                       (GEST_ELEM         VARCHAR2(20),
                        TIPO_MISURA       VARCHAR2(3),
                        TIPO_RETE         CHAR(1),
                        DATA_MIS          DATE,
                        QUALITA           VARCHAR2(4),
                        VALORE            NUMBER(20,4)
                       );
/
