Prompt Type T_MISREQ_OBJ;
--
-- T_MISREQ_OBJ  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE TYPE  "T_MISREQ_OBJ" AS OBJECT  
                      (REQ_ID              NUMBER,
                       COD_GEST_ELEMENTO   VARCHAR2(20),
                       COD_TIPO_MISURA_ST  VARCHAR2(20),
                       DATA_DA             DATE,
                       DATA_A              DATE,
                       TIPI_FONTE          VARCHAR2(30)
                      );
/

SHOW ERRORS;

