--------------------------------------------------------
Prompt  Type T_MISMETEO_OBJ
--------------------------------------------------------

  CREATE OR REPLACE TYPE T_MISMETEO_OBJ AS OBJECT
                      (COD_ELEMENTO     NUMBER,
                       COD_TIPO_MISURA  VARCHAR2(3),
                       DATA             DATE,
                       VALORE           NUMBER,
                       COD_TIPO_FONTE   VARCHAR2(2)
                      );
/
