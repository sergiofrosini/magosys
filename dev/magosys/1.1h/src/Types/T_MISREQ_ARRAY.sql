Prompt Type T_MISREQ_ARRAY;
--
-- T_MISREQ_ARRAY  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--   T_MISREQ_OBJ (Type)
--
CREATE OR REPLACE TYPE "T_MISREQ_ARRAY" IS TABLE OF T_MISREQ_OBJ;
/

SHOW ERRORS;

