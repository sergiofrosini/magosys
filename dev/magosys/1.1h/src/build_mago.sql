
PROMPT _______________________________________________________________________________________
PROMPT creazione strutture oracle per MAGO
PROMPT

--parametri in ingresso
--$1 password di sys
--$2 azione (create_MAGO | rebuild_MAGO)
--$3 Tipo Installazione Oracle

WHENEVER SQLERROR EXIT SQL.SQLCODE

@./01_user_mago.sql &1 &2
@./02_grant_mago.sql

connect mago/mago

@./04_mago_build_all.sql 'MAGO' &3 LOC

PROMPT
PROMPT
PROMPT fine creazione strutture oracle per MAGO
PROMPT

exit
