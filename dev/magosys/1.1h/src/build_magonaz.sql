
PROMPT _______________________________________________________________________________________
PROMPT creazione strutture oracle per MAGONAZ
PROMPT

--parametri in ingresso
--$1 password di sys
--$2 azione (create_MAGONAZ | rebuild_MAGONAZ)
--$3 Tipo Installazione Oracle

WHENEVER SQLERROR EXIT SQL.SQLCODE

@./01_user_mago_NAZ.sql &1 &2
@./02_grant_magonaz.sql

connect magonaz/magonaz

@./04_mago_NAZ_build_all.sql 'MAGONAZ' &3 NAZ

PROMPT
PROMPT
PROMPT fine creazione strutture oracle per MAGONAZ
PROMPT

exit
