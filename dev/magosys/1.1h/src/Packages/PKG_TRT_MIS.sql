--------------------------------------------------------
Prompt Package PKG_TRT_MIS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_TRT_MIS AS

procedure SetFileElaborated(pFileinfo T_FILE_INFO_OBJ);

procedure GetFileElaborated(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                    pNomefile   IN VARCHAR2 DEFAULT NULL,
                           pTipofile   IN VARCHAR2 DEFAULT NULL) ;

END PKG_TRT_MIS;

 
/
