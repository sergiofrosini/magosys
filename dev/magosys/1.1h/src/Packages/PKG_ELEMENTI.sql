--------------------------------------------------------
Prompt Package PKG_ELEMENTI
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_ELEMENTI AS

/* ***********************************************************************************************************
   NAME:       PKG_Elementi
   PURPOSE:    Servizi per la gestione degli elementi

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.1.c      13/05/2013  Moretti C.       Visualizzazione codici Direttrici per Montanti di linea MT
   1.1.h      14/03/2014  Moretti C.       In funzione GetElemInfo esclude da selezione la PI = 0

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_RowElemento         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       COD_TIPO_FONTE       ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                                       COD_TIPO_PRODUTTORE     ELEMENTI_DEF.COD_TIPO_PRODUTTORE%TYPE,
                                       ID_ELEMENTO          ELEMENTI_DEF.ID_ELEMENTO%TYPE,
                                       RIF_ELEMENTO         ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                                       COORDINATA_X         ELEMENTI_DEF.COORDINATA_X%TYPE,
                                       COORDINATA_Y         ELEMENTI_DEF.COORDINATA_Y%TYPE,
                                       POTENZA_INSTALLATA   ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE,
                                       POTENZA_CONTRATTUALE ELEMENTI_DEF.POTENZA_CONTRATTUALE%TYPE);

 TYPE t_DefElemento         IS TABLE OF t_RowElemento;

 TYPE t_RowElemList         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       DES_TIPO_ELEMENTO    TIPI_ELEMENTO.DESCRIZIONE%TYPE,
                                       IMP_SN               NUMBER(1),
                                       IMP_SA               NUMBER(1),
                                       GEO                  NUMBER(1),
                                       AMM                  NUMBER(1),
                                       COUNT                NUMBER);
 TYPE t_TabElemList         IS TABLE OF t_RowElemList;

 TYPE t_RowGerarchiaLineare IS RECORD (COD_ELEMENTO  NUMBER,
                                       L01           NUMBER,
                                       L02           NUMBER,
                                       L03           NUMBER,
                                       L04           NUMBER,
                                       L05           NUMBER,
                                       L06           NUMBER,
                                       L07           NUMBER,
                                       L08           NUMBER,
                                       L09           NUMBER,
                                       L10           NUMBER,
                                       L11           NUMBER,
                                       L12           NUMBER,
                                       L13           NUMBER);
 TYPE t_TabGerarchiaLineare IS TABLE OF t_RowGerarchiaLineare;

 TYPE t_RowGerarchia        IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       LIVELLO              INTEGER,
                                       SEQUENZA             INTEGER);
 TYPE t_TabGerarchia        IS TABLE OF t_RowGerarchia;


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetElemBaseEseDef (pElementoBase     ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoEseDef   ELEMENTI.COD_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoBase    RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoEseDef  RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION DecodeTipElem      (pTipEle         IN VARCHAR2,
                              pCodeOnNotFound IN BOOLEAN DEFAULT TRUE)
                       RETURN TIPI_ELEMENTO.DESCRIZIONE%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultCO      (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultESE     (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetCodElemento    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetGestElemento   (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  CalcOutCodGest    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pData           IN DATE DEFAULT SYSDATE,
                              pTipoElemento   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                              pRifElemento    IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE DEFAULT NULL)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoPadre   (pElemFiglio     IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipoPadre      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pData           IN DATE,
                              pOrganizzazione IN NUMBER,
                              pStato          IN NUMBER)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  InsertElement     (pCodGest        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) RETURN  ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetElemInfo       (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataRif        IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pTipiProduttore    IN VARCHAR2,
                              pOrganizzaz     IN NUMBER,
                              pStatoRete      IN INTEGER) RETURN INTEGER; -- DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoProd       IN VARCHAR2 DEFAULT NULL,
                              pElemTypes      IN VARCHAR2 DEFAULT NULL,
                              pCompilaInfo    IN INTEGER  DEFAULT PKG_Mago.gcON,
                              pCalcOutCodGest IN INTEGER  DEFAULT 1);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN VARCHAR2,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoProd       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementList    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pSearchString   IN VARCHAR2);

 FUNCTION GetElementList     (pData           IN DATE,
                              pSearchString   IN VARCHAR2)
                       RETURN t_TabElemList PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaLineare (pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE)
                          RETURN t_TabGerarchiaLineare PIPELINED;

 FUNCTION LeggiGerarchiaLineare2(pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE)
                          RETURN t_TabGerarchiaLineare PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaSup     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE)
                          RETURN t_TabGerarchia PIPELINED;

 FUNCTION LeggiGerarchiaInf     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pFiltroTipEle   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL)
                          RETURN t_TabGerarchia PIPELINED;

END PKG_Elementi;
 
/
