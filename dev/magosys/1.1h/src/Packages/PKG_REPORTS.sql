--------------------------------------------------------
Prompt Package PKG_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_REPORTS AS

/* ***********************************************************************************************************
   NAME:       PKG_Reports
   PURPOSE:    Servizi per la gestione dei Reports

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      02/11/2011  Moretti C.       Created this package.
   1.0.a      06/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni
   1.0.h      08/10/2012  Moretti C.       .....

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisElem               (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisFnte               (pFlagTab        IN INTEGER,
                                    pCodTipoFonte   IN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER;

 PROCEDURE InitRepConsEnergia      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pDataDa         IN DATE,
                                    pDataA          IN DATE,
                                    pOrganizzazione IN NUMBER,
                                    pStatoRete      IN NUMBER,
                                    pTipologiaRete  IN VARCHAR2,
                                    pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipEleFiglio   IN VARCHAR2);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsEnergiaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetRepConsEnergiaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsPotenzaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pData           IN DATE);

 PROCEDURE GetRepConsPotenzaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                    pData           IN DATE);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Reports;
 
/
