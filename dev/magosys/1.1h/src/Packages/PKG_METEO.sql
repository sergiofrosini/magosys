--------------------------------------------------------
Prompt Package PKG_METEO
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_METEO AS

/* ***********************************************************************************************************
   NAME:       PKG_Meteo
   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      14/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.c      21/03/2012  Risso M.         Integrazione SPC relative flusso meteo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni - Aggiornamento SPC relative flusso meteo
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.g      08/08/2012  Migliaccio G.    Gestione distrib.list diversivicata per tipi di installazione
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetCodiciMeteo       (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2);

 PROCEDURE GetProduttori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2);

 PROCEDURE GetGeneratori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2);

 PROCEDURE GetTrasformatori     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2);

 PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                 pTipoMeteo      IN INTEGER);

 PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY);

 PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY);

 PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pTipiElemento   IN VARCHAR2,
                                 pData           IN DATE DEFAULT SYSDATE);

 PROCEDURE ElaboraMisure        (pMisureMeteo    IN T_MISMETEO_ARRAY);

 PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                 pFinishTimestamp IN DATE);

 PROCEDURE GetMeteoDistribListCO(pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetMeteoDistribList  (pRefCurs       OUT PKG_UtlGlb.t_query_cur);


-- ----------------------------------------------------------------------------------------------------------

END PKG_Meteo;
 
/
