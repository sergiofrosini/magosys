--------------------------------------------------------
Prompt Package PKG_ANAGRAFICHE
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_ANAGRAFICHE AS

/* ***********************************************************************************************************
   NAME:       PKG_Anagrafiche
   PURPOSE:    Utilities e Definizioni schema CORELE (SOLO modalita' STM)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      21/02/2012  Moretti C.       Created this package.
   1.0.d      20/04/2012  Moretti C.       Avanzamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.1.c      13/05/2013  Moretti C.       Controllo codici gestionali
   1.1.h      14/03/2014  Moretti C.       Eliminata forzatura introdotta in attesa della gestione
                                           file nea  IMTRAP.nea

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitApplMagoSTM     (pCodGestESE     IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                               pData           IN DATE DEFAULT TRUNC(SYSDATE));

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetListaAttualizzazioni (pRefCurs   OUT PKG_UtlGlb.t_query_cur) ;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElementsCheckReport (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pStato          IN INTEGER,
                               pData           IN DATE);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckCodGestCorele  (pStato          IN INTEGER,
                               pBlockOnly      IN BOOLEAN,
                               pData           IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AllineaAnagrafica  (pElabSincrona   IN INTEGER DEFAULT PKG_UtlGlb.gkFlagON,
                               pForzaCompleta  IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOFF);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraGerarchia     (pData          IN DATE,
                                pStato         IN INTEGER,
                                pModAssetto    IN BOOLEAN,
                                pAUI           IN BOOLEAN,
                                pElabSincrona  IN BOOLEAN,
                                pForzaCompleta IN BOOLEAN);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Anagrafiche;
 
/
