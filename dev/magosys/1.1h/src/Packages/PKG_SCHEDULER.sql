--------------------------------------------------------
Prompt Package PKG_SCHEDULER
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE PKG_SCHEDULER AS

/* ***********************************************************************************************************
   NAME:       PKG_Scheduler
   PURPOSE:    Servizi per la gestione dei jobs schedulati

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.e      11/06/2012  Moretti C.       revisione schedulazione jobs
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- PROCEDURE AddJobLoadAnagr          (pDataRif         DATE,
--                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
--                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobLinearizzazione    (pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobCalcMisStatiche    (pDataRif         DATE);

 PROCEDURE ConsolidaJobAggregazione (pTipo            VARCHAR2 DEFAULT NULL,
                                     pUseLog          BOOLEAN DEFAULT FALSE);

 PROCEDURE ConsolidaJobAggrMETEO;

 PROCEDURE ConsolidaJobAggrGME;

 PROCEDURE AddJobCalcAggregazione   (pTipoProcesso    VARCHAR2,
                                     pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.STATO%TYPE,
                                     pTipoRete        TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     pTipoMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     pDataIns         DATE DEFAULT NULL);

 PROCEDURE ElaboraJobs;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Scheduler;
 
/
