/* Formatted on 20/10/2014 14:34:19 (QP5 v5.252.13127.32847) */
Prompt View V_CURRENT_VERSION;
--
-- V_CURRENT_VERSION  (View)
--
--  Dependencies:
--   VERSIONS (Table)
--

CREATE OR REPLACE VIEW V_CURRENT_VERSION AS
    SELECT 'DB=MAGO_' || VERSION || '.' || RELEASE || '/' || PATCH || ' (' || TO_CHAR (DATA, 'dd/mm/yyyy') || ')' VERSION
      FROM VERSIONS
     WHERE DATA = (SELECT MAX (DATA) FROM VERSIONS)
/


