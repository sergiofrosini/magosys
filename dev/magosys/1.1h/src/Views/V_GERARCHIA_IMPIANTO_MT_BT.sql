/* Formatted on 20/10/2014 14:34:20 (QP5 v5.252.13127.32847) */
Prompt View V_GERARCHIA_IMPIANTO_MT_BT;
--
-- V_GERARCHIA_IMPIANTO_MT_BT  (View)
--
--  Dependencies:
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   TRASFORMATORIBT_SA (Table)
--

CREATE OR REPLACE VIEW V_GERARCHIA_IMPIANTO_MT_BT AS
    SELECT   DISTINCT SCS_ID L1, P.COD_ELEMENTO L2, F.COD_ELEMENTO L3
        FROM (SELECT COD_ELEMENTO SCS_ID, CODICE_ST CSE_ID, COD_GEST_SBARRA SCS_COD
                FROM CORELE.IMPIANTIMT_SA INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST_SBARRA
               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE)
             LEFT OUTER JOIN
             (SELECT COD_GEST ELE_COD, COD_ENTE ELE_TIP, CODICEST_IMPMT CSE_ID
                FROM CORELE.TRASFORMATORIBT_SA
               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
              UNION ALL
              SELECT COD_GEST ELE_COD, COD_ENTE ELE_TIP, CODICEST_PADRE CSE_ID
                FROM CORELE.CLIENTIMT_SA A
                     INNER JOIN V_CLIENTI_TLC@PKG1_STMAUI.IT B /*CLIENTI_TLC@PKG1_STMAUI.IT*/
                         ON COD_ORG_NODO || SER_NODO || NUM_NODO || TIPO_ELEMENTO || ID_CLIENTE = COD_GEST
               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                 AND A.STATO = 'CH'
                 AND TRATTAMENTO = 0
                 AND TIPO_FORN = 'PD'
                 AND B.STATO = 'E'
                 AND SER_NODO = '2') USING
                 USING (CSE_ID)
             LEFT OUTER JOIN ELEMENTI P ON COD_GEST_ELEMENTO = ELE_COD
             LEFT OUTER JOIN (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO GMT_COD, ID_ELEMENTO ELE_COD
                                FROM ELEMENTI E INNER JOIN ELEMENTI_DEF USING (COD_ELEMENTO)
                               WHERE E.COD_TIPO_ELEMENTO IN ('GMT', 'TRX')
                                 AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) F
                 USING (ELE_COD)
    ORDER BY L1, L2, L3
/


