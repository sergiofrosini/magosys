/* Formatted on 20/10/2014 14:34:21 (QP5 v5.252.13127.32847) */
Prompt View V_LOG_HISTORY_MAGO;

/* This object may not be sorted properly in the script due to cirular references */
--
-- V_LOG_HISTORY_MAGO  (View)
--
--  Dependencies:
--   PKG_ELEMENTI (Package)
--   LOG_HISTORY_INFO (Table)
--   LOG_HISTORY (Table)
--

CREATE OR REPLACE VIEW V_LOG_HISTORY_MAGO AS
    SELECT   LOGDATE, ELAPSED, DATA_RIF, DATA_RIF_TO, CLASSE, PROC_NAME
           , CASE WHEN ERROR_NUM IS NULL THEN MESSAGE ELSE 'ERR: ' || TO_CHAR (ERROR_NUM) || ' - ' || MESSAGE END MSG
           , CASE WHEN CODICE IS NULL THEN NULL ELSE PKG_Elementi.DecodeTipElem (TIPO) || ': ' || CASE WHEN NOME IS NULL THEN CODICE ELSE CODICE || '  (' || NOME || ')' END END elemento
        FROM LOG_HISTORY LEFT OUTER JOIN LOG_HISTORY_INFO USING (RUN_ID)
    ORDER BY TRUNC (logdate) DESC, LOGDATE, RUN_ID, SEQ
/


