--------------------------------------------------------
Prompt  Trigger BEF_SCHEDULED_TMP_GEN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER BEF_SCHEDULED_TMP_GEN  
BEFORE INSERT OR UPDATE
ON SCHEDULED_TMP_GEN REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_SCHEDULED_TMP_GEN
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.h      26/09/2012   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.JOB_NUMBER   IS NULL THEN
            SELECT SCHEDULED_JOBS_PKSEQ.NEXTVAL INTO :NEW.JOB_NUMBER FROM DUAL;
        END IF;
        :NEW.DATAINS := SYSDATE;
    END IF;

END BEF_SCHEDULED_TMP_GEN;

/
ALTER TRIGGER BEF_SCHEDULED_TMP_GEN ENABLE;
