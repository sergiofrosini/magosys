--------------------------------------------------------
Prompt  Trigger BEF_IUR_ELEMENTI
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER BEF_IUR_ELEMENTI 
BEFORE INSERT OR UPDATE
ON ELEMENTI REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_ELEMENTI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      19/09/2011   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_ELEMENTO IS NULL THEN
            SELECT ELEMENTI_PKSEQ.NEXTVAL INTO :NEW.COD_ELEMENTO FROM DUAL;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_ELEMENTO <> :NEW.COD_ELEMENTO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice Elemento in tabella ''Elementi''');
        END IF;
    END IF;

END BEF_UR_ELEMENTI;

/
ALTER TRIGGER BEF_IUR_ELEMENTI ENABLE;
