--------------------------------------------------------
Prompt  Trigger BEF_IUR_SCHEDULED_JOBS
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER BEF_IUR_SCHEDULED_JOBS 
BEFORE INSERT OR UPDATE
ON SCHEDULED_JOBS REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_IUR_SCHEDULED_JOBS
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.e      23/04/2012   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.JOB_NUMBER   IS NULL THEN
            SELECT SCHEDULED_JOBS_PKSEQ.NEXTVAL INTO :NEW.JOB_NUMBER FROM DUAL;
        END IF;
    END IF;
    IF :NEW.DATAINS   IS NULL THEN
        :NEW.DATAINS := SYSDATE;
    END IF;
END BEF_IUR_SCHEDULED_JOBS;

/
ALTER TRIGGER BEF_IUR_SCHEDULED_JOBS ENABLE;
