--------------------------------------------------------
Prompt  Trigger AFTI_APPLICATION_RUN
--------------------------------------------------------

  CREATE OR REPLACE TRIGGER AFTI_APPLICATION_RUN  
AFTER INSERT
ON APPLICATION_RUN
REFERENCING NEW AS NEW OLD AS OLD
DECLARE
cnt NUMBER;
num NUMBER;
BEGIN
   IF PKG_Mago.IsMagoDGF THEN
       SELECT COUNT(*) INTO cnt FROM APPLICATION_RUN;
       IF cnt > 1 THEN
         RAISE_APPLICATION_ERROR(-20999,'Table can have only one row');
       END IF;
   END IF ;
END ;

/
ALTER TRIGGER AFTI_APPLICATION_RUN ENABLE;
