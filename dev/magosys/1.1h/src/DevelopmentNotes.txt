## 07/04/2014 VERSION 1.1h

20/11/2014 rel_11h
- adeguamento per Oracle Standard Edition - pm_tc 29597

20/10/2014 rel_11h
- completato pacchetto di installazione iniziale
- predisposizione per Oracle Standard Edition

22/09/2014 rel_11h
- al pacchetto di installazione mancavano i job schedulati - Definiti !
	@./SchedulerJobs/ALLINEA_ANAGRAFICA.sql;
	@./SchedulerJobs/MAGO_SCHEDULER.sql;
	@./SchedulerJobs/MAGO_INS_REQ_AGGREG.sql;
	@./SchedulerJobs/MAGO_INS_REQ_AGG_METEO.sql;
	@./SchedulerJobs/MAGO_INS_REQ_AGG_GME.sql;
	@./SchedulerJobs/PULIZIA_GIORNALIERA.sql;
  modificato per quanto sopra anche build_Mago_STM_1.1h.sql

19/09/2014 rel_11h rev 1
- tolta init METEO_CITTA (la riempe dinamicamente MDS leggendo i file xml)
- tolto  EXIT su ogni build MAGO altrimenti non viene aggioranta la versione nello script install.sql

17/09/2014 rel_11h rev 1
- modificati separatori in METEO_CITTA.sql
- modificato ordine chiamata insert GRUPPI_MISURE.sql e TIPI_RETE.sql
- portati gi� fix per Xeon (by Giuseppe):
    vista:  V_GERARCHIA_GEOGRAFICA [le colonne cod_istat_xxx non hanno l'alias della tabella a cui si riferiscono ]                               

    package body:   PKG_ANAGRAFICHE 
                              [ la funz GetListaAttualizzazioni  contiene la definizione di un cursore (pRefCurs) in cui la
                                colonna DATA_IMPORT non ha alias della tabella a cui si riferisce (st.)]
                    PKG_METEO [ la funz GetProduttori  contiene la definizione di un cursore (pRefCurs) in cui la
                                colonna COD_TIPO_CLIENTE non ha l' alias della tabella a cui si riferisce (A.)]                   

06/06/2014 
- rel_11h:
- modificati i pkg di corele PKG_IMPORT_CONS e PKG_IMPORT_NEA_FILES;
- eliminata la differenza tra versione 14 e 2012 di ST e aggiornato SOLO lo script di installazione Corele_2.1.0-ST_R2012.sql

08/05/2014 rel_11h rev 0
- fix per installazione in campo di COrEle

07/04/2014 rel_11h rev 0
- Bug 774 -CORELE - 'reset' del semaforo CORELE + Sequenzializzazione lettura Set Files *DAT e *nea
- Bug 1041 - Non tutte le connessioni al DB vengono rilasciate
- Bug 1012 - Variazioni di rete: evitare situazioni di elaborazioni lanciate in parallelo per errore
- Bug 1005 - CO.UDINE- SA e variazione di PI non corrispondente a manovre su STM, verificare
- Bug 1003 - CO.UDINE - CP con 3 TR situazione in SA non coerente con STM
- Bug 771 - CO.NOVARA- elaborazione .DAT e .nea ferma da tempo (cartella fromST piena) 
- Bug 1000 - pm_tc 28766    Stima parametri MDS con PAS oraria 
- Bug 1060 - ENEL - CO.ANCONA - verifica mancanza direttrice ""SFERCIA DH60448204"" sotteso alla sbarra verde delle CP di Camerino2K"

06/02/2014 rel_11g
- Fix script (add PKG_MISURE)

20/01/2014 rel_11g
- Fix script

17/01/2014 rel_11g
- Aggiunte tabella per gestione FILE_PROCESSE anche se parte della 1.4,richiesta effettuata da enel (modulo StwebDataloader per inoltro superClientDMS) 
