spool initialized_mago.log

conn mago/mago@arcdb2

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500


PROMPT _______________________________________________________________________________________
PROMPT Inizializazzione Tabelle ...
PROMPT
@./InitTables/RAGGRUPPAMENTO_FONTI.sql;
@./InitTables/TIPO_FONTI.sql;
@./InitTables/TIPI_RETE.sql;
@./InitTables/TIPI_ELEMENTO.sql;
@./InitTables/TIPI_PRODUTTORE.sql;
@./InitTables/TIPI_MISURA.sql;
@./InitTables/TIPI_MISURA_CONV_ORIG.sql;
@./InitTables/TIPI_MISURA_CONV_UM.sql;
@./InitTables/TIPI_GRUPPI_MISURA.sql;
@./InitTables/GRUPPI_MISURA.sql;
@./InitTables/TIPO_TECNOLOGIA_SOLARE.sql;
@./InitTables/SCHEDULED_JOBS_DEF.sql;
@./InitTables/METEO_CITTA.sql;
@./InitTables/METEO_REL_ISTAT.sql;

spool off

exit