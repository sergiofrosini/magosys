PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI SAR_ADMIN per MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn sar_admin/sar_admin_dba

@./Views/SAR_ADMIN_V_ESERCIZI_MAGO.sql

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT =======================================================================================
PROMPT Dblinks
PROMPT _______________________________________________________________________________________
@./Dblinks/PKG1_STMAUI.IT.sql

PROMPT =======================================================================================
PROMPT Directories
PROMPT _______________________________________________________________________________________
@./Directories/MAGO_LOGDIR_&3.sql;

PROMPT =======================================================================================
PROMPT Types
PROMPT _______________________________________________________________________________________
@./Types/T_FILE_INFO_OBJ.sql
@./Types/T_MISMETEO_OBJ.sql
@./Types/T_MISURA_GME_OBJ.sql
@./Types/T_PARAM_EOLIC_OBJ.sql
@./Types/T_PARAM_PREV_OBJ.sql
@./Types/T_MISREQ_OBJ.sql
@./Types/T_FILE_INFO_ARRAY.sql
@./Types/T_MISMETEO_ARRAY.sql
@./Types/T_MISURA_GME_ARRAY.sql
@./Types/T_PARAM_EOLIC_ARRAY.sql
@./Types/T_PARAM_PREV_ARRAY.sql
@./Types/T_MISREQ_ARRAY.sql

PROMPT =======================================================================================
PROMPT Sequences
PROMPT _______________________________________________________________________________________
@./Sequences/ELEMENTI_PKSEQ.sql
@./Sequences/FILEINFO_SEQUENCE.sql
@./Sequences/LOG_SEQUENCE.sql
@./Sequences/METEO_FILE_XML_SEQ.sql
@./Sequences/METEO_FILE_ZIP_SEQ.sql
@./Sequences/METEO_JOB_SEQ.sql
@./Sequences/SCHEDULED_JOBS_PKSEQ.sql
@./Sequences/TRATTAMENTO_ELEMENTI_PKSEQ.sql

PROMPT =======================================================================================
PROMPT Tabelle
PROMPT _______________________________________________________________________________________
@./Tables/TIPI_RETE.sql
@./Tables/TIPI_ELEMENTO.sql
@./Tables/TIPI_GRUPPI_MISURA.sql
@./Tables/TIPI_MISURA.sql
@./Tables/TIPI_MISURA_CONV_ORIG.sql
@./Tables/TIPI_MISURA_CONV_UM.sql
@./Tables/TIPI_PRODUTTORE.sql
@./Tables/RAGGRUPPAMENTO_FONTI.sql
@./Tables/TIPO_FONTI.sql
@./Tables/TIPO_TECNOLOGIA_SOLARE.sql
@./Tables/ANAGRAFICA_PUNTI.sql
@./Tables/APPLICATION_RUN.sql
@./Tables/ELEMENTI.sql
@./Tables/ELEMENTI_DEF.sql
@./Tables/DEFAULT_CO.sql
@./Tables/FILE_PROCESSED.sql
@./Tables/FORECAST_PARAMETRI.sql
@./Tables/GERARCHIA_AMM.sql
@./Tables/GERARCHIA_GEO.sql
@./Tables/GERARCHIA_IMP_SA.sql
@./Tables/GERARCHIA_IMP_SN.sql
@./Tables/GRUPPI_MISURA.sql
@./Tables/GTTD_CALC_GERARCHIA.sql
@./Tables/GTTD_FORECAST_ELEMENTS.sql
@./Tables/GTTD_IMPORT_GERARCHIA.sql
@./Tables/GTTD_MISURE.sql
@./Tables/GTTD_MOD_ASSETTO_RETE_SA.sql
@./Tables/GTTD_REP_ENERGIA_POTENZA_&2.sql
@./Tables/GTTD_VALORI_TEMP.sql
@./Tables/LOG_HISTORY.sql
@./Tables/LOG_HISTORY_INFO_&2.sql
@./Tables/MANUTENZIONE.sql
@./Tables/METEO_JOB.sql
@./Tables/METEO_JOB_RUNTIME_CONFIG.sql
@./Tables/METEO_JOB_STATIC_CONFIG.sql
@./Tables/METEO_CITTA.sql
@./Tables/METEO_FILE_LETTO.sql
@./Tables/METEO_FILE_ZIP.sql
@./Tables/METEO_FILE_XML.sql
@./Tables/METEO_PREVISIONE_&2.sql
@./Tables/METEO_REL_ISTAT.sql
@./Tables/TRATTAMENTO_ELEMENTI_&2.sql
@./Tables/MISURE_ACQUISITE_STATICHE.sql
@./Tables/MISURE_AGGREGATE_STATICHE.sql
@./Tables/MISURE_ACQUISITE_&2.sql
@./Tables/MISURE_AGGREGATE_&2.sql
@./Tables/REL_ELEMENTI_AMM.sql
@./Tables/REL_ELEMENTI_ECP_SA.sql
@./Tables/REL_ELEMENTI_ECP_SN.sql
@./Tables/REL_ELEMENTI_ECS_SA.sql
@./Tables/REL_ELEMENTI_ECS_SN.sql
@./Tables/REL_ELEMENTI_GEO.sql
@./Tables/REL_ELEMENTO_TIPMIS.sql
@./Tables/SCHEDULED_JOBS_DEF.sql
@./Tables/SCHEDULED_JOBS_&2.sql
@./Tables/SCHEDULED_TMP_GEN.sql
@./Tables/SCHEDULED_TMP_GME.sql
@./Tables/SCHEDULED_TMP_MET.sql
@./Tables/SERVIZIO_MAGO.sql
@./Tables/STORICO_IMPORT.sql
@./Tables/TMP_CONV_TRATT_ELEM.sql
@./Tables/TMP_MANUTENZIONE.sql
@./Tables/VERSIONS.sql
@./Tables/VERSION.sql

PROMPT =======================================================================================
PROMPT Packages
PROMPT _______________________________________________________________________________________
@./Packages/PKG_AGGREGAZIONI.sql
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_INTEGRST.sql
@./Packages/PKG_LOGS.sql
@./Packages/PKG_MAGO.sql
@./Packages/PKG_MANUTENZIONE.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_REPORTS.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_TRT_MIS.sql

PROMPT =======================================================================================
PROMPT Viste
PROMPT _______________________________________________________________________________________
@./Views/V_ESERCIZI.sql
@./Views/V_CURRENT_VERSION.sql
@./Views/V_ELEMENTI.sql
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql
@./Views/V_GERARCHIA_GEOGRAFICA.sql
@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./Views/V_GERARCHIA_IMPIANTO_MT_BT.sql
@./Views/V_LOG_HISTORY_MAGO.sql
@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./Views/V_MOD_ASSETTO_RETE_SA.sql
@./Views/V_PARAMETRI_APPLICATIVI.sql
@./Views/V_TIPI_MISURA.sql
@./Views/V_SEARCH_ELEMENTS.sql

PROMPT =======================================================================================
PROMPT Viste Materializzate
PROMPT _______________________________________________________________________________________
@./Views/V_SCHEDULED_JOBS.sql

PROMPT =======================================================================================
PROMPT Procedure
PROMPT _______________________________________________________________________________________
@./Procedures/GET_METEODISTRIBLIST.sql

PROMPT =======================================================================================
PROMPT Package Bodies
PROMPT _______________________________________________________________________________________
@./PackageBodies/PKG_AGGREGAZIONI.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_INTEGRST.sql
@./PackageBodies/PKG_LOGS.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_TRT_MIS.sql

PROMPT =======================================================================================
PROMPT Scheduler Jobs
PROMPT _______________________________________________________________________________________
@./SchedulerJobs/ALLINEA_ANAGRAFICA.sql;
@./SchedulerJobs/MAGO_SCHEDULER.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGGREG.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGG_METEO.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGG_GME.sql;
@./SchedulerJobs/PULIZIA_GIORNALIERA.sql;

PROMPT =======================================================================================
PROMPT Inizializzazione Tabelle
PROMPT _______________________________________________________________________________________
@./InitTables/METEO_JOB_RUNTIME_CONFIG.sql
@./InitTables/METEO_JOB_STATIC_CONFIG.sql
@./InitTables/METEO_REL_ISTAT.sql
@./InitTables/RAGGRUPPAMENTO_FONTI.sql
@./InitTables/SCHEDULED_JOBS_DEF.sql
@./InitTables/TIPI_RETE.sql
@./InitTables/TIPI_ELEMENTO.sql
@./InitTables/TIPI_GRUPPI_MISURA.sql
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPI_MISURA_CONV_ORIG.sql
@./InitTables/TIPI_MISURA_CONV_UM.sql
@./InitTables/TIPI_PRODUTTORE.sql
@./InitTables/TIPO_FONTI.sql
@./InitTables/TIPO_TECNOLOGIA_SOLARE.sql
@./InitTables/GRUPPI_MISURA.sql
@./InitTables/VERSIONS.sql

PROMPT =======================================================================================
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
PROMPT _______________________________________________________________________________________
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT =======================================================================================
PROMPT OGGETTI INVALIDI
PROMPT _______________________________________________________________________________________
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE 1.1.h.0
PROMPT
