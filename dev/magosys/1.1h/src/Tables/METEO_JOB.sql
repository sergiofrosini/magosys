Prompt Table METEO_JOB;
--
-- METEO_JOB  (Table) 
--
CREATE TABLE METEO_JOB
(
  ID                 NUMBER(8)                  NOT NULL,
  JOB_ID             VARCHAR2(50)               NOT NULL,
  START_DATE         DATE,
  END_DATE           DATE,
  STATUS             VARCHAR2(100),
  DESCRIPTION        VARCHAR2(1024),
  TIMER_DESCRIPTION  VARCHAR2(1024)
)
TABLESPACE &TBS&DAT
/


Prompt Index METEO_JOB_PK;
--
-- METEO_JOB_PK  (Index) 
--
--  Dependencies: 
--   METEO_JOB (Table)
--
CREATE UNIQUE INDEX METEO_JOB_PK ON METEO_JOB
(ID)
TABLESPACE &TBS&IDX
/


-- 
-- Non Foreign Key Constraints for Table METEO_JOB 
-- 
Prompt Non-Foreign Key Constraints on Table METEO_JOB;
ALTER TABLE METEO_JOB ADD (
  CONSTRAINT METEO_JOB_PK
  PRIMARY KEY
  (ID)
  USING INDEX METEO_JOB_PK
  ENABLE VALIDATE)
/
