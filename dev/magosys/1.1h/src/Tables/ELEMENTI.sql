Prompt Table ELEMENTI;
--
-- ELEMENTI  (Table) 
--
--  Dependencies: 
--   TIPI_ELEMENTO (Table)
--
CREATE TABLE ELEMENTI
(
  COD_ELEMENTO       NUMBER,
  COD_GEST_ELEMENTO  VARCHAR2(20),
  COD_TIPO_ELEMENTO  VARCHAR2(3)
)
TABLESPACE &TBS&DAT
/

COMMENT ON TABLE ELEMENTI IS 'Identifica un elemento (elettrico, geografico, organizzativo)'
/

COMMENT ON COLUMN ELEMENTI.COD_ELEMENTO IS 'Codice Elemento interno'
/

COMMENT ON COLUMN ELEMENTI.COD_GEST_ELEMENTO IS 'Codice gestionale Elemento (ST)'
/

COMMENT ON COLUMN ELEMENTI.COD_TIPO_ELEMENTO IS 'Codice Tipo Elemento (ultimo valido)'
/



Prompt Index ELEMENTI_AK;
--
-- ELEMENTI_AK  (Index) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE UNIQUE INDEX ELEMENTI_AK ON ELEMENTI
(COD_GEST_ELEMENTO)
TABLESPACE &TBS&IDX
/


Prompt Index ELEMENTI_PK;
--
-- ELEMENTI_PK  (Index) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE UNIQUE INDEX ELEMENTI_PK ON ELEMENTI
(COD_ELEMENTO)
TABLESPACE &TBS&IDX
/


Prompt Index ELEMENTI_TIPO;
--
-- ELEMENTI_TIPO  (Index) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE INDEX ELEMENTI_TIPO ON ELEMENTI
(COD_TIPO_ELEMENTO)
TABLESPACE &TBS&IDX
/


Prompt Trigger BEF_IUR_ELEMENTI;
--
-- BEF_IUR_ELEMENTI  (Trigger) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_ELEMENTI 
BEFORE INSERT OR UPDATE
ON ELEMENTI REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_ELEMENTI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      19/09/2011   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_ELEMENTO IS NULL THEN
            SELECT ELEMENTI_PKSEQ.NEXTVAL INTO :NEW.COD_ELEMENTO FROM DUAL;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_ELEMENTO <> :NEW.COD_ELEMENTO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice Elemento in tabella ''Elementi''');
        END IF;
    END IF;

END BEF_UR_ELEMENTI;
/


-- 
-- Non Foreign Key Constraints for Table ELEMENTI 
-- 
Prompt Non-Foreign Key Constraints on Table ELEMENTI;
ALTER TABLE ELEMENTI ADD (
  CONSTRAINT ELEMENTI_PK
  PRIMARY KEY
  (COD_ELEMENTO)
  USING INDEX ELEMENTI_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table ELEMENTI 
-- 
Prompt Foreign Key Constraints on Table ELEMENTI;
ALTER TABLE ELEMENTI ADD (
  CONSTRAINT TPELE_ELE 
  FOREIGN KEY (COD_TIPO_ELEMENTO) 
  REFERENCES TIPI_ELEMENTO (COD_TIPO_ELEMENTO)
  ENABLE VALIDATE)
/
