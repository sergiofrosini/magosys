Prompt Table REL_ELEMENTI_GEO;
--
-- REL_ELEMENTI_GEO  (Table) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE TABLE REL_ELEMENTI_GEO
(
  COD_ELEMENTO_PADRE   NUMBER,
  COD_ELEMENTO_FIGLIO  NUMBER,
  DATA_ATTIVAZIONE     DATE,
  DATA_DISATTIVAZIONE  DATE                     DEFAULT TO_DATE('01013000','ddmmyyyy')
)
TABLESPACE &TBS&DAT
/

COMMENT ON TABLE REL_ELEMENTI_GEO IS 'Gerarchia Geografica (Istat)'
/

COMMENT ON COLUMN REL_ELEMENTI_GEO.COD_ELEMENTO_PADRE IS 'Codice elemento Padre'
/

COMMENT ON COLUMN REL_ELEMENTI_GEO.COD_ELEMENTO_FIGLIO IS 'Codice elemento Figlio'
/

COMMENT ON COLUMN REL_ELEMENTI_GEO.DATA_ATTIVAZIONE IS 'data inizio validita'''
/

COMMENT ON COLUMN REL_ELEMENTI_GEO.DATA_DISATTIVAZIONE IS 'data fine validita'''
/



Prompt Index REL_ELE_GEO_IF1;
--
-- REL_ELE_GEO_IF1  (Index) 
--
--  Dependencies: 
--   REL_ELEMENTI_GEO (Table)
--
CREATE INDEX REL_ELE_GEO_IF1 ON REL_ELEMENTI_GEO
(COD_ELEMENTO_FIGLIO)
TABLESPACE &TBS&IDX
/


Prompt Index REL_ELE_GEO_IF2;
--
-- REL_ELE_GEO_IF2  (Index) 
--
--  Dependencies: 
--   REL_ELEMENTI_GEO (Table)
--
CREATE INDEX REL_ELE_GEO_IF2 ON REL_ELEMENTI_GEO
(COD_ELEMENTO_PADRE)
TABLESPACE &TBS&IDX
/


Prompt Index REL_ELE_GEO_PK;
--
-- REL_ELE_GEO_PK  (Index) 
--
--  Dependencies: 
--   REL_ELEMENTI_GEO (Table)
--
CREATE UNIQUE INDEX REL_ELE_GEO_PK ON REL_ELEMENTI_GEO
(COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO, DATA_ATTIVAZIONE)
TABLESPACE &TBS&IDX
/


Prompt Trigger BEF_IUR_REL_ELEMENTI_GEO;
--
-- BEF_IUR_REL_ELEMENTI_GEO  (Trigger) 
--
--  Dependencies: 
--   REL_ELEMENTI_GEO (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_REL_ELEMENTI_GEO 
BEFORE INSERT OR UPDATE
ON REL_ELEMENTI_GEO REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_REL_ELEMENTI_GEO
   PURPOSE:    Controllare la validita' dei dati in inserimento o modifica
               e gestire in modo automatico la versione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        07/04/2009   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
 --vDataAttiv     DATE  := NULL;
 --vDataDisatt    DATE  := NULL;

 BEGIN
    IF UPDATING THEN
        IF :NEW.DATA_ATTIVAZIONE <> :OLD.DATA_ATTIVAZIONE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su campo Data Attivazione');
        END IF;

        IF :NEW.COD_ELEMENTO_PADRE <> :OLD.COD_ELEMENTO_PADRE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su Codice Elemento Padre');
        END IF;

        IF :NEW.COD_ELEMENTO_FIGLIO <> :OLD.COD_ELEMENTO_FIGLIO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su Codice Elemento Figlio');
        END IF;
    END IF;

    IF :NEW.COD_ELEMENTO_PADRE = :NEW.COD_ELEMENTO_FIGLIO THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'I Codici Padre e Figlio non devono coincidere');
    END IF;

--    IF INSERTING THEN
--        BEGIN
--            SELECT DATA_ATTIVAZIONE INTO vDataAttiv
--              FROM REL_ELEMENTI_GEO
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND :NEW.DATA_ATTIVAZIONE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
--            -- Relazione gia' presente - non procedere con l'inserimento
--            RAISE PKG_UtlGlb.geNothingToDo;
--            EXCEPTION
--                WHEN NO_DATA_FOUND THEN NULL;
--                WHEN OTHERS        THEN RAISE;
--        END;

--        BEGIN
--            /* cerco se e' presente una relazione compatibile con data
--               Data attivazione superiore alla Data attivazione in inserimento  */
--            SELECT DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE
--              INTO vDataAttiv,       vDataDisatt
--              FROM REL_ELEMENTI_GEO
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE = (SELECT MIN(DATA_ATTIVAZIONE)
--                                         FROM REL_ELEMENTI_GEO
--                                        WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--                                          AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--                                          AND DATA_ATTIVAZIONE   >= :NEW.DATA_ATTIVAZIONE);
--            /* poiche' il trigger di REL_ELEMENTI_GEO non consernte di modificare la Data di attivazione
--               la relazione viene cancellata e reinserita con la nuova Data di attivazione */
--            DELETE REL_ELEMENTI_GEO
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE    = vDataAttiv;
--            /* il record in inserimento assume ora la Data di disattivazione
--               del record precedentemente cancellato */
--            :NEW.DATA_DISATTIVAZIONE  := vDataDisatt;
--            EXCEPTION
--                WHEN NO_DATA_FOUND THEN NULL;
--                WHEN OTHERS        THEN RAISE;
--        END;

--        BEGIN
--            /* cerco se e' presente una relazione compatibile con data
--               Data disattivazione inferiore alla Data attivazione in inserimento  */
--            SELECT DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE
--              INTO vDataAttiv,       vDataDisatt
--              FROM REL_ELEMENTI_GEO
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_DISATTIVAZIONE = (SELECT MAX(DATA_DISATTIVAZIONE)
--                                            FROM REL_ELEMENTI_GEO
--                                           WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--                                             AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--                                             AND DATA_DISATTIVAZIONE < :NEW.DATA_ATTIVAZIONE);
--            -- la Data di attivazieme dell'elemento e' quella della relazione trovata
--            :NEW.DATA_ATTIVAZIONE := vDataAttiv;
--            -- elimino la relazione trovata che sara' sostituita dalla relazione in inserimento!
--            DELETE REL_ELEMENTI_GEO
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE    = vDataAttiv
--               AND DATA_DISATTIVAZIONE = vDataDisatt;
--        EXCEPTION
--            WHEN NO_DATA_FOUND THEN NULL;
--            WHEN OTHERS        THEN RAISE;
--        END;
--    END IF;

    IF :NEW.DATA_DISATTIVAZIONE > PKG_UtlGlb.gkDataTappo THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Data Disattivazione non deve eccedere la Data limite ' ||
                                                        '(' || TO_CHAR(PKG_UtlGlb.gkDataTappo,'yyyy/mm/dd') || ')');
    END IF;

--    IF :NEW.DATA_ATTIVAZIONE >= :NEW.DATA_DISATTIVAZIONE THEN
--        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR, 'Cod Elemento: Padre:'||:NEW.COD_ELEMENTO_PADRE||
--                                                                   ', Figlio:'||:NEW.COD_ELEMENTO_FIGLIO||
--                                                        ' - Data Attivazione deve essere inferiore a Data disattivazione '||
--                                                        TO_CHAR(:NEW.DATA_ATTIVAZIONE,'dd/mm/yyyy hh24:mi.ss')|| ' - '||
--                                                        TO_CHAR(:NEW.DATA_DISATTIVAZIONE,'dd/mm/yyyy hh24:mi.ss'));
--    END IF;

 EXCEPTION
    WHEN OTHERS THEN RAISE;

 END BEF_IUR_REL_ELEMENTI_GEO;
/


-- 
-- Non Foreign Key Constraints for Table REL_ELEMENTI_GEO 
-- 
Prompt Non-Foreign Key Constraints on Table REL_ELEMENTI_GEO;
ALTER TABLE REL_ELEMENTI_GEO ADD (
  CONSTRAINT REL_ELE_GEO_PK
  PRIMARY KEY
  (COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO, DATA_ATTIVAZIONE)
  USING INDEX REL_ELE_GEO_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table REL_ELEMENTI_GEO 
-- 
Prompt Foreign Key Constraints on Table REL_ELEMENTI_GEO;
ALTER TABLE REL_ELEMENTI_GEO ADD (
  CONSTRAINT ELE_RELGEO__F 
  FOREIGN KEY (COD_ELEMENTO_FIGLIO) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/

ALTER TABLE REL_ELEMENTI_GEO ADD (
  CONSTRAINT ELE_RELGEO__P 
  FOREIGN KEY (COD_ELEMENTO_PADRE) 
  REFERENCES ELEMENTI (COD_ELEMENTO)
  ENABLE VALIDATE)
/
