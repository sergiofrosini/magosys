Prompt Table SCHEDULED_TMP_MET;
--
-- SCHEDULED_TMP_MET  (Table) 
--
CREATE TABLE SCHEDULED_TMP_MET
(
  JOB_NUMBER       NUMBER,
  DATARIF          DATE,
  ORGANIZZAZIONE   NUMBER(1),
  STATO            NUMBER(1),
  ID_RETE          NUMBER(1),
  COD_TIPO_MISURA  VARCHAR2(6),
  DATAINS          DATE
)
TABLESPACE &TBS&DAT
/


Prompt Index SCHEDULED_TMP_MET_PK1;
--
-- SCHEDULED_TMP_MET_PK1  (Index) 
--
--  Dependencies: 
--   SCHEDULED_TMP_MET (Table)
--
CREATE UNIQUE INDEX SCHEDULED_TMP_MET_PK1 ON SCHEDULED_TMP_MET
(DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, COD_TIPO_MISURA)
TABLESPACE &TBS&IDX
/


Prompt Trigger BEF_SCHEDULED_TMP_MET;
--
-- BEF_SCHEDULED_TMP_MET  (Trigger) 
--
--  Dependencies: 
--   SCHEDULED_TMP_MET (Table)
--
CREATE OR REPLACE TRIGGER BEF_SCHEDULED_TMP_MET 
BEFORE INSERT OR UPDATE
ON SCHEDULED_TMP_MET REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_SCHEDULED_TMP_MET
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.h      26/09/2012   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.JOB_NUMBER   IS NULL THEN
            SELECT SCHEDULED_JOBS_PKSEQ.NEXTVAL INTO :NEW.JOB_NUMBER FROM DUAL;
        END IF;
        :NEW.DATAINS := SYSDATE;
    END IF;

END BEF_SCHEDULED_TMP_MET;
/


-- 
-- Non Foreign Key Constraints for Table SCHEDULED_TMP_MET 
-- 
Prompt Non-Foreign Key Constraints on Table SCHEDULED_TMP_MET;
ALTER TABLE SCHEDULED_TMP_MET ADD (
  CONSTRAINT SCHEDULED_TMP_MET_PK1
  PRIMARY KEY
  (DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, COD_TIPO_MISURA)
  USING INDEX SCHEDULED_TMP_MET_PK1
  ENABLE VALIDATE)
/
