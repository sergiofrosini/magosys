Prompt Table METEO_PREVISIONE;
--
-- METEO_PREVISIONE  (Table) 
--
CREATE TABLE METEO_PREVISIONE
(
  COD_CITTA                 NUMBER(8)           NOT NULL,
  DATA                      DATE                NOT NULL,
  TEMPERATURA               NUMBER(32,10),
  DESCRIZIONE               NUMBER(8),
  PRECIPITAZIONI            NUMBER(32,10),
  DIREZIONE_VENTO           NUMBER(32,10),
  VELOCITA_VENTO            NUMBER(32,10),
  INDICE_DI_RAFFREDDAMENTO  NUMBER(32,10),
  INDICE_DI_CALORE          NUMBER(32,10),
  UMIDITA_RELATIVA          NUMBER(32,10),
  VISIBILITA                NUMBER(32,10),
  PRESSIONE                 NUMBER(32,10),
  HZERO                     NUMBER(32,10),
  IRRAGGIAMENTO_SOLARE      NUMBER(32,10),
  COD_TIPO_COORD            VARCHAR2(1)         DEFAULT 'C',
  COD_PREV_METEO            VARCHAR2(1)
)
TABLESPACE &TBS&DAT
PARTITION BY RANGE (DATA)
(  
  PARTITION GEN2012 VALUES LESS THAN (TO_DATE(' 2012-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2012 VALUES LESS THAN (TO_DATE(' 2012-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2012 VALUES LESS THAN (TO_DATE(' 2012-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2012 VALUES LESS THAN (TO_DATE(' 2012-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2012 VALUES LESS THAN (TO_DATE(' 2012-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2012 VALUES LESS THAN (TO_DATE(' 2012-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2012 VALUES LESS THAN (TO_DATE(' 2012-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2012 VALUES LESS THAN (TO_DATE(' 2012-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2012 VALUES LESS THAN (TO_DATE(' 2012-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2012 VALUES LESS THAN (TO_DATE(' 2012-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2012 VALUES LESS THAN (TO_DATE(' 2012-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2012 VALUES LESS THAN (TO_DATE(' 2012-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2013 VALUES LESS THAN (TO_DATE(' 2013-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2013 VALUES LESS THAN (TO_DATE(' 2013-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2013 VALUES LESS THAN (TO_DATE(' 2013-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2013 VALUES LESS THAN (TO_DATE(' 2013-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2013 VALUES LESS THAN (TO_DATE(' 2013-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2013 VALUES LESS THAN (TO_DATE(' 2013-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2013 VALUES LESS THAN (TO_DATE(' 2013-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2013 VALUES LESS THAN (TO_DATE(' 2013-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2013 VALUES LESS THAN (TO_DATE(' 2013-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2013 VALUES LESS THAN (TO_DATE(' 2013-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2013 VALUES LESS THAN (TO_DATE(' 2013-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2013 VALUES LESS THAN (TO_DATE(' 2013-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2014 VALUES LESS THAN (TO_DATE(' 2014-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2014 VALUES LESS THAN (TO_DATE(' 2014-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2014 VALUES LESS THAN (TO_DATE(' 2014-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2014 VALUES LESS THAN (TO_DATE(' 2014-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2014 VALUES LESS THAN (TO_DATE(' 2014-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2014 VALUES LESS THAN (TO_DATE(' 2014-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2014 VALUES LESS THAN (TO_DATE(' 2014-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2014 VALUES LESS THAN (TO_DATE(' 2014-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2014 VALUES LESS THAN (TO_DATE(' 2014-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2014 VALUES LESS THAN (TO_DATE(' 2014-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2014 VALUES LESS THAN (TO_DATE(' 2014-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2014 VALUES LESS THAN (TO_DATE(' 2014-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2015 VALUES LESS THAN (TO_DATE(' 2015-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2015 VALUES LESS THAN (TO_DATE(' 2015-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2015 VALUES LESS THAN (TO_DATE(' 2015-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2015 VALUES LESS THAN (TO_DATE(' 2015-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2015 VALUES LESS THAN (TO_DATE(' 2015-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2015 VALUES LESS THAN (TO_DATE(' 2015-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2015 VALUES LESS THAN (TO_DATE(' 2015-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2015 VALUES LESS THAN (TO_DATE(' 2015-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2015 VALUES LESS THAN (TO_DATE(' 2015-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2015 VALUES LESS THAN (TO_DATE(' 2015-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2015 VALUES LESS THAN (TO_DATE(' 2015-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2015 VALUES LESS THAN (TO_DATE(' 2015-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2016 VALUES LESS THAN (TO_DATE(' 2016-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2016 VALUES LESS THAN (TO_DATE(' 2016-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2016 VALUES LESS THAN (TO_DATE(' 2016-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2016 VALUES LESS THAN (TO_DATE(' 2016-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2016 VALUES LESS THAN (TO_DATE(' 2016-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2016 VALUES LESS THAN (TO_DATE(' 2016-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2016 VALUES LESS THAN (TO_DATE(' 2016-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2016 VALUES LESS THAN (TO_DATE(' 2016-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2016 VALUES LESS THAN (TO_DATE(' 2016-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2016 VALUES LESS THAN (TO_DATE(' 2016-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2016 VALUES LESS THAN (TO_DATE(' 2016-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2016 VALUES LESS THAN (TO_DATE(' 2016-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2017 VALUES LESS THAN (TO_DATE(' 2017-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2017 VALUES LESS THAN (TO_DATE(' 2017-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2017 VALUES LESS THAN (TO_DATE(' 2017-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2017 VALUES LESS THAN (TO_DATE(' 2017-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2017 VALUES LESS THAN (TO_DATE(' 2017-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2017 VALUES LESS THAN (TO_DATE(' 2017-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2017 VALUES LESS THAN (TO_DATE(' 2017-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2017 VALUES LESS THAN (TO_DATE(' 2017-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2017 VALUES LESS THAN (TO_DATE(' 2017-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2017 VALUES LESS THAN (TO_DATE(' 2017-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2017 VALUES LESS THAN (TO_DATE(' 2017-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2017 VALUES LESS THAN (TO_DATE(' 2017-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2018 VALUES LESS THAN (TO_DATE(' 2018-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2018 VALUES LESS THAN (TO_DATE(' 2018-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2018 VALUES LESS THAN (TO_DATE(' 2018-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2018 VALUES LESS THAN (TO_DATE(' 2018-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2018 VALUES LESS THAN (TO_DATE(' 2018-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2018 VALUES LESS THAN (TO_DATE(' 2018-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2018 VALUES LESS THAN (TO_DATE(' 2018-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2018 VALUES LESS THAN (TO_DATE(' 2018-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2018 VALUES LESS THAN (TO_DATE(' 2018-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2018 VALUES LESS THAN (TO_DATE(' 2018-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2018 VALUES LESS THAN (TO_DATE(' 2018-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2018 VALUES LESS THAN (TO_DATE(' 2018-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2019 VALUES LESS THAN (TO_DATE(' 2019-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2019 VALUES LESS THAN (TO_DATE(' 2019-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2019 VALUES LESS THAN (TO_DATE(' 2019-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2019 VALUES LESS THAN (TO_DATE(' 2019-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2019 VALUES LESS THAN (TO_DATE(' 2019-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2019 VALUES LESS THAN (TO_DATE(' 2019-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2019 VALUES LESS THAN (TO_DATE(' 2019-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2019 VALUES LESS THAN (TO_DATE(' 2019-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2019 VALUES LESS THAN (TO_DATE(' 2019-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2019 VALUES LESS THAN (TO_DATE(' 2019-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2019 VALUES LESS THAN (TO_DATE(' 2019-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2019 VALUES LESS THAN (TO_DATE(' 2019-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GEN2020 VALUES LESS THAN (TO_DATE(' 2020-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION FEB2020 VALUES LESS THAN (TO_DATE(' 2020-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAR2020 VALUES LESS THAN (TO_DATE(' 2020-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION APR2020 VALUES LESS THAN (TO_DATE(' 2020-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MAG2020 VALUES LESS THAN (TO_DATE(' 2020-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION GIU2020 VALUES LESS THAN (TO_DATE(' 2020-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION LUG2020 VALUES LESS THAN (TO_DATE(' 2020-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION AGO2020 VALUES LESS THAN (TO_DATE(' 2020-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION SET2020 VALUES LESS THAN (TO_DATE(' 2020-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION OTT2020 VALUES LESS THAN (TO_DATE(' 2020-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION NOV2020 VALUES LESS THAN (TO_DATE(' 2020-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION DIC2020 VALUES LESS THAN (TO_DATE(' 2020-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
        TABLESPACE &TBS&DAT,  
  PARTITION MIS_NEXT_ALL VALUES LESS THAN (MAXVALUE)
        TABLESPACE &TBS&DAT
)
/


Prompt Index METEO_PREVISIONE_PK;
--
-- METEO_PREVISIONE_PK  (Index) 
--
--  Dependencies: 
--   METEO_PREVISIONE (Table)
--
CREATE UNIQUE INDEX METEO_PREVISIONE_PK ON METEO_PREVISIONE
(COD_CITTA, DATA)
  TABLESPACE &TBS&IDX
LOCAL (  
  PARTITION GEN2012
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2012
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2012
        TABLESPACE &TBS&IDX,  
  PARTITION APR2012
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2012
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2012
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2012
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2012
        TABLESPACE &TBS&IDX,  
  PARTITION SET2012
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2012
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2012
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2012
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2013
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2013
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2013
        TABLESPACE &TBS&IDX,  
  PARTITION APR2013
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2013
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2013
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2013
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2013
        TABLESPACE &TBS&IDX,  
  PARTITION SET2013
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2013
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2013
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2013
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2014
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2014
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2014
        TABLESPACE &TBS&IDX,  
  PARTITION APR2014
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2014
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2014
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2014
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2014
        TABLESPACE &TBS&IDX,  
  PARTITION SET2014
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2014
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2014
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2014
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2015
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2015
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2015
        TABLESPACE &TBS&IDX,  
  PARTITION APR2015
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2015
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2015
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2015
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2015
        TABLESPACE &TBS&IDX,  
  PARTITION SET2015
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2015
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2015
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2015
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2016
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2016
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2016
        TABLESPACE &TBS&IDX,  
  PARTITION APR2016
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2016
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2016
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2016
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2016
        TABLESPACE &TBS&IDX,  
  PARTITION SET2016
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2016
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2016
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2016
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2017
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2017
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2017
        TABLESPACE &TBS&IDX,  
  PARTITION APR2017
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2017
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2017
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2017
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2017
        TABLESPACE &TBS&IDX,  
  PARTITION SET2017
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2017
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2017
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2017
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2018
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2018
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2018
        TABLESPACE &TBS&IDX,  
  PARTITION APR2018
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2018
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2018
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2018
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2018
        TABLESPACE &TBS&IDX,  
  PARTITION SET2018
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2018
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2018
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2018
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2019
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2019
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2019
        TABLESPACE &TBS&IDX,  
  PARTITION APR2019
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2019
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2019
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2019
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2019
        TABLESPACE &TBS&IDX,  
  PARTITION SET2019
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2019
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2019
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2019
        TABLESPACE &TBS&IDX,  
  PARTITION GEN2020
        TABLESPACE &TBS&IDX,  
  PARTITION FEB2020
        TABLESPACE &TBS&IDX,  
  PARTITION MAR2020
        TABLESPACE &TBS&IDX,  
  PARTITION APR2020
        TABLESPACE &TBS&IDX,  
  PARTITION MAG2020
        TABLESPACE &TBS&IDX,  
  PARTITION GIU2020
        TABLESPACE &TBS&IDX,  
  PARTITION LUG2020
        TABLESPACE &TBS&IDX,  
  PARTITION AGO2020
        TABLESPACE &TBS&IDX,  
  PARTITION SET2020
        TABLESPACE &TBS&IDX,  
  PARTITION OTT2020
        TABLESPACE &TBS&IDX,  
  PARTITION NOV2020
        TABLESPACE &TBS&IDX,  
  PARTITION DIC2020
        TABLESPACE &TBS&IDX,  
  PARTITION MIS_NEXT_ALL
        TABLESPACE &TBS&IDX
)
/
