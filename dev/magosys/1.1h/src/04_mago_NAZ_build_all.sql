
PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGONAZ   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS=&1;
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT _______________________________________________________________________________________
PROMPT Definizione DBlinks...
PROMPT
@./DBLinks/PKG1_STMAUI.IT.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione Types...
PROMPT
@./Types/T_MISURA_GME_OBJ.sql;
@./Types/T_MISURA_GME_ARRAY.sql;
@./Types/T_MISREQ_OBJ.sql;
@./Types/T_MISREQ_ARRAY.sql;
@./Types/T_MISMETEO_OBJ.sql;
@./Types/T_MISMETEO_ARRAY.sql;
@./Types/T_PARAM_EOLIC_OBJ.sql
@./Types/T_PARAM_EOLIC_ARRAY.sql
@./Types/T_PARAM_PREV_OBJ.sql
@./Types/T_PARAM_PREV_ARRAY.sql

PROMPT _______________________________________________________________________________________
PROMPT Definizione Directoties...
PROMPT
@./Directories/MAGO_LOGDIR_&3.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione Sequences...
PROMPT
@./Sequences/ELEMENTI_PKSEQ.sql;
@./Sequences/METEO_FILE_XML_SEQ.sql;
@./Sequences/METEO_FILE_ZIP_SEQ.sql;
@./Sequences/METEO_JOB_PKSEQ.sql;
@./Sequences/SCHEDULED_JOBS_PKSEQ.sql;
@./Sequences/TRATTAMENTO_ELEMENTI_PKSEQ.sql;
@./Sequences/LOG_SEQUENCE.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione Tabelle...
PROMPT
@./Tables/GTTD_IMPORT_GERARCHIA.sql;
@./Tables/GTTD_CALC_GERARCHIA.sql;
@./Tables/GTTD_REP_ENERGIA_POTENZA.sql;
@./Tables/GTTD_MISURE.sql;
@./Tables/GTTD_MOD_ASSETTO_RETE_SA.sql;
@./Tables/GTTD_VALORI_TEMP.sql;
@./Tables/GTTD_FORECAST_ELEMENTS.sql
@./Tables/METEO_CITTA.sql
@./Tables/METEO_FILE_LETTO.sql;
@./Tables/METEO_JOB.sql;
@./Tables/METEO_JOB_STATIC_CONFIG.sql;
@./Tables/METEO_JOB_RUNTIME_CONFIG.sql;
@./Tables/METEO_PREVISIONE.sql;
@./Tables/METEO_REL_ISTAT.sql;
@./Tables/METEO_FILE_ZIP.sql;
@./Tables/METEO_FILE_XML.sql;
@./Tables/RAGGRUPPAMENTO_FONTI.sql;
@./Tables/TIPI_MISURA.sql;
@./Tables/TIPI_GRUPPI_MISURA.sql;
@./Tables/TIPI_PRODUTTORE.sql;
@./Tables/TIPI_RETE.sql;
@./Tables/TIPO_FONTI.sql;
@./Tables/TIPO_TECNOLOGIA_SOLARE.sql;
@./Tables/TIPI_ELEMENTO.sql;
@./Tables/TIPI_MISURA_CONV_ORIG.sql;
@./Tables/TIPI_MISURA_CONV_UM.sql;
@./Tables/ELEMENTI.sql;
@./Tables/ELEMENTI_DEF.sql;
@./Tables/DEFAULT_CO.sql;
@./Tables/TRATTAMENTO_ELEMENTI.sql;
@./Tables/FORECAST_PARAMETRI.sql;
@./Tables/GRUPPI_MISURA.sql;
@./Tables/MISURE_ACQUISITE.sql;
@./Tables/MISURE_ACQUISITE_STATICHE.sql;
@./Tables/MISURE_AGGREGATE.sql;
@./Tables/MISURE_AGGREGATE_STATICHE.sql;
@./Tables/REL_ELEMENTI_AMM.sql;
@./Tables/REL_ELEMENTI_ECP_SN.sql;
@./Tables/REL_ELEMENTI_ECP_SA.sql;
@./Tables/REL_ELEMENTI_ECS_SN.sql;
@./Tables/REL_ELEMENTI_ECS_SA.sql;
@./Tables/REL_ELEMENTI_GEO.sql;
@./Tables/REL_ELEMENTO_TIPMIS.sql;
@./Tables/SERVIZIO_MAGO.sql;
@./Tables/GERARCHIA_AMM.sql;
@./Tables/GERARCHIA_GEO.sql;
@./Tables/GERARCHIA_IMP_SN.sql;
@./Tables/GERARCHIA_IMP_SA.sql;
@./Tables/SCHEDULED_JOBS_DEF.sql;
@./Tables/SCHEDULED_TMP_GEN.sql;
@./Tables/SCHEDULED_TMP_MET.sql;
@./Tables/SCHEDULED_TMP_GME.sql;
@./Tables/SCHEDULED_JOBS.sql;
@./Tables/STORICO_IMPORT.sql;
@./Tables/APPLICATION_RUN.sql;
@./Tables/LOG_HISTORY.sql;
@./Tables/LOG_HISTORY_INFO.sql;
@./Tables/VERSIONS.sql;

PROMPT _______________________________________________________________________________________
PROMPT Procedure ...
PROMPT
@./Procedures/GET_METEODISTRIBLIST.sql

PROMPT _______________________________________________________________________________________
PROMPT Scheduled Jobs...
PROMPT
@./SchedulerJobs/ALLINEA_ANAGRAFICA.sql;
@./SchedulerJobs/MAGO_SCHEDULER.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGGREG.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGG_METEO.sql;
@./SchedulerJobs/MAGO_INS_REQ_AGG_GME.sql;
@./SchedulerJobs/PULIZIA_GIORNALIERA.sql;

PROMPT _______________________________________________________________________________________
PROMPT Pakages...
PROMPT
@./Packages/PKG_MAGO.sql;
@./Packages/PKG_ELEMENTI.sql;
@./Packages/PKG_LOGS.sql;
@./Packages/PKG_MISURE.sql;
@./Packages/PKG_AGGREGAZIONI.sql;
@./Packages/PKG_METEO.sql;
@./Packages/PKG_REPORTS.sql;
@./Packages/PKG_ANAGRAFICHE.sql;
@./Packages/PKG_SCHEDULER.sql;
@./Packages/PKG_INTEGRST.sql;
@./Packages/PKG_MANUTENZIONE.sql;

PROMPT _______________________________________________________________________________________
PROMPT Viste ...
PROMPT
@./Views/V_ESERCIZI.sql;
@./Views/V_ANAGRAFICA_IMPIANTO.sql;
@./Views/V_CURRENT_VERSION.sql;
@./Views/V_ELEMENTI.sql;
@./Views/V_MOD_ASSETTO_RETE_SA.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql;
@./Views/V_GERARCHIA_GEOGRAFICA.sql;
@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql;
@./Views/V_GERARCHIA_IMPIANTO_MT_BT.sql;
@./Views/V_PARAMETRI_APPLICATIVI.sql;
@./Views/V_TIPI_MISURA.sql;
@./Views/V_SCHEDULED_JOBS.sql;
@./Views/V_LOG_HISTORY_MAGO.sql;

PROMPT _______________________________________________________________________________________
PROMPT Viste Materializzate...
PROMPT
@./Views/V_SEARCH_ELEMENTS.sql;

PROMPT _______________________________________________________________________________________
PROMPT Pakage Bodies...
PROMPT
@./PackageBodies/PKG_MAGO.sql;
@./PackageBodies/PKG_ELEMENTI.sql;
@./PackageBodies/PKG_LOGS.sql;
@./PackageBodies/PKG_MISURE.sql;
@./PackageBodies/PKG_AGGREGAZIONI.sql;
@./PackageBodies/PKG_METEO.sql;
@./PackageBodies/PKG_REPORTS.sql;
@./PackageBodies/PKG_ANAGRAFICHE.sql;
@./PackageBodies/PKG_SCHEDULER.sql;
@./PackageBodies/PKG_INTEGRST.sql;
@./PackageBodies/PKG_MANUTENZIONE.sql;

PROMPT _______________________________________________________________________________________
PROMPT Inizializazzione Tabelle ...
PROMPT
@./InitTables/RAGGRUPPAMENTO_FONTI.sql;
@./InitTables/TIPO_FONTI.sql;
@./InitTables/TIPI_RETE.sql;
@./InitTables/TIPI_ELEMENTO.sql;
@./InitTables/TIPI_PRODUTTORE.sql;
@./InitTables/TIPI_MISURA.sql;
@./InitTables/TIPI_MISURA_CONV_ORIG.sql;
@./InitTables/TIPI_MISURA_CONV_UM.sql;
@./InitTables/TIPI_GRUPPI_MISURA.sql;
@./InitTables/GRUPPI_MISURA.sql;
@./InitTables/TIPO_TECNOLOGIA_SOLARE.sql;
@./InitTables/SCHEDULED_JOBS_DEF.sql;
@./InitTables/METEO_CITTA.sql;
@./InitTables/METEO_REL_ISTAT.sql;

PROMPT _______________________________________________________________________________________
PROMPT Triggers Tabelle ...
PROMPT
@./Triggers/BEF_IUR_TRATTAMENTO_ELEMENTI.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_GEO.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_AMM.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_ECP_SA.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_ECS_SN.sql;
@./Triggers/BEF_IUR_ELEMENTI.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_ECS_SA.sql;
@./Triggers/BEF_IUR_REL_ELEMENTI_ECP_SN.sql;
@./Triggers/BEF_IUR_SCHEDULED_JOBS.sql;
@./Triggers/BEF_SCHEDULED_TMP_GME.sql;
@./Triggers/BEF_SCHEDULED_TMP_GEN.sql;
@./Triggers/BEF_SCHEDULED_TMP_MET.sql;
@./Triggers/AFTI_APPLICATION_RUN.sql;

BEGIN
   INSERT INTO V_PARAMETRI_APPLICATIVI (KEY, VALORE, NOTA) VALUES ('TRACELEVEL', '0', 'Livello di Trace per packages:   2=Solo Errori; 1=Errori+Info; 0=Errori+Info+Dettaglio');
   COMMIT;
EXCEPTION 
   WHEN DUP_VAL_ON_INDEX THEN NULL;
END;
/

TRUNCATE TABLE VERSIONS;
INSERT INTO VERSIONS (DATA, VERSION, RELEASE, PATCH) VALUES (TO_DATE('19/12/2012', 'DD/MM/YYYY'), '1', '0.i', 2);
COMMIT;
/

PROMPT _______________________________________________________________________________________
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

