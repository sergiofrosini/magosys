
conn mago/mago

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WRAPPED;
SET LINES 220;
SET TIMING ON;

COL SPOOLNAME NEW_VALUE SPOOLNAME

SELECT COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_Situazione-Produttori-Mago.txt' SPOOLNAME
  FROM DEFAULT_CO
 INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
 LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
 WHERE flag_primario = 1
   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

SPOOL '&SPOOLNAME'

DECLARE 
    TYPE       t_ele IS VARRAY(12) OF ELEMENTI.COD_ELEMENTO%TYPE;
    vEle       t_ele :=  t_ele();
    vTip       TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vGst       ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCount     INTEGER := 0;
    vProd      VARCHAR2(100) := '*';
    vProd_old  VARCHAR2(100) := '*';
    vESE       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vCP        ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vTRF       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vRTR       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vSMT       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vLMT       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vSCS       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vCOM       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    vZNA       ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;  
    
    vNumPrdNoGen  INTEGER := 0;  
    vPotPrdNoGen  INTEGER := 0;  
    vPotTotGener  INTEGER := 0;  
    
BEGIN
    FOR i IN 1 .. 12 LOOP
        vEle.EXTEND;
        vEle(i) := NULL;
    END LOOP;
    
    DBMS_OUTPUT.PUT_LINE(RPAD('Num.',6,' ')        ||' '||
                         RPAD('PRODUTTORE',16,' ') ||' '||
                         RPAD('Nome',25,' ')       ||' '||
                         RPAD('POD',14,' ')        ||' '||
                         RPAD('Tipo',4,' ')        ||' '||
                         RPAD('Pot.Gr.',8,' ')     ||' '||
                         RPAD('Generatore',15,' ') ||' '||
                         RPAD('FTE',3,' ')         ||' '||
                         RPAD('Pot.Disp',8,' ')    ||'  '||
                         RPAD('Ese.',4,' ')        ||' '||        
                         RPAD('Cab.Prim.' ,12,' ') ||' '||        
                         RPAD('Trasf.',15,' ')     ||' '||        
                         RPAD('Sec.Trasf.',15,' ') ||' '||        
                         RPAD('Sbarra MT',15,' ')  ||' '||        
                         RPAD('Linea MT',15,' ')   ||' '||        
                         RPAD('Sbarra CS',15,' ')  ||' '||
                         RPAD('Comune',7,' ')      ||' '||
                         RPAD('Zona',5,' ')
                        );
    DBMS_OUTPUT.PUT_LINE(RPAD('-',6,'-')           ||' '||
                         RPAD('-',16,'-')          ||' '||
                         RPAD('-',25,'-')          ||' '||
                         RPAD('-',14,'-')          ||' '||
                         RPAD('-',4,'-')           ||' '||
                         RPAD('-',8,'-')           ||' '||
                         RPAD('-',15,'-')          ||' '||
                         RPAD('-',3,'-')           ||' '||
                         RPAD('-',8,'-')           ||'  '||
                         RPAD('-',4,'-')           ||' '||        
                         RPAD('-',12,'-')          ||' '||        
                         RPAD('-',15,'-')          ||' '||        
                         RPAD('-',15,'-')          ||' '||        
                         RPAD('-',15,'-')          ||' '||        
                         RPAD('-',15,'-')          ||' '||        
                         RPAD('-',15,'-')          ||' '||
                         RPAD('-',7,'-')           ||' '||
                         RPAD('-',5,'-')
                        );
    FOR I IN (SELECT E.COD_ELEMENTO,
                     E.COD_GEST_ELEMENTO,
                     RPAD(E.COD_GEST_ELEMENTO,16,' ')   ||' '||
                     RPAD(C.RAGIO_SOC,25,' ')           ||' '||
                     RPAD(C.POD,15,' ')                 ||' '||
                     RPAD(C.TIPO_FORN,2,' ')            ||' '||
                     LPAD(TO_CHAR(C.POT_GRUPPI),8,' ')   PRODUTTORE,
                     G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE GENERATORE, 
                     G.TIPO_IMP FONTE, 
                     (G.P_APP_NOM * NVL(F_P_NOM,1)) P_APP_NOM,
                     L02 l1, 
                     L03 l2, 
                     L04 l3, 
                     L05 l4, 
                     L06 l5, 
                     L07 l6, 
                     L08 l7, 
                     L09 l8, 
                     L10 l9,
                     L11 l10, 
                     L12 l11,
                     L13 l12,
                     C.POT_DISP
                FROM ELEMENTI E
               INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO
               INNER JOIN GERARCHIA_IMP_SA G ON G.COD_ELEMENTO = E.COD_ELEMENTO
                LEFT JOIN (SELECT *
                              FROM CLIENTI_TLC@PKG1_STMAUi.IT C
                             WHERE C.STATO = 'E'
                               AND C.TRATTAMENTO = 0
                            ) C ON C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE = E.COD_GEST_ELEMENTO
                  LEFT OUTER JOIN 
                           (SELECT *
                              FROM GENERATORI_TLC@PKG1_STMAUi.IT G
                             WHERE G.STATO = 'E'
                               AND G.TRATTAMENTO = 0
                            ) G ON G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||'U'||G.ID_CL = E.COD_GEST_ELEMENTO
               WHERE E.COD_TIPO_ELEMENTO = 'PMT'
                 AND SYSDATE BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                 AND SYSDATE BETWEEN G.DATA_ATTIVAZIONE AND G.DATA_DISATTIVAZIONE
               ORDER BY 2, 4
             ) LOOP
        vESE      := ' ';  
        vCP       := ' ';  
        vTRF      := ' ';  
        vRTR      := ' ';  
        vSMT      := ' ';  
        vLMT      := ' ';  
        vSCS      := ' ';  
        vCOM      := ' ';  
        vZNA      := ' ';  
        IF i.PRODUTTORE <> vProd_old THEN
            vCount := vCount + 1;
            vProd     := RPAD(TO_CHAR(vCount),5,'  ')||'  '||i.PRODUTTORE;
            vProd_old := i.PRODUTTORE;
            vEle(1) := i.l1;
            vEle(2) := i.l2;
            vEle(3) := i.l3;
            vEle(4) := i.l4;
            vEle(5) := i.l5;
            vEle(6) := i.l6;
            vEle(7) := i.l7;
            vEle(8) := i.l8;
            vEle(9) := i.l9;
            vEle(10) := i.l10;
            vEle(11) := i.l11;
            vEle(12) := i.l12;
            FOR j IN 1 .. 12 LOOP
                IF vEle(j) IS NOT NULL THEN
                    SELECT COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO
                      INTO vGst, vTip
                      FROM ELEMENTI 
                     WHERE COD_ELEMENTO = vEle(j);
                    CASE vTip
                        WHEN 'ESE' THEN vESE := vGst;
                        WHEN 'CPR' THEN vCP  := vGst;
                        WHEN 'TRF' THEN vTRF := vGst;
                        WHEN 'TRS' THEN vRTR := vGst;
                        WHEN 'TRT' THEN vRTR := vGst;
                        WHEN 'SMT' THEN vSMT := vGst;
                        WHEN 'LMT' THEN vLMT := vGst;
                        WHEN 'SCS' THEN vSCS := vGst;
                        ELSE NULL;
                    END CASE;
                    IF vSCS IS NULL THEN 
                        vSCS := PKG_ELEMENTi.GETGESTELEMENTO(PKG_ELEMENTi.GETELEMENTOPADRE(vEle(j),'SCS',SYSDATE,1,2));
                    END IF;
                    vCOM := PKG_ELEMENTi.GETGESTELEMENTO(PKG_ELEMENTi.GETELEMENTOPADRE(vEle(j),'COM',SYSDATE,2,2));
                    vZNA := PKG_ELEMENTi.GETGESTELEMENTO(PKG_ELEMENTi.GETELEMENTOPADRE(vEle(j),'ZNA',SYSDATE,3,2));
                END IF;
            END LOOP;
            IF i.GENERATORE IS NULL THEN
                vNumPrdNoGen := vNumPrdNoGen + 1;
                vPotPrdNoGen := vPotPrdNoGen + NVL(i.POT_DISP,0);
            END IF; 
        ELSE
            vProd     := ' ';
        END IF;
        vPotTotGener := vPotTotGener + NVL(i.P_APP_NOM,0);
        DBMS_OUTPUT.PUT_LINE(RPAD(vProd,77,' ')                        ||'  '||
                             RPAD(NVL(i.GENERATORE,' '),15,' ')        ||'  '|| 
                             NVL(i.FONTE,' ')                          ||'  '|| 
                             LPAD(NVL(TO_CHAR(i.P_APP_NOM),' '),8,' ') ||'  '||
                             RPAD(vESE,4,' ')                          ||' '||        
                             RPAD(vCP ,12,' ')                         ||' '||        
                             RPAD(vTRF,15,' ')                         ||' '||        
                             RPAD(vRTR,15,' ')                         ||' '||        
                             RPAD(vSMT,15,' ')                         ||' '||        
                             RPAD(vLMT,15,' ')                         ||' '||        
                             RPAD(vSCS,15,' ')                         ||' '||
                             RPAD(vCOM,7,' ')                          ||' '||
                             RPAD(vZNA,5,' ')
                            );
    END LOOP;
    DBMS_OUTPUT.PUT_LINE(' ');
    DBMS_OUTPUT.PUT_LINE(RPAD('*',220,'*'));
    DBMS_OUTPUT.PUT_LINE(' ');
    DBMS_OUTPUT.PUT_LINE('Produttori senza generarori associati           : '||vNumPrdNoGen);
    --DBMS_OUTPUT.PUT_LINE('Potenza Produttori persa per assenza generarori : '||TO_CHAR(vPotPrdNoGen / 1000)||' Kw');
    DBMS_OUTPUT.PUT_LINE(' ');
    DBMS_OUTPUT.PUT_LINE('Potenza Totale Generatori                       : '||TO_CHAR(vPotTotGener / 1000)||' Kw');
END;
/


SPOOL OFF

EXIT;

