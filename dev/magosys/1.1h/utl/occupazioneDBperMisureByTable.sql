SELECT SUBSTR(TABLE_NAME,1,20) TABELLA,
       INITCAP(TO_CHAR(MESE,'month yyyy', 'NLS_DATE_LANGUAGE=Italian')) MESE,
       MB,
       SUM(MB) OVER (PARTITION BY 1 ORDER BY TABLE_NAME,MESE ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) "PROGR.MB",
       MISURE,NOTA,
       TO_CHAR(Tot_MB) Tot_MB,
       TO_CHAR(ROUND(Tot_MB / 1024,2)) Tot_GB,
       TO_CHAR(Tot_Misure) Tot_Misure
  FROM (SELECT MESE,
               TABLE_NAME,MB,
               SUM(MB) OVER (PARTITION BY 1 ORDER BY 1 DESC) Tot_MB,
               SUM(Misure) OVER (PARTITION BY 1 ORDER BY 1 DESC) Tot_Misure,
               MISURE,
               NOTA
          FROM (SELECT MESE, 
                       TABLE_NAME,
                       ROUND(((BYTES/1024)/1024),2) MB, 
                       NUM_ROWS MISURE,
                       CASE 
                        WHEN LAST_DAY(MESE) <= LAST_ANALYZED THEN NULL
                                                             ELSE 'Misure stimate al '||TO_CHAR(LAST_ANALYZED,'dd/mm/yyyy hh24:mi')
                       END NOTA 
                  FROM (SELECT UI.TABLE_NAME, 
                               CASE US.PARTITION_NAME
                                 WHEN 'MIS_NEXT_ALL' THEN TO_DATE('01013000','ddmmyyyy')
                                 ELSE TO_DATE(US.PARTITION_NAME,'MONYYYY', 'NLS_DATE_LANGUAGE=Italian')
                               END MESE, 
                               SUM(UP.NUM_ROWS) NUM_ROWS,
                               SUM(BYTES) BYTES,
                               MIN(UP.LAST_ANALYZED) LAST_ANALYZED
                          FROM USER_SEGMENTS US
                         INNER JOIN USER_INDEXES UI ON UI.INDEX_NAME=US.SEGMENT_NAME
                         LEFT OUTER JOIN USER_TAB_PARTITIONS UP ON UP.TABLE_NAME=UI.TABLE_NAME AND UP.PARTITION_NAME=US.PARTITION_NAME
                         WHERE UI.TABLE_NAME IN ('MISURE_ACQUISITE','MISURE_AGGREGATE') 
                           AND UP.NUM_ROWS > 0
                         GROUP BY UI.TABLE_NAME, US.PARTITION_NAME
                       ) A
               )
       ) A
 ORDER BY TABLE_NAME, A.MESE;
