
conn mago/mago

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WRAPPED;
SET LINES 90;
SET TIMING ON;

COL SPOOLNAME NEW_VALUE SPOOLNAME

SELECT COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_Stato-Rete-Elettrica.txt' SPOOLNAME
  FROM DEFAULT_CO
 INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
 LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
 WHERE flag_primario = 1
   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

SPOOL '&SPOOLNAME'

DECLARE
   vData DATE    := SYSDATE;     -- to_date('14/06/2013 18:20:59','dd/mm/yyyy hh24:mi:ss');
   vSta  INTEGER := 2;           -- 1=stato Normale,  2=stato Attuale

   vLst  PKG_UtlGlb.t_query_cur;
   vTot  INTEGER := 0;
   vTip  GTTD_VALORI_TEMP.TIP%TYPE := 'TIPELETMP';
   vNum  INTEGER;
   vIndent  CHAR(2) := ' ';

   vNomeEse VARCHAR2(90);
   
   vBlok    INTEGER;
   vTipErr  VARCHAR2(50);
   vCodSG   VARCHAR2(20);
   vCodST   VARCHAR2(20);
   vTipEle  VARCHAR2(10);
   vNomEle  VARCHAR2(90);
   vTipo    INTEGER;

   vNumero INTEGER := 0;
   vBlock_sav   INTEGER      := -1;
   vTipo_sav    INTEGER      := -1;
   vTipErr_sav  VARCHAR2(50) := '*';
   vCodSG_sav   VARCHAR2(20) := '*';
   
   PROCEDURE StampaTotale AS
        BEGIN
             IF vNumero > 0 THEN
                DBMS_OUTPUT.PUT_LINE(' ');
                DBMS_OUTPUT.PUT_LINE(vIndent||vIndent||'Totale : '||vNumero);
                vNumero := 0;
             END IF;
        END;

BEGIN

    DELETE GTTD_VALORI_TEMP WHERE TIP = vTip; 

    SELECT COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO INTO vNomeEse
      FROM DEFAULT_CO
     INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
     LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
     WHERE flag_primario = 1
       AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

    pkg_anagrafiche.ElementsCheckReport(vLst,vSta,vData);
    IF vSta = 1 THEN
        DBMS_OUTPUT.PUT_LINE(vNomeEse||' - STATO NORMALE');
    ELSE
        DBMS_OUTPUT.PUT_LINE(vNomeEse||' - STATO ATTUALE');
    END IF;
    DBMS_OUTPUT.PUT_LINE(RPAD('*',90,'*'));
    DBMS_OUTPUT.PUT_LINE(' ');
    LOOP
        FETCH vLst INTO   vBlok,vTipErr,vCodSG ,vCodST,vTipEle,vNomEle,vTipo;
        EXIT WHEN vLst%NOTFOUND;
        
        SELECT COUNT(*) INTO vNum FROM GTTD_VALORI_TEMP WHERE TIP = vTip AND ALF1 = vTipEle;
        IF vNum = 0 THEN
            INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1) VALUES (vTip,vTipEle);
        END IF;
        
        vTot := vTot + 1;
        IF (vBlock_sav <> vBlok) OR (vTipo_sav <> vTipo) THEN
            StampaTotale;
        END IF;
       IF vBlock_sav <> vBlok THEN
            DBMS_OUTPUT.PUT_LINE(' ');
            DBMS_OUTPUT.PUT_LINE(RPAD('=',90,'='));
            IF vBlok = 1 THEN
                DBMS_OUTPUT.PUT_LINE('Severita'': Alta  - Elementi esclusi dalla gerarchia');
            ELSE
                DBMS_OUTPUT.PUT_LINE('Severita'': Media - Elementi inclusi in gerarchia ma con inesattezze in composizione del CG');
            END IF;
            DBMS_OUTPUT.PUT_LINE(RPAD('=',90,'='));
            vBlock_sav := vBlok;
        END IF;
        vTipo_sav := vTipo;
        IF vTipErr_sav <> vTipErr THEN
            DBMS_OUTPUT.PUT_LINE(' ');
            DBMS_OUTPUT.PUT_LINE(vIndent||RPAD('-',88,'-'));
            DBMS_OUTPUT.PUT_LINE(vIndent||SUBSTR(vTipErr,1,45));
            DBMS_OUTPUT.PUT_LINE('  '||RPAD('-',88,'-'));
            DBMS_OUTPUT.PUT_LINE('  '||'  '||RPAD('Cod. Gestionale',20,' ')||'  '||RPAD('Codice ST',20,' ')||'  '||RPAD('Tipo',10,' ')||'  '||RPAD('NOME',30,' ')) ;
            DBMS_OUTPUT.PUT_LINE('  '||'  '||RPAD('-',20,'-')||'  '||RPAD('-',20,'-')||'  '||RPAD('-',10,'-')||'  '||   RPAD('-',30,'-')) ;
            vTipErr_sav := vTipErr;
        END IF;
        IF INSTR(vTipErr,'duplicato') > 0 THEN
            IF vCodSG_sav <> vCodSG THEN
                DBMS_OUTPUT.PUT_LINE('  '||'  '||RPAD(vCodSG,20,' ')||'  '||RPAD(vCodST,20,' ')||'  '||RPAD(vTipEle ,10,' ')||'  '||SUBSTR(vNomEle,1,30)) ;
                vCodSG_sav := vCodSG;
            ELSE
                DBMS_OUTPUT.PUT_LINE('  '||'  '||RPAD(' ',20,' ')||'  '||RPAD(vCodST,20,' ')||'  '||RPAD(vTipEle ,10,' ')||'  '||SUBSTR(vNomEle,1,30)) ;
            END IF;
        ELSE
            DBMS_OUTPUT.PUT_LINE('  '||'  '||RPAD(vCodSG,20,' ')||'  '||RPAD(vCodST,20,' ')||'  '||RPAD(vTipEle ,10,' ')||'  '||SUBSTR(vNomEle,1,30)) ;
        END IF;
        vNumero := vNumero + 1;
   END LOOP;
   StampaTotale;
   DBMS_OUTPUT.PUT_LINE(' ');
   DBMS_OUTPUT.PUT_LINE(RPAD('*',90,'*'));
   DBMS_OUTPUT.PUT_LINE('Legenda');
   DBMS_OUTPUT.PUT_LINE(RPAD('-',90,'-'));
   DBMS_OUTPUT.PUT_LINE(' ');
   FOR i IN (SELECT CODIFICA_ST, DESCRIZIONE 
               FROM GTTD_VALORI_TEMP
              INNER JOIN TIPI_ELEMENTO ON CODIFICA_ST  = ALF1 AND TIP = vTip
              ORDER BY 1
            ) LOOP
       DBMS_OUTPUT.PUT_LINE(vIndent||RPAD(i.CODIFICA_ST,11,' ')||i.Descrizione);
   END LOOP;
END;
/


SPOOL OFF

EXIT;
