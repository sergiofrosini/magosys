#!/usr/bin/ksh
#
#       scarica_gmepr_MAGO.ksh        (Siemens Italia SpA)
#
#       Data:           Giugno 2012 
#	Autore		E. Castrignano
#	Aggiornamenti:  
#
#       Modifiche da implementare :
#            - Ottobre 2012: gestione scarico da siti esercizio. 
# 
#       Descrizione:
#            Script che scarica dal un server di scambio
#            i files GMEPR per l'utilizzo nell'applicativo MAGO
#       Parametri:
#            Esistenza di un file che indica se i dati dell'esercizio di
#            competenza sono utilizzati anche come test per il centro prove.
#
#       Modalita' di attivazione:
#            Da crontab
#
#----------------------------------------------------------------------------------

##
# Definizione delle variabili di ambiente ORACLE 
##
export ORACLE_HOME="/oracle_bases/ARCDB2"
export ORACLE_SID="ARCDB2"

##
# Definizione delle directory di lavoro 
##
TMPDIR="/usr/NEW/magosys/tmp"
UTLDIR="/usr/NEW/magosys/utl"
CORDIR="/usr/NEW/magosys/corrupted/gme/MT"
BINDIR="/usr/NEW/magosys/bin"
LOGDIR="/usr/NEW/magosys/log"
GMEMAGODIR="/usr/NEW/magosys/fromGME/MT"
PCAUIMAGODIR="/MAGO"
##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"

##
# Definizione dei comandi di sistema utilizzati nello script 
##

AUI_HOST="pc_aui"
AUI_PWD="chktele28"
AUI_USR="TELECHK"
AUTH="sar_admin/sar_admin_dba"
CAT="${USRDIR}/cat"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
GREP="${USRDIR}/grep"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
UNZIP="${USRDIR2}/unzip"
WC="${USRDIR2}/wc"

##
# Definizione delle variabili di date-time 
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}

##
# Definizione nomi files
##
MAGOLOG="download_GMEPR_${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp.log"
ESECOMP_X_CP="${UTLDIR}/esecompTest.txt"

##
# Definizione variabili varie
##
MAX_TRY=3

function gest_file_integrity_GMEPR
{
     EXIST_FILE=$(${LS} -1a ${TMPDIR}/*_GMEPR_*.zip | ${WC} -l)
     if (( $EXIST_FILE != 0 )); then
          cd $TMPDIR
          ${LS} *_GMEPR_*.zip > ${TMPDIR}/test_integrity_gmepr.txt
          for filename in $(${CAT} ${TMPDIR}/test_integrity_gmepr.txt)
          do
               ${UNZIP} -t $filename > /dev/null
               case "${?}" in
               0 )           ${ECHO} "FILE $filename INTEGRO (TRASMISSIONE COMPLETATA) [OK] al ${DATA}" >> ${LOGDIR}/${MAGOLOG}
     	                     ;;
               1 )           ${ECHO} "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [ERROR] al ${DATA}" >> ${LOGDIR}/${MAGOLOG}
                             ${MV} $filename $CORDIR
     	                     ;;
               2 )           ${ECHO} "FILE $filename NON INTEGRO (TRASMISSIONE NON COMPLETATA) [WARNING] al ${DATA}" >> ${LOGDIR}/${MAGOLOG}
                             ${MV} $filename $CORDIR
     	                     ;;
               * )           ${ECHO} "FILE $filename NON INTEGRO (gestione di default del case) [ERROR] al ${DATA}" >> ${LOGDIR}/${MAGOLOG}
                             ${MV} $filename $CORDIR
     	                     ;;
               esac;
          done
          ${RM} -f ${TMPDIR}/test_integrity_gmepr.txt
     fi
     return 0
}

# MAGO
##
# Processo di attivazione FTP per ricezione file GMEPR
##
function ricevi_files_GMEPR
{ 
     attempt=1
     check=0

     while (( $check == 0 && $attempt <= $MAX_TRY ))
     do
          ${ECHO} "Inizio fase ricezione file(s) GMEPR tentativo $attempt di ${MAX_TRY}" >> ${LOGDIR}/${MAGOLOG}

          cd ${TMPDIR}

          ${NETCAT} -vw 5 -z $AUI_HOST 21
          retping=$(${ECHO} ${?})

          if [[ "${retping}" != "0" ]]; then
               ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

	       if (( $attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                    sleep ${n_random}m
	       fi	   

	       attempt=$(($attempt+1))
          else
               ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile!" >> ${LOGDIR}/${MAGOLOG}
	  
     
${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd STM_${ESECOMP}/GME 
mget ${ESECOMP}_GMEPR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GMEPR FALLITA" >> ${LOGDIR}/${MAGOLOG} 

	            if (( $attempt < 3 )); then 
                         n_random=$((${RANDOM}%15+1))
                         echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                         sleep ${n_random}m
	            fi	   

	            attempt=$(($attempt+1))
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
	            ${ECHO} "FTP file(s) GMEPR COMPLETATA; cancellazione file(s) da pc_aui." >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd STM_${ESECOMP}/GME 
mdelete ${ESECOMP}_GMEPR_*.zip
bye
eofftp

                    gest_file_integrity_GMEPR || return 1

     	            ${ECHO} "Riposizionamento file(s) GMEPR nella cartella di lavoro ${GMEMAGODIR}." >> ${LOGDIR}/${MAGOLOG}

	            ${MV} *_GMEPR_*.zip $GMEMAGODIR

		    check=1
               fi
          fi
     done

     if (( $attempt > $MAX_TRY )); then
	  return 1
     fi

     ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
     ${ECHO} "Fine fase ricezione file(s) GMEPR" >> ${LOGDIR}/${MAGOLOG}

     return 0
}

function send_file_to_cp
{
     s_attempt=1
     s_check=0

     while (( $s_check == 0 && $s_attempt <= $MAX_TRY ))
     do
          ${ECHO} "Inizio spedizione file(s) GMEPR ad utilizzo centro prove tentativo $s_attempt di ${MAX_TRY}" >> ${LOGDIR}/${MAGOLOG}

          cd ${TMPDIR}

          ${NETCAT} -vw 5 -z $AUI_HOST 21
          retping=$(${ECHO} ${?})

          if [[ "${retping}" != "0" ]]; then
               ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

	       if (( $s_attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                    sleep ${n_random}m
	       fi	   

	       s_attempt=$(($s_attempt+1))
          else
               ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd ${PCAUIMAGODIR} 
mput ${ESECOMP}_GMEPR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GMEPR FALLITA" >> ${LOGDIR}/${MAGOLOG} 

	            if (( $s_attempt < 3 )); then 
                         n_random=$((${RANDOM}%15+1))
                         echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                         sleep ${n_random}m
	            fi	   

	            s_attempt=$(($s_attempt+1))
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GMEPR COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

		    s_check=1
	       fi
          fi
     done

     if (( $s_attempt > $MAX_TRY )); then
	  return 1
     fi

     ${ECHO} "Fine spedizione file(s) GMEPR ad utilizzo centro prove" >> ${LOGDIR}/${MAGOLOG}
     return 0
}

function ricevi_files_GMEPR_double
{ 
     attempt=1
     check=0

     while (( $check == 0 && $attempt <= $MAX_TRY ))
     do
          ${ECHO} "Inizio fase ricezione file(s) GMEPR per gestione cabina duplicata in centro prove tentativo $attempt di ${MAX_TRY}" >> ${LOGDIR}/${MAGOLOG}

          cd ${TMPDIR}

          ${NETCAT} -vw 5 -z $AUI_HOST 21
          retping=$(${ECHO} ${?})

          if [[ "${retping}" != "0" ]]; then
               ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile!" >> ${LOGDIR}/${MAGOLOG}
	       
	       if (( $attempt < 3 )); then 
                    n_random=$((${RANDOM}%15+1))
                    echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                    sleep ${n_random}m
	       fi	   

	       attempt=$(($attempt+1))
          else
               ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd STM_${ESECOMP}/GME 
mget ${ESECOMP}_GMEPR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} ${?}) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GMEPR FALLITA" >> ${LOGDIR}/${MAGOLOG} 
	       
	            if (( $attempt < 3 )); then 
                         n_random=$((${RANDOM}%15+1))
                         echo "sleep di $n_random min prima del prossimo tentativo..." >> ${LOGDIR}/${MAGOLOG}
                         sleep ${n_random}m
	            fi	   

	            attempt=$(($attempt+1))
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GMEPR COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

                    gest_file_integrity_GMEPR || return 1

                    send_file_to_cp || return 1

	            ${ECHO} "Riposizionamento file(s) GMEPR nella cartella di lavoro ${GMEMAGODIR}." >> ${LOGDIR}/${MAGOLOG}

                    ${MV} *_GMEPR_*.zip $GMEMAGODIR

	            ${ECHO} "Cancellazione file(s) da pc_aui." >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd STM_${ESECOMP}/GME 
mdelete ${ESECOMP}_GMEPR_*.zip
bye
eofftp
                    
                   check=1
               fi
          fi
     done

     if (( $attempt > $MAX_TRY )); then
	  return 1
     fi

     ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
     ${ECHO} "Fine fase ricezione file(s) GMEPR" >> ${LOGDIR}/${MAGOLOG}

     return 0
}

function main
{

     ${ECHO} "__________________________________________________________________________________________________________________________" >> ${LOGDIR}/${MAGOLOG}
     ${ECHO} "Controllo esistenza e trasmissione file(s) GMEPR per MAGO ${GIORNO}/${MESE}/${ANNO} ${H}:${M}:${S}\n" >> ${LOGDIR}/${MAGOLOG}

     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 1;
whenever oserror exit 1;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select ut.CODIFICA_UTR||e.CODIFICA_ESERCIZIO
  from ESERCIZI_ABILITATI eab
 inner join ESERCIZI e on eab.COD_ESERCIZIO = e.COD_ESERCIZIO
                      and eab.COD_UTR = e.COD_UTR
 inner join UNITA_TERRITORIALI ut on ut.COD_UTR = e.COD_UTR
 where eab.COD_APPLICAZIONE='ADM';

exit 140
!)
     if [[ "${?}" != "140" ]]; then
          echo "Errore recupero dati!" >> ${LOGDIR}/${MAGOLOG}
          return 1
     fi

     export ESECOMP

     if [[ -e "${ESECOMP_X_CP}" ]]; then

          ricevi_files_GMEPR_double || return 1
     else
          ricevi_files_GMEPR || return 1
     fi

     return 0
}

main || exit 1

exit 0
