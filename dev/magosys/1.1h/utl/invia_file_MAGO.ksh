#!/usr/bin/ksh
#
#       invia_file_MAGO.ksh        (Siemens Italia SpA)
#
#       Data:           Oct. 2011 
#	Autore		G. Migliaccio, E. Castrignano
#	Aggiornamenti:  
#
#       Modifiche da implementare :
#               - N.A. 
# 
#       Descrizione:
#            tool per rigirare i file misura STM*MIS verso 
#            il tmesx2 del centro prove facendoli transitare 
#            da server di scambio
#       Modalita' di attivazione:
#         Solo per C.O. MILANO
#
#----------------------------------------------------------------------------------

##
# Definizione delle variabili di ambiente ORACLE 
##
export ORACLE_HOME="/oracle_bases/ARCDB1"
export ORACLE_SID="ARCDB1"
export TWO_TASK="ARCDB1"

##
# Definizione delle directory di lavoro 
##
FILDIR="/usr/NEW/tmesx_2/filedati/from-AUI"
MAGODIR="/usr/NEW/tmesx_2/to_MAGO"
MAGODIRAUI="/MAGO"
GMEDIR="/usr/NEW/tmesx_2/filedati/to-GME"
OUTDIR="/usr/NEW/tmesx_2/filedati"
TMPDIR="/usr/NEW/tmesx_2/tmp"
REJDIR="/usr/NEW/tmesx_2/rejected"
CORDIR="/usr/NEW/tmesx_2/corrupted"
BINDIR="/usr/NEW/tmesx_2/bin"
LOGDIR="/usr/NEW/tmesx_2/log"
MISBCKDIR="/usr/NEW/tmesx_2/backup"
GMEBCKDIR="/usr/NEW/tmesx_2/backup_gme"
MAGOGMEDIR="/usr/NEW/magosys/fromGME/AT"
##
# Definizione delle directory di sistema
##
USRDIR="/bin"
USRDIR2="/usr/bin"
TOODIR="/bin/mtools"

##
# Definizione dei comandi di sistema utilizzati nello script 
##

AUI_HOST="pc_aui"
AUI_PWD="chktele28"
AUI_USR="TELECHK"
AUTH="tmesx_2/tmesx_2"
MAGO_HOST="pkg_arcdb2"
MAGO_USR="magosys"
MAGO_PWD="Magosys"
CAT="${USRDIR}/cat"
CP="${USRDIR}/cp"
CUT="${USRDIR2}/cut"
ECHO="${USRDIR}/echo -e"
FTP="${USRDIR2}/ftp"
GREP="${USRDIR}/grep"
KSH="${USRDIR}/ksh"
LDR="${ORACLE_HOME}/bin/sqlldr"
LS1="${USRDIR}/ls -1"
LS="${USRDIR}/ls -tr"
MV="${USRDIR}/mv"
NETCAT="${USRDIR2}/netcat"
RM="${USRDIR}/rm"
SQP="${ORACLE_HOME}/bin/sqlplus"
WC="${USRDIR2}/wc"

##
# Definizione delle variabili di date-time 
##
GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
DATA=${GIORNO}-${MESE}-${ANNO}_${ORA}
DATA_NEW=${ANNO}${MESE}${GIORNO}_${ORA}


##
# Definizione delle variabili di controllo 
##
CHECKFILE="0"

##
# Definizione nomi files
##
MAGOLOG="mago_upload____${ANNO}${MESE}${GIORNO}.log"
FTPMAGOLOG="mago_ftp.log"
FTPMAGOCHECKSEMAFORO="mago_chek_semaforo.log"
SEMAFORO_MIS="semaforo_mis.txt"
SEMAFORO_GME="semaforo_gme.txt"


# MAGO
##
# Check file semaforo 
##
function CheckSemaforo
{ 

     ${ECHO} "Inizio controllo semaforo per invio file(s)" >> ${LOGDIR}/${MAGOLOG}

     CHECKFILE="0"

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile!" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
	  ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI 
ls ${semaforo}
bye
eofftp

          ${GREP} ${semaforo} ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} > /dev/null
          retcode=$(${ECHO} $?) 
          if [[ "${retcode}" = "0" ]]; then
               ${CAT} ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} >> ${LOGDIR}/${MAGOLOG} 
               ${ECHO} "Semaforo rosso per invio!" >> ${LOGDIR}/${MAGOLOG} 
          else
               ${CAT} ${TMPDIR}/${FTPMAGOCHECKSEMAFORO} >> ${LOGDIR}/${MAGOLOG} 
               ${ECHO} "Semaforo verde per invio!" >> ${LOGDIR}/${MAGOLOG}
	       CHECKFILE="1"
          fi;

          ${RM} -f ${TMPDIR}/${FTPMAGOCHECKSEMAFORO}
     fi;

     ${ECHO} "Fine controllo semaforo per invio file(s)" >> ${LOGDIR}/${MAGOLOG}

     return 0
}


# MAGO
##
# Processo di attivazione FTP per trasferimento file MIS di tmesx per MAGO-centroprove
##
function invia_files_MIS
{ 
     ${ECHO} "Inizio fase trasmissione file(s) MIS per centro prove\n" >> ${LOGDIR}/${MAGOLOG}

     cd ${MAGODIR}

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile! (UPLOAD MIS TO MAGO)\n" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile! (UPLOAD MIS TO MAGO)\n" >> ${LOGDIR}/${MAGOLOG}

	  ${ECHO} "Trasmissione file(s) MIS per MAGO" > $semaforo

          EXIST_FILE_MIS=$(${LS1} STM*MIS.gz | ${WC} -l)

          if [[ "${EXIST_FILE_MIS}" != "0" ]]; then

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI 
mput STM*MIS.gz
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} $?) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) MIS FALLITA" >> ${LOGDIR}/${MAGOLOG} 
		    return 1
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) MIS COMPLETATA" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
put $semaforo
bye
eofftp

                    ${MV} STM*MIS.gz $MISBCKDIR
               fi;
          else
               ${ECHO} "Nessun file(s) MIS da trasferire" >> ${LOGDIR}/${MAGOLOG}
          fi;

	  ${RM} -f $semaforo

          ${ECHO} "Fine fase trasmissione file(s) MIS per centro prove" >> ${LOGDIR}/${MAGOLOG}
     fi;

     return 0
}

# MAGO
##
# Processo di attivazione FTP per trasferimento file GME per MAGO-centroprove
##
function invia_files_GME_to_centro_prove
{ 
     ${ECHO} "Inizio fase trasmissione file(s) GME per centro prove\n" >> ${LOGDIR}/${MAGOLOG}

     cd ${MAGODIR}
# rimozione file txt scompattati
     rm -f *.txt.gz

     ${NETCAT} -vw 5 -z $AUI_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "Nodo server AUI (${AUI_HOST}) NON raggiungibile! (UPLOAD GME TO MAGO)" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "Nodo server AUI (${AUI_HOST}) raggiungibile! (UPLOAD GME TO MAGO)" >> ${LOGDIR}/${MAGOLOG}

	  ${ECHO} "Trasmissione file(s) GME per MAGO" > $semaforo

	  EXIST_FILE_GME=$(${LS1} ${ESECOMP}_GMETR_*.zip | ${WC} -l)

          if [[ "${EXIST_FILE_GME}" != "0" ]]; then

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI 
mput ${ESECOMP}_GMETR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} $?) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GME FALLITA verso pc_aui" >> ${LOGDIR}/${MAGOLOG} 
		    return 1
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GME COMPLETATA verso pc_aui" >> ${LOGDIR}/${MAGOLOG}

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $AUI_HOST
user $AUI_USR $AUI_PWD
bin
prompt
cd $MAGODIRAUI
put $semaforo
bye
eofftp

               fi;

               ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
          else
               ${ECHO} "Nessun file(s) GME da trasferire" >> ${LOGDIR}/${MAGOLOG}
          fi;

	  ${RM} -f $semaforo

          ${ECHO} "Fine fase trasmissione file(s) GME per centro prove" >> ${LOGDIR}/${MAGOLOG}
     fi;

     return 0
}

# MAGO
##
# Processo di attivazione FTP per trasferimento file GME per MAGO-centroprove
##
function invia_files_GME_to_MAGO
{ 
     ${ECHO} "Inizio fase trasmissione file(s) GME verso MAGO\n" >> ${LOGDIR}/${MAGOLOG}

     cd ${MAGODIR}

     ${NETCAT} -vw 5 -z $MAGO_HOST 21
     retping=$(${ECHO} $?)

     if [[ "${retping}" != "0" ]]; then
          ${ECHO} "${MAGO_HOST} NON raggiungibile!" >> ${LOGDIR}/${MAGOLOG}
	  return 1
     else
          ${ECHO} "${MAGO_HOST} raggiungibile!" >> ${LOGDIR}/${MAGOLOG}

	  ${ECHO} "Trasmissione file(s) GME verso MAGO" > $semaforo

	  EXIST_FILE_GME=$(${LS1} ${ESECOMP}_GMETR_*.zip | ${WC} -l)

          if [[ "${EXIST_FILE_GME}" != "0" ]]; then

${FTP} -vn <<eofftp 1>&2 > ${TMPDIR}/${FTPMAGOLOG}
open $MAGO_HOST
user $MAGO_USR $MAGO_PWD
bin
prompt
cd $MAGOGMEDIR
mput ${ESECOMP}_GMETR_*.zip
bye
eofftp

               ${GREP} "226" ${TMPDIR}/${FTPMAGOLOG} > /dev/null
               retcode=$(${ECHO} $?) 
               if [[ "${retcode}" = "1" ]]; then
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GME FALLITA verso MAGO" >> ${LOGDIR}/${MAGOLOG} 
		    return 1
               else
                    ${CAT} ${TMPDIR}/${FTPMAGOLOG} >> ${LOGDIR}/${MAGOLOG} 
                    ${ECHO} "FTP file(s) GME COMPLETATA verso MAGO" >> ${LOGDIR}/${MAGOLOG}

                    ${MV} ${ESECOMP}_GMETR_*.zip $GMEBCKDIR
               fi;

               ${RM} -f ${TMPDIR}/${FTPMAGOLOG}
          else
               ${ECHO} "Nessun file(s) GME da trasferire" >> ${LOGDIR}/${MAGOLOG}
          fi;

	  ${RM} -f $semaforo

          ${ECHO} "Fine fase trasmissione file(s) GME verso MAGO" >> ${LOGDIR}/${MAGOLOG}
     fi;

     return 0
}


##
# 
##
function main
{

     ${ECHO} "__________________________________________________________________________________________________________________________" >> ${LOGDIR}/${MAGOLOG}
     ${ECHO} "Controllo esistenza e trasmissione files MIS e GME per MAGO ${GIORNO}/${MESE}/${ANNO} ${H}:${M}:${S}\n" >> ${LOGDIR}/${MAGOLOG}

     ESECOMP=$(${SQP} -s ${AUTH} <<!
whenever sqlerror exit 1;
whenever oserror exit 1;
set echo off
set feed off
set term off
set verify off
set pages 0 lines 11 tab off
set head off
select e.COD_GEST_ELEMENTO
  from ELEMENTI e
 inner join DEFAULT_CO d on e.COD_ELEMENTO = d.COD_ELEMENTO_ESE
 where d.FLAG_PRIMARIO=1
   and e.COD_TIPO_ELEMENTO='W';
exit 140
!)
     if [[ "${?}" != "140" ]]; then
          echo "Errore recupero dati!" >> ${LOGDIR}/${MAGOLOG}
          return 1
     fi

     export ESECOMP

     semaforo=$SEMAFORO_MIS
     CheckSemaforo || return 1

     if [[ "${CHECKFILE}" = "1" ]]; then
	  invia_files_MIS 
     fi;

     semaforo=$SEMAFORO_GME
     CheckSemaforo || return 1

     if [[ "${CHECKFILE}" = "1" ]]; then
	 invia_files_GME_to_centro_prove || return 1
     fi;

     invia_files_GME_to_MAGO || return 1

     return 0
}

main || exit 1

exit 0
