spool check_start_date_mds.log

conn mago/mago@arcdb2

select START_DATE
from METEO_JOB_RUNTIME_CONFIG;

update METEO_JOB_RUNTIME_CONFIG set START_DATE = '06-mag-2013'
where START_DATE = '08-mag-2013';

select START_DATE
from METEO_JOB_RUNTIME_CONFIG;

commit;

spool off;

exit;