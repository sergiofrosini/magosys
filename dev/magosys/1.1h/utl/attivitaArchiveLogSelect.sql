SELECT TO_CHAR(first_time,'YYYY-MM-DD  Day', 'NLS_DATE_LANGUAGE=ITALIAN') DAY,
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'00',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'01',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'02',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'03',1,0))) * 200 "00-03",
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'04',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,3),'05',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'06',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'07',1,0))) * 200 "04-07",
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'08',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'09',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'10',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'11',1,0))) * 200 "08-11",
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'12',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'13',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'14',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'15',1,0))) * 200 "12-15",
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'16',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'17',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'18',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'19',1,0))) * 200 "16-19",
       (SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'20',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'21',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'22',1,0)) +
        SUM(DECODE(SUBSTR(TO_CHAR(first_time,'HH24'),1,2),'23',1,0))) * 200 "20-23",
       COUNT(*) * 200 tot_gg
  FROM v$log_history
 WHERE first_time > TRUNC(SYSDATE) -20 
 GROUP BY TO_CHAR(first_time,'YYYY-MM-DD  Day', 'NLS_DATE_LANGUAGE=ITALIAN')
 ORDER BY  1;
