#!/usr/bin/csh

if ( $# != 2 ) then
   echo "Inserire Password utente SYS"
   echo "Inserire tipologia di lancio        ( create_MAGONAZ, rebuild_MAGONAZ ) "
   echo ""
   echo "usage : ./BUILD_MAGONAZ [ sys_password ] [ create_MAGONAZ | rebuild_MAGONAZ ] "
   echo ""
else
   cd ../src
   switch($2)
     case create_MAGONAZ :
         setenv SPOOL ../install/create_MAGONAZ.log
         breaksw
      case rebuild_MAGONAZ :
         setenv SPOOL ../install/rebuild_MAGONAZ.log
         breaksw
      case * :
           echo ""
           echo "usage : ./BUILD_MAGONAZ [ create_MAGONAZ | rebuild_MAGONAZ ] "
           echo ""
           exit
   endsw
   set OraInst = ""
   $ORACLE_HOME/bin/sqlplus -s sys/$1 as sysdba @../src/test_oracle_edition.sql > $SPOOL
   set esito = $?
   if ( $esito == 10 ) then
      set OraInst = "ENT"
   else if ( $esito == 20 ) then
      set OraInst = "STD"
   else
      echo ""
      echo ">>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!">>$SPOOL
      echo "\a\a>>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!"
      echo ""
      exit
   endif
   echo "Esecuzione in corso - attendere prego ..."
   switch($2)
      case create_MAGONAZ :
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./create_magonaz.sql>>$SPOOL
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_magonaz.sql $1 $2 $OraInst>>$SPOOL
         breaksw
      case rebuild_MAGONAZ :
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_magonaz.sql $1 $2 $OraInst>>$SPOOL
         breaksw
      case nothing :
         breaksw
      case * :
         echo "usage : ./BUILD_MAGONAZ [ create_MAGONAZ | rebuild_MAGONAZ ] "
         exit
   endsw
   echo ""
   echo ""
#  grep "ERROR" $SPOOL 
   echo "--------------------------------------------------------------------------"
   grep -E '(ERROR|unable to open file)' $SPOOL 
   if ( $? == 0 ) then
      echo "Riscontrati probabili errori, consultare file di log $SPOOL"
   else
      echo "Elaborazione terminata regolarmente"
      echo "--------------------------------------------------------------------------"
      #echo "eseguire ./definisci_CO.sh per la definizione del CO di default"
      echo "L'inizializzazione sara' eseguita automaticamente alla ricezione della prima Consistenza Rete"
   endif
   echo "--------------------------------------------------------------------------"
   echo ""
   echo ""
endif


