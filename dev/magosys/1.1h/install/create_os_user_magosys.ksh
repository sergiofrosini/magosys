#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
TOUCH="/bin/touch"
DF="/bin/df"
GREP="/bin/grep"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"
SSH="/usr/bin/ssh"
SCP="/usr/bin/scp"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  $ECHO "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi

RUN_DIR=`${PWD}`

#
# Aggiunta dell'utente per applicazione MAGO 
#
${MKDIR} /home/magosys
useradd -u 10320 -c "Utente applicazione MAGO" -d /home/magosys -g dba -G nobody -s /bin/bash magosys  
# Creazione file .bashrc che definisce le variabili globali ORACLE_HOME e ORACLE_SID
${TOUCH} /home/magosys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magosys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magosys/.bashrc 
${ECHO} "cd /usr/NEW/magosys" >> /home/magosys/.bashrc
${CHOWN} -R magosys:dba /home/magosys
${CHMOD} 644 /home/magosys/.bashrc

path=`$PWD`
#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magosys/magosys
${CHOWN} -R magosys:dba /home/magosys/magosys
cd /home/magosys/magosys 
${LN} /usr/NEW/magosys Runtime
cd $path 

#
# Impostazione password
#
$ECHO "Impostazione password per utente magosys"
${PASWD} magosys


#
# Installazione ambiente Trasferimento misure per MAGO 
#
${MKDIR} /usr/NEW
${MKDIR} /usr/NEW/magosys
${CP} ambiente.tar.gz /usr/NEW/magosys
cd /usr/NEW/magosys
${GUNZIP} ambiente.tar.gz|${TAR} xvfp -
${RM} ambiente.tar*

#
# Chown sulle dir di mago
#
cd /usr/NEW
${CHOWN} -R magosys:dba magosys
cd /usr/NEW/magosys
${CHMOD} -R 775 *

#
# Attivazione scheduling processi MAGO 
#
$ECHO "Ricordarsi di attivare il cron"
#crontab -u magosys -r
#crontab -u magosys $path/magosys_crontab 


