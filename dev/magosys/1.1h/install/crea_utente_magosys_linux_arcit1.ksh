#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
TOUCH="/bin/touch"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  ${ECHO} "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi

PWD="/bin/pwd"

#
# aggiunta dell'utente per applicazione Trasferimento misure per MAGO
#
${MKDIR} /home/magosys
useradd -u 10320 -c "Utente Gestione misure per Mon. Active Distr. Grid Op" -d /home/magosys -g dba -G nobody -s /bin/bash magosys  
${TOUCH} /home/magosys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magosys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magosys/.bashrc
${ECHO} "cd /usr/NEW/magosys" >> /home/magosys/.bashrc
${CHMOD} 644 /home/magosys/.bashrc
${CHOWN} -R magosys:dba /home/magosys

#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magosys/magosys
${CHOWN} -R magosys:dba /home/magosys/magosys
cd /home/magosys/magosys
${LN} /usr/NEW/magosys Runtime

#
# Impostazione password
#
$ECHO " impostazione password per utente magosys"
${PASWD} magosys

${MKDIR} /usr/NEW
cd /usr/NEW
${LN} /oracle_bases/ARCDB2/dataload/magosys magosys

#
# Attivazione scheduling processi MAGO
#
#crontab -u magosys -r
#crontab -u magosys /tmp/magosys_crontab
