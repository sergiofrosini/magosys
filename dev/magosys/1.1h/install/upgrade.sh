#!/usr/bin/csh
set home = "/usr/NEW/magosys"
if ( $# != 2 ) then
   echo "Inserire Utente DGF di riferimento e versione"
else
   set username = $1
   set version = $2
   set filename = "to_"$version"_version.sql" 
   mv $home/upgrade/$filename $home/src/$filename
   cd $home/src
   setenv SPOOL ../install/upgrade_$version.log
   echo "Esecuzione in corso - attendere prego ..."
   $ORACLE_HOME/bin/sqlplus $username/$username @./$filename>$SPOOL
   mv $home/src/$filename $home/upgrade/$filename
   echo "Esecuzione Terminata"
endif
