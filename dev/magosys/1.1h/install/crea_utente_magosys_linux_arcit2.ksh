#!/bin/ksh

#
# Comandio utilizzati nello script 
#
AWK="/bin/awk"
TOUCH="/bin/touch"
DF="/bin/df"
GREP="/bin/grep"
ECHO="/bin/echo"
PASWD="/usr/bin/passwd"
PWD="/bin/pwd"
TAR="/bin/tar"
GUNZIP="/bin/gunzip -c"
CHMOD="/bin/chmod"
CHOWN="/bin/chown"
CP="/bin/cp -p"
RM="/bin/rm -f"
MKDIR="/bin/mkdir -p"
LN="/bin/ln -ns"
SSH="/usr/bin/ssh"
SCP="/usr/bin/scp"

#
# Verifica login root 
#
RUID=`/usr/bin/id|$AWK -F\( '{print $2}'|$AWK -F\) '{print $1}'`
if [ ${RUID} != "root" ];then
  $ECHO "Questo script deve essere eseguito da utente root, eseguire il login come root"
  exit 1
fi
# Controllo se ARCDBx e' montato
IS_MOUNTED_DB1=`${DF}|${GREP} ARCDB2 |${AWK} -F/ '{print $3}'`
if [ x${IS_MOUNTED_DB1} != "xARCDB2" ];then
  ${ECHO} "   ATTENZIONE eseguire il mount di /oracle_bases/ARCDB2"
  ${ECHO} "              eseguendo lo spostamento di pkg_ARCDB2"
  exit 1
fi

RUN_DIR=`${PWD}`

#
# Aggiunta dell'utente per applicazione MAGO 
#
${MKDIR} /home/magosys
useradd -u 10320 -c "Utente Gestione misure per Mon. Active Distr. Grid Op" -d /home/magosys -g dba -G nobody -s /bin/bash magosys  
# Creazione file .bashrc che definisce le variabili globali ORACLE_HOME e ORACLE_SID
${TOUCH} /home/magosys/.bashrc
${ECHO} "export ORACLE_HOME=/oracle_bases/ARCDB2" >> /home/magosys/.bashrc
${ECHO} "export ORACLE_SID=ARCDB2" >> /home/magosys/.bashrc 
${ECHO} "cd /usr/NEW/magosys" >> /home/magosys/.bashrc
${CHOWN} -R magosys:dba /home/magosys
${CHMOD} 644 /home/magosys/.bashrc

path=`$PWD`
#
# Impostazione per ambiente runtime
#
${MKDIR} /home/magosys/magosys
${CHOWN} -R magosys:dba /home/magosys/magosys
cd /home/magosys/magosys 
${LN} /usr/NEW/magosys Runtime
cd $path 

#
# Impostazione password
#
$ECHO "Impostazione password per utente magosys"
${PASWD} magosys


#
# Installazione ambiente Trasferimento misure per MAGO 
#
${MKDIR} /oracle_bases/ARCDB2/dataload 
${MKDIR} /oracle_bases/ARCDB2/dataload/magosys
${CHMOD} 775 /oracle_bases/ARCDB2/dataload
${CHMOD} 775 /oracle_bases/ARCDB2/dataload/magosys
${CP} ambiente.tar.gz /oracle_bases/ARCDB2/dataload/magosys
cd /oracle_bases/ARCDB2/dataload/magosys
${GUNZIP} ambiente.tar.gz|${TAR} xvfp -
${RM} ambiente.tar.gz

#
# Chown sulle dir di magosys
#
cd /oracle_bases/ARCDB2/dataload
${CHOWN} -R magosys:dba magosys
cd /oracle_bases/ARCDB2/dataload/magosys
${CHMOD} -R 775 backup bin lib corrupted fromST fromGME fromEXT install log tmp utl
${MKDIR} /usr/NEW
cd /usr/NEW
${LN} /oracle_bases/ARCDB2/dataload/magosys magosys

#
# Attivazione scheduling processi MAGO 
# da sistemare con script di cluster
# crontab -u magosys -r
# crontab -u magosys $path/magosys_crontab 

#
# Creazione utente su arcit1 
#
cd ${RUN_DIR}
${ECHO} "Creazione utente su Arcit1"
${ECHO} "Inserire password di root"
${SCP}  $path/crea_utente_magosys_linux_arcit1.ksh ArcIt1:/tmp
${ECHO} "Inserire password di root"
${SSH} ArcIt1 /tmp/crea_utente_magosys_linux_arcit1.ksh
# ${ECHO} "Inserire password di root"
# ${SCP}  $path/magosys_crontab arcit2:/tmp
