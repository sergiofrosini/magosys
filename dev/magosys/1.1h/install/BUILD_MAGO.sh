#!/usr/bin/csh

if ( $# != 2 ) then
   echo "Inserire Password utente SYS"
   echo "Inserire tipologia di lancio        ( create_MAGO, rebuild_MAGO ) "
   echo ""
   echo "usage : ./BUILD_MAGO [ sys_password ] [ create_MAGO | rebuild_MAGO ] "
   echo ""
else
   cd ../src
   switch($2)
     case create_MAGO :
         setenv SPOOL ../install/create_MAGO.log
         breaksw
      case rebuild_MAGO :
         setenv SPOOL ../install/rebuild_MAGO.log
         breaksw
      case * :
           echo ""
           echo "usage : ./BUILD_MAGO [ create_MAGO | rebuild_MAGO ] "
           echo ""
           exit
   endsw
   set OraInst = ""
   $ORACLE_HOME/bin/sqlplus -s sys/$1 as sysdba @../src/test_oracle_edition.sql > $SPOOL
   set esito = $?
   if ( $esito == 10 ) then
      set OraInst = "ENT"
   else if ( $esito == 20 ) then
      set OraInst = "STD"
   else
      echo ""
      echo ">>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!">>$SPOOL
      echo "\a\a>>>ERRORE: Tipologia installazione Oracle non riconosciuta  ($esito) - richiedere assistenza!"
      echo ""
      exit
   endif
   echo "Esecuzione in corso - attendere prego ..."
   switch($2)
      case create_MAGO :
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./create_mago.sql>>$SPOOL
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_mago.sql $1 $2 $OraInst>>$SPOOL
         breaksw
      case rebuild_MAGO :
         $ORACLE_HOME/bin/sqlplus sys/$1 as sysdba @./build_mago.sql $1 $2 $OraInst>>$SPOOL
         breaksw
      case nothing :
         breaksw
      case * :
         echo "usage : ./BUILD_MAGO [ create_MAGO | rebuild_MAGO ] "
         exit
   endsw
   echo ""
   echo ""
#  grep "ERROR" $SPOOL 
   echo "--------------------------------------------------------------------------"
   grep -E '(ERROR|unable to open file)' $SPOOL 
   if ( $? == 0 ) then
      echo "Riscontrati probabili errori, consultare file di log $SPOOL"
   else
      echo "Elaborazione terminata regolarmente"
      echo "--------------------------------------------------------------------------"
      #echo "eseguire ./definisci_CO.sh per la definizione del CO di default"
      echo "L'inizializzazione sara' eseguita automaticamente alla ricezione della prima Consistenza Rete"
   endif
   echo "--------------------------------------------------------------------------"
   echo ""
   echo ""
endif


