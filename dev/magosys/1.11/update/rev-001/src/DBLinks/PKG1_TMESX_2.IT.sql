PROMPT DATABASE LINK "PKG1_TMESX_2.IT";
--
-- "PKG1_TMESX_2.IT"  (Database Link) 
--

BEGIN
    EXECUTE IMMEDIATE 'DROP DATABASE LINK PKG1_TMESX_2.IT';
EXCEPTION
    WHEN others THEN IF SQLCODE = -02024 THEN  --ORA-02024: database link non trovato
                        NULL;
                     ELSE
                        RAISE;
                     END IF;
END;
/

CREATE DATABASE LINK PKG1_TMESX_2.IT
 CONNECT TO TMESX_2
 IDENTIFIED BY TMESX_2
 USING 'ARCDB1'
/


