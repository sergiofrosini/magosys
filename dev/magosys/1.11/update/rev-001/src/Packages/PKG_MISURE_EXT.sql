PROMPT PACKAGE PKG_MISURE_EXT;
--
-- PKG_MISURE_EXT  (Package) 
--
--  Dependencies: 
--   CORELE ()
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   GTTD_MISURE (Table)
--   GTTD_VALORI_TEMP (Table)
--   MONTANTIMT_SA (Table)
--   PKG_ELEMENTI (Package)
--   PKG_MAGO (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   PLITBLM ()
--   PLITBLM (Synonym)
--   STANDARD (Package)
--   TIPI_ELEMENTO (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--
CREATE OR REPLACE PACKAGE PKG_MISURE_EXT AS

/* ***********************************************************************************************************
   NAME:       PKG_MISURE_EXT
   PURPOSE:    Servizi per la gestione delle Misure reperibili da sistemi esterni

   REVISIONS:  Vedere Package Body per History

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_MisTmesx2 IS RECORD  (COD_TIPO_MISURA TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE, -- tipo misuta TMESX2
                              DATA            DATE,
                              COD_TIPO_FONTE  RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,  
                              VALORE          NUMBER
                             );

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureTmesx2(pTipoMis          IN TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE, 
                           pGstElem          IN TMX2_ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pDataDa           IN DATE,
                           pDataA            IN DATE,
                           pAgrTemp          IN NUMBER);

 PROCEDURE GetMisureTmesx2(pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                           pTipoMis          IN TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE, 
                           pGstElem          IN TMX2_ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pDataDa           IN DATE,
                           pDataA            IN DATE,
                           pAgrTemp          IN NUMBER);

-- ----------------------------------------------------------------------------------------------------------

END PKG_MISURE_EXT;
/
SHOW ERRORS;


