PROMPT TABLE TIPI_MISURA_EXT;
--
-- TIPI_MISURA_EXT  (Table) 
--

CREATE OR REPLACE TRIGGER BEF_IUR_TIPI_MISURA 
BEFORE INSERT OR UPDATE
ON TIPI_MISURA
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_TIPI_MISURA
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      13/06/2016   Moretti C.      Created this trigger.
   
   NOTES:

***************************************************************************** */
    vCount INTEGER;
BEGIN
    SELECT COUNT(*)
      INTO vCount 
      FROM TIPI_MISURA_EXT
     WHERE COD_TIPO_MISURA_EXT = :NEW.COD_TIPO_MISURA;
     IF vCount > 0 THEN
        RAISE_APPLICATION_ERROR(-20111,'Il codice Tipo Misura '''||:NEW.COD_TIPO_MISURA||''' e'' gia'' usato in tabella TIPI_MISURA_EXT'); 
     END IF;
END BEF_IUR_TIPI_MISURA;
/