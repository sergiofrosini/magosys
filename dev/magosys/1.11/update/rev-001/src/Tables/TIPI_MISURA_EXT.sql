PROMPT TABLE TIPI_MISURA_EXT;
--
-- TIPI_MISURA_EXT  (Table) 
--

BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE TIPI_MISURA_EXT PURGE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/
 
CREATE TABLE TIPI_MISURA_EXT
(
  COD_TIPO_MISURA_EXT             VARCHAR2(6),
  CODIFICA_ST                     VARCHAR2(20),
  DESCRIZIONE                     VARCHAR2(60),
  EXT_APPLIC                      VARCHAR2(20)  NOT NULL,
  MIS_GENERAZIONE                 NUMBER(1)     DEFAULT 0,
  RISOLUZIONE_FE                  NUMBER(3),
  COD_UM_STANDARD                 VARCHAR2(6),
  RILEVAZIONI_NAGATIVE            NUMBER(1)     DEFAULT 0,
  SPLIT_FONTE_ENERGIA             NUMBER(1)     DEFAULT 0,
  TIPO_INTERPOLAZIONE_FREQ_CAMP   CHAR(1)       DEFAULT '0',
  TIPO_INTERPOLAZIONE_BUCHI_CAMP  CHAR(1)       DEFAULT '0',
  FLG_MANUTENZIONE                NUMBER        DEFAULT 1,
  PRIORITA_AGGR                   NUMBER(1)     DEFAULT 1,
  FLG_MIS_CARICO                  NUMBER(1)     DEFAULT 0,
  FLG_MIS_GENERAZIONE             NUMBER(1)     DEFAULT 0,
  FLG_APPLY_FILTER                NUMBER(1)     DEFAULT 0,
  FLG_KPI                         NUMBER        DEFAULT 0,
  FLG_VAR_PI                      NUMBER(1)     DEFAULT 0,
  FLG_VAR_PIMP                    NUMBER(1)     DEFAULT 0
)
TABLESPACE &TBS&DAT
/

COMMENT ON TABLE TIPI_MISURA_EXT IS 'Tabella di definizione dei tipi misura  non gestite direttamente da MAGO ma reperibili attraverso mago da sisteni esterni'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.COD_TIPO_MISURA_EXT IS 'Codice Tipo Misura conosciuto dall''applicazione esterna'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.CODIFICA_ST IS 'Codice Tipo Misura ST'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.DESCRIZIONE IS 'Descrizione del tipo misura'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.EXT_APPLIC IS 'Identificativo dell''applicazione esterna da cui reperire le misure'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.MIS_GENERAZIONE IS '1=Misura proveniente da generazione'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.RISOLUZIONE_FE IS 'Risoluzione di visualizzazione da parte del FE'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.COD_UM_STANDARD IS 'Unita'' di misura standard'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.RILEVAZIONI_NAGATIVE IS 'La misura puo'' avere rilevazioni negative: 0=false; 1=true'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.TIPO_INTERPOLAZIONE_FREQ_CAMP IS 'Tipo di interpolazione da applicare alla curva per la frequenza di campionamento; 0=Default; 1=Lineare; 2=Spline; 3=Gradino'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.TIPO_INTERPOLAZIONE_BUCHI_CAMP IS 'Tipo di interpolazione da applicare alla curva che presenta buchi di campionamento; 0=nessuna(Lascia il buco); 1=Lineare; 2=Spline; 3=Gradino'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_MANUTENZIONE IS '1 = Misura che varia in funzione degli elementi in manutenzione'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.PRIORITA_AGGR IS 'Priorit¿ di aggregazione'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_APPLY_FILTER IS '1=Alla misura si applicano i filtri per fonti diverse da Solare/Eolico'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_KPI IS '1=La misura e'' un indice KPI'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_VAR_PI IS '1=La misura varia in funzione della Potenza Installata - Utilizzato per Replay'
/

COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_VAR_PIMP IS '1=La misura varia in funzione della Potenza Impegnata - Utilizzato per Replay'
/



PROMPT INDEX TIPI_MISURA_EXT_PK;
--
-- TIPI_MISURA_EXT_PK  (Index) 
--
--  Dependencies: 
--   TIPI_MISURA_EXT (Table)
--
CREATE UNIQUE INDEX TIPI_MISURA_EXT_PK ON TIPI_MISURA_EXT
(COD_TIPO_MISURA_EXT)
TABLESPACE &TBS&IDX
/


PROMPT TRIGGER BEF_IUR_TIPI_MISURA_EXT;
--
-- BEF_IUR_TIPI_MISURA_EXT  (Trigger) 
--
--  Dependencies: 
--   TIPI_MISURA_EXT (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_TIPI_MISURA_EXT 
BEFORE INSERT OR UPDATE
ON TIPI_MISURA_EXT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_IUR_TIPI_MISURA_EXT
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      13/06/2016   Moretti C.      Created this trigger.
   
   NOTES:

***************************************************************************** */
    vCount INTEGER;
BEGIN
    SELECT COUNT(*)
      INTO vCount 
      FROM TIPI_MISURA
     WHERE COD_TIPO_MISURA = :NEW.COD_TIPO_MISURA_EXT;
     IF vCount > 0 THEN
        RAISE_APPLICATION_ERROR(-20111,'Il codice Tipo Misura '''||:NEW.COD_TIPO_MISURA_EXT||''' e'' gia'' usato in tabella TIPI_MISURA'); 
     END IF;
END BEF_IUR_TIPI_MISURA_EXT;
/


-- 
-- Non Foreign Key Constraints for Table TIPI_MISURA_EXT 
-- 
PROMPT Non-FOREIGN KEY CONSTRAINTS ON TABLE TIPI_MISURA_EXT;
ALTER TABLE TIPI_MISURA_EXT ADD (
  CONSTRAINT TIPI_MISURA_EXT_C01
  CHECK (RILEVAZIONI_NAGATIVE IN (0,1))
  ENABLE VALIDATE)
/

ALTER TABLE TIPI_MISURA_EXT ADD (
  CONSTRAINT TIPI_MISURA_EXT_C02
  CHECK (SPLIT_FONTE_ENERGIA IN (0,1))
  ENABLE VALIDATE)
/

ALTER TABLE TIPI_MISURA_EXT ADD (
  CONSTRAINT TIPI_MISURA_EXT_C03
  CHECK (TIPO_INTERPOLAZIONE_BUCHI_CAMP IN ('0','1','2','3'))
  ENABLE VALIDATE)
/

ALTER TABLE TIPI_MISURA_EXT ADD (
  CONSTRAINT TIPI_MISURA_EXT_C04
  CHECK (TIPO_INTERPOLAZIONE_BUCHI_CAMP IN ('0','1','2','3'))
  ENABLE VALIDATE)
/

ALTER TABLE TIPI_MISURA_EXT ADD (
  CONSTRAINT TIPI_MISURA_EXT_PK
  PRIMARY KEY
  (COD_TIPO_MISURA_EXT)
  USING INDEX TIPI_MISURA_EXT_PK
  ENABLE VALIDATE)
/
