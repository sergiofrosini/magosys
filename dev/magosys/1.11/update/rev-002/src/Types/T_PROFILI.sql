PROMPT TYPE T_PROFILO / T_PROFILI;

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PROFILI';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PROFILO';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILO non esiste
                            ELSE RAISE;
                         END IF;
    END;

END;
/


CREATE OR REPLACE TYPE T_PROFILO  AS OBJECT
                       (MESE                 NUMBER(2),
                        TIPO                 NUMBER(1),
                        DAYTIME              VARCHAR2(5),
                        VALORE               NUMBER
                      );
/

CREATE OR REPLACE TYPE T_PROFILI AS TABLE OF T_PROFILO;
/
