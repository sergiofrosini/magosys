SPOOL &spool_all append

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 1.11.2 SRC
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

conn sys/sys_dba&tns_arcdb1 as sysdba

COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;
SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;


conn sar_admin/sar_admin_dba&tns_arcdb2

GRANT SELECT  ON CALENDARIO_FESTIVITA TO MAGO;
GRANT EXECUTE ON PKG_CALENDARIO       TO MAGO;

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Synonym
PROMPT
@./Synonyms/SAR_PKG_CALENDARIO.sql

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT
@./Sequences/PROFILI_PKSEQ.sql

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT
@./Types/T_PROFILI.sql


PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

@./Tables/DropTabelleProfili.sql
@./Tables/PROFILI.sql
@./Tables/PROFILI_DEF.sql
@./Tables/PROFILI_VAL_&ORATYPE.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_PROFILI.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_PROFILI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_PROFILI.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.11.2 SRC
PROMPT

spool off
