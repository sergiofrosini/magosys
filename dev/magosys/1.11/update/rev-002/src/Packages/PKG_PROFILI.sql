PROMPT PACKAGE PKG_PROFILI;
--
-- PKG_PROFILI  (Package) 
--
--  Dependencies: 
--   T_PROFILI (Type)
--
CREATE OR REPLACE PACKAGE PKG_PROFILI AS

/* ***********************************************************************************************************
   NAME:       PKG_PROFILI
   PURPOSE:    Gestione dei Profili Misura

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.11.2     21/06/2016  Moretti C.       Definizione Package

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_TabProfili IS TABLE OF V_PROFILI%ROWTYPE;

/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

    gcProfiloFeriale     NUMBER(1) := 1; -- lavorativo
    gcProfiloSemisestivo NUMBER(1) := 2; -- semifestivo o sabato
    gcProfiloFestivo     NUMBER(1) := 3; -- festivo o domenica


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION AddProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER,
                                 pProfili          IN T_PROFILI,
                                 pDataInizio       IN DATE,
                                 pDataFine         IN DATE
                                ) RETURN NUMBER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetProfili           (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                );

 FUNCTION GetProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElencoProfili     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                );

 FUNCTION GetElencoProfili      (pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED;

-- ----------------------------------------------------------------------------------------------------------


END PKG_PROFILI;
/
SHOW ERRORS;


