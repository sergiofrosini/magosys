PROMPT TABLE PROFILI;
--
-- PROFILI  (Table) 
--
--  Dependencies: 
--   TIPI_MISURA (Table)
--
CREATE TABLE PROFILI
(
  COD_PROFILO      NUMBER,
  COD_ELEMENTO     NUMBER,
  COD_TIPO_MISURA  VARCHAR2(6),
  TIPO             NUMBER(1)
)
TABLESPACE &TBS&DAT
/

COMMENT ON TABLE PROFILI IS 'Definizione dei PROFILI Misura'
/

COMMENT ON COLUMN PROFILI.COD_PROFILO IS 'Identificativo del profilo'
/

COMMENT ON COLUMN PROFILI.COD_ELEMENTO IS 'Codice dell''elemento'
/

COMMENT ON COLUMN PROFILI.COD_TIPO_MISURA IS 'Codice Tipo Misura relativo ai valori del profilo'
/

COMMENT ON COLUMN PROFILI.TIPO IS 'tipo giorno (1=Feriale, 2=Prefestivo/Sabato, 3=Festivo/Domenica)'
/



PROMPT INDEX PROFILI_PK;
--
-- PROFILI_PK  (Index) 
--
--  Dependencies: 
--   PROFILI (Table)
--
CREATE UNIQUE INDEX PROFILI_PK ON PROFILI
(COD_PROFILO)
TABLESPACE &TBS&IDX
/


PROMPT INDEX PROFILI_UK1;
--
-- PROFILI_UK1  (Index) 
--
--  Dependencies: 
--   PROFILI (Table)
--
CREATE UNIQUE INDEX PROFILI_UK1 ON PROFILI
(COD_ELEMENTO, COD_TIPO_MISURA, TIPO)
TABLESPACE &TBS&IDX
/


PROMPT TRIGGER BEF_IUR_PROFILI;
--
-- BEF_IUR_PROFILI  (Trigger) 
--
--  Dependencies: 
--   PROFILI (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_PROFILI
BEFORE INSERT OR UPDATE
ON PROFILI
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_PROFILI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.0     08/06/2016   Moretti C.      definizione trigger
   
   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_PROFILO IS NULL THEN
            -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
            EXECUTE IMMEDIATE 'SELECT PROFILI_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.COD_PROFILO ;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_PROFILO <> :NEW.COD_PROFILO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice Profili in tabella ''PROFILI''');
        END IF;
    END IF;

END BEF_IUR_PROFILI;
/


-- 
-- Non Foreign Key Constraints for Table PROFILI 
-- 
PROMPT Non-FOREIGN KEY CONSTRAINTS ON TABLE PROFILI;
ALTER TABLE PROFILI ADD (
  CHECK (TIPO IN (1,2,3))
  ENABLE VALIDATE)
/

ALTER TABLE PROFILI ADD (
  CONSTRAINT PROFILI_PK
  PRIMARY KEY
  (COD_PROFILO)
  USING INDEX PROFILI_PK
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table PROFILI 
-- 
PROMPT FOREIGN KEY CONSTRAINTS ON TABLE PROFILI;
ALTER TABLE PROFILI ADD (
  FOREIGN KEY (COD_TIPO_MISURA) 
  REFERENCES TIPI_MISURA (COD_TIPO_MISURA)
  ENABLE VALIDATE)
/
