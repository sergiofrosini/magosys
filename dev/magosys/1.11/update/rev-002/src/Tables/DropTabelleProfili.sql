PROMPT DROP TABLE PROFILI_VAL;
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE PROFILI_VAL PURGE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/

PROMPT DROP TABLE PROFILI_DEF;
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE PROFILI_DEF PURGE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/

PROMPT DROP TABLE PROFILI;
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE PROFILI PURGE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/

