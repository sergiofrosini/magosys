PROMPT PACKAGE BODY PKG_PROFILI;
--
-- PKG_PROFILI  (Package Body) 
--
--  Dependencies: 
--   PKG_PROFILI (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_PROFILI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.11.2
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION AddProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER,
                                 pProfili          IN T_PROFILI,
                                 pDataInizio       IN DATE,
                                 pDataFine         IN DATE
                                ) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
   Insersce i profili ricevuti
-----------------------------------------------------------------------------------------------------------*/
    vCount     NUMBER := 0;
    vCodElem   ELEMENTI.COD_ELEMENTO%TYPE;
 BEGIN
    vCodElem := PKG_Elementi.GetCodElemento(pCodGestElem);
    IF vCodElem IS NULL THEN
        RETURN 0;
    END IF;

    MERGE INTO PROFILI B
         USING (SELECT vCodElem COD_ELEMENTO, gcProfiloFeriale     TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
                UNION ALL
                SELECT vCodElem COD_ELEMENTO, gcProfiloSemisestivo TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
                UNION ALL
                SELECT vCodElem COD_ELEMENTO, gcProfiloFestivo     TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
               ) A ON (    B.COD_ELEMENTO    = A.COD_ELEMENTO 
                       AND B.TIPO            = A.TIPO
                       AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)        
         WHEN NOT MATCHED THEN
                INSERT (  COD_ELEMENTO,  COD_TIPO_MISURA,  TIPO)
                VALUES (A.COD_ELEMENTO,A.COD_TIPO_MISURA,A.TIPO);

    MERGE INTO PROFILI_DEF B
         USING (SELECT B.COD_PROFILO,ANNO,MESE, 
                       pDataInizio DT_MISURE_INIZIO,pDataFine DT_MISURE_FINE
                  FROM (SELECT DISTINCT pAnnoRif ANNO, MESE 
                          FROM TABLE (CAST(pProfili AS T_PROFILI))
                       ) A
                  LEFT OUTER JOIN PROFILI B ON (    B.COD_ELEMENTO = vCodElem
                                                AND B.COD_TIPO_MISURA = pTipoMisura)
               ) A ON (    B.COD_PROFILO = A.COD_PROFILO 
                       AND B.ANNO        = A.ANNO        
                       AND B.MESE        = A.MESE)        
         WHEN NOT MATCHED THEN
                INSERT (  COD_PROFILO,   ANNO,  MESE,   DT_MISURE_INIZIO,   DT_MISURE_FINE)
                VALUES (A.COD_PROFILO, A.ANNO,A.MESE, A.DT_MISURE_INIZIO, A.DT_MISURE_FINE)
         WHEN MATCHED THEN
                UPDATE SET B.DT_MISURE_INIZIO = A.DT_MISURE_INIZIO,
                           B.DT_MISURE_FINE   = A.DT_MISURE_FINE;

    MERGE INTO PROFILI_VAL B
         USING (SELECT B.COD_PROFILO, pAnnoRif ANNO, A.MESE, REPLACE(A.DAYTIME,':','') DAYTIME, A.VALORE
                  FROM (SELECT MESE, TIPO, DAYTIME, MAX(VALORE) VALORE
                          FROM TABLE (CAST(pProfili AS T_PROFILI))
                         GROUP BY MESE,TIPO,DAYTIME
                       ) A
                  JOIN PROFILI B ON B.COD_ELEMENTO = vCodElem
                                AND B.COD_TIPO_MISURA = pTipoMisura
                                AND B.TIPO = A.TIPO
               ) A
           ON (    B.COD_PROFILO = A.COD_PROFILO
               AND B.ANNO        = A.ANNO
               AND B.MESE        = A.MESE
               AND B.DAYTIME     = A.DAYTIME)
         WHEN NOT MATCHED THEN
                INSERT (  COD_PROFILO,  ANNO,  MESE,  DAYTIME,  VALORE)
                VALUES (A.COD_PROFILO,A.ANNO,A.MESE,A.DAYTIME,A.VALORE)
         WHEN MATCHED THEN
                UPDATE SET B.VALORE = A.VALORE
                 WHERE B.VALORE <> A.VALORE;
    vCount := SQL%ROWCOUNT;
    COMMIT;
    RETURN vCount;
 END AddProfili;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetProfili           (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                ) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il cursore con i profili richiesti.
   La chiusura del cursore e' a cura del chiamente 
-----------------------------------------------------------------------------------------------------------*/
    vNum     INTEGER;
    vAnnoRif INTEGER := NVL(pAnnoRif,TO_NUMBER(TO_CHAR(SYSDATE,'YYYY')));
 BEGIN
    SELECT COUNT(*) 
      INTO vNum
      FROM V_PROFILI 
     WHERE COD_GEST_ELEMENTO = pCodGestElem
       AND COD_TIPO_MISURA = pTipoMisura
       AND ANNO = vAnnoRif;
    IF vNum = 0 THEN
        SELECT MAX(ANNO)
          INTO vAnnoRif
          FROM V_PROFILI
         WHERE COD_GEST_ELEMENTO = pCodGestElem
           AND COD_TIPO_MISURA = pTipoMisura
           AND ANNO < pAnnoRif;
    END IF;
    OPEN pRefCurs FOR SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO,
                             DAYTIME,VALORE,DT_MISURE_INIZIO,DT_MISURE_FINE
                        FROM V_PROFILI 
                       WHERE ANNO = vAnnoRif
                         AND COD_TIPO_MISURA = pTipoMisura
                         AND COD_GEST_ELEMENTO = pCodGestElem
                         AND CASE WHEN pMeseRif 
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pMeseRif = MESE 
                                                        THEN 1
                                                        ELSE 0
                                            END
                             END = 1
                         AND CASE WHEN pTipoGiorno 
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pTipoGiorno = TIPO
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1
                         AND CASE WHEN pDayTime 
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pDayTime = DAYTIME 
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1;
                       --ORDER BY COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO,DAYTIME;
 END GetProfili;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce la Pipeline con i profili richiesti.
-----------------------------------------------------------------------------------------------------------*/
    vCursore    PKG_UtlGlb.t_query_cur;
    vProfilo    V_PROFILI%ROWTYPE;
 BEGIN
    GetProfili(vCursore,pCodGestElem,pTipoMisura,pAnnoRif,pMeseRif,pTipoGiorno,pDayTime);
    LOOP
        FETCH vCursore INTO vProfilo;
        EXIT WHEN vCursore%NOTFOUND;
        PIPE ROW(vProfilo);
    END LOOP;
    CLOSE vCursore;
    RETURN;
 END GetProfili;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElencoProfili     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                ) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il cursore con l'elenco dei profili richiesti.
   La chiusura del cursore e' a cura del chiamente 
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    OPEN pRefCurs FOR SELECT COD_ELEMENTO,E.COD_GEST_ELEMENTO,P.COD_TIPO_MISURA,D.ANNO,D.MESE,P.TIPO,
                             MIN(D.DT_MISURE_INIZIO) DT_MISURE_INIZIO, MAX(D.DT_MISURE_FINE) DT_MISURE_FINE
                        FROM PROFILI P
                        JOIN PROFILI_DEF D USING(COD_PROFILO) 
                        JOIN ELEMENTI E USING(COD_ELEMENTO) 
                       WHERE D.ANNO = pAnnoRif
                         AND E.COD_GEST_ELEMENTO LIKE NVL(pCodGestElem,'%')
                         AND P.COD_TIPO_MISURA LIKE NVL(pTipoMisura,'%')
                         AND CASE WHEN pMeseRif 
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pMeseRif = D.MESE 
                                                        THEN 1
                                                        ELSE 0
                                            END
                             END = 1
                         AND CASE WHEN pTipoGiorno 
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pTipoGiorno = P.TIPO
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1
                       GROUP BY COD_ELEMENTO,E.COD_GEST_ELEMENTO,P.COD_TIPO_MISURA,D.ANNO,D.MESE,P.TIPO;
                       --ORDER BY COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO;
 END GetElencoProfili;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElencoProfili      (pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce la Pipeline con l'elenco dei profili richiesti.
-----------------------------------------------------------------------------------------------------------*/
    vCursore    PKG_UtlGlb.t_query_cur;
    vProfilo    V_PROFILI%ROWTYPE;
 BEGIN
    GetElencoProfili(vCursore,pAnnoRif,pCodGestElem,pTipoMisura,pMeseRif,pTipoGiorno);
    LOOP
        FETCH vCursore INTO vProfilo.COD_ELEMENTO, vProfilo.COD_GEST_ELEMENTO, vProfilo.COD_TIPO_MISURA, 
                            vProfilo.ANNO, vProfilo.MESE, vProfilo.TIPO, 
                            vProfilo.DT_MISURE_INIZIO, vProfilo.DT_MISURE_FINE;
        EXIT WHEN vCursore%NOTFOUND;
        PIPE ROW(vProfilo);
    END LOOP;
    CLOSE vCursore;
    RETURN;
 END GetElencoProfili;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_PROFILI;
/
SHOW ERRORS;


