PROMPT PACKAGE BODY      PKG_SPV_RETE_ELE AS

create or replace PACKAGE BODY      PKG_SPV_RETE_ELE AS


/* ***********************************************************************************************************
   NAME:       PKG_SPV_RETE_ELE
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
   2.3.2       30/05/2018  Frosini S.       Aggiornato SpvDetail per recupero corretto date
   2.3.5       23/10/2018  Frosini S.       MAGO 1831 - inserito clausola NULLS LAST in EleSPvDetail per files storico import senza date

   NOTES:

*********************************************************************************************************** */

    g_tipoMago_Client    CONSTANT elementi.cod_tipo_elemento%TYPE := 'CMT';
    g_tipoMago_Trasf     CONSTANT elementi.cod_tipo_elemento%TYPE := 'TRM';
    g_tipoMago_Genera    CONSTANT elementi.cod_tipo_elemento%TYPE := 'GMT';
    g_tipoMago_Montante  CONSTANT elementi.cod_tipo_elemento%TYPE := 'LMT';
    g_tipoMago_Comune    CONSTANT elementi.cod_tipo_elemento%TYPE := 'COM';
    g_tipoMago_CabinaP   CONSTANT elementi.cod_tipo_elemento%TYPE := 'CPR';
    g_tipoMago_sbarraSec CONSTANT elementi.cod_tipo_elemento%TYPE := 'SCS';


    g_ProdPuro_AUI CONSTANT VARCHAR2(2) := 'PP';
    g_AutoProd_AUI CONSTANT VARCHAR2(2) := 'AP';
    g_ProdAlia_AUI CONSTANT VARCHAR2(2) := 'PD';

    g_CollectorName         CONSTANT spv_collector_info.collector_name%TYPE 	:='RETE_ELETTRICA';
	g_IdSistema             CONSTANT spv_dettaglio_parametri.ID_SISTEMA%TYPE 	:= 5;

    g_isMunic   NUMBER := -1;
    g_DefaultMunic   NUMBER := 0;

	gID_CHIAVE_SPV_ENABLED  CONSTANT VARCHAR2(100 BYTE) := 'consistenza.supervision_enabled';

    g_SogliaLinea CONSTANT NUMBER := 6000; -- 6MW

    gMis_PI     CONSTANT VARCHAR2(10)  := 'PI';
    gMis_PAS    CONSTANT VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT CONSTANT VARCHAR2(10)  := 'PMC';
    gMis_PRI    CONSTANT VARCHAR2(10)  := 'PRI';


    g_sep_CSV_excel CONSTANT VARCHAR2(1) := ';';

    --mago.supervision.detail.anagraficaAui.elenco.title



    -- 01 err 1,'Controllo Codice gestionale esercizio abilitato
    -- 02 err 2,'CG duplicati
    -- 03 err 3,'CG non definiti
    -- 04 err 4,'CG minuscoli in codice gestionale
    -- 05 err 5,'CG non standard
    -- 06 err 6,'Produttori MT e TR MT/BT definiti in AUI e non presenti in STM
    -- 07 err 7,'CG in STM non trovato in AUI
    -- 08 err 8,'Produttori MT trasmessi da STM a MAGO che risultano Â¿dismessiÂ¿ in AUI
    pID_DIZIONARIO_ALLARME_01 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 1;
	pID_DIZIONARIO_ALLARME_02 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 2;
	pID_DIZIONARIO_ALLARME_03 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 3;
	pID_DIZIONARIO_ALLARME_04 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 4;
	pID_DIZIONARIO_ALLARME_05 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 5;
	pID_DIZIONARIO_ALLARME_06 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 6;
	pID_DIZIONARIO_ALLARME_07 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 7;
	pID_DIZIONARIO_ALLARME_08 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 8;

    vPARAMETRI_DESCRIZIONE VARCHAR2(1000);

/*============================================================================*/

PROCEDURE printa(pSTR IN VARCHAR2) IS

BEGIN

   --	   DBMS_OUTPUT.PUT_LINE(psTR);

  PKG_Logs.TraceLog(psTR, PKG_UtlGlb.gcTrace_VRB);
END;

/*============================================================================*/

PROCEDURE sp_AddUnCollector(pErrore       IN INTEGER,
                            pGestElem     IN VARCHAR2,
                            pTipoElem     IN VARCHAR2 DEFAULT NULL,
                            pData         IN DATE DEFAULT SYSDATE) AS

BEGIN


   pkg_supervisione.AddSpvCollectorInfo(g_CollectorName,
                                        pData,
                                        g_IdSistema,
                                        pErrore,
                                        NULL,
                                        pGestElem,
                                        NULL,
                                        NULL,
                                        1,
                                        pTipoElem);


END sp_AddUnCollector;

/*============================================================================*/

PROCEDURE sp_CheckCO(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                     pData           IN DATE DEFAULT SYSDATE) AS

 vDummy VARCHAR2(100);

BEGIN

    SELECT COD_GEST_ELEMENTO
      INTO vDummy
      FROM DEFAULT_CO
     INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
     LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
     WHERE flag_primario = 1
       AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
 --  dbms_output.put_line(' sp_AddUnCollector 1 ');
             sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, NULL, NULL);

    WHEN OTHERS THEN RAISE;

END sp_CheckCO;

/*============================================================================*/

PROCEDURE sp_CheckCGEle(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


   vLst  PKG_UtlGlb.t_query_cur;


   vBlok    INTEGER;
   vTipErr  VARCHAR2(50);
   vCodSG   VARCHAR2(20);
   vCodST   VARCHAR2(20);
   vTipEle  VARCHAR2(10);
   vNomEle  VARCHAR2(90);
   vTipo    INTEGER;


    FUNCTION f_DaTipoAMsg(ptipo IN NUMBER) RETURN NUMBER IS
    BEGIN

        CASE pTipo
        WHEN    1 THEN
 RETURN pID_DIZIONARIO_ALLARME_02;
        WHEN    2 THEN
 RETURN pID_DIZIONARIO_ALLARME_03;
        WHEN    3 THEN
 RETURN pID_DIZIONARIO_ALLARME_04;
        WHEN    4 THEN
 RETURN pID_DIZIONARIO_ALLARME_05;
        ELSE RETURN -1; END CASE;
    END;

BEGIN

    pkg_anagrafiche.ElementsCheckReport(vLst,1,pData);

    LOOP
        FETCH vLst INTO   vBlok,vTipErr,vCodSG ,vCodST,vTipEle,vNomEle,vTipo;
        EXIT WHEN vLst%NOTFOUND;

 --      dbms_output.put_line(' sp_AddUnCollector sp_CheckCGEle'||f_DaTipoAMsg(vtipo)||' '||vTipEle||' - '||vCodSG);
    sp_AddUnCollector(f_DaTipoAMsg(vtipo), vCodSG, vTipEle);

   END LOOP;

END sp_CheckCGEle;

/*============================================================================*/

PROCEDURE sp_CheckProdAUI_NoSTM(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


    vSql VARCHAR2(3000) :=
            'WITH '||
            '    AUI_TRASF AS '||
            '    (SELECT '||
            '            t.COD_ORG_NODO, '||
            '            t.SER_NODO, '||
            '            t.NUM_NODO, '||
            '            t.ID_TRASF, '||
            '            t.TIPO_ELEMENTO '||
            '        FROM '||
            '            AUI_TRASFORMATORI_TLC t '||
            '        JOIN AUI_TRASF_PROD_BT_TLC p '||
            '        ON '||
            '            ( '||
            '                t.COD_ORG_NODO '||
            '                ||t.SER_NODO '||
            '                ||t.NUM_NODO '||
            '                ||t.TIPO_ELEMENTO '||
            '                ||t.ID_TRASF= p.COD_ORG_NODO '||
            '                ||p.SER_NODO '||
            '                ||p.NUM_NODO '||
            '                ||p.tipo_ele '||
            '                ||p.ID_TRASF '||
            '            ) '||
            '        WHERE '||
            '            ( '||
            PKG_SPV_UTL.f_ClientAUISi('t') ||
            '            ) '||
            '            AND '||
            '            ( '||
            PKG_SPV_UTL.f_ClientAUISi('p') ||
            '            ) '||
            '    ) '||
            'SELECT '||
            '     CASE  T.TIPO_ELEMENTO '||
            'WHEN PKG_SPV_UTL.f_TipoTrasfAUI THEN t.COD_ORG_NODO||t.SER_NODO||t.NUM_NODO||T.TIPO_ELEMENTO||t.ID_TRASF '||
            'ELSE  C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE END COD_GEST, '||
            '     CASE  T.TIPO_ELEMENTO '||
            'WHEN PKG_SPV_UTL.f_TipoTrasfAUI THEN PKG_SPV_UTL.f_TipoTrasfST '||
            'ELSE  PKG_SPV_UTL.f_TipoProdST END COD_TIPO_ELEMENTO '||
            'FROM '||
			 '  (SELECT * FROM AUI_CLIENTI_TLC WHERE '|| PKG_SPV_UTL.f_ClientAUISi||' '||
				'  AND SER_NODO=2) C '||
                'LEFT OUTER JOIN AUI_TRASF t '||
            'ON '||
            '    ( '||
            '        t.COD_ORG_NODO '||
            '        ||t.SER_NODO '||
            '        ||t.NUM_NODO '||
            '        ||t.ID_TRASF= C.COD_ORG_NODO '||
            '        ||C.SER_NODO '||
            '        ||C.NUM_NODO '||
            '        ||C.ID_CLIENTE '||
            '    ) '||
            'MINUS '||
            'SELECT '||
            '    COD_GEST, COD_TIPO_ELEMENTO '||
            'FROM '||
            '    ( '||
            '        SELECT '||
            '            COD_GEST, '||
            '            PKG_SPV_UTL.f_TipoProdST COD_TIPO_ELEMENTO '||
            '        FROM '||
            '            CORELE_CLIENTIMT_## '||
            '        WHERE '||
            '            :myData BETWEEN DATA_INIZIO AND DATA_FINE '||
            '        UNION ALL '||
            '        SELECT '||
            '            COD_GEST, '||
            '            PKG_SPV_UTL.f_TipoTrasfST COD_TIPO_ELEMENTO '||
            '        FROM '||
            '            CORELE_TRASFORMATORIBT_## '||
            '        WHERE '||
            '            :myData BETWEEN DATA_INIZIO AND DATA_FINE '||
            '            AND INSTR(UPPER(COD_GEST),''FIT'') = 0 '||
            '            AND INSTR(UPPER(NOME),''FIT'')     = 0 '||
            '    ) '||
            'order by COD_TIPO_ELEMENTO, COD_GEST';



    emp_refcur      SYS_REFCURSOR;

    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;


BEGIN

  OPEN emp_refcur FOR PKG_SPV_UTL.f_AppiccicaStato(vSql,1) USING pData,pData;
  LOOP
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;

-- dbms_output.put_line(' sp_AddUnCollector 6 '||vTipo||' - '||vGest);
    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_06, vGest,vTipo);


  END LOOP;
  CLOSE emp_refcur;

END sp_CheckProdAUI_NoSTM;

/*============================================================================*/

PROCEDURE sp_CheckCG_STM_NoAUI(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

/*

select base per costruire la sqlinAUI

set pagesize 10000


SELECT
case WHEN table_name <> table_prev THEN 'SELECT ' else ' ' END,
column_name||(case WHEN table_name = table_succ THEN '||' ELSE ' COD_GEST FROM ' END),
case WHEN table_name <> table_succ THEN table_name||'@PKG1_STMAUI.IT where ###### union all ' else ' ' END
from (
select column_name, table_name,
LAG(table_name, 1, 0) OVER (ORDER BY table_name,column_id) AS table_prev,
LEAD(table_name, 1, 0) OVER (ORDER BY table_name,column_id) AS table_succ
FROM user_tab_cols
WHERE table_name like'%TLC'
and not table_name like 'VÂ¿_%' ESCAPE 'Â¿'
and column_id between 2 and 6
order by table_name, column_id)


*/

    vSql_inSTM VARCHAR(3000) :=
    'INSERT INTO GTTD_VALORI_TEMP '||
    '(TIP, ALF1, ALF2) ' ||
    'SELECT :tip, e_STM.COD_GEST, t.COD_TIPO_ELEMENTO '                                                    ||
                      'FROM (SELECT COD_ENTE,COD_GEST FROM CORELE_AVVOLGIMENTI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '     ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_IMPIANTIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_CLIENTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_CONGIUNTORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_MONTANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_PARALLELI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_RIFASATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_SBARRE_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '            ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_SEZIONATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASFORMATORIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASFORMATORIBT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                                         'AND INSTR(UPPER(COD_GEST),''FIT'') = 0 AND INSTR(UPPER(NOME),''FIT'') = 0 '                                         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASLATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT DISTINCT COD_ENTE,COD_GEST FROM CORELE_IMPIANTIMT_SA WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT ''SBCS'' COD_ENTE,COD_GEST_SBARRA COD_GEST FROM CORELE_IMPIANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '||
                            ') e_STM '||
			    ' JOIN TIPI_ELEMENTO T ON (t.CODIFICA_ST = e_STM.COD_ENTE AND e_STM.COD_ENTE <> ''CSMT'' ) '; -- escludo c.sec non presenti in elementi mago

    vSql_inAUI VARCHAR(5000) :=
    'INSERT INTO GTTD_VALORI_TEMP '||
    '(TIP, ALF1) ' ||
    'SELECT :tip, COD_GEST FROM ( '||
'SELECT COD_ORG_NODO||' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_CLIENTE COD_GEST FROM AUI_APPO_CLIENTI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_TRASF COD_GEST FROM AUI_AVVOLGIMENTI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_TRASF COD_GEST FROM AUI_CLIENTI_BT_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_CLIENTE COD_GEST FROM AUI_CLIENTI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_COLL_ELE_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_COLL_RAMO_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_COLL_TRASF_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_COND COD_GEST FROM AUI_CONDENSATORI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_CONN_TLC where '||PKG_SPV_UTL.f_ClientAUISi||' UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_TRASF COD_GEST FROM AUI_COPPIA_AVVO_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_GENERATORE COD_GEST FROM AUI_GENERATORI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_MONTANTI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO ' ||
--' DATA_AUI|| ' ||
--' DATA_TLC
' COD_GEST FROM AUI_NODI_TLC where num_nodo<>2 AND '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' || -- escludo c.sec non presenti in elementi mago
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_MONTANTE COD_GEST FROM AUI_PROTEZIONI_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_RTU|| ' ||
' DATA_AUI COD_GEST FROM AUI_RTU_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' ID_SIST_SBAR|| ' ||
' ID_SEZI_SBAR COD_GEST FROM AUI_SBARRE_SEZ_TLC where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELEMENTO|| ' ||
' ID_TRASF COD_GEST FROM AUI_TRASFORMATORI_TLC  where '||PKG_SPV_UTL.f_ClientAUISi||'  UNION ALL ' ||
'SELECT COD_ORG_NODO|| ' ||
' SER_NODO|| ' ||
' NUM_NODO|| ' ||
' TIPO_ELE|| ' ||        
' ID_TRASF COD_GEST FROM AUI_TRASF_PROD_BT_TLC where '||PKG_SPV_UTL.f_ClientAUISi||' '||
' ) ';


BEGIN


  EXECUTE IMMEDIATE PKG_SPV_UTL.f_AppiccicaStato(vSql_inSTM,1)
  USING 'CGdiSTM',
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData;


  EXECUTE IMMEDIATE vSql_inAUI
  USING 'CGdiAUI';


  FOR riga IN (
        SELECT  e_stm.ALF1,  e_stm.ALF2 FROM
        (SELECT ALF1, ALF2 FROM GTTD_VALORI_TEMP WHERE TIP = 'CGdiSTM') e_stm
        LEFT JOIN
        (SELECT ALF1 FROM GTTD_VALORI_TEMP WHERE TIP = 'CGdiAUI') e_aui
        ON (e_stm.ALF1 = e_aui.ALF1)
        WHERE e_aui.ALF1 IS NULL
        ORDER BY ALF2,ALF1
  ) LOOP


--  dbms_output.put_line(' sp_AddUnCollector 7 '||riga.ALF2||' - '||riga.ALF1);
   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_07,riga.ALF1, riga.ALF2);


  END LOOP;


END sp_CheckCG_STM_NoAUI;


/*============================================================================*/

PROCEDURE sp_CheckCliDismessi_AUI(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


vSql VARCHAR2(2000) :=
 'SELECT c_stm.COD_GEST,t.COD_TIPO_ELEMENTO FROM '||
 ' (SELECT * FROM CORELE_CLIENTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE )c_stm ' ||
 'JOIN (SELECT * FROM AUI_CLIENTI_TLC WHERE SER_NODO=2  AND stato = '''||PKG_SPV_UTL.f_DismessoAUI||''' ) C_aui '||
' ON (C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE = c_stm.COD_GEST  ) '||
' JOIN TIPI_ELEMENTO T ON (t.CODIFICA_ST = c_STM.COD_ENTE)';

    emp_refcur      SYS_REFCURSOR;

    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;


BEGIN
--dbms_output.put_line(PKG_SPV_UTL.f_AppiccicaStato(vSql,1));
  OPEN emp_refcur FOR PKG_SPV_UTL.f_AppiccicaStato(vSql,1) USING pData;
  LOOP
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;

-- dbms_output.put_line(' sp_AddUnCollector 8 AUI '||vTipo||' - '||vGest);
    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, vGest,vTipo);


  END LOOP;
  CLOSE emp_refcur;

END sp_CheckCliDismessi_AUI;

/*============================================================================*/

PROCEDURE sp_CheckCliDismessi_stm(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


vSql VARCHAR2(2000) :=
'SELECT DISTINCT * FROM  ('||
' SELECT C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE, ''CMT'' '||
 'JOIN (SELECT * FROM AUI_CLIENTI_TLC WHERE SER_NODO=2 AND '|| PKG_SPV_UTL.f_ClientAUISi ||' ) C_aui '||
' LEFT JOIN  (SELECT * FROM CORELE_CLIENTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE ) c_stm ' ||
' ON (C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE = c_stm.COD_GEST ) '||
' WHERE c_stm.COD_GEST is null ) ';
    emp_refcur      SYS_REFCURSOR;

    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;


BEGIN


  OPEN emp_refcur FOR PKG_SPV_UTL.f_AppiccicaStato(vSql,1) USING pData;
  LOOP
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;

--   dbms_output.put_line(' sp_AddUnCollector 8 stm '||vTipo||' - '||vGest);
  sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, vGest,vTipo);


  END LOOP;
  CLOSE emp_refcur;

END sp_CheckCliDismessi_stm;

/*============================================================================*/

/*============================================================================*/

PROCEDURE sp_CheckAll(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vOra TIMESTAMP;

    vAllarmeCO NUMBER := 0;

BEGIN

    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCO ',40,' '));
     sp_CheckCO (pBlockOnly, pData );
    printa(RPAD('out sp_CheckCO ',40,' ')||to_char(current_timestamp-vOra));


    WITH allarmi AS (
		  SELECT d.id_dizionario_allarme
		  , nvl(sum(i.valore), 0) n_allarmi
          , i.informazioni
		  FROM spv_rel_sistema_dizionario d
		  LEFT JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
		  WHERE d.id_sistema = g_IdSistema
		  AND trunc(i.data_inserimento) = trunc(sysdate)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  ORDER BY d.id_dizionario_allarme, i.informazioni
		)
		SELECT
		 sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01, A.n_allarmi, 0))
		INTO vAllarmeCO
		FROM allarmi A
		;

    IF NVL(vAllarmeCO,0) = 0 THEN

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckCGEle ',40,' '));
         sp_CheckCGEle (pBlockOnly, pData );
        printa(RPAD('out sp_CheckCGEle ',40,' ')||to_char(current_timestamp-vOra));

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckProdAUI_NoSTM ',40,' '));
         sp_CheckProdAUI_NoSTM (pBlockOnly, pData );
        printa(RPAD('out sp_CheckProdAUI_NoSTM ',40,' ')||to_char(current_timestamp-vOra));

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckCG_STM_NoAUI ',40,' '));
         sp_CheckCG_STM_NoAUI (pBlockOnly, pData );
        printa(RPAD('out sp_Chsp_CheckCG_STM_NoAUIeckProdAUI_NoSTM ',40,' ')||to_char(current_timestamp-vOra));

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckCliDismessi_AUI ',40,' '));
         sp_CheckCliDismessi_AUI (pBlockOnly, pData );
        printa(RPAD('out sp_CheckCliDismessi_AUI ',40,' ')||to_char(current_timestamp-vOra));

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckCliDismessi_stm ',40,' '));
         sp_CheckCliDismessi_stm (pBlockOnly, pData );
        printa(RPAD('out sp_CheckCliDismessi_stm ',40,' ')||to_char(current_timestamp-vOra));

    END IF;

END sp_CheckAll;

/*============================================================================*/

PROCEDURE EleSpvCollector
	IS
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvCollector',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvCollector - Start';
		printa(vStrLog);

		sp_CheckAll;

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvCollector - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
		EXCEPTION
		WHEN OTHERS THEN
		   printa ('PKG_SPV_RETE_ELE.EleSpvCollector error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));

END EleSpvCollector;

/*============================================================================*/

PROCEDURE EleSpvProcessor
	IS
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento VARCHAR2(100);
		p_cod_tipo_elemento VARCHAR2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%TYPE;
		pID_DIZIONARIO_ALLARME NUMBER;
		pDATA_ALLARME DATE := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := NULL;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := NULL;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := NULL;


		n_allarmi_1 	NUMBER;
		n_allarmi_2 	NUMBER;
		n_allarmi_3 	NUMBER;
		n_allarmi_4 	NUMBER;
		n_allarmi_5 	NUMBER;
		n_allarmi_6     NUMBER;
		n_allarmi_7 	NUMBER;
		n_allarmi_8 	NUMBER;

		soglia_allarme_1 		NUMBER;
		soglia_allarme_2 		NUMBER;
		soglia_allarme_3 		NUMBER;
		soglia_allarme_4 		NUMBER;
		soglia_allarme_5 		NUMBER;
		soglia_allarme_6    	NUMBER;
		soglia_allarme_7 		NUMBER;
		soglia_allarme_8 		NUMBER;

		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvProcessor',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - Start';
		printa(vStrLog);

		P_VALORE :=0; --supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_gen');
		soglia_allarme_1 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE :=0; -- pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_nd');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '999');

		P_VALORE :=0; -- pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_lt_0');
		soglia_allarme_3 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE :=0; -- pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_pot_gruppi');
		soglia_allarme_4 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE :=0; -- pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_fonte');
		soglia_allarme_5 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'consistenza.n_cmt_in_aui_non_in_stm');
		soglia_allarme_6 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'consistenza.n_cod_gest_in_stm_non_in_aui');
		soglia_allarme_7 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'consistenza.n_cod_gest_in_stm_dismessi_in_aui');
		soglia_allarme_8 := TO_NUMBER(P_VALORE, '9999');

		--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'smileGme.n_hour');

		--allarme 3:
		--			GENERATORI MT vs TR MT/bt
		--			GMT / TRM
		--allarme 6:
		--			PRODUTTORI MT vs TR MT/bt
		--			CMT / TRM


		WITH allarmi AS (
		  SELECT d.id_dizionario_allarme
		  , nvl(sum(i.valore), 0) n_allarmi
          , i.informazioni
		  FROM spv_rel_sistema_dizionario d
		  LEFT JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
		  WHERE d.id_sistema = g_IdSistema
		  AND trunc(i.data_inserimento) = trunc(sysdate)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  ORDER BY d.id_dizionario_allarme, i.informazioni
		)
		SELECT
		 sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01  , A.n_allarmi, 0)) n_allarmi_1
		,sum(
			CASE WHEN (A.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_02
                        AND A.informazioni IN (g_tipoMago_Trasf,g_tipoMago_Client,g_tipoMago_Genera)) THEN A.n_allarmi
				 ELSE 0
			END
		) n_allarmi_2
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_03  , A.n_allarmi, 0)) n_allarmi_3
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_04  , A.n_allarmi, 0)) n_allarmi_4
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_05  , A.n_allarmi, 0)) n_allarmi_5
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_06  , A.n_allarmi, 0)) n_allarmi_6
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_07  , A.n_allarmi, 0)) n_allarmi_7
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_08  , A.n_allarmi, 0)) n_allarmi_8
		INTO n_allarmi_1,n_allarmi_2,n_allarmi_3,n_allarmi_4,n_allarmi_5,n_allarmi_6,n_allarmi_7,n_allarmi_8
		FROM allarmi A
		;

		OPEN pRefCurs FOR
			SELECT d.id_dizionario_allarme, i.COD_GEST_ELEMENTO,
            CASE WHEN i.id_rel_sistema_dizionario IN (1,2,3,4,5,6,7,8) THEN  i.informazioni ELSE E.cod_tipo_elemento END , i.informazioni
			FROM spv_rel_sistema_dizionario d
			JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
			LEFT JOIN elementi E ON E.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
			WHERE d.id_sistema = g_IdSistema
			AND trunc(i.data_inserimento) = trunc(sysdate)
			ORDER BY d.id_dizionario_allarme
			;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);


			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 2;

				-- Gli allarmi 4-5-6-8 hanno una soglia di tipo GIALLO sotto la quale non vengono segnalati
			IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_04 AND n_allarmi_4 >= soglia_allarme_4)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_05 AND n_allarmi_5 >= soglia_allarme_5)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06 AND n_allarmi_6 >= soglia_allarme_6)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_08 AND n_allarmi_8 >= soglia_allarme_8)
					)THEN
				vStrLog := 'PKG_SPV_RETE_ELE.auiSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'pPARAMETRI_DESCRIZIONE: ' || pPARAMETRI_DESCRIZIONE ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);

			ELSE

            -- gli allarmi vengono gestiti cosi:
            -- da 0 a soglia --> allarme Giallo
            -- da soglia in poi --> allarme Rosso
            IF     (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_01  AND n_allarmi_1  >= soglia_allarme_1 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_02  AND n_allarmi_2  >= soglia_allarme_2 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  AND n_allarmi_3  >= soglia_allarme_3 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_07  AND n_allarmi_7  >= soglia_allarme_7 )
            THEN
                pID_LIVELLO := 3;
            END IF;

            vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - Calling AddSpvAllarmi ['||
            'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
            'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
            'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
            'pID_LIVELLO: ' || pID_LIVELLO
            ||']';
    --		printa(vStrLog);

            pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
            END IF;
		END LOOP;
		CLOSE PREFCURS;

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_RETE_ELE.EleSpvProcessor error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END EleSpvProcessor;

/*============================================================================*/

FUNCTION f_SetDettaglioData(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN DATE DEFAULT NULL) RETURN DATE IS

l_Quanti DATE;
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               l_Quanti,
                                               NULL);

    RETURN v_quanti;

END f_SetDettaglioData;

/*============================================================================*/

FUNCTION f_SetDettaglioNum(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN NUMBER DEFAULT NULL) RETURN NUMBER IS

l_Quanti  VARCHAR2(200);
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := REPLACE(to_char(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

    RETURN l_quanti;

END f_SetDettaglioNum;

/*============================================================================*/

FUNCTION f_SetDettaglioStr(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 IS

l_Quanti VARCHAR2(200);
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := REPLACE(to_char(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

    RETURN l_quanti;

END f_SetDettaglioStr;

/*============================================================================*/
PROCEDURE EleSpvDetail(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    v_tutti NUMBER := 0;
    v_CCP NUMBER := 0;
    v_CCQ NUMBER := 0;
    v_quando DATE := sysdate;
    v_retNum NUMBER := 0;
    v_retDat DATE;
    v_retStr VARCHAR2(200);

    v_Esito NUMBER;
    v_Data TIMESTAMP(4);

    v_File VARCHAR2(2000);

    v_Quanti NUMBER;

    vElem          ELEMENTI.COD_ELEMENTO%TYPE;
    vGest          ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

    vLog  PKG_Logs.t_StandardLog;
    vStrLog VARCHAR2(2000);

    v_label VARCHAR2(200);

    v_sql_dummy VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL';
    v_sql_nihil VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL WHERE ROWNUM<1';

/*----------------------------------------------------------------------------*/
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvDetail',
                               pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_RETE_ELE.EleSpvDetail - Start';
    printa(vStrLog);


    PKG_ELEMENTI.GetDefaultCO(vElem, vGest, vNome);

    FOR iRiga IN (SELECT PKG_MAGO.gcStatoNormale statoRete FROM DUAL
                    UNION
                    SELECT PKG_MAGO.gcStatoAttuale statoRete FROM DUAL )
    LOOP


        BEGIN
select data,stato
            INTO v_Data, v_Esito
            from (
  select data,stato ,  DENSE_RANK () OVER (ORDER BY data DESC NULLS LAST) primo from (
    SELECT j.actual_start_date DATA,
                   CASE j.status WHEN 'SUCCEEDED' THEN 1 ELSE 3 END stato
            FROM (
            SELECT data_elab_fin FROM
                     (SELECT DATA_ELAB_FIN,
                         RANK () OVER (PARTITION BY tipo ORDER BY data_elab_ini DESC NULLS LAST)
                              AS rango
                     FROM STORICO_IMPORT
                      WHERE (iRiga.StatoRete = PKG_MAGO.gcStatoNormale AND tipo IN ('SN','AUI'))
                       OR  (iRiga.StatoRete = PKG_MAGO.gcStatoAttuale AND tipo IN ('SA','NEA'))
                  )
             WHERE rango = 1)s
            JOIN (SELECT *
                   FROM ALL_SCHEDULER_JOB_RUN_DETAILS
                    WHERE owner = 'MAGO' AND JOB_NAME = 'ALLINEA_ANAGRAFICA') j
            ON (abs (extract( DAY FROM (j.log_date - S.DATA_ELAB_fin) ))*24*60 +
               abs(extract( HOUR FROM (j.log_date - S.DATA_ELAB_fin) ))*60 +
               abs(extract( MINUTE FROM (j.log_date - S.DATA_ELAB_fin) )) <=5)))
			   -- differneza di 5 minuti  fra l'ultijmo ALLINEA e l'ultimo STORICO IMPORT,
			   -- non c'Ã¨ altro modo per  associare il job esegiuto alfile elaborato
      where primo =1;

/*
            SELECT j.actual_start_date DATA,
                   CASE j.status WHEN 'SUCCEEDED' THEN 1 ELSE 3 END stato
            INTO v_Data, v_Esito
            FROM (SELECT data_elab_fin FROM
                     (SELECT DATA_ELAB_FIN,
                         RANK () OVER (PARTITION BY tipo ORDER BY data_elab_ini DESC)
                              AS rango
                     FROM STORICO_IMPORT
                     WHERE (iRiga.StatoRete = PKG_MAGO.gcStatoNormale AND tipo IN ('SN','AUI'))
                       OR  (iRiga.StatoRete = PKG_MAGO.gcStatoAttuale AND tipo IN ('SA','NEA'))
                  )
             WHERE rango = 1)s
            JOIN (SELECT *
                   FROM ALL_SCHEDULER_JOB_RUN_DETAILS
                    WHERE owner = 'MAGO' AND JOB_NAME = 'ALLINEA_ANAGRAFICA') j
            ON (abs (extract( DAY FROM (j.log_date - S.DATA_ELAB_fin) ))*24*60 +
               abs(extract( HOUR FROM (j.log_date - S.DATA_ELAB_fin) ))*60 +
               abs(extract( MINUTE FROM (j.log_date - S.DATA_ELAB_fin) )) <=1);
*/

        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                v_Esito := 0; -- N.D.
                v_Data := current_timestamp; --NULL; --
        END;

        IF v_Esito = 1 THEN

            SELECT count(*)
            INTO v_Quanti
            FROM spv_allarmi
            WHERE
            id_rel_sistema_dizionario IN
            (
                SELECT
                    srsd.id_rel_sistema_dizionario
                FROM
                    spv_rel_sistema_dizionario srsd
                WHERE
                    srsd.id_sistema = g_IdSistema
            );
        --    AND trunc(data_Allarme) = trunc(pData);

            IF v_Quanti > 0 THEN
                v_Esito := 2; -- succ. con errori
            END IF;


        END IF;


        v_retNum := f_SetDettaglioNum('consistenza.'||
                                        (CASE iRiga.StatoRete
                                            WHEN PKG_MAGO.gcStatoNormale THEN 'statoNormale'
                                            ELSE 'statoAttuale'
                                         END)||
                                        '.esitoUltimaModifica', NULL, v_Esito);


        v_label := 'consistenza.'||
                            (CASE iRiga.StatoRete
                                WHEN PKG_MAGO.gcStatoNormale THEN 'statoNormale'
                                ELSE 'statoAttuale'
                            END)||
                            '.dataUltimaModifica';
        IF v_Data IS NULL THEN
            v_retStr := f_SetDettaglioStr(v_label, NULL, '--');
        ELSE
            v_retNum := f_SetDettaglioNum(v_label,
                            NULL,
                            PKG_SPV_UTL.f_timestamp_to_unix(v_Data));
        END IF;


    END LOOP;

    vStrLog := 'PKG_SPV_RETE_ELE.EleSpvDetail - End';
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    printa(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

EXCEPTION
WHEN OTHERS THEN
    printa('PKG_SPV_RETE_ELE.EleSpvDetail error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END EleSpvDetail;
/*============================================================================*/

PROCEDURE EleSpvMain(pDoDeleteOldSpvInfo IN NUMBER DEFAULT 0
							,pDoDeleteSpvAlarm IN NUMBER DEFAULT 1
							,pDoDeleteTodaySpvInfo IN NUMBER DEFAULT 1
							,pDoCollector IN NUMBER DEFAULT 1
							,pDoProcessor IN NUMBER DEFAULT 1
							,pDoDetail IN NUMBER DEFAULT 1
							)
	IS
    vIsSupervisionEnabled VARCHAR2(100);
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(1000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvMain',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Start';
		printa(vStrLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'consistenza.supervision_enabled');
    	vStrLog :=  'PKG_SPV_RETE_ELE --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
			'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
			'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;
     printa(vStrLog);


           IF vIsSupervisionEnabled = 'true' OR vIsSupervisionEnabled = 'TRUE' THEN
                -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
              IF pDoDeleteTodaySpvInfo = 1 THEN
                  vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - delete Today spv_collector_info ';
                   printa(vStrLog);
                DELETE FROM spv_collector_info
                WHERE collector_name = g_CollectorName
                AND trunc(data_inserimento) = trunc(sysdate)
                          ;
              END IF;

              -- cancello TUTTE le info generate dal collector ANAGRAFICA_AUI prima di gc-48h
              IF pDoDeleteOldSpvInfo = 1 THEN
                  vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - delete Old spv_collector_info ';
                  printa(vStrLog);
                DELETE FROM spv_collector_info
                WHERE collector_name = g_CollectorName
                AND data_inserimento < trunc(sysdate)
                AND id_rel_sistema_dizionario IN (
                  SELECT id_rel_sistema_dizionario
                  FROM spv_rel_sistema_dizionario
                  WHERE id_sistema = g_IdSistema
                );
              END IF;

              -- eseguo il collector e creo le spv_collector_info
              IF pDoCollector = 1 THEN
                vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvCollector ';
                printa(vStrLog);
                EleSpvCollector;
              END IF;

              -- cancello TUTTI gli allarmi generati dal collector ANAGRAFICA_AUI
              IF pDoDeleteSpvAlarm = 1 THEN
                vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain -  delete from spv_allarmi ';
                 printa(vStrLog);
                DELETE FROM spv_allarmi
                WHERE id_rel_sistema_dizionario
                       IN (SELECT id_rel_sistema_dizionario FROM spv_rel_sistema_dizionario WHERE id_sistema = g_IdSistema);
              END IF;

              -- eseguo il processor e creo le spv_allarmi
              IF pDoProcessor = 1 THEN
                vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvProcessor ';
                printa(vStrLog);

                EleSpvProcessor;
              END IF;

              -- valorizzo il pannello di dettaglio
              IF pDoDetail = 1 THEN
               vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvDetail ';
                printa(vStrLog);

                EleSpvDetail;
              END IF;
            ELSE
              vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Supervision AUI is NOT enabled';
              printa(vStrLog);
            END IF;

    PKG_SPV_UTL.sp_SystemBroom;


	vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - End';
	printa(vStrLog);

	PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_RETE_ELE.EleSpvMain error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END EleSpvMain;

/*============================================================================*/

END PKG_SPV_RETE_ELE;
/