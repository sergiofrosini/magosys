PROMPT PROCEDURE      jira2374 as
create or replace PROCEDURE      jira2374 as

  gMisura           PKG_GestAnagr.t_DefAnagr;
  
  vtipele           elementi.cod_tipo_elemento%type;
  vcodele           elementi.cod_elemento%type;
  
  vtipret           tipi_rete.cod_tipo_rete%type;
  vtipfon           tipo_fonti.cod_tipo_fonte%type;
  
  vtipcli           tipi_cliente.cod_tipo_cliente%type;
  vtipmis           tipi_misura.cod_tipo_misura%type:='PCT-P';
  vTrtEle           trattamento_elementi.cod_trattamento_elem%type;
  
  vValMis           NUMBER;
  
  
  vGiorno           date;
  gcModalitaKPI     CONSTANT NUMBER(1) := 3;        -- Modalita' KPI (usato per TIPO_AGGREGAZIONE in TRATTAMENTO_ELEMENTI
  
  gPrevData         DATE := PKG_UtlGlb.gkDataTappo;
  pResult           number;
-- ----------------------------------------------------------------------------------------------------------

  procedure sp_print2374(ptesto varchar2) Is
    begin

      insert into log2374 
        select current_timestamp,ptesto from dual;
  end;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetTrattamentoElemento(pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pTipEle        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                pTipFon        IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                pTipCli        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                pOrganizzaz    IN NUMBER,
                                pTipoAggreg    IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce il trattamento elemento - Se non presente lo crea
-----------------------------------------------------------------------------------------------------------*/

    vTrtEle         TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;
    vTipEle         TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipEle;
    vGerEcs         TIPI_ELEMENTO.GER_ECS%TYPE;
--    vTipoAggreg     NUMBER := pTipoAggreg;
    vOrganizzazione NUMBER(1);

         PROCEDURE SetRel_Ele_TipMis (pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                      pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                      pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                      vTipTabMisure  IN INTEGER) AS
        /*---------------------------------------------------------------------------------------------------
            Inserisce/Modifica la relazione Elemento / Tipo Misura indicando se le misure saranno reperibili
            in tabella MISURE_ACQUISITE, i tablella MISURE_AGGREGATE o in entrambe e il tipo rete interessato
        ---------------------------------------------------------------------------------------------------*/
            vTabMisure    REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;
            vReteAT       REL_ELEMENTO_TIPMIS.RETE_AT%TYPE := 0;
            vReteMT       REL_ELEMENTO_TIPMIS.RETE_MT%TYPE := 0;
            vReteBT       REL_ELEMENTO_TIPMIS.RETE_BT%TYPE := 0;
            vres NUMBER;
         BEGIN
            CASE pTipRet
                WHEN PKG_Mago.gcTipReteAT THEN vReteAT := 1;
                WHEN PKG_Mago.gcTipReteMT THEN vReteMT := 1;
                WHEN PKG_Mago.gcTipReteBT THEN vReteBT := 1;
            END CASE;
            BEGIN
                SELECT TAB_MISURE
                  INTO vTabMisure
                  FROM REL_ELEMENTO_TIPMIS
                 WHERE COD_ELEMENTO = pCodEle
                   AND COD_TIPO_MISURA = pTipMis;
                IF NVL(BITAND(vTabMisure,vTipTabMisure),-1) <> vTipTabMisure THEN
                    UPDATE REL_ELEMENTO_TIPMIS SET TAB_MISURE = TAB_MISURE + vTipTabMisure
                     WHERE COD_ELEMENTO = pCodEle
                       AND COD_TIPO_MISURA = pTipMis;
                END IF;
                CASE pTipRet
                    WHEN PKG_Mago.gcTipReteAT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_AT = vReteAT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_AT <> vReteAT;
                    WHEN PKG_Mago.gcTipReteMT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_MT = vReteMT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_MT <> vReteMT;
                    WHEN PKG_Mago.gcTipReteBT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_BT = vReteBT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_BT <> vReteBT;
                END CASE;
            EXCEPTION
             WHEN NO_DATA_FOUND THEN
                 INSERT INTO REL_ELEMENTO_TIPMIS (COD_ELEMENTO, COD_TIPO_MISURA, TAB_MISURE,    RETE_AT, RETE_MT, RETE_BT )
                                          VALUES (pCodEle,      pTipMis,         vTipTabMisure, vReteAT, vReteMT, vReteBT );
            END;
         END SetRel_Ele_TipMis;
BEGIN

    IF vTipEle IS NULL THEN
        SELECT COD_TIPO_ELEMENTO
          INTO vTipEle
          FROM ELEMENTI
         WHERE COD_ELEMENTO = pCodEle;
    END IF;

    SELECT GER_ECS
      INTO vGerEcs
      FROM TIPI_ELEMENTO
     WHERE COD_TIPO_ELEMENTO = vTipEle;

    IF vGerEcs = 1 THEN
        -- eseguo forzatura per evitare il proliferare di misure aggregate
        -- a livello di gerarchie di Stato normale e Attuale:
        -- per elementi in gerarchia di CS uso solo l'organizazzione Elettrica
        vOrganizzazione := PKG_Mago.gcOrganizzazELE;
    ELSE
        vOrganizzazione := pOrganizzaz;
    END IF;

    BEGIN
        SELECT COD_TRATTAMENTO_ELEM
          INTO vTrtEle
          FROM TRATTAMENTO_ELEMENTI
         WHERE COD_ELEMENTO = pCodEle
           AND COD_TIPO_MISURA = pTipMis
           AND COD_TIPO_FONTE = pTipFon
           AND COD_TIPO_RETE = pTipRet
           AND COD_TIPO_CLIENTE = pTipCli
           AND ORGANIZZAZIONE = pOrganizzaz
           AND TIPO_AGGREGAZIONE = pTipoAggreg;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO TRATTAMENTO_ELEMENTI (COD_ELEMENTO,
                                              COD_TIPO_ELEMENTO,
                                              COD_TIPO_MISURA,
                                              COD_TIPO_FONTE,
                                              COD_TIPO_RETE,
                                              COD_TIPO_CLIENTE,
                                              ORGANIZZAZIONE,
                                              TIPO_AGGREGAZIONE)
                                      VALUES (pCodEle,
                                              vTipEle,
                                              pTipMis,
                                              pTipFon,
                                              pTipRet,
                                              pTipCli,
                                              pOrganizzaz,
                                              pTipoAggreg)
                                   RETURNING COD_TRATTAMENTO_ELEM INTO vTrtEle;
        WHEN OTHERS THEN RAISE;
    END;

    CASE
        WHEN pTipoAggreg = PKG_Mago.gcStatoNullo
            THEN SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAcquisite);
        WHEN pTipoAggreg IN (PKG_Mago.gcStatoNormale,PKG_Mago.gcStatoAttuale)
            THEN SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAggregate);
        WHEN pTipoAggreg = gcModalitaKPI
            THEN NULL;
    END CASE;
    RETURN vTrtEle;

EXCEPTION
    WHEN OTHERS THEN
    --     ROLLBACK; --commentata oer MAGO 1168
       dbms_output.put_line('Codici         : '||'Ele='||NVL(TO_CHAR(pCodEle),'<null>')||' - '||
                                                    'Tip='||NVL(pTipEle,'<null>')||' - '||
                                                    'Mis='||NVL(pTipMis,'<null>')||' - '||
                                                    'Fon='||NVL(pTipFon,'<null>')||' - '||
                                                    'Ret='||NVL(pTipRet,'<null>')||' - '||
                                                    'Cli='||NVL(pTipCli,'<null>')||' - '||
                                                    'Org='||NVL(TO_CHAR(pOrganizzaz),'<null>')||'('||NVL(TO_CHAR(vOrganizzazione),'<null>')||') - '||
                                                    'Agr='||NVL(TO_CHAR(pTipoAggreg),'<null>'));
         dbms_output.put_line(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    --     dbms_output.put_line(PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetTrattamentoElemento;

---- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisura         (pTipoProcesso  IN VARCHAR2,
                                 pCodTrtEle     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                 pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                 pTipoMis       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pValore        IN MISURE_ACQUISITE.VALORE%TYPE,
                                 pTipRete       IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 pAggrega       IN BOOLEAN,
                                 pResult       OUT INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza la misura:
     - Si inserisce la misura; se esiste si controlla se va aggiornata.
     - Si esegue il controllo sulle date da aggregare.
-----------------------------------------------------------------------------------------------------------*/
    vResult  NUMBER;
    vValore  NUMBER(20,4) := ROUND(pValore,4);
    vPerc    NUMBER(5,2):= 100;
    vAggrega BOOLEAN := pAggrega;
 BEGIN

     pResult := PKG_Mago.gcNoOper;
     IF PKG_MISURE.IsMisuraStatica(pTipoMis) = PKG_UtlGlb.gkFlagOn THEN
         IF gPrevData <> pDataMis THEN
             gPrevData := pDataMis;
             gMisura := NULL;
             PKG_GestAnagr.InitTab(gMisura,pDataMis,USER,'MISURE_ACQUISITE_STATICHE','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
             PKG_GestAnagr.AddCol (gMisura,'COD_TRATTAMENTO_ELEM',PKG_GestAnagr.cColChiave);
             PKG_GestAnagr.AddCol (gMisura,'VALORE',PKG_GestAnagr.cColAttributo);
         END IF;
         PKG_GestAnagr.InitRow(gMisura);
         PKG_GestAnagr.AddVal (gMisura, 'COD_TRATTAMENTO_ELEM', pCodTrtEle);
         vValore := ROUND(vValore * (vperc / 100),4);
         PKG_GestAnagr.AddVal (gMisura, 'VALORE', vValore);
         PKG_GestAnagr.Elabora(gMisura,vResult);
         CASE vResult
              WHEN PKG_GestAnagr.cInsertito  THEN pResult := PKG_Mago.gcInserito;
              WHEN PKG_GestAnagr.cModificato THEN pResult := PKG_Mago.gcModificato;
              ELSE pResult := PKG_Mago.gcNoOper;
         END CASE;
     ELSE
        BEGIN
            IF vperc != 0 THEN
               vValore := ROUND(vValore * (vperc / 100),4);
            END IF;
            INSERT INTO MISURE_Acquisite(COD_TRATTAMENTO_ELEM
                                        ,DATA
                                        ,VALORE)
                                  VALUES(pCodTrtEle
                                        ,pDataMis
                                        ,vValore);
            pResult := PKG_Mago.gcInserito;
     --       dbms_output.put_line(' ins mis '||to_char(pDataMis,'ddmmyyyy hh24:mi'));
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                 UPDATE MISURE_Acquisite
                    SET VALORE  = vValore
                  WHERE COD_TRATTAMENTO_ELEM = pCodTrtEle
                    AND DATA = pDataMis
                     AND VALORE != vValore;
                 IF SQL%ROWCOUNT > 0 THEN
                      pResult := PKG_Mago.gcModificato;
    --        dbms_output.put_line(' mod mis '||to_char(pDataMis,'ddmmyyyy hh24:mi'));
                 END IF;
            WHEN OTHERS THEN
             RAISE;
        END;
     END IF;

     RETURN;
 END AddMisura;

---- ----------------------------------------------------------------------------------------------------------

procedure sp_ungiorno is
  vnumore integer := 24;

begin

  -- 1 = SN e 2 = SA
  for iStato in 0..0  loop
      
 
      FOR rigaProd in
      ( 
           SELECT e.cod_tipo_elemento vtipele,cod_elemento vcodele,d.cod_tipo_cliente vtipcli, e.cod_gest_elemento
            FROM v_ELEMENTI e
            join elementi_def d
            using (cod_elemento)
            where e.cod_tipo_elemento in ('TRM')
            --where e.cod_tipo_elemento in ('CMT')
      ) LOOP
      
        vValMis := 0;
        vtipcli := rigaProd.vtipcli;
        
        IF rigaProd.vTipEle = 'TRM' THEN
            -- Produttori BT
            select count(*)*1000 quanti 
              into vValMis
            from (select * from table(pkg_elementi.LeggiGerarchiaInf (rigaProd.vcodele,1,1)) WHERE cod_tipo_elemento='TRX'); 

            if vValMis > 0 then
          
              vtipmis := 'PMP';
              
              vTrtEle := GetTrattamentoElemento(rigaProd.vcodele,
                                                rigaProd.vTipEle,
                                                vTipMis,
                                                'S', --pTipFte,
                                                'B', --vTipRet,
                                                'A',
                                                PKG_Mago.gcOrganizzazELE,
                                                iStato);
              /*test solo per 15/09 ore 1 */
              FOR rigaGiorni in
              (
                SELECT vGiorno+(level/24) dataMisura
                from dual
                connect by level<=vnumore
              ) LOOP
  
                sp_print2374('data mis '||rigagiorni.dataMisura||' '||'elemento '||rigaProd.vcodele||' '||'tip cli. '||vtipcli||' '||' tipo mis. '||vtipmis||' '||'tratt. elem. '||vTrtEle);
                
                AddMisura         ('MET',
                                     vTrtEle,--     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                     rigagiorni.dataMisura, --pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                     vTipMis , --     IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     vValMis, --          IN MISURE_ACQUISITE.VALORE%TYPE,
                                     'B'  , --        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     TRUE, --pAggrega       IN BOOLEAN,
                                     pResult);
          
              END LOOP;
                                  
            end if;
            
            begin
              -- consumatori BT
              select n_ute_bt
                into vValMis
              from AUI_TRASFORMATORI_TLC
              where rigaProd.cod_gest_elemento = cod_org_nodo||ser_nodo||num_nodo||tipo_elemento||id_trasf;

              if vValMis > 0 then
            
                vtipmis := 'PCT-P';
                vValMis := vValMis*1000;
                
                vTrtEle := GetTrattamentoElemento(rigaProd.vcodele,
                                                  rigaProd.vTipEle,
                                                  vTipMis,
                                                  'S', --pTipFte,
                                                  'B', --vTipRet,
                                                  'C',
                                                  PKG_Mago.gcOrganizzazELE,
                                                  iStato);
                /*test solo per 15/09 ore 1 */
                FOR rigaGiorni in
                (
                  SELECT vGiorno+(level/24) dataMisura
                  from dual
                  connect by level<=vnumore
                ) LOOP
    
                  sp_print2374('data mis '||rigagiorni.dataMisura||' '||'elemento '||rigaProd.vcodele||' '||'tip cli. '||vtipcli||' '||' tipo mis. '||vtipmis||' '||'tratt. elem. '||vTrtEle);
                  
                  AddMisura         ('MET',
                                       vTrtEle,--     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                       rigagiorni.dataMisura, --pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                       vTipMis , --     IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                       vValMis, --          IN MISURE_ACQUISITE.VALORE%TYPE,
                                       'B'  , --        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                       TRUE, --pAggrega       IN BOOLEAN,
                                       pResult);
            
                END LOOP;
                                    
              end if;
            
            exception
              when others then
                sp_print2374('elemento non trovato in TRASFORMATORI_TLC '||rigaProd.cod_gest_elemento||' error msg: '||sqlerrm);

            end;
        ELSE
          vValMis := 10000; -- 10KW
          for rigamisura in (select 'PCT-P' misura from dual
                              union 
                              select 'PMP' misura from dual) 
          loop
      
            vtipmis := rigamisura.misura;
            /*per produttori MT solo misura PMP; per clienti MT solo misura PCT-P; per clienti e produttori entrambe */
            if (vtipcli = 'A' and vtipmis = 'PMP') OR (vtipcli = 'C' and vtipmis = 'PCT-P') OR (vtipcli = 'B') then
                
                vTrtEle := GetTrattamentoElemento(rigaProd.vcodele,
                                                  rigaProd.vTipEle,
                                                  vTipMis,
                                                  'S', --pTipFte,
                                                  'M', --vTipRet,
                                                  vtipcli,
                                                  PKG_Mago.gcOrganizzazELE,
                                                  iStato);
                /*test solo per 15/09 ore 1 */
                FOR rigaGiorni in
                (
                  SELECT vGiorno+(level/24) dataMisura
                  from dual
                  connect by level<=vnumore
                ) LOOP

                   sp_print2374('data mis '||rigagiorni.dataMisura||' '||'elemento '||rigaProd.vcodele||' '||'tip cli. '||vtipcli||' '||' tipo mis. '||vtipmis||' '||'tratt. elem. '||vTrtEle);
 
                   AddMisura         ('MET',
                                         vTrtEle,--     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                         rigagiorni.dataMisura, --pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                         vTipMis , --     IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                         vValMis, --          IN MISURE_ACQUISITE.VALORE%TYPE,
                                         'M'  , --        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                         TRUE, --pAggrega       IN BOOLEAN,
                                         pResult);
        
                END LOOP;
              
            end if;
            
          end loop;          
        
        END IF;
    END LOOP;

  END LOOP;

end;   
   
begin
/*
vgiorno := to_date('01072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31072020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30082020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31082020','ddmmyyyy'); sp_ungiorno; commit;

vgiorno := to_date('01092020','ddmmyyyy'); sp_ungiorno; commit;

vgiorno := to_date('02092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14092020','ddmmyyyy'); sp_ungiorno; commit;
*/
vgiorno := to_date('15092020','ddmmyyyy'); sp_ungiorno; commit;
/*
vgiorno := to_date('16092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29092020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30092020','ddmmyyyy'); sp_ungiorno; commit;

/*
vgiorno := to_date('01102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30102020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31102020','ddmmyyyy'); sp_ungiorno; commit;

/*
vgiorno := to_date('01112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08112020','ddmmyyyy'); sp_ungiorno; commit;

vgiorno := to_date('09112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29112020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30112020','ddmmyyyy'); sp_ungiorno; commit;

vgiorno := to_date('01122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30122020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31122020','ddmmyyyy'); sp_ungiorno; commit;
*/
/*
vgiorno := to_date('01012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31012020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29022020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31032020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30042020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('31052020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('01062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('02062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('03062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('04062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('05062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('06062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('07062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('08062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('09062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('10062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('11062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('12062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('13062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('14062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('15062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('16062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('17062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('18062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('19062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('20062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('21062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('22062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('23062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('24062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('25062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('26062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('27062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('28062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('29062020','ddmmyyyy'); sp_ungiorno; commit;
vgiorno := to_date('30062020','ddmmyyyy'); sp_ungiorno; commit;
*/

end;
/