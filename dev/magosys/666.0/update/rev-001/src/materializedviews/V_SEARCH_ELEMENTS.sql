 BEGIN
   EXECUTE IMMEDIATE ('DROP MATERIALIZED VIEW v_search_elements');
EXCEPTION WHEN OTHERS THEN
   NULL;
END;
/

 CREATE MATERIALIZED VIEW v_search_elements
 TABLESPACE MAGO_DATA
 BUILD IMMEDIATE
 USING INDEX 
 REFRESH FORCE ON DEMAND USING TRUSTED CONSTRAINTS
 DISABLE  QUERY REWRITE 
 AS 
SELECT COD_ELEMENTO,
        SEARCH_STRING,
        TIPO
  FROM (SELECT A.COD_ELEMENTO,
               DATA_ATTIVAZIONE,
               SEARCH_STRING,
               TIPO,
               MAX(DATA_ATTIVAZIONE) OVER(PARTITION BY E.COD_ELEMENTO ORDER BY E.COD_ELEMENTO,DATA_ATTIVAZIONE DESC) MAXDATA,
               T.COD_TIPO_ELEMENTO
          FROM (SELECT COD_ELEMENTO,
                       MIN (DATA_ATTIVAZIONE)    DATA_ATTIVAZIONE,
                       LOWER (COD_GEST_ELEMENTO) SEARCH_STRING,
                       'GST' TIPO
                  FROM ELEMENTI
                 INNER JOIN ELEMENTI_DEF USING (COD_ELEMENTO)
                 WHERE INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                 GROUP BY COD_ELEMENTO, COD_GEST_ELEMENTO
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       REGEXP_REPLACE(TRIM(LOWER(NOME_ELEMENTO)),'[^[:alnum:]]') SEARCH_STRING,
                       'NOM' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI USING (cod_elemento)
                 WHERE (NVL(NOME_ELEMENTO, '.')) <> '.'
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       TRIM(LOWER(RIF_ELEMENTO)) SEARCH_STRING,
                       'DIR' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI E USING (cod_elemento)
                 WHERE RIF_ELEMENTO IS NOT NULL
                   AND E.COD_TIPO_ELEMENTO = ('LMT')  -- LINEAMT
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
                UNION
                SELECT COD_ELEMENTO,
                       DATA_ATTIVAZIONE,
                       TRIM(LOWER(RIF_ELEMENTO)) SEARCH_STRING,
                       'POD' TIPO
                  FROM ELEMENTI_DEF INNER JOIN ELEMENTI E USING (cod_elemento)
                 WHERE RIF_ELEMENTO IS NOT NULL
                   AND E.COD_TIPO_ELEMENTO IN ('CMT','CBT','CAT')  -- POD0
                   AND INSTR (COD_GEST_ELEMENTO, CHR (167)) = 0
               ) A
         INNER JOIN ELEMENTI E ON A.COD_ELEMENTO = E.COD_ELEMENTO
         INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
         WHERE T.NASCOSTO <> 1
           AND E.COD_TIPO_ELEMENTO <> 'CFT'
       )
 WHERE CASE
          WHEN TIPO = 'GST' THEN 1
          ELSE CASE
                  WHEN DATA_ATTIVAZIONE = MAXDATA
                    THEN 1
                    ELSE 0
               END
       END = 1
/

CREATE  INDEX v_search_elements_k1 ON v_search_elements ( search_string ) LOGGING TABLESPACE MAGO_IDX;
/

