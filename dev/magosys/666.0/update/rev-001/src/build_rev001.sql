SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 666.0 rev 1
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn STMAUI/stmaui@ARCDB1_MCO_BO.enel.it 

PROMPT _______________________________________________________________________________________
PROMPT Grants STMAUI
PROMPT
@./Grants/GRANT_STMAUI.sql

PROMPT _______________________________________________________________________________________
PROMPT Synonyms
PROMPT
@./Synonyms/STMAUI_SYNONYMS.sql

conn RIGEDI/RIGEDI@ARCDB1_MCO_BO.enel.it 

PROMPT _______________________________________________________________________________________
PROMPT Grants RIGEDI
PROMPT
@./Grants/GRANT_RIGEDI.sql

PROMPT _______________________________________________________________________________________
PROMPT Synonyms
PROMPT
@./Synonyms/RIGEDI_SYNONYMS.sql

conn TMESX_2/TMESX_2@ARCDB1_MCO_BO.enel.it 

PROMPT _______________________________________________________________________________________
PROMPT Grants RIGEDI
PROMPT
@./Grants/GRANT_TMESX.sql

PROMPT _______________________________________________________________________________________
PROMPT Synonyms
PROMPT
@./Synonyms/TMX_SYNONYMS.sql

--conn mago/mago&tns_arcdb2

conn MAGO/mago@ARCDB1_MCO_BO.enel.it 

PROMPT _______________________________________________________________________________________
PROMPT materialized views MAGO
PROMPT
@./materializedviews/V_SEARCH_ELEMENTS.sql

PROMPT _______________________________________________________________________________________
PROMPT Package bodies
PROMPT
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_ASID_UTL.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_SPV_RETE_ELE.sql
@./PackageBodies/PKG_TRANSFER_RES.sql

PROMPT _______________________________________________________________________________________
PROMPT Procedures
PROMPT
@./Procedures/jira2374.sql



disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 666.0 rev 1
PROMPT =======================================================================================

SPOOL OFF
