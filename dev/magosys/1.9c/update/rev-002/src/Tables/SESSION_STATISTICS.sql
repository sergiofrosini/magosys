PROMPT TABLE SESSION_STATISTICS;
--
-- SESSION_STATISTICS  (Table) 
--

BEGIN 
    EXECUTE IMMEDIATE 'ALTER TABLE SESSION_STATISTICS ADD (DATE_START  DATE)';
    DBMS_OUTPUT.PUT_LINE('Definita Colonna DATE_START in tabella SESSION_STATISTICS');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -01430 THEN NULL; --ORA-01430: la colonna che si sta aggiungendo esiste gia' nella tabella
                                 DBMS_OUTPUT.PUT_LINE('Colonna DATE_START in tabella SESSION_STATISTICS gia'' presente');
                            ELSE RAISE;
        END IF;
END;
/

COMMENT ON COLUMN SESSION_STATISTICS.DATE_START IS 'data/ora di avvio della sessione'
/


BEGIN 
    EXECUTE IMMEDIATE 'ALTER TABLE SESSION_STATISTICS ADD (DATE_END    DATE)';
    DBMS_OUTPUT.PUT_LINE('Definita Colonna DATE_END in tabella SESSION_STATISTICS');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -01430 THEN NULL; --ORA-01430: la colonna che si sta aggiungendo esiste gia' nella tabella
                                 DBMS_OUTPUT.PUT_LINE('Colonna DATE_END in tabella SESSION_STATISTICS gia'' presente');
                            ELSE RAISE;
        END IF;
END;
/

COMMENT ON COLUMN SESSION_STATISTICS.DATE_START IS 'data/ora di termine della sessione (Status=DONE)'
/


CREATE OR REPLACE TRIGGER BEF_IUR_SESSION_STATISTICS 
BEFORE INSERT OR UPDATE
ON SESSION_STATISTICS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:      BEF_IUR_SESSION_STATISTICS
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.2     16/09/2015   Moretti C.      Created this trigger.
   
   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        :NEW.DATE_START := SYSDATE;
    END IF;
    IF :NEW.STATUS = PKG_Stats.gSessionStatusDone THEN
        :NEW.DATE_END := SYSDATE;
    END IF; 
END BEF_IUR_SESSION_STATISTICS;
/


BEGIN 
    EXECUTE IMMEDIATE 'CREATE INDEX SESSION_STATISTICS_SERV_ID ON SESSION_STATISTICS (SERVICE_ID) TABLESPACE &TBS&IDX';
    DBMS_OUTPUT.PUT_LINE('Definito Indice SESSION_STATISTICS_SERV_ID');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -00955 THEN NULL; --ORA-00955: nome gia' utilizzato da un oggetto esistente
                                 DBMS_OUTPUT.PUT_LINE('Indice SESSION_STATISTICS_SERV_ID gia'' definito');
                            ELSE RAISE;
        END IF;
END;
/

BEGIN 
    EXECUTE IMMEDIATE 'CREATE INDEX SESSION_STATISTICS_SERV_REF ON SESSION_STATISTICS (SERVICE_REF) TABLESPACE &TBS&IDX';
    DBMS_OUTPUT.PUT_LINE('Definito Indice SESSION_STATISTICS_SERV_REF');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -00955 THEN NULL; --ORA-00955: nome gia' utilizzato da un oggetto esistente
                                 DBMS_OUTPUT.PUT_LINE('Indice SESSION_STATISTICS_SERV_REF gia'' definito');
                            ELSE RAISE;
        END IF;
END;
/

