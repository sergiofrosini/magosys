PROMPT TABLE TIPI_MISURA;
--
-- TIPI_MISURA  (Table) 
--

BEGIN 
    EXECUTE IMMEDIATE 'ALTER TABLE TIPI_MISURA ADD (FLG_VAR_PI NUMBER(1) DEFAULT 0)';
    DBMS_OUTPUT.PUT_LINE('Definita Colonna FLG_VAR_PI in tabella TIPI_MISURA');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -01430 THEN NULL; --ORA-01430: la colonna che si sta aggiungendo esiste gia' nella tabella
                                 DBMS_OUTPUT.PUT_LINE('Colonna FLG_VAR_PI in tabella TIPI_MISURA gia'' presente');
                            ELSE RAISE;
        END IF;
END;
/

BEGIN 
    EXECUTE IMMEDIATE 'ALTER TABLE TIPI_MISURA ADD (FLG_VAR_PIMP NUMBER(1) DEFAULT 0)';
    DBMS_OUTPUT.PUT_LINE('Definita Colonna FLG_VAR_PIMP in tabella TIPI_MISURA');
EXCEPTION
    WHEN others THEN
        IF SQLCODE = -01430 THEN NULL; --ORA-01430: la colonna che si sta aggiungendo esiste gia' nella tabella
                                 DBMS_OUTPUT.PUT_LINE('Colonna FLG_VAR_PIMP in tabella TIPI_MISURA gia'' presente');
                            ELSE RAISE;
        END IF;
END;
/

COMMENT ON COLUMN TIPI_MISURA.FLG_VAR_PI IS '1=La misura varia in funzione della Potenza Installata - Utilizzato per Replay'
/

COMMENT ON COLUMN TIPI_MISURA.FLG_VAR_PIMP IS '1=La misura varia in funzione della Potenza Impegnata - Utilizzato per Replay'
/
