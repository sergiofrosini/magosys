PROMPT PACKAGE PKG_STATS;
--
-- PKG_STATS  (Package) 
--
--  Dependencies: 
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   DEFAULT_CO (Table)
--   DUAL ()
--   DUAL (Synonym)
--   MEASURE_OFFLINE_PARAMETERS (Table)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   SCHEDULED_JOBS (Table)
--   SESSION_STATISTICS (Table)
--   STANDARD (Package)
--
CREATE OR REPLACE PACKAGE PKG_STATS AS

/* ***********************************************************************************************************
   NAME:       PKG_Stats
   PURPOSE:    Gestione delle statistiche

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.6.a      14/01/2014  Moretti C.       Implementazioni per versione 1.6a
   1.8a.1     30/04/2014  Moretti C.       Eliminazione Log 'invasivo' in procedure CheckSessions
   1.9c.0     23/06/2015  Moretti C.       Utilizzo Sequence tramite SQL dinamico
   1.9c.2     27/08/2015  Moretti C.       Definizione Proc InitStatisticSession  
                                              con parametro SESSION_STATISTICS%ROWTYPE  
              10/09/2015  Moretti C.       Definizione Procedure GetServiceRunStatus

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Globali Pubbliche
*********************************************************************************************************** */

    TYPE t_RowSession IS RECORD (COD_SESSIONE       SESSION_STATISTICS.COD_SESSIONE%TYPE, 
                                 SERVICE_ID         SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 SERVICE_REF        SESSION_STATISTICS.SERVICE_REF%TYPE,
                                 COD_TIPO_MISURA    SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                 STATUS             SESSION_STATISTICS.STATUS%TYPE,
                                 DATE_FROM          SESSION_STATISTICS.DATE_FROM%TYPE,
                                 DATE_TO            SESSION_STATISTICS.DATE_TO%TYPE,
                                 LEAF_START_DATE    SESSION_STATISTICS.LEAF_START_DATE%TYPE,
                                 LEAF_END_DATE      SESSION_STATISTICS.LEAF_END_DATE%TYPE,
                                 AGGREG_START_DATE  SESSION_STATISTICS.AGGREG_START_DATE%TYPE,
                                 AGGREG_END_DATE    SESSION_STATISTICS.AGGREG_END_DATE%TYPE,
                                 AGGREG_COMPLETED   SESSION_STATISTICS.AGGREG_COMPLETED%TYPE);


    gSessionID_MdsServ   SESSION_STATISTICS.SERVICE_ID%TYPE := ''; -- Legge da parametro in MEASURE_OFFLINE_PARAMETERS 
    gSessionID_fromST    SESSION_STATISTICS.SERVICE_ID%TYPE := 'MAGO-Load-Data-fromST';
    gSessionID_aggreg    SESSION_STATISTICS.SERVICE_ID%TYPE := 'MAGO-Load-Data-aggreg';
    gSessionID_RplyInit  SESSION_STATISTICS.SERVICE_ID%TYPE := 'RPLY-Init';
    gSessionID_RplyLoad  SESSION_STATISTICS.SERVICE_ID%TYPE := 'RPLY-Load';

    gSessionStatusAggr   SESSION_STATISTICS.STATUS %TYPE    := 'AGGREGATION_PROGRESS';
    gSessionStatusWork   SESSION_STATISTICS.STATUS %TYPE    := 'WORKING';
    gSessionStatusDone   SESSION_STATISTICS.STATUS %TYPE    := 'DONE';


/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 PROCEDURE CheckSessions;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetStatisticSessionID  (pSessionID     OUT NUMBER);

 FUNCTION  GetStatisticSessionID  RETURN NUMBER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitStatisticSession (pCodSession    SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                 pServiceID     SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pServiceRef    SESSION_STATISTICS.SERVICE_REF%TYPE,
                                 pTipMisura     SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                 pStatus        SESSION_STATISTICS.STATUS%TYPE,
                                 pDateFrom      SESSION_STATISTICS.DATE_FROM%TYPE,
                                 pDateTo        SESSION_STATISTICS.DATE_TO%TYPE,
                                 pLeafStartDate SESSION_STATISTICS.LEAF_START_DATE%TYPE DEFAULT NULL,
                                 pLeafEndDate   SESSION_STATISTICS.LEAF_END_DATE%TYPE DEFAULT NULL,
                                 pAggrStartDate SESSION_STATISTICS.AGGREG_START_DATE%TYPE DEFAULT NULL,
                                 pAggrEndDate   SESSION_STATISTICS.AGGREG_END_DATE%TYPE DEFAULT NULL,
                                 pAggrCompleted SESSION_STATISTICS.AGGREG_COMPLETED%TYPE DEFAULT NULL
                                );

 PROCEDURE InitStatisticSession (pSessionRow    SESSION_STATISTICS%ROWTYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                 pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pDataRif       IN DATE,
                                 pTipMisList    IN VARCHAR2 DEFAULT NULL); 

 PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                 pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pTipMisList    IN VARCHAR2 DEFAULT NULL); 
                               

-- ----------------------------------------------------------------------------------------------------------


END PKG_Stats;
/
SHOW ERRORS;

