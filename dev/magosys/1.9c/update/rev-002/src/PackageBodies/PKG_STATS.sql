PROMPT PACKAGE BODY PKG_STATS;
--
-- PKG_STATS  (Package Body) 
--
--  Dependencies: 
--   PKG_STATS (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_STATS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9c.2
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckSessions AS
/*-----------------------------------------------------------------------------------------------------------
    Controlla le sessioni ancora aperte ed aggiorna lo stato di avanzamento delle aggregazioni
-----------------------------------------------------------------------------------------------------------*/
    vLog                  PKG_Logs.t_StandardLog;
    vCount                NUMBER;
    vStatusRun            SESSION_STATISTICS.STATUS%TYPE := gSessionStatusAggr;
    vStatusDone           SESSION_STATISTICS.STATUS%TYPE := gSessionStatusDone;

BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassSRV,
                                pFunzione     => 'PKG_Stats.CheckSessions',
                                pForceLogFile => TRUE);

    -- ********************************************************************************
    -- Gestione sessioni di aggregazione
    -- ********************************************************************************
    FOR i IN (-- sessioni di aggregazione a fronte di immissione misure da STWebRegistry
              SELECT '1' TIPO, A.* 
                FROM SESSION_STATISTICS A
               WHERE AGGREG_COMPLETED = 0
                 AND SERVICE_ID = gSessionID_MdsServ 
                 AND LEAF_END_DATE IS NOT NULL
              UNION -- sessioni di aggregazione a fronte di ricalcolo aggregazione a per attualizzazioni (*DAT) o modifiche assetto rete (*nea) 
              SELECT '2' TIPO, A.*
                FROM SESSION_STATISTICS A
               WHERE AGGREG_COMPLETED = 0
                 AND SERVICE_ID = gSessionID_aggreg
             ) LOOP

        IF i.AGGREG_START_DATE IS NULL THEN
            -- assume che il il processo di aggregazione sia partito
            PKG_Logs.StdLogAddTxt(i.SERVICE_ID||' '||i.SERVICE_REF||' '||i.COD_TIPO_MISURA||' :  '||vStatusRun,TRUE,NULL,vLog);
            UPDATE SESSION_STATISTICS SET AGGREG_START_DATE = SYSDATE,
                                          STATUS = vStatusRun
             WHERE COD_SESSIONE = i.COD_SESSIONE
               AND DATE_FROM = i.DATE_FROM
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;

        IF PKG_Mago.IsReplay THEN
            -- in ambiente REPLAY non si calcolano le aggregate per cui azzero il contatore 
            -- simulando il fatto che non esistano richieste di aggregazione ancora da soddisfare !!!!
            vCount := 0;
        ELSE
            SELECT COUNT(*) INTO vCount
              FROM SCHEDULED_JOBS
             WHERE DATARIF BETWEEN i.DATE_FROM AND i.DATE_TO
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;

        IF vCount = 0 THEN
            -- non esistono richieste di aggregazione per il periodo ancora  
            PKG_Logs.StdLogAddTxt(i.SERVICE_ID||' '||i.SERVICE_REF||' '||i.COD_TIPO_MISURA||' :  '||vStatusDone,TRUE,NULL,vLog);

            UPDATE SESSION_STATISTICS SET AGGREG_END_DATE = SYSDATE,
                                          STATUS = vStatusDone,
                                          AGGREG_COMPLETED = 1
             WHERE COD_SESSIONE = i.COD_SESSIONE
               AND DATE_FROM = i.DATE_FROM
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;

        COMMIT;

    END LOOP;

    --PKG_Logs.StdLogPrint(vLog);  -- eliminato 30/04/2014

 EXCEPTION
    WHEN OTHERS THEN
         -- annulla gli aggiornamenti non ancora committati.
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         COMMIT;
         RAISE;

END CheckSessions;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetStatisticSessionID  (pSessionID     OUT NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce un nuovo identificativo di sessione Forecast
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    -- per impredire che in caso di rigenerazione della Sequence (Replay) il package si invalidi uso sql dinamico 
    EXECUTE IMMEDIATE 'SELECT SESSION_STATISTIC_IDSEQ.NEXTVAL FROM DUAL' INTO pSessionID;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Stats.GetStatisticSessionID'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetStatisticSessionID;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetStatisticSessionID  RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce un nuovo identificativo di sessione Forecast
-----------------------------------------------------------------------------------------------------------*/
    vCodSession SESSION_STATISTICS.COD_SESSIONE%TYPE;  
BEGIN
    GetStatisticSessionID(vCodSession);
    RETURN vCodSession;
END GetStatisticSessionID;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitStatisticSession (pCodSession    SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                pServiceID     SESSION_STATISTICS.SERVICE_ID%TYPE,
                                pServiceRef    SESSION_STATISTICS.SERVICE_REF%TYPE,
                                pTipMisura     SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                pStatus        SESSION_STATISTICS.STATUS%TYPE,
                                pDateFrom      SESSION_STATISTICS.DATE_FROM%TYPE,
                                pDateTo        SESSION_STATISTICS.DATE_TO%TYPE,
                                pLeafStartDate SESSION_STATISTICS.LEAF_START_DATE%TYPE DEFAULT NULL,
                                pLeafEndDate   SESSION_STATISTICS.LEAF_END_DATE%TYPE DEFAULT NULL,
                                pAggrStartDate SESSION_STATISTICS.AGGREG_START_DATE%TYPE DEFAULT NULL,
                                pAggrEndDate   SESSION_STATISTICS.AGGREG_END_DATE%TYPE DEFAULT NULL,
                                pAggrCompleted SESSION_STATISTICS.AGGREG_COMPLETED%TYPE DEFAULT NULL
                               )  AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza/Aggiorna una tupla di statistica sessione
-----------------------------------------------------------------------------------------------------------*/
    vNum INTEGER;
BEGIN

    --SELECT COUNT(*)INTO vNum
    --  FROM SESSION_STATISTICS WHERE SERVICE_ID = pServiceID
    --                            AND DATE_FROM  = pDateFrom;
    --IF vNum > 0 THEN
    --    DELETE SESSION_STATISTICS WHERE SERVICE_ID = pServiceID
    --                                AND DATE_FROM  = pDateFrom;
    --END IF;

    MERGE INTO SESSION_STATISTICS S
         USING
              ( SELECT pCodSession    COD_SESSIONE,
                       pServiceID     SERVICE_ID,
                       pServiceRef    SERVICE_REF,
                       pTipMisura     COD_TIPO_MISURA,
                       pStatus        STATUS,
                       pDateFrom      DATE_FROM,
                       pDateTo        DATE_TO,
                       pLeafStartDate LEAF_START_DATE,
                       pLeafEndDate   LEAF_END_DATE,
                       pAggrStartDate AGGREG_START_DATE,
                       pAggrEndDate   AGGREG_END_DATE,
                       pAggrCompleted AGGREG_COMPLETED
                  FROM DUAL
              ) X
--            ON (    S.COD_SESSIONE      = X.COD_SESSIONE
--                AND S.COD_TIPO_MISURA   = X.COD_TIPO_MISURA
--                AND S.DATE_FROM         = X.DATE_FROM)
            ON (S.COD_SESSIONE      = X.COD_SESSIONE)
          WHEN MATCHED THEN
        UPDATE SET  S.SERVICE_ID        = X.SERVICE_ID,
                    S.COD_TIPO_MISURA   = X.COD_TIPO_MISURA,
                    S.SERVICE_REF       = X.SERVICE_REF,
                    S.STATUS            = X.STATUS,
                    S.DATE_TO           = X.DATE_TO,
                    S.DATE_FROM         = X.DATE_FROM,
                    S.LEAF_START_DATE   = X.LEAF_START_DATE,
                    S.LEAF_END_DATE     = X.LEAF_END_DATE,
                    S.AGGREG_START_DATE = X.AGGREG_START_DATE,
                    S.AGGREG_END_DATE   = X.AGGREG_END_DATE,
                    S.AGGREG_COMPLETED  = X.AGGREG_COMPLETED
          WHEN NOT MATCHED THEN
        INSERT (COD_SESSIONE, SERVICE_ID, SERVICE_REF, COD_TIPO_MISURA, STATUS,
                DATE_FROM, DATE_TO, LEAF_START_DATE, LEAF_END_DATE,
                AGGREG_START_DATE, AGGREG_END_DATE, AGGREG_COMPLETED)
        VALUES (X.COD_SESSIONE, X.SERVICE_ID, X.SERVICE_REF, X.COD_TIPO_MISURA, X.STATUS,
                X.DATE_FROM, X.DATE_TO, X.LEAF_START_DATE, X.LEAF_END_DATE,
                X.AGGREG_START_DATE, X.AGGREG_END_DATE, X.AGGREG_COMPLETED);

    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Stats.InitStatisticSession'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END InitStatisticSession;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitStatisticSession (pSessionRow    SESSION_STATISTICS%ROWTYPE)  AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza/Aggiorna una tupla di statistica sessione
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    InitStatisticSession(pSessionRow.COD_SESSIONE,
                         pSessionRow.SERVICE_ID,
                         pSessionRow.SERVICE_REF,
                         pSessionRow.COD_TIPO_MISURA,
                         pSessionRow.STATUS,
                         pSessionRow.DATE_FROM,
                         pSessionRow.DATE_TO,
                         pSessionRow.LEAF_START_DATE,
                         pSessionRow.LEAF_END_DATE,
                         pSessionRow.AGGREG_START_DATE,
                         pSessionRow.AGGREG_END_DATE,
                         pSessionRow.AGGREG_COMPLETED
                        );

END InitStatisticSession;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                pDataRif       IN DATE,
                                pTipMisList    IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
   Ritorna il cursore con stato ed altre info relative al servizio richiesto
   Se pDataRif nullo       ritorna i records relativi all'ultima sessione definita
   Se pDataRif valorizzato ritorna i records relativi alla prima sessione con data (DATE_FROM) >= pDataRif
   Se pTipMisList (Lista tipi misura separati da '|' [pipe]) e' valorizzato 
     seleziona solo le occorrenze con i tipi misura richiesti
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    cTipMis     CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'SRVTIPMIS';
    
BEGIN

    IF pTipMisList IS NOT NULL THEN
        DELETE FROM GTTD_VALORI_TEMP WHERE TIP = cTipMis;
        PKG_Mago.TrattaListaCodici(pTipMisList, cTipMis, vFlgNull);
    END IF;

    OPEN pRefCurs FOR SELECT COD_SESSIONE,SERVICE_ID,SERVICE_REF,COD_TIPO_MISURA,STATUS,
                             DATE_FROM,DATE_TO,LEAF_START_DATE,LEAF_END_DATE,
                             AGGREG_START_DATE,AGGREG_END_DATE,AGGREG_COMPLETED
                        FROM (SELECT S.*,
                                     RANK() OVER(PARTITION BY SERVICE_ID,COD_TIPO_MISURA ORDER BY COD_SESSIONE DESC) RNK1,
                                     RANK() OVER(PARTITION BY SERVICE_ID,COD_TIPO_MISURA ORDER BY COD_SESSIONE ASC)  RNK2
                                FROM SESSION_STATISTICS S
                                LEFT OUTER JOIN GTTD_VALORI_TEMP T ON T.ALF1 = S.COD_TIPO_MISURA
                                                                  AND T.TIP  = cTipMis
                               WHERE SERVICE_ID = pServiceId
                                 AND DATE_FROM >= NVL(pDataRif,TO_DATE('01011900','ddmmyyyy'))
                                 AND CASE
                                          WHEN pTipMisList IS NULL 
                                              THEN 1
                                              ELSE CASE WHEN T.ALF1 IS NULL 
                                                      THEN 0
                                                      ELSE 1
                                                   END
                                     END = 1
                             )
                       WHERE CASE 
                               WHEN pDataRif IS NULL 
                                    THEN CASE WHEN RNK1 = 1 THEN 1
                                                            ELSE 0
                                         END
                                    ELSE CASE WHEN RNK2 = 1 THEN 1
                                                            ELSE 0
                                         END
                             END = 1;
        
END GetServiceRunStatus;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                pTipMisList    IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
   Ritorna il cursore con stato ed altre info relative al servizio richiesto relative all'ultima sessione definita
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    GetServiceRunStatus  (pRefCurs, pServiceId, NULL, pTipMisList);
    
END GetServiceRunStatus;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN
    
    SELECT VALUE 
      INTO gSessionID_MdsServ
      FROM MEASURE_OFFLINE_PARAMETERS 
     WHERE KEY = 'STWebRegistry-WS.measure.offline.forecast_info_mds_serviceid';    

END PKG_Stats;
/
SHOW ERRORS;


