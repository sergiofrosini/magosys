UPDATE TIPI_MISURA 
   SET flg_var_pi = 1
 WHERE cod_tipo_misura LIKE 'PMP%' OR cod_tipo_misura LIKE 'PMC%' OR cod_tipo_misura IN ('PI','PAS');

UPDATE TIPI_MISURA 
   SET flg_var_pimp = 1
 WHERE cod_tipo_misura IN ('PCT-P','PCT-Q');

COMMIT;
