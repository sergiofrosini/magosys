PROMPT VIEW V_ANAGRAFICA_IMPIANTO;
--
-- V_ANAGRAFICA_IMPIANTO  (View) 
--
--  Dependencies: 
--   ESERCIZI (Table)
--   AVVOLGIMENTI_SA (Table)
--   IMPIANTIAT_SA (Table)
--   MONTANTIMT_SA (Table)
--   TRASFORMATORIAT_SA (Table)
--   SBARRE_SA (Table)
--   CLIENTIMT_SA (Table)
--   IMPIANTIMT_SA (Table)
--   TRASFORMATORIBT_SA (Table)
--   TIPI_MISURA (Table)
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   PKG_MAGO (Package)
--   PKG_ELEMENTI (Package)
--   V_ESERCIZI (View)
--   VW_VAR_POTENZA_ELEMENTI@PKG1_STMAUI.IT (View) 
--
CREATE OR REPLACE VIEW V_ANAGRAFICA_IMPIANTO
AS 
WITH
     /* ***************************************************************************
        ATTENZIONE. Per i commenti NON usare il doppio trattino !!!!
                    NON eliminare prefissi di schema !!!!
        ***************************************************************************
        Vista Personalizzata per ambiente REPLAY
     */
     MyVarStams AS (SELECT A.*
                      FROM VW_VAR_POTENZA_ELEMENTI@PKG1_STMAUI.IT A
                     INNER JOIN TIPI_MISURA B ON COD_TIPO_MISURA = 'PI'
                                             AND FLG_VAR_PI = 1
                   )
   , MyCorele   AS (SELECT COD_GEST, NOME, COD_ENTE, RIF_ELEM, ID_ELEM, CENTROSATELLITE, NODO_RIF
                      FROM (SELECT E.CODICE_ST
                                 , CASE WHEN PKG_Mago.IsRomaniaFL = 0
                                        THEN CASE WHEN E.ESE <> SUBSTR (E.COD_GEST, 3, 2) 
                                                    THEN E.COD_GEST || '_' || E.ESE 
                                                    ELSE E.COD_GEST
                                             END
                                        ELSE E.UT||E.ESE 
                                   END COD_GEST
                                 , E.NOME, E.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
                              FROM CORELE.ESERCIZI E
                             WHERE SYSDATE BETWEEN E.DATA_INIZIO AND E.DATA_FINE
                            UNION ALL
                            SELECT cp.CODICE_ST, cp.COD_GEST, cp.NOME, cp.COD_ENTE, cp.CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
                              FROM CORELE.IMPIANTIAT_SA cp
                             WHERE SYSDATE BETWEEN cp.DATA_INIZIO AND cp.DATA_FINE
                            UNION ALL
                            SELECT ta.CODICE_ST, ta.COD_GEST, ta.NOME, ta.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM
                                 , NULL NODO_RIF
                              FROM CORELE.TRASFORMATORIAT_SA ta
                             WHERE SYSDATE BETWEEN ta.DATA_INIZIO AND ta.DATA_FINE
                            UNION ALL
                            SELECT av.CODICE_ST, av.COD_GEST, NOME_TRASF NOME, av.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM
                                 , NULL NODO_RIF
                              FROM CORELE.AVVOLGIMENTI_SA av
                             WHERE av.COD_ENTE <> 'PRIM'
                               AND SYSDATE BETWEEN av.DATA_INIZIO AND av.DATA_FINE
                            UNION ALL
                            SELECT sb.CODICE_ST, sb.COD_GEST, sb.NOME, sb.COD_ENTE, NULL CENTROSATELLITE, cp.COD_GEST RIF_ELEM, NULL ID_ELEM
                                 , NULL NODO_RIF
                              FROM CORELE.SBARRE_SA sb
                                   INNER JOIN CORELE.IMPIANTIAT_SA cp
                                       ON cp.CODICE_ST = sb.CODICEST_IMPAT
                                      AND SYSDATE BETWEEN cp.DATA_INIZIO AND cp.DATA_FINE
                             WHERE SYSDATE BETWEEN sb.DATA_INIZIO AND sb.DATA_FINE
                               AND sb.TIPO = 'MT'
                            UNION ALL
                            SELECT mm.CODICE_ST, mm.COD_GEST, mm.NOME, mm.COD_ENTE, NULL CENTROSATELLITE
                                 , CASE mm.COD_GEST_DIRMT WHEN 'ND' THEN NULL ELSE mm.COD_GEST_DIRMT END RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
                              FROM CORELE.MONTANTIMT_SA mm
                             WHERE SYSDATE BETWEEN mm.DATA_INIZIO AND mm.DATA_FINE
                            UNION ALL
                            SELECT cs.CODICE_ST, cs.COD_GEST_SBARRA COD_GEST, cs.NOME, 'SBCS' COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM
                                 , cs.COD_GEST ID_ELEM, NULL NODO_RIF
                              FROM CORELE.IMPIANTIMT_SA cs
                             WHERE SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
                            UNION ALL
                            SELECT cm.CODICE_ST, cm.COD_GEST, cm.NOME, cm.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, cm.COD_GEST_SBARRA ID_ELEM
                                 , cs.COD_GEST NODO_RIF
                              FROM CORELE.CLIENTIMT_SA cm
                                   INNER JOIN CORELE.IMPIANTIMT_SA cs
                                       ON cs.CODICE_ST = cm.CODICEST_PADRE
                                      AND SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
                             WHERE SYSDATE BETWEEN cm.DATA_INIZIO AND cm.DATA_FINE
                            UNION ALL
                            SELECT tm.CODICE_ST, tm.COD_GEST, tm.NOME, tm.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, tm.COD_GEST_SBARRA ID_ELEM
                                 , cs.COD_GEST NODO_RIF
                              FROM CORELE.TRASFORMATORIBT_SA tm
                                   INNER JOIN CORELE.IMPIANTIMT_SA cs
                                       ON cs.CODICE_ST = tm.CODICEST_IMPMT
                                      AND SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
                             WHERE SYSDATE BETWEEN tm.DATA_INIZIO AND tm.DATA_FINE)
                   )
   , MyAngrBase AS (SELECT A.COD_GEST COD_GEST_ELEMENTO,
                           A.NOME NOME_ELEMENTO,
                           T.COD_TIPO_ELEMENTO  COD_TIPO_ELEMENTO,
                           CASE T.COD_TIPO_ELEMENTO
                             WHEN 'CPR' THEN CASE A.CENTROSATELLITE
                                                 WHEN 'SI' THEN 1
                                                           ELSE 0
                                             END
                             ELSE NULL
                           END FLAG,
                           TRIM (ID_ELEM) ID_ELEMENTO,
                           TRIM (RIF_ELEM) RIF_ELEMENTO
                      FROM MyCorele A
                     INNER JOIN TIPI_ELEMENTO T ON T.CODIFICA_ST = A.COD_ENTE
                     WHERE T.COD_TIPO_ELEMENTO <> 'ESE'
                   )
   , MyAngrAT   AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO IN ('CPR','TRF','TRS','TRT','LMT','SMT')
                   )
   , MyAngrSCS  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO = 'SCS'
                   )
   , MyAngrCLI  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO IN ('CMT','CAT','CBT')
                   )
   , MyAngrTRM  AS (SELECT *
                      FROM MyAngrBase A
                     WHERE A.COD_TIPO_ELEMENTO = 'TRM'
                   )
SELECT COD_GEST_ELEMENTO,
       NOME_ELEMENTO,
       COD_TIPO_ELEMENTO,
       CASE WHEN COD_TIPO_FONTE = ' ' THEN NULL /* trasformo blank in null */
                                      ELSE COD_TIPO_FONTE
       END COD_TIPO_FONTE,
       COD_TIPO_CLIENTE,
       ID_ELEMENTO,
       RIF_ELEMENTO,
       FLAG,
       NUM_IMPIANTI,
       POTENZA_INSTALLATA,
       POTENZA_CONTRATTUALE,
       FATTORE,
       COORDINATA_X,
       COORDINATA_Y
  FROM (/*CENTRO OPERATIVO */
        SELECT E.COD_GEST_ELEMENTO,
               D.NOME_ELEMENTO,
               D.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               NULL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               TO_NUMBER(NULL) FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM ELEMENTI E
         INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
         WHERE COD_ELEMENTO = PKG_Elementi.GetElementoBase
        UNION ALL /* ESERCIZI */
        SELECT E.COD_ESE COD_GEST_ELEMENTO,
               E.NOME NOME_ELEMENTO,
               'ESE' COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               NULL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               V.ESE_PRIMARIO FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM (SELECT CASE WHEN PKG_Mago.IsRomaniaFL = 0
                            THEN CASE
                                      WHEN SUBSTR(COD_GEST,3,2) = E.ESE THEN E.COD_GEST
                                      ELSE E.COD_GEST || '_' || E.ESE
                                 END
                            ELSE E.UT||E.ESE
                       END COD_ESE,
                       E.NOME,
                       E.COD_ENTE,
                       E.ESE
                  FROM CORELE.ESERCIZI E
                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
               ) E
         LEFT OUTER JOIN V_ESERCIZI V ON E.COD_ESE = V.GST_ESE
        UNION ALL /* ELEMENTI AT/MT ( CS Escluse )*/
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               A.RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrAT A
        UNION ALL /* SBARRE DI CABINA SECONDARIA */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               'X' COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               B.COD_CFT RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               TO_NUMBER(NULL) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               TO_NUMBER(NULL) FATTORE,
               B.GPS_X COORDINATA_X,
               B.GPS_Y COORDINATA_Y
          FROM MyAngrSCS A
          LEFT OUTER JOIN NODI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                   AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                   AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                   AND B.TRATTAMENTO  = 0
                                                   AND B.STATO        = 'E'
        UNION ALL /* CLIENTI */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               CASE TIPO_FORN
                    WHEN 'PP' THEN 'A' /* PP = Produttori Puri collegati e linee E.D. */
                    WHEN 'AP' THEN 'B' /* AP - AutoProduttori collegati e linee E.D. */
                    WHEN 'PD' THEN 'B' /* PD - Produttori collegati e linee E.D. - Introdotto per AUI Romania */
                    ELSE 'C'
               END COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               B.POD RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               B.POT_GRUPPI * NVL(VAR_POT_PROD,1) POTENZA_INSTALLATA,
               B.POT_DISP POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrCLI A
          LEFT OUTER JOIN CLIENTI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO  = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                      AND B.SER_NODO      = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                      AND B.NUM_NODO      = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                      AND B.TIPO_ELEMENTO = SUBSTR(A.COD_GEST_ELEMENTO,12,1)
                                                      AND B.ID_CLIENTE    = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                      AND B.TRATTAMENTO   = 0
                                                      AND B.STATO         = 'E'
          LEFT OUTER JOIN MyVarStams V ON V.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
        UNION ALL /* GENERATORI (clienti) */
        SELECT COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_GENERATORE COD_GEST_ELEMENTO,
               NULL NOME_ELEMENTO,
               CASE A.COD_TIPO_ELEMENTO
                    WHEN 'CAT' THEN 'GAT'
                    WHEN 'CMT' THEN 'GMT'
                    WHEN 'CBT' THEN 'GBT'
               END COD_TIPO_ELEMENTO,
               CASE WHEN B.TIPO_IMP = ' ' 
                        THEN NULL
                        ELSE B.TIPO_IMP
               END COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               B.COD_ORG_NODO||B.SER_NODO||B.NUM_NODO||'U'||B.ID_CL ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               TO_NUMBER(NULL) FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               (B.P_APP_NOM * NVL(B.N_GEN_PAR,1)) * NVL(VAR_POT_PROD,1) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               B.F_P_NOM FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrCLI A
          LEFT OUTER JOIN GENERATORI_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                         AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                         AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                         AND B.ID_CL        = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                         AND B.TRATTAMENTO  = 0
                                                         AND B.STATO        = 'E'
          LEFT OUTER JOIN MyVarStams V ON V.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
        UNION ALL /* TRASFORMATORI MT/BT */
        SELECT A.COD_GEST_ELEMENTO,
               A.NOME_ELEMENTO,
               A.COD_TIPO_ELEMENTO,
               NULL COD_TIPO_FONTE,
               'X' COD_TIPO_CLIENTE,
               A.ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               A.FLAG,
               TO_NUMBER(NULL) NUM_IMPIANTI,
               SUM(B.POT_PROD) * NVL(VAR_POT_PROD,1) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrTRM A
          LEFT OUTER JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO,1,4)
                                                            AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,5,1)
                                                            AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO,6,6)
                                                            AND B.TIPO_ELE     = SUBSTR(A.COD_GEST_ELEMENTO,12,1)
                                                            AND B.ID_TRASF     = SUBSTR(A.COD_GEST_ELEMENTO,13,2)
                                                            AND B.TRATTAMENTO  = 0
                                                            AND B.STATO        = 'E'
          LEFT OUTER JOIN MyVarStams V ON V.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
         GROUP BY A.COD_GEST_ELEMENTO,A.NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,A.ID_ELEMENTO,A.FLAG,V.POT_PROD,V.VAR_POT_PROD,V.VAR_POT_DISP
        UNION ALL /* GENERATORI FITTIZI DI TRASFORMATORI MT/BT */
        SELECT A.COD_GEST_ELEMENTO||'_'|| NVL(TIPO_GEN,'3') COD_GEST_ELEMENTO,
               NULL NOME_ELEMENTO,
               'TRX' COD_TIPO_ELEMENTO,
               NVL(B.TIPO_GEN,'3') COD_TIPO_FONTE,
               NULL COD_TIPO_CLIENTE,
               A.COD_GEST_ELEMENTO ID_ELEMENTO,
               NULL RIF_ELEMENTO,
               NULL FLAG,
               NVL(B.NUM_PROD,0) NUM_IMPIANTI,
               NVL(B.POT_PROD,0) * NVL(VAR_POT_PROD,1) POTENZA_INSTALLATA,
               TO_NUMBER(NULL) POTENZA_CONTRATTUALE,
               1 FATTORE,
               TO_NUMBER(NULL) COORDINATA_X,
               TO_NUMBER(NULL) COORDINATA_Y
          FROM MyAngrTRM A
          LEFT OUTER JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT B ON B.COD_ORG_NODO = SUBSTR(A.COD_GEST_ELEMENTO ,1,4)
                                                            AND B.SER_NODO     = SUBSTR(A.COD_GEST_ELEMENTO ,5,1)
                                                            AND B.NUM_NODO     = SUBSTR(A.COD_GEST_ELEMENTO ,6,6)
                                                            AND B.TIPO_ELE     = SUBSTR(A.COD_GEST_ELEMENTO ,12,1)
                                                            AND B.ID_TRASF     = SUBSTR(A.COD_GEST_ELEMENTO ,13,2)
                                                            AND B.TRATTAMENTO  = 0
                                                            AND B.STATO        = 'E'
          LEFT OUTER JOIN MyVarStams V ON V.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
       )
 ORDER BY PKG_Elementi.GetOrderByElemType(COD_TIPO_ELEMENTO,COD_GEST_ELEMENTO),
       CASE SUBSTR(COD_GEST_ELEMENTO,1,1)
                               WHEN 'D' THEN 1
                               WHEN 'A' THEN 2
                                        ELSE 3
       END,
       CASE COD_TIPO_ELEMENTO||FLAG
            WHEN 'ESE1' THEN 0
            WHEN 'ESE0' THEN 1
            WHEN 'CPR0' THEN 0
            WHEN 'CPR1' THEN 1
       END,
       COD_GEST_ELEMENTO
/
