PROMPT PACKAGE PKG_REPLAY;
--
-- PKG_REPLAY  (Package) 
--
--  Dependencies: 
--   DBMS_MVIEW ()
--   DBMS_MVIEW (Synonym)
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   DEFAULT_CO (Table)
--   DUAL ()
--   DUAL (Synonym)
--   LOG_HISTORY (Table)
--   MISURE_ACQUISITE (Table)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_SCHEDULER (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Package)
--   PKG_UTLGLB (Synonym)
--   PLITBLM ()
--   PLITBLM (Synonym)
--   SCHEDULED_JOBS (Table)
--   SCHEDULED_TMP_GEN (Table)
--   STANDARD (Package)
--   SYS_PLSQL_295453_35_2 (Type)
--   TIPI_RETE (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   USER_CONSTRAINTS ()
--   USER_CONSTRAINTS (Synonym)
--   USER_CONS_COLUMNS ()
--   USER_CONS_COLUMNS (Synonym)
--   USER_SEQUENCES ()
--   USER_SEQUENCES (Synonym)
--   USER_SEQUENCES (View)
--   USER_SNAPSHOTS ()
--   USER_SNAPSHOTS (Synonym)
--   USER_TAB_COLS ()
--   USER_TAB_COLS (Synonym)
--   V_CURRENT_VERSION (View)
--
CREATE OR REPLACE PACKAGE PKG_REPLAY AS
/********************************************************************************************************************
   NAME:       PKG_REPLAY
   PURPOSE:    Servizi per la gestione di CREL in ambiente REPLAY

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.0     23/06/2015  Moretti C.       Definizione ambiente REPLAY
   1.9c.1     04/08/2015  Moretti C.       Ottimizzazione import da MAGO Esercizio
   1.9c.2     20/08/2015  Moretti C.       Cambio politica gestione import ed aggregazione Misure Statiche
              24/08/2015  Moretti C.       Genera schedulazioni solo per Gerarchia Elettrica di stato attuale 
                                                    (NO stato Normale - NO gerarchie Istat e Amministrativa
              25/08/2015  Moretti C.       Definizione funzione di inizializzazione
              07/09/2015  Campi P.         Modulazione misure in base a Potenza Installata e Potenza Impegnata

*********************************************************************************************************************/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

    gcDefaultGiorni         CONSTANT NUMBER := 3;
    gcInputDateFormat       CONSTANT VARCHAR2(16) := 'yyyymmddhh24miss'; 

 -------------------------------------------------------------------------------------------------------------

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 FUNCTION  Inizializza  (pDataStart   IN VARCHAR2 DEFAULT NULL,
                         pDataFine    IN VARCHAR2 DEFAULT NULL) 
                  RETURN PKG_Mago.t_RetArea;

 FUNCTION  LoadByDate   (pDataStart   IN VARCHAR2,
                         pGiorni      IN NUMBER DEFAULT gcDefaultGiorni) 
                  RETURN PKG_Mago.t_RetArea;
                                     
 FUNCTION  LoadByPeriod (pDataStart   IN VARCHAR2,
                         pDataFine    IN VARCHAR2) 
                  RETURN PKG_Mago.t_RetArea;
                                     
 -------------------------------------------------------------------------------------------------------------

END PKG_REPLAY;
/
SHOW ERRORS;


