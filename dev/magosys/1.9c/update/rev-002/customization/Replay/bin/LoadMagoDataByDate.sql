/*----------------------------------------------------------------------
  Importa, a partire dalla data indicata i dati anadrafici e le misure
  relative al periodo Data + N (N giorni - default 3)
*/----------------------------------------------------------------------
SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET LINE 200

WHENEVER SQLERROR EXIT 99
WHENEVER OSERROR  EXIT 98;

var exit_val NUMBER;
EXECUTE :exit_val := 0;

var data_val VARCHAR2(14);
EXECUTE :data_val := '&1';

conn mago/mago@arcdb2

SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET TIMING ON
SET LINE 200

PROMPT 
PROMPT ********************************************************************************
PROMPT 
PROMPT COPIA DATI DA ESERCIZIO
PROMPT 
PROMPT ********************************************************************************
PROMPT 

PROMPT Abilita il calcolo delle statistiche di Database (SCHEMA MAGO)
EXEC DBMS_STATS.UNLOCK_SCHEMA_STATS (USER);

PROMPT
PROMPT ================================================================================
PROMPT
PROMPT Load Dati da Esercizio - data '&1'
PROMPT

DECLARE
    vEsito         PKG_Mago.t_RetArea;
	vGiorniPeriodo INTEGER := 3;
BEGIN
    vEsito := PKG_Replay.LoadByDate(:data_val,vGiorniPeriodo);
    IF vEsito.rc < 0 THEN
        :exit_val := 99;
    ELSE
        :exit_val := 140;
    END IF;
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    :exit_val := 99;
END;
/

PROMPT
PROMPT

conn corele/corele@arcdb2

SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET TIMING ON
SET LINE 200

PROMPT Abilita il calcolo delle statistiche di Database (SCHEMA CORELE)
EXEC DBMS_STATS.UNLOCK_SCHEMA_STATS (USER);

PROMPT abilita il caricamento dei dati provenienti da ST
DELETE SEMAFORO;
INSERT INTO SEMAFORO (VALORE, DATA) VALUES(0, NULL);
COMMIT;

EXIT :exit_val

