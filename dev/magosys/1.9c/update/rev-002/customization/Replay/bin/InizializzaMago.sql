/*----------------------------------------------------------------------
  Inizializza Ambiente Mago
*/----------------------------------------------------------------------

WHENEVER SQLERROR EXIT 99
WHENEVER OSERROR  EXIT 98;

PROMPT 
PROMPT ********************************************************************************
PROMPT 
PROMPT INIZIALIZZAZIONE MAGO 
PROMPT 
PROMPT ********************************************************************************
PROMPT 

var exit_val NUMBER;
EXECUTE :exit_val := 0;

var data_Da VARCHAR2(14);
EXECUTE :data_Da := NULL;

var data_A VARCHAR2(14);
EXECUTE :data_A := NULL;

conn corele/corele@arcdb2
SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET TIMING ON
SET LINE 200

PROMPT Disabilita il caricamento dei dati provenienti da ST (Semaforo = 9)
DELETE SEMAFORO;
INSERT INTO SEMAFORO (VALORE, DATA) VALUES(9, NULL);
COMMIT;

PROMPT Disabilita calcolo delle statistiche di Database (SCHEMA CORELE)
EXEC DBMS_STATS.LOCK_SCHEMA_STATS (USER);

PROMPT
PROMPT

conn mago/mago@arcdb2
SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET TIMING ON
SET LINE 200

PROMPT disabilita calcolo delle statistiche di database (SCHEMA MAGO)
EXEC DBMS_STATS.LOCK_SCHEMA_STATS (USER);

PROMPT
PROMPT ================================================================================
PROMPT
PROMPT Inizializzazione MAGO  
PROMPT

DECLARE
    vEsito PKG_Mago.t_RetArea;
BEGIN
    vEsito := PKG_Replay.Inizializza(:data_Da,:data_A);
    IF vEsito.rc < 0 THEN
        :exit_val := 99;
    ELSE
        :exit_val := 140;
    END IF;
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    :exit_val := 99;
END;
/

EXIT :exit_val

