SPOOL rel_1.9c.2_NAZ.log

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_1.9c.2_NAZ
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/TIPI_MISURA.sql
@./Tables/SESSION_STATISTICS.sql

PROMPT _______________________________________________________________________________________
PROMPT Eliminazione Package MAGONAZ.PKG_ANAGRAFICHE
PROMPT

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP PACKAGE BODY MAGONAZ.PKG_ANAGRAFICHE';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -04043  -- ORA-04043: l'oggetto ... non esiste  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP PACKAGE MAGONAZ.PKG_ANAGRAFICHE';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -04043  -- ORA-04043: l'oggetto ... non esiste  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
END;
/
     
PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MISURE.sql
@./Packages/PKG_MANUTENZIONE.sql
@./Packages/PKG_STATS.sql
@./Packages/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_CURRENT_VERSION.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_STATS.sql
@./PackageBodies/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/TIPI_MISURA.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.2 NAZ
PROMPT

spool off
