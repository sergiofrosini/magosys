SPOOL rel_1.9c.2_ROM.log

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_1.9c.2_ROM
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn mago/mago

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/TIPI_MISURA.sql
@./Tables/SESSION_STATISTICS.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MISURE.sql
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_MANUTENZIONE.sql
@./Packages/PKG_STATS.sql
@./Packages/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_CURRENT_VERSION.sql
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql
@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_STATS.sql
@./PackageBodies/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
@./InitTables/METEO_REL_ISTAT.sql
@./InitTables/TIPI_MISURA.sql
@./InitTables/GRUPPI_MISURA.sql
@./InitTables/METEO_JOB_STATIC_CONFIG.sql


PROMPT ***************************************************************************************
PROMPT START!  Ricalcolo gerarchie (modificata vista V_GERARCHIA_AMMINISTRATIVA) 
PROMPT ***************************************************************************************
BEGIN
    PKG_Logs.StdLogInit (pClasseFunz => PKG_Mago.gcJobClassANA,
                         pFunzione   => '** Ricalcolo forzato **');
    PKG_Logs.StdLogPrint;
    FOR i IN (SELECT DATA_IMPORT, 
                     CASE TIPO
                        WHEN 'SN'  THEN PKG_Mago.gcStatoNormale 
                        WHEN 'SA'  THEN PKG_Mago.gcStatoAttuale 
                        WHEN 'NEA' THEN PKG_Mago.gcStatoAttuale 
                     END TipoRete,
                     CASE TIPO
                        WHEN 'NEA' THEN 1
                                   ELSE 0 
                     END Nea
                FROM STORICO_IMPORT
               WHERE (DATA_IMPORT = (SELECT MAX(DATA_IMPORT) FROM STORICO_IMPORT WHERE TIPO = 'SN') AND TIPO = 'SN')
                  OR (DATA_IMPORT = (SELECT MAX(DATA_IMPORT) FROM STORICO_IMPORT WHERE TIPO IN ('SA','NEA')) AND TIPO IN ('SA','NEA'))
             ) LOOP
        PKG_anagrafiche.ElaboraGerarchia(i.DATA_IMPORT,i.TipoRete,PKG_UTLGLB.FlagToBoolean(i.Nea),FALSE,TRUE,TRUE);
        COMMIT;
    END LOOP;
    COMMIT;
    PKG_Logs.StdLogPrint;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogInit (pClasseFunz => PKG_Mago.gcJobClassANA,
                              pFunzione   => '** Ricalcolo forzato **');
         PKG_Logs.StdLogAddTxt(SQLERRM || CHR(10) || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;    
END;
/
PROMPT ***************************************************************************************
PROMPT END!    Ricalcolo gerarchie (modificata vista V_GERARCHIA_AMMINISTRATIVA) 
PROMPT ***************************************************************************************



PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.2 STM
PROMPT

spool off
