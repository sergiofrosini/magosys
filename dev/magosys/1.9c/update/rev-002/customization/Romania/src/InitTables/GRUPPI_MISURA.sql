PROMPT 
PROMPT Inizializzazione GRUPPI_MISURA
PROMPT 

DELETE GRUPPI_MISURA 
 WHERE COD_TIPO_MISURA LIKE '%.P1';

DELETE GRUPPI_MISURA 
 WHERE COD_TIPO_MISURA IN ('PAG',        -- Potenza Attiva Generata
                           'PCT-P',      -- Previsione Carico Totale-P
						   'PCT-Q',      -- Previsione Carico Totale-Q
						   'CC-P',       -- Carico Consolidato-P
						   'CC-Q',       -- Carico Consolidato-Q
						   'PAG',        -- Potenza attiva generata
						   'PRI',        -- Potenza Reattiva Induttiva
						   'PAGS',       -- Potenza attiva generata allo scambio
						   'PAAS',       -- Potenza attiva generata allo scambio
						   'PPATMP',     -- Previsione della potenza attiva Transitante
						   'PAS-TR',     -- Potenza Attiva Scambiata - Trasformatore
						   'PRI-TR',     -- Potenza Reattiva Induttiva - Trasformatore
						   'PAT',        -- Potenza Attiva Transitante
						   'PPATCT',     -- Previsione della potenza attiva Transitante Cielo Terso
						   'PPATMP'      -- Previsione della potenza attiva Transitante
						  );

COMMIT;




