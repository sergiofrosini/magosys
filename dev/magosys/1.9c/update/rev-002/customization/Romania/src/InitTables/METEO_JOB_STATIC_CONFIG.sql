MERGE INTO METEO_JOB_STATIC_CONFIG A USING
 (SELECT 'MFM.supplier.suffix.available.hour.second.1' as KEY, '08' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.download.useURL.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.download.URL.1' as KEY, 'http://www.ilmeteo.it/clienti/siemens/xml.zip' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.suffix.available.hour.first.1' as KEY, '20' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.ftp.timeout.connection' as KEY, '300' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.ftp.timeout.read' as KEY, '300' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.ftp.timeout.close' as KEY, '300' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.ftp.use.for.finaltrasfer' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'AUI.remote.log.filename' as KEY, 'Meteo_AAMM.log' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.suffix.folder.extraction' as KEY, 'EX' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.suffix.folder.elaboration' as KEY, 'EL' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.local.mantaining.day' as KEY, '30' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.local.download.retry' as KEY, '3' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.local.recovery.days' as KEY, '7' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.forward.hour' as KEY, '72' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.sampling.minutes' as KEY, '60' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.enable.producer' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.enable.transformator' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.enable.generator' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.enable.clearsky' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.enable.meteo.acquired' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.export.exportAtProducerLevel' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.export.exportAtGeneratorLevel' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.export.exportAtTransformerLevel' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.prediction.export.ftpRetryDelay' as KEY, '60000' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.prediction.start.hour.firstsuffix' as KEY, '20' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.prediction.start.hour.secondsuffix' as KEY, '08' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.prediction.days.to.save' as KEY, '4' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.prediction.start.day' as KEY, '0' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.types.network' as KEY, 'M' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.types.producer' as KEY, 'A' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.types.sources' as KEY, 'S|E' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.ws.wsdl.url' as KEY, 'http://localhost:80/STWebRegistry-WS/registry?wsdl' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.measure.week.number' as KEY, '4' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.measure.interval.days' as KEY, '7' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.force' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.sampling.minutes' as KEY, '60' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.involvedMeasureType' as KEY, 'PAS' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.enable.producer' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.enable.transformator' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.estimation.enable.generator' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.types.network' as KEY, 'A|M|B' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.types.producer' as KEY, 'A|B|X|Z' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.types.sources' as KEY, 'S|E|I|T|R|C' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.types.eolic.generator' as KEY, 'CAT|CMT|CBT|GAT|GMT|GBT' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.thread.number' as KEY, '5' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.recovery.enabled' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MDS.statistics.enabled' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.enabled' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.customer.enabled' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.transformer.enabled' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.types.network' as KEY, 'A|M|B' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.types.producer' as KEY, 'A|B|X|Z|C' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.types.sources' as KEY, 'S|E|I|T|R|C|3' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.stardard.forward.hour' as KEY, '24' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.install.forward.hour' as KEY, '72' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.conventional.name.1' as KEY, 'Ilmeteo' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.download.useFtp.1' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.download.useSftp.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.upload.useSftp.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.port.1' as KEY, '21' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.host.1' as KEY, 'dastc.siemens.it' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.username.1' as KEY, 'magosys' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.password.1' as KEY, 'MagoSystem2012' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.useProxy.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.proxyHost.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.proxyPort.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.proxyUser.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.ftp.source.proxyPass.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.folder.source.path.1' as KEY, '/MAGOsys/METEO/Rel-Root/ROMANIA' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.local.destination.path.1' as KEY, './tmp1' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.remote.mantaining.day.1' as KEY, '30' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.enabled.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.forward.hour.1' as KEY, '48' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.host.1' as KEY, '127.0.0.1' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.username.1' as KEY, 'ftpuser' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.password.1' as KEY, 'ftpuser' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.path.1' as KEY, '/1' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.useProxy.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.proxy.host.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.proxy.port.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.proxy.username.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.ftp.proxy.password.1' as KEY, NULL as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.supplier.prediction.export.daysToKeepZipFile.1' as KEY, '30' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.point.type.1' as KEY, 'c' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.forecast.curve.name.1' as KEY, 'PMP' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.forecast.clearsky.curve.name.1' as KEY, 'PMC' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.suffix.zip.file.extension.1' as KEY, '.zip' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.suffix.zip.file.match.first.1' as KEY, '_01_xml' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.suffix.zip.file.match.second.1' as KEY, '_00_xml' as VALUE FROM DUAL UNION ALL
  SELECT 'MFM.supplier.national.skip.elaboration.1' as KEY, 'false' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.recovery.day.amount' as KEY, '3' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.recovery.enable' as KEY, 'true' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.elaboration.chunk.size' as KEY, '500' as VALUE FROM DUAL UNION ALL
  SELECT 'MPS.load.forecast.elaboration.default.forward.days' as KEY, '2' as VALUE FROM DUAL
 ) B  ON (A.KEY = B.KEY)
WHEN NOT MATCHED THEN  INSERT (KEY, VALUE)
                       VALUES (B.KEY, B.VALUE)
WHEN MATCHED THEN UPDATE SET A.VALUE = B.VALUE;

COMMIT;
