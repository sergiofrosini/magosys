SET LINES 200
SET VERIFY OFF

COLUMN REP_ELETIP NEW_VALUE REP_ELETIP
COLUMN REP_ESEGST NEW_VALUE REP_ESEGST
COLUMN REP_ESENOM NEW_VALUE REP_ESENOM
COLUMN REP_STADES NEW_VALUE REP_STADES
COLUMN REP_STATO  NEW_VALUE REP_STATO
COLUMN LINE_SEP   NEW_VALUE LINE_SEP

COLUMN SPOOLNAME1 NEW_VALUE SPOOLNAME1
COLUMN SPOOLNAME2 NEW_VALUE SPOOLNAME2

ACCEPT TIPO_FONTE CHAR FORMAT 'A1'    PROMPT 'Specifica la fonte produttori (S,E,I,T,C,R - *=Tutte)'
ACCEPT GEST_ESE   CHAR FORMAT 'A14'   PROMPT 'Codice Gestionale Esercizio)'
ACCEPT STATO_RETE CHAR FORMAT INTEGER PROMPT 'Stato rete (1=Normale - 2=Attuale)'
ACCEPT PROD_ONLY  CHAR FORMAT 'A1'    PROMPT 'Solo Produttori (N=Tutti, S=Produttori)' DEFAULT 'N'

SELECT DESCRIZIONE          REP_ELETIP, 
       GST_ESE              REP_ESEGST, 
       NOM_ESE              REP_ESENOM,
       CASE 
         WHEN STATO = 1 
           THEN 'Stato Normale'
           ELSE 'Stato Attuale'
       END                  REP_STADES,
       STATO                REP_STATO,
       COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_ListaClienti.txt' SPOOLNAME1, 
       COD_GEST_ELEMENTO||'-'||NOME_ELEMENTO||'_ListaClienti.csv' SPOOLNAME2,
       RPAD('=',150,'=') LINE_SEP 
  FROM (SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO, DESCRIZIONE, GST_ESE, NOM_ESE, &STATO_RETE STATO 
          FROM ELEMENTI E
         INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
         INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO
         INNER JOIN V_ESERCIZI V ON V.GST_ESE = E.COD_GEST_ELEMENTO
         WHERE E.COD_GEST_ELEMENTO = '&GEST_ESE'
           AND E.COD_TIPO_ELEMENTO = 'ESE'
           AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
       );

CREATE OR REPLACE FORCE VIEW TMP_LISTA_CLIENTI_VIEW
AS 
SELECT PRODUTTORE, NOME, IN_AUI, TIPO_FORN, FONTI, POT_GRUPPI, NUM_GEN, PI, POD,
       REPLACE(REPLACE(TRIM(BOTH '/' FROM NOTA2||'/'||NOTA3||'/'||NOTA1),'//','/'),'/',' / ') NOTE
  FROM (SELECT PRODUTTORE, NOME, IN_AUI, TIPO_FORN, FONTI, POT_GRUPPI, NUM_GEN, PI, POD,
               NOTA1,
               CASE WHEN FL_PROD = 1 THEN DECODE (NVL(NUM_GEN,-99),-99,'Nessun generatore definito',NULL) END NOTA2,
               CASE WHEN POD IS NULL THEN 'POD non specificato'
                    WHEN NUM_POD > 1 THEN 'POD duplicato'
               END NOTA3,
               FL_PROD
          FROM (SELECT SUBSTR(A.COD_GEST_ELEMENTO,1,15) PRODUTTORE, NOME_ELEMENTO NOME,
                       CASE 
                          WHEN B.COD_ORG_NODO IS NOT NULL THEN ' Si    '
                                                          ELSE '    No '
                       END IN_AUI,
                       TIPO_FORN, POT_GRUPPI, FONTI, NUM_GEN, NOTA1, PI, POD, NUM_POD, FL_PROD
                  FROM (SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO
                          FROM TABLE(PKG_ELEMENTI.LEGGIGERARCHIAINF(PKG_ELEMENTI.GETCODELEMENTO('&REP_ESEGST'),1,&REP_STATO,SYSDATE)) A
                         INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                          LEFT OUTER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = 'PMT' 
                         WHERE A.COD_TIPO_ELEMENTO = 'CMT'
                           AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                        ) A
                  LEFT OUTER JOIN 
                        (SELECT COD_ORG_NODO,SER_NODO,NUM_NODO,TIPO_ELEMENTO,ID_CLIENTE,
                                COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_CLIENTE COD_GEST_ELEMENTO,
                                TIPO_FORN, POT_GRUPPI, POD, COUNT(*) OVER(PARTITION BY POD) NUM_POD,
                                CASE WHEN TIPO_FORN IN ('AP','PP','PD') THEN 1 END FL_PROD
                           FROM CLIENTI_TLC@PKG1_STMAUI.IT 
                          WHERE STATO = 'E'
                            AND TRATTAMENTO = 0
                        ) B ON A.COD_GEST_ELEMENTO = B.COD_GEST_ELEMENTO
                  LEFT OUTER JOIN 
                        (SELECT COD_ORG_NODO,SER_NODO,NUM_NODO,ID_CL ID_CLIENTE,
                                COUNT(*) NUM_GEN,
                                SUM((P_APP_NOM * NVL(N_GEN_PAR,1) * NVL(F_P_NOM,1))) PI
                           FROM GENERATORI_TLC@PKG1_STMAUI.IT 
                          WHERE STATO = 'E'
                            AND TRATTAMENTO = 0
                          GROUP BY COD_ORG_NODO,SER_NODO,NUM_NODO,ID_CL
                        ) C ON C.COD_ORG_NODO = B.COD_ORG_NODO
                           AND C.SER_NODO = B.SER_NODO
                           AND C.NUM_NODO = B.NUM_NODO
                           AND C.ID_CLIENTE = B.ID_CLIENTE
                     LEFT OUTER JOIN 
                        (SELECT COD_GEST_ELEMENTO,SUBSTR(FONTI,1,LENGTH(FONTI) -1) FONTI, NOTA1
                           FROM (SELECT COD_GEST_ELEMENTO,
                                        CASE WHEN BITAND(BITMAP_FONTI,1)  =  1 THEN 'S ' ELSE '  ' END||
                                        CASE WHEN BITAND(BITMAP_FONTI,2)  =  2 THEN 'E ' ELSE '  ' END||
                                        CASE WHEN BITAND(BITMAP_FONTI,4)  =  4 THEN 'I ' ELSE '  ' END||
                                        CASE WHEN BITAND(BITMAP_FONTI,8)  =  8 THEN 'T ' ELSE '  ' END||
                                        CASE WHEN BITAND(BITMAP_FONTI,16) = 16 THEN 'R ' ELSE '  ' END||
                                        CASE WHEN BITAND(BITMAP_FONTI,32) = 32 THEN 'C ' ELSE '  ' END FONTI,
                                        CASE TIPO_FONTE
                                             WHEN '1' THEN 'Fonti non Omogenee'
                                             ELSE NULL
                                        END NOTA1
                                   FROM (SELECT COD_GEST_ELEMENTO, 
                                                CASE
                                                  WHEN MAX(COD_TIPO_FONTE) IS NULL OR
                                                       MIN(COD_TIPO_FONTE) IS NULL THEN NULL
                                                  WHEN MAX(COD_TIPO_FONTE) = MIN(COD_TIPO_FONTE) 
                                                      THEN MAX(COD_TIPO_FONTE)
                                                      ELSE '1'
                                                END TIPO_FONTE,
                                                COUNT(*) NUM_FONTI,
                                                BITMAP_FONTI
                                          FROM (SELECT COD_GEST_ELEMENTO, COD_TIPO_FONTE,
                                                       SUM(ID_RAGGR_FONTE) OVER (PARTITION BY COD_GEST_ELEMENTO) BITMAP_FONTI
                                                  FROM (SELECT DISTINCT F.COD_RAGGR_FONTE COD_TIPO_FONTE, ID_ELEMENTO COD_GEST_ELEMENTO
                                                          FROM ELEMENTI A
                                                         INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
                                                         INNER JOIN TIPO_FONTI F ON F.COD_TIPO_FONTE = D.COD_TIPO_FONTE
                                                         WHERE SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE 
                                                           AND A.COD_TIPO_ELEMENTO = 'GMT'
                                                           AND ( INSTR('&TIPO_FONTE',D.COD_TIPO_FONTE) > 0 OR '&TIPO_FONTE' = '*')
                                                       )
                                                 INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE) 
                                                 INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE)
                                               )
                                          GROUP BY COD_GEST_ELEMENTO, BITMAP_FONTI
                                        )
                                )
                        ) D ON A.COD_GEST_ELEMENTO = D.COD_GEST_ELEMENTO
               )
       )
 WHERE CASE WHEN UPPER('&PROD_ONLY') <> 'S' 
                  THEN 1
                  ELSE NVL(FL_PROD,0)
       END = 1
 ORDER BY FL_PROD NULLS LAST,PRODUTTORE;

SET pages0

SPOOL '&SPOOLNAME2'

SELECT 'PRODUTTORE;NOME;IN_AUI;TIPO_FORN;FONTI;POT_GRUPPI;NUM_GEN;PI;POD;NOTE' AS " "
  FROM DUAL 
UNION ALL
SELECT PRODUTTORE||';'||NOME||';'||TRIM(IN_AUI)||';'||TIPO_FORN||';'||TRIM(FONTI)||';'||POT_GRUPPI||';'||NUM_GEN||';'||PI||';'||POD||';'||NOTE 
  FROM TMP_LISTA_CLIENTI_VIEW;

SPOOL OFF
       
COL PRODUTTORE FOR A14
COL NOME       FOR A15
COL FONTI      FOR A13
COL TIPO_FORN  FOR A4      HEADING 'TIPO|FORN'
COL POT_GRUPPI FOR   99990 HEADING 'POT|GRUPPI'
COL NUM_GEN    FOR     990 HEADING 'NUM|GEN'
COL PI         FOR 9999990 HEADING 'POT INST|KW'
COL NOTE       FOR A55

SET PAGES 9999

SPOOL '&SPOOLNAME1'
      
TTITLE LINE_SEP SKIP 1 -
       REP_ESEGST "-" REP_ESENOM "      " SKIP 1 -  
       LINE_SEP SKIP 1 -
       "Lista clienti/produttori sottesi a    " REP_ELETIP "  " REP_ESEGST " " REP_ESENOM "   " REP_STADES SKIP 1 -
       LINE_SEP SKIP 1;

BREAK ON REPORT;
COMPUTE SUM LABEL "Totale PI" OF PI ON REPORT;

SELECT * FROM TMP_LISTA_CLIENTI_VIEW;

SPOOL OFF
       
DROP VIEW TMP_LISTA_CLIENTI_VIEW;
 
EXIT;

        
        
        