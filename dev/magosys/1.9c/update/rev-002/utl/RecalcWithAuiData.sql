
/*
Aggiorna l'anagrafica e le strutture MAGO con i dati provenienti da AUI
*/

conn mago/mago

SET SERVEROUTPUT ON SIZE UNLIMITED
SET TIMING ON

ALTER SESSION SET NLS_DATE_FORMAT='dd/mm/yyyy hh24:mi:ss';

DECLARE
  vData DATE := SYSDATE;
BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'Script.AcqDatiAui',
                          pDataRif     => vData,
                          pStoreOnFile => FALSE);
    PKG_Anagrafiche.ElaboraGerarchia(vData,PKG_Mago.gcStatoAttuale,FALSE,TRUE,TRUE,FALSE);
    PKG_Logs.StdLogPrint;

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'Script.AcqDatiAui',
                          pDataRif     => vData,
                          pStoreOnFile => FALSE);
    PKG_Anagrafiche.ElaboraGerarchia(vData,PKG_Mago.gcStatoNormale,FALSE,TRUE,TRUE,FALSE);
    PKG_Logs.StdLogPrint;

    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt(SQLERRM || CHR(10) || DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END;
/

exit;

