SPOOL &3 APPEND

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_19c.7_SRC
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sys_dba as sysdba

COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;
SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;


conn mago/mago

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

-- PROMPT _______________________________________________________________________________________
PROMPT Tables
@./Tables/GTTD_KPI_MISURE.sql
@./Tables/KPI_INDICI_&ORATYPE.sql

-- PROMPT _______________________________________________________________________________________
PROMPT Init Tables
@./InitTables/KPI_INDICI.sql

-- PROMPT _______________________________________________________________________________________
PROMPT Packages
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_KPI.sql

-- PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_KPI.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.7 SRC
PROMPT

spool off
