Prompt Insert Table KPI_INDICI;

DECLARE

v_conto number;

BEGIN

SELECT COUNT(*) INTO v_conto from user_tables where table_name='OLD_KPI_INDICI';

IF v_conto > 0 then

	MERGE INTO KPI_INDICI t
		 USING (
				select distinct cod_trattamento_elem,r.lista_tipi_misura_forecast, i.risoluzione,i.data,i.valore  
				from old_kpi_indici i
				join trattamento_elementi t using (cod_trattamento_elem)
				INNER JOIN KPI_PARAMETRI P ON P.TIPO = 'TIPKPI'
				join KPI_RICHIESTA R ON P.ID_REQ_KPI = r.ID_REQ_KPI
				AND i.RISOLUZIONE = R.RISOLUZIONE
				AND i.DATA BETWEEN R.PERIODO_INIZIO
				AND R.PERIODO_FINE
				AND TO_CHAR(i.DATA,'hh24:mi') BETWEEN R.ORARIO_INIZIO
				AND R.ORARIO_FINE
				and p.ID_REQ_KPI = R.ID_REQ_KPI
			  ) s
			ON (t.cod_trattamento_elem =s.cod_trattamento_elem AND t.tipo_curva = s.lista_tipi_misura_forecast)
		  WHEN MATCHED THEN
		UPDATE SET 
					t.risoluzione = s.risoluzione,
					t.data = s.data,
					t.valore= s.valore
		  WHEN NOT MATCHED THEN 
		INSERT   
			  (cod_trattamento_elem,tipo_curva, risoluzione,data,valore) 
		VALUES (s.cod_trattamento_elem,s.lista_tipi_misura_forecast, s.risoluzione,s.data,s.valore) ;
END IF;
END;
/