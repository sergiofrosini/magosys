PROMPT PACKAGE PKG_ANAGRAFICHE;
--
-- PKG_ANAGRAFICHE  (Package) 
--
--  Dependencies: 
--   CLIENTI_TLC (Table)
--   DATE_IMPORT_CONSISTENZA (Table)
--   DBMS_LOCK ()
--   DBMS_LOCK (Synonym)
--   DBMS_MVIEW ()
--   DBMS_MVIEW (Synonym)
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_SCHEDULER ()
--   DBMS_SCHEDULER (Synonym)
--   DBMS_STANDARD (Package)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   DEFAULT_CO (Table)
--   DUAL ()
--   DUAL (Synonym)
--   ELEMENTI (Table)
--   ELEMENTI_CFG (Table)
--   ELEMENTI_DEF (Table)
--   ESERCIZI (Table)
--   GENERATORI_TLC (Table)
--   GTTD_IMPORT_GERARCHIA (Table)
--   GTTD_MOD_ASSETTO_RETE_SA (Table)
--   GTTD_VALORI_TEMP (Table)
--   MISURE_AGGREGATE (Table)
--   NEA_INTCLI (Table)
--   NODI_TLC (Table)
--   PKG_AGGREGAZIONI (Package)
--   PKG_ELEMENTI (Package)
--   PKG_GENERA_FILE_GEO (Package)
--   PKG_GESTANAGR ()
--   PKG_GESTANAGR (Synonym)
--   PKG_LOCALIZZA_GEO (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_SCHEDULER (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   REL_ELEMENTI_AMM (Table)
--   REL_ELEMENTI_ECP_SA (Table)
--   REL_ELEMENTI_ECS_SA (Table)
--   REL_ELEMENTI_GEO (Table)
--   SCHEDULED_TMP_GEN (Table)
--   STANDARD (Package)
--   STORICO_IMPORT (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_MISURA (Table)
--   TIPI_RETE (Table)
--   TRASF_PROD_BT_TLC (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   UPDATE_AUI_TLC (Table)
--   USER_VIEWS ()
--   USER_VIEWS (Synonym)
--   UTILIZZO_TLC (Table)
--   V_ESERCIZI (View)
--   V_GERARCHIA_AMMINISTRATIVA (View)
--   V_GERARCHIA_GEOGRAFICA (View)
--   V_MOD_ASSETTO_RETE_SA (View)
--
CREATE OR REPLACE PACKAGE PKG_ANAGRAFICHE AS

/* ***********************************************************************************************************
   NAME:       PKG_Anagrafiche
   PURPOSE:    Utilities e Definizioni schema CORELE (SOLO modalita' STM)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      21/02/2012  Moretti C.       Created this package.
   1.0.d      20/04/2012  Moretti C.       Avanzamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.1.c      13/05/2013  Moretti C.       Controllo codici gestionali
   1.1.h      14/03/2014  Moretti C.       Eliminata forzatura introdotta in attesa della gestione
                                           file nea  IMTRAP.nea
   1.8.a.1    28/04/2014  Moretti C.       Ottimizzazione
   1.9.a.0    05/08/2014  Moretti C.       Gestione Centri satellite primo step
   1.9.a.3    24/08/2014  Moretti C.       Gestione Centri satellite implementazione finale
   1.9.a.9    13/02/2015  Moretti C.       Correzione per integrazione
   1.9.a.12   11/02/2015  Moretti C.       Correzione per integrazione
   1.9.a.14   16/06/2015  Moretti C.       Non elabora l'arrivo di un aggiornamento AUI 
              23/06/2015  Moretti C.       Non elabora in caso di Aggiornamento AUI in corso
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   1.9c.2     01/07/2015  Moretti C.       Differenzuazione parametri in open cursore di vista V_GERARCHIA_AMMINISTRATIVA  per Romania
              24/08/2015  Moretti C.       Per Installazione Replay genera solo Gerarchia Elettrica in stato Attuale
   1.9c.3     18/11/2015  Moretti C.       Revisione controllo di sincronizzazione con elaborazione in corso AUI
                                           Ripristinata elaborazione al ricevimento dati da AUI

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitApplMagoSTM     (pCodGestESE     IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                               pData           IN DATE DEFAULT TRUNC(SYSDATE));

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetListaAttualizzazioni (pRefCurs   OUT PKG_UtlGlb.t_query_cur) ;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElementsCheckReport (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pStato          IN INTEGER,
                               pData           IN DATE);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckCodGestCorele  (pStato          IN INTEGER,
                               pBlockOnly      IN BOOLEAN,
                               pData           IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AllineaAnagrafica  (pElabSincrona   IN INTEGER DEFAULT PKG_UtlGlb.gkFlagON,
                               pForzaCompleta  IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOFF);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraGerarchia     (pData          IN DATE,
                                pStato         IN INTEGER,
                                pModAssetto    IN BOOLEAN,
                                pAUI           IN BOOLEAN,
                                pElabSincrona  IN BOOLEAN,
                                pForzaCompleta IN BOOLEAN);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Anagrafiche;
/
SHOW ERRORS;


