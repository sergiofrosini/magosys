SPOOL rel_1.9c.3_NAZ.log

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_1.9c.3_NAZ
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sys_dba as sysdba

GRANT EXECUTE ON DBMS_LOCK to MAGONAZ;

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGONAZ <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Eliminazione Package MAGONAZ.PKG_ANAGRAFICHE (se presente)
PROMPT

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP PACKAGE BODY MAGONAZ.PKG_ANAGRAFICHE';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -04043  -- ORA-04043: l'oggetto ... non esiste  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP PACKAGE MAGONAZ.PKG_ANAGRAFICHE';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -04043  -- ORA-04043: l'oggetto ... non esiste  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
END;
/
     
PROMPT _______________________________________________________________________________________
PROMPT InitTables
@./InitTables/METEO_JOB_RUNTIME_CONFIG.sql


--PROMPT _______________________________________________________________________________________
--PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
--EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

--PROMPT _______________________________________________________________________________________
--PROMPT OGGETTI INVALIDI
--COL OBJECT_NAME FORMAT A35
--SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.3 NAZ
PROMPT

spool off