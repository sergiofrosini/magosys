SPOOL rel_1.9c_NAZROM.log

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_1.9c.0_NAZROM
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sys_dba as sysdba

GRANT EXECUTE ON DBMS_LOCK to MAGONAZ;

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGONAZ ROMANIA <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MAGO.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_STATS.sql
@./Packages/PKG_LOGS.sql
@./Packages/PKG_GENERA_FILE_GEO.sql
@./Packages/PKG_LOCALIZZA_GEO.sql
@./Packages/PKG_MAGO_DGF.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ESERCIZI.sql
--@./Views/V_ANAGRAFICA_IMPIANTO.sql
--@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./Views/V_SCHEDULED_JOBS.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_STATS.sql
@./PackageBodies/PKG_LOGS.sql
@./PackageBodies/PKG_GENERA_FILE_GEO.sql
@./PackageBodies/PKG_LOCALIZZA_GEO.sql
@./PackageBodies/PKG_MAGO_DGF.sql

PROMPT _______________________________________________________________________________________
PROMPT Triggers
@./Triggers/ELEMENTI.sql
@./Triggers/FILE_PROCESSED.sql
@./Triggers/KPI_RICHIESTA.sql
@./Triggers/SCHEDULED_JOBS.sql
@./Triggers/SCHEDULED_TMP_GEN.sql
@./Triggers/SCHEDULED_TMP_GME.sql
@./Triggers/SCHEDULED_TMP_MET.sql
@./Triggers/TRATTAMENTO_ELEMENTI.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
@./InitTables/METEO_JOB_STATIC_CONFIG.sql
@./InitTables/V_PARAMETRI_APPLICATIVI.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.0 Romania
PROMPT

spool off