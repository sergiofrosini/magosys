PROMPT METEO_JOB_STATIC_CONFIG

MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MDS.prediction.timezone' key ,'Europe/Bucharest' value  FROM dual
			union all
            SELECT 'MFM.supplier.folder.source.path.1' key ,'/MAGO/meteo/' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.download.useFtp.1' key ,'true' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.download.useSftp.1' key ,'false' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.download.useURL.1' key ,'false' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.source.host.1' key ,'172.19.129.192' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.source.password.1' key ,'stxsys$1' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.source.port.1' key ,'21' value  FROM dual
			union all
            SELECT 'MFM.supplier.ftp.source.username.1' key ,'stxsys' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

