PROMPT PACKAGE PKG_REPLAY;
--
-- PKG_REPLAY  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   PKG_CREL ()
--
CREATE OR REPLACE PACKAGE PKG_REPLAY AS
/********************************************************************************************************************
   NAME:       PKG_REPLAY
   PURPOSE:    Servizi per la gestione di CREL in ambiente REPLAY

   REVISIONS:
   Ver        Date        Author           Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.0     23/06/2015  Moretti C.       Definizione ambiente REPLAY

*********************************************************************************************************************/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

    gcDefaultGiorni         CONSTANT NUMBER := 3;
    gcInputDateFormat       CONSTANT VARCHAR2(16) := 'yyyymmddhh24miss'; 

 -------------------------------------------------------------------------------------------------------------

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 FUNCTION  LoadByDate   (pDataStart   IN VARCHAR2,
                         pGiorni      IN NUMBER DEFAULT gcDefaultGiorni) 
                  RETURN PKG_Mago.t_RetArea;
                                     
 FUNCTION  LoadByPeriod (pDataStart   IN VARCHAR2,
                         pDataFine    IN VARCHAR2) 
                  RETURN PKG_Mago.t_RetArea;
                                     
 -------------------------------------------------------------------------------------------------------------

END PKG_REPLAY;
/
SHOW ERRORS;


