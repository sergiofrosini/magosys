/*----------------------------------------------------------------------
  Importa, i dati anadrafici e le misure relative al periodo indicato
*/----------------------------------------------------------------------
SET SERVEROUTPUT ON SIZE UNLIMITED
SET ECHO OFF
SET FEEDBACK OFF
SET LINE 200

WHENEVER SQLERROR EXIT 99
WHENEVER OSERROR  EXIT 98;

var exit_val NUMBER;
EXECUTE :exit_val := 0;

var data_Da VARCHAR2(14);
EXECUTE :data_Da := '&1';

var data_A VARCHAR2(14);
EXECUTE :data_A := '&2';

DECLARE
    vEsito PKG_Mago.t_RetArea;
BEGIN
    vEsito := PKG_Replay.LoadByPeriod(:data_Da,:data_A);
    IF vEsito.rc < 0 THEN
        :exit_val := 99;
    ELSE
        :exit_val := 140;
    END IF;
EXCEPTION
    WHEN OTHERS THEN DBMS_OUTPUT.PUT_LINE(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    :exit_val := 99;
END;
/

EXIT :exit_val

