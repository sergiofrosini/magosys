PROMPT TRIGGER BEF_IUR_SCHEDULED_JOBS di tabella SCHEDULED_JOBS
--
-- BEF_IUR_SCHEDULED_JOBS  (Trigger) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   SCHEDULED_JOBS_PKSEQ (Sequence)
--   SCHEDULED_JOBS (Table)
--   DUAL ()
--
CREATE OR REPLACE TRIGGER BEF_IUR_SCHEDULED_JOBS
BEFORE INSERT OR UPDATE
ON SCHEDULED_JOBS REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_IUR_SCHEDULED_JOBS
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.e      23/04/2012   Moretti C.      1. Created this trigger.
   1.9c.0     23/06/2015   Moretti C.      Utilizzo Sequence tramite SQL dinamico

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.JOB_NUMBER   IS NULL THEN
		    -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
			EXECUTE IMMEDIATE 'SELECT SCHEDULED_JOBS_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.JOB_NUMBER ;
        END IF;
    END IF;
    IF :NEW.DATAINS   IS NULL THEN
        :NEW.DATAINS := SYSDATE;
    END IF;
END BEF_IUR_SCHEDULED_JOBS;
/
