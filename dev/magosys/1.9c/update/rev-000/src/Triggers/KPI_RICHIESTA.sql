PROMPT TRIGGER BEF_IUR_KPI_RICHIESTA di tabella KPI_RICHIESTA
--
-- BEF_IUR_KPI_RICHIESTA  (Trigger) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   PKG_UTLGLB (Synonym)
--   KPI_PKSEQ (Sequence)
--   KPI_RICHIESTA (Table)
--   PKG_KPI (Package)
--   DUAL ()
--   PKG_UTLGLB ()
--
CREATE OR REPLACE TRIGGER BEF_IUR_KPI_RICHIESTA
BEFORE INSERT OR UPDATE
ON KPI_RICHIESTA REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_KPI_RICHIESTA
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      18/08/2014   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF UPDATING THEN
        IF :NEW.ID_REQ_KPI <> :NEW.ID_REQ_KPI THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice KPI in tabella ''KPI_RICHIESTA''');
        END IF;
    END IF;
    IF INSERTING THEN
        IF :NEW.ID_REQ_KPI IS NULL THEN
		    -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
            EXECUTE IMMEDIATE 'SELECT KPI_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.ID_REQ_KPI;
            :NEW.ELAB_STATO := NVL(:NEW.ELAB_STATO,PKG_KPI.gcStatoDaElaborare);
            :NEW.DATA_RICHIESTA := NVL(:NEW.DATA_RICHIESTA,SYSDATE);
        END IF;
    END IF;
    IF :NEW.ELAB_STATO NOT IN (PKG_KPI.gcStatoDaElaborare,
                               PKG_KPI.gcStatoInElaborazione,
                               PKG_KPI.gcStatoElabTerminata,
                               PKG_KPI.gcStatoElabInterrotta,
                               PKG_KPI.gcStatoErroreDiElab
                              ) THEN
        RAISE_APPLICATION_ERROR(PKG_UTLGLB.gkErrElaborazione,'Stato non previsto ('||:NEW.ELAB_STATO||') per richiesta n. '||:NEW.ID_REQ_KPI);
    END IF;
    IF :NEW.ELAB_STATO <> :OLD.ELAB_STATO THEN
        :NEW.DATA_STATO := NVL(:NEW.DATA_STATO,SYSDATE);
    END IF;
END BEF_IUR_KPI_RICHIESTA;
/

