PROMPT TABLE ELEMENTI
--
-- BEF_IUR_ELEMENTI  (Trigger) 
--
--  Dependencies: 
--   ELEMENTI (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_ELEMENTI
BEFORE INSERT OR UPDATE
ON ELEMENTI
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_ELEMENTI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      19/09/2011   Moretti C.      Created this trigger.
   1.9c.0     23/06/2015   Moretti C.      Utilizzo Sequence tramite SQL dinamico
   
   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_ELEMENTO IS NULL THEN
		    -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
            EXECUTE IMMEDIATE 'SELECT ELEMENTI_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.COD_ELEMENTO ;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_ELEMENTO <> :NEW.COD_ELEMENTO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice Elemento in tabella ''Elementi''');
        END IF;
    END IF;

END BEF_UR_ELEMENTI;
/
