PROMPT TRIGGER FILEINFO_ID_PK di tabella FILE_PROCESSED
--
-- FILEINFO_ID_PK  (Trigger) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   DUAL ()
--   FILEINFO_SEQUENCE (Sequence)
--   FILE_PROCESSED (Table)
--
CREATE OR REPLACE TRIGGER FILEINFO_ID_PK
BEFORE INSERT ON FILE_PROCESSED
FOR EACH ROW
BEGIN
IF INSERTING THEN
   IF :NEW.id_file IS NULL THEN
      -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
      EXECUTE IMMEDIATE 'SELECT FILEINFO_SEQUENCE.NEXTVAL FROM DUAL' INTO :NEW.ID_FILE;
   END IF;
END IF;
END;
/
