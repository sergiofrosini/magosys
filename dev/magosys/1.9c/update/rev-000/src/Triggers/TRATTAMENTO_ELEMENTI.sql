PROMPT TABLE TRATTAMENTO_ELEMENTI
--
-- BEF_IUR_TRATTAMENTO_ELEMENTI  (Trigger) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   PKG_UTLGLB (Synonym)
--   TRATTAMENTO_ELEMENTI_PKSEQ (Sequence)
--   TRATTAMENTO_ELEMENTI (Table)
--   PKG_UTLGLB ()
--   DUAL ()
--
CREATE OR REPLACE TRIGGER BEF_IUR_TRATTAMENTO_ELEMENTI
BEFORE INSERT OR UPDATE
ON TRATTAMENTO_ELEMENTI REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_TRATTAMENTO_ELEMENTI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      19/09/2011   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_TRATTAMENTO_ELEM  IS NULL THEN
		    -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
			EXECUTE IMMEDIATE 'SELECT TRATTAMENTO_ELEMENTI_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.COD_TRATTAMENTO_ELEM ;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_TRATTAMENTO_ELEM <> :OLD.COD_TRATTAMENTO_ELEM THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_TRATTAMENTO_ELEM non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.COD_ELEMENTO <> :OLD.COD_ELEMENTO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_ELEMENTO non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.COD_TIPO_MISURA <> :OLD.COD_TIPO_MISURA THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_TIPO_MISURA non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.COD_TIPO_FONTE <> :OLD.COD_TIPO_FONTE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_TIPO_FONTE non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.COD_TIPO_RETE <> :OLD.COD_TIPO_RETE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_TIPO_RETE non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.COD_TIPO_CLIENTE <> :OLD.COD_TIPO_CLIENTE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica COD_TIPO_CLIENTE non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.ORGANIZZAZIONE <> :OLD.ORGANIZZAZIONE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica ORGANIZZAZIONE non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
        IF :NEW.TIPO_AGGREGAZIONE <> :OLD.TIPO_AGGREGAZIONE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica TIPO_AGGREGAZIONE non ammessa in tabella TRATTAMENTO_ELEMENTI');
        END IF;
    END IF;

END BEF_UR_TRATTAMENTO_ELEMENTI;
/
