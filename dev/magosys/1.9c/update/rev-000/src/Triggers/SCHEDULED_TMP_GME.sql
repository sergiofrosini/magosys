PROMPT TRIGGER BEF_SCHEDULED_TMP_GME di tabella SCHEDULED_TMP_GME

--
-- BEF_SCHEDULED_TMP_GME  (Trigger) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   SCHEDULED_JOBS_PKSEQ (Sequence)
--   SCHEDULED_TMP_GME (Table)
--   DUAL ()
--
CREATE OR REPLACE TRIGGER BEF_SCHEDULED_TMP_GME
BEFORE INSERT OR UPDATE
ON SCHEDULED_TMP_GME REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_SCHEDULED_TMP_GME
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.h      26/09/2012   Moretti C.      1. Created this trigger.
   1.9c.0     23/06/2015   Moretti C.      Utilizzo Sequence tramite SQL dinamico

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.JOB_NUMBER   IS NULL THEN
		    -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
            EXECUTE IMMEDIATE 'SELECT SCHEDULED_JOBS_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.JOB_NUMBER ;
        END IF;
        :NEW.DATAINS := SYSDATE;
    END IF;

END BEF_SCHEDULED_TMP_GME;
/
