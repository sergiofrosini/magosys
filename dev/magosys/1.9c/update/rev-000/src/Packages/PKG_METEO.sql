PROMPT PACKAGE PKG_METEO;
--
-- PKG_METEO  (Package) 
--
--  Dependencies: 
--   ANAGRAFICA_PUNTI (Table)
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_SCHEDULER ()
--   DBMS_SCHEDULER (Synonym)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   DUAL ()
--   DUAL (Synonym)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   FORECAST_PARAMETRI (Table)
--   GERARCHIA_GEO (Table)
--   GERARCHIA_IMP_SN (Table)
--   GTTD_FORECAST_ELEMENTS (Table)
--   GTTD_VALORI_TEMP (Table)
--   METEO_PREVISIONE (Table)
--   METEO_REL_ISTAT (Table)
--   MISURE_ACQUISITE (Table)
--   MISURE_ACQUISITE_STATICHE (Table)
--   PKG_ELEMENTI (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Synonym)
--   PLITBLM ()
--   PLITBLM (Synonym)
--   RAGGRUPPAMENTO_FONTI (Table)
--   REL_ELEMENTI_GEO (Table)
--   STANDARD (Package)
--   TIPI_CLIENTE (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_RETE (Table)
--   TIPO_FONTI (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   T_MISMETEO_ARRAY (Type)
--   T_PARAM_EOLIC_ARRAY (Type)
--   T_PARAM_EOLIC_OBJ (Type)
--   T_PARAM_PREV_ARRAY (Type)
--   T_PARAM_PREV_OBJ (Type)
--
CREATE OR REPLACE PACKAGE PKG_METEO AS

/* ***********************************************************************************************************
   NAME:       PKG_Meteo
   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      14/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.c      21/03/2012  Risso M.         Integrazione SPC relative flusso meteo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni - Aggiornamento SPC relative flusso meteo
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.g      08/08/2012  Migliaccio G.    Gestione distrib.list diversivicata per tipi di installazione
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8a       07/10/2013  Moretti C.       Calcolo citt� di default per Clienti, Generatori, Trasformatori MT/BT
                                           vedi funzione GetCitta_Cliente_TrasfMT
   1.9a.3     02/10/2014  Moretti C.       Gestione Centri satellite
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetCitta_Cliente_TrasfMT(pCodGest      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                   pData         IN DATE) RETURN METEO_REL_ISTAT.COD_CITTA%TYPE;

 PROCEDURE GetCodiciMeteo       (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1);

 PROCEDURE GetProduttori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetGeneratori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetTrasformatori     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                  pTipoMeteo      IN INTEGER,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrev IN VARCHAR2 DEFAULT NULL);

 PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY);

 PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY);

 PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pTipiElemento   IN VARCHAR2,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrevMeteo IN VARCHAR2 DEFAULT '0');

 PROCEDURE ElaboraMisure        (pMisureMeteo    IN T_MISMETEO_ARRAY);

 PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                 pFinishTimestamp IN DATE);

 PROCEDURE GetMeteoDistribListCO(pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetMeteoDistribList  (pRefCurs       OUT PKG_UtlGlb.t_query_cur, pTipoGeo IN VARCHAR2 DEFAULT 'C');


-- ----------------------------------------------------------------------------------------------------------

END PKG_Meteo;
/
SHOW ERRORS;


