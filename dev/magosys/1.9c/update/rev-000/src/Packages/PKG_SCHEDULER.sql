PROMPT PACKAGE PKG_SCHEDULER;
--
-- PKG_SCHEDULER  (Package) 
--
--  Dependencies: 
--   DBMS_LOCK ()
--   DBMS_LOCK (Synonym)
--   DBMS_OUTPUT ()
--   DBMS_OUTPUT (Synonym)
--   DBMS_UTILITY ()
--   DBMS_UTILITY (Synonym)
--   GTTD_VALORI_TEMP (Table)
--   PKG_AGGREGAZIONI (Package)
--   PKG_LOGS (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB ()
--   PKG_UTLGLB (Package)
--   PKG_UTLGLB (Synonym)
--   SCHEDULED_JOBS (Table)
--   SCHEDULED_JOBS_DEF (Table)
--   STANDARD (Package)
--   TIPI_MISURA (Table)
--   TIPI_RETE (Table)
--   USER_SCHEDULER_JOBS ()
--   USER_SCHEDULER_JOBS (Synonym)
--   V_TIPI_MISURA (View)
--
CREATE OR REPLACE PACKAGE PKG_SCHEDULER AS

/* ***********************************************************************************************************
   NAME:       PKG_Scheduler
   PURPOSE:    Servizi per la gestione dei jobs schedulati

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.e      11/06/2012  Moretti C.       revisione schedulazione jobs
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8.a.1    29/04/2014  Moretti C.       ElaboraJobs - Sleep 2 secondi dopo 90 sec. di elaborazione
   1.9.a.13   10/03/2015  Moretti C.       - Chiude l'elaborazione dopo 1 ora. il Rimanente sar� preso in 
                                             carico dal giro successivo. 
                                           - Sleep 6 secondi dopo ogni ciclo di aggregazione (problemi di 
                                             sessioni bloccate da traffico su log writer di oracle)
   1.9.a.14   08/05/2015  Moretti C.       Correzione per chiusura elaborazione dopo 1 ora
                                           Parametrizzazione Secondi di Pausa tra uno step e l'altro
              18/06/2015  Moretti C.       Calcolo aggregazioni per periodo futuro pilotate da parametrizzazione
   1.9c.0     16/07/2015  Moretti C.       Definizione ambiente REPLY - Non esegue schedulazioni 

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- PROCEDURE AddJobLoadAnagr          (pDataRif         DATE,
--                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
--                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobLinearizzazione    (pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobCalcMisStatiche    (pDataRif         DATE);

 PROCEDURE ConsolidaJobAggregazione (pTipo            VARCHAR2 DEFAULT NULL,
                                     pUseLog          BOOLEAN DEFAULT FALSE);

 PROCEDURE ConsolidaJobAggrMETEO;

 PROCEDURE ConsolidaJobAggrGME;

 PROCEDURE AddJobCalcAggregazione   (pTipoProcesso    VARCHAR2,
                                     pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.STATO%TYPE,
                                     pTipoRete        TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     pTipoMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     pDataIns         DATE DEFAULT NULL);

 PROCEDURE ElaboraJobs;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Scheduler;
/
SHOW ERRORS;


