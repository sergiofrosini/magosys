PROMPT PACKAGE PKG_MAGO_DGF;
--
-- PKG_MAGO_DGF  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   PROVINCE (Table)
--   COMUNI (Table)
--   MANUTENZIONE (Table)
--   TMP_MANUTENZIONE (Table)
--   TIPI_MISURA (Table)
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   MISURE_ACQUISITE_STATICHE (Table)
--   PRODUTTORI_TMP_EXT (Table)
--   MANUTENZIONE_SEQ (Sequence)
--   ELEM_DEF_EOLICO_TMP_EXT (Table)
--   ELEM_DEF_SOLARE_TMP_EXT (Table)
--   MISURE_TMP_EXT (Table)
--   PRODUTTORI_TMP (Table)
--   ELEM_DEF_EOLICO_TMP (Table)
--   ELEM_DEF_SOLARE_TMP (Table)
--   MISURE_TMP (Table)
--   MISURE_ERR (Table)
--   MISURE_ACQUISITE_MANUTENZIONE (Table)
--   REL_ELEMENTO_TIPMIS (Table)
--   ELEMENTI_GDF_EOLICO (Table)
--   ELEMENTI_GDF_SOLARE (Table)
--   CFG_APPLICATION_PARAMETER (Table)
--   OFFSET (Table)
--   T_ELEMAN_OBJ (Type)
--   T_ELEMAN_OBJ (Type)
--   PKG_GERARCHIA_DGF (Package)
--   PKG_MAGO_UTL (Package)
--   V_ELEMENTI_DGF (View)
--   APPLICATION_RUN (Table)
--   PKG_MISURE (Package)
--   PKG_SCHEDULER (Package)
--   DUAL ()
--   PLITBLM ()
--   MISURE_ACQUISITE (Table)
--
CREATE OR REPLACE PACKAGE pkg_mago_dgf AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_Dgf
   PURPOSE:    Servizi per la gestione del caricamento dai Eolico e Solare
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      20/04/2012  Paolo Campi      Created this package.
   1.9c.0     16/07/2015  Moretti C.       Utilizzo Sequence tramite SQL dinamico
   NOTES:
*********************************************************************************************************** */
   PROCEDURE initMagoDGF;
   FUNCTION  checkMis( checktype IN VARCHAR2 ) RETURN NUMBER;
   PROCEDURE load_produttori;
   PROCEDURE load_def_eolico;
   PROCEDURE load_def_solare;
   PROCEDURE load_misure;
   PROCEDURE insert_element;
   PROCEDURE insert_rel_misure;
   PROCEDURE insert_def (pData IN DATE);
   PROCEDURE insert_def_eolico (pData IN DATE);
   PROCEDURE insert_def_solare (pData IN DATE);
   PROCEDURE insert_trattamento_elementi (pData IN DATE);
   PROCEDURE insert_misure (pMin IN NUMBER);
   PROCEDURE insert_manutenzione(pEle IN T_ELEMAN_OBJ); 
   PROCEDURE elabora_manutenzione;
END;
/
SHOW ERRORS;


