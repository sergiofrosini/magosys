PROMPT VIEW V_ESERCIZI;
--
-- V_ESERCIZI  (View) 
--
--  Dependencies: 
--   UNITA_TERRITORIALI (Table)
--   MACRO_AREE (Table)
--   ESERCIZI_ABILITATI (Table)
--   ESERCIZI (Table)
--   PKG_MAGO (Package)
--   ELEMENTI (Table)
--
CREATE OR REPLACE VIEW V_ESERCIZI
AS 
SELECT COD_MA,
       NOM_MA,
       ACR_MA,
       COD_UTR,
       GST_UTR,
       NOM_UTR,
       ACR_UTR,
       COD_ESERCIZIO,
       GST_ESE,
       NOM_ESE,
       ESE,
       ACR_ESE,
       CASE WHEN PKG_Mago.IsRomaniaFL = 0
                THEN ESE_PRIMARIO
                ELSE CASE WHEN ORD_ESE = 1 THEN 1
                                           ELSE 0
                     END
       END ESE_PRIMARIO,
       E.COD_ELEMENTO COD_ESE
  FROM (SELECT M.COD_MACRO_AREA COD_MA,
               M.NOME NOM_MA,
               CASE M.COD_MACRO_AREA   /* Acroniomo Macro Area provvisoriamente valorizzato fittiziamente */
                   WHEN 1 THEN 'DANO'
                   WHEN 2 THEN 'DANE'
                   WHEN 3 THEN 'DAC'
                   WHEN 4 THEN 'DAS'
               END ACR_MA,
               U.COD_UTR COD_UTR,
               U.NOME NOM_UTR,
               U.ACRONIMO ACR_UTR,
               U.CODIFICA_STM GST_UTR,
               E.COD_ESERCIZIO COD_ESE,
               CASE WHEN PKG_Mago.IsRomaniaFL = 0
                        THEN U.CODIFICA_UTR||E.CODIFICA_ESERCIZIO
                        ELSE U.CODIFICA_STM||E.CODIFICA_ESERCIZIO
               END GST_ESE,
               UPPER(E.NOME) NOM_ESE,
               E.ACRONIMO ACR_ESE,
               CASE WHEN PKG_Mago.IsRomaniaFL = 0
                        THEN E.COD_ESERCIZIO
                        ELSE A.COD_ESERCIZIO
               END COD_ESERCIZIO,
               CODIFICA_ESERCIZIO ESE,
               CASE
                    WHEN A.COD_UTR = E.COD_UTR AND A.COD_ESERCIZIO = E.COD_ESERCIZIO
                            THEN 1
                            ELSE 0
               END ESE_PRIMARIO,
               CASE WHEN PKG_Mago.IsRomaniaFL = 0
                        THEN NULL
                        ELSE RANK() OVER (PARTITION BY 1 ORDER BY U.CODIFICA_STM||E.CODIFICA_ESERCIZIO)
               END ORD_ESE
          FROM SAR_ADMIN.ESERCIZI_ABILITATI A
         INNER JOIN SAR_ADMIN.UNITA_TERRITORIALI U ON U.COD_UTR = A.COD_UTR
         INNER JOIN SAR_ADMIN.MACRO_AREE M ON M.COD_MACRO_AREA = U.COD_MACRO_AREA
         INNER JOIN SAR_ADMIN.ESERCIZI E ON E.COD_UTR = A.COD_UTR
         WHERE COD_APPLICAZIONE = 'MAGO'
           AND E.CODIFICA_ESERCIZIO <> '00'
           AND U.COD_UTR BETWEEN 1 AND 99
           AND U.FLAG_DISATTIVATO = 0
           AND E.COD_ESERCIZIO BETWEEN 1 AND 99
           AND E.FLAG_DISATTIVATO = 0
           AND CASE WHEN PKG_Mago.IsRomaniaFL = 0
                          THEN 1
                          ELSE CASE WHEN A.COD_ESERCIZIO = E.COD_ESERCIZIO
                                        THEN 0
                                        ELSE 1
                               END
               END = 1
       ) X
  LEFT OUTER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = X.GST_ESE
 ORDER BY ESE_PRIMARIO DESC, NOM_ESE, GST_ESE
/
