PROMPT PACKAGE BODY PKG_MAGO;
--
-- PKG_MAGO  (Package Body) 
--
--  Dependencies: 
--   PKG_MAGO (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_MAGO AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9c.0
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gStartDate         DATE;

 gIsReplay          BOOLEAN;
 gIsRomania         BOOLEAN;

 gTipoInstallazione DEFAULT_CO.TIPO_INST%TYPE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetEcho            (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pEchoString     IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna Echo
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    OPEN pRefCurs FOR SELECT pEchoString ECHO_STRING   FROM DUAL;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Mago.GetEcho'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetEcho;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitMagoSession AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza la sessione
-----------------------------------------------------------------------------------------------------------*/
BEGIN

   BEGIN
        IF PKG_UtlGlb.GetParamGenNum('IS_REPLAY',PKG_UtlGlb.gkFlagOff) = PKG_UtlGlb.gkFlagON THEN
            gIsReplay := TRUE;
        ELSE 
            gIsReplay := FALSE;
        END IF;
   END;

   BEGIN
        IF PKG_UtlGlb.GetParamGenNum('IS_ROMANIA',PKG_UtlGlb.gkFlagOff) = PKG_UtlGlb.gkFlagON THEN
            gIsRomania := TRUE;
        ELSE 
            gIsRomania := FALSE;
        END IF;
   END;

   BEGIN
        SELECT TIPO_INST,          START_DATE
          INTO gTipoInstallazione, gStartDate
          FROM DEFAULT_CO
         WHERE FLAG_PRIMARIO = 1;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN gTipoInstallazione := gcMagoSTM;
        WHEN OTHERS        THEN RAISE;
    END;

    IF isMagoDgf THEN
        cSeparatore := '#';
    END IF;

END InitMagoSession;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION StdOutDate          (pData          IN DATE) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la data formattata in modo standard
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pData IS NULL THEN
        RETURN '<nulldate>';
    ELSE
        RETURN TO_CHAR(pData,'dd/mm/yyyy hh24:mi:ss');
    END IF;
END StdOutDate;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetVersion        (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce la versione correnre dell'applicazione
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    OPEN pRefCurs FOR 'SELECT VERSION FROM V_CURRENT_VERSION';
END GetVersion;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsMagoStm          RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce true se l'installazione e' di tipo STM
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF BITAND(gTipoInstallazione,gcMagoSTM) = gcMagoSTM THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
END IsMagoStm;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsMagoStmFL        RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce 1 se l'installazione e' di tipo STM altrimenti 0
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    RETURN PKG_UtlGlb.BooleanToFlag(IsMagoStm);
END IsMagoStmFL;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsMagoDgf          RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce true se l'installazione e' di tipo DGF
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF BITAND(gTipoInstallazione,gcMagoDGF) = gcMagoDGF THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
 END IsMagoDgf;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsMagoDgfFL        RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce 1 se l'installazione e' di tipo DGF altrimenti 0
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    RETURN PKG_UtlGlb.BooleanToFlag(IsMagoDgf);
END IsMagoDgfFL;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsNazionale        RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce true se l'installazione e' di tipo nazionale
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF BITAND(gTipoInstallazione,gcMagoNazionale) = gcMagoNazionale THEN
        RETURN TRUE;
    ELSE
        RETURN FALSE;
    END IF;
 END IsNazionale;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsNazionaleFL      RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce 1 se l'installazione e' di tipo nazionale altrimenti 0
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN PKG_UtlGlb.BooleanToFlag(IsNazionale);
 END IsNazionaleFL;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsReplay          RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce true se l'installazione e' di tipo replay
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gIsReplay;
 END IsReplay;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsReplayFL       RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce 1 se l'installazione e' di tipo Replay altrimenti 0
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN PKG_UtlGlb.BooleanToFlag(gIsReplay);
 END IsReplayFL;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsRomania         RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce true se l'installazione e' di tipo Romania
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gIsRomania;
 END IsRomania;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION IsRomaniaFL       RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce 1 se l'installazione e' di tipo Romania altrimenti 0
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN PKG_UtlGlb.BooleanToFlag(gIsRomania);
 END IsRomaniaFL;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetStartDate (pData IN DATE DEFAULT NULL) RETURN DATE AS
/*----------------------------------------------------------------------------------------------------------
    Restituisce la data start
        - se pData IS Null ritorna DEFAULT_CO.START_DATE
        - se pData <  DEFAULT_CO.START_DATE ritorna DEFAULT_CO.START_DATE
        - se pData >= DEFAULT_CO.START_DATE ritorna pData
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF gStartDate IS NULL THEN
        SELECT START_DATE INTO gStartDate FROM DEFAULT_CO  WHERE FLAG_PRIMARIO = 1;
    END IF;
    IF pData IS NULL THEN
        RETURN gStartDate;
    ELSE
        IF pData < gStartDate THEN
            RETURN gStartDate;
        ELSE
            RETURN pData;
        END IF;
    END IF;
END GetStartDate;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE TrattaListaCodici  (pLista         IN VARCHAR2,
                              pTempTip       IN GTTD_VALORI_TEMP.TIP%TYPE,
                              pAggrega      OUT NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Tratta in modo standard le liste codici
-----------------------------------------------------------------------------------------------------------*/
    vLista PKG_UtlGlb.t_SplitTbl;
 BEGIN
    IF SUBSTR(pLista,2,1) = '!' THEN
        pAggrega := TO_NUMBER(SUBSTR(pLista,1,1));
        vLista   := PKG_UtlGlb.SplitString(SUBSTR(pLista,3),cSepCharLst);
    ELSE
        pAggrega := -1;
        vLista   := PKG_UtlGlb.SplitString(pLista,cSepCharLst);
    END IF;
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            IF vLista(i) IS NOT NULL THEN
                INSERT INTO GTTD_VALORI_TEMP (TIP, ALF1) VALUES (pTempTip,vLista(i));
            END IF;
        END LOOP;
    END IF;
END TrattaListaCodici;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGruppiMisura   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con la struttura dei gruppi misura definiti per il tipo elemento recevuto
-----------------------------------------------------------------------------------------------------------*/

BEGIN

    OPEN pRefCurs FOR SELECT /*+ INDEX_JOIN(G) INDEX_JOIN(TIPI_GRUPPI_MISURA) INDEX_JOIN(M) INDEX_JOIN(C) INDEX_JOIN(E) */
                             G.COD_GRUPPO ID_GRUPPO,
                             FLAG_ATTIVO CHECKED,
                             M.COD_TIPO_MISURA,
                             COD_UM_STANDARD,
                             FATTORE_MOLTIPLICATIVO,
                             RISOLUZIONE_FE,
                             TIPO_INTERPOLAZIONE_FREQ_CAMP,
                             TIPO_INTERPOLAZIONE_BUCHI_CAMP,
                             SPLIT_FONTE_ENERGIA,
                             RILEVAZIONI_NAGATIVE,
                             FONTE_NON_OMOGENEA,
                             FONTE_NON_APPLICABILE,
                             PRODUTTORE_NON_APPLICABILE,
                             PRODUTTORE_NON_DISPONIBILE
                        FROM GRUPPI_MISURA G
                       INNER JOIN TIPI_GRUPPI_MISURA T ON  (G.COD_GRUPPO = T.COD_GRUPPO)
                       INNER JOIN TIPI_MISURA         M ON M.COD_TIPO_MISURA   = G.COD_TIPO_MISURA
                       INNER JOIN TIPI_MISURA_CONV_UM C ON C.COD_TIPO_MISURA   = M.COD_TIPO_MISURA AND
                                                           C.TIPO_MISURA_CONV  = M.COD_UM_STANDARD
                       INNER JOIN TIPI_ELEMENTO       E ON E.COD_TIPO_ELEMENTO = G.COD_TIPO_ELEMENTO
                       WHERE E.COD_TIPO_ELEMENTO = pTipElem
                         AND DISATTIVATO = 0
                       ORDER BY G.COD_GRUPPO, SEQ_ORD, M.COD_TIPO_MISURA;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Mago.GetGruppiMisura'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetGruppiMisura;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTipiMisura     (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con la definizione dei tipi misura definiti
-----------------------------------------------------------------------------------------------------------*/

BEGIN

    OPEN pRefCurs FOR SELECT COD_TIPO_MISURA,
                             CODIFICA_ST,
                             DESCRIZIONE,
                             MIS_GENERAZIONE,
                             RISOLUZIONE_FE,
                             COD_UM_STANDARD,
                             RILEVAZIONI_NAGATIVE,
                             SPLIT_FONTE_ENERGIA,
                             TO_NUMBER(TIPO_INTERPOLAZIONE_FREQ_CAMP) TIPO_INTERPOLAZIONE_FREQ_CAMP,
                             TO_NUMBER(TIPO_INTERPOLAZIONE_BUCHI_CAMP) TIPO_INTERPOLAZIONE_BUCHI_CAMP
                        FROM TIPI_MISURA
                       ORDER BY CASE
                                   WHEN COD_TIPO_MISURA IN (SELECT DISTINCT COD_TIPO_MISURA FROM GRUPPI_MISURA) THEN 1
                                   ELSE 0
                                END DESC,
                                COD_TIPO_MISURA;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Mago.GetTipiMisura'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetTipiMisura;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                              pGestElem      IN VARCHAR2,
                              pData          IN DATE DEFAULT SYSDATE,
                              pStatoRete     IN INTEGER DEFAULT PKG_Mago.gcStatoAttuale) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con il dettaglio dei codici gestionali ricevuti
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PKG_Elementi.GetElementsRegistry (pRefCurs, pGestElem, pData, pStatoRete);

END GetElementsRegistry;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetTrattamentoElemento(pCodEle      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pTipEle      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                pTipMis      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                pTipFon      IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                pTipRet      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                pTipCli      IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                pOrganizzaz  IN NUMBER,
                                pTipoAggreg  IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il trattamento Elemento appropriato - Wrapper di PKG_Misure.GetTrattamentoElemento
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    RETURN PKG_Misure.GetTrattamentoElemento(pCodEle,
                                             pTipEle,
                                             pTipMis,
                                             pTipFon,
                                             pTipRet,
                                             pTipCli,
                                             pOrganizzaz,
                                             pTipoAggreg);

END GetTrattamentoElemento;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL,
                             pElemTypes      IN VARCHAR2 DEFAULT NULL,
                             pDisconnect     IN INTEGER  DEFAULT gcOFF) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con la struttura degli elementi all'istante richiesto (da codice elemento )
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PKG_Elementi.GetElements(pRefCurs,
                             pCodElem,pDataDa,pDataA,pOrganizzazione,pStatoRete,
                             pTipologiaRete,pTipoClie,pElemTypes,gcON,gcON,pDisconnect);

END GetElements;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pGestElem       IN VARCHAR2,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Localizza un codice gestionale nella gerarchia/stato indicate e restituisce la catena di elementi
    necessaria a raggiungere l'elemento stesso
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PKG_Elementi.FindElement (pRefCurs,
                              pGestElem,pDataDa,pDataA,pOrganizzazione,pStatoRete,
                              pTipologiaRete,pTipoClie);

END FindElement;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Mago',
                          pStoreOnFile => FALSE);

    InitMagoSession;

END PKG_Mago;
/
SHOW ERRORS;


