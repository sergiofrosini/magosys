PROMPT PACKAGE BODY PKG_MAGO_DGF;
--
-- PKG_MAGO_DGF  (Package Body) 
--
--  Dependencies: 
--   PKG_MAGO_DGF (Package)
--
CREATE OR REPLACE PACKAGE BODY pkg_mago_dgf AS
proc_name VARCHAR2(50);
--vLog         PKG_Mago.t_StandardLog;
err_code NUMBER;
err_msg VARCHAR2(250);
read_err EXCEPTION;
PRAGMA EXCEPTION_INIT(read_err, -29913); 
PROCEDURE initMagoDGF AS
BEGIN
 pkg_mago_utl.run_application_group('INIT');
 pkg_mago_utl.run_application_group('ANAGRAFICA');
 pkg_mago_utl.run_application_group('MISURE');
END;
FUNCTION checkMis( checktype IN VARCHAR2 ) RETURN NUMBER AS
   valcheck VARCHAR2(50);
   vrunid NUMBER;
BEGIN
   SELECT par_value_char
     INTO valcheck
     FROM CFG_APPLICATION_PARAMETER
    WHERE parameter_name = checktype;
   SELECT run_id 
     INTO vrunid
     FROM APPLICATION_RUN;
   CASE checktype 
      WHEN 'MIS.NOELEM' THEN
         INSERT INTO MISURE_ERR
            (run_id
             ,cg_impianto
             ,errore)
         SELECT vrunid
               ,cg_impianto
               ,checktype
           FROM (
                 SELECT TRIM(cg_impianto) cg_impianto
                   FROM MISURE_TMP
                  MINUS
                 SELECT cod_gest_elemento
                   FROM ELEMENTI
                  WHERE cod_tipo_elemento IN ('GAT','GMT','GBT')
                ) 
          WHERE cg_impianto IS NOT NULL; 
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Elementi Misure failed',3);
             IF valcheck = 'ERROR' THEN
                RETURN 0;
             END IF;
         END IF;
         RETURN 1;
      WHEN 'MIS.NOMIS' THEN
         INSERT INTO MISURE_ERR
            (run_id
             ,cg_impianto
             ,errore)
         SELECT vrunid
               ,cod_gest_elemento cg_impianto
               ,checktype
           FROM ELEMENTI
          WHERE cod_tipo_elemento IN ('GAT','GMT','GBT')
          MINUS
         SELECT vrunid
               ,TRIM(cg_impianto) cg_impianto
               ,checktype
           FROM MISURE_TMP;
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Misure Elementi failed',4);
             IF valcheck = 'ERROR' THEN
                RETURN 0;
             END IF;
         END IF;
         RETURN 1;
      WHEN 'MIS.DUPL' THEN
         INSERT INTO MISURE_ERR
         (run_id
          ,cg_impianto
          ,DATA 
          ,errore
         )
         SELECT vrunid, cg_impianto, TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora, 'dd/mm/yyyyhh24.mi.ss') , 'MIS.DUPL_SAME'
           FROM MISURE_TMP
           WHERE TRIM(cg_impianto) IS NOT NULL
          GROUP BY cg_impianto , TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora, 'dd/mm/yyyyhh24.mi.ss'), 'MIS.DUPL_SAME'
         HAVING COUNT (*) > 1;
         IF SQL%ROWCOUNT > 0 THEN
            pkg_mago_utl.insLog(proc_name,'Check Misure Duplicate Same File failed',2);
            DELETE FROM MISURE_TMP 
             WHERE cg_impianto IN ( SELECT DISTINCT cg_impianto 
                                      FROM MISURE_ERR
                                      WHERE ERRORE = 'MIS.DUPL_SAME' 
                                        AND run_id = vrunid);                                        
         END IF;
         INSERT INTO MISURE_ERR
         ( run_id
          ,cod_trattamento_elem
          ,cg_impianto
          ,cod_tipo_misura
          ,DATA
          ,errore
         )
         SELECT vrunid,tr.COD_TRATTAMENTO_ELEM,el.cod_gest_elemento,tr.cod_tipo_misura, DATA,checktype  || ':' || valcheck
           FROM MISURE_ACQUISITE mis
                ,TRATTAMENTO_ELEMENTI tr
                ,ELEMENTI el
          WHERE mis.cod_trattamento_elem = tr.cod_trattamento_elem
            AND el.cod_elemento = tr.cod_elemento
            AND tr.cod_tipo_elemento IN ('GAT','GMT','GBT')
            AND tr.cod_tipo_misura IN ('VVE','DVE','TMP','PAG','IRR')
      INTERSECT
         SELECT vrunid,tr.COD_TRATTAMENTO_ELEM,el.cod_gest_elemento,tr.cod_tipo_misura
               ,TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24.mi.ss'),checktype  || ':' || valcheck
           FROM MISURE_TMP mis
               ,TRATTAMENTO_ELEMENTI tr
               ,ELEMENTI el
          WHERE mis.cg_impianto = el.cod_gest_elemento
            AND el.cod_elemento = tr.cod_elemento
            AND el.cod_tipo_elemento IN ('GAT','GMT','GBT')
            AND tr.cod_tipo_misura IN ('VVE','DVE','TMP','PAG','IRR');
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Misure Duplicate failed',5);
             IF valcheck = 'SKIP' THEN
                DELETE FROM MISURE_TMP
                 WHERE (cg_impianto, DATA)
                   IN (SELECT cg_impianto, DATA 
                          FROM MISURE_ERR
                         WHERE errore = checktype || ':' || valcheck
                           AND run_id = vrunid 
                       );        
             ELSIF valcheck = 'SUBSTITUTE' THEN 
                DELETE FROM MISURE_ACQUISITE
                 WHERE (cod_trattamento_elem, DATA)
                    IN (SELECT cod_trattamento_elem, DATA 
                          FROM MISURE_ERR
                         WHERE errore = checktype || ':' || valcheck
                           AND run_id = vrunid
                      );
             ELSE 
                RETURN 0;             
             END IF;
          END IF;
          RETURN 1;
     END CASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
   pkg_mago_utl.insLog(proc_name,'Check Misure No CFG ' || checktype,1);
   RETURN 1;
END;
PROCEDURE LOAD_PRODUTTORI AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_PRODUTTORI';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table produttori_tmp';
   INSERT INTO PRODUTTORI_TMP
          ( cod_gruppo
           ,descr_gruppo
           ,cod_contatore_fis
           ,cg_produttore
           ,descr_produttore
           ,cod_contatore_sc
           ,cg_generatore
           ,comune
           ,cod_comune
           ,tipo_coord_p
           ,coord_nord_p
           ,coord_est_p
           ,coord_up_p
           ,tipo_coord_a
           ,coord_nord_a
           ,coord_est_a
           ,coord_up_a
           ,h_anem
           ,tipo_rete
           ,fonte
          )
   SELECT UPPER(REPLACE(cod_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,descr_gruppo
           ,UPPER(REPLACE(cod_contatore_fis,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,descr_produttore cg_produttore --CASE WHEN descr_produttore IS NOT NULL THEN UPPER( SUBSTR(REPLACE(descr_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem),1,3) || UPPER( SUBSTR(REPLACE(descr_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem),-3,3) || '-' || SUBSTR(descr_produttore,1,3) || SUBSTR(descr_produttore,-3,3))) END cg_produttore
           ,descr_produttore
           ,UPPER(REPLACE(cod_contatore_sc,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,UPPER(REPLACE(cg_generatore,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,UPPER(comune)
           ,cod_comune
           ,tipo_coord_p
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_nord_p,',','.'),CHR(176),' '),'"',' '),'''',' '),'  ',' ')
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_est_p,',','.'),CHR(176),' '),'"',' '),'''',' '),'  ',' ')
           ,TO_NUMBER(coord_up_p,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,tipo_coord_a
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_nord_a,',','.'),CHR(176),' '),'"',' '),'''',' '),'  ',' ')
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_est_a,',','.'),CHR(176),' '),'"',' '),'''',' '),'  ',' ')
           ,TO_NUMBER(coord_up_a,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,TO_NUMBER(h_anem,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,tipo_rete
           ,fonte
     FROM PRODUTTORI_TMP_EXT
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);     
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN read_err THEN
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_PRODUTTORI;
PROCEDURE LOAD_DEF_EOLICO AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_DEF_EOLICO';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table elem_def_eolico_tmp';
   INSERT INTO ELEM_DEF_EOLICO_TMP
          ( cg_generatore
           ,potenza_installata
           ,marca_turbina
           ,modello_turbina
           ,h_mozzo
           ,vel_cutin
           ,vel_cutoff
           ,vel_max 
           ,vel_recutin
           ,delta_cutoff
           ,delta_recutin       
           ,wind_sect_manager    
           ,start_wind_sect     
           ,stop_wind_sect      
           ,vel_cutoff_shdown
           ,vel_recutin_shdown
          )
   SELECT UPPER(cg_generatore)
         ,TO_NUMBER(potenza_installata,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''') * 1000
         ,marca_turbina
         ,modello_turbina
         ,TO_NUMBER(altezza_mozzo,'9999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutoff,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_max,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_recutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(delta_cutoff,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(delta_recutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,wind_sect_manager
         ,TO_NUMBER(start_wind_sect,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(stop_wind_sect,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutoff_shdown,'99999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_recutin_shdown,'99999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
     FROM ELEM_DEF_EOLICO_TMP_EXT
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN read_err THEN
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_DEF_EOLICO;
PROCEDURE LOAD_DEF_SOLARE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_DEF_SOLARE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table elem_def_solare_tmp';
   INSERT INTO ELEM_DEF_SOLARE_TMP
          ( cg_generatore
           ,potenza_installata
           ,id_tecnologia
           ,flag_tracking
           ,orientazione
           ,inclinazione
           ,superficie
          )
   SELECT UPPER(cg_generatore)
         ,TO_NUMBER(potenza_installata,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''') 
         ,id_tecnologia
         ,flag_tracking
         ,TO_NUMBER(orientazione,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(inclinazione,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(superficie,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
     FROM ELEM_DEF_SOLARE_TMP_EXT
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN read_err THEN
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_DEF_SOLARE;
PROCEDURE LOAD_MISURE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table misure_tmp';
   INSERT INTO MISURE_TMP
          ( cg_impianto
           ,DATA
           ,ora
           ,valore
           ,velocita_vento
           ,direzione_vento
           ,irraggiamento
           ,temperatura
          )
   SELECT  cg_impianto
           ,TO_DATE(DATA,'dd/mm/yyyy')
           ,LPAD(SUBSTR(REPLACE (ora, ':','.'),1,INSTR(REPLACE (ora, ':','.'),'.')-1),2,'0') || SUBSTR(REPLACE (ora, ':','.'),INSTR(REPLACE (ora, ':','.'),'.'))
           ,TO_NUMBER(SUBSTR(REPLACE(valore,'.',','),1,INSTR(REPLACE(valore,'.',','),',')+3),'999999D999','NLS_NUMERIC_CHARACTERS = '',.''')*1000
           ,TO_NUMBER(REPLACE(velocita_vento,'.',','),'99999D999999999','NLS_NUMERIC_CHARACTERS = '',.''')
           ,TO_NUMBER(REPLACE(direzione_vento,'.',','),'99999D999999999','NLS_NUMERIC_CHARACTERS = '',.''')
           ,TO_NUMBER(REPLACE(irraggiamento,'.',','),'99999D999999999','NLS_NUMERIC_CHARACTERS = '',.''')
           ,TO_NUMBER(REPLACE(temperatura,'.',','),'999D999999999','NLS_NUMERIC_CHARACTERS = '',.''')
     FROM MISURE_TMP_EXT;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN read_err THEN
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_MISURE;
PROCEDURE INSERT_ELEMENT AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_ELEMENT';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO ELEMENTI
          ( cod_gest_elemento 
           ,cod_tipo_elemento 
          ) 
   SELECT NEW.cod_gest_elemento 
         ,NEW.cod_tipo_elemento
     FROM ( 
           --ELEMENTO PER GERARCHIA AMMINISTRATIVA
           SELECT DISTINCT 
                   cod_gruppo cod_gest_elemento
                   ,tprod.cod_tipo_elemento 
              FROM PRODUTTORI_TMP prod 
                 ,TIPI_ELEMENTO tprod 
            WHERE UPPER(tprod.descrizione) = 'ZONA'
            UNION ALL
           --ELEMENTI PER GERARCHIA DI CAB SEC
           SELECT DISTINCT 
                  prod.cg_produttore cod_gest_elemento
                 ,tprod.cod_tipo_elemento 
             FROM PRODUTTORI_TMP prod 
                 ,TIPI_ELEMENTO tprod 
            WHERE UPPER(tprod.descrizione) = 'PRODUTTORE' || tipo_rete 
              AND cg_produttore IS NOT NULL
            UNION ALL 
           SELECT prod.cg_generatore
                 ,tprod.cod_tipo_elemento 
             FROM PRODUTTORI_TMP prod 
                 ,TIPI_ELEMENTO tprod 
            WHERE UPPER(tprod.descrizione) = 'GENERATORE' || tipo_rete
            UNION ALL
            --ELEMENTI PER GERARCHIA GEOGRAFICA (COMUNE, PROVINCIA, PRODUTTORE@COMUNE)
           SELECT DISTINCT 
                  cod_comune cod_gest_elemento 
                 ,tprod.cod_tipo_elemento 
             FROM PRODUTTORI_TMP prod 
                 ,TIPI_ELEMENTO tprod
            WHERE UPPER(tprod.descrizione) = 'COMUNE'
            UNION ALL
           SELECT DISTINCT  
                  prv.sigla 
                 ,tprod.cod_tipo_elemento 
             FROM PRODUTTORI_TMP prod 
                 ,TIPI_ELEMENTO tprod
                 ,sar_Admin.comuni com 
                 ,sar_admin.province prv 
            WHERE  com.cod_istat_prov = prv.cod_istat_prov 
              AND com.cod_istat_comune = prod.cod_comune
              AND UPPER(tprod.descrizione) ='PROVINCIA'
            UNION ALL
           SELECT cod_gest_elemento , cod_tipo_elemento
             FROM (
                  SELECT cod_gest_elemento , cod_tipo_elemento
                        ,COUNT(*) OVER( PARTITION BY cg_produttore ) tot
                   FROM(
                        SELECT DISTINCT
                               prod.cod_comune || pkg_mago_utl.gcSeparatoreElem || prod.cg_produttore cod_gest_elemento
                              ,tprod.cod_tipo_elemento 
                              ,prod.cg_produttore 
                          FROM PRODUTTORI_TMP prod 
                              ,TIPI_ELEMENTO tprod 
                         WHERE UPPER(tprod.descrizione) = 'PRODUTTORE' || tipo_rete
                           AND  prod.cg_produttore IS NOT NULL
                       )
                  )
            WHERE tot > 1
          ) NEW
          ,ELEMENTI OLD
    WHERE NEW.cod_gest_elemento = OLD.cod_gest_elemento(+)
      AND OLD.cod_gest_elemento IS NULL ;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_ELEMENT;
PROCEDURE INSERT_DEF (pData IN DATE) AS
BEGIN
/* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
   Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce 
   ma si spostera' la data validita' attraverso le merge successive
*/
proc_name := 'PKG_MAGO_DGF.INSERT_DEF';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO ELEMENTI_DEF
          ( cod_elemento
           ,nome_elemento
           ,cod_tipo_elemento
           ,cod_tipo_fonte
           ,cod_tipo_cliente
           ,potenza_installata
           ,fattore
           ,rif_elemento
           ,cod_geo
           ,cod_geo_a
           ,h_anem
           ,coordinata_x
           ,coordinata_y
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (SELECT v.* 
          FROM v_elementi_dgf v
       )
      , old_data AS
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              ELEMENTI_DEF el
      )
   SELECT cod_elemento_new
         ,nome_elemento_new
         ,cod_tipo_elemento_new
         ,cod_tipo_fonte_new
         ,cod_tipo_cliente_new
         ,potenza_installata_new
         ,fattore_new
         ,rif_elemento_new
         ,cod_geo_new
         ,cod_geo_a_new
         ,h_anem_new
         ,coordinata_x_new
         ,coordinata_y_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento_new) tot_diff -- E' possibile che ci siano pi� record di cui uno uguale e uno no
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento_new ORDER BY data_attivazione_new) num  
                 ,tot
                 ,cod_elemento_new
                 ,nome_elemento_new
                 ,cod_tipo_elemento_new
                 ,cod_tipo_fonte_new
                 ,cod_tipo_cliente_new
                 ,potenza_installata_new
                 ,fattore_new
                 ,rif_elemento_new
                 ,cod_geo_new
                 ,cod_geo_a_new
                 ,h_anem_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
                 ,coordinata_x_new
                 ,coordinata_y_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY NEW.cod_elemento) tot
                         ,NEW.cod_elemento cod_elemento_new
                         ,NEW.descrizione nome_elemento_new
                         ,NEW.cod_tipo_elemento cod_tipo_elemento_new
                         ,NEW.cod_tipo_cliente cod_tipo_cliente_new
                         ,NEW.fonte cod_tipo_fonte_new
                         ,NEW.potenza_installata potenza_installata_new
                         ,NEW.fattore fattore_new
                         ,NEW.cod_comune rif_elemento_new
                         ,NEW.cod_geo cod_geo_new
                         ,NEW.cod_geo_a cod_geo_a_new
                         ,NEW.h_anem h_anem_new
                         ,pdata data_attivazione_new
                         ,TO_DATE('01013000','ddmmyyyy') data_disattivazione_new
                         ,OLD.nome_elemento nome_elemento_old
                         ,OLD.cod_tipo_elemento cod_tipo_elemento_old
                         ,OLD.cod_tipo_fonte cod_tipo_fonte_old 
                         ,OLD.cod_tipo_cliente cod_tipo_cliente_old
                         ,OLD.potenza_installata potenza_installata_old
                         ,OLD.fattore fattore_old
                         ,OLD.rif_elemento rif_elemento_old
                         ,OLD.cod_geo cod_geo_old
                         ,OLD.cod_geo_a cod_geo_a_old
                         ,OLD.h_anem h_anem_old
                         ,NVL(OLD.prev_data_att,OLD.data_attivazione) data_attivazione_recalc
                         ,NVL(OLD.next_data_dis,OLD.data_disattivazione) data_disattivazione_recalc
                         ,OLD.data_disattivazione data_disattivazione_old
                         ,OLD.data_attivazione data_attivazione_old
                         ,OLD.coordinata_x coordinata_x_old
                         ,OLD.coordinata_y coordinata_y_old
                         ,NEW.coordinata_x coordinata_x_new
                         ,NEW.coordinata_y coordinata_y_new
                     FROM
                          (SELECT * FROM new_data) NEW
                         ,(SELECT * FROM old_data) OLD
                    WHERE OLD.cod_elemento(+) = NEW.cod_elemento
                      AND pdata BETWEEN NVL(OLD.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))  
                      AND OLD.data_disattivazione(+) > pdata
                  )
            WHERE NVL(nome_elemento_old,' ') != NVL(nome_elemento_new,' ')  
               OR NVL(cod_tipo_elemento_old,' ') != NVL(cod_tipo_elemento_new,' ')
               OR NVL(cod_tipo_fonte_old,' ') != NVL(cod_tipo_fonte_new, ' ')
               OR NVL(cod_tipo_cliente_old,' ') != NVL(cod_tipo_cliente_new, ' ')
               OR NVL(potenza_installata_old,0) != NVL(potenza_installata_new,0)
               OR NVL(fattore_old,0) != NVL(fattore_new,0)
               OR NVL(rif_elemento_old,' ') != NVL(rif_elemento_new,' ')
               OR NVL(cod_geo_old,' ') != NVL(cod_geo_new,' ')
               OR NVL(cod_geo_a_old,' ') != NVL(cod_geo_a_new,' ')
               OR NVL(h_anem_old,0) != NVL(h_anem_new,0)
               OR NVL(coordinata_x_old,0) != NVL(coordinata_x_new,0)
               OR NVL(coordinata_y_old,0) != NVL(coordinata_y_new,0)
          )
     WHERE tot = tot_diff
       AND num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e, 
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO ELEMENTI_DEF T
        USING
       (  
        WITH new_data AS
            (
             SELECT v.* 
               FROM v_elementi_dgf v
            )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                ELEMENTI_DEF el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT NEW.cod_elemento cod_elemento
                      ,NEW.descrizione nome_elemento_new
                      ,NEW.cod_tipo_elemento cod_tipo_elemento_new
                      ,NEW.fonte cod_tipo_fonte_new
                      ,NEW.cod_tipo_cliente cod_tipo_cliente_new
                      ,NEW.potenza_installata potenza_installata_new
                      ,NEW.fattore fattore_new
                      ,NEW.cod_comune rif_elemento_new
                      ,NEW.cod_geo cod_geo_new
                      ,NEW.cod_geo_a cod_geo_a_new
                      ,NEW.h_anem h_anem_new
                      ,pdata data_attivazione_new
                      ,NVL(OLD.next_data_dis,LEAST(pdata,OLD.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,OLD.nome_elemento nome_elemento_old
                      ,OLD.cod_tipo_elemento cod_tipo_elemento_old
                      ,OLD.cod_tipo_fonte cod_tipo_fonte_old 
                      ,OLD.cod_tipo_cliente cod_tipo_cliente_old
                      ,OLD.potenza_installata potenza_installata_old
                      ,OLD.fattore fattore_old
                      ,OLD.rif_elemento rif_elemento_old
                      ,OLD.cod_geo cod_geo_old
                      ,OLD.cod_geo_a cod_geo_a_old
                      ,OLD.h_anem h_anem_old
                      ,OLD.data_disattivazione data_disattivazione_old
                      ,OLD.data_attivazione data_attivazione_old
                      ,OLD.prev_data_att data_attivazione_prec
                      ,OLD.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) NEW
                       ,(SELECT * FROM old_data) OLD
                 WHERE OLD.cod_elemento = NEW.cod_elemento
                   AND NVL(OLD.nome_elemento,' ') = NVL(NEW.descrizione,' ')  
                   AND NVL(OLD.cod_tipo_elemento,' ') = NVL(NEW.cod_tipo_elemento,' ')
                   AND NVL(OLD.cod_tipo_fonte,' ') = NVL(NEW.fonte, ' ')
                   AND NVL(OLD.cod_tipo_cliente,' ') = NVL(NEW.cod_tipo_cliente, ' ')
                   AND NVL(OLD.potenza_installata,0) = NVL(NEW.potenza_installata,0)
                   AND NVL(OLD.fattore,0) = NVL(NEW.fattore,0)
                   AND NVL(OLD.rif_elemento,' ') = NVL(NEW.cod_comune,' ')
                   AND NVL(OLD.h_anem,0) = NVL(NEW.h_anem,0)
                   AND NVL(OLD.cod_geo,' ') = NVL(NEW.cod_geo,' ')
                   AND NVL(OLD.cod_geo_a,' ') = NVL(NEW.cod_geo_a,' ')
                   AND pdata BETWEEN NVL(OLD.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (pdata  <= OLD.data_attivazione OR pdata > OLD.data_disattivazione)
                   AND NVL(OLD.next_data_dis,OLD.prev_data_att) IS NOT NULL
               )
     ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Cancello tutti quei record che hanno perso validita a causa di variazioni successive arrivate per la stessa data*/
   DELETE FROM ELEMENTI_DEF WHERE data_attivazione >= data_disattivazione;
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO ELEMENTI_DEF T
        USING
       (
        SELECT * 
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy')/*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM ELEMENTI_DEF el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,el.num
                                  FROM v_elementi_dgf el
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF;
PROCEDURE INSERT_DEF_EOLICO (pData IN DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_DEF_EOLICO';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   /* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
      Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce 
      ma si spostera' la data validita' attraverso le merge successive
   */
   INSERT INTO ELEMENTI_GDF_EOLICO
          ( cod_elemento
           ,marca_turbina
           ,modello_turbina
           ,h_mozzo
           ,vel_cutin
           ,vel_cutoff
           ,vel_max
           ,vel_recutin
           ,delta_cutoff
           ,delta_recutin
           ,wind_sect_manager
           ,start_wind_sect
           ,stop_wind_sect
           ,vel_cutoff_shdown
           ,vel_recutin_shdown
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,eol.cg_generatore
               ,eol.marca_turbina
               ,eol.modello_turbina
               ,eol.h_mozzo
               ,eol.vel_cutin
               ,eol.vel_recutin
               ,eol.vel_cutoff
               ,eol.vel_max
               ,eol.delta_cutoff
               ,eol.delta_recutin
               ,CASE WHEN eol.wind_sect_manager = 'SI' THEN 1 ELSE 0 END wind_sect_manager
               ,eol.start_wind_sect
               ,eol.stop_wind_sect
               ,eol.vel_cutoff_shdown
               ,eol.vel_recutin_shdown
          FROM 
                ELEM_DEF_EOLICO_TMP eol
               ,ELEMENTI el
         WHERE el.cod_gest_elemento = eol.cg_generatore
       )
      , old_data AS
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              ELEMENTI_GDF_EOLICO el
      )
   SELECT cod_elemento_new
         ,marca_turbina_new
         ,modello_turbina_new
         ,h_mozzo_new
         ,vel_cutin_new
         ,vel_cutoff_new
         ,vel_max_new
         ,vel_recutin_new
         ,delta_cutoff_new
         ,delta_recutin_new
         ,wind_sect_manager_new
         ,start_wind_sect_new
         ,stop_wind_sect_new
         ,vel_cutoff_shdown_new
         ,vel_recutin_shdown_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento_new) tot_diff
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento_new ORDER BY data_attivazione_new) num  
                 ,tot
                 ,cod_elemento_new
                 ,marca_turbina_new
                 ,modello_turbina_new
                 ,h_mozzo_new
                 ,vel_cutin_new
                 ,vel_cutoff_new
                 ,vel_max_new
                 ,vel_recutin_new
                 ,delta_cutoff_new
                 ,delta_recutin_new
                 ,wind_sect_manager_new
                 ,start_wind_sect_new
                 ,stop_wind_sect_new
                 ,vel_cutoff_shdown_new
                 ,vel_recutin_shdown_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY NEW.cod_elemento) tot
                          ,pdata data_attivazione_new
                          ,NEW.cod_elemento cod_elemento_new
                          ,NEW.marca_turbina marca_turbina_new
                          ,NEW.modello_turbina modello_turbina_new
                          ,NEW.h_mozzo h_mozzo_new
                          ,NEW.vel_cutin vel_cutin_new
                          ,NEW.vel_recutin vel_recutin_new
                          ,NEW.vel_cutoff vel_cutoff_new
                          ,NEW.vel_max vel_max_new
                          ,NEW.delta_cutoff delta_cutoff_new
                          ,NEW.delta_recutin delta_recutin_new
                          ,NEW.wind_sect_manager wind_sect_manager_new
                          ,NEW.start_wind_sect start_wind_sect_new
                          ,NEW.stop_wind_sect stop_wind_sect_new
                          ,NEW.vel_cutoff_shdown vel_cutoff_shdown_new
                          ,NEW.vel_recutin_shdown vel_recutin_shdown_new
                          ,TO_DATE('01013000','ddmmyyyy') data_disattivazione_new
                          ,OLD.marca_turbina marca_turbina_old
                          ,OLD.modello_turbina modello_turbina_old
                          ,OLD.h_mozzo h_mozzo_old
                          ,OLD.vel_cutin vel_cutin_old
                          ,OLD.vel_recutin vel_recutin_old
                          ,OLD.vel_cutoff vel_cutoff_old
                          ,OLD.vel_max vel_max_old
                          ,OLD.delta_cutoff delta_cutoff_old
                          ,OLD.delta_recutin delta_recutin_old
                          ,OLD.wind_sect_manager wind_sect_manager_old
                          ,OLD.start_wind_sect start_wind_sect_old
                          ,OLD.stop_wind_sect stop_wind_sect_old
                          ,OLD.vel_cutoff_shdown vel_cutoff_shdown_old
                          ,OLD.vel_recutin_shdown vel_recutin_shdown_old
                          ,NVL(OLD.prev_data_att,OLD.data_attivazione) data_attivazione_recalc
                          ,NVL(OLD.next_data_dis,OLD.data_disattivazione) data_disattivazione_recalc
                          ,OLD.data_disattivazione data_disattivazione_old
                          ,OLD.data_attivazione data_attivazione_old
                     FROM
                          (SELECT * FROM new_data) NEW
                         ,(SELECT * FROM old_data) OLD
                    WHERE OLD.cod_elemento(+) = NEW.cod_elemento
                      AND NEW.num = 1
                      AND pdata BETWEEN NVL(OLD.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))  
                      AND OLD.data_disattivazione(+) > pdata
                  )
            WHERE NVL(marca_turbina_old,' ') != NVL(marca_turbina_new,' ')
               OR NVL(modello_turbina_old,' ') != NVL(modello_turbina_new,' ')
               OR NVL(h_mozzo_old,0) != NVL(h_mozzo_new,0)
               OR NVL(vel_cutin_old,0) != NVL(vel_cutin_new,0)
               OR NVL(vel_recutin_old,0) != NVL(vel_recutin_new,0)
               OR NVL(vel_cutoff_old,0) != NVL(vel_cutoff_new,0)
               OR NVL(vel_max_old,0) != NVL(vel_max_new,0)
               OR NVL(delta_cutoff_old,0) != NVL(delta_cutoff_new,0)
               OR NVL(delta_recutin_old,0) != NVL(delta_recutin_new,0)
               OR NVL(wind_sect_manager_old,0) != NVL(wind_sect_manager_new,0)
               OR NVL(start_wind_sect_old,0) != NVL(start_wind_sect_new,0)
               OR NVL(stop_wind_sect_old,0) != NVL(stop_wind_sect_new,0)
               OR NVL(vel_cutoff_shdown_old,0) != NVL(vel_cutoff_shdown_new,0)
               OR NVL(vel_recutin_shdown_old,0) != NVL(vel_recutin_shdown_new,0)
          )
     WHERE tot = tot_diff
       AND num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e, 
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO ELEMENTI_GDF_EOLICO T
        USING
       (  
        WITH new_data AS
            (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,eol.cg_generatore
               ,eol.marca_turbina
               ,eol.modello_turbina
               ,eol.h_mozzo
               ,eol.vel_cutin
               ,eol.vel_recutin
               ,eol.vel_cutoff
               ,eol.vel_max
               ,eol.delta_cutoff
               ,eol.delta_recutin
               ,CASE WHEN eol.wind_sect_manager = 'SI' THEN 1 ELSE 0 END wind_sect_manager
               ,eol.start_wind_sect
               ,eol.stop_wind_sect
               ,eol.vel_cutoff_shdown
               ,eol.vel_recutin_shdown
          FROM 
                ELEM_DEF_EOLICO_TMP eol
               ,ELEMENTI el
         WHERE el.cod_gest_elemento = eol.cg_generatore
             )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                ELEMENTI_GDF_EOLICO el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT NEW.cod_elemento cod_elemento
                      ,NEW.marca_turbina marca_turbina_new
                      ,NEW.modello_turbina modello_turbina_new
                      ,NEW.h_mozzo h_mozzo_new
                      ,NEW.vel_cutin vel_cutin_new
                      ,NEW.vel_recutin vel_recutin_new
                      ,NEW.vel_cutoff vel_cutoff_new
                      ,NEW.vel_max vel_max_new
                      ,NEW.delta_cutoff delta_cutoff_new
                      ,NEW.delta_recutin delta_recutin_new
                      ,NEW.wind_sect_manager wind_sect_manager_new
                      ,NEW.start_wind_sect start_wind_sect_new
                      ,NEW.stop_wind_sect stop_wind_sect_new
                      ,NEW.vel_cutoff_shdown vel_cutoff_shdown_new
                      ,NEW.vel_recutin_shdown vel_recutin_shdown_new
                      ,pdata data_attivazione_new
                      ,NVL(OLD.next_data_dis,LEAST(NEW.data_attivazione,OLD.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,OLD.marca_turbina marca_turbina_old
                      ,OLD.modello_turbina modello_turbina_old
                      ,OLD.h_mozzo h_mozzo_old
                      ,OLD.vel_cutin vel_cutin_old
                      ,OLD.vel_recutin vel_recutin_old
                      ,OLD.vel_cutoff vel_cutoff_old
                      ,OLD.vel_max vel_max_old
                      ,OLD.delta_cutoff delta_cutoff_old
                      ,OLD.delta_recutin delta_recutin_old
                      ,OLD.wind_sect_manager wind_sect_manager_old
                      ,OLD.start_wind_sect start_wind_sect_old
                      ,OLD.stop_wind_sect stop_wind_sect_old
                      ,OLD.vel_cutoff_shdown vel_cutoff_shdown_old
                      ,OLD.vel_recutin_shdown vel_recutin_shdown_old
                      ,OLD.data_attivazione data_attivazione_old
                      ,OLD.data_disattivazione data_disattivazione_old
                      ,OLD.prev_data_att data_attivazione_prec
                      ,OLD.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) NEW
                       ,(SELECT * FROM old_data) OLD
                 WHERE OLD.cod_elemento = NEW.cod_elemento
                   AND NEW.num = 1
                   AND NVL(OLD.marca_turbina,' ') = NVL(NEW.marca_turbina,' ')
                   AND NVL(OLD.modello_turbina,' ') = NVL(NEW.modello_turbina,' ')
                   AND NVL(OLD.h_mozzo,0) = NVL(NEW.h_mozzo,0)
                   AND NVL(OLD.vel_cutin,0) = NVL(NEW.vel_cutin,0)
                   AND NVL(OLD.vel_recutin,0) = NVL(NEW.vel_recutin,0)
                   AND NVL(OLD.vel_cutoff,0) = NVL(NEW.vel_cutoff,0)
                   AND NVL(OLD.vel_max,0) = NVL(NEW.vel_max,0)
                   AND NVL(OLD.delta_cutoff,0) = NVL(NEW.delta_cutoff,0)
                   AND NVL(OLD.delta_recutin,0) = NVL(NEW.delta_recutin,0)
                   AND NVL(OLD.wind_sect_manager,0) = NVL(NEW.wind_sect_manager,0)
                   AND NVL(OLD.start_wind_sect,0) = NVL(NEW.start_wind_sect,0)
                   AND NVL(OLD.stop_wind_sect,0) = NVL(NEW.stop_wind_sect,0)
                   AND NVL(OLD.vel_cutoff_shdown,0) = NVL(NEW.vel_cutoff_shdown,0)
                   AND NVL(OLD.vel_recutin_shdown,0) = NVL(NEW.vel_recutin_shdown,0)
                   AND NEW.data_attivazione BETWEEN NVL(OLD.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (NEW.data_attivazione  <= OLD.data_attivazione OR NEW.data_attivazione > OLD.data_disattivazione)
                   AND NVL(OLD.next_data_dis,OLD.prev_data_att) IS NOT NULL
               )
     ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO ELEMENTI_GDF_EOLICO T
        USING
       (
        SELECT * 
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy') /*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM ELEMENTI_GDF_EOLICO el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,ROW_NUMBER() OVER(PARTITION BY el.cod_elemento ORDER BY pData) num
                                  FROM ELEM_DEF_EOLICO_TMP eol
                                      ,ELEMENTI el
                                 WHERE eol.cg_generatore = el.cod_gest_elemento
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                           AND new_data.num = 1
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF_EOLICO;
PROCEDURE INSERT_DEF_SOLARE (pData IN DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_DEF_SOLARE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   /* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
      Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce 
      ma si spostera' la data validita' attraverso le merge successive
   */
   INSERT INTO ELEMENTI_GDF_SOLARE
          ( cod_elemento
           ,orientazione
           ,id_tecnologia
           ,flag_tracking
           ,inclinazione
           ,superficie
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,sol.cg_generatore
               ,sol.orientazione
               ,CASE WHEN sol.flag_tracking = 'SI' THEN 1 ELSE 0 END flag_tracking
               ,sol.id_tecnologia
               ,sol.inclinazione
               ,sol.superficie
          FROM 
                ELEM_DEF_SOLARE_TMP sol
               ,ELEMENTI el
         WHERE el.cod_gest_elemento = sol.cg_generatore
       )
      , old_data AS
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              ELEMENTI_GDF_SOLARE el
      )
   SELECT cod_elemento
         ,orientazione_new
         ,id_tecnologia_new
         ,flag_tracking_new
         ,inclinazione_new
         ,superficie_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento) tot_diff
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento ORDER BY data_attivazione_new) num  
                 ,tot
                 ,cod_elemento
                 ,id_tecnologia_new
                 ,flag_tracking_new
                 ,orientazione_new
                 ,inclinazione_new
                 ,superficie_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY NEW.cod_elemento) tot
                         ,NEW.cod_elemento cod_elemento
                         ,NEW.data_attivazione data_attivazione_new
                         ,NEW.id_tecnologia id_tecnologia_new
                         ,NEW.flag_tracking flag_tracking_new
                         ,NEW.orientazione orientazione_new
                         ,NEW.inclinazione inclinazione_new
                         ,NEW.superficie superficie_new
                         ,TO_DATE('01013000','ddmmyyyy') data_disattivazione_new
                         ,OLD.id_tecnologia id_tecnologia_old
                         ,OLD.flag_tracking flag_tracking_old
                         ,OLD.orientazione orientazione_old
                         ,OLD.inclinazione inclinazione_old
                         ,OLD.superficie superficie_old
                         ,NVL(OLD.prev_data_att,OLD.data_attivazione) data_attivazione_recalc
                         ,NVL(OLD.next_data_dis,OLD.data_disattivazione) data_disattivazione_recalc
                         ,OLD.data_disattivazione data_disattivazione_old
                         ,OLD.data_attivazione data_attivazione_old
                     FROM
                          (SELECT * FROM new_data) NEW
                         ,(SELECT * FROM old_data) OLD
                    WHERE OLD.cod_elemento(+) = NEW.cod_elemento
                      AND NEW.num = 1
                      AND NEW.data_attivazione BETWEEN NVL(OLD.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))  
                      AND OLD.data_disattivazione(+) > NEW.data_attivazione
                  )
            WHERE NVL(orientazione_old,0) != NVL(orientazione_new,0)
               OR NVL(id_tecnologia_old,' ') != NVL(id_tecnologia_new,' ')
               OR NVL(flag_tracking_old,0) != NVL(flag_tracking_new,0)
               OR NVL(inclinazione_old,0) != NVL(inclinazione_new,0)
               OR NVL(superficie_old,0) != NVL(superficie_new,0)
          )
     WHERE tot = tot_diff
       AND num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e, 
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO ELEMENTI_GDF_SOLARE T 
        USING
       (  
        WITH new_data AS
            (
             SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
                   ,pData data_attivazione
                   ,el.cod_elemento
                   ,sol.cg_generatore
                   ,sol.orientazione
                   ,CASE WHEN sol.flag_tracking = 'SI' THEN 1 ELSE 0 END flag_tracking
                   ,sol.id_tecnologia
                   ,sol.inclinazione
                   ,sol.superficie
          FROM 
                ELEM_DEF_SOLARE_TMP sol
               ,ELEMENTI el
         WHERE el.cod_gest_elemento = sol.cg_generatore
             )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                ELEMENTI_GDF_SOLARE el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT NEW.cod_elemento cod_elemento
                      ,NEW.orientazione orientazione_new
                      ,NEW.id_tecnologia id_tecnologia_new
                      ,NEW.flag_tracking flag_tracking_new
                      ,NEW.inclinazione inclinazione_new
                      ,NEW.superficie superficie_new
                      ,NEW.data_attivazione data_attivazione_new
                      ,NVL(OLD.next_data_dis,LEAST(NEW.data_attivazione,OLD.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,OLD.orientazione orientazione_old
                      ,OLD.id_tecnologia id_tecnologia_old
                      ,OLD.flag_tracking flag_tracking_old
                      ,OLD.inclinazione inclinazione_old
                      ,OLD.superficie superficie_old
                      ,OLD.data_attivazione data_attivazione_old
                      ,OLD.data_disattivazione data_disattivazione_old
                      ,OLD.prev_data_att data_attivazione_prec
                      ,OLD.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) NEW
                       ,(SELECT * FROM old_data) OLD
                 WHERE OLD.cod_elemento = NEW.cod_elemento
                   AND NEW.num = 1
                   AND NVL(OLD.orientazione,0) = NVL(NEW.orientazione,0)
                   AND NVL(OLD.id_tecnologia,' ') = NVL(NEW.id_tecnologia,' ')
                   AND NVL(OLD.flag_tracking,0) = NVL(NEW.flag_tracking,0)
                   AND NVL(OLD.inclinazione,0) = NVL(NEW.inclinazione,0)
                   AND NVL(OLD.superficie,0) = NVL(NEW.superficie,0)
                   AND NEW.data_attivazione BETWEEN NVL(OLD.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(OLD.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (NEW.data_attivazione  <= OLD.data_attivazione OR NEW.data_attivazione > OLD.data_disattivazione)
                   AND NVL(OLD.next_data_dis,OLD.prev_data_att) IS NOT NULL
               )
     ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO ELEMENTI_GDF_SOLARE T
        USING
       (
        SELECT * 
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy') /*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM ELEMENTI_GDF_SOLARE el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,ROW_NUMBER() OVER(PARTITION BY el.cod_elemento ORDER BY pData) num
                                  FROM ELEM_DEF_SOLARE_TMP eol
                                      ,ELEMENTI el
                                 WHERE eol.cg_generatore = el.cod_gest_elemento
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                           AND new_data.num = 1
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF_SOLARE;
PROCEDURE INSERT_TRATTAMENTO_ELEMENTI (pData IN DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_TRATTAMENTO_ELEMENTI';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO TRATTAMENTO_ELEMENTI
         ( cod_elemento
          ,cod_tipo_elemento
          ,cod_tipo_fonte
          ,cod_tipo_rete
          ,cod_tipo_cliente
          ,organizzazione
          ,tipo_aggregazione
          ,cod_tipo_misura
         )
   WITH data_el AS
        (
         SELECT /*+ use_hash (el def tp) use_nl(new el)*/
                el.cod_elemento
               ,DEF.cod_tipo_elemento
               ,DEF.cod_tipo_fonte
               ,tp.cod_tipo_rete
               ,DEF.cod_tipo_cliente
               ,1 organizzazione
               ,0 tipo_aggregazione
           FROM ELEMENTI el
               ,ELEMENTI_DEF DEF
               ,TIPI_ELEMENTO tp
               ,(
                 SELECT DISTINCT cg_produttore cg_elemento 
                   FROM PRODUTTORI_TMP
                  UNION ALL
                 SELECT cg_generatore 
                   FROM PRODUTTORI_TMP
                ) NEW
          WHERE el.cod_elemento = DEF.cod_elemento
            AND tp.cod_tipo_elemento = DEF.cod_tipo_elemento
            AND NEW.cg_elemento = el.cod_gest_elemento
            AND pdata BETWEEN DEF.data_attivazione AND DEF.data_disattivazione  
        )
   SELECT NEW.* 
     FROM (
           SELECT el.* 
                 ,pkg_mago_utl.gcPotenzaAttGenerata cod_tipo_misura
             FROM data_el el
            UNION ALL
           SELECT el.* 
                 ,pkg_mago_utl.gcVelVento cod_tipo_misura
             FROM data_el el
            UNION ALL
           SELECT el.* 
                 ,pkg_mago_utl.gcDirVento cod_tipo_misura
             FROM data_el el
            UNION ALL
           SELECT el.* 
                 ,pkg_mago_utl.gcIrradiamentoSol cod_tipo_misura
             FROM data_el el
            UNION ALL
           SELECT el.* 
                 ,pkg_mago_utl.gcTemperatura cod_tipo_misura
             FROM data_el el
          ) NEW
         , TRATTAMENTO_ELEMENTI OLD
    WHERE NEW.cod_elemento = OLD.cod_elemento(+)
      AND NEW.cod_tipo_elemento = OLD.cod_tipo_elemento(+)
      AND NEW.cod_tipo_fonte = OLD.cod_tipo_fonte(+)
      AND NEW.cod_tipo_rete = OLD.cod_tipo_rete(+)
      AND NEW.cod_tipo_cliente = OLD.cod_tipo_cliente(+)
      AND NEW.organizzazione = OLD.organizzazione(+)
      AND NEW.tipo_aggregazione = OLD.tipo_aggregazione(+)
      AND NEW.cod_tipo_misura = OLD.cod_tipo_misura(+)
      AND OLD.cod_elemento IS NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_TRATTAMENTO_ELEMENTI;
PROCEDURE INSERT_MISURE (pMin IN NUMBER) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   --EXECUTE IMMEDIATE 'TRUNCATE TABLE misure_err';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE offset';
   INSERT INTO OFFSET
   SELECT ((LEVEL) * (1 /(1440/pMin))-(1 / (1440/pMin))) AS offset_start
           ,(LEVEL) * (1 / (1440/pMin)) AS offset_stop
           ,LEVEL num
     FROM DUAL
   CONNECT BY LEVEL <= (1440/pMin);
   IF pkg_mago_dgf.checkmis('MIS.NOELEM') = 1 AND pkg_mago_dgf.checkmis('MIS.NOMIS') = 1 AND pkg_mago_dgf.checkmis('MIS.DUPL') = 1 THEN
      FOR rec_data IN ( SELECT TRUNC(DATA,'mm') DATA
                          FROM MISURE_TMP
                         WHERE DATA IS NOT NULL
                         GROUP BY TRUNC(DATA,'mm')
                         ORDER BY 1  ) LOOP
      INSERT INTO MISURE_ACQUISITE
             ( cod_trattamento_elem
              ,DATA
              ,valore
             )
      WITH min_data AS 
           ( 
             SELECT /*+ use_nl(offset tmp) */ 
                    MIN(data_val) min_data_val
                   ,cg_impianto
                   ,num 
               FROM OFFSET
                   ,
                   ( SELECT TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') data_val 
                           ,cg_impianto
                           ,valore
                           ,velocita_vento
                           ,direzione_vento
                           ,irraggiamento
                           ,temperatura 
                       FROM MISURE_TMP tmp
                      WHERE TRUNC(DATA,'mm') = rec_data.DATA
                   ) tmp 
              WHERE tmp.data_val >= TRUNC(tmp.data_val) + offset_start AND tmp.data_val < TRUNC(tmp.data_val) + offset_stop 
              GROUP BY cg_impianto , TRUNC(tmp.data_val), num 
           ) 
      SELECT /*+ use_hash(mis el tr) full(mis) full(el) full(tr)*/ 
             tr.cod_trattamento_elem
            ,mis.min_data_val
            ,mis.valore 
        FROM 
            ( 
             SELECT 
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,'PAG' cod_tipo_misura
                   ,val.valore 
               FROM min_data
                   ,MISURE_TMP val 
              WHERE val.cg_impianto = min_data.cg_impianto 
                AND TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.valore IS NOT NULL
              UNION ALL 
             SELECT 
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,'VVE' cod_tipo_misura
                   ,val.velocita_vento 
               FROM min_data
                   ,MISURE_TMP val 
              WHERE val.cg_impianto = min_data.cg_impianto 
                AND TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.velocita_vento IS NOT NULL
              UNION ALL 
             SELECT 
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,'DVE' cod_tipo_misura
                   ,val.direzione_vento 
               FROM min_data
                   ,MISURE_TMP val 
              WHERE val.cg_impianto = min_data.cg_impianto 
                AND TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.direzione_vento IS NOT NULL
              UNION ALL 
             SELECT 
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,'IRR' cod_tipo_misura, val.irraggiamento 
               FROM min_data
                   ,MISURE_TMP val 
              WHERE val.cg_impianto = min_data.cg_impianto 
                AND TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.irraggiamento IS NOT NULL
              UNION ALL 
             SELECT 
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,'TMP' cod_tipo_misura
                   ,val.temperatura 
               FROM min_data
                   ,MISURE_TMP val 
              WHERE val.cg_impianto = min_data.cg_impianto 
                AND TO_DATE(TO_CHAR(DATA,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.temperatura IS NOT NULL
            ) mis 
           , 
           ( 
            SELECT /*+ use_hash (el def tp) */ 
                   el.cod_elemento 
                  ,el.cod_gest_elemento 
                  ,DEF.cod_tipo_elemento 
                  ,DEF.cod_tipo_fonte 
                  ,tp.cod_tipo_rete 
                  ,DEF.cod_tipo_cliente
                  ,1 organizzazione 
                  ,0 tipo_aggregazione 
                  ,DEF.data_attivazione 
                  ,DEF.data_disattivazione 
              FROM ELEMENTI el 
                  ,ELEMENTI_DEF DEF 
                  ,TIPI_ELEMENTO tp 
             WHERE el.cod_elemento = DEF.cod_elemento 
               AND tp.cod_tipo_elemento = DEF.cod_tipo_elemento 
           ) el 
           , TRATTAMENTO_ELEMENTI tr 
      WHERE mis.cg_impianto = el.cod_gest_elemento 
         AND mis.min_data_val BETWEEN el.data_attivazione AND el.data_disattivazione 
         AND el.cod_elemento = tr.cod_elemento 
         AND el.cod_tipo_elemento = tr.cod_tipo_elemento 
         AND el.cod_tipo_fonte = tr.cod_tipo_fonte 
         AND el.cod_tipo_rete = tr.cod_tipo_rete 
         AND el.cod_tipo_cliente = tr.cod_tipo_cliente 
         AND el.organizzazione = tr.organizzazione 
         AND el.tipo_aggregazione = tr.tipo_aggregazione 
         AND mis.cod_tipo_misura = tr.cod_tipo_misura;
      COMMIT;
         FOR rec_job IN ( SELECT DISTINCT DATA DATA, cod_tipo_misura , cod_tipo_rete, 1 stato 
                             FROM MISURE_ACQUISITE mis
                                 ,TRATTAMENTO_ELEMENTI tr
                            WHERE mis.cod_trattamento_elem = tr.cod_trattamento_elem
                              AND TRUNC(DATA,'mm') = rec_data.DATA
                              AND cod_tipo_misura ='PAG' 
                            ORDER BY DATA 
                         )  LOOP
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,1, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,2, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE); 
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,3, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
         END LOOP;
         --PKG_Mago.StdLogInit  (NULL,'PKG_Mago_DGF.Insert_Misure',NULL,vLog);
         --PKG_Mago.StdLogAddTxt('Loaded Mese: '||NVL(TO_CHAR(rec_data.data,'mm/yyyy'),'<null>'),TRUE,vLog);
         pkg_mago_utl.inslog(proc_name,'Procedure loaded month: '||NVL(TO_CHAR(rec_data.DATA,'mm/yyyy'),'<null>') ,NULL);
         --PKG_Mago.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_INF);
      END LOOP;
         --PKG_Mago.StdLogInit  (NULL,'PKG_Mago_DGF.Insert_Misure',NULL,vLog);
         --PKG_Mago.StdLogAddTxt('End Proc',TRUE,vLog);
         --PKG_Mago.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_INF);
   END IF;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
   ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_MISURE;
PROCEDURE INSERT_REL_MISURE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_REL_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   MERGE INTO REL_ELEMENTO_TIPMIS T
        USING
       (
        SELECT tr.cod_elemento
              ,tr.cod_tipo_misura
              ,NVL(OLD.tab_misure,0) tab_misure
              ,CASE WHEN tr.cod_tipo_rete='A' THEN 1 ELSE 0 END rete_at
              ,CASE WHEN tr.cod_tipo_rete='M' THEN 1 ELSE 0 END rete_mt
              ,CASE WHEN tr.cod_tipo_rete='B' THEN 1 ELSE 0 END rete_bt
          FROM TRATTAMENTO_ELEMENTI tr
              ,REL_ELEMENTO_TIPMIS OLD
         WHERE tr.cod_elemento = OLD.cod_elemento (+)
           AND tr.cod_tipo_misura = OLD.cod_tipo_misura(+)
           AND tr.tipo_aggregazione = 0
        ) s 
     ON (T.cod_elemento = s.cod_elemento AND T.cod_tipo_misura = s.cod_tipo_misura )
   WHEN MATCHED THEN 
   UPDATE SET 
          T.tab_misure = CASE WHEN s.tab_misure = 0 THEN 1 WHEN s.tab_misure = 2 THEN 3 ELSE T.tab_misure END
   WHEN NOT MATCHED THEN
   INSERT 
         ( T.cod_elemento
            ,T.cod_tipo_misura
            ,T.tab_misure
            ,T.rete_at
            ,T.rete_mt
            ,T.rete_bt)
   VALUES 
         ( s.cod_elemento
            ,s.cod_tipo_misura
            ,1
            ,s.rete_at
            ,s.rete_mt
            ,s.rete_bt);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN others THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,SUBSTR(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_REL_MISURE;
PROCEDURE insert_manutenzione(pEle IN T_ELEMAN_OBJ) AS
tipo_elemento ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
id_manut MANUTENZIONE.ID_MANUTENZIONE%TYPE;  
BEGIN
   proc_name := 'PKG_MAGO_DGF.INSERT_MANUTENZIONE';
   SELECT cod_tipo_elemento 
     INTO tipo_elemento
     FROM ELEMENTI
    WHERE cod_elemento = pEle.cod_elemento;
   IF pEle.oper = 'D' THEN
      DELETE FROM TMP_MANUTENZIONE 
       WHERE cod_elemento = pEle.cod_elemento
         AND data_inizio >= pele.datafrom
         AND data_fine <= pele.datato;
   ELSE   
      IF pEle.datatoold IS NOT NULL THEN
         UPDATE TMP_MANUTENZIONE
            SET percentuale = pele.percentuale
               ,data_fine = pele.datato
          WHERE cod_elemento = pele.cod_elemento
            AND data_inizio >= pele.datafrom
            AND data_fine <= pele.datatoold;
      ELSE
         -- per impredire che in caso di rigenerazione della Sequence (Replay) il package si invalidi uso sql dinamico 
         EXECUTE IMMEDIATE 'SELECT MANUTENZIONE_SEQ.NEXTVAL FROM DUAL' INTO id_manut;
         INSERT INTO TMP_MANUTENZIONE
            (id_manutenzione
             ,cod_elemento
             ,tipo_elemento
             ,data_inizio
             ,data_fine
             ,percentuale
            )
         VALUES
            (id_manut
             ,pele.cod_elemento
             ,tipo_elemento
             ,pele.datafrom
             ,pele.datato
             ,pele.percentuale
            );
      END IF;
   END IF;
   COMMIT;
END INSERT_MANUTENZIONE;
PROCEDURE elabora_manutenzione AS
CURSOR c_data IS
      SELECT data_inizio,data_fine,data_fine_old 
        FROM
         (
         SELECT DISTINCT man.data_inizio 
              , NVL(manold.data_fine,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))) data_fine 
              , man.data_fine_old data_fine_old
           FROM MANUTENZIONE man
               ,MANUTENZIONE manold
               ,TIPI_ELEMENTO tip
          WHERE man.elaborato = 0
            AND man.cod_elemento = manold.cod_elemento(+)
            AND man.data_fine_old > manold.data_fine(+)
            AND man.data_fine = manold.data_inizio(+) 
            AND manold.elaborato(+) = 1
            AND tip.cod_tipo_elemento = man.tipo_elemento
            AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
         )
         ORDER BY data_inizio, data_fine ;
rec_data c_data%ROWTYPE;
TYPE tdate  IS TABLE OF DATE;
rec_pi tdate;
i NUMBER;
BEGIN
proc_name := 'PKG_MAGO_DGF.ELABORA_MANUTENZIONE';
   pkg_gerarchia_dgf.linearizza_gerarchia('AMM');
   pkg_gerarchia_dgf.linearizza_gerarchia('GEO');
     --Impostazione della nuova percentuale di misura per valori di manutenzione != 0 per quei record cancellati
   MERGE INTO MISURE_ACQUISITE T
     USING (
            SELECT mis.DATA, mis.cod_trattamento_elem, 
                   (mis.valore * 100 / NVL(man.percentuale_old,man.percentuale))  valore 
              FROM MISURE_ACQUISITE mis
                  ,TIPI_MISURA tmis
                  ,TRATTAMENTO_ELEMENTI el
                  ,MANUTENZIONE man
             WHERE man.cod_elemento = el.cod_elemento
               AND man.flg_deleted = 1
               AND NVL(man.percentuale_old,man.percentuale) != 0
               AND mis.cod_trattamento_elem = el.cod_trattamento_elem
               AND el.cod_tipo_misura = tmis.cod_tipo_misura
               AND tmis.flg_manutenzione = 1
               AND DATA >= man.data_inizio
             AND DATA < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))
             ) s 
           ON (s.DATA = T.DATA AND s.cod_trattamento_elem = T.cod_trattamento_elem)
         WHEN MATCHED THEN 
       UPDATE SET
                 T.valore = s.valore  
         WHEN NOT MATCHED THEN
       INSERT 
             (cod_trattamento_elem)
       VALUES
             (NULL);

   FOR rec_data IN c_data LOOP
      BEGIN
         SELECT DISTINCT data_attivazione data_attivazione
           BULK COLLECT INTO rec_pi
           FROM MISURE_ACQUISITE_STATICHE mis
               ,TRATTAMENTO_ELEMENTI tr
               ,MANUTENZIONE man
          WHERE cod_tipo_misura = 'PI'
            AND tr.cod_elemento = man.cod_elemento
            AND man.elaborato = 0
            AND data_attivazione > rec_data.data_inizio;
            
         DELETE FROM MISURE_ACQUISITE_STATICHE 
           WHERE data_attivazione > rec_data.data_inizio
             AND data_disattivazione < GREATEST(NVL(rec_data.data_fine_old,TO_DATE('01011970','ddmmyyyy')),NVL(rec_data.data_fine,TO_DATE('01013000','ddmmyyyy')))
             AND cod_trattamento_elem  IN ( SELECT cod_trattamento_elem 
                                              FROM TRATTAMENTO_ELEMENTI tr
                                                  ,MANUTENZIONE man
                                                  ,TIPI_ELEMENTO tip
                                             WHERE cod_tipo_misura = 'PI'
                                               AND tr.cod_elemento = man.cod_elemento
                                               AND man.elaborato = 0
                                               AND tip.cod_tipo_elemento = man.tipo_elemento
                                               AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                                           );
         pkg_misure.addmisurepi(rec_data.data_inizio);
      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
         DELETE FROM MISURE_ACQUISITE_STATICHE
          WHERE data_attivazione = rec_data.data_inizio;
          pkg_misure.addmisurepi(rec_data.data_inizio);
      END;
     IF rec_data.data_fine IS NOT NULL THEN
         BEGIN
            pkg_misure.addmisurepi(rec_data.data_fine);
         EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
            DELETE FROM MISURE_ACQUISITE_STATICHE
             WHERE data_attivazione = rec_data.data_fine;
            pkg_misure.addmisurepi(rec_data.data_fine);
         END;
      END IF;
         FOR i IN rec_pi.FIRST .. rec_pi.LAST LOOP
             BEGIN
                pkg_misure.addmisurepi(rec_pi(i));        
             EXCEPTION WHEN DUP_VAL_ON_INDEX THEN  
                DELETE FROM MISURE_ACQUISITE_STATICHE
                 WHERE data_attivazione = rec_pi(i);
              pkg_misure.addmisurepi(rec_pi(i));        
             END;
        END LOOP;      
      --Ripristino le misure settate a 0 nei run precedenti 
      MERGE INTO MISURE_ACQUISITE T
      USING (
          SELECT mis.DATA, mis.cod_trattamento_elem, mis.valore 
            FROM MISURE_ACQUISITE_MANUTENZIONE mis
                ,TIPI_MISURA tmis
                ,TRATTAMENTO_ELEMENTI el
                ,MANUTENZIONE man
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND man.elaborato = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND DATA >= CASE WHEN ((NVL(man.percentuale_old,100) = 0 AND man.percentuale != 0) OR ( NVL(man.percentuale_old,man.percentuale) = 0 AND man.flg_deleted = 1)) THEN man.data_inizio ELSE NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) END
             AND DATA < NVL(man.data_fine_old,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')))
         ) s 
      ON (s.DATA = T.DATA AND s.cod_trattamento_elem = T.cod_trattamento_elem)
       WHEN MATCHED THEN 
     UPDATE SET
            T.valore = s.valore  
       WHEN NOT MATCHED THEN
     INSERT 
           (cod_trattamento_elem)
     VALUES
           (NULL);

      --Memorizzo le misure che imposter� a 0 per rollback successivi
      INSERT INTO MISURE_ACQUISITE_MANUTENZIONE 
                      (DATA,cod_trattamento_elem, valore)
          SELECT mis.DATA, mis.cod_trattamento_elem
                ,(mis.valore * 100 / CASE WHEN mis.DATA > NVL(data_fine_old,TO_DATE('01011970','ddmmyyyy')) AND man.percentuale_old IS NULL THEN 100 ELSE NVL(man.percentuale_old,100) END )valore 
            FROM MISURE_ACQUISITE mis
                ,TIPI_MISURA tmis
                ,TRATTAMENTO_ELEMENTI el
                ,MANUTENZIONE man
                ,MISURE_ACQUISITE_MANUTENZIONE misman
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND (NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) > NVL(man.data_fine_old,TO_DATE('01011970','ddmmyyyy')) OR man.percentuale_old IS NOT NULL )
             AND man.percentuale = 0
             AND man.elaborato = 0
             AND man.flg_deleted = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND mis.cod_trattamento_elem = misman.cod_trattamento_elem (+)
             AND mis.DATA = misman.DATA (+)
             AND misman.cod_trattamento_elem IS NULL
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND mis.DATA >= man.data_inizio 
             AND mis.DATA < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'));
 
      --Impostazione della nuova percentuale di misura
      MERGE INTO MISURE_ACQUISITE T
      USING (
          SELECT mis.DATA, mis.cod_trattamento_elem, 
                 (mis.valore * 100 / CASE WHEN NVL(man.percentuale_old,0) = 0 OR DATA > man.data_fine_old THEN 100 ELSE man.percentuale_old END ) 
                 * (CASE WHEN DATA >= NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) THEN 100 ELSE man.percentuale END / 100) valore 
            FROM MISURE_ACQUISITE mis
                ,TIPI_MISURA tmis
                ,TRATTAMENTO_ELEMENTI el
                ,MANUTENZIONE man
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND man.elaborato = 0
             AND man.flg_deleted = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND DATA >= man.data_inizio
             AND DATA < GREATEST(NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')),NVL(man.data_fine_old,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))))
             ) s 
      ON (s.DATA = T.DATA AND s.cod_trattamento_elem = T.cod_trattamento_elem)
       WHEN MATCHED THEN 
     UPDATE SET
            T.valore = s.valore  
       WHEN NOT MATCHED THEN
     INSERT 
           (cod_trattamento_elem)
     VALUES
           (NULL);
   
   IF NVL(rec_data.data_fine_old,TO_DATE('01011970','ddmmyyyy')) > NVL(rec_data.data_fine,TO_DATE('01013000','ddmmyyyy')) THEN   
      DELETE FROM MISURE_ACQUISITE_MANUTENZIONE
       WHERE cod_trattamento_elem IN (SELECT tr.cod_trattamento_elem 
                                        FROM TRATTAMENTO_ELEMENTI tr
                                            ,MANUTENZIONE man
                                            ,TIPI_ELEMENTO tip
                                       WHERE tr.cod_elemento = man.cod_elemento 
                                         AND man.elaborato = 0 
                                         AND tip.cod_tipo_elemento = man.tipo_elemento
                                         AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                                     )
         AND DATA >= NVL(rec_data.data_fine,TO_DATE('01013000','ddmmyyyy'))
         AND DATA < NVL(rec_data.data_fine_old,TO_DATE('01013000','ddmmyyyy'));
   END IF;
MERGE INTO MISURE_ACQUISITE_MANUTENZIONE T
  USING 
  ( SELECT tr.cod_trattamento_elem, man.data_inizio, man.data_fine, man.data_fine_old --mis.DATA
       FROM MANUTENZIONE man
           ,TRATTAMENTO_ELEMENTI tr
           ,TIPI_ELEMENTO tip
      WHERE tr.cod_elemento = man.cod_elemento
        AND ((man.percentuale != 0 AND NVL (man.percentuale_old, 100) = 0)
              OR (man.flg_deleted = 1 AND NVL(man.percentuale_old,man.percentuale) = 0)
            )
        AND tip.cod_tipo_elemento = man.tipo_elemento
        AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
        AND man.elaborato = 0
   ) s
    ON (T.cod_trattamento_elem = s.cod_trattamento_elem 
        AND T.DATA >= s.data_inizio AND T.DATA < NVL (s.data_fine_old,NVL (s.data_fine,TO_DATE ('01013000', 'ddmmyyyy')))
       )
  WHEN MATCHED THEN
    UPDATE SET T.valore = 0
    DELETE WHERE 1 = 1;

     FOR rec_job IN ( SELECT DISTINCT mis.DATA DATA, tmis.cod_tipo_misura , el.cod_tipo_rete, 1 stato
                        FROM MISURE_ACQUISITE mis
                            ,TIPI_MISURA tmis
                            ,TRATTAMENTO_ELEMENTI el
                            ,MANUTENZIONE man
                            ,TIPI_ELEMENTO tip
                       WHERE man.data_inizio = rec_data.data_inizio
                         AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
                         AND man.elaborato = 0
                         AND man.cod_elemento = el.cod_elemento
                         AND tip.cod_tipo_elemento = man.tipo_elemento
                         AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                         AND mis.cod_trattamento_elem = el.cod_trattamento_elem
                         AND el.cod_tipo_misura = tmis.cod_tipo_misura
                         AND tmis.flg_manutenzione = 1
                         AND DATA >= man.data_inizio 
                         AND DATA < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))
                       ORDER BY DATA
                    )  LOOP
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,1, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,2, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.DATA,3, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
     END LOOP;
     UPDATE MANUTENZIONE 
        SET elaborato = 1
           ,data_fine_old = NULL
           ,percentuale_old = NULL
      WHERE data_inizio = rec_data.data_inizio
        AND NVL(data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
        AND elaborato = 0;
     COMMIT;
  END LOOP; 
  DELETE FROM MANUTENZIONE
   WHERE elaborato = 1
     AND flg_deleted = 1;
  COMMIT;  
  pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);
  COMMIT;
END ELABORA_MANUTENZIONE;

END;
/
SHOW ERRORS;


