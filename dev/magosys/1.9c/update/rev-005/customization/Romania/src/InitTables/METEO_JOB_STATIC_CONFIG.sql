PROMPT METEO_JOB_STATIC_CONFIG


MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MFM.ftp.timeout.close' key ,'300' value  FROM dual
            UNION ALL
            SELECT 'MFM.ftp.timeout.connection' key ,'300' value  FROM dual
            UNION ALL
            SELECT 'MFM.ftp.timeout.read' key ,'300' value  FROM dual
            UNION ALL
            SELECT 'MFM.supplier.ftp.source.host.1' key ,'pkg_ARCDB2' value  FROM dual
            UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.1' key ,'magosys' value  FROM dual
            UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.1' key ,'Magosys' value  FROM dual
            UNION ALL
            SELECT 'MFM.supplier.folder.source.path.1' key ,'/usr/NEW/magosys/meteofile/ilmeteo' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

