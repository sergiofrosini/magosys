PROMPT METEO_JOB_STATIC_CONFIG


MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MFM.supplier.ftp.source.host.1' key ,'172.19.129.192' value  FROM dual
			UNION ALL
            SELECT 'MFM.supplier.ftp.source.username.1' key ,'stxsys' value  FROM dual
            UNION ALL
            SELECT 'MFM.supplier.ftp.source.password.1' key ,'stxsys$1' value  FROM dual
            UNION ALL
            SELECT 'MFM.ftp.timeout.close' key ,'300' value  FROM dual
            UNION ALL
            SELECT 'MFM.ftp.timeout.connection' key ,'300' value  FROM dual
            UNION ALL
            SELECT 'MFM.ftp.timeout.read' key ,'300' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;

