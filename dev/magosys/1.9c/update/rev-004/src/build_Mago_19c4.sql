SPOOL &3 APPEND

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGO rel_19c.4_SRC
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sar_admin/sar_admin_dba@arcdb1

@./InitTables/ESERCIZI_ABILITATI_MAGO.sql

@./InitTables/RETESAR_ESERCIZI.sql

conn sar_admin/sar_admin_dba

 ALTER MATERIALIZED VIEW  ESERCIZI_ABILITATI COMPILE;
 execute  dbms_mview.refresh('ESERCIZI_ABILITATI');

 ALTER MATERIALIZED VIEW  RETESAR_ESERCIZI COMPILE;
 execute  dbms_mview.refresh('RETESAR_ESERCIZI');

conn mago/mago

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
@./PackageBodies/PKG_METEO.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
@./InitTables/METEO_JOB_STATIC_CONFIG.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9c.4 SRC
PROMPT

spool off
