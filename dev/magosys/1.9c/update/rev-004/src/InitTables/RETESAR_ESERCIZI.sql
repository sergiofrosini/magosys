MERGE INTO sar_admin.retesar_esercizi t USING
( select v.* ,'pkg_arcdb1' ip_pkg1 ,'pkg_arcdb2' ip_pkg2, r.cod_sar 
from sar_admin.v_esercizio_corrente v ,
 (SELECT NVL(MAX(cod_sar),0) cod_sar FROM sar_admin.retesar_esercizi r ) r)
s
ON ( s.cod_utr = t.cod_utr AND s.cod_esercizio = t.cod_esercizio )
WHEN MATCHED THEN 
UPDATE SET
ip_pkg1 = s.ip_pkg1
, ip_pkg2 = s.ip_pkg2
WHEN NOT MATCHED THEN
INSERT 
(cod_sar
,cod_utr
,cod_esercizio
,ip_pkg1
,ip_pkg2
)
VALUES
(s.cod_sar+1
,s.cod_utr
,s.cod_esercizio
,s.ip_pkg1
,s.ip_pkg2
);

COMMIT;
