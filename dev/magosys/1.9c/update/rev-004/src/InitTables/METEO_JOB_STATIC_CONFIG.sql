PROMPT METEO_JOB_STATIC_CONFIG


MERGE INTO meteo_job_static_config t 
     USING (
            SELECT 'MPS.prediction.eolic.clearSky.gain' key ,'0.5' value  FROM dual
			UNION ALL
            SELECT 'MPS.load.forecast.external.measure.conversion.factor' key ,'0.001' value  FROM dual
            ) s        
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;
