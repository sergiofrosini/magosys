MERGE INTO sar_admin.esercizi_abilitati t USING 
( select cod_applicazione , cod_utr , cod_esercizio 
    from sar_admin.v_esercizio_corrente , sar_admin.applicazioni
   where applicazioni.cod_applicazione = 'MAGO'
) s
ON (s.cod_applicazione = t.cod_applicazione AND s.cod_utr = t.cod_utr AND s.cod_esercizio = t.cod_esercizio )
WHEN NOT MATCHED THEN
INSERT
(t.cod_applicazione , t.cod_utr , t.cod_esercizio 
)
VALUES
(s.cod_applicazione , s.cod_utr , s.cod_esercizio );

COMMIT;
