PROMPT PACKAGE BODY PKG_REPLAY;
--
-- PKG_REPLAY  (Package Body) 
--
--  Dependencies: 
--   PKG_REPLAY (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_REPLAY AS
/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9c.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

    cLine             CONSTANT INTEGER := 100;
    cSep              CONSTANT CHAR(1) := '*';
    
    cBase             CONSTANT CHAR(1) := '0';
    cRangeDate        CONSTANT CHAR(1) := '1'; -- prevede che la tabella contenga il campo DATA
    cInPeriod         CONSTANT CHAR(1) := '2'; -- prevede che la tabella contenga i campi DATA_ATTIVAZIONE e DATA_DISATTIVAZIONE
    cSpecial1         CONSTANT CHAR(1) := '3'; -- caso particolare REL_ELEMENTO_TIPMIS 
    
    gObjInElab        VARCHAR2(100);
    gTimeStartJob     TIMESTAMP; 
    
    gSplitElem        PKG_UtlGlb.t_SplitTbl;
    gLog              PKG_Logs.t_StandardLog;
    gRetArea          PKG_Mago.t_RetArea;
    
    TYPE t_lista    IS VARRAY(60) OF VARCHAR2(300);
    gTableList      t_lista := t_lista
                     ( ----- nome tabella ------         -tipoload-       ------------ condizione di filtro ----------------------------------------------- 
                      'MISURE_ACQUISITE'         ||cSep||cRangeDate||cSep,
                      'MISURE_AGGREGATE'         ||cSep||NULL      ||cSep,
                      'MISURE_ACQUISITE_STATICHE'||cSep||cInPeriod ||cSep,
                      'MISURE_AGGREGATE_STATICHE'||cSep||cInPeriod ||cSep,
                      'TRATTAMENTO_ELEMENTI'     ||cSep||cBase     ||cSep||'TIPO_AGGREGAZIONE=0 OR PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = 1 ',
                      'GERARCHIA_AMM'            ||cSep||cInPeriod ||cSep,
                      'GERARCHIA_GEO'            ||cSep||cInPeriod ||cSep,
                      'GERARCHIA_IMP_SA'         ||cSep||cInPeriod ||cSep,
                      'GERARCHIA_IMP_SN'         ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_AMM'         ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_GEO'         ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_ECP_SA'      ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_ECP_SN'      ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_ECS_SA'      ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTI_ECS_SN'      ||cSep||cInPeriod ||cSep,
                      'REL_ELEMENTO_TIPMIS'      ||cSep||cSpecial1 ||cSep,
                      'METEO_PREVISIONE'         ||cSep||cRangeDate||cSep,
                      'METEO_FILE_LETTO'         ||cSep||cBase     ||cSep,
                      'METEO_FILE_XML'           ||cSep||cBase     ||cSep,
                      'METEO_FILE_ZIP'           ||cSep||cBase     ||cSep,
                      'METEO_JOB'                ||cSep||cBase     ||cSep,
                      'SERVIZIO_MAGO'            ||cSep||cBase     ||cSep,
                      'SESSION_STATISTICS'       ||cSep||NULL      ||cSep,
                      'MANUTENZIONE'             ||cSep||NULL      ||cSep,
                      'SCHEDULED_JOBS'           ||cSep||NULL      ||cSep,
                      'SCHEDULED_TMP_GEN'        ||cSep||NULL      ||cSep,
                      'SCHEDULED_TMP_GME'        ||cSep||NULL      ||cSep,
                      'SCHEDULED_TMP_MET'        ||cSep||NULL      ||cSep,
                      'STORICO_IMPORT'           ||cSep||NULL      ||cSep,
                      'ANAGRAFICA_PUNTI'         ||cSep||cBase     ||cSep,
                      'DEFAULT_CO'               ||cSep||cBase     ||cSep,
                      'ELEMENTI_CFG'             ||cSep||cInPeriod ||cSep,
                      'KPI_INDICI'               ||cSep||NULL      ||cSep,
                      'KPI_ELEMENTI'             ||cSep||NULL      ||cSep,
                      'KPI_PARAMETRI'            ||cSep||NULL      ||cSep,
                      'KPI_RICHIESTA'            ||cSep||NULL      ||cSep,
                      'FORECAST_PARAMETRI'       ||cSep||cBase     ||cSep,
                      'ELEMENTI_DEF'             ||cSep||cInPeriod ||cSep,
                      --'ELEMENTI_GDF_EOLICO'      ||cSep||NULL      ||cSep,
                      --'ELEMENTI_GDF_SOLARE'      ||cSep||NULL      ||cSep,
                      'ELEMENTI'                 ||cSep||cBase     ||cSep
                     );

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - DBMS_OUTPUT
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LogSectionInit(pTitle VARCHAR2)  AS
/*-----------------------------------------------------------------------------------------------------------
    Scrive sul log la linea di inizio sezione  
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    PKG_Logs.TraceLog(gLog,RPAD(LPAD('>',10,'>')||' '||TRIM(pTitle||' '||LPAD('-',cLine,'-')),cLine,'-'));
 END LogSectionInit;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LogWriteTotal(pTxt1 VARCHAR2, pTxt2 VARCHAR2, pTS TIMESTAMP)  AS
/*-----------------------------------------------------------------------------------------------------------
    Scrive sul log la linea di inizio sezione  
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    PKG_Logs.TraceLog(gLog,RPAD(NVL(pTxt1,' '),39,' ')||RPAD(NVL(pTxt2,' '),50,' ')||SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - pTS),13,11));   
 END LogWriteTotal;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE HistoryLog(pProc VARCHAR2, pStart DATE, pFine DATE)  AS
/*-----------------------------------------------------------------------------------------------------------
    Scrive sul log la linea di inizio sezione  
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
     INSERT INTO LOG_HISTORY (LOGDATE, PROC_NAME, MESSAGE, ERROR_NUM, DATA_RIF, DATA_RIF_TO, CLASSE, ELAPSED) 
                      VALUES (CAST(gTimeStartJob AS DATE),
                              'PKG_Replay.'||pProc,
                              gRetArea.LONG_MSG,
                              gRetArea.RC,
                              pStart,
                              pFine,
                              PKG_Mago.gcJobClassReplay,
                              gTimeStartJob - CURRENT_TIMESTAMP
                             );
 END HistoryLog;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AlterTableConstaint(pTableName VARCHAR2, pAction VARCHAR2,pLog BOOLEAN DEFAULT TRUE)  AS
/*-----------------------------------------------------------------------------------------------------------
    Scrive sul log la linea di inizio sezione  
-----------------------------------------------------------------------------------------------------------*/
    vTime TIMESTAMP; 
 BEGIN
    gObjInElab := pTableName;
    FOR i IN (SELECT B.TABLE_NAME, B.CONSTRAINT_NAME
                FROM USER_CONSTRAINTS A 
               INNER JOIN USER_CONSTRAINTS B ON B.R_CONSTRAINT_NAME = A.CONSTRAINT_NAME
                                            AND B.CONSTRAINT_TYPE = 'R' 
               WHERE A.TABLE_NAME = pTableName
                 AND A.CONSTRAINT_TYPE = 'P' 
               ORDER BY B.TABLE_NAME, B.CONSTRAINT_NAME
             ) LOOP
        vTime := CURRENT_TIMESTAMP; 
        gObjInElab := i.TABLE_NAME||'.'||i.CONSTRAINT_NAME;
        EXECUTE IMMEDIATE 'ALTER TABLE '||i.TABLE_NAME||' '||pAction||' CONSTRAINT '||i.CONSTRAINT_NAME;
        gObjInElab := pTableName;
        --IF pAction = 'ENABLE' THEN
        --    LogWriteTotal('     '||pTableName,'     '||i.CONSTRAINT_NAME,vTime);
        --ELSE
            LogWriteTotal('     '||i.TABLE_NAME,'     '||i.CONSTRAINT_NAME,NULL);
        --END IF;
    END LOOP;
    gObjInElab := NULL;
 END AlterTableConstaint;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitWork (pDataStart      IN DATE,
                     pDataFine       IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza il lavoro  
-----------------------------------------------------------------------------------------------------------*/
    vTime TIMESTAMP := CURRENT_TIMESTAMP; 
    vVersionEsercizio V_CURRENT_VERSION.VERSION@MAGO_ESERCIZIO%TYPE; 
    vVersionReplay    V_CURRENT_VERSION.VERSION%TYPE;
    vNum              INTEGER;
    vWarning          BOOLEAN := FALSE;
 BEGIN
 
    gTimeStartJob := CURRENT_TIMESTAMP;
 
    PKG_UtlGlb.gv_FilLog    := PKG_UtlGlb.GetLogFileName('RPLY_Init');
    PKG_UtlGlb.gv_FilLogVrb := PKG_UtlGlb.GetLogFileName('RPLY_InitVrb');

    gRetArea.RC := 0;

    PKG_Logs.TraceLog(gLog,LPAD('*',cLine,'*'));
    PKG_Logs.TraceLog(gLog,'START - '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss')||'   '||
                      'periodo richiesto: '||TO_CHAR(pDataStart,'dd/mm/yyyy hh24:mi:ss')||' - '||TO_CHAR(pDataFine,'dd/mm/yyyy hh24:mi:ss'));
    PKG_Logs.TraceLog(gLog,LPAD('=',cLine,'='));
    
    -- Pulizia/Reset iniziale dati 

    gObjInElab := NULL;

    -- Log versioni MAGO di esercizio e replay   

    gObjInElab := 'V_CURRENT_VERSION';
    SELECT MAX(VERSION) INTO vVersionEsercizio FROM V_CURRENT_VERSION@MAGO_ESERCIZIO;
    SELECT MAX(VERSION) INTO vVersionReplay    FROM V_CURRENT_VERSION;
    gObjInElab := NULL;
    
    PKG_Logs.TraceLog(gLog,'Vers.MAGO Esercizio: '||vVersionEsercizio);
    PKG_Logs.TraceLog(gLog,'Vers.MAGO Replay   : '||vVersionReplay);
    PKG_Logs.TraceLog(gLog,LPAD('-',cLine,'-'));
 
    EXECUTE IMMEDIATE 'TRUNCATE TABLE LOG_HISTORY_INFO DROP STORAGE';
    DELETE LOG_HISTORY WHERE CLASSE <> PKG_Mago.gcJobClassReplay;
    COMMIT;

    LogWriteTotal('Inizializzazione eseguita...',NULL,vTime);

 END InitWork;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE EndWork(pDataStart   IN DATE,
                   pDataFine    IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Termina il lavoro  
-----------------------------------------------------------------------------------------------------------*/
    vMsg LOG_HISTORY.MESSAGE%TYPE := 'Elaborazione terminata';
 BEGIN
 
    gObjInElab := NULL;
    
    IF gRetArea.RC < 0 THEN
        vMsg := vMsg || ' con ERRORE ';
    ELSE
        UPDATE DEFAULT_CO SET START_DATE = pDataStart WHERE START_DATE IS NOT NULL;
        COMMIT;
    END IF;
    vMsg := vMsg || ' - Durata totale: '||SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - gTimeStartJob),13,11);

    IF gRetArea.SHORT_MSG IS NULL THEN
        gRetArea.SHORT_MSG := vMsg;
    ELSE
        gRetArea.SHORT_MSG := SUBSTR(gRetArea.SHORT_MSG||CHR(10)||vMsg,1,1000);
    END IF;

    vMsg := 'Periodo '||TO_CHAR(pDataStart,'dd/mm/yyyy hh24:mi:ss')||' - '||TO_CHAR(pDataFine,'dd/mm/yyyy hh24:mi:ss');
    IF gRetArea.LONG_MSG IS NULL THEN
        gRetArea.LONG_MSG := vMsg;
    ELSE
        gRetArea.LONG_MSG := SUBSTR(vMsg||CHR(10)||gRetArea.LONG_MSG,1,4000);
    END IF;

    IF gRetArea.RC < 0 THEN
        PKG_Logs.TraceLog(gLog,SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
    END IF;

    PKG_Logs.TraceLog(gLog,LPAD('=',cLine,'='));
    PKG_Logs.TraceLog(gLog,gRetArea.SHORT_MSG);
    PKG_Logs.TraceLog(gLog,LPAD('*',cLine,'*'));

 END EndWork;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ErrorGest(pProc IN VARCHAR2, pStart IN DATE, pFine IN DATE)  AS
/*-----------------------------------------------------------------------------------------------------------
    Scrive sul log la linea di inizio sezione  
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
     COMMIT;
     gRetArea.RC := SQLCODE;
     IF gObjInElab IS NULL THEN 
        gRetArea.SHORT_MSG := SUBSTR('ERRORE: ' ||SQLERRM,1,1000);
     ELSE
        gRetArea.SHORT_MSG := SUBSTR('ERRORE: ' ||SQLERRM||' in elaborazione di '||gObjInElab,1,1000);
     END IF;
     gRetArea.LONG_MSG := SUBSTR(gRetArea.SHORT_MSG||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,1,4000);
     HistoryLog(pProc,pStart,pFine);
     COMMIT;
     PKG_Logs.TraceLog(gLog,gRetArea.LONG_MSG,PKG_UtlGlb.gcTRACE_ERR);
     EndWork(pStart,pFine);
 END ErrorGest;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE DeleteLocalData(pTableName VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Svuota il contenuto della tabella 
-----------------------------------------------------------------------------------------------------------*/

    PROCEDURE TuncateTable(pTableName VARCHAR2) AS
       vTime TIMESTAMP := CURRENT_TIMESTAMP; 
    BEGIN
       EXECUTE IMMEDIATE 'TRUNCATE TABLE '||pTableName;
       LogWriteTotal(pTableName,'troncamento eseguito',vTime);
    END TuncateTable;

    PROCEDURE DeleteTable(pTableName VARCHAR2) AS
       vTot  INTEGER := 0;
       vStep INTEGER := 0;
       vTime TIMESTAMP := CURRENT_TIMESTAMP; 
    BEGIN
        LOOP
           EXECUTE IMMEDIATE 'DELETE  '||pTableName||' WHERE ROWNUM <= 30000';
           IF SQL%ROWCOUNT = 0 THEN
               EXIT;
           END IF;
           vTot := vTot + SQL%ROWCOUNT;
           vStep := vStep + 1;
           COMMIT;
        END LOOP;
        IF vStep > 1 THEN
           LogWriteTotal(pTableName,'records cancellati : '||TO_CHAR(LPAD(TO_CHAR(vTot),9,' '))||' in '||TO_CHAR(vStep)||' steps',vTime);
        ELSE 
           LogWriteTotal(pTableName,'records cancellati : '||TO_CHAR(LPAD(TO_CHAR(vTot),9,' ')),vTime);
        END IF;
    END DeleteTable;

 BEGIN
    gSplitElem := PKG_UtlGlb.SplitString(pTableName,cSep);
    gObjInElab := gSplitElem(1);
    TuncateTable(gObjInElab);
 EXCEPTION
     WHEN OTHERS THEN 
            IF SQLCODE = -02266 THEN -- ORA-02266 La tabella referenziata da chiavi esterne ...
                DeleteTable(gSplitElem(1));
                COMMIT;
            ELSE 
                RAISE;
            END IF;
 END DeleteLocalData;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE CopyFromEsercizio(pTableName VARCHAR2,
                             pDataStart DATE, 
                             pDataFine  DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Copia i dati da MAGO esercizio a MAGO di REPLAY  
-----------------------------------------------------------------------------------------------------------*/
     vTime TIMESTAMP := CURRENT_TIMESTAMP; 
     vSQL          VARCHAR2(30000);
     vNum          INTEGER;
     vTot          INTEGER := 0;
     vSelectList   VARCHAR2(4000);
     vLoadMode     CHAR(1);
     vWhereCond    VARCHAR2(200);
     vEsePart      INTEGER;
     vDtOmolStart  DATE; 
     vDtOmolFine   DATE;
 BEGIN

    gSplitElem := PKG_UtlGlb.SplitString(pTableName,cSep);

    IF gSplitElem(2) IS NULL THEN 
        RETURN;
    END IF; 

    gObjInElab := gSplitElem(1);
    vLoadMode  := gSplitElem(2);
    vWhereCond := gSplitElem(3);
    
    FOR i IN (SELECT COLUMN_ID,
                     COLUMN_NAME,
                     MAX_COL
                 FROM (SELECT T.COLUMN_ID,T.COLUMN_NAME,
                              MAX(T.COLUMN_ID)  OVER (PARTITION BY 1) MAX_COL
                         FROM USER_TAB_COLS T
                         LEFT OUTER JOIN USER_CONSTRAINTS  C ON C.TABLE_NAME = T.TABLE_NAME
                                                            AND C.CONSTRAINT_TYPE = 'P'
                         LEFT OUTER JOIN USER_CONS_COLUMNS L ON L.TABLE_NAME = T.TABLE_NAME
                                                            AND L.CONSTRAINT_NAME = C.CONSTRAINT_NAME 
                                                            AND L.COLUMN_NAME = T.COLUMN_NAME 
                        WHERE T.TABLE_NAME = gObjInElab
                        ORDER BY T.COLUMN_ID
                      )
              )
    LOOP
        vSelectList := vSelectList || i.COLUMN_NAME;
        IF i.COLUMN_ID = i.MAX_COL THEN
            vSelectList := vSelectList || ' ';
        ELSE
            vSelectList := vSelectList || ',';
        END IF;
    END LOOP;
    
    vSQL := 'INSERT INTO '||gObjInElab||' ('||vSelectList||') ';
    
    CASE 
        WHEN vLoadMode  = cBase THEN 
            IF vLoadMode = cSpecial1 THEN 
                vSelectList := REPLACE(vSelectList,'TAB_MISURE','CASE ' ||
                                                                   'WHEN PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = 1 THEN TAB_MISURE ' ||
                                                                   'WHEN BITAND(TAB_MISURE,2) = 2 THEN TAB_MISURE - 2 ' ||
                                                                   'ELSE TAB_MISURE ' ||
                                                                'END TAB_MISURE');
            END IF;
            vSQL := vSQL || 'SELECT '||vSelectList||' '||
                              'FROM '||gObjInElab||'@MAGO_ESERCIZIO';
            IF vLoadMode = cSpecial1 THEN
                 vSQL := 'SELECT * FROM (' || vSQL || ') WHERE TAB_MISURE <> 0';
                 PRINT(vSQL);
            END IF;
            IF vWhereCond IS NOT NULL THEN
                vSQL := vSQL || ' WHERE ' || vWhereCond;
            END IF;
            --PRINT(vLoadMode||' - '||gObjInElab||' - '||vSQL);
            EXECUTE IMMEDIATE vSQL;
            vTot := SQL%ROWCOUNT;
        WHEN vLoadMode = cRangeDate THEN
            IF TRIM(gObjInElab) LIKE 'MISURE_A%' THEN
                PKG_Logs.TraceLog(gLog,gObjInElab);
            END IF;
            vSQL := vSQL || 'SELECT '||vSelectList||' '||
                              'FROM '||gObjInElab||'@MAGO_ESERCIZIO '||
                             'WHERE DATA BETWEEN :pDA and :pA';
            FOR i IN (SELECT DATAVAL DATA_DA,
                             NVL(LEAD (DATAVAL) OVER (ORDER BY DATAVAL) - 1/86400,pDataFine) DATA_A
                        FROM TABLE (PKG_UTLGLB.GETCALENDARIO(pDataStart,pDataFine, 12)) -- ritorna una occorrenza ogni 2 ore
                     ) LOOP 
                IF vWhereCond IS NOT NULL THEN
                    vSQL := vSQL || ' AND (' || vWhereCond || ')';
                END IF;
                --PRINT(vLoadMode||' - '||gObjInElab||' - '||vSQL);
                EXECUTE IMMEDIATE vSQL USING i.DATA_DA, i.DATA_A;
                vNum := SQL%ROWCOUNT;
                vTot := vTot + vNum;
                --IF vNum > 0 AND TRIM(gObjInElab) LIKE 'MISURE_A%' THEN 
                IF TRIM(gObjInElab) LIKE 'MISURE_A%' THEN 
                    PKG_Logs.TraceLog(gLog,CHR(9)||CHR(9)||LPAD(TO_CHAR(vNum),9,' ')||' misure - (periodo:   '||TO_CHAR(i.DATA_DA,'dd/mm/yyyy hh24:mi:ss')||' - '||TO_CHAR(i.DATA_A,'dd/mm/yyyy hh24:mi:ss')||')');
                END IF;
                COMMIT;                
            END LOOP;
            
            IF TRIM(gObjInElab) LIKE 'MISURE_A%'  THEN
                -- copia le misure relative alla settimana omologa
                vDtOmolStart := pDataStart - 7; 
                vDtOmolFine  := vDtOmolStart + (pDataFine - pDataStart);
                IF vDtOmolFine > pDataStart THEN
                    vDtOmolFine := vDtOmolFine - 1/86400;
                END IF;
                vSQL := 'INSERT INTO '||gObjInElab||' ('||vSelectList||') ';
                vSQL := vSQL || 'SELECT ' || vSelectList || 
                                  'FROM '||gObjInElab||'@MAGO_ESERCIZIO ' ||
                                 'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) ' || -- non uso DBlink per TRATT_ELEM xche' gia' copiato da esercizio
                                 'WHERE DATA BETWEEN :pDA AND :pA ' ||
                                   'AND COD_TIPO_MISURA IN ('''||PKG_Mago.gcPotenzaAttScambiata||''','''||PKG_Mago.gcPotenzaMeteoPrevisto||''')';
                --PRINT(vLoadMode||' - '||gObjInElab||' - '||vSQL);
                vNum := 0;
                FOR i IN (SELECT DATAVAL DATA_DA,
                                 NVL(LEAD (DATAVAL) OVER (ORDER BY DATAVAL) - 1/86400,vDtOmolFine) DATA_A
                            FROM TABLE (PKG_UTLGLB.GETCALENDARIO(vDtOmolStart,vDtOmolFine, 2)) -- ritorna una occorrenza ogni 12 ore
                         ) LOOP 
                    EXECUTE IMMEDIATE vSQL USING i.DATA_DA, i.DATA_A;
                    vNum := vNum + SQL%ROWCOUNT;
                    vTot := vTot + vNum;
                END LOOP;
                PKG_Logs.TraceLog(gLog,CHR(9)||CHR(9)||LPAD(TO_CHAR(vNum),9,' ')||' misure - (sett.omol: '||TO_CHAR(vDtOmolStart,'dd/mm/yyyy hh24:mi:ss')||' - '||TO_CHAR(vDtOmolFine,'dd/mm/yyyy hh24:mi:ss')||')');
            END IF;
        WHEN vLoadMode = cInPeriod THEN  
            --vSelectList := REPLACE(vSelectList,'DATA_DISATTIVAZIONE_UNDISCONN','UNDISCONN');
            --vSelectList := REPLACE(vSelectList,'DATA_ATTIVAZIONE','CASE WHEN DATA_ATTIVAZIONE < :pDa THEN :pDa ELSE DATA_ATTIVAZIONE END DATA_ATTIVAZIONE');
            --vSelectList := REPLACE(vSelectList,'DATA_DISATTIVAZIONE'  ,'CASE WHEN DATA_DISATTIVAZIONE > :pA THEN :pTappo ELSE DATA_DISATTIVAZIONE END DATA_DISATTIVAZIONE');
            --vSelectList := REPLACE(vSelectList,'UNDISCONN','DATA_DISATTIVAZIONE_UNDISCONN');
            vSQL := vSQL || 'SELECT '||vSelectList||' '||
                              'FROM '||gObjInElab||'@MAGO_ESERCIZIO '||
                             'WHERE DATA_DISATTIVAZIONE > :pDa AND DATA_ATTIVAZIONE <= :pA';
            IF vWhereCond IS NOT NULL THEN
                vSQL := vSQL || ' AND (' || vWhereCond || ')';
            END IF;
            --PRINT(vLoadMode||' - '||gObjInElab||' - '||vSQL);
            --EXECUTE IMMEDIATE vSQL USING pDataStart, pDataStart, pDataFine, PKG_UtlGlb.gkDataTappo, pDataStart, pDataFine;
            EXECUTE IMMEDIATE vSQL USING pDataStart, pDataFine;
            vTot := SQL%ROWCOUNT;
        WHEN vLoadMode = cSpecial1 THEN 
            vSelectList := REPLACE(vSelectList,'TAB_MISURE','CASE ' ||
                                                               'WHEN PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = 1 THEN TAB_MISURE ' ||
                                                               'WHEN BITAND(TAB_MISURE,2) = 2 THEN TAB_MISURE - 2 ' ||
                                                               'ELSE TAB_MISURE ' ||
                                                            'END TAB_MISURE');
            vSQL := 'SELECT * FROM (SELECT '||vSelectList||' '||'FROM '||gObjInElab||'@MAGO_ESERCIZIO) WHERE TAB_MISURE <> 0';
            IF vWhereCond IS NOT NULL THEN
                vSQL := vSQL || ' WHERE ' || vWhereCond;
            END IF;
            vSQL := 'INSERT INTO '||gObjInElab||' ('||vSQL||') ';
            --PRINT(vLoadMode||' - '||gObjInElab||' - '||vSQL);
            EXECUTE IMMEDIATE vSQL;
            vTot := SQL%ROWCOUNT;
    END CASE;
    LogWriteTotal(gObjInElab,'records copiati    : '||LPAD(TO_CHAR(vTot),9,' '),vTime);
        
 END CopyFromEsercizio;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE RefreshMV(pMatView VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Rigenera (refresh) la vista materializzata  
-----------------------------------------------------------------------------------------------------------*/
     vTime TIMESTAMP := CURRENT_TIMESTAMP; 
 BEGIN
     gObjInElab := pMatView;
     DBMS_MVIEW.REFRESH(pMatView, 'CF');
     --DBMS_STATS.GATHER_TABLE_STATS(ownname          => USER ,
     --                              tabname          => pMatView,
     --                              estimate_percent => DBMS_STATS.AUTO_SAMPLE_SIZE,
     --                              method_opt       => 'FOR ALL COLUMNS SIZE AUTO');
     LogWriteTotal(pMatView,NULL,vTime);
 END RefreshMV;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE RebuildSeq(pSequence VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Rigenera (refresh) la vista materializzata  
-----------------------------------------------------------------------------------------------------------*/
    vSeq  USER_SEQUENCES%ROWTYPE; 
    vStmt VARCHAR2(1000);
 BEGIN
        gObjInElab := pSequence;
        BEGIN
            SELECT * INTO vSeq FROM USER_SEQUENCES@MAGO_ESERCIZIO WHERE SEQUENCE_NAME = pSequence;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN 
                 PKG_Logs.TraceLog(gLog,'SEQUENCE '||pSequence||' non trovata in MAGO ESERCIZIO');
                 RETURN;
        END;
        BEGIN
            EXECUTE IMMEDIATE 'DROP SEQUENCE '||pSequence;
        EXCEPTION
            WHEN OTHERS THEN 
                IF SQLCODE = -02289 THEN 
                    NULL;
                END IF;
        END;
        vStmt := 'CREATE SEQUENCE '||pSequence||
                     ' START WITH '||TO_CHAR(vSeq.LAST_NUMBER)||
                     ' MAXVALUE '  ||TO_CHAR(vSeq.MAX_VALUE)||
                     ' MINVALUE '  ||TO_CHAR(vSeq.MIN_VALUE)||' NOCACHE';
        IF vSeq.CYCLE_FLAG = 'N' THEN
           vStmt := vStmt || ' NOCYCLE';
        END IF;
        IF vSeq.ORDER_FLAG = 'N' THEN
           vStmt := vStmt || ' NOORDER';
        END IF;
        EXECUTE IMMEDIATE vStmt;
        LogWriteTotal(pSequence,'ricreata con MAXVAL = '||vSeq.LAST_NUMBER,NULL);
        gObjInElab := NULL;
 END RebuildSeq;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE Elabora (pDataStart      IN DATE,
                    pDataFine       IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce i dati correnti e carica le informazioni da MAGO esercizio per il periodo richiesto
-----------------------------------------------------------------------------------------------------------*/

    vNum   INTEGER;
    vTime  TIMESTAMP := CURRENT_TIMESTAMP; 

 BEGIN

    gRetArea := NULL;

    BEGIN
    
        InitWork (pDataStart,pDataFine);   
            
        -- Disattiva i constraints per consentire il troncamento delle tabelle
        LogSectionInit('DISABLE FOREIGN KEYS constaints');
        vTime := CURRENT_TIMESTAMP; 
        FOR i IN gTableList.FIRST .. gTableList.LAST LOOP
            gSplitElem := PKG_UtlGlb.SplitString(gTableList(i),cSep);
            AlterTableConstaint(gSplitElem(1),'DISABLE');
        END LOOP;
        LogWriteTotal('DISABLE FK ...',NULL,vTime);
        
        -- svuota le tabelle prima di copia da Esercizio
        LogSectionInit('Svuotamento Tabelle Mago Replay');
        FOR i IN gTableList.FIRST .. gTableList.LAST LOOP
            DeleteLocalData(gTableList(i));
        END LOOP;
        COMMIT;
 
        -- copia i dati da MAGO di esercizio
        LogSectionInit('Copia dati da DB Esercizio');
        FOR i IN REVERSE gTableList.FIRST .. gTableList.LAST LOOP
            CopyFromEsercizio(gTableList(i),pDataStart,pDataFine);
        END LOOP;
        COMMIT;

        -- inizializzazione scheduled Jobs 
        vTime := CURRENT_TIMESTAMP; 
        gObjInElab := 'SCHEDULED_TMP_GEN';
        MERGE INTO SCHEDULED_TMP_GEN B
             USING (SELECT DATA, ORGANIZZAZIONE, STATO, ID_RETE, COD_TIPO_MISURA
                      FROM (SELECT DATA, ID_RETE, COD_TIPO_MISURA
                                   FROM MISURE_ACQUISITE
                                  INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                                  INNER JOIN TIPI_RETE USING (COD_TIPO_RETE)
                                  WHERE TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                                  GROUP BY DATA, ID_RETE, COD_TIPO_MISURA
                           )
                     INNER JOIN (SELECT PKG_Mago.gcOrganizzazELE ORGANIZZAZIONE, PKG_Mago.gcStatoAttuale STATO FROM DUAL UNION ALL
                                 SELECT PKG_Mago.gcOrganizzazGEO ORGANIZZAZIONE, PKG_Mago.gcStatoAttuale STATO FROM DUAL UNION ALL
                                 SELECT PKG_Mago.gcOrganizzazAMM ORGANIZZAZIONE, PKG_Mago.gcStatoAttuale STATO FROM DUAL UNION ALL
                                 SELECT PKG_Mago.gcOrganizzazELE ORGANIZZAZIONE, PKG_Mago.gcStatoNormale STATO FROM DUAL
                                ) ON 1=1
                   ) A ON (B.DATARIF = A.DATA
              AND B.ORGANIZZAZIONE = A.ORGANIZZAZIONE
              AND B.STATO = A.STATO
              AND B.ID_RETE = A.ID_RETE
              AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
             WHEN NOT MATCHED THEN
                    INSERT (DATARIF, ORGANIZZAZIONE,   STATO,   ID_RETE,   COD_TIPO_MISURA)
                    VALUES (A.DATA,  A.ORGANIZZAZIONE, A.STATO, A.ID_RETE, A.COD_TIPO_MISURA);
        gObjInElab := 'SCHEDULED_JOBS';
        PKG_Scheduler.ConsolidaJobAggregazione(NULL,TRUE);
        SELECT COUNT(*) INTO vNum FROM SCHEDULED_JOBS;
        LogWriteTotal('SCHEDULED_JOBS','records inseriti   : '||TO_CHAR(LPAD(TO_CHAR(vNum),9,' ')),vTime);
        COMMIT;
        
        -- Riattiva i constraints precedentemente disattivati
        LogSectionInit('ENABLE FOREIGN KEYS constaints');
        vTime := CURRENT_TIMESTAMP; 
        FOR i IN gTableList.FIRST .. gTableList.LAST LOOP
            gSplitElem := PKG_UtlGlb.SplitString(gTableList(i),cSep);
            AlterTableConstaint(gSplitElem(1),'ENABLE');
        END LOOP;
        LogWriteTotal('ENABLE FK ...',NULL,vTime);

        -- refresh Mat views
        LogSectionInit('REFRESH Viste Materializzate');
        FOR i IN (SELECT NAME  FROM USER_SNAPSHOTS ORDER BY NAME) LOOP
            RefreshMV(i.NAME);
        END LOOP;

        -- inizializzazione Sequences 
        LogSectionInit('Inizializzazione Sequenze');
        FOR i IN (SELECT SEQUENCE_NAME FROM USER_SEQUENCES ORDER BY SEQUENCE_NAME) LOOP
            RebuildSeq(i.SEQUENCE_NAME);
        END LOOP;
        
        -- Fine lavoro 
        EndWork(pDataStart,pDataFine);

    END;

 END Elabora;


/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

  FUNCTION  LoadByDate   (pDataStart   IN VARCHAR2,
                          pGiorni      IN NUMBER DEFAULT gcDefaultGiorni) 
                  RETURN PKG_Mago.t_RetArea AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce i dati correnti e carica le informazioni da MAGO esercizio per il periodo richiesto
-----------------------------------------------------------------------------------------------------------*/

    vDataStart  DATE := TO_DATE(pDataStart,gcInputDateFormat);
    vDataFine   DATE := vDataStart + pGiorni;

 BEGIN

    gLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassReplay,
                                pFunzione   => 'PKG_Replay.LoadByDate',
                                pDataRif    => vDataStart);

    Elabora(vDataStart,vDataFine);
    HistoryLog('LoadByDate',vDataStart, vDataFine);
    COMMIT;
    RETURN gRetArea;

    EXCEPTION
        WHEN OTHERS THEN ErrorGest('LoadByDate',vDataStart, vDataFine);
                         RETURN gRetArea;
 END LoadByDate;

-- ----------------------------------------------------------------------------------------------------------

  FUNCTION  LoadByPeriod (pDataStart   IN VARCHAR2,
                          pDataFine    IN VARCHAR2) 
                  RETURN PKG_Mago.t_RetArea AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce i dati correnti e carica le informazioni da MAGO esercizio per il periodo richiesto
-----------------------------------------------------------------------------------------------------------*/

    vDataStart  DATE := TO_DATE(pDataStart,gcInputDateFormat);
    vDataFine   DATE := TO_DATE(pDataFine,gcInputDateFormat);

 BEGIN

    gLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassReplay,
                                pFunzione   => 'PKG_Replay.LoadByPeriod',
                                pDataRif    => vDataStart);

    Elabora (vDataStart,vDataFine);
    HistoryLog('LoadByPeriod',vDataStart, vDataFine);
    COMMIT;
    RETURN gRetArea;

    EXCEPTION
        WHEN OTHERS THEN ErrorGest('LoadByPeriod',vDataStart, vDataFine);
                         RETURN gRetArea;
 END LoadByPeriod;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_REPLAY;
/
SHOW ERRORS;


