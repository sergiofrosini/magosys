Prompt Package Body PKG_MANUTENZIONE;
--
-- PKG_MANUTENZIONE  (Package Body) 
--
--  Dependencies: 
--   PKG_MANUTENZIONE (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_MANUTENZIONE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9c.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE RebuildIndexes (pTable IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Riorganizza gli indici di una tabella
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    FOR i IN (SELECT INDEX_NAME FROM USER_INDEXES WHERE table_name = pTable ) LOOP
        EXECUTE IMMEDIATE 'ALTER INDEX '||i.INDEX_NAME||' REBUILD';
    END LOOP;

END RebuildIndexes;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaLogs AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce i log piu' vecchi
-----------------------------------------------------------------------------------------------------------*/
    vData DATE := (TRUNC(SYSDATE) - PKG_UtlGlb.GetParamGenNum('LOGDAYS',30));
BEGIN

    DELETE LOG_HISTORY_INFO WHERE RUN_ID IN (SELECT RUN_ID FROM LOG_HISTORY WHERE LOGDATE < vData);
    DELETE LOG_HISTORY WHERE LOGDATE < vData;
    PKG_Logs.TraceLog('PKG_Manutenzione.PuliziaLogs - Eliminati '||SQL%ROWCOUNT||' records da LOG_HISTORY - LOGDATE < '||TO_CHAR(vData,'dd/mm/yyyy hh24:mi:ss'));
    COMMIT;
    RebuildIndexes('LOG_HISTORY_INFO');
    RebuildIndexes('LOG_HISTORY');

END PuliziaLogs;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaMeteo AS
/*-----------------------------------------------------------------------------------------------------------
    Pulisce le tabelle METEO_FILE_xxx
-----------------------------------------------------------------------------------------------------------*/
    vData DATE;
    vNum  INTEGER;
BEGIN

    SELECT TO_NUMBER(VALUE) INTO  vNum
      FROM METEO_JOB_STATIC_CONFIG WHERE KEY = 'MFM.local.mantaining.day';

    vData := TRUNC(SYSDATE) - vNum;

    DELETE METEO_FILE_XML WHERE CREATION_DATE < vData;
    PKG_Logs.TraceLog('PKG_Manutenzione.PuliziaMeteo - Eliminati '||SQL%ROWCOUNT||' records da METEO_FILE_XML - CREATION_DATE < '||TO_CHAR(vData,'dd/mm/yyyy hh24:mi:ss'));
    
    DELETE METEO_FILE_ZIP WHERE CREATION_DATE < vData;
    PKG_Logs.TraceLog('PKG_Manutenzione.PuliziaMeteo - Eliminati '||SQL%ROWCOUNT||' records da METEO_FILE_ZIP - CREATION_DATE < '||TO_CHAR(vData,'dd/mm/yyyy hh24:mi:ss'));

    COMMIT;

    RebuildIndexes('METEO_FILE_XML');
    RebuildIndexes('METEO_FILE_ZIP');


END PuliziaMeteo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PuliziaKPI AS
/*-----------------------------------------------------------------------------------------------------------
    Pulizia Richieste e Indici KPI 
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PKG_KPI.PuliziaRichieste;
    PKG_Logs.TraceLog('PKG_Manutenzione.PuliziaKPI - Pulizia KPI eseguita');
    COMMIT;

END PuliziaKPI;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckJobs AS
/*-----------------------------------------------------------------------------------------------------------
    Stoppa e riavvia i jobs in elaborazione da oltre 12 ore
-----------------------------------------------------------------------------------------------------------*/
    vOpe BOOLEAN := FALSE;
BEGIN

    FOR i IN (SELECT JOB_NAME,LAST_START_DATE
                FROM (SELECT JOB_NAME,TO_DATE(TO_CHAR(LAST_START_DATE,'yyyy/mm/dd hh24.mi.ss'),'yyyy/mm/dd hh24.mi.ss') LAST_START_DATE 
                        FROM USER_SCHEDULER_JOBS
                       WHERE STATE = 'RUNNING'
                     )
               WHERE (SYSDATE - LAST_START_DATE) > 0.5
             ) LOOP
        vOpe:= TRUE;
        PKG_Logs.TraceLog('PKG_Manutenzione.CheckJobs - Riavvio job '||i.JOB_NAME||' in elaborazione dal '||TO_CHAR(i.LAST_START_DATE,'dd/mm/yyyy hh24:mi:ss'));
        BEGIN
            DBMS_SCHEDULER.STOP_JOB(i.JOB_NAME);
			BEGIN
				DBMS_SCHEDULER.RUN_JOB(i.JOB_NAME,FALSE);
			EXCEPTION
				WHEN OTHERS THEN
					PKG_Logs.TraceLog('PKG_Manutenzione.CheckJobs - Riavvio job '||i.JOB_NAME||' fallito - errore '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
			END;
        EXCEPTION
            WHEN OTHERS THEN
                PKG_Logs.TraceLog('PKG_Manutenzione.CheckJobs - Stop job '||i.JOB_NAME||' fallito - errore '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
        END;
     END LOOP;

     IF NOT vOpe THEN
        PKG_Logs.TraceLog('PKG_Manutenzione.CheckJobs - Nessuna operazione riavvio eseguito: OK');
     END IF;

END CheckJobs;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PeriodicMaintenance AS
/*-----------------------------------------------------------------------------------------------------------
    Pulizia Giornaliera delle tabella applicative
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    PuliziaLogs;
    PuliziaMeteo;
    PuliziaKPI;
    CheckJobs;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Manutenzione.PeriodicMaintenance'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END PeriodicMaintenance;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Manutenzione',
                          pStoreOnFile => FALSE);

END PKG_Manutenzione;
/
SHOW ERRORS;


