PROMPT 
PROMPT Inizializzazione GRUPPI_MISURA
PROMPT 

DELETE GRUPPI_MISURA 
 WHERE COD_TIPO_MISURA IN ('PAG',     -- Potenza attiva generata
                           'PAS-TR',  -- Potenza Attiva Scambiata - Trasformatore
                           'PRI-TR',  -- Potenza Reattiva Induttiva - Trasformatore
                           'PRI',     -- Potenza Reattiva Induttiva
                           'PCT-P',   -- Previsione Carico Totale-P
                           'PCT-Q',   -- Previsione Carico Totale-Q
                           'CC-P',    -- Carico Consolidato-P
                           'CC-Q',    -- Carico Consolidato-Q
                           'PMC.P1',  -- Meteo Previsto Cielo Terso in Potenza per Punto
                           'PMP.P1',  -- Meteo Previsto in Potenza per Punto
                           'PPATMP',  -- Previsione della potenza attiva Transitante
                           'PPATCT'   -- Previsione della potenza attiva Transitante Cielo Terso
						  );

COMMIT;




