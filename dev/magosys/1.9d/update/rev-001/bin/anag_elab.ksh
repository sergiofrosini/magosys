#!/usr/bin/ksh
#
#       anag_elab.ksh        (Siemens Italia SpA)
#
#       Data:           Febbraio 2016 (Primo rilascio)
#       Autore          Paolo Campi
#       Aggiornamenti:  
#
#       Modalita' di attivazione:
#         crontab
#
#       Parametri: Nessuno
#
#----------------------------------------------------------------------------------

##
# Definizione delle variabili di ambiente ORACLE 
##
cluster_control_dir_real="/opt/cmcluster/bin"
cluster_control_dir_virt="/oracle_bases/ARCDB1/oradata"
null_ctrl='aaa'

if [[ -d $cluster_control_dir_real || -d $cluster_control_dir_virt ]]; then
   export ORACLE_HOME="/oracle_bases/ARCDB2"
   export ORACLE_SID="ARCDB2"
else
   export ORACLE_HOME="/orakit/app/oracle/product/10.2.0/db_1"
   export ORACLE_SID="ARCDB1"
fi

##
# Definizione delle directory di lavoro 
##
USRDIR="/bin"
USRDIR2="/usr/bin"
BASEDIR="/usr/NEW/magosys"
SOURCEDIR="${BASEDIR}/fromEXT/anagrafiche"
DESTDIR="${BASEDIR}/fromEXT"
BCKDIR="${DESTDIR}/backup"
LOGDIR="${BASEDIR}/log"
TMPDIR="${DESTDIR}/tmp"
##
# Definizione dei comandi di sistema utilizzati nello script 
##
CP="${USRDIR}/cp"
LS="${USRDIR}/ls"
RM="${USRDIR}/rm"
MKD="${USRDIR}/mkdir -p"
TOUCH="${USRDIR}/touch"
CAT="${USRDIR}/cat"
MV="${USRDIR}/mv"
ECHO="${USRDIR}/echo -e"
GREP="${USRDIR}/grep"
CHMOD="${USRDIR}/chmod"
SQP="${ORACLE_HOME}/bin/sqlplus"

FIND="${USRDIR2}/find"
WC="${USRDIR2}/wc"
LS1="${USRDIR}/ls -1"
CUT="${USRDIR2}/cut"
SED="${USRDIR}/sed"

GIORNO=$(${USRDIR}/date +%d)
MESE=$(${USRDIR}/date +%m)
ANNO=$(${USRDIR}/date +%Y)
ORA=$(${USRDIR}/date +%H%M%S)
H=$(${USRDIR}/date +%H)
M=$(${USRDIR}/date +%M)
S=$(${USRDIR}/date +%S)
OGGI=${ANNO}${MESE}${GIORNO}

##
# Definizione delle variabili di controllo 
##
N_PAR=$#
MIN_PAR_NUMBER=0
cf=0

##
# Definizione nomi files 
##

LOGFILE=${LOGDIR}/"Anag_elab.log"

#File letto da External Table
#Viene configurato in base al tipo di Anagrafica all'interno della funzione:
# produttori.csv
# eolico.csv
# solare.csv

FILEDEST=""

#File sql per elaborazione anagrafica
LOADMIS=${TMPDIR}/"ElaboraAnagrafica.sql"

function usage
{
     ${ECHO} ""
     ${ECHO} "usage : ./${script_name} "
     ${ECHO} ""

     exit 1
}

function esci
{
     if [[ "${1}" != 0 ]]; then
          dataora=$(date +'%Y.%m.%d %H:%M:%S')
          ${ECHO} "${dataora} => Uscita anomala!!! <= " >> $LOGFILE
     fi
     
     exit $1
}

function RunElab
{
     dataora=$(date +'%Y.%m.%d %H:%M:%S')
     ${ECHO} "${dataora} - Inizio Elaborazione Anagrafica " >> $LOGFILE
     ${MKD} ${TMPDIR}
     SQL_FIL="${LOADMIS}"

        ${CAT} <<eofsql 1>${SQL_FIL}
whenever sqlerror exit 99;
whenever oserror exit 98;

BEGIN
pkg_mago_utl.run_application_group('INIT');
pkg_mago_utl.run_application_group('ANAGRAFICA');
END;
/

exit 140
eofsql

   ${SQP} mago/mago @${SQL_FIL} 1>> $LOGFILE 2>&1 

     if [[ "${?}" != "140" ]]; then
          dataora=$(date +'%Y.%m.%d %H:%M:%S')
          ${ECHO} "${dataora} - Errore Elaborazione Anagrafica " >> $LOGFILE
          ${RM} -f ${SQL_FIL}
          return 1
     fi

     ${RM} -f ${SQL_FIL}
     ${RM} -r ${TMPDIR}
     dataora=$(date +'%Y.%m.%d %H:%M:%S')
     ${ECHO} "${dataora} - Fine Elaborazione Anagrafica " >> $LOGFILE
     return 0
}

function files_aggregate
{
file_type="${1}"
case ${file_type} in
   PRODUTTORI) FILEDEST="produttori.csv"
               FILECHECK="anagrafica-produt-generatori" 
               ;;
   SOLARE) FILEDEST="solare.csv"
           FILECHECK="anagrafica-gen-solari"
           ;;
   EOLICO) FILEDEST="eolico.csv"
           FILECHECK="anagrafica-gen-eolici"
           ;;
esac
dataora=$(date +'%Y.%m.%d %H:%M:%S')
${TOUCH} $DESTDIR/$FILEDEST
${MKD} -p $BCKDIR
cd $SOURCEDIR
cf=0
NUMFILE=$( ${LS} | ${GREP} ${FILECHECK}*.csv | ${WC} -l)
if (( ${NUMFILE} > 0 )); then
   ${LS} ${FILECHECK}*.csv | while read FILE
   do
      cf=$(( ${cf} + 1))
      if (( ${cf} == 1)); then
         ${CAT} $FILE > $FILEDEST
      else
         ${SED} '1d' $FILE >> $FILEDEST
      fi
   ${MV} $FILE $BCKDIR/$FILE.${OGGI}
   done
   ${MV} $FILEDEST $DESTDIR
   ${CHMOD} 777 $DESTDIR/$FILEDEST
   ${ECHO} "${dataora}: Elaborati ${NUMFILE} files di tipo ${file_type} " >> $LOGFILE
   return 0
else
   ${ECHO} "${dataora}: Non ci sono files di anagrafica da elaborare" >> $LOGFILE
   return 1
fi
}

function main
{
     if [[ "${N_PAR}" -gt "${MIN_PAR_NUMBER}" ]]; then
         usage
     fi

     files_aggregate PRODUTTORI || return 1
     
     files_aggregate SOLARE || return 1
     
     files_aggregate EOLICO || return 1
     RunElab || return 1

     return 0
}

main || esci 1

esci 0
