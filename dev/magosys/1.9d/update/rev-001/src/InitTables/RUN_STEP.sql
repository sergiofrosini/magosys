PROMPT init table RUN_STEP

MERGE INTO RUN_STEP t
USING
(SELECT 'PKG_MAGO_UTL.STARTRUN'  proc_name, 'INIT' proc_group, '1'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.SET_DATAVAL'  proc_name, 'INIT'  proc_group, '2'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.LOAD_PRODUTTORI'  proc_name, 'ANAGRAFICA'  proc_group, '1'  stepid, null  param from dual union
SELECT 'PKG_LOCALIZZA_GEO.LOAD_PUNTI'  proc_name, 'ANAGRAFICA'  proc_group, '2'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.LOAD_DEF_SOLARE'  proc_name, 'ANAGRAFICA'  proc_group, '3'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.LOAD_DEF_EOLICO'  proc_name, 'ANAGRAFICA'  proc_group, '4'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_ELEMENT'  proc_name, 'ANAGRAFICA'  proc_group, '5'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_DEF'  proc_name, 'ANAGRAFICA'  proc_group, '6'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_DEF_SOLARE'  proc_name, 'ANAGRAFICA'  proc_group, '7'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_DEF_EOLICO'  proc_name, 'ANAGRAFICA'  proc_group, '8'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECS_AMM'  proc_name, 'ANAGRAFICA'  proc_group, '9'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECS_GEO'  proc_name, 'ANAGRAFICA'  proc_group, '10'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECP'  proc_name, 'ANAGRAFICA'  proc_group, '11'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_AMM'  proc_name, 'ANAGRAFICA'  proc_group, '12'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_GEO'  proc_name, 'ANAGRAFICA'  proc_group, '13'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LINEARIZZA_GERARCHIA_IMP'  proc_name, 'ANAGRAFICA'  proc_group, '14'  stepid, null  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LINEARIZZA_GERARCHIA_AMM'  proc_name, 'ANAGRAFICA'  proc_group, '15'  stepid, null  param from dual union
SELECT 'PKG_GERARCHIA_DGF.LINEARIZZA_GERARCHIA_GEO'  proc_name, 'ANAGRAFICA'  proc_group, '16'  stepid, null  param from dual union
SELECT 'PKG_MISURE.ADDMISUREPI'  proc_name, 'ANAGRAFICA'  proc_group, '17'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_MAGO_DGF.LOAD_MISURE'  proc_name, 'MISURE'  proc_group, '1'  stepid, null  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_TRATTAMENTO_ELEMENTI'  proc_name, 'MISURE'  proc_group, '2'  stepid, 'RUN.DATA'  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_MISURE'  proc_name, 'MISURE'  proc_group, '3'  stepid, 'RUN.MIS_INTERVAL'  param from dual union
SELECT 'PKG_MAGO_DGF.INSERT_REL_MISURE'  proc_name, 'MISURE'  proc_group, '4'  stepid, null  param from dual ) s
ON (t.proc_name=s.proc_name AND t.proc_group=s.proc_group)
WHEN MATCHED THEN UPDATE 
                  SET t.stepid= s.stepid,
					  t.param= s.param				  
WHEN NOT MATCHED THEN INSERT (proc_name,proc_group,stepid,param) 
		              VALUES (s.proc_name,s.proc_group,s.stepid,s.param);

