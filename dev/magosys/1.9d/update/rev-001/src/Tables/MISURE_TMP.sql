Prompt Table MISURE_TMP;


BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE misure_tmp ADD (COD_TIPO_MISURA VARCHAR2(6))';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -01430  -- ORA-01430: la colonna è già presente  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE misure_tmp DROP  (velocita_vento)';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -00904  -- ORA-00904: identificativo non valido (colonna non esiste)  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE misure_tmp DROP  (direzione_vento)';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -00904  -- ORA-00904: identificativo non valido (colonna non esiste)  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE misure_tmp DROP  (irraggiamento)';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -00904  -- ORA-00904: identificativo non valido (colonna non esiste)  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE misure_tmp DROP  (temperatura)';
    EXCEPTION 
        WHEN OTHERS THEN IF SQLCODE = -00904  -- ORA-00904: identificativo non valido (colonna non esiste)  
                                THEN NULL;
                                ELSE RAISE;
                         END IF;    
    END;
END;
/