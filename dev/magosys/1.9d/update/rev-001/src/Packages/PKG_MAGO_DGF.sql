create or replace PACKAGE pkg_mago_dgf AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_Dgf
   PURPOSE:    Servizi per la gestione del caricamento dai Eolico e Solare
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      20/04/2012  Paolo Campi      Created this package.
   NOTES:
*********************************************************************************************************** */
   PROCEDURE initMagoDGF;
   PROCEDURE set_dataval;
   FUNCTION  checkMis( checktype IN VARCHAR2 ) RETURN NUMBER;
   PROCEDURE load_produttori;
   PROCEDURE load_def_eolico;
   PROCEDURE load_def_solare;
   PROCEDURE load_misure;
   PROCEDURE insert_element;
   PROCEDURE insert_rel_misure;
   PROCEDURE insert_def (pData IN DATE);
   PROCEDURE insert_def_eolico (pData IN DATE);
   PROCEDURE insert_def_solare (pData IN DATE);
   PROCEDURE insert_trattamento_elementi (pData IN DATE);
   PROCEDURE insert_misure (pMin IN NUMBER);
   PROCEDURE insert_manutenzione(pEle IN T_ELEMAN_OBJ);
   PROCEDURE elabora_manutenzione;
END;
/