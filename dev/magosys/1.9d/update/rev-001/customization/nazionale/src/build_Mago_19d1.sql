SPOOL &3 APPEND

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

PROMPT =======================================================================================
PROMPT MAGONAZ rel_19d.1_SRC
PROMPT =======================================================================================

conn magonaz/magonaz

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/MISURE_TMP_EXT.sql
@./Tables/MISURE_TMP.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_MAGO_DGF.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MAGO_DGF.sql
@./PackageBodies/PKG_GERARCHIA_DGF.sql
@./PackageBodies/PKG_MAGO_UTL.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/RUN_STEP.sql

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.9d.1 SRC
PROMPT

spool off
