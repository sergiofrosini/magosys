PROMPT PACKAGE BODY       PKG_SPV_UTL AS

create or replace PACKAGE BODY PKG_SPV_UTL AS

/*============================================================================*/
    g_isMunic   NUMBER := -1;
    g_DefaultMunic   NUMBER := 0;


FUNCTION f_StatoNormale RETURN VARCHAR2 IS BEGIN RETURN 'SN'; END;
FUNCTION f_StatoAttuale RETURN VARCHAR2 IS BEGIN RETURN 'SA'; END;

FUNCTION f_StatoNormaleSuffisso RETURN VARCHAR2 IS BEGIN RETURN '_' || f_StatoNormale; END;
FUNCTION f_StatoAttualeSuffisso RETURN VARCHAR2 IS BEGIN RETURN '_' || f_StatoAttuale; END;

FUNCTION f_TipoTrasfAUI  RETURN VARCHAR2 IS BEGIN RETURN 'T' ; END;
FUNCTION f_TipoProdAUI  RETURN VARCHAR2 IS BEGIN RETURN 'U' ; END;
FUNCTION f_TipoTrasfST  RETURN VARCHAR2 IS BEGIN RETURN 'TRM'; END;
FUNCTION f_TipoProdST  RETURN VARCHAR2 IS BEGIN RETURN 'CMT'; END;

/*============================================================================*/
FUNCTION f_IsMunic RETURN NUMBER IS

  vRetVal NUMBER;
  v_num_par  NUMBER;

BEGIN
--  0 ENEL
--  1 MUNICIPALIZZATE
--
--

    IF g_IsMunic = -1 THEN
        SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamMunic, g_DumDum_s);
        v_num_par := NVL(g_DumDum_s,g_defaultMunic);
    END IF;-- TODO - esiste un flag is_municip?

    RETURN g_IsMunic;

END;

/*============================================================================*/

FUNCTION f_DismessoAUI  RETURN VARCHAR2 IS BEGIN RETURN CASE f_IsMunic WHEN 0 THEN 'U' ELSE 'X' END; END;

/*============================================================================*/

FUNCTION f_Date2DettStr(pData IN DATE) RETURN VARCHAR2 IS

BEGIN
 RETURN to_char(pData,'dd/mm/yyyy')||' ore '||to_char(pData,'hh24:mi');
END;

/*============================================================================*/

  FUNCTION f_date_to_unix (p_date  DATE,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Rome') RETURN NUMBER  AS

  BEGIN
    RETURN  1000*round((cast((FROM_TZ(CAST(p_date AS TIMESTAMP), p_in_src_tz) AT TIME ZONE 'CET') AS DATE)
    -TO_DATE('01.01.1970','dd.mm.yyyy'))*(24*60*60)
    );
  END f_date_to_unix;

/*============================================================================*/

  FUNCTION f_timestamp_to_unix (p_time  TIMESTAMP,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Rome') RETURN NUMBER AS

  BEGIN
        RETURN  1000*round((cast((FROM_TZ(p_time, p_in_src_tz) AT TIME ZONE 'CET') AS DATE)-TO_DATE('01.01.1970','dd.mm.yyyy'))*(24*60*60));
  END f_timestamp_to_unix;

/*============================================================================*/

FUNCTION f_ClientAUISi(pAlias IN VARCHAR2 DEFAULT '666') RETURN VARCHAR2 IS


    vForse VARCHAR2(100) :=  CASE nvl(pAlias,'666') WHEN '666' THEN '' ELSE pAlias||'.' END;

BEGIN

    RETURN vForse||'TRATTAMENTO = 0 AND '||vForse||'STATO IN (''A'', ''E'') ';

END;

/*============================================================================*/

FUNCTION f_AppiccicaStato(pSql IN VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##') RETURN VARCHAR2 IS

BEGIN

    RETURN
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoAttualeSuffisso)
        WHEN PKG_Mago.gcStatoNormale THEN REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoNormaleSuffisso)
    END;

END;

/*============================================================================*/

PROCEDURE sp_AppiccicaStato(pSql IN OUT VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##') IS

BEGIN
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN pSql := REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoAttualeSuffisso);
        WHEN PKG_Mago.gcStatoNormale THEN pSql := REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoNormaleSuffisso);
    END CASE;

END;

/*============================================================================*/

END PKG_SPV_UTL;
/

show errors;