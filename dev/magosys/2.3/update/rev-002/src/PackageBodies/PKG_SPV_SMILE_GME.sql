PROMPT PACKAGE BODY: PKG_SPV_SMILE_GME
create or replace PACKAGE BODY PKG_SPV_SMILE_GME AS


/* ***********************************************************************************************************
   NAME:       PKG_SPV_SMILE_GME
   PURPOSE:    Modulo Supervisione Smile GME

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      08/03/2017  Favatella F.     Created this package.               -- Spec. Version 2.2.1
   2.2.2      12/05/2017  Favatella F.     Modifiche dopo test                  -- Spec. Version 2.2.2
   2.2.13     30/11/2017  Frosini S.       commentati DBMS_OUTPUT
   2.3.2      30/05/2015  Frosini S.       aggiunti nvL(... pe rla lettura dei valori numeir dettaglio
   2.3.2      13/06/2018  Frosini S.       corretta gestione "soglie" per alalrme 20 8in realtà on/off senza soglie rosso/giallo)
  
*********************************************************************************************************** */


  procedure getDateFromAndTo (pDateFrom out date, pDateTo out date)
  is
    vVALORE  varchar2(100);
    vN_hour number;
  begin

    vVALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.n_hour');
    vN_hour := TO_NUMBER(vVALORE, '999999');
    select trunc(trunc(sysdate)-(vN_hour/24)), trunc(trunc(sysdate+1)-(vN_hour/24))-(1/(60*24*60))
		into pDateFrom, pDateTo
		from dual
		;

    --pDateFrom := TO_DATE ('23/01/2017 00.00', 'dd/mm/yyyy hh24.mi'); pDateTo := TO_DATE ('23/01/2017 23.59', 'dd/mm/yyyy hh24.mi');
  end getDateFromAndTo;


	-- Procedura che invoca la PKG_IntegrST.GetMeasureReq e recupera le misure per la supervisione SmileGme
	-- Scrive sulla tabella SPV_COLLECTOR_MEASURE
	-- Recupera 96 campioni per ogni misura all'interno delle 24 ore
	-- Da integrare con la GET_ELEMENT_FORECAST per recuperare i cod_gestionale da processare
	procedure smileGmeLoadMeasure(pDoAddSpvCollectorMeasure in NUMBER)
	is
		vNum  NUMBER := 0;
		tmdat TIMESTAMP ( 4 );
		vLst  PKG_UtlGlb.t_query_cur;
		vRec  T_MISREQ_OBJ := T_MISREQ_OBJ(NULL,NULL,NULL,NULL,NULL,NULL) ;
		vTab  T_MISREQ_ARRAY;
		--vDt1         DATE := TO_DATE ('13/11/2016 00.00', 'dd/mm/yyyy hh24.mi'); vDt2          DATE := TO_DATE ('13/11/2016 23.59', 'dd/mm/yyyy hh24.mi');
    --vDt1         DATE := TO_DATE ('23/01/2017 00.00', 'dd/mm/yyyy hh24.mi'); vDt2          DATE := TO_DATE ('23/01/2017 23.59', 'dd/mm/yyyy hh24.mi');
		vDt1         DATE; 		vDt2         DATE;
		vFon  VARCHAR2(20)  := '0!S|E|I|T|R|C|1|2|3';
		--vMis_PI VARCHAR2(10)  := 'PI';
		--vMis_PAS VARCHAR2(10)  := 'PAS';
		--vMis_PPAGCT VARCHAR2(10)  := 'PAGct';
		--vMis_PRI VARCHAR2(10)  := 'PRI';
		vReq  NUMBER := 0;
		vGst  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
		vMis  TIPI_MISURA.CODIFICA_ST%TYPE;
		vVal  NUMBER;
		vSta  INTEGER;

    --P_VALORE  varchar2(100);
    --p_n_hour number;

		--vCOLLECTOR_NAME spv_collector_measure.collector_name%type := 'SMILE_GME';
		vdata_inserimento DATE := sysdate;

		pNUM_ELEMENTI NUMBER := 2;
    vLog  PKG_Logs.t_StandardLog;

    vStrLog VARCHAR2(2000);
	BEGIN

         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure',
                               pDataRif    => sysdate);

        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure - Start';
        PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    getDateFromAndTo (vDt1, vDt2);
    null; -- DBMS_OUTPUT.PUT_LINE ('smileGmeLoadMeasure from ['||TO_CHAR (vDt1)||'] to ['||TO_CHAR(vDt2)||']');
	  vNum := 0;
		vTab := T_MISREQ_ARRAY();
		FOR i IN (
			select * from (
        select * from table(PKG_SPV_ELEMENT.f_getelement(vDt1,pTipologiaRete,pFonte,pTipoProd, pTipoElementCMT))

/*
				minus select distinct m.cod_gest_elemento
				from spv_collector_measure m
				where m.collector_name = p_collectorName
				and trunc(m.data_inserimento) = trunc(sysdate)

				minus select distinct i.cod_gest_elemento
				from spv_collector_info i
				where i.collector_name = p_collectorName
				and trunc(i.data_inserimento) = trunc(sysdate)
				ORDER BY COD_GEST_ELEMENTO				*/
			)
      --where rownum <= pNUM_ELEMENTI
    ) LOOP
			vReq := vReq + 1;
			vRec.REQ_ID             := vReq;
			vRec.COD_GEST_ELEMENTO  := i.COD_GEST_ELEMENTO;
			vRec.COD_TIPO_MISURA_ST := vMis_PI;
			vRec.DATA_DA            := vDt1;
			vRec.DATA_A             := vDt2;
			vRec.TIPI_FONTE         := vFon;
			vTab.EXTEND;
			vTab(vTab.LAST) := vRec;

			vRec.COD_TIPO_MISURA_ST := vMis_PAS;
			vTab.EXTEND;
			vTab(vTab.LAST) := vRec;

			vRec.COD_TIPO_MISURA_ST := vMis_PPAGCT;
			vTab.EXTEND;
			vTab(vTab.LAST) := vRec;

			vRec.COD_TIPO_MISURA_ST := vMis_PRI;
			vTab.EXTEND;
			vTab(vTab.LAST) := vRec;
      vNum := vNum + 1;
		END LOOP;



    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure - PKG_SPV_ELEMENT.f_getelement --->  Elementi trovati: '||vNum;
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    vNum := 0;
		tmdat := CURRENT_TIMESTAMP;
		PKG_IntegrST.GetMeasureReq(vLst,vTab);
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure - PKG_IntegrST.GetMeasureReq -->' ||TO_CHAR (CURRENT_TIMESTAMP - tmdat);
        PKG_Logs.TraceLog( vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
		LOOP
		   FETCH vLst INTO vReq, vGst, vMis, vFon, vDt1, vVal, vSta;
		   EXIT WHEN vLst%NOTFOUND;
			if vVal is not null then
			   vNum := vNum + 1;
         if pDoAddSpvCollectorMeasure = 1 then
          PKG_SUPERVISIONE.ADDSPVCOLLECTORMEASURE(p_collectorName,vdata_inserimento,vDt1,vGst,vMis,vFon,vVal);
         end if;

--			   null; -- DBMS_OUTPUT.PUT_LINE (LPAD(vNum,4)|| ' - '||
--									 LPAD(vReq,8)|| ' - '||
--									 RPAD(TO_CHAR(vGst),20,' ')||' - '||
--									 RPAD(TO_CHAR(vMis),7,' ')||' - '||
--									 RPAD(TO_CHAR(NVL(vFon,' ')),5,' ')||' - '||
--									 TO_CHAR(vDt1,'dd/mm/yyyy hh24:mi:ss')||' - '||
--									 vSta||' - '||
--									 vVal);
			end if;
		END LOOP;




        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure - ' || TO_CHAR (CURRENT_TIMESTAMP - tmdat)||'    Linee lette: '||vNum;
        PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
		CLOSE vLst;
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeLoadMeasure - End';
        PKG_Logs.TraceLog('PKG_SPV_SMILE_GME.smileGmeLoadMeasure - End', PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        PKG_Logs.StdLogPrint (vLog);

	END smileGmeLoadMeasure;--smileGmeLoadMeasure

	-- Analizzo le misure lette e raccolgo i dati da inserire nella spv_collector_info necessari al rilevamento degli allarmi
	-- Leggo le spv_soglie necessarie ad effettuare i confronti
	-- Nel caso dell'allarme 8 scarto gli elementi che hanno PI null o =0 per evitare divisione 0
	procedure checkSupervisioneSmileGme (pRefCurs       OUT PKG_UtlGlb.t_query_cur)
	is
		p_date date;
    p_dateTo date;
		P_VALORE spv_dettaglio_parametri.VALORE%type;

		--p_n_hour number;

		p_start_night number;
		p_end_night number;

		p_soglia_percent number;
		p_n_campioni_x_prod number;

	begin
		null; -- DBMS_OUTPUT.PUT_LINE ('checkSupervisioneSmileGme');

		--P_VALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.n_hour');
		--p_n_hour := TO_NUMBER(P_VALORE, '99');

		--select trunc(sysdate-(p_n_hour/24)) into p_date from dual;
		--select TO_DATE ('13/11/2016 00.00', 'dd/mm/yyyy hh24.mi') into p_date from dual; --TODO questa ¿ una data di test. Selezionare data corrente-48H
    getDateFromAndTo (p_date, p_dateTo);


		P_VALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.start_night');
		p_start_night := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.end_night');
		p_end_night := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.pas_lt_pi_soglia_percent');
		p_soglia_percent := TO_NUMBER(P_VALORE, '9999')/100;

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.pas_lt_pi_n_campioni_x_prod');
		p_n_campioni_x_prod := TO_NUMBER(P_VALORE, '9999');

		OPEN pRefCurs FOR
		with vElementi as(
		  select *
		  from (
				select * from table(PKG_SPV_ELEMENT.f_getelement(p_date,pTipologiaRete,pFonte,pTipoProd, pTipoElementCMT))
				--minus select distinct i.cod_gest_elemento from spv_collector_info i where i.collector_name = p_collectorName	and trunc(i.data_inserimento) = trunc(sysdate)
				ORDER BY COD_GEST_ELEMENTO
		  ) cod_gest_elementi,
		  --( SELECT TO_DATE ('13/11/2016 00.00', 'dd/mm/yyyy hh24.mi') + ((LEVEL -1)/96) data_misura
		  ( SELECT p_date + ((LEVEL -1)/96) data_misura
			FROM dual
			CONNECT BY LEVEL <= 96
		  ) time_axis
		)
		, misure_pi as(
		select vElem.COD_GEST_ELEMENTO, vElem.is_solare_omogeneo, vElem.data_misura, m.cod_tipo_misura, m.valore
		  from vElementi vElem
		  left join spv_collector_measure m on trunc(vElem.data_misura) = trunc(m.data_misura)
		  and  m.collector_name = p_collectorName
		  and m.cod_gest_elemento = vElem.cod_gest_elemento
		  and m.cod_tipo_misura = vMis_PI
		)
		, misure as (
		  select vElem.COD_GEST_ELEMENTO, vElem.is_solare_omogeneo, vElem.data_misura,
		  max(DECODE(m.cod_tipo_misura, vMis_PAS, m.valore, null)) AS pas_valore,
		  max(DECODE(m.cod_tipo_misura, vMis_PRI, m.valore, null)) AS pri_valore,
		  max(DECODE(m_pi.cod_tipo_misura, vMis_PI , m_pi.valore, null)) AS pi_valore,
		  max(DECODE(m.cod_tipo_misura, vMis_PPAGCT, m.valore, null)) AS pmc_valore
		  from vElementi vElem
		  left join spv_collector_measure m on vElem.data_misura = m.data_misura and  m.collector_name = p_collectorName and m.cod_gest_elemento = vElem.cod_gest_elemento
		left join misure_pi m_pi on vElem.data_misura = m_pi.data_misura and m.cod_gest_elemento = vElem.cod_gest_elemento and m_pi.cod_gest_elemento = m.cod_gest_elemento
		  GROUP BY vElem.COD_GEST_ELEMENTO, vElem.is_solare_omogeneo, vElem.data_misura
		  --ORDER BY vElem.COD_GEST_ELEMENTO, vElem.data_misura
		)
		, allarmi as (
			select m.*
			,CASE
			  WHEN m.pas_valore is null THEN 1
			  WHEN m.pri_valore is null THEN 1
			  ELSE 0
			END alarm2
			,CASE
			  WHEN m.pas_valore > m.pi_valore THEN 1
			  ELSE 0
			END alarm4
			,CASE
			  WHEN m.is_solare_omogeneo = 1 and m.pas_valore > 0
			  and (
					m.data_misura >= (p_date+(p_start_night/24)) and m.data_misura < (p_date+(p_end_night/24))
			--		m.data_misura >= TO_DATE ('13/11/2016 01.00', 'dd/mm/yyyy hh24.mi') and m.data_misura < TO_DATE ('13/11/2016 04.00', 'dd/mm/yyyy hh24.mi')
				) THEN 1
			  ELSE 0
			END alarm5 --DEVO distinguere i solari omogenei puri
			,CASE
			  WHEN m.pas_valore > m.pmc_valore THEN 1
			  ELSE 0
			END alarm6
      ,CASE
			  WHEN m.is_solare_omogeneo = 1 and m.pas_valore > m.pmc_valore THEN 1
			  ELSE 0
			END alarm7 --DEVO distinguere i solari omogenei puri
			,CASE
			  WHEN (m.pi_valore is not null) and (m.pi_valore <> 0) and (((m.pi_valore - m.pas_valore) / m.pi_valore) >=  p_soglia_percent) THEN 1
			  ELSE 0
			END alarm8
			from misure m
			--ORDER BY m.COD_GEST_ELEMENTO, m.data_misura
		)
		select a.cod_gest_elemento
		, min (a.data_misura)
		, max(alarm2) alarm2
		, max(alarm4) alarm4
		, max(alarm5) alarm5
		, max(alarm6) alarm6
    , max(alarm7) alarm7
		, case  when sum(alarm8)>p_n_campioni_x_prod then 1
			  else 0
		end alarm8
		from allarmi a
		group by a.cod_gest_elemento
		;

		EXCEPTION
		WHEN OTHERS THEN
			null; -- DBMS_OUTPUT.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	end checkSupervisioneSmileGme;

	procedure smileGmeSpvCollectorAlarm1
	is
		pCodElem       ELEMENTI.COD_ELEMENTO%TYPE;
		pGestElem      ELEMENTI.COD_GEST_ELEMENTO%TYPE;
		pNomeElem      ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
		-----------
		vNum  NUMBER := 0;
		tmdat TIMESTAMP ( 4 );
		vLst  PKG_UtlGlb.t_query_cur;
		vRec  T_MISREQ_OBJ := T_MISREQ_OBJ(NULL,NULL,NULL,NULL,NULL,NULL) ;
		vTab  T_MISREQ_ARRAY;
		--vDt1         DATE := TO_DATE ('13/11/2016 00.00', 'dd/mm/yyyy hh24.mi'); vDt2          DATE := TO_DATE ('13/11/2016 23.59', 'dd/mm/yyyy hh24.mi');
    vDt1 date; vDt2 date;
		vFon  VARCHAR2(20)  := '0!S|E|I|T|R|C|1|2|3';
		--vMis_PAS VARCHAR2(10)  := 'PAS';
		--vMis_PRI VARCHAR2(10)  := 'PRI';
		vReq  NUMBER := 0;
		vGst  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
		vMis  TIPI_MISURA.CODIFICA_ST%TYPE;
		vVal  NUMBER;
		vSta  INTEGER;
		vdata_inserimento DATE := sysdate;
		-------------------------
		pID_LIVELLO NUMBER := 3;
		p_cod_tipo_elemento varchar2(100) := 'Esercizio';

		P_VALORE spv_dettaglio_parametri.VALORE%type;
		pID_DIZIONARIO_ALLARME number;
		pDATA_ALLARME date := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := null;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := null;
		pINFO VARCHAR2(100) := null;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := null;
	begin

		null; -- DBMS_OUTPUT.PUT_LINE('smileGmeSpvCollectorAlarm1');
		PKG_ELEMENTI.GetCOprevalente(pCodElem,pGestElem,pNomeElem);

    getDateFromAndTo (vDt1, vDt2);
		--null; -- DBMS_OUTPUT.PUT_LINE('pCodElem ' || pCodElem
		--		|| ' - ' || 'pGestElem ' || pGestElem
		--		|| ' - ' || 'pNomeElem ' || pNomeElem
		--);

		vTab := T_MISREQ_ARRAY();
		vReq := vReq + 1;
		vRec.REQ_ID             := vReq;
		vRec.COD_GEST_ELEMENTO  := pGestElem;
		vRec.DATA_DA            := vDt1;
		vRec.DATA_A             := vDt2;
		vRec.TIPI_FONTE         := vFon;

		vRec.COD_TIPO_MISURA_ST := vMis_PAS;
		vTab.EXTEND;
		vTab(vTab.LAST) := vRec;

		vRec.COD_TIPO_MISURA_ST := vMis_PRI;
		vTab.EXTEND;
		vTab(vTab.LAST) := vRec;

		tmdat := CURRENT_TIMESTAMP;
		PKG_IntegrST.GetMeasureReq(vLst,vTab);
		--null; -- DBMS_OUTPUT.PUT_LINE (TO_CHAR (CURRENT_TIMESTAMP - tmdat));
		LOOP
		FETCH vLst INTO vReq, vGst, vMis, vFon, vDt1, vVal, vSta;
		EXIT WHEN vLst%NOTFOUND;
			vNum := vNum + 1;
      --PKG_SUPERVISIONE.ADDSPVCOLLECTORMEASURE(p_collectorName,vdata_inserimento,vDt1,vGst,vMis,vFon,vVal);

			--null; -- DBMS_OUTPUT.PUT_LINE (LPAD(vNum,4)|| ' - '||
			--					 	LPAD(vReq,8)|| ' - '||
			--					 	RPAD(TO_CHAR(vGst),20,' ')||' - '||
			--					 	RPAD(TO_CHAR(vMis),7,' ')||' - '||
			--					 	RPAD(TO_CHAR(NVL(vFon,' ')),5,' ')||' - '||
			--					 	TO_CHAR(vDt1,'dd/mm/yyyy hh24:mi:ss')||' - '||
			--					 	vSta||' - '||
			--					 	vVal);
		END LOOP;
		--null; -- DBMS_OUTPUT.PUT_LINE (TO_CHAR (CURRENT_TIMESTAMP - tmdat)||'    Linee lette: '||vNum);
		CLOSE vLst;

		IF (vNum < (96*2))THEN
			--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.n_hour');
      pPARAMETRI_DESCRIZIONE := to_char(vDt1,'dd-mm-yyyy');
			--pkg_supervisione.AddSpvAllarmi(PKG_SPV_SMILE_GME.pID_SISTEMA, PKG_SPV_SMILE_GME.pID_DIZIONARIO_ALLARME_1, pDATA_ALLARME, p_cod_tipo_elemento, pGestElem, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
			pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, PKG_SPV_SMILE_GME.pID_DIZIONARIO_ALLARME_1, null, pGestElem, null, null, 1, pPARAMETRI_DESCRIZIONE);
			--null; -- DBMS_OUTPUT.PUT_LINE ('Allarme inserito');
		END IF;

		EXCEPTION
			WHEN OTHERS THEN
			null; -- DBMS_OUTPUT.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	end smileGmeSpvCollectorAlarm1;

	procedure smileGmeSpvCollector
	is
		PREFCURS PKG_UtlGlb.t_query_cur;
		p_cod_gest_elemento varchar2(100);
    pPARAMETRI_DESCRIZIONE VARCHAR2(100) := null;
		data_misura date;
		p_pas number;
		p_pri number;
		p_pi number;
		p_ppagct number;
		p_alarm2 number;
		p_alarm4 number;
		p_alarm5 number;
		p_alarm6 number;
    p_alarm7 number;
		p_alarm8 number;

    p_dateFrom date;
    p_dateTo date;
	begin
		checkSupervisioneSmileGme (PREFCURS);
		null; -- DBMS_OUTPUT.PUT_LINE ('smileGmeSpvCollector');

    --pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_hour');
    getDateFromAndTo (p_dateFrom, p_dateTo);
    pPARAMETRI_DESCRIZIONE := to_char(p_dateFrom,'dd-mm-yyyy');

		LOOP
			FETCH PREFCURS INTO p_cod_gest_elemento,data_misura,p_alarm2,p_alarm4,p_alarm5,p_alarm6,p_alarm7,p_alarm8;
			EXIT WHEN PREFCURS%NOTFOUND;
		   null; -- DBMS_OUTPUT.PUT_LINE(
--			'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
--			'data_misura: '			|| data_misura 	||' - '||
--			'p_alarm2: '			|| p_alarm2 	||' - '||
--			'p_alarm4: '			|| p_alarm4 	||' - '||
----			'p_alarm5: '			|| p_alarm5 	||' - '||
--			'p_alarm6: '			|| p_alarm6 	||' - '||
  --    'p_alarm7: '			|| p_alarm7 	||' - '||
	--		'p_alarm8: '			|| p_alarm8
	--		);

			IF (p_alarm2=1) THEN
				pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_2, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;

			IF (p_alarm4=1) THEN
			   pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_4, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;

			IF (p_alarm5=1) THEN --DEVO distinguere i solari omogenei puri
			   pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_5, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;

			IF (p_alarm6=1) THEN
			   pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_6, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;

			IF (p_alarm7=1) THEN --DEVO distinguere i solari omogenei puri
			   pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_7, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;

			IF (p_alarm8=1) THEN
			   pkg_supervisione.AddSpvCollectorInfo(p_collectorName, sysdate, PKG_SPV_SMILE_GME.PID_SISTEMA, pID_DIZIONARIO_ALLARME_8, null, p_cod_gest_elemento, null, null, 1, pPARAMETRI_DESCRIZIONE);
			END IF;
		END LOOP;
		CLOSE PREFCURS;

    smileGmeSpvCollectorAlarm1();

		EXCEPTION
		WHEN OTHERS THEN
			null; -- DBMS_OUTPUT.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	end smileGmeSpvCollector;

	procedure smileGmeSpvProcessor
	is
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento varchar2(100);
		p_cod_tipo_elemento varchar2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%type;
		pID_DIZIONARIO_ALLARME number;
		pDATA_ALLARME date := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := null;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := null;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := null;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := null;

		n_allarmi_1 number;
		n_allarmi_2 number;
		n_allarmi_3 number;
		n_allarmi_4 number;
		n_allarmi_5 number;
		n_allarmi_6 number;
		n_allarmi_7 number;
		n_allarmi_8 number;

		soglia_allarme_2 number;
		soglia_allarme_3 number;
		soglia_allarme_4 number;
		soglia_allarme_8 number;

		p_dateFrom date;
    p_dateTo date;
	begin
		null; -- DBMS_OUTPUT.PUT_LINE ('smileGmeSpvProcessor');
		P_VALORE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_prod_no_pas');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_prod_scartati');
		soglia_allarme_3 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_prod_pas_gt_pi');
		soglia_allarme_4 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_prod_pas_lt_pi');
		soglia_allarme_8 := TO_NUMBER(P_VALORE, '9999');

		--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(PID_SISTEMA, 'smileGme.n_hour');

    getDateFromAndTo (p_dateFrom, p_dateTo);

		with allarmi as (
      (
       -- non considero allarme 3 (cod_gest scartati)
        select d.id_dizionario_allarme
        , nvl(sum(i.valore), 0) n_allarmi
        from spv_rel_sistema_dizionario d
        left join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = p_collectorName
        where d.id_sistema = pID_SISTEMA
        and d.id_dizionario_allarme <> pID_DIZIONARIO_ALLARME_3  and trunc(i.data_inserimento) = trunc(sysdate)
        GROUP BY d.id_dizionario_allarme
      )
      union
      -- elimino i duplicati per allarme 3 (cod_gest scartati)
      (
        select id_dizionario_allarme
        , nvl(sum(valore), 0) n_allarmi
        from (
          select distinct
            d.id_dizionario_allarme
          , i.valore valore
          , i.cod_gest_elemento
          , i.informazioni
          from spv_rel_sistema_dizionario d
          left join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = p_collectorName
          where d.id_sistema = pID_SISTEMA
          and d.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_3
          and trunc(i.data_inserimento) = trunc(p_dateFrom)
        )
        GROUP BY id_dizionario_allarme
      )
		)
		select
		 max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_1 , a.n_allarmi, null)) n_allarmi_1
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_2 , a.n_allarmi, null)) n_allarmi_2
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_3 , a.n_allarmi, null)) n_allarmi_3
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_4 , a.n_allarmi, null)) n_allarmi_4
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_5 , a.n_allarmi, null)) n_allarmi_5
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_6 , a.n_allarmi, null)) n_allarmi_6
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_7 , a.n_allarmi, null)) n_allarmi_7
		,max(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_8 , a.n_allarmi, null)) n_allarmi_8
		into n_allarmi_1,n_allarmi_2,n_allarmi_3,n_allarmi_4,n_allarmi_5,n_allarmi_6,n_allarmi_7,n_allarmi_8
		from allarmi a
		;

		OPEN pRefCurs FOR
      select * from
      (
        -- non considero allarme 3 (cod_gest scartati)
        select d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, e.cod_tipo_elemento, i.informazioni
        from spv_rel_sistema_dizionario d
        join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = p_collectorName--'SMILE_GME'
        left join elementi e on e.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
        where d.id_sistema = pID_SISTEMA
        and d.id_dizionario_allarme <> pID_DIZIONARIO_ALLARME_3  and trunc(i.data_inserimento) = trunc(sysdate)
        union
        -- elimino i duplicati per allarme 3 (cod_gest scartati)
        select distinct d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, e.cod_tipo_elemento, i.informazioni
        from spv_rel_sistema_dizionario d
        join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = p_collectorName--'SMILE_GME'
        left join elementi e on e.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
        where d.id_sistema = pID_SISTEMA
        and d.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_3  and trunc(i.data_inserimento) = trunc(p_dateFrom)
      )
      order BY id_dizionario_allarme;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);

			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 3;
			IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_2 AND n_allarmi_2 < soglia_allarme_2)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_3 AND n_allarmi_3 < soglia_allarme_3)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_4 AND n_allarmi_4 < soglia_allarme_4)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_8 AND n_allarmi_8 < soglia_allarme_8)
			)THEN
				pID_LIVELLO := 2;
			END IF;

			null; -- DBMS_OUTPUT.PUT_LINE(
	--		'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
	--		'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
	--		'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
	--		'pID_LIVELLO: ' || pID_LIVELLO
	--		);
			pkg_supervisione.AddSpvAllarmi(pID_SISTEMA, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
		END LOOP;
		CLOSE PREFCURS;

		EXCEPTION
		WHEN OTHERS THEN
			null; -- DBMS_OUTPUT.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	end smileGmeSpvProcessor;

  procedure smileGmeSpvDetail
  is
    vLog  PKG_Logs.t_StandardLog;
    vStrLog VARCHAR2(2000);

    p_dateFrom date;
    p_dateTo date;

    n_produttori            number;
    n_produttori_con_pas    number;
    n_produttori_con_pas_pc number;
    n_produttori_con_pri    number;
    n_produttori_con_pri_pc number;

    pi_valore               number;
    pi_puro_con_pas         number;
    pi_puro_con_pas_pc      number;
    pi_non_puro_con_pas     number;
    pi_non_puro_con_pas_pc  number;
    pi_con_pas              number;
    pi_con_pas_pc           number;

    n_trasformatori         number;
  begin

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                                pFunzione   => 'PKG_SPV_SMILE_GME.smileGmeSpvDetail',
                                pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvDetail - Start';
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
    --PKG_Logs.StdLogPrint (vLog);

     pkg_spv_smile_gme.getDateFromAndTo (p_dateFrom, p_dateTo);
     select p_dateFrom + 0.5 into p_dateFrom from dual; -- 0.5 mi fa arrivare alle ore 12
     --select TO_DATE ('22/01/2017 12.00', 'dd/mm/yyyy hh24.mi') into p_dateFrom from dual;


      with vElementi as ( -- lista produttori da valutare
      	select * from table(PKG_SPV_ELEMENT.f_getelement(p_dateFrom,pkg_spv_smile_gme.pTipologiaRete,pkg_spv_smile_gme.pFonte,pkg_spv_smile_gme.pTipoProd, pkg_spv_smile_gme.pTipoElementCMT))
        ORDER BY COD_GEST_ELEMENTO
      )
      ,misure as ( -- raccolta misure per la pagina dettaglio. La pi viene letta alle ore 00.00
      select m.COD_GEST_ELEMENTO, --m.data_misura,
        max(DECODE(m.cod_tipo_misura, 'PAS', m.valore, null)) AS pas_valore,
        max(DECODE(m.cod_tipo_misura, 'PRI', m.valore, null)) AS pri_valore,
        max(DECODE(m.cod_tipo_misura, 'PI', m.valore, null))  AS pi_valore
        from spv_collector_measure m
      where m.collector_name = pkg_spv_smile_gme.p_collectorName--'SMILE_GME'
      and (
        (
          m.data_misura = p_dateFrom--TO_DATE ('22/01/2017 12.00', 'dd/mm/yyyy hh24.mi')
          and m.cod_tipo_misura in ('PAS', 'PRI')
        )
        or (
          m.data_misura = trunc(p_dateFrom)--TO_DATE ('22/01/2017 12.00', 'dd/mm/yyyy hh24.mi')
          and m.cod_tipo_misura = 'PI'
        )
      )
      GROUP BY m.COD_GEST_ELEMENTO--, m.data_misura
      )
      , checkMisure as( -- effettua i confronti sulle le misure e restituisce 1 = ok, 0 = ko
      select e.cod_gest_elemento
            , CASE
              WHEN m.pas_valore IS NOT NULL THEN 1
              ELSE 0
              end pas_presente
              ,
              CASE
              WHEN m.pri_valore IS NOT NULL THEN 1
              ELSE 0
              end pri_presente
              , m.pi_valore pi_valore
              , CASE
              WHEN m.pas_valore IS NOT NULL and e.IS_SOLARE_OMOGENEO = 1 THEN m.pi_valore
              ELSE 0
              end pi_puro_con_pas
              , CASE
              WHEN m.pas_valore IS NOT NULL and e.IS_SOLARE_OMOGENEO = 0 THEN m.pi_valore
              ELSE 0
              end pi_non_puro_con_pas
              , CASE
              WHEN m.pas_valore IS NOT NULL THEN m.pi_valore
              ELSE 0
              end pi_con_pas
        from vElementi e
        left join misure m on e.COD_GEST_ELEMENTO = m.COD_GEST_ELEMENTO
      )
      -- posso calcolare i valori della pagina di dettaglio
      select count(*)                                                   n_produttori

      , nvl(sum(cs.pas_presente),0)                                           n_produttori_con_pas
      , nvl(round((sum(cs.pas_presente) * 100 ) / count(*)) ,0)                 n_produttori_con_pas_pc

      , nvl(sum(cs.pri_presente),0)                                             n_produttori_con_pri
      , nvl(round((sum(cs.pri_presente) * 100 ) / count(*)),0)                  n_produttori_con_pri_pc

      , nvl(round(sum(cs.pi_valore)/1000) ,0)                                          pi_valore
      , nvl(round(sum(cs.pi_puro_con_pas)/1000) ,0)                                    pi_puro_con_pas
      , nvl(round((sum(cs.pi_puro_con_pas) * 100 ) /sum(cs.pi_valore)) ,0)             pi_puro_con_pas_pc
      , nvl(round(sum(cs.pi_non_puro_con_pas)/1000)  ,0)                               pi_non_puro_con_pas
      , nvl(round((sum(cs.pi_non_puro_con_pas) * 100 ) / sum(cs.pi_valore)),0)   pi_non_puro_con_pas_pc
      , nvl(round(sum(cs.pi_con_pas)/1000)   ,0)                                       pi_con_pas
      , nvl(round((sum(cs.pi_con_pas) * 100 ) / sum(cs.pi_valore))  ,0)                pi_con_pas_pc

      into n_produttori, n_produttori_con_pas, n_produttori_con_pas_pc, n_produttori_con_pri, n_produttori_con_pri_pc
      , pi_valore, pi_puro_con_pas, pi_puro_con_pas_pc,  pi_non_puro_con_pas, pi_non_puro_con_pas_pc, pi_con_pas, pi_con_pas_pc
      from checkMisure cs
      ;

    select count(*)
    into n_trasformatori
    from table(PKG_SPV_ELEMENT.f_getelement(p_dateFrom,pkg_spv_smile_gme.pTipologiaRete,pkg_spv_smile_gme.pFonte,pkg_spv_smile_gme.pTipoProd, pkg_spv_smile_gme.pTipoElementTRM))
    ;

    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvDetail --> '||
    ' p_dateFrom: '||p_dateFrom|| ' - '||
    ' n_produttori: '||n_produttori|| ' - '||
    ' n_produttori_con_pas: '||n_produttori_con_pas|| ' - '||
    ' n_produttori_con_pas_pc: '||n_produttori_con_pas_pc|| ' - '||
    ' n_produttori_con_pri: '||n_produttori_con_pri|| ' - '||
    ' n_produttori_con_pri_pc: '||n_produttori_con_pri_pc|| ' - '||

    ' pi_valore: '||pi_valore|| ' - '||
    ' pi_puro_con_pas: '||pi_puro_con_pas|| ' - '||
    ' pi_puro_con_pas_pc: '||pi_puro_con_pas_pc|| ' - '||
    ' pi_non_puro_con_pas: '||pi_non_puro_con_pas|| ' - '||
    ' pi_non_puro_con_pas_pc: '||pi_non_puro_con_pas_pc|| ' - '||
    ' pi_con_pas: '||pi_con_pas|| ' - '||
    ' pi_con_pas_pc: '||pi_con_pas_pc|| ' - '||

    ' n_trasformatori: '||n_trasformatori
    ;
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.dataRiferimento'				, pkg_supervisione.getTimestamp(p_dateFrom), null);

    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numProduttoriTot'				, n_produttori 				    , null);
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numProduttoriPas'				, n_produttori_con_pas 		, null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numProduttoriPasPercent', n_produttori_con_pas_pc	, null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numProduttoriPri'				, n_produttori_con_pri 		, null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numProduttoriPriPercent', n_produttori_con_pri_pc	, null );

    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'foglie.numTrasformatoriTot'		, n_trasformatori 			  , null);

    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piTot'							        , pi_valore 				      , null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piProdPuriPas'					    , pi_puro_con_pas 			  , null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piProdPuriPasPercent'				, pi_puro_con_pas_pc		  , null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piProdPas'						      , pi_non_puro_con_pas		  , null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piProdPasPercent'					  , pi_non_puro_con_pas_pc 	, null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piTotPas'							      , pi_con_pas				      , null );
    pkg_supervisione.SetSpvDettaglioParametri( pkg_spv_smile_gme.PID_SISTEMA, 'pi.piTotPasPercent'					  , pi_con_pas_pc 			    , null );

    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvDetail - End';
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

    EXCEPTION
		WHEN OTHERS THEN
			null; -- DBMS_OUTPUT.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
  end smileGmeSpvDetail;

  procedure smileGmeSpvMain(pDoDeleteOldSpvInfo in number default 1
							,pDoDeleteOldSpvMeasure in number default 1
							,pDoDeleteSpvAlarm in number default 1
							,pDoDeleteTodaySpvInfo in number default 1
              ,pDoLoadMeasure in number default 1
              ,pDoCollector in number default 1
              ,pDoProcessor in number default 1
              ,pDoDetail in number default 1
  )
  is
    vLog  PKG_Logs.t_StandardLog;
    vStrLog VARCHAR2(2000);

    vIsSupervisionEnabled varchar2(100);
    p_dateFrom date;
    p_dateTo date;
  begin

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                                pFunzione   => 'PKG_SPV_SMILE_GME.smileGmeSpvMain',
                                pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Start';
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
    --PKG_Logs.StdLogPrint (vLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(PKG_SPV_SMILE_GME.PID_SISTEMA, 'smileGme.supervision_enabled');

  	vStrLog :=  'smileGmeSpvMain --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteOldSpvMeasure: ' || pDoDeleteOldSpvMeasure ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
      'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
      'pDoLoadMeasure: ' || pDoLoadMeasure ||' - '||
      'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    if vIsSupervisionEnabled = 'true' or vIsSupervisionEnabled = 'TRUE' then
      getDateFromAndTo (p_dateFrom, p_dateTo);

          -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
      if pDoDeleteTodaySpvInfo = 1 then
          vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - delete Today spv_collector_info ';
          PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
          null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        delete from spv_collector_info
        where collector_name = p_collectorName
        and trunc(data_inserimento) = trunc(sysdate)
        and id_rel_sistema_dizionario <> pID_DIZIONARIO_ALLARME_3
        ;
      end if;

      -- cancello TUTTE le info generate dal collector SMILE_GME prima di gc-48h
      if pDoDeleteOldSpvInfo = 1 then
          vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - delete Old spv_collector_info ';
          PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
          null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        delete from spv_collector_info
        where collector_name = p_collectorName
        and data_inserimento < p_dateFrom
        and id_rel_sistema_dizionario in (
          select id_rel_sistema_dizionario
          from spv_rel_sistema_dizionario
          where id_sistema = PID_SISTEMA
        );
      end if;

      -- cancello TUTTE le misure generate dal collector SMILE_GME prima di gc-48h-30giorni
      if pDoDeleteOldSpvMeasure = 1 then
          vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - delete Old spv_collector_measure ';
          PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
          null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        delete from spv_collector_measure
        where collector_name = p_collectorName
        and data_misura < (p_dateFrom-30)-- conservo 30 giorni di storia
        ;
      end if;

      -- invoco la procedura che legge le misure e le inserisce nella spv_collector_measure
      if pDoLoadMeasure = 1 then
          vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Calling smileGmeLoadMeasure';
          PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
          null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        pkg_spv_smile_gme.smileGmeLoadMeasure();
      end if;

      -- eseguo il collector e creo le spv_collector_info
      if pDoCollector = 1 then
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Calling smileGmeSpvCollector ';
        PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        pkg_spv_smile_gme.smileGmeSpvCollector();
      end if;

      -- cancello TUTTI gli allarmi generati dal collector SMILE_GME
      if pDoDeleteSpvAlarm = 1 then
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain -  delete from spv_allarmi ';
        PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
        delete from spv_allarmi where id_rel_sistema_dizionario in (select id_rel_sistema_dizionario from spv_rel_sistema_dizionario where id_sistema = PID_SISTEMA);
      end if;

      -- eseguo il processor e creo le spv_allarmi
      if pDoProcessor = 1 then
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Calling smileGmeSpvProcessor ';
        PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

        pkg_spv_smile_gme.smileGmeSpvProcessor();
      end if;

      -- valorizzo il pannello di dettaglio
      if pDoDetail = 1 then
        vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Calling smileGmeSpvDetail ';
        PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
        null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

        pkg_spv_smile_gme.smileGmeSpvDetail();
      end if;
    else
      vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - Supervision Pas is NOT enabled';
      PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
      null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);
    end if;


    vStrLog := 'PKG_SPV_SMILE_GME.smileGmeSpvMain - End';
    PKG_Logs.TraceLog(vStrLog, PKG_UtlGlb.gcTrace_VRB);
    null; -- DBMS_OUTPUT.PUT_LINE(vStrLog);

    PKG_Logs.StdLogPrint (vLog);
  end smileGmeSpvMain;

END PKG_SPV_SMILE_GME;
/

show errors;