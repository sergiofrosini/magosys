PROMPT Package Body PKG_SPV_ANAG_AUI

create or replace PACKAGE BODY      PKG_SPV_ANAG_AUI AS


/* ***********************************************************************************************************
   NAME:       PKG_SPV_ANAG_AUI
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      10/04/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1
   2.2.7      03/08/2017  Frosini S.       modificata PKG_SPV_AUI.sp_CheckProdAUI  MAGO-1176/1260
              14/09/2017  Frosini S.       modificata PKG_SPV_AUI.sp_CheckProdAUI limitatat a elem MAGO MAGO-1178 et al.
   2.2.8      18/09/2017  Frosini S.       modificata PKG_SPV_AUI.sp_CheckProdAUI errori su totali MAGO-1271
   2.2.10     05/10/2017  Frosini S.       MAGO-858 modificata PKG_SPV_AUI.sp_CheckProdAUI x produtotri privi di Comune Istat - corretta ricerca considerando i disconnessi nella ricerca
              09/10/2017  Frosini S.       MAGO-1238 modificata sp_CheckSbarreMAGO da stato normale a stato attuale
   2.2.11     13/10/2017  Frosini S.       MAGO-1423 modificata PKG_SPV_AUI.sp_CheckProdAUI controllo cosphi<0 (non uguale)
   2.2.13     01/12/2017  Frosini S.       MAGO-1426 eliminato allarme 7 e modificato test da produttori a GENERATORI

   2.3.1      14/11/2017  Frosini S.       MAGO-1426 modificata PKG_SPV_AUI.sp_CheckProdAUI controllo tipo fonte passa da client a generatore, sparisce msg 07
   2.3.2      30/05/2018  Frosini S.       modifcata lettura clientiBT (non reperibili) in detail
   2.3.3      20/07/2018  Frosini S.       modificato test err30 TRM "cod_org_nodo is not null"
              27/07/2018  Frosini S.       MAGO-1767 corrette alcvne select in AuiSpvDetail sporcate durante la merge delle veriosni
   2.3.5      24/10/2017  Frosini S.       MAGO-1834 Verificare export elenco dal dettaglio: non contiene cliente bt           
/*********************************************************************************************************** */

    g_tipoMago_Esercizio CONSTANT elementi.cod_tipo_elemento%TYPE := 'ESE';
    g_tipoMago_Client    CONSTANT elementi.cod_tipo_elemento%TYPE := 'CMT';
    g_tipoMago_Trasf     CONSTANT elementi.cod_tipo_elemento%TYPE := 'TRM';
    g_tipoMago_Montante  CONSTANT elementi.cod_tipo_elemento%TYPE := 'LMT';
    g_tipoMago_Comune    CONSTANT elementi.cod_tipo_elemento%TYPE := 'COM';
    g_tipoMago_CabinaP   CONSTANT elementi.cod_tipo_elemento%TYPE := 'CPR';
    g_tipoMago_sbarraSec CONSTANT elementi.cod_tipo_elemento%TYPE := 'SCS';


    g_ProdPuro_AUI CONSTANT VARCHAR2(2) := 'PP';
    g_AutoProd_AUI CONSTANT VARCHAR2(2) := 'AP';
    g_ProdAlia_AUI CONSTANT VARCHAR2(2) := 'PD';

    g_CollectorName         CONSTANT spv_collector_info.collector_name%TYPE 	:='ANAGRAFICA_AUI';
	g_IdSistema             CONSTANT spv_dettaglio_parametri.ID_SISTEMA%TYPE 	:= 6;

	gID_CHIAVE_SPV_ENABLED  CONSTANT VARCHAR2(100 BYTE) := 'aui.supervision_enabled';

    g_SogliaLinea CONSTANT NUMBER := 6000; -- 6MW

    gMis_PI     CONSTANT VARCHAR2(10)  := 'PI';
    gMis_PAS    CONSTANT VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT CONSTANT VARCHAR2(10)  := 'PMC';
    gMis_PRI    CONSTANT VARCHAR2(10)  := 'PRI';


    g_tit_CodGes CONSTANT VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.codGes';
    g_tit_TipCli CONSTANT VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipCli';
    g_tit_TipEle CONSTANT VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipEle';
    g_tit_PI     CONSTANT VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.PI';

    g_tit_NoData CONSTANT VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.NoData';

    g_sep_CSV_excel CONSTANT VARCHAR2(1) := ';';

    --mago.supervision.detail.anagraficaAui.elenco.title


    -- 01 err 28,'Produttore senza generatori sottesi');
    -- 02 err 29,'Generatore MT con Potenza Installata non definita');
    -- 03 err 30,'Generatore MT o TR MT/BT con fonte d'energia definita ma con PI <= 0');
    -- 04 err 31,'La PI del produttore e' diverso dalla somma PI dei generatori sottesi');
    -- 05 err 32,'Gruppo con potenza superiore a 6MW senza linea MT dedicata');
    -- 06 err 33,'Fonte di energia per GENERATORI MT o TR MT/BT non definita in AUI');
    -- 08 err 35,'Produttori MT o TR MT/BT senza valorizzazione campo Tipo Produttore');
    -- 09 err 36,'POD0 ai quali corrispondono piu' di un Produttore MT');
    -- 10 err 37,'Clienti MT marcati come ¿¿non produttori¿¿ ma con generatori associati');
    -- 11 err 38,'Clienti MT o TR MT/BT con cos(phi)<0 che darebbero luogo a PI<=0');
    -- 12 err 39,'Montanti di linea MT marcati erroneamente come clienti MT');
    -- 13 err 40,'Sbarre MT di CS per le quali non sono disponibili le coordinate lat/log');
    -- 14 err 41,'Sbarre MT di CS non agganciate in tutte le gerarchie ');
    -- 15 err 42,'Produttori MT o TR MT/BT senza comune ISTAT associato');
    -- 16 err 43 'Produttori MT Puri probabilmente marcati in AUI in modo erroneo');
	pID_DIZIONARIO_ALLARME_01 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 28;
	pID_DIZIONARIO_ALLARME_02 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 29;
	pID_DIZIONARIO_ALLARME_03 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 30;
	pID_DIZIONARIO_ALLARME_04 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 31;
	pID_DIZIONARIO_ALLARME_05 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 32;
	pID_DIZIONARIO_ALLARME_06 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 33;
	pID_DIZIONARIO_ALLARME_08 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 35;
	pID_DIZIONARIO_ALLARME_09 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 36;
	pID_DIZIONARIO_ALLARME_10 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 37;
	pID_DIZIONARIO_ALLARME_11 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 38;
	pID_DIZIONARIO_ALLARME_12 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 39;
	pID_DIZIONARIO_ALLARME_13 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 40;
	pID_DIZIONARIO_ALLARME_14 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 41;
	pID_DIZIONARIO_ALLARME_15 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 42;
	pID_DIZIONARIO_ALLARME_16 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 43;
	pID_DIZIONARIO_ALLARME_17 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 44;

    vPARAMETRI_DESCRIZIONE VARCHAR2(1000);

/*============================================================================*/
FUNCTION f_Date2DettStr(pData IN date) RETURN varchar2 IS

BEGIN
 RETURN TO_CHAR(pData,'dd/mm/yyyy')||' ore '||TO_CHAR(pData,'hh24:mi');
END;
/*============================================================================*/

PROCEDURE printa(pSTR IN VARCHAR2) IS

BEGIN
--	DBMS_OUTPUT.PUT_LINE(psTR);
PKG_Logs.TraceLog(psTR, PKG_UtlGlb.gcTrace_VRB);
END;

/*============================================================================*/

PROCEDURE sp_AddUnCollector(pErrore       IN INTEGER,
                            pGestElem     IN VARCHAR2,
                            pTipoElem     IN VARCHAR2 DEFAULT NULL,
                            pData         IN DATE DEFAULT SYSDATE) AS

BEGIN

   pkg_supervisione.AddSpvCollectorInfo(g_CollectorName,
                                        pData,
                                        g_IdSistema,
                                        pErrore,
                                        NULL,
                                        pGestElem,
                                        NULL,
                                        NULL,
                                        1,
                                        pTipoElem);

END sp_AddUnCollector;

/*============================================================================*/

PROCEDURE sp_CheckProdAUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vMsg        GTTD_VALORI_TEMP.ALF3%TYPE;
    vSql        VARCHAR2(4000) ;

    vNum INTEGER := 0;
    vContaGen  INTEGER;
    vPrd VARCHAR2(123);
    vGen VARCHAR2(50);

    vSommaxGruppo NUMBER;

    vPIgen NUMBER;

    vOKtipoProd NUMBER;

    vEseBase ELEMENTI.COD_GEST_ELEMENTO%TYPE;

    /* ====================================================================== */

    PROCEDURE sp_CheckAllGene IS

        vQuanti NUMBER;

    BEGIN

           FOR iGen IN (SELECT DISTINCT G.COD_ORG_NODO,G.SER_NODO,G.NUM_NODO,G.TIPO_ELEMENTO,G.ID_GENERATORE,
                 G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE GEST_GEN,
                 F.COD_TIPO_FONTE,
                 E.COD_TIPO_ELEMENTO,
                 G.tipo_imp,
                 P_APP_NOM,
                 F_P_NOM,
                 N_GEN_PAR
            FROM GENERATORI_TLC@PKG1_STMAUI.IT G
           /* LEFT OUTER*/ JOIN V_ELEMENTI E
            ON (E.COD_GEST_ELEMENTO = G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE)
            LEFT OUTER JOIN TIPO_FONTI F ON F.COD_TIPO_FONTE = G.TIPO_IMP
           WHERE G.TRATTAMENTO  = 0
             AND G.STATO        = 'E'
             --AND G.COD_ORG_NODO = vEseBase
         ) LOOP

            vPIgen := NVL(iGen.P_APP_NOM,0)*NVL(iGen.N_GEN_PAR,0)*NVL(iGen.F_P_NOM,0);

            IF NVL(iGen.F_P_NOM,0)<0 THEN -- mago-1423
               -- AUI 11 cosphi < 0
               sp_AddUnCollector(pID_DIZIONARIO_ALLARME_11, iGen.gest_gen, iGen.cod_tipo_elemento);
            END IF;

            IF NOT (iGen.TIPO_IMP IS NULL OR iGen.COD_TIPO_FONTE IS NULL) THEN

                IF iGen.P_APP_NOM IS NULL OR iGen.N_GEN_PAR IS NULL OR iGen.F_P_NOM IS NULL THEN
                    -- AUI 02 pI non definita (con tipo fonte definita)
                   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_02, iGen.gest_gen, iGen.tipo_elemento);
                END IF;

                IF vPIGen<=0 THEN
                    -- AUI 03 pI <= 0 (con tipo fonte definita)
                   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, iGen.gest_gen, iGen.cod_tipo_elemento);
                END IF;

            END IF;

        END LOOP;

    END;
    /* ====================================================================== */

    PROCEDURE sp_CheckProdPAS_AUI(pCodGest IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pCodtipo IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE) IS

        vQuanti NUMBER;

    BEGIN

    SELECT COUNT(*)
        INTO vQuanti
        FROM (
            SELECT precedente,successivo FROM (
            (
            SELECT M.valore attuale,
                LAG( M.valore ) OVER (PARTITION BY cod_trattamento_elem ORDER BY M.DATA) precedente,
                LEAD( M.valore ) OVER (PARTITION BY cod_trattamento_elem ORDER BY M.DATA) successivo,
                COD_ELEMENTO
            FROM (SELECT * FROM misure_acquisite WHERE TRUNC(DATA)
                 BETWEEN TRUNC(pData-2) AND TRUNC(pData-1)) M
            JOIN (SELECT * FROM trattamento_elementi
                    WHERE cod_elemento=pkg_elementi.GetCodElemento(pCodGest)) T
            USING (cod_trattamento_elem)
            WHERE T.cod_tipo_misura = gMis_PAS
          )
        ) WHERE attuale=0 )
        WHERE precedente <> 0
        AND successivo <> 0;


        IF vQuanti > 0 THEN
            -- AUI 16 Produttori MT Puri probabilmente marcati in AUI in modo erroneo
            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_16, pCodGest, pCodTipo);
        END IF;

    END sp_CheckProdPAS_AUI;

    /* ====================================================================== */

    PROCEDURE sp_CheckLineaGruppo(pCodGest IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pCodtipo IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE) IS

        vQuanti NUMBER;

    BEGIN

        SELECT COUNT(E.cod_gest_elemento)
        INTO vQuanti
        FROM
        (SELECT * FROM v_elementi WHERE cod_gest_elemento=pCodGest) E
        JOIN
        (SELECT * FROM gerarchia_imp_sn
                WHERE pData BETWEEN data_attivazione AND data_disattivazione) G
        ON (E.cod_elemento=G.cod_elemento)
        JOIN (SELECT * FROM v_elementi WHERE cod_tipo_elemento=g_tipoMago_Montante)  T
        ON (
               G.l01 = T.cod_elemento
            OR G.l02 = T.cod_elemento
            OR G.l03 = T.cod_elemento
            OR G.l04 = T.cod_elemento
            OR G.l05 = T.cod_elemento
            OR G.l06 = T.cod_elemento
            OR G.l07 = T.cod_elemento
            OR G.l08 = T.cod_elemento
            OR G.l09 = T.cod_elemento
            OR G.l10 = T.cod_elemento
            OR G.l11 = T.cod_elemento
            OR G.l12 = T.cod_elemento
            OR G.l13 = T.cod_elemento
            OR G.l14 = T.cod_elemento
            OR G.l15 = T.cod_elemento
            OR G.l16 = T.cod_elemento
            OR G.l17 = T.cod_elemento
            OR G.l18 = T.cod_elemento
            OR G.l19 = T.cod_elemento
            OR G.l20 = T.cod_elemento
            OR G.l21 = T.cod_elemento
            OR G.l22 = T.cod_elemento
            OR G.l23 = T.cod_elemento
            OR G.l24 = T.cod_elemento
            OR G.l25 = T.cod_elemento
        );

        IF vQuanti = 0 THEN
            -- AUI 05 Gruppo con potenza superiore a 6MW senza linea MT dedicata
            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_05, pCodGest, pCodTipo);
        END IF;

    END;

    /* ====================================================================== */

BEGIN

-- CLIENT

    SELECT CREL_PKG_Crel.FormatCodGestApp('RGD',CREL_PKG_Crel.getDefaultCO,'ESER')
    INTO vEseBase
    FROM dual;

    sp_CheckAllGene;


    FOR iClient IN (WITH
            AUI_TRASF AS (SELECT  T.COD_ORG_NODO,T.SER_NODO,T.NUM_NODO,T.TIPO_ELEMENTO,T.ID_TRASF, P.tipo_gen
                             , P.POT_PROD
                             , P.NUM_PROD FROM
                             TRASFORMATORI_TLC@PKG1_STMAUI.IT T
                             JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT P
                             ON (T.COD_ORG_NODO||T.SER_NODO||T.NUM_NODO||T.TIPO_ELEMENTO||T.ID_TRASF=
                                            P.COD_ORG_NODO||P.SER_NODO||P.NUM_NODO||P.tipo_ele||P.ID_TRASF)
                            WHERE (T.TRATTAMENTO = 0 AND T.STATO = 'E')
                            AND (P.TRATTAMENTO = 0 AND P.STATO = 'E')
                        )
           ,CLI_MAGO AS (SELECT * FROM (
           SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_CLIENTE,POTENZA_INSTALLATA
                              ,COD_TIPO_ELEMENTO
                              ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, g_tipoMago_Comune, pData, 2, 1, 1) COMUNE
                              ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO, g_tipoMago_CabinaP, pData, 1, 1, 1) CAB_PRI
                              ,PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO,g_tipoMago_Esercizio, pData, 1, 1, 1) ESE
                          FROM V_ELEMENTI   WHERE COD_TIPO_ELEMENTO IN ( g_tipoMago_Client,g_tipoMago_Trasf)
                      )  v
                          JOIN DEFAULT_CO d
                          ON (v.ese = d.cod_elemento_ese AND d.flag_primario=1)
                         --  AND COD_TIPO_CLIENTE IN ('A','B')
                       )
           ,CLI_AUI AS (SELECT A.COD_ORG_NODO,A.SER_NODO,A.NUM_NODO,A.TIPO_ELEMENTO,A.ID_CLIENTE,
                               A.COD_ORG_NODO||A.SER_NODO||A.NUM_NODO||A.TIPO_ELEMENTO||A.ID_CLIENTE GEST_PROD,
                               A.TIPO_ELEMENTO TIPO_AUI,
                               A.RAGIO_SOC   NOME_PROD,
                               A.TIPO_FORN,
                               A.POT_GRUPPI
                          FROM (SELECT C.COD_ORG_NODO,C.SER_NODO,C.NUM_NODO,C.ID_CLIENTE,
                                       C.RAGIO_SOC,
                                       NVL(T.TIPO_ELEMENTO,C.TIPO_ELEMENTO) TIPO_ELEMENTO,
                                       CASE
                                         WHEN NOT T.TIPO_ELEMENTO IS NULL THEN g_ProdAlia_AUI
                                         ELSE C.TIPO_FORN END  TIPO_FORN,
                                       CASE
                                           WHEN NOT T.TIPO_ELEMENTO IS NULL THEN T.POT_PROD
                                           ELSE C.POT_GRUPPI END  POT_GRUPPI
                                  FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  LEFT OUTER JOIN AUI_TRASF T
                                  ON (T.COD_ORG_NODO||T.SER_NODO||T.NUM_NODO||T.ID_TRASF=
                                            C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.ID_CLIENTE)
                                 WHERE --C.TIPO_FORN IN (g_AutoProd_AUI,g_ProdPuro_AUI,g_ProdAlia_AUI)
                                   --AND
                                   C.TRATTAMENTO = 0
                                   AND C.STATO = 'E'
                        --          AND C.COD_ORG_NODO = vEseBase
                               ) A
                         GROUP BY  A.COD_ORG_NODO,A.SER_NODO,A.NUM_NODO,A.TIPO_ELEMENTO,A.ID_CLIENTE,A.RAGIO_SOC,A.TIPO_FORN,A.POT_GRUPPI
                       )
           ,CLI_PI  AS (SELECT COD_GEST_ELEMENTO,T.COD_TIPO_FONTE PI_FONTE,M.VALORE PI_MISURA
                          FROM CLI_MAGO E
                          LEFT OUTER JOIN TRATTAMENTO_ELEMENTI T
                                       ON T.COD_ELEMENTO = E.COD_ELEMENTO
                                      AND T.COD_TIPO_MISURA = gMis_PI
                                      AND TIPO_AGGREGAZIONE = 1
                          LEFT OUTER JOIN MISURE_AGGREGATE_STATICHE M
                                       ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                      AND pData BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE
                       )
      SELECT DISTINCT
            COD_ORG_NODO,SER_NODO,NUM_NODO,TIPO_ELEMENTO,ID_CLIENTE,
             COD_GEST_ELEMENTO,
             COD_TIPO_ELEMENTO,
             NVL(COD_GEST_AUI,' ') COD_GEST_AUI,
             COD_GEST_MAGO,
             NVL(NOME_PROD,' ') NOME_PROD,
             NVL(CAB_PRI,' ') CAB_PRI,
             COMUNE,
             IS_PROD_AUI,
             TIPO_PROD_AUI,
             TIPO_FONTE_MAGO,
             COD_TIPO_CLIENTE,
             PI_AUI,
             PI_MAGO_ELE
        FROM (SELECT Z.*,Z.PI_MISURA PI_MAGO_MIS
                FROM (SELECT NVL(M.COD_GEST_ELEMENTO,A.GEST_PROD) COD_GEST_ELEMENTO,
                             A.COD_ORG_NODO,A.SER_NODO,A.NUM_NODO,A.TIPO_ELEMENTO,A.ID_CLIENTE,
                             M.COD_GEST_ELEMENTO COD_GEST_MAGO,M.COD_TIPO_ELEMENTO,
                             A.GEST_PROD COD_GEST_AUI,
                             M.NOME_ELEMENTO NOME_PROD,
                             PKG_ELEMENTI.GETGESTELEMENTO(CAB_PRI) CAB_PRI,
                             C.NOME_ELEMENTO COMUNE,
                             CASE WHEN A.TIPO_FORN IN (g_AutoProd_AUI,g_ProdPuro_AUI,g_ProdAlia_AUI)
                                THEN CASE WHEN M.COD_TIPO_ELEMENTO IS NULL THEN 0 ELSE 1 END
                                ELSE 0
                             END IS_PROD_AUI,
                             A.TIPO_FORN TIPO_PROD_AUI,
                             T.cod_raggr_fonte TIPO_FONTE_MAGO,
                             A.POT_GRUPPI PI_AUI,
                             P.PI_MISURA,
                             M.COD_TIPO_CLIENTE,
                             M.POTENZA_INSTALLATA PI_MAGO_ELE
                        FROM CLI_AUI A
                        FULL OUTER JOIN CLI_MAGO M ON M.COD_GEST_ELEMENTO = A.GEST_PROD
                        LEFT OUTER JOIN CLI_PI P ON P.COD_GEST_ELEMENTO = NVL(M.COD_GEST_ELEMENTO,A.GEST_PROD)
                        LEFT OUTER JOIN (SELECT f.cod_raggr_fonte,f.cod_tipo_fonte  FROM RAGGRUPPAMENTO_FONTI r
                             LEFT JOIN TIPO_FONTI f
                               ON (r.cod_raggr_fonte = f.cod_raggr_fonte
                                  AND f.COD_RAGGR_FONTE_ALFA_RIF IS NULL)
                             ) T
                        ON P.PI_FONTE = T.cod_tipo_fonte
                        LEFT OUTER JOIN V_ELEMENTI C ON C.COD_ELEMENTO      = COMUNE
            )Z ORDER BY CAB_PRI NULLS LAST, Z.COD_GEST_ELEMENTO)Z
     ) LOOP

        IF NOT iClient.COD_GEST_MAGO IS NULL THEN

            vOKtipoProd := CASE
                WHEN iClient.TIPO_PROD_AUI IN (g_AutoProd_AUI,g_ProdPuro_AUI,g_ProdAlia_AUI) THEN  1
                ELSE 0 END;


            IF iClient.TIPO_PROD_AUI = g_ProdPuro_AUI AND iClient.TIPO_FONTE_MAGO IN ('T','I') THEN
                -- se definito puro termico/idraulico controllo la PAS
                sp_CheckProdPAS_AUI(iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
            END IF;


			IF (vOKtipoProd=1) AND iClient.COMUNE IS NULL THEN
               -- AUI 15 comune non associato
			   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_15, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
			END IF;

	--		IF  ((iClient.TIPO_PROD_AUI IS NULL)AND(iCLient.COD_TIPO_ELEMENTO <> 'TRM')) THEN
			IF  ((iClient.COD_TIPO_CLIENTE IS NULL)AND(iCLient.COD_TIPO_ELEMENTO <> 'TRM')) THEN
                 -- AUI 08 tipo prod non definito
			   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
			END IF;

--            IF ((iClient.TIPO_PROD_AUI IS NULL)AND(iCLient.COD_TIPO_ELEMENTO <> 'TRM')) AND NVL(iClient.PI_AUI,0) > 0 THEN
            IF ((iClient.COD_TIPO_CLIENTE IS NULL)AND(iCLient.COD_TIPO_ELEMENTO <> 'TRM')) AND NVL(iClient.PI_AUI,0) > 0 THEN
                -- AUI (08) PI>0 senza definizione tipo produttore
                sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
            END IF;


            IF NOT iClient.TIPO_FONTE_MAGO IS NULL THEN

                -- correzione di luglio 2018: i trasf/clienti in left outer join
                -- se non matchano sulla cli_tlc non li considero
                IF ((iClient.COD_TIPO_ELEMENTO=g_tipoMago_Trasf)  AND (NOT iClient.COD_ORG_NODO IS NULL))
                     AND NVL(iClient.PI_MAGO_ELE,0)<=0 THEN
                    -- AUI 03 - Trasf. con fonte definita ma con PI<=0
                    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, iClient.cod_gest_elemento, iClient.cod_tipo_elemento);
                END IF;

            END IF;



           vNum := vNum + 1;
           vContaGen := 0;

           vSommaxGruppo := 0;

           FOR iGen IN (SELECT DISTINCT G.COD_ORG_NODO,G.SER_NODO,G.NUM_NODO,G.TIPO_ELEMENTO,G.ID_GENERATORE,
                 G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE GEST_GEN,
                 E.COD_TIPO_FONTE,
                 E.COD_TIPO_ELEMENTO,
                 G.tipo_imp,
                 P_APP_NOM,
                 F_P_NOM,
                 N_GEN_PAR
            FROM GENERATORI_TLC@PKG1_STMAUI.IT G
            LEFT OUTER JOIN V_ELEMENTI E
            ON (E.COD_GEST_ELEMENTO = G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO||G.TIPO_ELEMENTO||G.ID_GENERATORE)
      --      LEFT OUTER JOIN TIPO_FONTI F ON F.COD_TIPO_FONTE = G.TIPO_IMP
           WHERE G.COD_ORG_NODO||G.SER_NODO||G.NUM_NODO = iClient.COD_ORG_NODO||iClient.SER_NODO||iClient.NUM_NODO
             AND G.ID_CL        = iClient.id_cliente
             AND G.TRATTAMENTO  = 0
             AND G.STATO        = 'E'
         ) LOOP


            vContaGen := vContaGen + 1;

            vPIgen := NVL(iGen.P_APP_NOM,0)*NVL(iGen.N_GEN_PAR,0)*NVL(iGen.F_P_NOM,0);

            vSommaxGruppo := vSommaxGruppo + vPIgen;


        --    IF NVL(iGen.F_P_NOM,0)<0 THEN
        --       -- AUI 11 cosphi < 0
        --       sp_AddUnCollector(pID_DIZIONARIO_ALLARME_11, iGen.gest_gen, iGen.cod_tipo_elemento);
        --    END IF;

       --     IF iGen.TIPO_IMP IS NULL OR iGen.COD_TIPO_FONTE IS NULL THEN
       --        -- AUI 07 tipo fonte non definita
       --        sp_AddUnCollector(pID_DIZIONARIO_ALLARME_07, iGen.gest_gen, iGen.cod_tipo_elemento);
       --     ELSE

       --         IF iGen.P_APP_NOM IS NULL OR iGen.N_GEN_PAR IS NULL OR iGen.F_P_NOM IS NULL THEN
       --             -- AUI 02 pI non definita (con tipo fonte definita)
       --            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_02, iGen.gest_gen, iGen.tipo_elemento);
       --         END IF;

       --         IF vPIGen<=0 THEN
       --             -- AUI 03 pI <= 0 (con tipo fonte definita)
       --            sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, iGen.gest_gen, iGen.cod_tipo_elemento);
       --         END IF;

       --     END IF;


			 IF iGen.COD_TIPO_FONTE IS NULL THEN
			   -- AUI 06 tipo fonte non definita per generatore
			     sp_AddUnCollector(pID_DIZIONARIO_ALLARME_06, iGen.gest_gen, iGen.cod_tipo_elemento);
			 END IF;


        END LOOP;


        IF (vOKtipoProd=1) AND vContaGen = 0 AND (iCLient.COD_TIPO_ELEMENTO <> 'TRM') AND iClient.IS_PROD_AUI = 1 THEN
            -- AUI 01 cliente produttore senza generatori
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF (vOKtipoProd=1) AND vContaGen > 0  AND (iCLient.COD_TIPO_ELEMENTO <> 'TRM') AND iClient.IS_PROD_AUI = 0 THEN
            -- AUI 10 cliente non produttore ma con generatori
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_10, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF (vOKtipoProd=1) AND vSommaxGruppo <> iClient.PI_AUI THEN
            -- AUI 04 somma PI dei gen <> PI del prod
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_04, iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;

        IF (vOKtipoProd=1) AND vSommaxGruppo > g_sogliaLinea THEN
            sp_CheckLineaGruppo(iClient.COD_GEST_ELEMENTO, iClient.cod_tipo_elemento);
        END IF;


    END IF; -- mago is not null

    END LOOP;


END sp_CheckProdAUI;

/*============================================================================*/

PROCEDURE sp_CheckSbarreAUI (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga IN (

                SELECT cod_gest_elemento FROM
                (
                  SELECT
                    cod_gest_elemento ,
                    cod_tipo_elemento ,
                    PKG_ELEMENTI.GETELEMENTOPADRE(COD_ELEMENTO,g_tipoMago_Esercizio, pData, 1 , 1, 1) ESE
                 FROM v_elementi ) v
                JOIN DEFAULT_CO d
                ON
                (
                    v.ese                   = d.cod_elemento_ese
                    AND d.flag_primario     =1
                    AND v.cod_tipo_elemento = g_tipoMago_sbarraSec
                )
             JOIN AUI_NODI_TLC B
               ON     B.COD_ORG_NODO =
                         SUBSTR (v.COD_GEST_ELEMENTO, 1, 4)
                  AND B.SER_NODO = SUBSTR (v.COD_GEST_ELEMENTO, 5, 1)
                  AND B.NUM_NODO = SUBSTR (v.COD_GEST_ELEMENTO, 6, 6)
                  AND B.TRATTAMENTO = 0
                  AND B.STATO = 'E'
                  AND (NVL(B.gps_x,0)=0 OR NVL(B.gps_y,0)=0)
          ) LOOP


            -- AUI 13 Sbarra CS senza coordinate
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_13, iRiga.COD_GEST_ELEMENTO, g_tipoMago_sbarraSec);


    END LOOP;

END sp_CheckSbarreAUI;

/*============================================================================*/

PROCEDURE sp_CheckSbarreMAGO (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga IN (
            SELECT cod_elemento
            FROM
            (SELECT cod_elemento FROM v_elementi WHERE cod_tipo_elemento=g_tipoMago_sbarraSec ) E
            NATURAL JOIN
            (SELECT cod_elemento, 'AMM' ger FROM gerarchia_amm WHERE pData BETWEEN data_attivazione AND data_disattivazione
             UNION
             SELECT cod_elemento, 'IMP' ger FROM gerarchia_imp_sa WHERE pData BETWEEN data_attivazione AND data_disattivazione
             UNION
             SELECT cod_elemento, 'GEO' ger FROM gerarchia_geo WHERE pData BETWEEN data_attivazione AND data_disattivazione) G
            GROUP BY cod_elemento
            HAVING COUNT(G.ger)<3
    ) LOOP


            -- AUI 14 Sbarra CS non presente su tutte le gerarchie
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_14,
                              PKG_ELEMENTI.GetGestElemento(iRiga.COD_ELEMENTO),
                              g_tipoMago_sbarraSec);


    END LOOP;

END sp_CheckSbarreMAGO;

/*============================================================================*/

PROCEDURE sp_CheckMontanti_AUI  (pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS
BEGIN

    FOR iRiga IN (
            WITH
            iClienti AS
                (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE COD_GEST_ELEMENTO
                    FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                    LEFT OUTER JOIN TRASFORMATORI_TLC@PKG1_STMAUI.IT T
                    ON (T.COD_ORG_NODO||T.SER_NODO||T.NUM_NODO||T.TIPO_ELEMENTO||T.ID_TRASF=
                    C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)
                    WHERE C.TIPO_FORN IN (g_AutoProd_AUI,g_ProdPuro_AUI,g_ProdAlia_AUI)
                    AND C.TRATTAMENTO = 0 AND C.STATO = 'E'),
            iMontanti AS
                (SELECT COD_GEST_ELEMENTO
                    FROM v_ELEMENTI
                    WHERE COD_TIPO_ELEMENTO = g_tipoMago_Montante)
            SELECT COD_GEST_ELEMENTO
            FROM iClienti
            NATURAL JOIN iMontanti
    ) LOOP

            -- AUI 12 Montante MT definito come Cliente
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_12, iRiga.COD_GEST_ELEMENTO,g_tipoMago_Montante);

    END LOOP;

END sp_CheckMontanti_AUI;

/*============================================================================*/

PROCEDURE sp_CheckProdPod_AUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vEseBase ELEMENTI.COD_GEST_ELEMENTO%TYPE;

BEGIN

    SELECT CREL_PKG_Crel.FormatCodGestApp('RGD',CREL_PKG_Crel.getDefaultCO,'ESER')
    INTO vEseBase
    FROM dual;

    FOR iRiga IN (SELECT C.POD
                    FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                    LEFT OUTER JOIN TRASFORMATORI_TLC@PKG1_STMAUI.IT T
                    ON (T.COD_ORG_NODO||T.SER_NODO||T.NUM_NODO||T.TIPO_ELEMENTO||T.ID_TRASF=
                        C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)
                    WHERE C.TIPO_FORN IN (g_AutoProd_AUI,g_ProdPuro_AUI,g_ProdAlia_AUI)
                    AND C.TRATTAMENTO = 0
                    AND C.STATO = 'E'
            --        AND C.COD_ORG_NODO = vEseBase
                    AND NOT C.POD IS NULL
                    GROUP BY C.POD
                    HAVING COUNT(C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE)>1
     ) LOOP

            -- AUI 09 POD associato a piu' codici gestionali
			sp_AddUnCollector(pID_DIZIONARIO_ALLARME_09, iRiga.POD);

     END LOOP;

END sp_CheckProdPod_AUI;

/*============================================================================*/

PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

 vOra TIMESTAMP;
BEGIN

vOra := CURRENT_TIMESTAMP;
printa(RPAD('in  sp_CheckProdAUI ',40,' '));
 sp_CheckProdAUI  (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckProdAUI ',40,' ')||TO_CHAR(CURRENT_TIMESTAMP-vOra));

vOra := CURRENT_TIMESTAMP;
printa(RPAD('in  sp_CheckSbarreAUI ',40,' '));
 sp_CheckSbarreAUI (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckSbarreAUI ',40,' ')||TO_CHAR(CURRENT_TIMESTAMP-vOra));

vOra := CURRENT_TIMESTAMP;
printa(RPAD('in  sp_CheckSbarreMAGO ',40,' '));
 sp_CheckSbarreMAGO (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckSbarreMAGO ',40,' ')||TO_CHAR(CURRENT_TIMESTAMP-vOra));

vOra := CURRENT_TIMESTAMP;
printa(RPAD('in  sp_CheckMontanti_AUI ',40,' '));
 sp_CheckMontanti_AUI  (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckMontanti_AUI ',40,' ')||TO_CHAR(CURRENT_TIMESTAMP-vOra));

vOra := CURRENT_TIMESTAMP;
printa(RPAD('in  sp_CheckProdPod_AUI ',40,' '));
 sp_CheckProdPod_AUI (pStato, pBlockOnly, pData );
printa(RPAD('out sp_CheckProdPod_AUI ',40,' ')||TO_CHAR(CURRENT_TIMESTAMP-vOra));

END sp_CheckAll;

/*============================================================================*/

PROCEDURE auiSpvCollector
	IS
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvCollector',
								   pDataRif    => SYSDATE);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvCollector - Start';
		printa(vStrLog);

		sp_CheckAll;

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvCollector - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
		EXCEPTION
		WHEN OTHERS THEN
		   printa ('PKG_SPV_ANAG_AUI.auiSpvCollector error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));

END auiSpvCollector;

/*============================================================================*/

PROCEDURE auiSpvProcessor
	IS
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento varchar2(100);
		p_cod_tipo_elemento varchar2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%TYPE;
		pID_DIZIONARIO_ALLARME number;
		pDATA_ALLARME date := SYSDATE;
		pDESCRIZIONE_TIPO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := NULL;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := NULL;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := NULL;


		n_allarmi_1 	number;
		n_allarmi_2 	number;
		n_allarmi_3 	number;
		n_allarmi_3_GMT number;
		n_allarmi_3_TRM number;
		n_allarmi_4 	number;
		n_allarmi_5 	number;
		n_allarmi_6_CMT number;
		n_allarmi_6_TRM number;
		n_allarmi_8 	number;
		n_allarmi_9 	number;
		n_allarmi_10 	number;
		n_allarmi_11 	number;
		n_allarmi_12 	number;
		n_allarmi_13 	number;
		n_allarmi_14 	number;
		n_allarmi_15 	number;
		n_allarmi_16 	number;
		n_allarmi_17 	number;

		soglia_allarme_1 		number;
		soglia_allarme_2 		number;
		soglia_allarme_3_gmt 	number;
		soglia_allarme_3_trm 	number;
		soglia_allarme_4 		number;
		soglia_allarme_5 		number;
		soglia_allarme_6_cmt 	number;
		soglia_allarme_6_trm	number;
		soglia_allarme_8 		number;
		soglia_allarme_9 		number;
		soglia_allarme_10 		number;
		soglia_allarme_11 		number;
		soglia_allarme_12 		number;
		soglia_allarme_13 		number;
		soglia_allarme_14 		number;
		soglia_allarme_15 		number;
		soglia_allarme_16 		number;
		soglia_allarme_17 		number;

		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvProcessor',
								   pDataRif    => SYSDATE);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Start';
		printa(vStrLog);

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_gen');
		soglia_allarme_1 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_nd');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_lt_0');
		soglia_allarme_3_gmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_pi_lt_0');
		soglia_allarme_3_trm := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_pot_gruppi');
		soglia_allarme_4 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_fonte');
		soglia_allarme_6_cmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_no_fonte');
		soglia_allarme_6_trm := TO_NUMBER(P_VALORE, '9999');

--		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_no_fonte');
--		soglia_allarme_7 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_tr_tipo_prod');
		soglia_allarme_8 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_pod0');
		soglia_allarme_9 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_cmt_non_prod');
		soglia_allarme_10 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_cmt_tr_cos_phi_lt_0');
		soglia_allarme_11 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_sbarre_no_coordinate');
		soglia_allarme_13 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_sbarre_non_agganciate');
		soglia_allarme_14 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_tr_no_istat');
		soglia_allarme_15 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gruppi_no_linea');
		soglia_allarme_5 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_montanti');
		soglia_allarme_12 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_mal_identificati');
		soglia_allarme_16 := TO_NUMBER(P_VALORE, '9999');


		--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'smileGme.n_hour');

		--allarme 3:
		--			GENERATORI MT vs TR MT/bt
		--			GMT / TRM
		--allarme 6:
		--			PRODUTTORI MT vs TR MT/bt
		--			CMT / TRM


		WITH allarmi AS (
		  SELECT d.id_dizionario_allarme
		  , NVL(SUM(i.valore), 0) n_allarmi
      , i.informazioni
		  FROM spv_rel_sistema_dizionario d
		  LEFT JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
		  WHERE d.id_sistema = g_IdSistema
		  AND TRUNC(i.data_inserimento) = TRUNC(SYSDATE)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  ORDER BY d.id_dizionario_allarme, i.informazioni
		)
		SELECT
		 SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01  , A.n_allarmi, 0)) n_allarmi_1
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_02  , A.n_allarmi, 0)) n_allarmi_2
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_03  , a.n_allarmi, null)) n_allarmi_3
		,SUM(
			CASE WHEN (A.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 AND A.informazioni = 'GMT') THEN A.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_GMT
		,SUM(
			CASE WHEN (A.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 AND A.informazioni = g_tipoMago_Trasf) THEN A.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_TRM
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_04  , A.n_allarmi, 0)) n_allarmi_4
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_05  , A.n_allarmi, 0)) n_allarmi_5
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_06  , a.n_allarmi, null)) n_allarmi_6
		,SUM(
			CASE WHEN (A.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 AND A.informazioni = g_tipoMago_Client) THEN A.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_CMT
		,SUM(
			CASE WHEN (A.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 AND A.informazioni = g_tipoMago_Trasf) THEN A.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_TRM
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_08  , A.n_allarmi, 0)) n_allarmi_8
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_09  , A.n_allarmi, 0)) n_allarmi_9
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_10 , A.n_allarmi, 0)) n_allarmi_10
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_11 , A.n_allarmi, 0)) n_allarmi_11
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_12 , A.n_allarmi, 0)) n_allarmi_12
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_13 , A.n_allarmi, 0)) n_allarmi_13
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_14 , A.n_allarmi, 0)) n_allarmi_14
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_15 , A.n_allarmi, 0)) n_allarmi_15
		,SUM(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_16 , A.n_allarmi, 0)) n_allarmi_16
	INTO n_allarmi_1,n_allarmi_2,n_allarmi_3_GMT,n_allarmi_3_TRM,n_allarmi_4,n_allarmi_5,n_allarmi_6_CMT,n_allarmi_6_TRM,
        n_allarmi_8,n_allarmi_9,n_allarmi_10
		,n_allarmi_11,n_allarmi_12,n_allarmi_13,n_allarmi_14,n_allarmi_15,n_allarmi_16
		FROM allarmi A
		;



		OPEN pRefCurs FOR
			SELECT d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, E.cod_tipo_elemento, i.informazioni
			FROM spv_rel_sistema_dizionario d
			JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
			LEFT JOIN elementi E ON E.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
			WHERE d.id_sistema = g_IdSistema
			AND TRUNC(i.data_inserimento) = TRUNC(SYSDATE)
			ORDER BY d.id_dizionario_allarme
			;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);




			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 2;



			-- Gli allarmi 5-12-8 hanno una soglia di tipo GIALLO sotto la quale non vengono segnalati
			IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_05  AND n_allarmi_5  >= soglia_allarme_5 )
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_12 AND n_allarmi_12 >= soglia_allarme_12)
				OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_08 AND n_allarmi_8 >= soglia_allarme_8)
					)THEN
				vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'pPARAMETRI_DESCRIZIONE: ' || pPARAMETRI_DESCRIZIONE ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);

			ELSE
				-- tutti gli altri allarmi vengono gestiti cosi:
				-- da 0 a soglia --> allarme Giallo
				-- da soglia in poi --> allarme Rosso
				IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_01  AND n_allarmi_1  >= soglia_allarme_1 )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_02  AND n_allarmi_2  >= soglia_allarme_2 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3  >= soglia_allarme_3 )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  AND n_allarmi_3_GMT  >= soglia_allarme_3_gmt )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  AND n_allarmi_3_TRM  >= soglia_allarme_3_trm )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_04  AND n_allarmi_4  >= soglia_allarme_4 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6  >= soglia_allarme_6 )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  AND n_allarmi_6_CMT  >= soglia_allarme_6_cmt )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  AND n_allarmi_6_TRM  >= soglia_allarme_6_trm )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_08  AND n_allarmi_8  >= soglia_allarme_8 )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_09  AND n_allarmi_9  >= soglia_allarme_9 )
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_10 AND n_allarmi_10 >= soglia_allarme_10)
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_11 AND n_allarmi_11 >= soglia_allarme_11)
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_13 AND n_allarmi_13 >= soglia_allarme_13)
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_14 AND n_allarmi_14 >= soglia_allarme_14)
					OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_15 AND n_allarmi_15 >= soglia_allarme_15)
				)THEN
					pID_LIVELLO := 3;
				END IF;

				vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
			END IF;
		END LOOP;
		CLOSE PREFCURS;

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvProcessor - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_ANAG_AUI.auiSpvProcessor error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END auiSpvProcessor;

/*============================================================================*/

FUNCTION f_SetDettaglioLob(p_chiave  IN VARCHAR2,
                         p_query     IN VARCHAR2,
                         p_valforced IN NUMBER DEFAULT NULL) RETURN number IS

l_cursor integer;
l_cursor_status integer;
l_col_count number;
l_desc_tbl sys.dbms_sql.desc_tab2;
l_col_val varchar2(32767);

l_report blob;
l_raw raw(32767);

l_Quanti NUMBER;

/*----------------------------------------------------------------*/

BEGIN

    -- open BLOB to store CSV file
    dbms_lob.createtemporary( l_report, FALSE );
    dbms_lob.open( l_report, dbms_lob.lob_readwrite );

    IF NOT p_query IS NULL THEN

        -- parse query
        l_cursor := dbms_sql.open_cursor;
        dbms_sql.parse(l_cursor, p_query, dbms_sql.native);
        dbms_sql.describe_columns2(l_cursor, l_col_count, l_desc_tbl );

        -- define report columns
        FOR i IN 1 .. l_col_count LOOP
            dbms_sql.define_column(l_cursor, i, l_col_val, 32767 );
        END LOOP;

        -- write column headings to CSV file
        FOR i IN 1 .. l_col_count LOOP
            l_col_val := l_desc_tbl(i).col_name;
            IF UPPER(l_col_val) = UPPER('g_tit_CodGes') THEN
                l_col_val := g_tit_CodGes;
            ELSIF  UPPER(l_col_val) = UPPER('g_tit_TipCli') THEN
                l_col_val := g_tit_TipCli;
            ELSIF  UPPER(l_col_val) = UPPER('g_tit_TipEle') THEN
                l_col_val := g_tit_TipEle;
            ELSIF  UPPER(l_col_val) = UPPER('g_tit_NoData') THEN
                l_col_val := g_tit_Nodata;
            END IF;

            IF i = l_col_count THEN
    --            l_col_val := '"'||l_col_val||'"'||chr(10);
                l_col_val := l_col_val||CHR(10);
            ELSE
    --            l_col_val := '"'||l_col_val||'",';
                l_col_val := l_col_val||g_sep_CSV_excel;
            END IF;
            l_raw := utl_raw.cast_to_raw( l_col_val );
            dbms_lob.writeappend( l_report, utl_raw.LENGTH( l_raw ), l_raw );
        END LOOP;

        l_cursor_status := sys.dbms_sql.execute(l_cursor);

        l_Quanti := 0;
        -- write result set to CSV file
        LOOP
        exit WHEN dbms_sql.fetch_rows(l_cursor) <= 0;

            l_Quanti := l_Quanti + 1;

            FOR i IN 1 .. l_col_count LOOP
                dbms_sql.column_value(l_cursor, i, l_col_val);
                IF i = l_col_count THEN
             --       l_col_val := '"'||l_col_val||'"'||chr(10);
                    l_col_val := l_col_val||CHR(10);
                ELSE
             --       l_col_val := '"'||l_col_val||'",';
                    l_col_val := l_col_val||g_sep_CSV_excel;
                END IF;
                l_raw := utl_raw.cast_to_raw( l_col_val );
                dbms_lob.writeappend( l_report, utl_raw.LENGTH( l_raw ), l_raw );
            END LOOP;
        END LOOP;

        dbms_sql.close_cursor(l_cursor);

    END IF;

    dbms_lob.close( l_report );

    IF p_valforced IS NULL THEN
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   l_Quanti,
                                                   l_report);
    ELSE
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   p_valforced,
                                                   l_report);
    END IF;

    RETURN l_Quanti;
EXCEPTION
WHEN NO_DATA_FOUND THEN
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               0,
                                               NULL);
    RETURN 0;

END f_SetDettaglioLob;

/*============================================================================*/

FUNCTION f_SetDettaglioNum(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN NUMBER DEFAULT NULL) RETURN NUMBER IS

l_Quanti NUMBER;
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := REPLACE(TO_CHAR(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

printa('====================');
printa(p_chiave);
printa(v_Quanti);
--printa('--------------------');
--printa(p_query);
printa('====================');

    RETURN l_quanti;

END f_SetDettaglioNum;

/*============================================================================*/
PROCEDURE auiSpvDetail
	IS


		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);

        v_sql_dummy VARCHAR(2000) := 'SELECT 0 g_tit_NoData FROM DUAL';
        v_sql_nihil VARCHAR(2000) := 'SELECT ''  '' g_tit_NoData FROM DUAL';


        -- RICORDARSI: nelle select per la PI dividere il valore per 1000
        -- che i dati sono in kW ma sul video li vogliono in MW

    -- CONSISTENZA RETE _ AUI

       -- clienti MT
       v_sql_aui_consCliMT_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN '''||g_tipoMago_Trasf||''' ELSE '''||g_tipoMago_Client||''' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
            --    ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''','''||g_ProdPuro_AUI||''','''||g_ProdAlia_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN ) ';
        v_sql_aui_consCliMT_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_aui_consCliMT_from);
        v_sql_aui_consCliMT_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_consCliMT_from);

        -- clienti BT non reperibili
        v_sql_aui_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_aui_cliBT_PI VARCHAR2(2000) := v_sql_dummy;

        -- Trasformatori
        v_sql_aui_consTrMTBT_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) ';
        v_sql_aui_consTrMTBT_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_consTrMTBT_from);
        v_sql_aui_consTrMTBT_PI VARCHAR2(2000)  :=PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_consTrMTBT_from);

        -- Totale
        v_sql_aui_ConsTot_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN '''||g_tipoMago_Trasf||''' ELSE '''||g_tipoMago_Client||''' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
            --    ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''','''||g_ProdPuro_AUI||''','''||g_ProdAlia_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN  '
                ||'  UNION '
                ||'  select gest_prod COD_GEST_ELEMENTO, '
                ||'  '''||g_AutoProd_AUI||''' COD_TIPO_CLIENTE, '''||g_tipoMago_Trasf||''' COD_TIPO_ELEMENTO, '
                ||'      sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) ' ;
        v_sql_aui_ConsTot_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_aui_ConsTot_from);
        v_sql_aui_ConsTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_ConsTot_from);

    -- CONSISTENZA RETE _ MAGO

       -- clienti MT
       v_sql_mago_cliMT_from VARCHAR2(2000) := '   FROM V_ELEMENTI  '||
                ' WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||'''  '||
                ' AND COD_TIPO_CLIENTE IN (''A'',''B'',''C'') '||
                ' ';
        v_sql_mago_cliMT_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT  '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE g_tit_Tipele '
                ||v_sql_mago_cliMT_from);
        v_sql_mago_cliMT_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_cliMT_from);

        -- clienti BT non reperibili
        v_sql_mago_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_mago_cliBT_PI VARCHAR2(2000) := v_sql_dummy;

        -- Trasformatori
        v_sql_mago_TR_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||'''';
        v_sql_mago_TR_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_mago_TR_from);
        v_sql_mago_TR_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_mago_TR_from);

        -- Totali
        v_sql_mago_ConsTot_from  VARCHAR2(2000) :=
                ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''') '||
                ' OR (COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'',''C'')) ';
        v_sql_mago_ConsTot_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_ConsTot_from);
        v_sql_mago_ConsTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI  '
                || v_sql_mago_ConsTot_from);


    -- GENERAZIONE DISTRIBUITA _ AUI

       -- prod PURI
        v_sql_aui_prodPuri_from VARCHAR2(2000) :=' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN ('''||g_ProdPuro_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodPuri_N VARCHAR2(2000) :=PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodPuri_from);
        v_sql_aui_prodPuri_PI VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_aui_prodPuri_from);

       -- prod AUTOPROD
       v_sql_aui_prodAuto_from VARCHAR2(2000) := ' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodAuto_N VARCHAR2(2000)  :=PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodAuto_from);
        v_sql_aui_prodAuto_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_aui_prodAuto_from);

       -- trasf con Produzione
        v_sql_aui_TRprod_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'         AND POT_NOM_1>0 '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) '
                ||' WHERE POTENZA_INSTALLATA>0 ';
        v_sql_aui_TRprod_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_TRprod_from);
        v_sql_aui_TRprod_PI VARCHAR2(2000)  := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_aui_TRprod_from);

       -- totali
        v_sql_aui_GeneTot_from  VARCHAR2(2000) := ' FROM (   SELECT gest_prod COD_GEST_ELEMENTO, '
                ||' SUM(POT_GRUPPI)POTENZA_INSTALLATA, '
                ||''''||g_tipoMago_Client||''' COD_TIPO_ELEMENTO '
                ||'FROM ( SELECT '
                ||'C.COD_ORG_NODO '
                ||'||C.SER_NODO '
                ||'||C.NUM_NODO '
                ||'||C.TIPO_ELEMENTO '
                ||'||C.ID_CLIENTE GEST_PROD, '
                ||'C.POT_GRUPPI '
                ||'FROM '
                ||'CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'WHERE '
                ||'C.TRATTAMENTO    =0 '
                ||'AND C.STATO      =''E'' '
                ||' AND C.TIPO_FORN IN(''AP'',''PP'') '
                ||') '
                ||'A '
                ||'GROUP BY '
                ||'A.GEST_PROD, '
                ||'A.POT_GRUPPI '
                ||'UNION '
                ||'SELECT '
                ||'gest_prod COD_GEST_ELEMENTO, '
                ||'SUM(POT_NOM_1)POTENZA_INSTALLATA, '
                ||''''||g_tipoMago_Trasf||''' COD_TIPO_ELEMENTO '
                ||'FROM '
                ||'( '
                ||'SELECT '
                ||'C.COD_ORG_NODO '
                ||'||C.SER_NODO '
                ||'||C.NUM_NODO '
                ||'||C.TIPO_ELEMENTO '
                ||'||C.ID_TRASF GEST_PROD, '
                ||'C.POT_NOM_1 '
                ||'FROM '
                ||'TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'WHERE '
                ||' C.TRATTAMENTO=0 '
                ||'AND C.STATO  =''E'' '
                ||' AND POT_NOM_1>0 '
                ||' ) '
                ||'A '
                ||' GROUP BY '
                ||'A.GEST_PROD, '
                ||'A.POT_NOM_1 '
                ||') ';
        v_sql_aui_GeneTot_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes '
                ||v_sql_aui_GeneTot_from);
        v_sql_aui_GeneTot_PI  VARCHAR2(2000) := (' SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI   '
                ||v_sql_aui_GeneTot_from);


    -- GENERAZIONE DISTRIBUITA _ MAGO

       -- prod PURI
        v_sql_mago_prodPuri_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                ' AND COD_TIPO_CLIENTE = ''A''';
        v_sql_mago_prodPuri_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodPuri_from);
        v_sql_mago_prodPuri_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_mago_prodPuri_from);

       -- prod AUTOPROD
        v_sql_mago_prodAuto_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                ' AND COD_TIPO_CLIENTE = ''B''';
        v_sql_mago_prodAuto_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodAuto_from);
        v_sql_mago_prodAuto_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_mago_prodAuto_from);

       -- trasf con Produzione
        v_sql_mago_TRprod_from  VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''') '||
                '    AND POTENZA_INSTALLATA>0 '||
                ' ORDER BY COD_GEST_ELEMENTO';
        v_sql_mago_TRprod_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_GEST_ELEMENTO g_tit_CodGes '
                ||v_sql_mago_TRprod_from);
        v_sql_mago_TRprod_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT  nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI '
                ||v_sql_mago_TRprod_from);

        -- totali
        v_sql_mago_GeneTot_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''' AND POTENZA_INSTALLATA>0) '||
                ' OR (COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'')) ';
        v_sql_mago_GeneTot_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_GeneTot_from);
        v_sql_mago_GeneTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT nvl(SUM(POTENZA_INSTALLATA),0)/1000 g_tit_PI  '
                || v_sql_mago_GeneTot_from);



        v_dummyN            NUMBER;

        v_totN_auiCons      NUMBER := 0;
        v_totN_magoCons     NUMBER := 0;
        v_totPI_auiCons     NUMBER := 0;
        v_totPI_magoCons    NUMBER := 0;
        v_totN_auiGene      NUMBER := 0;
        v_totN_magoGene     NUMBER := 0;
        v_totPI_auiGene     NUMBER := 0;
        v_totPI_magoGene    NUMBER := 0;

        v_dataAUI VARCHAR2(100);
        v_dataMago VARCHAR2(100);

/*----------------------------------------------------------------------------*/
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvDetail',
                               pDataRif    => SYSDATE);

    vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvDetail - Start';
    printa(vStrLog);


    -- date ultimi aggiornamenti
    SELECT f_Date2DettStr(MAX(data_import))
    INTO v_dataAUI
    FROM STORICO_IMPORT
    WHERE TIPO = 'SN' AND ORIGINE = 'AUI';

   SELECT f_Date2DettStr(MAX(data_import))
    INTO v_dataMago
    FROM STORICO_IMPORT
    WHERE TIPO = 'SN' AND ORIGINE = 'CORELE';


    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.aui.ultimoAggiornamento',
                                               v_dataAUI,
                                               NULL);
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.mago.ultimoAggiornamento',
                                               v_dataMago,
                                               NULL);

    -- consistenza rete AUI
        v_totN_auiCons       := 0;
        v_totPI_auiCons      := 0;


    printa('consistenzaRete.aui.trMTBT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.trMTBT_N',
                                        v_sql_aui_consTrMTBT_N);
    printa('consistenzaRete.aui.trMTBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.trMTBT_PI',
                                        v_sql_aui_consTrMTBT_PI);

    printa('consistenzaRete.aui.clientiMT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiMT_N',
                                        v_sql_aui_consCliMT_N);
    printa('consistenzaRete.aui.clientiMT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiMT_PI',
                                        v_sql_aui_consCliMT_PI);
    -- clienti BT non reperibili
    printa('consistenzaRete.aui.clientiBT_N');
    --v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiBT_N', v_sql_aui_cliBT_N);
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiBT_N',
                                        NULL,
                                        0); -- MAGO-1834
    printa('consistenzaRete.aui.clientiBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiBT_PI',
                                        v_sql_aui_cliBT_PI,
                                        0);

    -- totali cons AUI
    printa('consistenzaRete.aui.totale_N');
    v_dummyN := f_SetDettaglioLob('consistenzaRete.aui.totale_N',
                                        v_sql_aui_ConsTot_N,
                                        v_totN_auiCons);

    v_dummyN := f_SetDettaglioNum('consistenzaRete.aui.totale_PI',
                                        v_sql_aui_ConsTot_PI,
                                        v_totPI_auiCons);


    -- consistenza rete Mago
        v_totN_magoCons      := 0;
        v_totPI_magoCons     := 0;

    printa('consistenzaRete.mago.trMTBT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.trMTBT_N',
                                        v_sql_mago_TR_N);
    printa('consistenzaRete.mago.trMTBT_PI');
    printa(v_sql_mago_TR_PI);
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.trMTBT_PI',
                                        v_sql_mago_TR_PI);

    printa('consistenzaRete.mago.clientiMT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiMT_N',
                                        v_sql_mago_cliMT_N);
    printa('consistenzaRete.mago.clientiMT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiMT_PI',
                                        v_sql_mago_cliMT_PI);

    -- clienti BT non reperibili
    printa('consistenzaRete.mago.clientiBT_N');
    --v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiBT_N', v_sql_mago_cliBT_N);
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiBT_N',
                                          NULL,
                                        0);   -- MAGO-1834   
    printa('consistenzaRete.mago.clientiBT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiBT_PI',
                                        NULL,
                                        0);

    -- totali cons Mago
    printa('consistenzaRete.mago.totale_N');
    v_dummyN := f_SetDettaglioLob('consistenzaRete.mago.totale_N',
                                        v_sql_mago_ConsTot_N,
                                        v_totN_magoCons);

    v_dummyN := f_SetDettaglioNum('consistenzaRete.mago.totale_PI',
                                        v_sql_mago_ConsTot_PI);



    --  generazione distribuitaAUI
        v_totN_auiGene       := 0;
        v_totPI_auiGene      := 0;

    printa('genDistribuita.aui.trMTBT_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.trMTBT_N',
                                        v_sql_aui_TRprod_N);
    printa(v_totN_auiGene);

    printa('genDistribuita.aui.trMTBT_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.trMTBT_PI',
                                        v_sql_aui_TRprod_PI);
--    printa(v_totPI_auiGene);

    printa('genDistribuita.aui.prodMTPuri_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTPuri_N',
                                        v_sql_aui_prodPuri_N);
--    printa(v_totN_auiGene);

    printa('genDistribuita.aui.prodMTPuri_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTPuri_PI',
                                        v_sql_aui_prodPuri_PI);
--    printa(v_totPI_auiGene);

    printa('genDistribuita.aui.prodMTAutoproduttori_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTAutoproduttori_N',
                                        v_sql_aui_prodAuto_N);
--    printa(v_totN_auiGene);

    printa('genDistribuita.aui.prodMTAutoproduttori_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTAutoproduttori_PI',
                                        v_sql_aui_prodAuto_PI);
--    printa(v_totPI_auiGene);

    -- totali gene dist AUI
--    printa('genDistribuita.aui.totale_N');
--    printa('-----------------------------');
--    printa(v_sql_aui_GeneTot_N);
 --   printa('-----------------------------');
--    printa(v_sql_aui_GeneTot_PI);
--    printa('-----------------------------');
    v_dummyN := f_SetDettaglioLob('genDistribuita.aui.totale_N',
                                        v_sql_aui_GeneTot_N,
                                        v_totN_auiGene);

    v_dummyN := f_SetDettaglioNum('genDistribuita.aui.totale_PI',
                                        v_sql_aui_GeneTot_PI,
                                        v_totPI_auiGene);
--    printa(v_totPI_auiGene);



    -- generazione distribuita Mago
        v_totN_magoGene      := 0;
        v_totPI_magoGene     := 0;

    printa('genDistribuita.mago.trMTBT_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.trMTBT_N',
                                        v_sql_mago_TRprod_N);
    printa('genDistribuita.mago.trMTBT_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.trMTBT_PI',
                                        v_sql_mago_TRprod_PI);

    printa('genDistribuita.mago.prodMTPuri_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTPuri_N',
                                        v_sql_mago_prodPuri_N);
    printa('genDistribuita.mago.prodMTPuri_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTPuri_PI',
                                        v_sql_mago_prodPuri_PI);

    printa('genDistribuita.mago.prodMTAutoproduttori_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTAutoproduttori_N',
                                        v_sql_mago_prodAuto_N);
    printa('genDistribuita.mago.prodMTAutoproduttori_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTAutoproduttori_PI',
                                        v_sql_mago_prodAuto_PI);



    -- totali gene dist Mago
    printa('genDistribuita.mago.totale_N');
    v_dummyN := f_SetDettaglioLob('genDistribuita.mago.totale_N',
                                        v_sql_mago_GeneTot_N);

    v_dummyN := f_SetDettaglioNum('genDistribuita.mago.totale_PI',
                                        v_sql_mago_GeneTot_PI,
                                        v_totPI_magoGene);





    vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvDetail - End';
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    printa(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

EXCEPTION
WHEN OTHERS THEN
    printa('PKG_SPV_ANAG_AUI.auiSpvDetail error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END auiSpvDetail;
/*============================================================================*/

PROCEDURE auiSpvMain(	 pDoDeleteOldSpvInfo IN number DEFAULT 0
							,pDoDeleteSpvAlarm IN number DEFAULT 1
							,pDoDeleteTodaySpvInfo IN number DEFAULT 1
							,pDoCollector IN number DEFAULT 1
							,pDoProcessor IN number DEFAULT 1
							,pDoDetail IN number DEFAULT 1
							)
	IS
    vIsSupervisionEnabled varchar2(100);
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(1000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_ANAG_AUI.auiSpvMain',
								   pDataRif    => SYSDATE);

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Start';
		printa(vStrLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.supervision_enabled');
    	vStrLog :=  'PKG_SPV_ANAG_AUI --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
			'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
			'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;
     printa(vStrLog);

    IF vIsSupervisionEnabled = 'true' OR vIsSupervisionEnabled = 'TRUE' THEN
        -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
      IF pDoDeleteTodaySpvInfo = 1 THEN
          vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - delete Today spv_collector_info ';
           printa(vStrLog);
        DELETE FROM spv_collector_info
        WHERE collector_name = g_CollectorName
        AND TRUNC(data_inserimento) = TRUNC(SYSDATE)
        ;
      END IF;

      -- cancello TUTTE le info generate dal collector ANAGRAFICA_AUI prima di gc-48h
      IF pDoDeleteOldSpvInfo = 1 THEN
          vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - delete Old spv_collector_info ';
          printa(vStrLog);
        DELETE FROM spv_collector_info
        WHERE collector_name = g_CollectorName
        AND data_inserimento < TRUNC(SYSDATE)
        AND id_rel_sistema_dizionario IN (
          SELECT id_rel_sistema_dizionario
          FROM spv_rel_sistema_dizionario
          WHERE id_sistema = g_IdSistema
        );
      END IF;

      -- eseguo il collector e creo le spv_collector_info
      IF pDoCollector = 1 THEN
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvCollector ';
        printa(vStrLog);
        auiSpvCollector();
      END IF;

      -- cancello TUTTI gli allarmi generati dal collector ANAGRAFICA_AUI
      IF pDoDeleteSpvAlarm = 1 THEN
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain -  delete from spv_allarmi ';
         printa(vStrLog);
        DELETE FROM spv_allarmi WHERE id_rel_sistema_dizionario IN (SELECT id_rel_sistema_dizionario FROM spv_rel_sistema_dizionario WHERE id_sistema = g_IdSistema);
      END IF;

      -- eseguo il processor e creo le spv_allarmi
      IF pDoProcessor = 1 THEN
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvProcessor ';
        printa(vStrLog);

        auiSpvProcessor();
      END IF;

      -- valorizzo il pannello di dettaglio
      IF pDoDetail = 1 THEN
        vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Calling auiSpvDetail ';
        printa(vStrLog);

        auiSpvDetail();
      END IF;
    ELSE
      vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - Supervision AUI is NOT enabled';
      printa(vStrLog);
    END IF;

    PKG_SPV_UTL.sp_SystemBroom;
	

		vStrLog := 'PKG_SPV_ANAG_AUI.auiSpvMain - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_ANAG_AUI.auiSpvMain error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END auiSpvMain;

/*============================================================================*/

END PKG_SPV_ANAG_AUI;
/

show errors;