SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.3 rev 5
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_SPV_UTL.sql
PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_SPV_UTL.sql
@./PackageBodies/PKG_SPV_RETE_ELE.sql
@./PackageBodies/PKG_SPV_LOAD_MIS.sql
@./PackageBodies/PKG_SPV_SMILE_GME.sql
@./PackageBodies/PKG_SUPERVISIONE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql

disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.3 rev 5
PROMPT =======================================================================================

SPOOL OFF
