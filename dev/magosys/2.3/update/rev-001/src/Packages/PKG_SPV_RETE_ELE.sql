PROMPT PACKAGE        "PKG_SPV_RETE_ELE" AS

create or replace PACKAGE        PKG_SPV_RETE_ELE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1x
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
   NAME:       PKG_SPV_RETE_ELE
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
    NOTES:

*********************************************************************************************************** */

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */


    PROCEDURE sp_CheckCliDismessi_stm(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckCliDismessi_AUI(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckCG_STM_NoAUI(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckProdAUI_NoSTM(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckCGEle(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckCO(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckAll(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);


PROCEDURE EleSpvDetail(pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE EleSpvCollector;
    PROCEDURE EleSpvProcessor;
    PROCEDURE EleSpvMain(pDoDeleteOldSpvInfo IN NUMBER DEFAULT 0
                            ,pDoDeleteSpvAlarm IN NUMBER DEFAULT 1
                            ,pDoDeleteTodaySpvInfo IN NUMBER DEFAULT 1
                            ,pDoCollector IN NUMBER DEFAULT 1
                            ,pDoProcessor IN NUMBER DEFAULT 1
                            ,pDoDetail IN NUMBER DEFAULT 0
                            );


END PKG_SPV_RETE_ELE;
/