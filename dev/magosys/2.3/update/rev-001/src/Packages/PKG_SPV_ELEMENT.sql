PROMPT PACKAGE: PKG_SPV_ELEMENT

CREATE OR REPLACE PACKAGE PKG_SPV_ELEMENT AS 


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


TYPE t_ForecastElement IS RECORD(IS_SOLARE_OMOGENEO NUMBER);

TYPE t_ForecastTable IS TABLE OF t_ForecastElement INDEX BY VARCHAR2(100);



--TYPE t_SPVElement IS RECORD(COD_GEST_ELEMENTO  ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                         --     IS_SOLARE_OMOGENEO NUMBER);

--TYPE t_SPVTable IS TABLE OF t_SPVElement;





 PROCEDURE sp_GetElement   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2 DEFAULT 'M|B',
                                pFonte          IN VARCHAR2 DEFAULT 'S|E|I|T|R|C|1',
                                pTipoProd       IN VARCHAR2 DEFAULT 'A|B|X',
                                pTipoElement    IN VARCHAR2 DEFAULT 'TRM|CMT|GMT',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pdisconnect     IN NUMBER DEFAULT 1);


FUNCTION f_SolareOmogeneo(pCOD_ELEMENTO IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN NUMBER;

FUNCTION f_GetElement   (pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2 DEFAULT 'M|B',
                                pFonte          IN VARCHAR2 DEFAULT 'S|E|I|T|R|C|1',
                                pTipoProd       IN VARCHAR2 DEFAULT 'A|B|X',
                                pTipoElement    IN VARCHAR2 DEFAULT 'TRM|CMT|GMT',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pdisconnect     IN NUMBER DEFAULT 1)  
RETURN t_SPVTable PIPELINED ;

END PKG_SPV_ELEMENT;
/