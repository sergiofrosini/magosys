PROMPT PACKAGE: PKG_STATS

CREATE OR REPLACE PACKAGE PKG_STATS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


/* ***********************************************************************************************************
 Tipi, Costanti e Globali Pubbliche
*********************************************************************************************************** */

    TYPE t_RowSession IS RECORD (COD_SESSIONE       SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                 SERVICE_ID         SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 SERVICE_REF        SESSION_STATISTICS.SERVICE_REF%TYPE,
                                 COD_TIPO_MISURA    SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                 STATUS             SESSION_STATISTICS.STATUS%TYPE,
                                 DATE_FROM          SESSION_STATISTICS.DATE_FROM%TYPE,
                                 DATE_TO            SESSION_STATISTICS.DATE_TO%TYPE,
                                 LEAF_START_DATE    SESSION_STATISTICS.LEAF_START_DATE%TYPE,
                                 LEAF_END_DATE      SESSION_STATISTICS.LEAF_END_DATE%TYPE,
                                 AGGREG_START_DATE  SESSION_STATISTICS.AGGREG_START_DATE%TYPE,
                                 AGGREG_END_DATE    SESSION_STATISTICS.AGGREG_END_DATE%TYPE,
                                 AGGREG_COMPLETED   SESSION_STATISTICS.AGGREG_COMPLETED%TYPE);


    gSessionID_MdsServ   SESSION_STATISTICS.SERVICE_ID%TYPE := ''; -- Legge da parametro in MEASURE_OFFLINE_PARAMETERS
    gSessionID_fromST    SESSION_STATISTICS.SERVICE_ID%TYPE := 'MAGO-Load-Data-fromST';
    gSessionID_aggreg    SESSION_STATISTICS.SERVICE_ID%TYPE := 'MAGO-Load-Data-aggreg';
    gSessionID_RplyInit  SESSION_STATISTICS.SERVICE_ID%TYPE := 'RPLY-Init';
    gSessionID_RplyLoad  SESSION_STATISTICS.SERVICE_ID%TYPE := 'RPLY-Load';

    gSessionStatusAggr   SESSION_STATISTICS.STATUS %TYPE    := 'AGGREGATION_PROGRESS';
    gSessionStatusWork   SESSION_STATISTICS.STATUS %TYPE    := 'WORKING';
    gSessionStatusDone   SESSION_STATISTICS.STATUS %TYPE    := 'DONE';


/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 PROCEDURE CheckSessions;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetStatisticSessionID  (pSessionID     OUT NUMBER);

 FUNCTION  GetStatisticSessionID  RETURN NUMBER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitStatisticSession (pCodSession    SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                 pServiceID     SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pServiceRef    SESSION_STATISTICS.SERVICE_REF%TYPE,
                                 pTipMisura     SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                 pStatus        SESSION_STATISTICS.STATUS%TYPE,
                                 pDateFrom      SESSION_STATISTICS.DATE_FROM%TYPE,
                                 pDateTo        SESSION_STATISTICS.DATE_TO%TYPE,
                                 pLeafStartDate SESSION_STATISTICS.LEAF_START_DATE%TYPE DEFAULT NULL,
                                 pLeafEndDate   SESSION_STATISTICS.LEAF_END_DATE%TYPE DEFAULT NULL,
                                 pAggrStartDate SESSION_STATISTICS.AGGREG_START_DATE%TYPE DEFAULT NULL,
                                 pAggrEndDate   SESSION_STATISTICS.AGGREG_END_DATE%TYPE DEFAULT NULL,
                                 pAggrCompleted SESSION_STATISTICS.AGGREG_COMPLETED%TYPE DEFAULT NULL
                                );

 PROCEDURE InitStatisticSession (pSessionRow    SESSION_STATISTICS%ROWTYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                 pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pDataRif       IN DATE,
                                 pTipMisList    IN VARCHAR2 DEFAULT NULL);

 PROCEDURE GetServiceRunStatus  (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                                 pServiceId     IN SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pTipMisList    IN VARCHAR2 DEFAULT NULL);


-- ----------------------------------------------------------------------------------------------------------


END PKG_Stats;
/