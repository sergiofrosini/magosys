PROMPT PACKAGE        "PKG_SPV_LOAD_MIS" AS

create or replace PACKAGE      PKG_SPV_LOAD_MIS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1x
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
   NAME:       PKG_SPV_LOAD_MIS
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
    NOTES:

*********************************************************************************************************** */

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

    FUNCTION f_CheckCO_PQ(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) RETURN NUMBER;

     PROCEDURE sp_CheckCMT_PQ(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE sp_CheckACQ(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE MisSpvCollector;
    PROCEDURE MisSpvProcessor;

    PROCEDURE MisSpvDetail(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);

    PROCEDURE MisSpvMain(	 pDoDeleteOldSpvInfo IN NUMBER DEFAULT 0
							,pDoDeleteSpvAlarm IN NUMBER DEFAULT 1
							,pDoDeleteTodaySpvInfo IN NUMBER DEFAULT 1
							,pDoCollector IN NUMBER DEFAULT 1
							,pDoProcessor IN NUMBER DEFAULT 1
							,pDoDetail IN NUMBER DEFAULT 1
							);



END PKG_SPV_LOAD_MIS;
/