PROMPT PACKAGE        PKG_SPV_UTL AS

create or replace PACKAGE PKG_SPV_UTL AS 

    g_collectorOkAcq      VARCHAR2(50) := 'SPV_CCPQ_OK_ACQ';
    g_collectorKoAcq      VARCHAR2(50) := 'SPV_CCPQ_KO_ACQ';

    g_DumDum_s          VARCHAR2(100); -- variabile generica stringa
    g_DumDum_n          NUMBER; -- variabile generica numerica

    g_ApplXParam        VARCHAR2(50) := 'MAGO';

    g_ParamMunic       VARCHAR2(50) := 'IS_MUNICIPALIZZATE';


    gMis_PI     CONSTANT VARCHAR2(10)  := 'PI';
    gMis_PAS    CONSTANT VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT CONSTANT VARCHAR2(10)  := 'PMC';
    gMis_PRI    CONSTANT VARCHAR2(10)  := 'PRI';


    gMis_PCTP    CONSTANT VARCHAR2(10)  := 'PCT-P';
    gMis_PCTQ    CONSTANT VARCHAR2(10)  := 'PCT-Q';
    gMis_PCTP_PCTQ  CONSTANT VARCHAR2(15) := 'PCT-P|PCT-Q';

    FUNCTION f_StatoNormale RETURN VARCHAR2;
    FUNCTION f_StatoAttuale RETURN VARCHAR2;

    FUNCTION f_StatoNormaleSuffisso RETURN VARCHAR2;
    FUNCTION f_StatoAttualeSuffisso RETURN VARCHAR2;

    FUNCTION f_TipoTrasfAUI  RETURN VARCHAR2;
    FUNCTION f_TipoProdAUI  RETURN VARCHAR2;
    FUNCTION f_TipoTrasfST  RETURN VARCHAR2;
    FUNCTION f_TipoProdST  RETURN VARCHAR2;

    FUNCTION f_IsMunic RETURN NUMBER;

    FUNCTION f_DismessoAUI  RETURN VARCHAR2;


    FUNCTION f_Date2DettStr(pData IN DATE) RETURN VARCHAR2;


   FUNCTION f_date_to_unix (p_date  DATE,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Rome') RETURN NUMBER ;
   FUNCTION f_timestamp_to_unix (p_time  TIMESTAMP,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Rome') RETURN NUMBER;


    FUNCTION f_ClientAUISi(pAlias IN VARCHAR2 DEFAULT '666') RETURN VARCHAR2;

FUNCTION f_AppiccicaStato(pSql IN VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##') RETURN VARCHAR2;


PROCEDURE sp_AppiccicaStato(pSql IN OUT VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##');


END PKG_SPV_UTL;
/