PROMPT
PROMPT DBLink PKG1_CREL.IT
PROMPT
 

BEGIN
    EXECUTE IMMEDIATE 'DROP DATABASE LINK PKG1_CREL.IT';
EXCEPTION
    WHEN others THEN IF SQLCODE = -02024 THEN  --ORA-02024: database link non trovato
                        NULL;
                     ELSE
                        RAISE;
                     END IF;
END;
/

BEGIN
	execute immediate 'CREATE DATABASE LINK PKG1_CREL.IT CONNECT TO CREL IDENTIFIED BY CREL USING ''ARCDB1''';
END;
/