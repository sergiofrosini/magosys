PROMPT TYPE: T_SPVSOGLIA

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVSOGLIA_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_SPVSOGLIA_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

create or replace TYPE T_SPVSOGLIA_OBJ AS OBJECT
      (  ID_SOGLIA	NUMBER
		,ID_SISTEMA	NUMBER
		,ID_CHIAVE	VARCHAR2(100 BYTE)
		,VALORE	VARCHAR2(300 BYTE)
      );
/

create or replace TYPE T_SPVSOGLIA_ARRAY IS TABLE OF T_SPVSOGLIA_OBJ;
/