PROMPT Package Body PKG_SPV_LOAD_MIS

CREATE OR REPLACE PACKAGE BODY MAGO.PKG_SPV_LOAD_MIS AS

/* ***********************************************************************************************************
   NAME:       PKG_SPV_LOAD_MIS
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
    NOTES:

*********************************************************************************************************** */
    g_CollectorName         CONSTANT spv_collector_info.collector_name%TYPE 	:='LOAD_MEASURE';

	g_IdSistema       CONSTANT spv_dettaglio_parametri.ID_SISTEMA%TYPE 	:= 4;


    g_isMunic   NUMBER := -1;
    g_DefaultMunic   NUMBER := 0;

    g_SogliaLinea CONSTANT NUMBER := 6000; -- 6MW

    gMis_PI     CONSTANT VARCHAR2(10)  := 'PI';
    gMis_PAS    CONSTANT VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT CONSTANT VARCHAR2(10)  := 'PMC';
    gMis_PRI    CONSTANT VARCHAR2(10)  := 'PRI';

    gMis_CCQ    CONSTANT VARCHAR2(10)  := 'CC-Q';
    gMis_CCP    CONSTANT VARCHAR2(10)  := 'CC-P';
    gMis_CCP_CCQ  CONSTANT VARCHAR2(10)  := 'CC-P|CC-Q';



    g_sep_CSV_excel CONSTANT VARCHAR2(1) := ';';

    --mago.supervision.detail.anagraficaAui.elenco.title



    -- 01 err 50	Assenza di CCP e/o CCQ   a livello di intero CO per il mese precedente
    -- 02 err 51	Clienti MT senza CCP o CCQ
    -- 03 err 52	Clienti MT  scartati in fase di acquisizione della misura di CCP o CCQ
    -- 04 err 53	Intero Esercizio principale del CO senza la PAT per il giorno corrente
    pID_DIZIONARIO_ALLARME_01 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 50;
	pID_DIZIONARIO_ALLARME_02 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 51;
	pID_DIZIONARIO_ALLARME_03 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 52;
	pID_DIZIONARIO_ALLARME_04 spv_dizionario_allarmi.id_dizionario_allarme%TYPE := 53;

    vPARAMETRI_DESCRIZIONE VARCHAR2(1000);

/*============================================================================*/

  FUNCTION f_date_to_unix (p_date  DATE,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Kiev') RETURN NUMBER  AS

  BEGIN
    RETURN  round((cast((FROM_TZ(CAST(p_date AS TIMESTAMP), p_in_src_tz) AT TIME ZONE 'GMT') AS DATE)-TO_DATE('01.01.1970','dd.mm.yyyy'))*(24*60*60));
  END f_date_to_unix;

/*============================================================================*/

  FUNCTION f_timestamp_to_unix (p_time  TIMESTAMP,p_in_src_tz IN VARCHAR2 DEFAULT 'Europe/Kiev') RETURN NUMBER AS

  BEGIN
        RETURN  round((cast((FROM_TZ(p_time, p_in_src_tz) AT TIME ZONE 'GMT') AS DATE)-TO_DATE('01.01.1970','dd.mm.yyyy'))*(24*60*60));
  END f_timestamp_to_unix;

/*============================================================================*/

PROCEDURE printa(pSTR IN VARCHAR2) IS

BEGIN
  --	    DBMS_OUTPUT.PUT_LINE(psTR);
  PKG_Logs.TraceLog(psTR, PKG_UtlGlb.gcTrace_VRB);
END;

/*============================================================================*/
FUNCTION f_ClientAUISi(pAlias IN VARCHAR2 DEFAULT '666') RETURN VARCHAR2 IS


    vForse VARCHAR2(100) :=  CASE nvl(pAlias,'666') WHEN '666' THEN '' ELSE pAlias||'.' END;

BEGIN

    RETURN vForse||'TRATTAMENTO = 0 AND '||vForse||'STATO IN (''A'', ''E'') ';

END;

/*============================================================================*/

FUNCTION f_AppiccicaStato(pSql IN VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##') RETURN VARCHAR2 IS

BEGIN

    RETURN
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoAttualeSuffisso)
        WHEN PKG_Mago.gcStatoNormale THEN REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoNormaleSuffisso)
    END;

END;

/*============================================================================*/

PROCEDURE sp_AppiccicaStato(pSql IN OUT VARCHAR2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 DEFAULT '_##') IS

BEGIN
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN pSql := REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoAttualeSuffisso);
        WHEN PKG_Mago.gcStatoNormale THEN pSql := REPLACE(pSql,pDove,PKG_SPV_UTL.f_StatoNormaleSuffisso);
    END CASE;

END;

/*============================================================================*/

PROCEDURE sp_AddUnCollector(pErrore       IN INTEGER,
                            pGestElem     IN VARCHAR2,
                            pTipoElem     IN VARCHAR2 DEFAULT NULL,
                            pData         IN DATE DEFAULT SYSDATE) AS

BEGIN

   pkg_supervisione.AddSpvCollectorInfo(g_CollectorName,
                                        pData,
                                        g_IdSistema,
                                        pErrore,
                                        NULL,
                                        pGestElem,
                                        NULL,
                                        NULL,
                                        1,
                                        pTipoElem);

END sp_AddUnCollector;

/*============================================================================*/

FUNCTION f_CheckCO_PQ(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) RETURN NUMBER AS

 v_data_da DATE;
 v_data_a   DATE;
 v_data_misura DATE;
 v_giorni NUMBER;
 v_elemento_ESE NUMBER;
 v_gest_elem_ese ELEMENTI.COD_GEST_ELEMENTO%TYPE;

 pOrganizzazione INTEGER := 1;                             -- 1 = organizzazione elettrica 2 = organizzazione geografica (Istat) 3 = organizzazione amministrativa
 pStatoRete      INTEGER := 1;                             -- 1 = stato normale 2 = stato attuale
 pTipiMisura     VARCHAR2 (300) := gMis_CCP_CCQ;       -- PAS = Potenza Attiva Scambiata PI = Potenza Installata PMP = Potenza Meteo Previsto PMC = Potenza Meteo Previsto Cielo Terso
 pTipologiaRete  VARCHAR2 (300) := 'A|B|M';                -- M = Rete MT B = Rete BT A = Rete AT X = Tipo Rete non disponibile
 pFonte          VARCHAR2 (300) := '0!';  -- 0 solo totale 1 dettaglio e totale
                                                              -- S = Solare E = Elolica I = Idraulica T = Termica R = Altra Rinnovavbile C = Altra Convenzionale
                                                              -- 1 = Non Omogenea 2 = Non Applicabile 3 = Non Disponibile
 pTipoProd     VARCHAR2 (300) := 'A|B|X|C';            -- A = Produttore puro B = Autoproduttore X = Non determinato Z = Non applicabile C = Cliente
 vAggr           INTEGER := 60;  -- Aggregazione
 vCnt             INTEGER;

 vLst              PKG_UtlGlb.t_query_cur;
 vCodMis        TIPI_MISURA.COD_TIPO_MISURA%TYPE;
 vData           DATE;
 vValore         NUMBER;
 vTipoFonte   TIPO_FONTI.COD_TIPO_FONTE%TYPE;
 vnumCCP      INTEGER := 0;
 vnumCCQ      INTEGER := 0;
 vinter            NUMBER;
 vTEST           BOOLEAN := TRUE;

BEGIN

  v_data_a := last_day(add_months(trunc(sysdate),-1))+1-1/(60*60*24) ;
  v_data_da   := to_date('01/'||substr(to_char(add_months(trunc(sysdate),-1),'dd/mm/yyyy'),4)||' 11:00:00','dd/mm/yyyy hh24:mi:ss') ;

  SELECT (v_data_a - v_data_da) +1 INTO v_giorni FROM dual;

  v_data_misura := v_data_da;

  SELECT cod_Elemento_ese, COD_GEST_ELEMENTO
  INTO v_elemento_ESE, v_gest_elem_ese
  FROM default_co INNER JOIN  ELEMENTI ON default_co.cod_Elemento_ese = ELEMENTI.COD_ELEMENTO
  WHERE FLAG_PRIMARIO = 1;



      SELECT count(*)--distinct ddt.cod_gest_elemento,ddt.misura
      INTO vCnt
        FROM
            (SELECT T.cod_gest_elemento,d.misura FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaInf(v_elemento_ESE,
                                            1,
                                            1,
                                            pData,
                                            'ESE',
                                            1)) T,
               (SELECT gMis_CCP misura FROM DUAL
                 UNION
                SELECT gMis_CCQ misura FROM DUAL) d) ddt
            JOIN
            (SELECT * FROM SPV_COLLECTOR_MEASURE
             WHERE collector_name = PKG_SPV_UTL.g_collectorKoAcq
               AND extract(MONTH FROM data_misura) = extract(MONTH FROM add_months(pData,-1)) ) c
            ON ( c.cod_gest_elemento = ddt.cod_gest_elemento
                AND ddt.misura = c.cod_tipo_misura);

        IF vCnt = 0 THEN
              sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, v_gest_elem_ese , 'ESE');
        END IF;


        RETURN 1 - vCnt; -- 0 = allarme non inserito <-> esiste il collector measure


-- GeTMisure
-- IF getmisure non restituisce niente then
 ----  sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, NULL, NULL);


    EXCEPTION WHEN OTHERS THEN RAISE;
   --- dbms_output.put_line( to_char(v_data_misura,'dd/mm/yyyy hh24:mi:ss')) ;

END f_CheckCO_PQ;

/*============================================================================*/

/*============================================================================*/

PROCEDURE sp_CheckCMT_PQ(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vOra TIMESTAMP;
    v_data_da DATE;
    v_data_a   DATE;
    v_data_misura DATE;
    v_giorni NUMBER;

    vLstCMT      PKG_UtlGlb.t_query_cur;
    vLst       PKG_UtlGlb.t_query_cur;
    pOrganizzazione INTEGER := 1;                             -- 1 = organizzazione elettrica 2 = organizzazione geografica (Istat) 3 = organizzazione amministrativa
    pStatoRete      INTEGER := 1;                             -- 1 = stato normale 2 = stato attuale
    pTipiMisura     VARCHAR2 (300) := gMis_CCP_CCQ;
    pTipologiaRete  VARCHAR2 (300) := 'B|M';                -- M = Rete MT B = Rete BT A = Rete AT X = Tipo Rete non disponibile
    pFonte          VARCHAR2 (300) := '0!';  -- 0 solo totale 1 dettaglio e totale
                                                              -- S = Solare E = Elolica I = Idraulica T = Termica R = Altra Rinnovavbile C = Altra Convenzionale
                                                              -- 1 = Non Omogenea 2 = Non Applicabile 3 = Non Disponibile
    pFonteCMT          VARCHAR2 (300) :=  'S|E|I|T|R|C|1';
    pTipoProd     VARCHAR2 (300) := 'A|B|X|Z|C';            -- A = Produttore puro B = Autoproduttore X = Non determinato Z = Non applicabile C = Cliente
    vAggr           INTEGER := 60;  -- Aggregazione
    vCnt             INTEGER;

    vCodMis        TIPI_MISURA.COD_TIPO_MISURA%TYPE;
    vData           DATE;
    vValore         NUMBER;
    vTipoFonte   TIPO_FONTI.COD_TIPO_FONTE%TYPE;
    vinter            NUMBER;

    vnumCCP      INTEGER := 0;
    vnumCCQ      INTEGER := 0;

    vTEST           BOOLEAN := TRUE;


    vElePadre        ELEMENTI.COD_ELEMENTO%TYPE;
    vGstPadre        ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodEle          ELEMENTI.COD_ELEMENTO%TYPE;
    vGstElem         ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vTipElem         TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vFonte           TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
    vPotenza         ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
    vCitta           METEO_REL_ISTAT.COD_CITTA%TYPE;
    vTipProd         TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
    vTipRet          TIPI_RETE.COD_TIPO_RETE%TYPE;
    vLat             NUMBER;
    vLon             NUMBER;
    vP01             FORECAST_PARAMETRI.PARAMETRO1%TYPE;
    vP02             FORECAST_PARAMETRI.PARAMETRO2%TYPE;
    vP03             FORECAST_PARAMETRI.PARAMETRO3%TYPE;
    vP04             FORECAST_PARAMETRI.PARAMETRO4%TYPE;
    vP05             FORECAST_PARAMETRI.PARAMETRO5%TYPE;
    vP06             FORECAST_PARAMETRI.PARAMETRO6%TYPE;
    vP07             FORECAST_PARAMETRI.PARAMETRO7%TYPE;
    vP08             FORECAST_PARAMETRI.PARAMETRO8%TYPE;
    vP09             FORECAST_PARAMETRI.PARAMETRO9%TYPE;
    vP10             FORECAST_PARAMETRI.PARAMETRO10%TYPE;
    vDT              FORECAST_PARAMETRI.DATA_ULTIMO_AGG%TYPE;
    pTipoGeo         VARCHAR2(10) := 'C';
    pFlagPI          NUMBER := 1 ;
    pTipoElement     VARCHAR2(300)  := 'CMT';

    vQuanti      NUMBER;

    vElem          ELEMENTI.COD_ELEMENTO%TYPE;
    vGest          ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;


    BEGIN


      PKG_ELEMENTI.GetDefaultCO(vElem, vGest, vNome);



      FOR rigaMis IN
      (SELECT DISTINCT ddt.cod_gest_elemento,ddt.misura
        FROM
            (SELECT T.cod_gest_elemento,d.misura FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaInf(vElem,
                                            1,
                                            1,
                                            pData,
                                            'CMT',
                                            1)) T,
               (SELECT gMis_CCP misura FROM DUAL
                 UNION
                SELECT gMis_CCQ misura FROM DUAL) d) ddt
            LEFT JOIN
            (SELECT * FROM SPV_COLLECTOR_MEASURE
             WHERE collector_name = PKG_SPV_UTL.g_collectorKoAcq
               AND extract(MONTH FROM data_misura) = extract(MONTH FROM add_months(pData,-1)) ) c
            ON ( c.cod_gest_elemento = vGstelem
                AND ddt.misura = c.cod_tipo_misura)
            WHERE c.cod_gest_elemento IS NULL)
        LOOP

              sp_AddUnCollector(pID_DIZIONARIO_ALLARME_02, rigaMis.cod_gest_elemento,rigaMis.misura);

        END LOOP;

END sp_CheckCMT_PQ;

/*============================================================================*/

/*============================================================================*/

PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                      pBlockOnly   IN BOOLEAN DEFAULT FALSE,
                      pData        IN DATE DEFAULT SYSDATE) AS

    vOra TIMESTAMP;

    vAllarmeCO NUMBER:=0;

BEGIN

    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCO ',40,' '));
    vAllarmeCO := f_CheckCO_PQ (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckCO ',40,' ')||to_char(current_timestamp-vOra));


    IF vAllarmeCO = 0 THEN

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckCMT',40,' '));
        sp_CheckCMT_PQ (pStato, pBlockOnly, pData );
        printa(RPAD('out sp_CheckCMT ',40,' ')||to_char(current_timestamp-vOra));

        vOra := current_timestamp;
        printa(RPAD('in  sp_CheckACQ',40,' '));
        sp_CheckACQ (pStato, pBlockOnly, pData );
        printa(RPAD('out sp_CheckACQ ',40,' ')||to_char(current_timestamp-vOra));

    END IF;

END sp_CheckAll;

/*============================================================================*/

PROCEDURE sp_CheckACQ(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

BEGIN


    FOR rigaMis IN (SELECT DISTINCT COD_GEST_ELEMENTO, cod_tipo_misura
                     FROM SPV_COLLECTOR_MEASURE
                    WHERE collector_name = PKG_SPV_UTL.g_collectorKoAcq
                    AND extract(MONTH FROM data_misura) = extract(MONTH FROM add_months(pData,-1)))
    LOOP

      sp_AddUnCollector(pID_DIZIONARIO_ALLARME_03, rigaMis.COD_GEST_ELEMENTO,rigaMis.cod_tipo_misura);

    END LOOP;

END sp_CheckACQ;

/*============================================================================*/

FUNCTION f_SetDettaglioData(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN DATE DEFAULT NULL) RETURN DATE IS

l_Quanti DATE;
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               l_Quanti,
                                               NULL);

    RETURN v_quanti;

END f_SetDettaglioData;

/*============================================================================*/

FUNCTION f_SetDettaglioNum(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN NUMBER DEFAULT NULL) RETURN NUMBER IS

l_Quanti  VARCHAR2(200);
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := REPLACE(to_char(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

    RETURN l_quanti;

END f_SetDettaglioNum;

/*============================================================================*/

FUNCTION f_SetDettaglioStr(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN VARCHAR2 DEFAULT NULL) RETURN VARCHAR2 IS

l_Quanti VARCHAR2(200);
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

BEGIN

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := REPLACE(to_char(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

    RETURN l_quanti;

END f_SetDettaglioStr;

/*============================================================================*/

PROCEDURE MisSpvCollector
	IS
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	BEGIN
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_LOAD_MIS.MisSpvCollector',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvCollector - Start';
		printa(vStrLog);

		sp_CheckAll;

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvCollector - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
		EXCEPTION
		WHEN OTHERS THEN
		   printa ('PKG_SPV_LOAD_MIS.MisSpvCollector error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));

END MisSpvCollector;

/*============================================================================*/

PROCEDURE MisSpvProcessor
	IS
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento VARCHAR2(100);
		p_cod_tipo_elemento VARCHAR2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%TYPE;
		pID_DIZIONARIO_ALLARME NUMBER;
		pDATA_ALLARME DATE := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := NULL;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := NULL;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := NULL;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := NULL;


		n_allarmi_1 	NUMBER;
		n_allarmi_2 	NUMBER;
		n_allarmi_3 	NUMBER;
		n_allarmi_4 	NUMBER;

		soglia_allarme_1 		NUMBER;
		soglia_allarme_2 		NUMBER;
		soglia_allarme_3 		NUMBER;
		soglia_allarme_4 		NUMBER;

		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);

	BEGIN

		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_LOAD_MIS.MisSpvProcessor',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvProcessor - Start';
		printa(vStrLog);

		soglia_allarme_1 := 1;

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'loadMeasure.num_prod_senza_misure');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '99999999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'loadMeasure.num_prod_scartati');
		soglia_allarme_3 := TO_NUMBER(P_VALORE, '99999999');

		soglia_allarme_4 := 1;


		WITH allarmi AS (
		  SELECT d.id_dizionario_allarme
		  , nvl(sum(i.valore), 0) n_allarmi
          , i.informazioni
		  FROM spv_rel_sistema_dizionario d
		  LEFT JOIN spv_collector_info i
          ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario
            AND i.collector_name = g_CollectorName
		  WHERE d.id_sistema = g_IdSistema
		  AND trunc(i.data_inserimento) = trunc(sysdate)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  ORDER BY d.id_dizionario_allarme, i.informazioni
		)
		SELECT
		 sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01  , A.n_allarmi, 0)) n_allarmi_1
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_02  , A.n_allarmi, 0)) n_allarmi_2
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_03  , A.n_allarmi, 0)) n_allarmi_3
		,sum(DECODE(A.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_04  , A.n_allarmi, 0)) n_allarmi_4
		INTO n_allarmi_1,n_allarmi_2,n_allarmi_3,n_allarmi_4
		FROM allarmi A
		;

		OPEN pRefCurs FOR
			SELECT d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, E.cod_tipo_elemento, i.informazioni
			FROM spv_rel_sistema_dizionario d
			JOIN spv_collector_info i ON d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario AND i.collector_name = g_CollectorName
			LEFT JOIN elementi E ON E.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
			WHERE d.id_sistema = g_IdSistema
			AND trunc(i.data_inserimento) = trunc(sysdate)
			ORDER BY d.id_dizionario_allarme
			;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);


			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 2;

            -- gli allarmi 2,3 vengono gestiti cosi:
            -- da 0 a soglia --> allarme Giallo
            -- da soglia in poi --> allarme Rosso
            -- allarmi 1,4 : sopra soglia -> rossi (ma sofglia 01 quindi basta la presenza dell'allarme)
            IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_01  AND n_allarmi_1  >= soglia_allarme_1 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_02  AND n_allarmi_2  >= soglia_allarme_2 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  AND n_allarmi_3  >= soglia_allarme_3 )
                OR (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_04  AND n_allarmi_4  >= soglia_allarme_4 )
                )
            THEN
                pID_LIVELLO := 3;
            END IF;

            vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvProcessor - Calling AddSpvAllarmi ['||
            'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
            'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
            'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
            'pID_LIVELLO: ' || pID_LIVELLO
            ||']';
    --		printa(vStrLog);

            pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);

		END LOOP;


    	CLOSE PREFCURS;

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvProcessor - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_LOAD_MIS.MisSpvProcessor error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END MisSpvProcessor;

/*============================================================================*/
PROCEDURE MisSpvDetail(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    v_tutti NUMBER := 0;
    v_CCP NUMBER := 0;
    v_CCQ NUMBER := 0;
    v_quando DATE := sysdate;
    v_retNum NUMBER := 0;
    v_retDat DATE;
    v_retStr VARCHAR2(200);


    vElem          ELEMENTI.COD_ELEMENTO%TYPE;
    vGest          ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

    vLog  PKG_Logs.t_StandardLog;
    vStrLog VARCHAR2(2000);

    v_sql_dummy VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL';
    v_sql_nihil VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL WHERE ROWNUM<1';

    v_partName VARCHAR2(30) :=  PKG_UTLGLB.GetPartitionName (add_months(pData,-1), 
                                        PKG_UTLGLB.gcpart_MENS, 
                                        TRUE);
                                        
    v_sqlcount VARCHAR2(2000) := Pkg_utlglb.compattaSelect(                                 
     'SELECT count(*) ' ||
     'FROM  ' ||
     '   (SELECT DISTINCT cod_elemento FROM trattamento_elementi T ' ||
     '   JOIN misure_acquisite '||v_partname||' M ' ||
     '   using (cod_trattamento_elem) ' ||
     '   WHERE T.cod_tipo_misura= :gMis ' || 
     '   AND T.cod_tipo_elemento = '''||PKG_MAGO.gcClienteMT||''' ');

                                        
/*----------------------------------------------------------------------------*/
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_LOAD_MIS.MisSpvDetail',
                               pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvDetail - Start';
    printa(vStrLog);


    PKG_ELEMENTI.GetDefaultCO(vElem, vGest, vNome);







    BEGIN
        SELECT max(data_misura)
        INTO v_quando
          FROM SPV_COLLECTOR_MEASURE
          WHERE collector_name = PKG_SPV_UTL.g_collectorOkAcq
 --         AND extract(MONTH FROM data_misura)
 --                   = extract(MONTH FROM add_months(pData,-1))
          AND cod_tipo_misura IN (gMis_CCP, gMis_CCQ);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            v_quando := NULL;
    END;

    IF v_quando IS NULL THEN
        v_retStr := f_SetDettaglioStr('loadMeasure.dataUltimaAcquisizione', 'SELECT '''' FROM dual', '');
    ELSE
        v_retNum := f_SetDettaglioNum('loadMeasure.dataUltimaAcquisizione',
                        NULL,
                        PKG_SPV_UTL.f_date_to_unix(v_quando));
    END IF;


    SELECT count(*)
    INTO v_tutti
    FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaInf(vElem,
                                            1,
                                            1,
                                            pData,
                                            'CMT',
                                            1));

     v_retNum := f_SetDettaglioNum('loadMeasure.clientiMT_TOT', NULL, v_tutti);


/*
     SELECT count(*)
     INTO v_CCP
     FROM
     (SELECT DISTINCT cod_gest_elemento
      FROM SPV_COLLECTOR_MEASURE
      WHERE collector_name = PKG_SPV_UTL.g_collectorOkAcq
      AND extract(MONTH FROM data_misura)
                = extract(MONTH FROM add_months(pData,-1))
      AND cod_tipo_misura=gMis_CCP);
*/

    EXECUTE IMMEDIATE v_sqlcount INTO v_CCP USING gMis_CCP;
    
     v_retNum := f_SetDettaglioNum('loadMeasure.clientiMT_CCP', NULL, v_CCP);



     v_retNum := f_SetDettaglioNum('loadMeasure.clientiMT_CCP_Percent',
                                        NULL,
                                        CASE v_tutti
                                        WHEN 0 THEN 0
                                        ELSE 100*v_CCP/v_tutti
                                        END);

/*
     SELECT count(*)
     INTO v_CCQ
     FROM
     (SELECT DISTINCT cod_gest_elemento
      FROM SPV_COLLECTOR_MEASURE
      WHERE collector_name = PKG_SPV_UTL.g_collectorOkAcq
      AND extract(MONTH FROM data_misura)
                = extract(MONTH FROM add_months(pData,-1))
      AND cod_tipo_misura=gMis_CCQ);

*/

    EXECUTE IMMEDIATE v_sqlcount INTO v_CCQ USING gMis_CCQ;

     v_retNum := f_SetDettaglioNum('loadMeasure.clientiMT_CCQ', NULL, v_CCQ);



     v_retNum := f_SetDettaglioNum('loadMeasure.clientiMT_CCQ_Percent',
                                        NULL,
                                        CASE v_tutti
                                        WHEN 0 THEN 0
                                        ELSE 100*v_CCQ/v_tutti
                                        END);



    vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvDetail - END';
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    printa(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

EXCEPTION
WHEN OTHERS THEN
    printa('PKG_SPV_LOAD_MIS.MisSpvDetail ERROR ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END MisSpvDetail;
/*============================================================================*/

PROCEDURE MisSpvMain(	 pDoDeleteOldSpvInfo IN NUMBER DEFAULT 0
							,pDoDeleteSpvAlarm IN NUMBER DEFAULT 1
							,pDoDeleteTodaySpvInfo IN NUMBER DEFAULT 1
							,pDoCollector IN NUMBER DEFAULT 1
							,pDoProcessor IN NUMBER DEFAULT 1
							,pDoDetail IN NUMBER DEFAULT 1
							)
	IS

    vIsSupervisionEnabled VARCHAR2(100);
    vLog  PKG_Logs.t_StandardLog;
    vStrLog VARCHAR2(1000);

	BEGIN

		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_LOAD_MIS.MisSpvMain',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - Start';
		printa(vStrLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'loadMeasure.supervision_enabled');


    	vStrLog :=  'PKG_SPV_LOAD_MIS --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
			'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
			'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;

     printa(vStrLog);


--   vIsSupervisionEnabled:='true';
   IF vIsSupervisionEnabled = 'true' OR vIsSupervisionEnabled = 'TRUE' THEN
        -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
      IF pDoDeleteTodaySpvInfo = 1 THEN
          vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - delete Today spv_collector_info ';
           printa(vStrLog);
        DELETE FROM spv_collector_info
        WHERE collector_name = g_CollectorName
        AND trunc(data_inserimento) = trunc(sysdate)
        ;
      END IF;

      -- cancello TUTTE le info generate dal collector ANAGRAFICA_AUI prima di gc-48h
      IF pDoDeleteOldSpvInfo = 1 THEN
          vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - delete Old spv_collector_info ';
          printa(vStrLog);
        DELETE FROM spv_collector_info
        WHERE collector_name = g_CollectorName
        AND data_inserimento < trunc(sysdate)
        AND id_rel_sistema_dizionario IN (
          SELECT id_rel_sistema_dizionario
          FROM spv_rel_sistema_dizionario
          WHERE id_sistema = g_IdSistema
        );
      END IF;

      -- eseguo il collector e creo le spv_collector_info
      IF pDoCollector = 1 THEN
        vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - Calling MisSpvCollector ';
        printa(vStrLog);
        MisSpvCollector();
      END IF;

      -- cancello TUTTI gli allarmi generati dal collector
      IF pDoDeleteSpvAlarm = 1 THEN
        vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain -  delete from spv_allarmi ';
         printa(vStrLog);
        DELETE FROM spv_allarmi
        WHERE id_rel_sistema_dizionario IN
        (SELECT id_rel_sistema_dizionario FROM spv_rel_sistema_dizionario WHERE id_sistema = g_IdSistema);
      END IF;

      -- eseguo il processor e creo le spv_allarmi
      IF pDoProcessor = 1 THEN
        vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - Calling MisSpvProcessor ';
        printa(vStrLog);

        MisSpvProcessor();
      END IF;

      -- valorizzo il pannello di dettaglio
      IF pDoDetail = 1 THEN
        vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - Calling MisSpvDetail ';
        printa(vStrLog);

        MisSpvDetail();
      END IF;
    ELSE
      vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - Supervision Load Measures/TMESX is NOT enabled';
      printa(vStrLog);
    END IF;

		vStrLog := 'PKG_SPV_LOAD_MIS.MisSpvMain - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_LOAD_MIS.MisSpvMain error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
END MisSpvMain;

/*============================================================================*/

END PKG_SPV_LOAD_MIS;
/

show errors;