PROMPT PACKAGE BODY: PKG_SCHEDULER

CREATE OR REPLACE PACKAGE BODY PKG_SCHEDULER AS

/* ***********************************************************************************************************
   NAME:       PKG_Scheduler
   PURPOSE:    Servizi per la gestione dei jobs schedulati

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.e      11/06/2012  Moretti C.       revisione schedulazione jobs
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8.a.1    29/04/2014  Moretti C.       ElaboraJobs - Sleep 2 secondi dopo 90 sec. di elaborazione
   1.9.a.13   10/03/2015  Moretti C.       - Chiude l'elaborazione dopo 1 ora. il Rimanente sar� preso in
                                             carico dal giro successivo.
                                           - Sleep 6 secondi dopo ogni ciclo di aggregazione (problemi di
                                             sessioni bloccate da traffico su log writer di oracle)
   1.9.a.14   08/05/2015  Moretti C.       Correzione per chiusura elaborazione dopo 1 ora
                                           Parametrizzazione Secondi di Pausa tra uno step e l'altro
              18/06/2015  Moretti C.       Calcolo aggregazioni per periodo futuro pilotate da parametrizzazione
   1.9c.0     16/07/2015  Moretti C.       Definizione ambiente REPLY - Non esegue schedulazioni
    2.2.1       28/04/2017  Forno           SUPERVISIONE			-- Spec. Version 2.2.1		
    2.3.1     11/12/2017  Frosini			modificata .ConsolidaJobAggrGME per gestire anche CC-P/Cc-Q 	
   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

  --gcCaricaAnagrafiche     SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 0;
    gcLinearizzazione       SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 1;
    gcCalcolaMisureStat     SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 2;
    gcAggregMisureStat      SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 3; -- aggregazione misure statiche
    gcAggregMisureStd       SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 4; -- aggregazione misure standard
    gcAggregMisureStdPrMed  SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 5; -- aggregazione misure standard prior media
    gcAggregMisureStdPrLow  SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE := 6; -- aggregazione misure standard prior bassa

    gReuseJobNumber         NUMBER := NULL;

    -- SUPERVISIONE
    gServiceIDSessionStat SESSION_STATISTICS.SERVICE_ID%TYPE  := 'GMESMILE-service';                   --
    gStatusSessionStat SESSION_STATISTICS.STATUS%TYPE  := 'AGGREGATION_PROGRESS';  
    gTipoMisuraSessionStat SESSION_STATISTICS.STATUS%TYPE  := 'PAS|CC-P|CC-Q|PCT-P|PCT-Q';

/* ***********************************************************************************************************
 Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

-- PROCEDURE AddJobLoadAnagr          (pDataRif         DATE,
--                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
--                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE) AS
--/*-----------------------------------------------------------------------------------------------------------
--    Inserisce una richiesta di elaborazione relativa al caricamento anagragica
-------------------------------------------------------------------------------------------------------------*/
-- BEGIN
--    -- Inserisco la richiesta per Data, Organizzazione e Stato Rete
--    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,            DATARIF,   ORGANIZZAZIONE,  STATO)
--                        VALUES (gcCaricaAnagrafiche, pDataRif,  pOrganizzazione, pStato);
-- EXCEPTION
--    WHEN DUP_VAL_ON_INDEX THEN NULL;
--    WHEN OTHERS THEN RAISE;

-- END AddJobLoadAnagr;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobLinearizzazione    (pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di elaborazione di linearizzazione gerarchia
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    -- Inserisco la richiesta per Data, Organizzazione e Stato Rete
    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,          DATARIF,   ORGANIZZAZIONE,  STATO)
                        VALUES (gcLinearizzazione, pDataRif,  pOrganizzazione, pStato);
 EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS THEN RAISE;

 END AddJobLinearizzazione;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobCalcMisStatiche    (pDataRif         DATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di calcolo misura statica
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    -- Inserisco la richiesta per Data
    INSERT INTO SCHEDULED_JOBS (TIPO_JOB,        DATARIF)
                        VALUES (gcCalcolaMisureStat, pDataRif);
 EXCEPTION
    WHEN DUP_VAL_ON_INDEX THEN NULL;
    WHEN OTHERS THEN RAISE;

 END AddJobCalcMisStatiche;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggregazione (pTipo            VARCHAR2 DEFAULT NULL,
                                     pUseLog          BOOLEAN DEFAULT FALSE) AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabelle SCHEDULED_TMP_xxx
    - se parametro pTipo Null considera tabelle SCHEDULED_TMP_GEN e MIS
    - se parametro pTipo valorizzato considera la tabella relativa al valore ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vLog    PKG_Logs.t_StandardLog;
    vTipo   VARCHAR2(3) := NVL(pTipo,'GEN');
    vCur    PKG_UtlGlb.t_query_cur;
    vDatIns DATE;
    vAgrNum NUMBER := 0;
    vMaxNum NUMBER;
    vStart  DATE;
    vMinDat DATE;
    vMaxDat DATE;

    vJob    NUMBER;
    vDat    DATE;
    vOrg    NUMBER(1);
    vSta    NUMBER(1);
    vRet    TIPI_RETE.COD_TIPO_RETE%TYPE;
    vMis    TIPI_MISURA.COD_TIPO_MISURA%TYPE;

    vSql    VARCHAR2(1000) :=
              'SELECT JOB_NUMBER, DATARIF, ORGANIZZAZIONE, STATO, '                ||
                     'COD_TIPO_RETE, COD_TIPO_MISURA '                             ||
                'FROM (SELECT DATARIF, ORGANIZZAZIONE, STATO, COD_TIPO_MISURA, '   ||
                             'MIN(JOB_NUMBER) JOB_NUMBER, MAX(ID_RETE) ID_RETE '   ||
                        'FROM SCHEDULED_TMP_#TAB# '                                ||
                       'WHERE JOB_NUMBER <= :num '                                 ||
                       'GROUP BY DATARIF, ORGANIZZAZIONE, STATO, COD_TIPO_MISURA ' ||
                      ') '                                                         ||
               'INNER JOIN TIPI_RETE USING(ID_RETE) '                              ||
               'ORDER BY 1 ';

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.ConsolidaJobAggregazione',
                                pCodice       => NVL(pTipo,'<null>'),
                                pTipo         => 'Tipo misure',
                                pForceLogFile => TRUE);

    IF vTipo NOT IN ('MET','GME','GEN') THEN
        vTipo := 'GEN';
    END IF;

    BEGIN
        SELECT SYSDATE - (1 / 24 * RITARDO_HH)
          INTO vDatIns
          FROM SCHEDULED_JOBS_DEF
         WHERE TIPO_JOB = gcAggregMisureStd;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    EXECUTE IMMEDIATE 'SELECT MAX(JOB_NUMBER) FROM SCHEDULED_TMP_'||vTipo INTO vMaxNum;

    OPEN vCur FOR REPLACE(vSql,'#TAB#',vTipo) USING vMaxNum;
    LOOP
        FETCH vCur INTO vJob, vDat, vOrg, vSta, vRet, vMis;
        EXIT WHEN vCur%NOTFOUND;
        gReuseJobNumber := vJob;
        AddJobCalcAggregazione(NULL,vDat,vOrg,vSta,vRet,vMis,vDatIns);
        vAgrNum := vAgrNum + 1;
    END LOOP;
    CLOSE vCur;

    IF pUseLog THEN
        IF vAgrNum > 0 THEN
            EXECUTE IMMEDIATE 'SELECT MIN(DATARIF), MAX(DATARIF), MIN(DATAINS) FROM SCHEDULED_TMP_'||vTipo||' '||
                               'WHERE JOB_NUMBER <= :num '
                         INTO vMinDat, vMaxDat, vStart USING vMaxNum ;
            vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                        pFunzione     => 'PKG_Scheduler.ConsolidaJobAggregazione',
                                        pTipo         => 'Tipo misure',
                                        pCodice       => vTipo,
                                        pNome         => 'num: '||TO_CHAR(vAgrNum),
                                        pDataRif      => vMinDat,
                                        pDataRif_fine => vMaxDat,
                                        pForceLogFile => TRUE);
            PKG_Logs.StdLogAddTxt('Inizio immissione',PKG_Mago.StdOutDate(vStart),NULL,vLog);
            PKG_Logs.StdLogPrint (vLog);
        END IF;
    END IF;

    EXECUTE IMMEDIATE 'DELETE FROM SCHEDULED_TMP_'||vTipo||' WHERE JOB_NUMBER <= :num' USING vMaxNum;

    --COMMIT;

 EXCEPTION
    WHEN OTHERS THEN RAISE;
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END ConsolidaJobAggregazione;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggrMETEO AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabella SCHEDULED_TMP_MET
-----------------------------------------------------------------------------------------------------------*/
 BEGIN

    ConsolidaJobAggregazione('MET',TRUE);
    COMMIT;

 END ConsolidaJobAggrMETEO;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE ConsolidaJobAggrGME AS
/*-----------------------------------------------------------------------------------------------------------
    consolida i job di aggregazione pendenti da tabella SCHEDULED_TMP_GME
-----------------------------------------------------------------------------------------------------------*/
 l_nextValSESSION integer;
 
 -- ----------------------------------------------------------------------------------------------------------

gv_RegExpSepara CONSTANT VARCHAR2(10) := '[^\|]+';

-- ----------------------------------------------------------------------------------------------------------

 BEGIN

    /*
    Supervisione GMESMILE - PAS + CC-P/Cc-Q
    gestione nella SESSIONSTATIC anche per il caricamento della PAS
    */
    
    for riga in ( select regexp_substr(gTipoMisuraSessionStat,gv_RegExpSepara, 1, level) misura from dual
            connect by regexp_substr(gTipoMisuraSessionStat, gv_RegExpSepara, 1, level) is not null )
    LOOP

        select MAGO.SESSION_STATISTIC_IDSEQ.nextval into l_nextValSESSION  from dual ;
    
        PKG_STATS .InitStatisticSession (
                                    l_nextValSESSION,
                                    gServiceIDSessionStat,
                                    l_nextValSESSION,
                                    riga.misura,
                                    gStatusSessionStat   ,
                                    sysdate,
                                    NULL,
                                    NULL,
                                    NULL,
                                    sysdate,
                                    NULL,
                                    0
                                   );

    END LOOP;

    ConsolidaJobAggregazione('GME',TRUE);
    COMMIT;

 END ConsolidaJobAggrGME;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddJobCalcAggregazione   (pTipoProcesso    VARCHAR2,
                                     pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.STATO%TYPE,
                                     pTipoRete        TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     pTipoMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     pDataIns         DATE DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce una richiesta di calcolo aggregazione
-----------------------------------------------------------------------------------------------------------*/
    vLog       PKG_Logs.t_StandardLog;
    vIdReteTip TIPI_RETE.ID_RETE%TYPE;
    vPriAggr   TIPI_MISURA.PRIORITA_AGGR%TYPE;
    vTipoJob   SCHEDULED_JOBS_DEF.TIPO_JOB%TYPE;
    vTrovato   BOOLEAN := FALSE;
    vTmpTable  VARCHAR2(30);
    vNum       INTEGER;
 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.AddJobCalcAggregazione',
                                pDataRif      => pDataRif,
                                pTipo         => 'Organizzazione/Stato',
                                pCodice       => NVL(TO_CHAR(pOrganizzazione),'<NULL>')||' / '||NVL(TO_CHAR(pStato),'<NULL>'),
                                pForceLogFile => TRUE);

    SELECT ID_RETE
      INTO vIdReteTip
      FROM TIPI_RETE
     WHERE COD_TIPO_RETE = pTipoRete;

    IF PKG_Misure.IsMisuraStatica(pTipoMis) = PKG_UtlGlb.gkFlagON THEN
        vTipoJob := gcAggregMisureStat;
    ELSE
        vTipoJob := -1; -- vTipoJob da calcolare
    END IF;

    IF (pTipoProcesso IS NULL) OR (vTipoJob = gcAggregMisureStat) THEN
        IF vTipoJob <> gcAggregMisureStat THEN
            SELECT PRIORITA_AGGR
              INTO vPriAggr
              FROM TIPI_MISURA
             WHERE COD_TIPO_MISURA = pTipoMis;
            CASE
                WHEN vPriAggr = 1 THEN vTipoJob := gcAggregMisureStd;
                WHEN vPriAggr = 2 THEN vTipoJob := gcAggregMisureStdPrMed;
                WHEN vPriAggr = 3 THEN vTipoJob := gcAggregMisureStdPrlow;
            END CASE;
        END IF;
        -- Aggiungo il tipo rete da elaborare alla richiesta
        UPDATE SCHEDULED_JOBS
          SET ID_RETE = ID_RETE + vIdReteTip
        WHERE TIPO_JOB = vTipoJob
          AND DATARIF = pDataRif
          AND ORGANIZZAZIONE = pOrganizzazione
          AND STATO = pStato
          AND COD_TIPO_MISURA = pTipoMis
          AND BITAND(ID_RETE,vIdReteTip) <> vIdReteTip;
        IF SQL%ROWCOUNT = 0 THEN
            BEGIN
                IF gReuseJobNumber IS NOT NULL THEN
                    SELECT COUNT(*)
                      INTO vNum
                      FROM SCHEDULED_JOBS
                     WHERE JOB_NUMBER = gReuseJobNumber;
                    IF vNum <> 0 THEN
                        gReuseJobNumber := NULL;
                    END IF;
                END IF;
                -- Inserisco la richiesta
                INSERT INTO SCHEDULED_JOBS (JOB_NUMBER,      TIPO_JOB, ORGANIZZAZIONE,  STATO,  DATARIF,  ID_RETE,    COD_TIPO_MISURA, DATAINS)
                                    VALUES (gReuseJobNumber, vTipoJob, pOrganizzazione, pStato, pDataRif, vIdReteTip, pTipoMis,        pDataIns);
                --PKG_Logs.TraceLog(vLog,'Inserita richiesta aggregazione: Tip='||vTipoJob||' - Org='||pOrganizzazione||
                --                                                     ' - Sta='||pStato  ||' - Rte='||vIdReteTip||' - Mis='||pTipoMis||
                --                                                     ' - Dat='||Pkg_Mago.StdOutDate(pDataRif),PKG_UtlGlb.gcTrace_VRB);
            END;
        END IF;
    ELSE
        CASE pTipoProcesso
            WHEN 'GME' THEN vTmpTable := 'SCHEDULED_TMP_GME';
            WHEN 'MET' THEN vTmpTable := 'SCHEDULED_TMP_MET';
            ELSE            vTmpTable := 'SCHEDULED_TMP_GEN';
        END CASE;
        EXECUTE IMMEDIATE 'SELECT COUNT(*) '                ||
                            'FROM '||vTmpTable||' '         ||
                           'WHERE DATARIF = :dt '           ||
                             'AND ORGANIZZAZIONE = :org '   ||
                             'AND STATO = :sta '            ||
                             'AND ID_RETE = :ret '          ||
                             'AND COD_TIPO_MISURA = :mis '
                      INTO vNum USING pDataRif, pOrganizzazione, pStato, vIdReteTip, pTipoMis;
        IF vNum = 0 THEN
            EXECUTE IMMEDIATE 'INSERT INTO '||vTmpTable||'(DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, COD_TIPO_MISURA) '||
                                                   'VALUES(:dt,     :org,           :sta,  :ret,    :mis)'
                        USING pDataRif, pOrganizzazione, pStato, vIdReteTip, pTipoMis;
        END IF;
    END IF;
    gReuseJobNumber := NULL;

    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            gReuseJobNumber := NULL;
        WHEN OTHERS THEN
            gReuseJobNumber := NULL;
            PKG_Logs.StdLogAddTxt('Tipo Rete',NVL(TO_CHAR(pTipoRete),'<NULL>'),NULL,vLog);
            PKG_Logs.StdLogAddTxt('Tipo Misura',NVL(TO_CHAR(pTipoMis),'<NULL>'),NULL,vLog);
            PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
            PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
            RAISE;
 END AddJobCalcAggregazione;

-- ----------------------------------------------------------------------------------------------------------

  PROCEDURE ElaboraJobs AS
/*-----------------------------------------------------------------------------------------------------------
    Elabora le richieste di elaborazione presenti
-----------------------------------------------------------------------------------------------------------*/
    vLog              PKG_Logs.t_StandardLog;
    vTrovato          BOOLEAN;
    vTxt              VARCHAR2(200);
    vRestart          INTEGER := 0;
    vTotElaborazioni  NUMBER := 0;
    vTimeFineJob      DATE := SYSDATE + (1/24);
    vTimeFineLoop     DATE;
    vTimeStartTot     TIMESTAMP := SYSTIMESTAMP;
    vTimeStart        TIMESTAMP;
    vScheduleWait     NUMBER := PKG_UtlGlb.GetParamGenNum('SCHED_WAIT',6);

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassSCH,
                                pFunzione     => 'PKG_Scheduler.ElaboraJobs',
                                pForceLogFile => TRUE);

    PKG_Logs.TraceLog('Inizio - Sched.ElaboraJobs - '||TO_CHAR(vTimeStartTot,'dd/mm/yyyy hh24:mi:ss'),PKG_UtlGlb.gcTrace_VRB);

    BEGIN
        SELECT CASE SCHEDULE_TYPE
                WHEN 'CALENDAR' THEN PKG_UtlGlb.gkFlagON
                                ELSE PKG_UtlGlb.gkFlagOFF
               END RESTART
          INTO vRestart
          FROM USER_SCHEDULER_JOBS
         WHERE JOB_NAME = 'MAGO_SCHEDULER';
    EXCEPTION
        WHEN OTHERS THEN vRestart := PKG_UtlGlb.gkFlagOFF;
    END;

    LOOP

        vTrovato := FALSE;

        vTimeFineLoop := SYSDATE + ((1/86400) * 90);

        FOR vRow IN (SELECT JOBMIN,JOBMAX,DATARIF,TIPO_JOB,ORGANIZZAZIONE,STATO,DATA_ELAB,ID_RETE,COD_TIPO_MISURA,MISURA_STATICA,NUM,DATAINS
                       FROM (SELECT A.JOBMIN,A.JOBMAX,A.DATARIF,A.TIPO_JOB,A.ORGANIZZAZIONE,A.STATO,A.DATA_ELAB,
                                    A.ID_RETE,A.COD_TIPO_MISURA,A.MISURA_STATICA,A.NUM,DATAINS,
                                    CASE WHEN TIPO_JOB IN (gcAggregMisureStd,gcAggregMisureStdPrMed,gcAggregMisureStdPrLow) THEN
                                            CASE WHEN PKG_UtlGlb.GetParamGenNum('ELAB_AGG_FUTURE',PKG_UtlGlb.gkFlagON) = PKG_UtlGlb.gkFlagOFF
                                                       THEN CASE WHEN DATARIF > SYSDATE + (1/1440*30)
                                                                      THEN PKG_UtlGlb.gkFlagOFF
                                                                      ELSE PKG_UtlGlb.gkFlagON
                                                            END
                                                       ELSE PKG_UtlGlb.gkFlagON
                                            END
                                         ELSE PKG_UtlGlb.gkFlagON
                                    END ELABORA
                               FROM (SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                            CASE
                                               WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                               ELSE DATARIF + (1 / 24 * RITARDO_HH)
                                            END data_elab, NULL DATAINS,
                                            ID_RETE, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                                       FROM SCHEDULED_JOBS
                                      INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                                       LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                                      WHERE TIPO_JOB IN (gcLinearizzazione, gcCalcolaMisureStat)
                                        AND DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH ))
                                      GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, MISURA_STATICA, RITARDO_HH
                                     UNION ALL
                                     SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                            CASE
                                               WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                               ELSE DATARIF + (1 / 24 * RITARDO_HH)
                                            END data_elab, NULL DATAINS,
                                            NULL, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                                       FROM SCHEDULED_JOBS
                                      INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                                       LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                                      WHERE TIPO_JOB = gcAggregMisureStat
                                        AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                                      GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH
                                     UNION ALL
                                     SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                                            CASE
                                               WHEN MAX(DATAINS)  < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                                               ELSE MAX(DATAINS)  + (1 / 24 * RITARDO_HH)
                                            END data_elab, MAX(DATAINS),
                                            NULL, NULL, MISURA_STATICA, COUNT(*) num
                                       FROM SCHEDULED_JOBS
                                      INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                                       LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                                      WHERE TIPO_JOB IN (gcAggregMisureStd,gcAggregMisureStdPrMed,gcAggregMisureStdPrLow)
                                        AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                                        AND PKG_Mago.IsReplayFL = PKG_UtlGlb.gkFlagOFF  -- Replay (PKG_UtlGlb.gkFlagON) non aggrega
                                      GROUP BY TIPO_JOB, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH, DATARIF
                                    ) A
                            )
                      WHERE ELABORA = PKG_UtlGlb.gkFlagON
                      ORDER BY TIPO_JOB,
                               CASE
                                  --WHEN TIPO_JOB IN (gcAggregMisureStd,gcAggregMisureStdPrMed,gcAggregMisureStdPrLow) THEN
                                  WHEN TIPO_JOB = gcAggregMisureStd THEN
                                    CASE
                                        WHEN TRUNC(SYSDATE,'HH24')        = TRUNC(DATARIF,'HH24')
                                          OR TRUNC(SYSDATE + 1/24,'HH24') = TRUNC(DATARIF,'HH24')
                                             THEN TIPO_JOB + (10*TIPO_JOB)  -- se in ora corrente o successiva aggreg prioritaria
                                             ELSE TIPO_JOB + (100*TIPO_JOB) -- altrimenti aggreg non prioritaria
                                    END
                                  ELSE TIPO_JOB
                               END,
                               CASE
                                  WHEN DATA_ELAB IS NULL THEN 0
                                  ELSE 1
                               END,
                               DATARIF, STATO DESC, ORGANIZZAZIONE, ID_RETE, MISURA_STATICA DESC, COD_TIPO_MISURA
                    )
        LOOP

            vTimeStart := SYSTIMESTAMP;

            vTotElaborazioni := vTotElaborazioni + 1;

            vTxt := ' ';

            --PKG_Logs.TraceLog('Inizio - ElaboraJobs - '||
            --                '  JOB='||vRow.TIPO_JOB||
            --               '  Data='||TO_CHAR(vRow.DATARIF,'dd/mm/yyyy hh24:mi.ss')||
            --                '  Org='||NVL(TO_CHAR(vRow.ORGANIZZAZIONE),' ')||
            --                '  Sta='||NVL(TO_CHAR(vRow.STATO),' ')||
            --                '  Ret='||NVL(TO_CHAR(vRow.ID_RETE),' ')||
            --                '  Mis='||NVL(vRow.COD_TIPO_MISURA,' '),PKG_UtlGlb.gcTrace_VRB);

            CASE

                WHEN vRow.TIPO_JOB = gcLinearizzazione THEN
                    PKG_Aggregazioni.LinearizzaGerarchia(NULL,vRow.DATARIF,vRow.ORGANIZZAZIONE,vRow.STATO);
                    DELETE FROM SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                                 AND DATARIF = vRow.DATARIF
                                                 AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                                 AND STATO = vRow.STATO;
                    PKG_Logs.StdLogAddTxt('Linearizza Gerarchia'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Tip='||vRow.TIPO_JOB||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(SYSTIMESTAMP - vTimeStart,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB = gcCalcolaMisureStat THEN
                    PKG_Misure.CalcMisureStatiche(vRow.DATARIF,vRow.ORGANIZZAZIONE,vRow.STATO);
                    DELETE FROM SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                                 AND DATARIF = vRow.DATARIF;
                    PKG_Logs.StdLogAddTxt('Calcola Mis.Statiche'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Tip='||vRow.TIPO_JOB||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(SYSTIMESTAMP - vTimeStart,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB = gcAggregMisureStat THEN
                    -- esegue il calcolo delle aggregate per i soli tipi misura interessati
                    DELETE GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey;
                    FOR i IN (SELECT DISTINCT PKG_Mago.gcTmpTipMisKey, COD_TIPO_MISURA
                                FROM SCHEDULED_JOBS
                               INNER JOIN TIPI_MISURA USING (COD_TIPO_MISURA)
                               WHERE TIPO_JOB = vRow.TIPO_JOB
                                 AND DATARIF = vRow.DATARIF
                                 AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                 AND STATO = vRow.STATO
                                 AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                 AND PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = vRow.MISURA_STATICA
                             ) LOOP
                        INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1) VALUES (PKG_Mago.gcTmpTipMisKey,i.COD_TIPO_MISURA);
                        vTxt := vTxt || i.COD_TIPO_MISURA||' ';
                    END LOOP;
                    PKG_Aggregazioni.EseguiAggregazione(vRow.ORGANIZZAZIONE,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
                    DELETE SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                            AND DATARIF = vRow.DATARIF
                                            AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                            AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                            AND STATO = vRow.STATO
                                            AND COD_TIPO_MISURA IN (SELECT ALF1
                                                                      FROM GTTD_VALORI_TEMP
                                                                     WHERE TIP = PKG_Mago.gcTmpTipMisKey);
                    PKG_Logs.StdLogAddTxt('Aggrega Mis.Statiche'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Tip='||vRow.TIPO_JOB||'/'||vRow.MISURA_STATICA||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        ,  SUBSTR(TO_CHAR(SYSTIMESTAMP - vTimeStart,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
                WHEN vRow.TIPO_JOB IN (gcAggregMisureStd,gcAggregMisureStdPrMed,gcAggregMisureStdPrLow) THEN
                    -- esegue il calcolo delle aggregate per i soli tipi misura interessati
                    DELETE GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey;
                    FOR i IN (SELECT DISTINCT COD_TIPO_MISURA
                                FROM SCHEDULED_JOBS
                               INNER JOIN TIPI_MISURA USING (COD_TIPO_MISURA)
                               WHERE TIPO_JOB = vRow.TIPO_JOB
                                 AND DATARIF = vRow.DATARIF
                                 AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                 AND STATO = vRow.STATO
                                 AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                 AND PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) = vRow.MISURA_STATICA
                             ) LOOP
                        INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1) VALUES (PKG_Mago.gcTmpTipMisKey,i.COD_TIPO_MISURA);
                        vTxt := vTxt || i.COD_TIPO_MISURA||' ';
                    END LOOP;
                    PKG_Aggregazioni.EseguiAggregazione(vRow.ORGANIZZAZIONE,vRow.STATO,vRow.DATARIF,vRow.MISURA_STATICA);
                    DELETE SCHEDULED_JOBS WHERE TIPO_JOB = vRow.TIPO_JOB
                                            AND DATARIF = vRow.DATARIF
                                            AND JOB_NUMBER BETWEEN vRow.JobMin AND vRow.JobMax
                                            AND STATO = vRow.STATO
                                            AND ORGANIZZAZIONE = vRow.ORGANIZZAZIONE
                                            AND COD_TIPO_MISURA IN (SELECT ALF1
                                                                      FROM GTTD_VALORI_TEMP
                                                                     WHERE TIP = PKG_Mago.gcTmpTipMisKey);
                    PKG_Logs.StdLogAddTxt('Aggregazione Misure'
                                        , PKG_Mago.StdOutDate(vRow.DATARIF)||
                                          '-Tip='||vRow.TIPO_JOB||'/'||vRow.MISURA_STATICA||
                                          '-Org='||vRow.ORGANIZZAZIONE||
                                          '-Sta='||vRow.STATO||
                                          '-Mis='||REPLACE(TRIM(vTxt),' ',',')
                                        , SUBSTR(TO_CHAR(SYSTIMESTAMP - vTimeStart,'hh24:mi:ss.ff3'),9,12)
                                        , vLog);
            END CASE;

            COMMIT;
            DBMS_LOCK.SLEEP (vScheduleWait);

            vTrovato := TRUE;

            IF SYSDATE > vTimeFineLoop THEN  -- superato il tempo per il loop interno
                EXIT;  -- esco dal loop per riselezionare eventuali nuove le richieste in base alla priorita'
            END IF;

        END LOOP;

        IF NOT vTrovato THEN
            EXIT;  -- il loop interno non ha rilevato elaborazioni. Esco dal loop esterno !
        END IF;

        IF vRestart = PKG_UtlGlb.gkFlagON THEN
            IF SYSDATE > vTimeFineJob THEN
                EXIT;   -- e' il tempo massimo per la durata dell'elanorazione. Esco! le richieste rimanenti saranno prese in carico dal run successivo
            END IF;
        END IF;

    END LOOP;

    IF vTotElaborazioni > 0 THEN
        PKG_Logs.StdLogPrint (vLog);
        PKG_Logs.StdLogAddTxt('Sched.ElaboraJobs - Start '||TO_CHAR(vTimeStartTot,'hh24:mi:ss')||' del '||TO_CHAR(vTimeStartTot,'dd/mm/yyyy')||
                                             ' - Durata: '||SUBSTR(TO_CHAR(SYSDATE - vTimeStartTot),12,8),FALSE,NULL,vLog);
    END IF;

    PKG_Logs.TraceLog('Fine   - Sched.ElaboraJobs - '||TO_CHAR(SYSDATE,'dd/mm/yyyy hh24:mi:ss')||
                                        ' - Durata: '||SUBSTR(TO_CHAR(SYSDATE - vTimeStartTot),12,8),PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END ElaboraJobs;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Scheduler;
/