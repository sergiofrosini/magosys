PROMPT NUOVA TABELLA SUPERVISIONE: SPV_SOGLIE 


BEGIN
	    EXECUTE IMMEDIATE 'ALTER TABLE SPV_SOGLIE DROP PRIMARY KEY CASCADE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/
BEGIN
	    EXECUTE IMMEDIATE 'DROP TABLE SPV_SOGLIE CASCADE CONSTRAINTS';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/

CREATE TABLE SPV_SOGLIE
(
  ID_SOGLIA   NUMBER,
  ID_SISTEMA  NUMBER                            NOT NULL,
  ID_CHIAVE   VARCHAR2(100 BYTE)                NOT NULL,
  VALORE      VARCHAR2(300 BYTE)                NOT NULL
)
TABLESPACE &TBS&DAT
PCTUSED    0
PCTFREE    10
INITRANS   1
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE SPV_SOGLIE IS 'Tabella relazione soglie con i singoli allarmi (dizionario descrizione)';

COMMENT ON COLUMN SPV_SOGLIE.ID_SOGLIA IS 'PK della soglia (progressivo)';

COMMENT ON COLUMN SPV_SOGLIE.ID_SISTEMA IS 'ID del sistema a cui � legato la soglia';

COMMENT ON COLUMN SPV_SOGLIE.ID_CHIAVE IS 'ID della soglia ';

COMMENT ON COLUMN SPV_SOGLIE.VALORE IS 'Valore del soglia da visualizzare sul FE ';



CREATE UNIQUE INDEX SPV_SOGLIE_I1 ON SPV_SOGLIE
(ID_SISTEMA, ID_CHIAVE)
LOGGING
TABLESPACE &TBS&IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


CREATE UNIQUE INDEX SPV_SOGLIE_PK ON SPV_SOGLIE
(ID_SOGLIA)
LOGGING
TABLESPACE &TBS&IDX
PCTFREE    10
INITRANS   2
MAXTRANS   255
STORAGE    (
            INITIAL          80K
            NEXT             1M
            MINEXTENTS       1
            MAXEXTENTS       UNLIMITED
            PCTINCREASE      0
            BUFFER_POOL      DEFAULT
           )
NOPARALLEL;


ALTER TABLE SPV_SOGLIE ADD (
  PRIMARY KEY
  (ID_SOGLIA)
  USING INDEX SPV_SOGLIE_PK
  ENABLE VALIDATE);

ALTER TABLE SPV_SOGLIE ADD (
  CONSTRAINT FK_SOGLIE_F1 
  FOREIGN KEY (ID_SISTEMA) 
  REFERENCES SPV_SISTEMA (ID_SISTEMA)
  ENABLE VALIDATE);
