PROMPT JOB: MAGO_SPV_LOAD_MEASURE_SMILE

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB (job_name  => 'MAGO_SPV_LOAD_MEASURE_SMILE');
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -27475 THEN NULL; -- ORA-00942: JOB non PRESENTE
                                         ELSE RAISE;
                     END IF;
END;
/	  


BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'MAGO_SPV_LOAD_MEASURE_SMILE'
      ,start_date      => TO_TIMESTAMP_TZ('2017/03/09 00:05:00.000000 Europe/Berlin','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;INTERVAL=1'
      ,end_date        => NULL      
      ,job_type        => 'STORED_PROCEDURE'
      ,job_action      => 'PKG_SPV_SMILE_GME.smileGmeSpvMain'
      ,comments        => 'Recupera le misure necessarie per il calcolo della Supervisione del modulo Smile GME'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 5);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_MEASURE_SMILE'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'MAGO_SPV_LOAD_MEASURE_SMILE');
END;
/
