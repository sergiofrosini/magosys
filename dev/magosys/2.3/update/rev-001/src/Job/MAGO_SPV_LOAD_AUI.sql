PROMPT JOB: MAGO_SPV_LOAD_AUI

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB (job_name  => 'MAGO_SPV_LOAD_AUI');
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -27475 THEN NULL; -- ORA-00942: JOB non PRESENTE
                                         ELSE RAISE;
                     END IF;
END;
/	  

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'MAGO.MAGO_SPV_LOAD_AUI'
      ,start_date      => TO_TIMESTAMP_TZ('2017/03/09 02:05:00.000000 Europe/Berlin','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;INTERVAL=1'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'STORED_PROCEDURE'
      ,job_action      => ' pkg_spv_anag_aui.auiSpvMain'
      ,comments        => 'Recupera le informazioni per il modulo supervisione AUI'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 5);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.MAGO_SPV_LOAD_AUI'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'MAGO.MAGO_SPV_LOAD_AUI');
END;
/