SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.3.1 SRC
PROMPT file :  build_Mago_2_3_1.sql
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT Grants 
PROMPT

conn sys/sys_dba&tns_arcdb2 as sysdba
@./Grants/MANAGE_SCHEDULER.sql

conn sar_admin/sar_admin_dba&tns_arcdb2
@./Grants/ADMIN_PARAMETRI_GENERALI.sql
@./Grants/ADMIN_PKG_UTL.sql

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2


PROMPT _______________________________________________________________________________________
PROMPT Dblinks 
PROMPT
conn mago/mago&tns_arcdb2
@./Dblinks/PKG1_CREL.sql
@./Dblinks/PKG1_RIGEDI.sql
@./Dblinks/RES_DB_NAZ.sql

PROMPT _______________________________________________________________________________________
PROMPT Stop/Drop old jobs 
PROMPT
conn mago/mago&tns_arcdb2
@./ScheduledJobs/STOP_SINCRO_TRANSFER_RES.sql
@./ScheduledJobs/STOP_SINCRO_TRANSFER_RES_R.sql
@./ScheduledJobs/STOP_STORICO_REGISTRO_INVIO.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables 
PROMPT
conn mago/mago&tns_arcdb2
@./Tables/GTTD_GET_MISURE.sql
@./Tables/GTTD_MIS_SCHEMA_FRONT.sql
@./Tables/GTTD_MISURE.sql
@./Tables/MISURE_ACQUISITE.sql
@./Tables/NOWCAST_CENTRALINE.sql
@./Tables/REGISTRO_RES_INVIO.sql
@./Tables/REGISTRO_RES_RITARDO.sql
@./Tables/SCAGLIONI_DI_POTENZA.sql
@./Tables/SPV_SISTEMA.sql
@./Tables/SPV_DIZIONARIO_ALLARMI.sql
@./Tables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./Tables/SPV_LIVELLO_ALLARMI.sql
@./Tables/SPV_ALLARMI.sql
@./Tables/SPV_CATEGORIA.sql
@./Tables/SPV_COLLECTOR_INFO.sql
@./Tables/SPV_COLLECTOR_MEASURE.sql
@./Tables/SPV_CONFIG_NOTIFICA.sql
@./Tables/SPV_CONFIG_NOTIFICA_ALARMLIST.sql
@./Tables/SPV_DETTAGLIO_PARAMETRI.sql
@./Tables/SPV_MACRO.sql
@./Tables/SPV_REL_CATEGORIA_SISTEMA.sql
@./Tables/SPV_REL_MACRO_CATEGORIA.sql
@./Tables/SPV_SOGLIE.sql
@./Tables/SPV_REL_SOGLIE_DIZIONARIO.sql

PROMPT _______________________________________________________________________________________
PROMPT Indexes 
PROMPT
conn mago/mago&tns_arcdb2
@./Indexes/SPV_COLL_MEAS_IDX.sql

PROMPT _______________________________________________________________________________________
PROMPT Sequences 
PROMPT
@./Sequences/SPV_ALLARMI_SEQ.sql
@./Sequences/SPV_COLLECTOR_INFO_SEQ.sql
@./Sequences/SPV_COLLECTOR_MEASURE_SEQ.sql
@./Sequences/SPV_CONFIG_NOTIF_SEQ.sql

PROMPT _______________________________________________________________________________________
PROMPT Triggers 
PROMPT
@./Triggers/AFT_IUR_ELEMENTI_DEF.sql

PROMPT _______________________________________________________________________________________
PROMPT Synonyms 
PROMPT
@./Synonyms/CREL_PKG_CREL.sql

PROMPT _______________________________________________________________________________________
PROMPT Types 
PROMPT
@./Types/T_MISMETEO_OBJ.sql
@./Types/T_ROWGERARCHIA.sql
@./Types/T_SISLIV.sql
@./Types/T_SPVCOLLECTORINFO.sql
@./Types/T_SPVCOLLECTORMEASURE.sql
@./Types/T_SPVELEMENT.sql
@./Types/T_SPVSOGLIA.sql

PROMPT _______________________________________________________________________________________
PROMPT Views 
PROMPT
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_ELEMENTI_DGF.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_MAGO.sql
@./Packages/PKG_MAGO_DGF.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_PROFILI.sql
@./Packages/PKG_REPORTS.sql
@./Packages/PKG_SPV_ANAG_AUI.sql
@./Packages/PKG_SPV_ELEMENT.sql
@./Packages/PKG_SPV_LOAD_MIS.sql
@./Packages/PKG_SPV_RETE_ELE.sql
@./Packages/PKG_SPV_SMILE_GME.sql
@./Packages/PKG_SPV_UTL.sql
@./Packages/PKG_STATS.sql
@./Packages/PKG_SUPERVISIONE.sql
@./Packages/PKG_TRANSFER_RES.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_KPI.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MAGO_DGF.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_PROFILI.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql
@./PackageBodies/PKG_SPV_ELEMENT.sql
@./PackageBodies/PKG_SPV_LOAD_MIS.sql
@./PackageBodies/PKG_SPV_RETE_ELE.sql
@./PackageBodies/PKG_SPV_SMILE_GME.sql
@./PackageBodies/PKG_SPV_UTL.sql
@./PackageBodies/PKG_STATS.sql
@./PackageBodies/PKG_SUPERVISIONE.sql
@./PackageBodies/PKG_TRANSFER_RES.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables 
PROMPT

conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/ADM_PARAMETRI_GENERALI.sql
@./InitTables/RUOLI.sql
@./InitTables/SAR_ADMIN_PARAMETRI_GENERALI.sql

conn crel/crel&tns_arcdb1
@./InitTables/CREL_APPL_DEFAULTS.sql

conn mago/mago&tns_arcdb2
@./InitTables/METEO_JOB_STATIC_CONFIG.sql
@./InitTables/MJ_STATIC_CONFIG_involvedMeasureType.sql
@./InitTables/SCAGLIONI_DI_POTENZA.sql
@./InitTables/SPV_CATEGORIA.sql
@./InitTables/SPV_SISTEMA.sql
@./InitTables/SPV_DIZIONARIO_ALLARMI.sql
@./InitTables/SPV_LIVELLO_ALLARMI.sql
@./InitTables/SPV_MACRO.sql
@./InitTables/SPV_REL_CATEGORIA_SISTEMA.sql
@./InitTables/SPV_REL_MACRO_CATEGORIA.sql
@./InitTables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./InitTables/SPV_SOGLIE.sql
@./InitTables/SPV_REL_SOGLIE_DIZIONARIO.sql
@./InitTables/SPV_DETTAGLIO_PARAMETRI.sql
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPI_MISURA_CONV_ORIG.sql
@./InitTables/TIPI_MISURA_CONV_UM.sql
@./InitTables/GRUPPI_MISURA.sql


PROMPT _______________________________________________________________________________________
PROMPT Scheduled Jobs
PROMPT
@./Job/MAGO_SPV_LOAD_AUI.sql
@./Job/MAGO_SPV_LOAD_MEASURE_SMILE.sql

PROMPT _______________________________________________________________________________________
PROMPT Scheduled Jobs
PROMPT
@./ScheduledJobs/MAGO_SPV_CLEAN_COLL_MIS.sql
@./ScheduledJobs/MAGO_SPV_LOAD_LOAD_MIS.sql
@./ScheduledJobs/MAGO_SPV_LOAD_RETE_ELE.sql
@./ScheduledJobs/SINCRO_TRANSFER_RES.sql
@./ScheduledJobs/SINCRO_TRANSFER_RES_R.sql
@./ScheduledJobs/STORICO_REGISTRO_INVIO.sql


COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.3.1 SRC
PROMPT

spool off
