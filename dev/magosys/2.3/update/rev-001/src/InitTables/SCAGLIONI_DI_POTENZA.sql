PROMPT Init table SCAGLIONI_DI_POTENZA

MERGE INTO SCAGLIONI_DI_POTENZA M
USING
(
SELECT 399 SCAGLIONE_POT,'Scaglione da 0 a 399 (KW)' DESCRIZIONE,0 POT_DA,400000 POT_A FROM DUAL
UNION
SELECT 1000 SCAGLIONE_POT,'Scaglione da 400 a 1000 (KW)' DESCRIZIONE,400000 POT_DA,1000000 POT_A FROM DUAL
UNION
SELECT 999999999 SCAGLIONE_POT,'Scaglione da  1001 ed oltre  (KW)' DESCRIZIONE,1000000 POT_DA,999999999999 POT_A FROM DUAL
) U
ON (M.SCAGLIONE_POT = U.SCAGLIONE_POT)
WHEN MATCHED THEN UPDATE SET
DESCRIZIONE = U.DESCRIZIONE,
POT_DA = U.POT_DA,
POT_A = U.POT_A
WHEN NOT MATCHED THEN INSERT
( SCAGLIONE_POT,DESCRIZIONE,POT_DA,POT_A )
VALUES
( U.SCAGLIONE_POT,U.DESCRIZIONE,U.POT_DA,U.POT_A );

COMMIT;
/