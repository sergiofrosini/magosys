PROMPT TABELLA: SPV_MACRO

MERGE INTO SPV_MACRO A USING
 (SELECT
  2 as ID_MACRO,
  '.M.A.G.O.' as DESCRIZIONE
  FROM DUAL) B
ON (A.ID_MACRO = B.ID_MACRO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_MACRO, DESCRIZIONE)
VALUES (
  B.ID_MACRO, B.DESCRIZIONE)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE;

MERGE INTO SPV_MACRO A USING
 (SELECT
  3 as ID_MACRO,
  'DATI IN USCITA' as DESCRIZIONE
  FROM DUAL) B
ON (A.ID_MACRO = B.ID_MACRO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_MACRO, DESCRIZIONE)
VALUES (
  B.ID_MACRO, B.DESCRIZIONE)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE;

MERGE INTO SPV_MACRO A USING
 (SELECT
  1 as ID_MACRO,
  'DATI IN INGRESSO' as DESCRIZIONE
  FROM DUAL) B
ON (A.ID_MACRO = B.ID_MACRO)
WHEN NOT MATCHED THEN 
INSERT (
  ID_MACRO, DESCRIZIONE)
VALUES (
  B.ID_MACRO, B.DESCRIZIONE)
WHEN MATCHED THEN
UPDATE SET 
  A.DESCRIZIONE = B.DESCRIZIONE;

COMMIT;
