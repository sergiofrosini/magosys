Prompt SCHEDULER JOB STORICO_REGISTRO_INVIO;

BEGIN
  DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'STORICO_REGISTRO_INVIO');
  Exception when others then
     null;
END;
/




BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'STORICO_REGISTRO_INVIO'
      ,start_date      => TO_TIMESTAMP_TZ('2016/10/07 23:30:00.000000 +02:00','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'STORED_PROCEDURE'
      ,job_action      => 'PKG_TRANSFER_RES.SP_STORICO_REGISTRO_INVIO'
      ,comments        => 'Esegue la storicizzazione delle procedure in scambio dati ST che hanno tot giorni'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'MAX_RUNS');
  BEGIN
    SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
      ( name      => 'STORICO_REGISTRO_INVIO'
       ,attribute => 'STOP_ON_WINDOW_CLOSE'
       ,value     => FALSE);
  EXCEPTION
    -- could fail if program is of type EXECUTABLE...
    WHEN OTHERS THEN
      NULL;
  END;
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'STORICO_REGISTRO_INVIO'
     ,attribute => 'AUTO_DROP'
     ,value     => TRUE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'STORICO_REGISTRO_INVIO');
    
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;

/