PROMPT JOB: MAGO_SPV_LOAD_RETE_ELE

BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB (job_name  => 'MAGO_SPV_LOAD_RETE_ELE');
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -27475 THEN NULL; -- ORA-00942: JOB non PRESENTE
                                         ELSE RAISE;
                     END IF;
END;
/	  

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'MAGO_SPV_LOAD_RETE_ELE'
      ,start_date      => TO_TIMESTAMP_TZ('2017/03/09 00:05:00.000000 Europe/Berlin','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;INTERVAL=1;BYHOUR=3'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'STORED_PROCEDURE'
      ,job_action      => 'pkg_spv_rete_ele.Elespvmain'
      ,comments        => 'Recupera le misure necessarie per il calcolo della Supervisione del modulo Smile GME'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 5);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO_SPV_LOAD_RETE_ELE'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'MAGO_SPV_LOAD_RETE_ELE');
END;
/
