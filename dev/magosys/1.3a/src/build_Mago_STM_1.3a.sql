spool rel_13a_STM.log
conn mago/mago@arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT Tabelle
PROMPT

@./Tables/GTTD_VALORI_TEMP.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione Viste
PROMPT

@./Views/V_ESERCIZI.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione Packages
PROMPT

@./Packages/PKG_REPORTS.sql;

PROMPT _______________________________________________________________________________________
PROMPT Definizione PackageBodies
PROMPT

@./PackageBodies/PKG_REPORTS.sql;
@./PackageBodies/PKG_ANAGRAFICHE.sql;

PROMPT _______________________________________________________________________________________
PROMPT Update Tables
PROMPT

@./UpdateTables/TIPI_ELEMENTO.sql;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
PROMPT

EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT fine Upgrade MAGO 1.3.a
PROMPT

spool off

