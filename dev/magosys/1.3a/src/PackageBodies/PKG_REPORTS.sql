--------------------------------------------------------
--  File creato - gioved�-settembre-26-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Package Body PKG_REPORTS
--------------------------------------------------------

  CREATE OR REPLACE PACKAGE BODY PKG_REPORTS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.h
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

-- ----------------
-- gi� presenti su PKG_REPORTS
--
--
    
     gcConsi             CONSTANT NUMBER(1) := 1;
     gcStima             CONSTANT NUMBER(1) := 2;
    
     gcFatt_kW          CONSTANT NUMBER := 1 / 1000;
     --gcFatt_kVA         CONSTANT NUMBER := 1 / 0.9 / 1000;
     gcFatt_MVh         NUMBER;
    
     gcRepBody          CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'RepBody';
    
     gOraDesc           VARCHAR2(10);
     gDeltaTime         NUMBER;
     gDataDa            DATE;
     gDataA             DATE;


-- ----------------
-- da aggiungere a PKG_REPORTS
--
--
 gTipRetA           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetM           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetB           TIPI_RETE.COD_TIPO_RETE%TYPE;


 gTipProdA         TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE;
 gTipProdB         TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE;

 gTipeleFiglio      ELEMENTI.COD_TIPO_ELEMENTO%TYPE;

 gOrganizzazione    NUMBER(1);
 gStatoRete         NUMBER(1);
 
 SCEGLIELEMENTO      INTEGER := 1;
 SCEGLIFONTE         INTEGER := 2;
 
 
 gTipEleMacroarea  CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'MAR';
 gTipEleDirez      CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'DTR';

 gCodPerRigaTotali CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'x';

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */
-- ----------------
-- gi� presenti su PKG_REPORTS
--
--
-- ----------------------------------------------------------------------------------------------------------

     PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
    /*-----------------------------------------------------------------------------------------------------------
        UTILITA' - dbms_output
    -----------------------------------------------------------------------------------------------------------*/
     BEGIN
        IF pLine THEN
            DBMS_OUTPUT.PUT_LINE(pTxt);
        ELSE
            DBMS_OUTPUT.PUT(pTxt);
        END IF;
     END PRINT;
    
    -- ----------------------------------------------------------------------------------------------------------
    
     FUNCTION DateDiff(pMask  IN VARCHAR2,
                       pD1    IN DATE,
                       pD2    IN DATE ) RETURN NUMBER AS
    /*-----------------------------------------------------------------------------------------------------------
        restituisce la differenza tra due date in ore o minuti
    -----------------------------------------------------------------------------------------------------------*/
         vRes    NUMBER;
     BEGIN
        SELECT (pD2 - pD1) * CASE UPPER(pMask)
                               WHEN 'MI' THEN 24*60
                               WHEN 'HH' THEN 24
                             END
          INTO vRes
          FROM dual;
        RETURN vRes;
     END;
    
-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */
-- ----------------
-- da aggiungere a PKG_REPORTS
--
--
-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisStimaFnte          (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pCodRaggrFonte  IN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per l'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
    dumdum      TIPO_FONTI.COD_TIPO_FONTE%TYPE;
 BEGIN
 
   SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT = pFlagTab
       AND COD_ELEMENTO        = NVL(pCodElemento,COD_ELEMENTO)
       AND COD_RAGGR_FONTE     = NVL(pCodRaggrFonte,COD_RAGGR_FONTE)
       AND COD_TIPO_MISURA     = pTipMisura
       AND COD_TIPO_RETE       = NVL(pTipRete,COD_TIPO_RETE)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN 0;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisStimaFnte;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisStima              (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipPrd2        IN TIPI_PRODUTTORE.COD_TIPO_PRODUTTORE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per l'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
 BEGIN
    SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT          = pFlagTab
       AND COD_ELEMENTO         = NVL(pCodElemento,COD_ELEMENTO)
       AND COD_TIPO_MISURA      = pTipMisura
       AND COD_TIPO_RETE        = NVL(pTipRete,COD_TIPO_RETE)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN NULL;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisStima;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitVarGlob     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pDataDa         IN DATE,
                            pDataA          IN DATE,
                            pOrganizzazione IN NUMBER,
                            pStatoRete      IN NUMBER,
                            pTipologiaRete  IN VARCHAR2,
                            pTipologiaProd  IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vOre            NUMBER;
    vMin            NUMBER;

    vFlgNull        INTEGER;

    vTmp            PKG_UtlGlb.t_SplitTbl;

    TYPE t_Fonti    IS TABLE OF RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE;
    vFonti          t_Fonti;

 BEGIN


    gOrganizzazione := pOrganizzazione;
    gStatoRete      := pStatoRete;


    gTipRetA := NULL;
    gTipRetM := NULL;
    gTipRetB := NULL;
    vTmp := PKG_UtlGlb.SplitString(pTipologiaRete,PKG_Mago.cSepCharLst);
    
    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcTipReteAT THEN
            gTipRetA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteMT THEN
            gTipRetM := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteBT THEN
            gTipRetB := vTmp(i);
        END IF;
    END LOOP;

    gTipProdA := NULL;
    gTipProdB := NULL;
    vTmp := PKG_UtlGlb.SplitString(pTipologiaProd,PKG_Mago.cSepCharLst);
    
    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcProduttorePuro THEN
            gTipProdA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcProduttoreCliente THEN
            gTipProdB := vTmp(i);
        END IF;
    END LOOP;




    IF pDataDa <> pDataA THEN
        gDeltaTime := DateDiff('HH', pDataDa, pDataA);
        vOre := TRUNC(gDeltaTime);
        vMin := (gDeltaTime - vOre) * 60;
        gcFatt_MVh := 1 / gDeltaTime / 1000000;
    ELSE
        gDeltaTime := 0;
        vOre := 0;
        vMin := 0;
        gcFatt_MVh := 1;
    END IF;

    gOraDesc   := TO_CHAR(vOre)||':'||TO_CHAR(vMin,'fm09');
 
    
    IF pDataDa <= pDataA THEN
      gDataDa    := pDataDa;
      gDataA     := pDataA + ((1/96) - (1/86400));
    ELSE
      gDataDa    := pDataA;
      gDataA     := pDataDa + ((1/96) - (1/86400));
    END IF;


    -- sf4 modifica 7-5-13 sulle date forzare giornate complete
    gDataDa := TO_DATE(TO_CHAR(gDataDa, 'DD-MM-YYYY') || ' 00:00:00','DD-MM-YYYY HH24:MI:SS');
    gDataA := TO_DATE(TO_CHAR(gDataA, 'DD-MM-YYYY') || ' 23:59:59','DD-MM-YYYY HH24:MI:SS');


    OPEN pRefCurs FOR SELECT COUNT(*) fake FROM DUAL; -- cursore fasullo ma necessario al FE
    
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.InitVarGlob',
                                     pDataRif      => pDataDa,
                                     pDataRif_fine => pDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Org:'||NVL(pOrganizzazione,'<null>')||'-'||
                                                      'Sta:'||NVL(pStatoRete,'<null>')||'-'||
                                                      'Ret:'||NVL(pTipologiaRete,'<null>')--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END InitVarGlob;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ScendiNellAlbero( pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE) AS

-- ----------------------------------------------------------------------------------------------------------
   vLog            PKG_Logs.t_StandardLog;
    vCod            ELEMENTI.COD_ELEMENTO%TYPE;
    vGst            ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNom            ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vTip            TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vGstCP          ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNomCP          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

BEGIN
   DBMS_OUTPUT.PUT_LINE(' ScendiNellAlbero '||pElementoPadre);

      vGstCP := '';
      vNomCP := '';

      FOR vRiga IN 
        --(SELECT  cod_elemento, T.cod_tipo_elemento, E.cod_gest_elemento, d.nome_elemento, T.sequenza FROM TABLE(PKG_ELEMENTI.LEGGIGERARCHIAInf(pElementoPadre,gOrganizzazione, gStatoRete,gDataDa)) T
        --   JOIN (SELECT * FROM ELEMENTI ) E USING (cod_elemento)  
        --   --JOIN (SELECT * FROM ELEMENTI WHERE cod_tipo_elemento = gTipEleFiglio) E USING (cod_elemento)  
        --   --JOIN (SELECT * FROM ELEMENTI WHERE cod_tipo_elemento IN ( gTipEleFiglio,PKG_MAGO.gcCabinaPrimaria)  ) E USING (cod_elemento)  
        --   JOIN (SELECT * FROM ELEMENTI_DEF) d USING (cod_elemento) ORDER BY sequenza
        -- )
        (SELECT cod_elemento, T.cod_tipo_elemento, E.cod_gest_elemento, d.nome_elemento, T.sequenza 
           FROM TABLE(PKG_ELEMENTI.LEGGIGERARCHIAInf(pElementoPadre,gOrganizzazione, gStatoRete,gDataDa)) T
           JOIN (SELECT * FROM ELEMENTI ) E USING (cod_elemento)
           --JOIN (SELECT * FROM ELEMENTI WHERE cod_tipo_elemento = gTipEleFiglio) E USING (COD_ELEMENTO)
           --JOIN (SELECT * FROM ELEMENTI WHERE cod_tipo_elemento IN ( gTipEleFiglio,PKG_MAGO.gcCabinaPrimaria)  ) E USING (COD_ELEMENTO)
           JOIN (SELECT * FROM ELEMENTI_DEF WHERE gDataDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) d USING (COD_ELEMENTO) 
      --     JOIN (SELECT COD_TIPO_ELEMENTO FROM TIPI_ELEMENTO WHERE COD_TIPO_RETE IN ( gTipRetA,gTipRetM,gTipRetB )  ) P USING (COD_TIPO_ELEMENTO)
          ORDER BY SEQUENZA
        )

      LOOP
  
        vCod := vRiga.COD_ELEMENTO; 
        vGst := vRiga.COD_GEST_ELEMENTO; 
        vNom := vRiga.NOME_ELEMENTO; 

        IF vRiga.COD_TIPO_ELEMENTO = PKG_MAGO.gcCabinaPrimaria THEN
          vGstCP := vRiga.COD_GEST_ELEMENTO; 
          vNomCP := vRiga.NOME_ELEMENTO; 
        END IF;

        IF vRiga.COD_TIPO_ELEMENTO = gTipEleFiglio THEN
         INSERT INTO GTTD_VALORI_TEMP(TIP,NUM1,ALF1,ALF2,ALF3,ALF4,ALF5)
                                VALUES(gcRepBody,
                                       vCod,
                                       vGst, --SUBSTR(vGst,INSTR(vGst,PKG_Mago.cSeparatore)+1),
                                       gTipEleFiglio,
                                       vNom,
                                       vGstCP,
                                       vnomCP);
                                     
      --    DBMS_OUTPUT.PUT_LINE(' insert GTTD_VALORI_TEMP '||vCod||' - '||vgst);
        END IF;
        
    END LOOP;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.ScendiNellAlbero',
                                     pDataRif      => gDataDa,
                                     pDataRif_fine => gDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Padre:'||pElementoPadre||'-'||
                                                      'Figlio:'||vCod--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END ScendiNellAlbero;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitGerarchia     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipEleFiglio   IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;
    vCod            ELEMENTI.COD_ELEMENTO%TYPE;
    vGst            ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vFlg            INTEGER;
    vFlg2           INTEGER;
    vNom            ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vTip            TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTSt            TIPI_ELEMENTO.CODIFICA_ST%TYPE;
    vOre            NUMBER;
    vMin            NUMBER;

    vFlgNull        INTEGER;

    vElemento       ELEMENTI.COD_ELEMENTO%TYPE := pElementoPadre;
    vGestionale     ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome           ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

    vTmp            PKG_UtlGlb.t_SplitTbl;

    TYPE t_Fonti    IS TABLE OF RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE;
    vFonti          t_Fonti;

 BEGIN

        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                    -- pLogArea    => vLog
                                     );
        PKG_Logs.StdLogAddTxt('log init '||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
        PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);


    OPEN pRefCurs FOR SELECT COUNT(*) fake FROM DUAL; -- cursore fasullo ma necessario al FE

    EXECUTE IMMEDIATE 'TRUNCATE TABLE GTTD_VALORI_TEMP';


    IF NVL(pElementoFiglio,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
        IF NVL(vElemento,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
            PKG_Elementi.GetDefaultCO (vElemento, vGestionale, vNome);
        END IF;
    END IF;
    
   DBMS_OUTPUT.PUT_LINE(' x elencoxxx '||pTipEleFiglio||' - '||vElemento);


    gTipeleFiglio := pTipEleFiglio;

    ScendiNellAlbero(vElemento);
    
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.InitRepGerarchia',
                                     pDataRif      => gDataDa,
                                     pDataRif_fine => gDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Padre:'||pElementoPadre||'-'||
                                                      'Figlio:'||pElementoFiglio--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END InitGerarchia;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitRepStimaConsi     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                  pDataDa         IN DATE,
                                  pDataA          IN DATE,
                                  pOrganizzazione IN NUMBER,
                                  pStatoRete      IN NUMBER,
                                  pTipologiaRete  IN VARCHAR2,
                                  pTipologiaProd  IN VARCHAR2,
                                  pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pTipEleFiglio   IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/

 BEGIN

   InitVarGlob     (pRefCurs,pDataDa, pDataA, pOrganizzazione, pStatoRete, pTipologiaRete, pTipologiaProd);
   InitGerarchia   (pRefCurs, pElementoPadre, pElementoFiglio, pTipEleFiglio);

 END InitRepStimaConsi;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
 BEGIN
 
    DELETE FROM GTTD_REP_ENERGIA_POTENZA WHERE TIPO_REPORT IN (gcStima,gcConsi);
    
    INSERT INTO GTTD_REP_ENERGIA_POTENZA (TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO,VALORE)
              WITH mieTR AS (SELECT * FROM TRATTAMENTO_ELEMENTI
                             WHERE COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcPotenzaMeteoPrevisto/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND ORGANIZZAZIONE = gOrganizzazione
                                 --AND COD_TIPO_RETE IN ( gTipRetA,gTipRetM,gTipRetB )            
                                 AND COD_TIPO_PRODUTTORE IN (gTipProdA, gTipProdB))             
              SELECT TIPO_REPORT, COD_ELEMENTO, COD_RAGGR_FONTE, COD_TIPO_RETE, COD_TIPO_PRODUTTORE, COD_TIPO_MISURA, SEGNO, SUM(VALORE) VALORE
                FROM (SELECT gcStima TIPO_REPORT ,NUM1 COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,
                             CASE
                                WHEN VALORE < 0 THEN '-'
                                ELSE                 '+'
                             END SEGNO,
                             ABS(VALORE) VALORE
                        FROM (SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE mis
                               INNER JOIN mieTR tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE mis
                               INNER JOIN mieTR tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                            WHEN 1 THEN gStatoRete --PKG_Mago.gcStatoNormale
                                                            ELSE gStatoRete 
                                                         END
                              UNION ALL
                             SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE_STATICHE mis
                               INNER JOIN mieTR tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               WHERE (   gdataDa BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                      OR gdataA  BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE)
                                AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE_STATICHE mis
                               INNER JOIN mieTR tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE (   gdataDa BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                      OR gdataA  BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE)
                                AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                            WHEN 1 THEN gStatoRete --PKG_Mago.gcStatoNormale
                                                            ELSE gStatoRete 
                                                         END
                             )
                       RIGHT OUTER JOIN GTTD_VALORI_TEMP  ON NUM1 = COD_ELEMENTO
                     )
               GROUP BY TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO;


    OPEN pRefCurs FOR
        SELECT  COD_ELEMENTO,
                COD_GEST_ELEMENTO,
                COD_TIPO_ELEMENTO,
                NOME_ELEMENTO,
                COD_GEST_CP,
                NOME_CP,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT 
           NUM1 COD_ELEMENTO,
           ALF1 COD_GEST_ELEMENTO,
           ALF2 COD_TIPO_ELEMENTO,
           ALF3 NOME_ELEMENTO,
           ALF4 COD_GEST_CP,
           ALF5 NOME_CP,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_1,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_1,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_2,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_2,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_3,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_3,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteTermica,  PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_4,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteTermica,  PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_4,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_5,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_5,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteConvenz,  PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_6,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteConvenz,  PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_MVh E_gen_6,
           0  P_contr0
                  FROM GTTD_VALORI_TEMP
                 WHERE TIP = gcRepBody
               )
        -- bug 797 ordinamento numeri/lettere
        ORDER BY NLSSORT(NOME_CP,'NLS_SORT=BINARY'), NLSSORT(NOME_ELEMENTO,'NLS_SORT=BINARY');
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                       pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
                       ID_RAGGR_FONTE,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_1,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_1,
                         
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_2,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_3,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_3,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_4,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_4,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_5,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_5,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN 
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_6,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN 
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         ELSE 0 /*NULL*/ END  E_gen_6,

                       COD_RAGGR_FONTE  P_contr0
                  FROM RAGGRUPPAMENTO_FONTI
                ) ORDER BY ID_RAGGR_FONTE;
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaFooter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaTot (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT  gCodPerRigaTotali COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT 
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteSolare, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_1,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteSolare,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_1,
                         
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteEolica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_2,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteEolica,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_2,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_3,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteIdraulica,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_3,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteTermica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_4,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteTermica,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_4,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_5,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteRinnovab,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_5,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteConvenz, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_6,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteConvenz,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)      * gcFatt_MVh 
                         E_gen_6,

                       gCodPerRigaTotali P_contr0
                  FROM dual);
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaTot;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsiBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
 BEGIN
 
    DELETE FROM GTTD_REP_ENERGIA_POTENZA WHERE TIPO_REPORT IN (gcConsi,gcConsi);
    
    INSERT INTO GTTD_REP_ENERGIA_POTENZA (TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO,VALORE)
              SELECT TIPO_REPORT, COD_ELEMENTO, COD_RAGGR_FONTE, COD_TIPO_RETE, COD_TIPO_PRODUTTORE, COD_TIPO_MISURA, SEGNO, SUM(VALORE) VALORE
                FROM (SELECT gcConsi TIPO_REPORT ,NUM1 COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,
                             CASE
                                WHEN VALORE < 0 THEN '-'
                                ELSE                 '+'
                             END SEGNO,
                             
                             ABS(VALORE) VALORE
                        FROM (SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcPotenzaMeteoPrevisto/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE mis.DATA BETWEEN gDataDa AND gDataA
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcPotenzaMeteoPrevisto/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                            WHEN 1 THEN gStatoRete --PKG_Mago.gcStatoNormale
                                                            ELSE gStatoRete 
                                                         END
                              UNION ALL
                             SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_ACQUISITE_STATICHE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               WHERE (   gdataDa BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                      OR gdataA  BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE)
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcPotenzaMeteoPrevisto/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                              UNION ALL
                              SELECT tr.COD_ELEMENTO,tr.COD_TIPO_MISURA,tpf.COD_RAGGR_FONTE,tr.COD_TIPO_RETE,tr.COD_TIPO_PRODUTTORE,mis.VALORE
                                FROM MISURE_AGGREGATE_STATICHE mis
                               INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM)
                               INNER JOIN TIPO_FONTI tpf USING (COD_TIPO_FONTE )
                               INNER JOIN GTTD_VALORI_TEMP GTTD ON GTTD.NUM1 = tr.COD_ELEMENTO
                               INNER JOIN TIPI_ELEMENTO tpel USING (COD_TIPO_ELEMENTO)
                               WHERE (   gdataDa BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE
                                      OR gdataA  BETWEEN mis.DATA_ATTIVAZIONE AND mis.DATA_DISATTIVAZIONE)
                                 AND tr.COD_TIPO_MISURA IN (PKG_Mago.gcNumeroImpianti,PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcPotenzaMeteoPrevisto/*,PKG_Mago.gcPotenzaContrattuale*/)
                                 AND tr.ORGANIZZAZIONE = gOrganizzazione
                                 AND tr.TIPO_AGGREGAZIONE = CASE tpel.GER_ECS
                                                            WHEN 1 THEN gStatoRete --PKG_Mago.gcStatoNormale
                                                            ELSE gStatoRete 
                                                         END
                             )
                       RIGHT OUTER JOIN GTTD_VALORI_TEMP ON NUM1 = COD_ELEMENTO
                     )
               GROUP BY TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO;


    OPEN pRefCurs FOR
        SELECT  COD_ELEMENTO,
                COD_GEST_ELEMENTO,
                COD_TIPO_ELEMENTO,
                NOME_ELEMENTO,
                COD_GEST_CP,
                NOME_CP,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, 
                P_prev_2,
                P_prev_3,
                P_prev_4,
                P_prev_5, 
                P_prev_6, 
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT 
           NUM1 COD_ELEMENTO,
           ALF1 COD_GEST_ELEMENTO,
           ALF2 COD_TIPO_ELEMENTO,
           ALF3 NOME_ELEMENTO,
           ALF4 COD_GEST_CP,
           ALF5 NOME_CP,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttoreCliente, NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_1,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_2,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_3,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteTermica,  PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_Prev_4,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_5,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteConvenz,  PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcProduttorePuro,     PKG_Mago.gcProduttoreCliente, NULL)                * gcFatt_kW  P_prev_6,
           0  P_contr0
                  FROM GTTD_VALORI_TEMP
                 WHERE TIP = gcRepBody
               )
        -- bug 797 ordinamento numeri/lettere
        ORDER BY NLSSORT(NOME_CP,'NLS_SORT=BINARY'), NLSSORT(NOME_ELEMENTO,'NLS_SORT=BINARY');
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsiFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, 
                P_prev_2, 
                P_prev_3,
                P_prev_4, 
                P_prev_5,
                P_prev_6, 
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
                       ID_RAGGR_FONTE,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_1,
                          
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_3,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_4,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_5,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN 
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         ELSE 0 /*NULL*/ END  P_prev_6,

                       COD_RAGGR_FONTE  P_contr0
                  FROM RAGGRUPPAMENTO_FONTI
                ) ORDER BY ID_RAGGR_FONTE;
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiFooter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsitot (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT  gCodPerRigaTotali COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, 
                P_prev_2, 
                P_prev_3,
                P_prev_4, 
                P_prev_5,
                P_prev_6, 
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT 
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttorePuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcProduttoreCliente,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteSolare, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_1,
                          
                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteEolica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_2,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_3,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteTermica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                         P_prev_4,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                          P_prev_5,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteConvenz, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcProduttorePuro,PKG_Mago.gcProduttoreCliente, NULL)    * gcFatt_kW 
                          P_prev_6,

                       gCodPerRigaTotali P_contr0 FROM DUAL)
                  ;
 
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiTot;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetAggregDaPerim(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona i tipi elemento presenti in gerarchia come discendenti di un elemento specificato
-----------------------------------------------------------------------------------------------------------*/
   vLog            PKG_Logs.t_StandardLog;
   
BEGIN  

 OPEN pRefCurs FOR 
          WITH tipiGer AS (SELECT DISTINCT cod_tipo_elemento FROM 
                          TABLE(PKG_ELEMENTI.LEGGIGERARCHIAInf(pElementoPadre,gOrganizzazione, gStatoRete,gDataDa))
                          --- SF4 correzione 7-5-13 escludo i generatori
                          WHERE cod_tipo_elemento NOT IN (
                            PKG_MAGO.gcGeneratoreAT,
                            PKG_MAGO.gcGeneratoreMT, 
                            PKG_MAGO.gcGeneratoreBT
                          ))
          SELECT cod_tipo_elemento, descrizione FROM 
          (SELECT cod_tipo_elemento, E.descrizione FROM
          tipiGer T JOIN (select * from TIPI_ELEMENTO 
                          --- bug 760 SF4 correzione 16-9-13 escludo i tipi nascosti
                         WHERE NASCOSTO = PKG_UtlGlb.gkFlagOFF) E
          USING (cod_tipo_elemento))
          ORDER BY descrizione;
          
 RETURN;
          
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetAggregDaPerim',
                                     pCodice       => 'Padre:'||pElementoPadre
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
         
END GetAggregDaPerim;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDatiPerimHeader(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElePerim IN ELEMENTI.COD_ELEMENTO%TYPE) AS

/*-----------------------------------------------------------------------------------------------------------
    seleziona i tipi elemento presenti in gerarchia come discendenti di un elemento specificato
-----------------------------------------------------------------------------------------------------------*/
   vLog       PKG_Logs.t_StandardLog;
   vEleEse    ELEMENTI.COD_ELEMENTO%TYPE;
   vEleGEse    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
BEGIN  
  
  SELECT G.COD_ELEMENTO, E.COD_GEST_ELEMENTO
    INTO vEleEse,vEleGEse
    FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaSup(pElePerim,gOrganizzazione,gStatoRete,gDataDa)) G
   INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = G.COD_ELEMENTO
                        AND E.COD_TIPO_ELEMENTO = PKG_Mago.gcEsercizio;
  
 /* --- caso per prove */
  
--  OPEN pRefCurs FOR 
--    SELECT 'DANO' pEleMa, 'DF00 ' pEleDTR,   /*'DF70'*/vEleGEse pEleCO
--    FROM DUAL;
    
/* --- caso corretto da elaborare quando ci sar� la vista */

  OPEN pRefCurs FOR 
    SELECT ACR_MA pEleMa, GST_UTR pEleDTR, ACR_ESE pEleCO 
    FROM V_ESERCIZI
    WHERE GST_ESE = vEleGEse;
 
 RETURN;
          
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetDatiPerimHeader',
                                     pCodice       => 'Elemento:'||pElePerim
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
         
END GetDatiPerimHeader;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_REPORTS;

/



