set serveroutput on;

declare

procedure insUna(pCOD_GRUPPO        in gruppi_misura.cod_gruppo%type,
                pCOD_TIPO_ELEMENTO  in gruppi_misura.COD_TIPO_ELEMENTO%type,
                pCOD_TIPO_MISURA    in gruppi_misura.COD_TIPO_MISURA%type,
                pseq_ord            in gruppi_misura.seq_ord%type,
                pflag_attivo IN NUMBER) is   
  
  begin

    if (pCOD_GRUPPO != 1) OR (pCOD_TIPO_MISURA NOT IN ('PCT-Q','CC-Q','PPRGP'/*CANCELLATE NELLA VERSIONE 1.6.a.4 *//*,'SPAP','SPRP','PAGCTI'*/)) THEN

     MERGE INTO gruppi_misura t
     USING ( 
            SELECT pCOD_GRUPPO cod_gruppo , pCOD_TIPO_ELEMENTO cod_tipo_elemento , pCOD_TIPO_MISURA cod_tipo_misura
                   ,pflag_attivo flag_attivo , pseq_ord seq_ord, 0 disattivato
              FROM DUAL 
            ) s
        ON (t.cod_gruppo =s.cod_gruppo AND t.cod_tipo_elemento = s.cod_tipo_elemento AND s.cod_tipo_misura = t.cod_tipo_misura)
      WHEN MATCHED THEN
    UPDATE SET 
               t.flag_attivo = s.flag_Attivo
              ,t.seq_ord = s.seq_ord
              ,t.disattivato = s.disattivato
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_gruppo,cod_tipo_elemento,cod_tipo_misura,flag_attivo,seq_ord,disattivato) 
    VALUES (s.cod_gruppo, s.cod_tipo_elemento, s.cod_tipo_misura, s.flag_attivo, s.seq_ord, s.disattivato);


    END IF;

 end;
 
 
 
begin


  for vRiga in 
    (select cod_gruppo, cod_tipo_elemento, max(seq_ord) m from gruppi_misura
    group by cod_gruppo, cod_tipo_elemento)
  loop
  
  dbms_output.put_line(vRiga.cod_gruppo||' '||vRiga.cod_tipo_elemento||' '||vRiga.m);

    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PPRGP',6,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);

IF vRiga.cod_tipo_elemento != 'GMT' THEN  
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PCT-P',vRiga.m+1,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PCT-Q',vRiga.m+2,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'CC-P',vRiga.m+3,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'CC-Q',vRiga.m+4,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
END IF;

/*CANCELLATE NELLA 1.6.a.4*/
--IF vRiga.cod_tipo_elemento in ('TRM','CMT','CBT') THEN  
    --insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PAAS',vRiga.m+5,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
    --insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PAGS',vRiga.m+6,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
--END IF;

--1.5
    --insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PMP.A1',vRiga.m+7,0);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PMP.P1',vRiga.m+8,0);
    --insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PMC.A1',vRiga.m+9,0);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PMC.P1',vRiga.m+9,0);

--1.6
/*CANCELLATE NELLA 1.6.a.4*/
--    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'SPAP',vRiga.m+11,1);
--    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'SPRP',vRiga.m+12,1);
--    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PAGCTI',vRiga.m+14,1);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PPRGP',vRiga.m+13,0);
            

  end loop;

--1.6
/*CANCELLATE NELLA 1.7.a.3*/
delete from gruppi_misura
 where cod_tipo_misura in ( 'PMP.A1' , 'PMP.P1')
  and cod_tipo_elemento in ('GAT','GMT','GBT');

end;
/

commit;