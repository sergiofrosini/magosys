PROMPT INSERIMENTO VALORI (invio per accettare il default)

ACCEPT wsdl_host  CHAR  FORMAT a64 DEFAULT 'localhost' PROMPT 'inserire HOST per MPS.estimation.ws.wsdl.url (localhost): '
ACCEPT wsdl_port  CHAR  FORMAT a64 DEFAULT '80' PROMPT 'inserire PORTA per MPS.estimation.ws.wsdl.url (80): '

ACCEPT name1 	  CHAR  FORMAT a16  DEFAULT 'Ilmeteo'         PROMPT 'inserire nome convenzionale supplier1 (Ilmeteo): '
ACCEPT geotype1 CHAR  FORMAT a5  DEFAULT 'c'         PROMPT 'inserire geotype supplier1 in [A,P,C] (C): '
ACCEPT ftp_yesno1 CHAR  FORMAT a5  DEFAULT 'true'         PROMPT 'inserire true o false per supplier1 use.ftp for download(true): '
ACCEPT dsftp_yesno1  CHAR  FORMAT a5  DEFAULT 'false' PROMPT 'inserire true o false per supplier1 use.sftp for download(false): '
ACCEPT usftp_yesno1  CHAR  FORMAT a5  DEFAULT 'false' PROMPT 'inserire true o false per supplier1 use.sftp for upload(false): '
ACCEPT src_host1  CHAR  FORMAT a16 DEFAULT 'pkg_ARCDB2'   PROMPT 'inserire IP supplier1 ftp.source.host1(pkg_ARCDB2): '
ACCEPT ftpport1 CHAR  FORMAT a5  DEFAULT '21'  PROMPT 'inserire porta di ftp.source.host(21): '
ACCEPT usrftp1    CHAR  FORMAT a16 DEFAULT 'magosys'      PROMPT 'inserire username supplier1 per ftp(magosys): '
ACCEPT paswwd1    CHAR  FORMAT a16 DEFAULT 'Magosys'      PROMPT 'inserire passwd supplier1 per ftp(Magosys): '
ACCEPT src_path1  CHAR  FORMAT a64 DEFAULT '/usr/NEW/magosys/meteofile/ilmeteo' PROMPT 'inserire path ftp.source.path.1(/usr/NEW/magosys/meteofile/ilmeteo): '
ACCEPT forecast1  CHAR  FORMAT a64 DEFAULT 'PMP'  PROMPT 'inserire forecast curve name supplier1 (PMP): '
ACCEPT forecast_cs1  CHAR  FORMAT a64 DEFAULT 'PMC'  PROMPT 'inserire forecast clearsky curve name supplier1 (PMC): '

/*
Commentata parte su fornitore 2 in quanto NON DISPONIBILE
ACCEPT name2 	  CHAR  FORMAT a16  DEFAULT 'Flyby'         PROMPT 'inserire nome convenzionale supplier2 (Flyby): '
ACCEPT geotype2 CHAR  FORMAT a5  DEFAULT 'P'         PROMPT 'inserire geotype supplier2 in [A,P,C] (P): '
ACCEPT ftp_yesno2 CHAR  FORMAT a5  DEFAULT 'true'         PROMPT 'inserire true o false per supplier2 use.ftp for download(true): '
ACCEPT dsftp_yesno2  CHAR  FORMAT a5  DEFAULT 'false' PROMPT 'inserire true o false per supplier2 use.sftp for download(false): '
ACCEPT usftp_yesno2  CHAR  FORMAT a5  DEFAULT 'false' PROMPT 'inserire true o false per supplier2 use.sftp for upload(false): '
ACCEPT src_host2  CHAR  FORMAT a16 DEFAULT 'pkg_ARCDB2'   PROMPT 'inserire IP supplier2 ftp.source.host(pkg_ARCDB2): '
ACCEPT ftpport2 CHAR  FORMAT a5  DEFAULT '21'  PROMPT 'inserire porta di ftp.source.host2(21): '
ACCEPT usrftp2    CHAR  FORMAT a16 DEFAULT 'magosys'      PROMPT 'inserire username supplier2 per ftp(magosys): '
ACCEPT paswwd2    CHAR  FORMAT a16 DEFAULT 'Magosys'      PROMPT 'inserire passwd supplier2 per ftp(Magosys): '
ACCEPT src_path2  CHAR  FORMAT a64 DEFAULT '/usr/NEW/magosys/meteofile/flyby' PROMPT 'inserire path ftp.source.path.2(/usr/NEW/magosys/meteofile/flyby): '
ACCEPT forecast2  CHAR  FORMAT a64 DEFAULT 'PMP.P1'  PROMPT 'inserire forecast curve name supplier1 (PMP.P1): '
ACCEPT forecast_cs2  CHAR  FORMAT a64 DEFAULT 'PMC.P1'  PROMPT 'inserire forecast clearsky curve name supplier1 (PMC.P1): '
*/
TRUNCATE TABLE METEO_JOB_STATIC_CONFIG;


INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.ftp.timeout.connection','300');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.ftp.timeout.read','300');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.ftp.timeout.close','300');
--ftp per trasferimento file meteo verso i locali
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.ftp.use.for.finaltrasfer', 'false'); 
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('AUI.remote.log.filename','Meteo_AAMM.log');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.suffix.folder.extraction','EX');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.suffix.folder.elaboration','EL');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.local.mantaining.day','30');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.local.download.retry','3');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.local.recovery.days', '7');
--INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.suffix.zip.file.extension','.zip');
--INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.suffix.xml.file.extension','.xml');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.forward.hour','72');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.sampling.minutes','60');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.enable.producer','true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.enable.transformator','true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.enable.generator','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.enable.clearsky','true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.enable.meteo.acquired','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.export.exportAtProducerLevel','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.export.exportAtGeneratorLevel','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.export.exportAtTransformerLevel','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.prediction.export.ftpRetryDelay','60000');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.prediction.start.hour.firstsuffix','20');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.prediction.start.hour.secondsuffix','20');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.prediction.days.to.save','4');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.prediction.start.day','0');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.types.network','M');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.types.producer','A');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.types.sources','S|E');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.ws.wsdl.url','http://&wsdl_host:&wsdl_port/STWebRegistry-WS/registry?wsdl'); 
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.measure.week.number','4');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.measure.interval.days','7');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.force','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.sampling.minutes','60');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.involvedMeasureType','PAS');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.enable.producer','true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.enable.transformator','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.estimation.enable.generator','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.types.network','A|M|B');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.types.producer','A|B|X|Z');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.types.sources','S|E|I|T|R|C');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.types.eolic.generator','CAT|CMT|CBT|TRF|TRM');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.thread.number','5');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.recovery.enabled', 'true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MDS.statistics.enabled', 'true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.enabled','true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.customer.enabled', 'true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.transformer.enabled', 'true');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.types.network','A|M|B');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.types.producer','A|B|X|Z|C');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.types.sources','S|E|I|T|R|C|3');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.stardard.forward.hour','24');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.install.forward.hour','72');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.load.forecast.external.measure.conversion.factor', '0.001'); --1.4.b.4

--Supplier ilmeteo
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.conventional.name.1','&name1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.download.useFtp.1','&ftp_yesno1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.download.useSftp.1','&dsftp_yesno1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.upload.useSftp.1','&usftp_yesno1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.port.1','&ftpport1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.host.1','&src_host1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.username.1','&usrftp1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.password.1','&paswwd1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.useProxy.1','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyHost.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyPort.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyUser.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyPass.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.folder.source.path.1','&src_path1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.local.destination.path.1','./tmp1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.remote.mantaining.day.1','30');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.enabled.1','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.forward.hour.1','48');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.host.1','127.0.0.1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.username.1','ftpuser');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.password.1','ftpuser');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.path.1','/1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.useProxy.1','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.host.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.port.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.username.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.password.1','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.daysToKeepZipFile.1','30');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.point.type.1','&geotype1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.forecast.curve.name.1','&forecast1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.forecast.clearsky.curve.name.1','&forecast_cs1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.extension.1','.zip');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.match.first.1','_01_xml');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.match.second.1','_00_xml');

--Supplier FlyBy
/*
Commentata parte su fornitore 2 in quanto NON DISPONIBILE
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.conventional.name.2','&name2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.download.useFtp.2','&ftp_yesno2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.download.useSftp.2','&dsftp_yesno2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.upload.useSftp.2','&usftp_yesno2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.port.2','&ftpport2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.host.2','&src_host2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.username.2','&usrftp2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.password.2','&paswwd2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.useProxy.2','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyHost.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyPort.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyUser.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.ftp.source.proxyPass.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.folder.source.path.2','&src_path2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.local.destination.path.2','./tmp2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.remote.mantaining.day.2','30');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.enabled.2','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.forward.hour.2','48');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.host.2','127.0.0.1');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.username.2','ftpuser');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.password.2','ftpuser');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.path.2','/2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.useProxy.2','false');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.host.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.port.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.username.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.ftp.proxy.password.2','');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MPS.supplier.prediction.export.daysToKeepZipFile.2','30');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.point.type.2','&geotype2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.forecast.curve.name.2','&forecast2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.forecast.clearsky.curve.name.2','&forecast_cs2');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.extension.2','.zip');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.match.first.2','_01_xml');
INSERT INTO METEO_JOB_STATIC_CONFIG VALUES ('MFM.supplier.suffix.zip.file.match.second.2','_00_xml');
*/

COMMIT;


