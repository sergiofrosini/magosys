
MERGE INTO measure_offline_parameters t
     USING ( 
            SELECT 'STWebRegistry-WS.measure.offline.max_gest_code_per_request' key ,'800' value
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.estimated_db_effort_perc','95'
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.max_concurrent_measure_offline_threads','20'
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.estimated_polling_interval_factor','0.1'
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.thread_timeout_factor','60000'
              FROM DUAL 
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.polling_interval_threshold' key , '60' value
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.purge_session_after_session_ok_closed' key , 'true' value
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.forecast_info_involved_measures','PMP|PMC'
              FROM DUAL
             UNION ALL
            SELECT 'STWebRegistry-WS.measure.offline.forecast_info_mds_serviceid','MDS-service'
              FROM DUAL
             UNION ALL
             SELECT 'STWebRegistry-WS.measure.offline.max_concurrent_csv_composer_threads','20'
              FROM DUAL
            ) s
        ON ( t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value)
    VALUES
          (s.key,s.value)
;
 
COMMIT;
