Prompt Table METEO_JOB_RUNTIME_CONFIG;

MERGE INTO meteo_job_runtime_config t
     USING 
          (
           SELECT 'LF-service' job_id , trunc(sysdate)-30 start_date, null end_date ,  '1' enabled, 1 force_install
             FROM DUAL
            UNION ALL             
           SELECT 'MDS-service' job_id , trunc(sysdate)-7 start_date, null end_date ,  '1' enabled, 0 force_install
             FROM DUAL
          ) s
        ON (t.job_id = s.job_id)
      WHEN MATCHED THEN
    UPDATE SET 
               t.start_date = s.start_date
              ,t.end_date = s.end_date
              ,t.enabled = s.enabled      
              ,t.force_install = s.force_install        
      WHEN NOT MATCHED THEN 
    INSERT   
          (job_id , start_date , end_date , enabled, force_install)
    VALUES
          (s.job_id , s.start_date , s.end_date , s.enabled, s.force_install)
; 

COMMIT;
