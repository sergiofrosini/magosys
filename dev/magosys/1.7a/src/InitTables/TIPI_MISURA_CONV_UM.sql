Prompt Insert Table TIPI_MISURA_CONV_UM;

MERGE INTO tipi_misura_conv_um t
     USING ( --CC-P 
            SELECT 'CC-P' cod_tipo_misura , 'W' tipo_misura_conv, 1 fattore_moltiplicativo, NULL formula
             FROM DUAL 
            UNION ALL
           SELECT 'CC-P','kW',1000,NULL
             FROM DUAL 
            UNION ALL
            --CC-Q
           SELECT 'CC-Q','VAr',1,NULL 
             FROM DUAL 
            UNION ALL
           SELECT 'CC-Q','kVAr',1000,null
             FROM DUAL 
            UNION ALL
            --PCT-P
           SELECT 'PCT-P','W',1,null
             FROM DUAL 
            UNION ALL
           SELECT 'PCT-P','kW',1000,null
             FROM DUAL 
            UNION ALL
            --PCT-Q
           SELECT 'PCT-Q','VAr',1,null
             FROM DUAL 
            UNION ALL
           SELECT 'PCT-Q','kVAr',1000,null
             FROM DUAL
            UNION ALL
           SELECT 'PAAS' cod_tipo_misura , 'kW' tipo_misura_conv, 1000 fattore_moltiplicativo, NULL formula
             FROM DUAL 
            UNION ALL
           SELECT 'PAAS','W',1,NULL 
             FROM DUAL 
            UNION ALL
            --PAGS
           SELECT 'PAGS','kW',1000,NULL 
             FROM DUAL 
            UNION ALL
           SELECT 'PAGS','W',1,NULL 
             FROM DUAL
            --PPRGP 
            UNION ALL
            SELECT 'PPRGP' cod_tipo_misura , 'kVAr' tipo_misura_conv, 1000 fattore_moltiplicativo, NULL formula
             FROM DUAL 
            UNION ALL
           SELECT 'PPRGP','VAr',1,NULL 
             FROM DUAL 
            --1.5.a.0
            UNION ALL
            SELECT 'PMP.A1' cod_tipo_misura, 'W' tipo_misura_conv, 1 fattore_moltiplicativo, NULL formula
              FROM dual
             UNION ALL
            SELECT 'PMP.A1', 'kW', 1000, NULL
              FROM dual
             UNION ALL
            SELECT 'PMC.A1', 'W', 1, NULL
              FROM dual
             UNION ALL
            SELECT 'PMC.A1', 'kW', 1000, NULL
              FROM dual
             UNION ALL
            SELECT 'PMP.P1', 'W', 1, NULL          
              FROM dual
             UNION ALL
            SELECT 'PMP.P1', 'kW', 1000, NULL        
              FROM dual
             UNION ALL
            SELECT 'PMC.P1', 'W', 1, NULL           
              FROM dual
             UNION ALL
            SELECT 'PMC.P1', 'kW', 1000, NULL
              FROM dual
            --1.6
             UNION ALL
            SELECT 'PAGCTI', 'kW', 1000, NULL
              FROM DUAL
             UNION ALL
            SELECT 'PAGCTI', 'W', 1, NULL
              FROM DUAL
             UNION ALL
            SELECT 'SPAP', 'kW', 1000, NULL
             FROM DUAL
            UNION ALL
           SELECT 'SPAP', 'W', 1, NULL
             FROM DUAL
            UNION ALL
           SELECT 'SPRP', 'kVAr', 1000, NULL
             FROM DUAL
            UNION ALL
           SELECT 'SPRP', 'VAr', 1, NULL
             FROM DUAL
          ) s
        ON (t.cod_tipo_misura =s.cod_tipo_misura AND t.tipo_misura_conv = s.tipo_misura_conv)
      WHEN MATCHED THEN
    UPDATE SET 
               t.fattore_moltiplicativo = s.fattore_moltiplicativo
              ,t.formula = s.formula
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_tipo_misura,tipo_misura_conv,fattore_moltiplicativo,formula) 
    VALUES (s.cod_tipo_misura, s.tipo_misura_conv, s.fattore_moltiplicativo, s.formula) 
;

          
COMMIT;
