--------------------------------------------------------
--  File creato - venerd�-agosto-02-2013   
--------------------------------------------------------
REM INSERTING into VERSION

PROMPT VERSION

MERGE INTO VERSION t
     USING
          ( SELECT sysdate data , '1' major , '4' minor , 'a' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+1/(24*60*60) data , '1' major , '4' minor , 'a' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+2/(24*60*60) data , '1' major , '4' minor , 'a' patch, 3 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+3/(24*60*60) data , '1' major , '4' minor , 'a' patch, 4 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+4/(24*60*60) data , '1' major , '4' minor , 'a' patch, 5 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+5/(24*60*60) data , '1' major , '4' minor , 'a' patch, 6 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+6/(24*60*60) data , '1' major , '4' minor , 'a' patch, 7 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+7/(24*60*60) data , '1' major , '4' minor , 'a' patch, 8 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+8/(24*60*60) data , '1' major , '4' minor , 'a' patch, 9 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+9/(24*60*60) data , '1' major , '4' minor , 'a' patch, 10 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+10/(24*60*60) data , '1' major , '4' minor , 'a' patch, 11 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+11/(24*60*60) data , '1' major , '4' minor , 'a' patch, 12 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+12/(24*60*60) data , '1' major , '4' minor , 'b' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+13/(24*60*60) data , '1' major , '4' minor , 'b' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+14/(24*60*60) data , '1' major , '4' minor , 'b' patch, 3 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+15/(24*60*60) data , '1' major , '4' minor , 'b' patch, 4 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+16/(24*60*60) data , '1' major , '5' minor , 'a' patch, 0 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+17/(24*60*60) data , '1' major , '5' minor , 'a' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+18/(24*60*60) data , '1' major , '5' minor , 'a' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+19/(24*60*60) data , '1' major , '5' minor , 'a' patch, 3 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+20/(24*60*60) data , '1' major , '6' minor , 'a' patch, 0 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+21/(24*60*60) data , '1' major , '6' minor , 'a' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+22/(24*60*60) data , '1' major , '6' minor , 'a' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+23/(24*60*60) data , '1' major , '6' minor , 'a' patch, 3 revision
              FROM DUAL              
             UNION ALL
            SELECT sysdate+24/(24*60*60) data , '1' major , '6' minor , 'a' patch, 4 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+25/(24*60*60) data , '1' major , '7' minor , 'a' patch, 0 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+26/(24*60*60) data , '1' major , '7' minor , 'a' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+27/(24*60*60) data , '1' major , '7' minor , 'a' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+28/(24*60*60) data , '1' major , '7' minor , 'a' patch, 3 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+29/(24*60*60) data , '1' major , '7' minor , 'a' patch, 4 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+30/(24*60*60) data , '1' major , '7' minor , 'a' patch, 5 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+31/(24*60*60) data , '1' major , '7' minor , 'a' patch, 6 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+32/(24*60*60) data , '1' major , '7' minor , 'a' patch, 7 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+33/(24*60*60) data , '1' major , '7' minor , 'a' patch, 8 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate+34/(24*60*60) data , '1' major , '7' minor , 'a' patch, 9 revision
              FROM DUAL
           ) s
        ON ( t.major = s.major AND t.minor = s.minor AND t.patch = s.patch AND t.revision = s.revision)
      WHEN MATCHED THEN 
    UPDATE SET
               t.data = s.data
      WHEN NOT MATCHED THEN
    INSERT (data, major, minor, patch, revision) 
    VALUES (s.data, s.major, s.minor, s.patch, s.revision) 
;

COMMIT;
