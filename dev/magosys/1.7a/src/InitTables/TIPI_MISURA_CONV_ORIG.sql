--------------------------------------------------------
--  File creato - venerd�-agosto-02-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TIPI_MISURA_CONV_ORIG
--------------------------------------------------------

REM INSERTING into TIPI_MISURA_CONV_ORIG

MERGE INTO tipi_misura_conv_orig t
     USING ( 
            SELECT'METEO' origine ,null cod_tipo_elemento , 'PCT-Q' cod_tipo_misura_in , 'PCT-Q' cod_tipo_misura_out , ':val * 1000' formula, 0 flg_split
              FROM DUAL
             UNION ALL
            SELECT'METEO',null,'PCT-P','PCT-P',':val * 1000',0
              FROM DUAL
             UNION ALL
            SELECT 'GME_M' origine, NULL cod_tipo_elemento
                  , 'CC-P' cod_tipo_misura_in, 'CC-P' cod_tipo_misura_out, ':val * 1000' formula, 0 flg_split
              FROM DUAL
             UNION ALL
            SELECT 'GME_M' origine, NULL cod_tipo_elemento
                  , 'CC-Q' cod_tipo_misura_in, 'CC-Q' cod_tipo_misura_out, ':val * 1000' formula, 0 flg_split
              FROM DUAL
             UNION ALL
            SELECT 'GME_M' origine, NULL cod_tipo_elemento
                  , 'PAAS' cod_tipo_misura_in, 'PAAS' cod_tipo_misura_out, ':val * 1000' formula, 0 flg_split
              FROM DUAL
             UNION ALL
            SELECT 'GME_M' origine, NULL cod_tipo_elemento
                  , 'PAGS' cod_tipo_misura_in, 'PAGS' cod_tipo_misura_out, ':val * 1000' formula, 0 flg_split
              FROM DUAL
             UNION ALL
            SELECT 'METEO' origine, NULL cod_tipo_elemento, 'PMP.P1' cod_tipo_misura_in, 'PMP.P1' cod_tipo_misura_out, ':val' formula, 1 flg_split
              FROM dual
             UNION ALL
            SELECT 'METEO',NULL,'PMP.A1','PMP.A1',':val',1
              FROM dual
             UNION ALL
            SELECT 'METEO',NULL,'PMC.P1','PMC.P1',':val',1
              FROM dual
             UNION ALL
            SELECT 'METEO',NULL,'PMC.A1','PMC.A1',':val',1    
              FROM dual
             UNION ALL
            SELECT 'GME_A' origine, NULL cod_tipo_elemento
                   , 'EAG' cod_tipo_misura_in, 'PAS' cod_tipo_misura_out, '(:val * 1000) * (60 / 15)' formula, 1 flg_split
              FROM DUAL
             UNION ALL
            SELECT 'GME_A' origine, NULL cod_tipo_elemento
                  , 'ERI' cod_tipo_misura_in, 'PRI' cod_tipo_misura_out, '(:val * 1000) * (60 / 15)' formula, 1 flg_split
              FROM DUAL
                             ) s
        ON ( t.origine = s.origine AND NVL(t.cod_tipo_elemento,'NULL') = NVL(s.cod_tipo_elemento,'NULL') AND t.cod_tipo_misura_in = s.cod_tipo_misura_in AND t.cod_tipo_misura_out =s.cod_tipo_misura_out)
      WHEN MATCHED THEN
    UPDATE SET 
              t.formula = s.formula
              ,t.flg_split = s.flg_split
      WHEN NOT MATCHED THEN 
    INSERT   
          (origine,cod_tipo_elemento,cod_tipo_misura_in,cod_tipo_misura_out,formula,flg_split)
    VALUES
          (s.origine , s.cod_tipo_elemento , s.cod_tipo_misura_in , s.cod_tipo_misura_out , s.formula , s.flg_split)
;

commit;