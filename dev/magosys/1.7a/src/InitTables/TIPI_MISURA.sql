--------------------------------------------------------
--  File creato - venerd�-agosto-02-2013   
--------------------------------------------------------
REM INSERTING into TIPI_MISURA

MERGE INTO tipi_misura t
     USING (SELECT 'PCT-P'cod_tipo_misura , NULL codifica_st , 'Previsione Carico Totale-P' descrizione , 0 mis_generazione , 15 risoluzione_fe , 'kW' cod_um_standard , 0 rilevazioni_nagative , 0 split_fonte_energia , '1' tipo_interpolazione_freq_camp , '1' tipo_interpolazione_buchi_camp, 2 priorita_aggr
              FROM DUAL 
             UNION ALL
            SELECT 'PCT-Q',NULL,'Previsione Carico Totale-Q',0,15,'kVAr',0,0,'1','1',2
              FROM DUAL 
             UNION ALL
            SELECT 'CC-P',NULL,'Carico Consolidato-P',0,15,'kW',1,0,'1','1',3
              FROM DUAL 
             UNION ALL
            SELECT 'CC-Q',NULL,'Carico Consolidato-Q',0,15,'kVAr',1,0,'1','1',3
              FROM DUAL 
             UNION ALL
            SELECT 'PAAS' cod_tipo_misura , NULL codifica_st , 'Potenza attiva assorbita  allo scambio' descrizione , 0 mis_generazione , 15 risoluzione_fe , 'kW' cod_um_standard, 1 rilevazioni_nagative, 0 split_fonte_energia, '1' tipo_interpolazione_freq_camp, '1' tipo_interpolazione_buchi_camp, 1 priorita_aggr
              FROM DUAL 
             UNION ALL
            SELECT 'PAGS',NULL,'Potenza attiva generata allo scambio',0,15,'kW',1,0,'1','1' ,1
              FROM DUAL
             UNION ALL
            SELECT 'PPRGP' cod_tipo_misura , 'PPRGP' codifica_st , 'Previsione Potenza Reattiva Generata' descrizione , 0 mis_generazione , 60 risoluzione_fe , 'kVAr' cod_um_standard, 0 rilevazioni_nagative, 1 split_fonte_energia, '1' tipo_interpolazione_freq_camp, '1' tipo_interpolazione_buchi_camp, 1 priorita_aggr
              FROM DUAL 
             UNION ALL
					  --1.5.a.0
					  SELECT 'PMP.A1' cod_tipo_misura, NULL codifica_st, 'Meteo Previsto in Potenza per Area' descrizione, 0 mis_generazione, 60 risoluzione_fe, 'kW' cod_um_standard, 0 rilevazioni_nagative, 1 split_fonte_energia, '1' tipo_interpolazione_freq_camp, '1' tipo_interpolazione_buchi_camp, 1 priorita_aggr
              FROM dual
             UNION ALL
            SELECT 'PMP.P1', NULL, 'Meteo Previsto in Potenza per Punto', 0, 60, 'kW', 0, 1, '1', '1',1
              FROM dual
             UNION ALL
            SELECT 'PMC.A1', NULL, 'Meteo Previsto Cielo Terso in Potenza per Area', 0, 60, 'kW', 0, 1, '1', '1',1
              FROM dual
             UNION ALL
            SELECT 'PMC.P1', NULL, 'Meteo Previsto Cielo Terso in Potenza per Punto', 0, 60, 'kW', 0, 1, '1', '1',1
              FROM dual
            --1.6.a.0
             UNION ALL
            SELECT 'PAGCTI', 'PPAGcti', 'Potenza Attiva Generata a cielo terso/interrompibile', 0, 60,'kW', 0, 1, '1', '1',1
              FROM DUAL
             UNION ALL
            SELECT 'SPAP', 'SPAP', 'Saldo di Potenza Attiva Prevista', 0, 60,'kW', 0, 1, '1', '1',1
              FROM DUAL
             UNION ALL
            SELECT 'SPRP', 'SPRP', 'Saldo di Potenza Reattiva Prevista', 0, 60,'kVAr', 0, 1, '1', '1',1
              FROM DUAL                      
           ) s
        ON (t.cod_tipo_misura = s.cod_tipo_misura)
      WHEN MATCHED THEN
    UPDATE SET 
               t.codifica_st = s.codifica_st
              ,t.descrizione = s.descrizione
              ,t.mis_generazione = s.mis_generazione
              ,t.risoluzione_fe = s.risoluzione_fe
              ,t.cod_um_standard = s.cod_um_standard
              ,t.rilevazioni_nagative = s.rilevazioni_nagative
              ,t.split_fonte_energia = s.split_fonte_energia
              ,t.tipo_interpolazione_freq_camp = s.tipo_interpolazione_freq_camp
              ,t.tipo_interpolazione_buchi_camp = s.tipo_interpolazione_buchi_camp
              ,t.priorita_aggr = s.priorita_aggr
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_tipo_misura,codifica_st,descrizione,mis_generazione,risoluzione_fe,cod_um_standard,rilevazioni_nagative,split_fonte_energia,tipo_interpolazione_freq_camp,tipo_interpolazione_buchi_camp,priorita_aggr) 
    VALUES (s.cod_tipo_misura, s.codifica_st, s.descrizione, s.mis_generazione, s.risoluzione_fe, s.cod_um_standard, s.rilevazioni_nagative, s.split_fonte_energia, s.tipo_interpolazione_freq_camp, s.tipo_interpolazione_buchi_camp,s.priorita_aggr)
;

commit;
