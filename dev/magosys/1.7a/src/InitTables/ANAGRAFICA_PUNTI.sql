MERGE INTO anagrafica_punti t
     USING ( SELECT 30120001 cod_meteo , 'P' tipo_punto , 'S' tipo_previsione
                   ,pkg_localizza_geo.coord_conversion('41 34 57.81' ,'DMS') coord_x, pkg_localizza_geo.coord_conversion('14 17 00.45','DMS') coord_y, 0 coord_h
               FROM dual
              UNION ALL
             SELECT 30120002 cod_meteo , 'P' tipo_punto , 'S' tipo_previsione
                   ,pkg_localizza_geo.coord_conversion('41 35 49.32' ,'DMS') coord_x, pkg_localizza_geo.coord_conversion('14 17 02.71','DMS') coord_y, 0 coord_h
               FROM DUAL
              UNION ALL
             SELECT 30120003 cod_meteo , 'P' tipo_punto , 'S' tipo_previsione
                   ,pkg_localizza_geo.coord_conversion('41 39 28.24' ,'DMS') coord_x, pkg_localizza_geo.coord_conversion('14 13 11.32','DMS') coord_y, 0 coord_h
               FROM DUAL
              UNION ALL
             SELECT 30120004 cod_meteo , 'P' tipo_punto , 'S' tipo_previsione
                   ,pkg_localizza_geo.coord_conversion('41 37 11.26' ,'DMS') coord_x, pkg_localizza_geo.coord_conversion('14 14 19.52','DMS') coord_y, 0 coord_h
               FROM DUAL
              UNION ALL
             SELECT 30120005 cod_meteo , 'P' tipo_punto , 'S' tipo_previsione
                   ,pkg_localizza_geo.coord_conversion('41 36 14.42' ,'DMS') coord_x, pkg_localizza_geo.coord_conversion('14 18 01.49','DMS') coord_y, 0 coord_h
               FROM DUAL
             ) s
        ON (s.cod_meteo = t.cod_geo)
      WHEN MATCHED THEN 
    UPDATE SET
               t.tipo_coord = s.tipo_punto
              ,t.coordinata_x = s.coord_x
              ,t.coordinata_y = s.coord_y
              ,t.coordinata_h = s.coord_h
     WHEN NOT MATCHED THEN
   INSERT 
         ( cod_geo,tipo_coord,coordinata_x,coordinata_y,coordinata_h ) 
   VALUES
         ( s.cod_meteo, s.tipo_punto, s.coord_x, s.coord_y, s.coord_h);
     
COMMIT;
