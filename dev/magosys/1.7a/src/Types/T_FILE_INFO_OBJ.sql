--------------------------------------------------------
--  File creato - venerdl-novembre-15-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Type T_FILE_INFO_OBJ
--------------------------------------------------------
PROMPT TYPE T_FILE_INFO_OBJ;
--
-- T_PARAM_PREV_OBJ  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--
DROP TYPE T_FILE_INFO_ARRAY;

  CREATE OR REPLACE TYPE T_FILE_INFO_OBJ as object
  (
    ID_FILE        NUMBER,
    NOME_FILE      VARCHAR2(200),
    TIPO_FILE      VARCHAR2(200),
    DATA_ELAB      DATE,
    PROCESSO       VARCHAR2(500) 
  );

/

