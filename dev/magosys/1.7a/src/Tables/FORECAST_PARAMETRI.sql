Prompt Table forecast_parametri;

ALTER TABLE forecast_parametri ADD
(COD_TIPO_COORD VARCHAR2(1) DEFAULT 'C'
,COD_PREV_METEO VARCHAR2(1) DEFAULT 0
);

ALTER TABLE forecast_parametri DROP CONSTRAINT forecast_parametri_pk;

drop index FORECAST_PARAMETRI_PK;

ALTER TABLE forecast_parametri ADD CONSTRAINT forecast_parametri_pk PRIMARY KEY (cod_elemento, cod_tipo_fonte, cod_tipo_coord, cod_prev_meteo);
