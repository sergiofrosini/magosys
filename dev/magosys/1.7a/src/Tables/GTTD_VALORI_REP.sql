--------------------------------------------------------
--  File creato - mercoledý-dicembre-11-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table GTTD_VALORI_REP
--------------------------------------------------------

  CREATE GLOBAL TEMPORARY TABLE GTTD_VALORI_REP 
   (	TIP VARCHAR2(10 BYTE), 
	NUM1 NUMBER, 
	NUM2 NUMBER, 
	NUM3 NUMBER, 
	NUM4 NUMBER, 
	NUM5 NUMBER, 
	ALF1 VARCHAR2(100 BYTE), 
	ALF2 VARCHAR2(100 BYTE), 
	ALF3 VARCHAR2(4000 BYTE), 
	ALF4 VARCHAR2(100 BYTE), 
	ALF5 VARCHAR2(100 BYTE), 
	DAT1 DATE, 
	DAT2 DATE, 
	DAT3 DATE, 
	HEX RAW(4)
   ) ON COMMIT PRESERVE ROWS ;
--------------------------------------------------------
--  DDL for Index GTTD_VALORI_REP_KA1
--------------------------------------------------------

  CREATE INDEX GTTD_VALORI_REP_KA1 ON GTTD_VALORI_REP (TIP, ALF1) ;
--------------------------------------------------------
--  DDL for Index GTTD_VALORI_REP_KA2
--------------------------------------------------------

  CREATE INDEX GTTD_VALORI_REP_KA2 ON GTTD_VALORI_REP (TIP, ALF2) ;
--------------------------------------------------------
--  DDL for Index GTTD_VALORI_REP_KEY
--------------------------------------------------------

  CREATE INDEX GTTD_VALORI_REP_KEY ON GTTD_VALORI_REP (TIP) ;
--------------------------------------------------------
--  DDL for Index GTTD_VALORI_REP_KN1
--------------------------------------------------------

  CREATE INDEX GTTD_VALORI_REP_KN1 ON GTTD_VALORI_REP (TIP, NUM1) ;
--------------------------------------------------------
--  DDL for Index GTTD_VALORI_REP_KN2
--------------------------------------------------------

  CREATE INDEX GTTD_VALORI_REP_KN2 ON GTTD_VALORI_REP (TIP, NUM2) ;
