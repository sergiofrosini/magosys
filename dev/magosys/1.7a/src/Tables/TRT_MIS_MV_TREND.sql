--------------------------------------------------------
--  File creato - gioved�-agosto-29-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TRT_MIS_MV_TREND
--------------------------------------------------------

  CREATE TABLE TRT_MIS_MV_TREND 
   (	IND_LCURVE   NUMBER, 
	    STAGIONE     NUMBER, 
	    GIORNO       NUMBER, 
	    CAMPIONE     NUMBER, 
	    TIPO_POTENZA VARCHAR2(1), 
	    VALORE       NUMBER(20,9)
   ) ;
--------------------------------------------------------
--  DDL for Index TRT_MIS_MV_TREND_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX TRT_MIS_MV_TREND_PK ON TRT_MIS_MV_TREND (IND_LCURVE, STAGIONE, GIORNO, CAMPIONE, TIPO_POTENZA) 
  ;
--------------------------------------------------------
--  Constraints for Table TRT_MIS_MV_TREND
--------------------------------------------------------

  ALTER TABLE TRT_MIS_MV_TREND MODIFY (IND_LCURVE NOT NULL ENABLE);
 
  ALTER TABLE TRT_MIS_MV_TREND MODIFY (STAGIONE NOT NULL ENABLE);
 
  ALTER TABLE TRT_MIS_MV_TREND MODIFY (GIORNO NOT NULL ENABLE);
 
  ALTER TABLE TRT_MIS_MV_TREND MODIFY (CAMPIONE NOT NULL ENABLE);
 
  ALTER TABLE TRT_MIS_MV_TREND MODIFY (TIPO_POTENZA NOT NULL ENABLE);
 
  ALTER TABLE TRT_MIS_MV_TREND ADD CONSTRAINT TRT_MIS_MV_TREND_PK PRIMARY KEY (IND_LCURVE, STAGIONE, GIORNO, CAMPIONE, TIPO_POTENZA) ENABLE;
