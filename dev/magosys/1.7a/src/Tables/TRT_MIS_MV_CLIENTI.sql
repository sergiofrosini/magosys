--------------------------------------------------------
--  File creato - venerd�-agosto-02-2013   
--------------------------------------------------------
--------------------------------------------------------
--  DDL for Table TRT_MIS_MV_CLIENTI
--------------------------------------------------------

CREATE TABLE TRT_MIS_MV_CLIENTI
  (
    COLNUMBER  NUMBER,
    POD        VARCHAR2(20 BYTE),
    AUI        VARCHAR2(20 BYTE),
    IND_LCURVE NUMBER,
    VAL_AVGAP  NUMBER(20,9),
    VAL_AVGAQ  NUMBER(20,9)
  ) ;