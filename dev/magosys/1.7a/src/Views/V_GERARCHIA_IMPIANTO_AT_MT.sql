Prompt View V_GERARCHIA_IMPIANTO_AT_MT;
--
-- V_GERARCHIA_IMPIANTO_AT_MT  (View) 
--
--  Dependencies: 
--   ESERCIZI (Table)
--   AVVOLGIMENTI_SA (Table)
--   IMPIANTIAT_SA (Table)
--   MONTANTIMT_SA (Table)
--   SEZIONATORI_SA (Table)
--   TRASFORMATORIAT_SA (Table)
--   SBARRE_SA (Table)
--   IMPIANTIMT_SA (Table)
--   ELEMENTI (Table)
--   PKG_ELEMENTI (Package)
--   V_ESERCIZI (View)
--
CREATE OR REPLACE VIEW V_GERARCHIA_IMPIANTO_AT_MT
AS 
SELECT L1, L2, L3, L4, L5, L6, L7, L8
  FROM (SELECT DISTINCT
               PKG_ELEMENTI.GetElementoBase L1,
               ESE L2,
               CPR L3,
               TRA L4,
               SEC L5,
               SMT L6,
               LMT L7,
               SCS L8,
               ORDINAMENTO_ESERCIZIO,
               CASE
                 WHEN SCS IS NOT NULL THEN 8
                 WHEN LMT IS NOT NULL THEN 7
                 WHEN SMT IS NOT NULL THEN 6
                 WHEN SEC IS NOT NULL THEN 5
                 WHEN TRA IS NOT NULL THEN 4
                 WHEN CPR IS NOT NULL THEN 3
                 WHEN ESE IS NOT NULL THEN 2
                 WHEN PKG_ELEMENTI.GetElementoBase IS NOT NULL THEN 1
                                      ELSE 0
               END LAST
          FROM (SELECT *
                  FROM (SELECT ESE,ESE_COD,
                               CPR,CPR_COD,
                               TRA,TRA_COD,
                               SEC,SEC_COD,
                               SMT,SMT_COD,
                               LMT,LMT_COD,LMT_ID,
                               ORDINAMENTO_ESERCIZIO
                          FROM (SELECT COD_ELEMENTO ESE,ESE_COD,COD_GEST,ESE_ID,ORDINAMENTO_ESERCIZIO
                                  FROM (SELECT CASE Y.ESE
                                                   WHEN X.ESE THEN COD_GEST
                                                   ELSE COD_GEST||'_'||Y.ESE
                                               END ESE_COD,COD_GEST,CODICE_ST ESE_ID,
                                               (NVL(ESE_PRIMARIO,-1)*-1) ORDINAMENTO_ESERCIZIO
                                          FROM CORELE.ESERCIZI Y
                                          LEFT OUTER JOIN V_ESERCIZI X ON Y.ESE = X.ESE
                                         WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                       )
                                 INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = ESE_COD
                               ) ESE
                        LEFT OUTER JOIN
                               (SELECT COD_ELEMENTO CPR,CODICE_ST CPR_ID, COD_GEST CPR_COD, COD_ESERCIZIO ESE_ID
                                  FROM CORELE.IMPIANTIAT_SA
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                               ) CPR USING (ESE_ID)
                        LEFT OUTER JOIN
                               (SELECT CODICE_ST SAT_ID, COD_GEST SAT_COD, CODICEST_IMPAT CPR_ID
                                  FROM CORELE.SBARRE_SA S
                                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND TIPO = 'AT'
                                 UNION ALL /* SELEZIONA EVENTUALI SBARRE MT CHE ALIMENTANO IL PRIMARIO DI UN TRASF.MT/MT */
                                SELECT CODICE_ST SAT_ID, COD_GEST SAT_COD, CODICEST_IMPAT CPR_ID
                                  FROM CORELE.SBARRE_SA S
                                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                                   AND CODICE_ST IN (SELECT /*+ PUSH_SUBQ */ CODICEST_SBARRA
                                                       FROM CORELE.AVVOLGIMENTI_SA AVVOLGIMENTI_SA1
                                                      WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                                        AND COD_ENTE = 'PRIM'
                                                      GROUP BY CODICEST_SBARRA)
                               ) SAT USING (CPR_ID)
                        LEFT OUTER JOIN
                               (SELECT CODICE_ST AVV_PRIM_ID, COD_GEST AVV_PRIM_COD,CODICEST_SBARRA SAT_ID
                                  FROM CORELE.AVVOLGIMENTI_SA
                                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND COD_ENTE = 'PRIM' /*AND STATO = 'CH'*/
                               ) TRP USING (SAT_ID)
                        LEFT OUTER JOIN
                               (SELECT COD_ELEMENTO TRA,CODICE_ST TRA_ID, COD_GEST TRA_COD, CODICEST_AVV_PRIM AVV_PRIM_ID
                                  FROM CORELE.TRASFORMATORIAT_SA
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                ) TRF USING (AVV_PRIM_ID)
                        LEFT OUTER JOIN
                               (SELECT COD_ELEMENTO SEC,TR.CODICEST_AVV_SECN AVV_SEC_TERZ_ID,AV.COD_GEST SEC_COD,
                                       TR.CODICE_ST TRA_ID,AV.STATO
                                  FROM CORELE.TRASFORMATORIAT_SA TR, CORELE.AVVOLGIMENTI_SA AV
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = AV.COD_GEST
                                 WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                                   AND SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                   AND AV.CODICE_ST = TR.CODICEST_AVV_SECN AND AV.STATO = 'CH' /*AND AV.STATO = 'CH'*/
                                UNION
                                SELECT COD_ELEMENTO SEC,TR.CODICEST_AVV_TERZ AVV_SEC_TERZ_ID,AV.COD_GEST AVV_SEC_TERZ_COD,
                                       TR.CODICE_ST TRA_ID,AV.STATO
                                  FROM CORELE.TRASFORMATORIAT_SA TR, CORELE.AVVOLGIMENTI_SA AV
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = AV.COD_GEST
                                 WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
                                   AND SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                   AND AV.CODICE_ST = TR.CODICEST_AVV_TERZ AND AV.STATO = 'CH' /*AND AV.STATO = 'CH'*/
                               ) AVS USING(TRA_ID)
                        LEFT OUTER JOIN
                               (/*
                                SELECT COD_ELEMENTO SMT,CODICE_ST SMT_ID, COD_GEST SMT_COD, CODICEST_AVV AVV_SEC_TERZ_ID
                                  FROM CORELE.SBARRE_SA
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND TIPO = 'MT'
                                   AND (CODICEST_AVV > 'ND' OR CODICEST_AVV < 'ND') --AND CODICEST_AVV <> 'ND'
                                */
                                /*
                                SELECT COD_ELEMENTO SMT,S.CODICE_ST SMT_ID, S.COD_GEST SMT_COD, S.CODICEST_AVV AVV_SEC_TERZ_ID,
                                        Z.STATO, z.cod_gest sez,  Z.CODICEST_SBARRA1,  Z.CODICEST_SBARRA2,
                                        z.data_inizio, z.data_fine
                                  FROM CORELE.SBARRE_SA S
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = S.COD_GEST
                                  LEFT OUTER JOIN (SELECT *
                                                     FROM CORELE.SEZIONATORI_SA
                                                    WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                                  ) Z ON (   Z.CODICEST_SBARRA1 = S.CODICE_ST
                                                          OR Z.CODICEST_SBARRA2 = S.CODICE_ST)
                                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                                   AND (S.CODICEST_AVV > 'ND' OR S.CODICEST_AVV < 'ND')  --AND S.CODICEST_AVV <> 'ND'
                                   AND Z.STATO IS NULL OR Z.STATO = 'CH'
                                */
                                SELECT COD_ELEMENTO SMT,S.CODICE_ST SMT_ID, S.COD_GEST SMT_COD, S.CODICEST_AVV AVV_SEC_TERZ_ID
                                  FROM CORELE.SBARRE_SA S
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = S.COD_GEST
                                  LEFT OUTER JOIN (SELECT CODICEST_SBARRA1 CODICE_ST
                                                     FROM CORELE.SEZIONATORI_SA Z
                                                    INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA1 = S.CODICE_ST
                                                    WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                                      AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                                   UNION
                                                   SELECT CODICEST_SBARRA2 CODICE_ST
                                                     FROM CORELE.SEZIONATORI_SA Z
                                                    INNER JOIN CORELE.SBARRE_SA S ON CODICEST_SBARRA2 = S.CODICE_ST
                                                    WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                                                      AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                                                  ) Z ON (Z.CODICE_ST = S.CODICE_ST)
                                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE AND S.TIPO = 'MT'
                                   AND S.CODICEST_AVV <> 'ND'
                               ) SMT USING(AVV_SEC_TERZ_ID)
                        LEFT OUTER JOIN
                               (SELECT COD_ELEMENTO LMT,CODICE_ST LMT_ID, COD_GEST LMT_COD,CODICEST_SBARRA SMT_ID
                                  FROM CORELE.MONTANTIMT_SA
                                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                                 WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND STATO = 'CH'
                               ) LMT USING(SMT_ID)
                       )
                  UNION
                       (SELECT ESE, ESE_COD,
                               CPR, CPR_COD,
                               NULL,NULL,
                               NULL,NULL,
                               SMT, SMT_COD,
                               LMT, LMT_COD,LMT_ID,ORDINAMENTO_ESERCIZIO
                          FROM (SELECT A.*, (NVL(ESE_PRIMARIO,-1)*-1) ORDINAMENTO_ESERCIZIO
                                  FROM (SELECT X.COD_ELEMENTO  ESE, A.COD_ESERCIZIO ESE_ID, E.COD_GEST ESE_COD,
                                               Y.COD_ELEMENTO  CPR, A.CODICE_ST     CPR_ID, A.COD_GEST CPR_COD
                                          FROM CORELE.IMPIANTIAT_SA A
                                         INNER JOIN CORELE.ESERCIZI E ON E.CODICE_ST = A.COD_ESERCIZIO
                                         INNER JOIN ELEMENTI x ON x.COD_GEST_ELEMENTO = E.COD_GEST
                                         INNER JOIN ELEMENTI y ON y.COD_GEST_ELEMENTO = A.COD_GEST
                                         WHERE A.CODICE_ST NOT IN (SELECT B.CODICEST_IMPAT
                                                                     FROM CORELE.SBARRE_SA B
                                                                    WHERE B.CODICEST_IMPAT = A.CODICE_ST
                                                                      AND B.TIPO='AT'
                                                                      AND SYSDATE BETWEEN B.DATA_INIZIO AND B.DATA_FINE
                                                                  )
                                         AND SYSDATE BETWEEN A.DATA_INIZIO AND A.DATA_FINE
                                       ) A
                                 INNER JOIN V_ESERCIZI ON GST_ESE = ESE_COD
                               ) CPR
                          LEFT OUTER JOIN
                               (SELECT CODICEST_IMPAT CPR_ID, X.COD_ELEMENTO SMT, A.CODICE_ST SMT_ID,A.COD_GEST SMT_COD
                                  FROM CORELE.SBARRE_SA A
                                 INNER JOIN ELEMENTI x ON x.COD_GEST_ELEMENTO = A.COD_GEST
                                 LEFT OUTER JOIN
                                    (SELECT A.CODICE_ST
                                       FROM CORELE.AVVOLGIMENTI_SA A
                                      WHERE A.COD_ENTE='SECN' OR A.COD_ENTE='TERZ'
                                        AND A.STATO='CH'
                                        AND SYSDATE BETWEEN A.DATA_INIZIO AND A.DATA_FINE
                                    ) V ON A.CODICEST_AVV = V.CODICE_ST
                                 WHERE A.TIPO='MT'
                                   AND A.CODICEST_AVV<>'ND'
                                   AND SYSDATE BETWEEN A.DATA_INIZIO AND A.DATA_FINE
                                ) SMT USING(CPR_ID)
                          LEFT OUTER JOIN
                                (SELECT COD_ELEMENTO LMT,CODICE_ST LMT_ID, COD_GEST LMT_COD,CODICEST_SBARRA SMT_ID
                                   FROM CORELE.MONTANTIMT_SA
                                   LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST
                                  WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE AND STATO = 'CH'
                                ) LMT USING(SMT_ID)
                         WHERE SMT IS NOT NULL
                       )
               ) A
          LEFT OUTER JOIN
               (SELECT COD_ELEMENTO SCS,SCS_COD,LMT_ID
                  FROM (SELECT COD_GEST CSE_COD,COD_GEST_SBARRA SCS_COD,CODICEST_MONTANTE LMT_ID
                          FROM CORELE.IMPIANTIMT_SA
                         WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                       )
                  LEFT OUTER JOIN ELEMENTI ON COD_GEST_ELEMENTO = SCS_COD
               ) B ON B.LMT_ID = A.LMT_ID
       )
/* inizio - C.M. 02/08/2013 - modifica per eliminare buchi */
 WHERE CASE
         WHEN LAST = 0 THEN 1  
         WHEN LAST = 8 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x')||NVL(TO_CHAR(l3),'x')||NVL(TO_CHAR(l4),'x')||NVL(TO_CHAR(l5),'x')||NVL(TO_CHAR(l6),'x')||NVL(TO_CHAR(l8),'x'),'x')  
         WHEN LAST = 7 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x')||NVL(TO_CHAR(l3),'x')||NVL(TO_CHAR(l4),'x')||NVL(TO_CHAR(l5),'x')||NVL(TO_CHAR(l6),'x'),'x')  
         WHEN LAST = 6 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x')||NVL(TO_CHAR(l3),'x')||NVL(TO_CHAR(l4),'x')||NVL(TO_CHAR(l5),'x'),'x')  
         WHEN LAST = 5 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x')||NVL(TO_CHAR(l3),'x')||NVL(TO_CHAR(l4),'x'),'x')
         WHEN LAST = 4 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x')||NVL(TO_CHAR(l3),'x'),'x')
         WHEN LAST = 3 THEN INSTR(NVL(TO_CHAR(l1),'x')||NVL(TO_CHAR(l2),'x'),'x')
         WHEN LAST = 2 THEN INSTR(NVL(TO_CHAR(l1),'x'),'x')
         ELSE 0
       END = 0 
/* fine  - C.M. 02/08/2013 - modifica per eliminare buchi */
 ORDER BY ORDINAMENTO_ESERCIZIO, L1 NULLS FIRST, L2 NULLS FIRST, L3 NULLS FIRST, L4 NULLS FIRST, L5 NULLS FIRST, L6 NULLS FIRST, L7 NULLS FIRST, L8 NULLS FIRST
/

 


