PROMPT Package Body PKG_LOCALIZZA_GEO;

CREATE OR REPLACE PACKAGE BODY PKG_LOCALIZZA_GEO AS
proc_name VARCHAR2(50);
--vLog         PKG_Mago.t_StandardLog;
err_code NUMBER;
err_msg VARCHAR2(250);
FUNCTION coord_conversion ( coord_value IN VARCHAR2, coord_type IN VARCHAR2, decimal_point IN VARCHAR2 DEFAULT '.' ) RETURN NUMBER AS
val NUMBER;
BEGIN
   CASE coord_type
      WHEN 'GPS' THEN
      /* Per convertire il formato GPS in WGS84 prendere i minuti e moltiplicarli per 60 */
         SELECT  SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                 +
                 TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = ''' ||decimal_point || '|''')  / 60 * SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ', 1)-1))
           INTO val
           FROM dual;
      WHEN 'DMS' THEN
         SELECT SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                +
                (( TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ') +1, INSTR(TRIM(coord_value),' ', -1) - INSTR(TRIM(coord_value),' ') -1),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 60
                  + TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 3600
                 )
                 *SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1))
                )
           INTO val
           FROM dual;
      WHEN 'WGS84' THEN
         val := to_number(TRIM(coord_value),'999D999999999999999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''');
      ELSE 
        val := to_number(TRIM(coord_value));
      END CASE;
RETURN (val);
END;
PROCEDURE load_punti AS
BEGIN
   proc_name := 'PKG_LOCALIZZA_GEO.LOAD_PUNTI';
   INSERT INTO anagrafica_punti
   (cod_geo , tipo_coord, coordinata_x, coordinata_y, coordinata_h )
   SELECT NVL(cod_geo_p,cod_geo_a),tipo_coord, coordinata_x, coordinata_y, altitudine
   FROM (
         SELECT new_coord.* 
               ,CASE WHEN new_coord.tipo_coord ='P' THEN cod_geo_p + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_p
               ,CASE WHEN new_coord.tipo_coord ='A' THEN cod_geo_a + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_a
           FROM
               (
                SELECT DISTINCT 'P' tipo_coord 
                       ,ROUND(ele.coordinata_y,7) coordinata_x --le coordinate in AUI sono invertite
                       ,ROUND(ele.coordinata_x,7) coordinata_y --le coordinate in AUI sono invertite
                       ,ele.altitudine altitudine
                  FROM elementi_def ele
                  WHERE NVL(ele.coordinata_x,ele.coordinata_y) IS NOT NULL
                    AND ele.cod_tipo_elemento = pkg_mago.gcSbarraCabSec
                 /* UNION ALL
                SELECT DISTINCT 'A' tipo_coord
                       ,ROUND(coord_conversion(prod.coord_nord_a, prod.tipo_coord_a),7) coordinata_x
                       ,ROUND(coord_conversion(prod.coord_est_a, prod.tipo_coord_a),7) coordinata_y
                       ,prod.coord_up_a altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NOT NULL
                 UNION
                SELECT 'A' tipo_coord
                       ,ROUND(AVG(coord_conversion(prod.coord_nord_p, prod.tipo_coord_p)),7) coordinata_x
                       ,ROUND(AVG(coord_conversion(prod.coord_est_p, prod.tipo_coord_p)),7) coordinata_y
                       ,ROUND(AVG(prod.coord_up_p)) altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NULL
                  GROUP BY NVL(prod.cg_produttore,prod.cod_gruppo)
                */
            ) new_coord
               , anagrafica_punti old_coord
               ,( SELECT 
                      NVL(MAX(CASE WHEN anag.TIPO_COORD = 'P' THEN anag.COD_GEO END),MAX(TO_NUMBER(ese.cod_utr || trim(to_char(ese.cod_Esercizio,'00')) || 20000))) cod_geo_p 
                     ,NVL(MAX(CASE WHEN anag.TIPO_COORD = 'A' THEN anag.COD_GEO END),MAX(TO_NUMBER(ese.cod_utr || trim(to_char(ese.cod_Esercizio,'00')) || 300000))) cod_geo_a
                    FROM anagrafica_punti anag
                        ,default_co def
                        ,elementi el 
                        ,sar_admin.unita_territoriali utr
                        ,sar_admin.esercizi ese
                   where ese.cod_utr = utr.cod_utr
                     and def.cod_elemento_ese = el.cod_elemento
                     and def.flag_primario = 1
                     and utr.codifica_utr || ese.codifica_esercizio = el.cod_gest_elemento
               ) maxval
          WHERE new_coord.coordinata_x = old_coord.coordinata_x(+)
            AND new_coord.coordinata_y = old_coord.coordinata_y(+)
            AND new_coord.altitudine = old_coord.coordinata_h(+) 
            AND new_coord.tipo_coord = old_coord.tipo_coord(+)
            AND old_coord.cod_geo IS NULL
        );
   COMMIT;
END LOAD_PUNTI;
PROCEDURE associa_punti(coord_type IN VARCHAR2) AS 
BEGIN 

   MERGE INTO elementi_def t
        USING (              
                SELECT anag.cod_geo
                       ,ele.cod_elemento
                       ,ROUND(ele.coordinata_y,7) coordinata_x --le coordinate in AUI sono invertite
                       ,ROUND(ele.coordinata_x,7) coordinata_y --le coordinate in AUI sono invertite
                       ,ele.altitudine altitudine
                  FROM elementi_def ele
                      ,anagrafica_punti anag
                  WHERE NVL(ele.coordinata_x,ele.coordinata_y) IS NOT NULL
                    AND ele.cod_tipo_elemento = pkg_mago.gcSbarraCabSec
                    AND ROUND(ele.coordinata_y,7) = anag.coordinata_x
                    AND ROUND(ele.coordinata_x,7) = anag.coordinata_y 
                    AND ele.altitudine = anag.coordinata_h
              ) s
           ON (t.cod_elemento = s.cod_elemento)
      WHEN MATCHED THEN
    UPDATE SET 
              t.cod_geo = s.cod_geo
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_elemento) 
    VALUES (NULL); 
    
   COMMIT;
END;
END;
/

SHO ERRORS;
