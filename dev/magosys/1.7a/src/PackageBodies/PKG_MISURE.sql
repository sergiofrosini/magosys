PROMPT PACKAGE BODY PKG_MISURE;
--
-- PKG_MISURE  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_MISURE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.7.a.7
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 pMisura                   PKG_GestAnagr.t_DefAnagr;
 gUltimoTipoMisura         TIPI_MISURA.COD_TIPO_MISURA%TYPE := '*';
 gPrevData                 DATE := PKG_UtlGlb.gkDataTappo;

 cTotalizza       CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagON);   -- Totalizza misure per tipo codice
 cNonTotalizzare  CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagOFF);  -- Per il tipo codice non totalizzare le misure
 
/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisuraOut     (pOrigineMis    IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                            pTipele        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                            pTipMisIn      IN TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_IN%TYPE,
                            pTipMisOut    OUT TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE,
                            pFattore      OUT NUMBER,
                            pFlagSplit    OUT TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE) RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna le informazioni relative alla misura da memorizzare
-----------------------------------------------------------------------------------------------------------*/
    vTrovato   BOOLEAN := FALSE;
    vFormula   TIPI_MISURA_CONV_ORIG.FORMULA%TYPE;
    vTipMisOut TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
 BEGIN
    IF pTipele IS NOT NULL THEN
        BEGIN
            SELECT FORMULA,  COD_TIPO_MISURA_OUT, FLG_SPLIT
              INTO vFormula, pTipMisOut,          pFlagSplit
              FROM TIPI_MISURA_CONV_ORIG
             WHERE ORIGINE = pOrigineMis
               AND COD_TIPO_MISURA_IN = pTipMisIn
               AND COD_TIPO_ELEMENTO = pTipele;
            vTrovato := TRUE;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vTrovato := FALSE;
            WHEN OTHERS THEN RAISE;
        END;
    END IF;
    IF NOT vTrovato THEN
        BEGIN
            SELECT FORMULA,  COD_TIPO_MISURA_OUT, FLG_SPLIT
              INTO vFormula, pTipMisOut,          pFlagSplit
              FROM TIPI_MISURA_CONV_ORIG
             WHERE ORIGINE = pOrigineMis
               AND COD_TIPO_MISURA_IN = pTipMisIn;
            vTrovato := TRUE;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vTrovato := FALSE;
            WHEN OTHERS THEN RAISE;
        END;
    END IF;
    IF vTrovato THEN
        IF vFormula IS NOT NULL THEN
            EXECUTE IMMEDIATE 'SELECT '||vFormula||' FROM DUAL' INTO pFattore USING 1;
        ELSE
            pFattore := 1;
        END IF;
    END IF;
    RETURN vTrovato;
 EXCEPTION
    WHEN OTHERS THEN RAISE;
 END GetMisuraOut;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisuraAcq         (pTipoProcesso  IN VARCHAR2,
                                 pCodTrtEle     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                 pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                 pTipoMis       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pValore        IN MISURE_ACQUISITE.VALORE%TYPE,
                                 pTipRete       IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 pAggrega       IN BOOLEAN,
                                 pResult       OUT INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza la misura:
     - Si inserisce la misura; se esiste si controlla se va aggiornata.
     - Si esegue il controllo sulle date da aggregare.
-----------------------------------------------------------------------------------------------------------*/
    vValore NUMBER(20,4) := ROUND(pValore,4);
    vPerc   NUMBER(5,2):= 100;
 BEGIN

     pResult := PKG_Mago.gcNoOper;
     BEGIN
        IF PKG_Mago.IsMagoDGF THEN
            SELECT PERCENTUALE
              INTO vPerc
              FROM MANUTENZIONE man
                   , TRATTAMENTO_ELEMENTI tr
              WHERE tr.COD_TRATTAMENTO_ELEM = pCodTrtEle
                AND tr.COD_ELEMENTO = man.COD_ELEMENTO
                AND pDataMis >= man.DATA_INIZIO
                AND pDataMis < NVL(man.DATA_FINE,TO_DATE('01013000','ddmmyyyy'))
                AND NVL(man.FLG_DELETED,0) != 1;
        END IF;
     EXCEPTION WHEN NO_DATA_FOUND THEN
        vPerc := 100;
     END;
     IF PKG_Misure.IsMisuraStatica(pTipoMis) = PKG_UtlGlb.gkFlagOn THEN
         IF gPrevData <> pDataMis THEN
             gPrevData := pDataMis;
             pMisura := NULL;
             PKG_GestAnagr.InitTab(pMisura,pDataMis,USER,'MISURE_ACQUISITE_STATICHE','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
             PKG_GestAnagr.AddCol (pMisura,'COD_TRATTAMENTO_ELEM',PKG_GestAnagr.cColChiave);
             PKG_GestAnagr.AddCol (pMisura,'VALORE',PKG_GestAnagr.cColAttributo);
         END IF;
         PKG_GestAnagr.InitRow(pMisura);
         PKG_GestAnagr.AddVal (pMisura, 'COD_TRATTAMENTO_ELEM', pCodTrtEle);
         vValore := ROUND(vValore * (vperc / 100),4);
         PKG_GestAnagr.AddVal (pMisura, 'VALORE', vValore);
         PKG_GestAnagr.Elabora(pMisura);
         pResult := PKG_Mago.gcInserito;
     ELSE
        BEGIN
            IF vperc != 0 THEN
               vValore := ROUND(vValore * (vperc / 100),4);
            END IF;
            INSERT INTO MISURE_ACQUISITE(COD_TRATTAMENTO_ELEM
                                        ,DATA
                                        ,VALORE)
                                  VALUES(pCodTrtEle
                                        ,pDataMis
                                        ,vValore);
            pResult := PKG_Mago.gcInserito;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                 UPDATE MISURE_ACQUISITE
                    SET VALORE  = vValore
                  WHERE COD_TRATTAMENTO_ELEM = pCodTrtEle
                    AND DATA = pDataMis
                    AND VALORE != vValore;
                 IF SQL%ROWCOUNT > 0 THEN
                      pResult := PKG_Mago.gcModificato;
                 END IF;
            WHEN OTHERS THEN
             RAISE;
        END;
     END IF;

     IF pAggrega THEN
         IF pResult IN (PKG_Mago.gcModificato,PKG_Mago.gcInserito) THEN
             -- Richiesta di calcolo aggregate
             CASE
                WHEN PKG_Mago.IsMagoSTM THEN
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     IF pTipRete <> PKG_Mago.gcTipReteAT THEN
                         PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                         PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     END IF;
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                WHEN PKG_Mago.IsMagoDGF THEN
                     -- inizio : DGF non utilizza la gerarchia eletterica
                     -- PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     -- fine   : DGF non utilizza la gerarchia eletterica
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                ELSE
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
             END CASE;
         END IF;
     END IF;
     RETURN;
 END AddMisuraAcq;
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGmeFgl(pTipoProcesso IN VARCHAR2,
                           pOrigineMis   IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                           pCodEle       IN ELEMENTI.COD_ELEMENTO%TYPE,
                           pGstEle       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pTipEle       IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                           pTipRete      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                           pTipFte       IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pAggrega      IN BOOLEAN,
                           pNumIns      OUT INTEGER,
                           pNumMod      OUT INTEGER,
                           pNumKO       OUT INTEGER,
                           pMisure       IN T_MISURA_GME_ARRAY,
                           pLog      IN OUT PKG_Logs.t_StandardLog) AS
/*-----------------------------------------------------------------------------------------------------------
    Ripartisce la misura sui figli dell'elemento ricevuto
    . i contolli su omogeneita' del tipo fonte devono essere eseguiti a monte
-----------------------------------------------------------------------------------------------------------*/

  vResult    INTEGER;

  vNomEle    ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vTipEle    ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
  vTipRet    TIPI_RETE.COD_TIPO_RETE%TYPE;
  vTipCli    ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vPotInst   NUMBER;

  vTrtEle    TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vTipMisOK  BOOLEAN;
  vTipMis    TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;
  vFattore   NUMBER;

  vTxtLog    VARCHAR2(10);

  vSavMis1   TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vSavMis2   TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vSavCod    ELEMENTI.COD_ELEMENTO%TYPE := -1;

BEGIN

    pNumIns := 0;
    pNumMod := 0;
    pNumKO  := 0;

    SELECT COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE, NOME_ELEMENTO, COD_TIPO_RETE
      INTO vTipEle,           vTipCli,             vNomEle,       vTipRet
      FROM (SELECT COD_ELEMENTO,D.COD_TIPO_ELEMENTO,COD_TIPO_CLIENTE,COD_TIPO_FONTE,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_RETE,
                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
              FROM ELEMENTI_DEF D
             INNER JOIN ELEMENTI USING(COD_ELEMENTO)
             INNER JOIN TIPI_ELEMENTO T ON  T.COD_TIPO_ELEMENTO = D.COD_TIPO_ELEMENTO
             WHERE COD_ELEMENTO = pCodEle
            )
      WHERE ORD = 1;

    IF vTipRet IS NULL THEN
        PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Rete non definito' ,FALSE,gkAnagrIncompl);
        RETURN;
    END IF;
    IF vTipCli IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vTipCli := PKG_Mago.gcClientePrdNonApplic;
        ELSE
            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
        END IF;
    END IF;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP

        IF vSavMis1 <> pMisure(i).TIPO_MISURA THEN
           vSavMis1 := pMisure(i).TIPO_MISURA;
            vTipMisOK := GetMisuraOut(pOrigineMis,pTipEle,pMisure(i).TIPO_MISURA,vTipMis,vFattore,vFlagSplit);
        END IF;

        IF vTipMisOK THEN
            FOR x IN (SELECT COD_GEST_ELEMENTO,
                             COD_ELEMENTO_FIGLIO COD_ELEMENTO,
                             POTENZA_INSTALLATA,
                             SUM(POTENZA_INSTALLATA) OVER (PARTITION BY COD_ELEMENTO_PADRE) POT_TOT
                        FROM (SELECT COD_ELEMENTO COD_ELEMENTO_FIGLIO,COD_ELEMENTO_PADRE,
                                     COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_GEST_ELEMENTO
                                FROM (SELECT COD_ELEMENTO COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO COD_GESTIONALE_PADRE,
                                             ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                        FROM ELEMENTI_DEF
                                       INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                                       WHERE COD_ELEMENTO = pCodEle
                                     ) P
                               INNER JOIN ELEMENTI_DEF ON ID_ELEMENTO = P.COD_GESTIONALE_PADRE
                               INNER JOIN ELEMENTI     USING(COD_ELEMENTO)
                               WHERE ORD = 1
                             )
                     )
            LOOP
                IF (vSavMis2 <> pMisure(i).TIPO_MISURA) OR (vSavCod <> x.COD_ELEMENTO) THEN
                    vSavMis2 := pMisure(i).TIPO_MISURA;
                    vSavCod  := x.COD_ELEMENTO;
                    vTrtEle := GetTrattamentoElemento(x.COD_ELEMENTO,
                                                      vTipEle,
                                                      vTipMis,
                                                      pTipFte,
                                                      vTipRet,
                                                      vTipCli,
                                                      PKG_Mago.gcOrganizzazELE,
                                                      PKG_Mago.gcStatoNullo);
                END IF;
                AddMisuraAcq(pTipoProcesso,
                             vTrtEle,
                             pMisure(i).DATA_MIS,
                             vTipMis,
                             ROUND(((pMisure(i).VALORE * X.POTENZA_INSTALLATA) / X.POT_TOT) * vFattore,4),
                             pMisure(i).TIPO_RETE,
                             pAggrega,
                             vResult);

                CASE vResult
                    WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                    WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                    ELSE NULL;
                END CASE;

            END LOOP;
        ELSE
            pNumKO := pNumKO + 1;
--            PKG_Logs.StdLogAddTxt('Non trovata associazione con Tipo Misura '||pMisure(i).TIPO_MISURA||'. '||
--                                  'Le misure di tipo '||pMisure(i).TIPO_MISURA||' vengono scartate!',FALSE,gkWarning);
        END IF;

    END LOOP;
 END AddMisureGmeFgl;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGmeEle(pTipoProcesso IN VARCHAR2,
                           pOrigineMis   IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                           pCodEle       IN ELEMENTI.COD_ELEMENTO%TYPE,
                           pGstEle       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pTipEle       IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                           pTipRete      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                           pFonteDef     IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pFonteSec     IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pAggrega      IN BOOLEAN,
                           pNumIns      OUT INTEGER,
                           pNumMod      OUT INTEGER,
                           pNumKO       OUT INTEGER,
                           pMisure       IN T_MISURA_GME_ARRAY,
                           pLog      IN OUT PKG_Logs.t_StandardLog) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure GME/SMILE sull'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
  vCodMisST  TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vResult    INTEGER;
  vTipMisOK  BOOLEAN;

  vCodFte    ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
  vTipCli    ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vNomEle    ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vTipRte    TIPI_RETE.COD_TIPO_RETE%TYPE;

  vTrtEle    TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;
  vTrtEleSec TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vFattore   NUMBER;
  vTipMis    TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

BEGIN
    pNumIns := 0;
    pNumMod := 0;
    pNumKO  := 0;

    SELECT COD_TIPO_RETE, COD_TIPO_FONTE, COD_TIPO_CLIENTE, NOME_ELEMENTO
      INTO vTipRte,       vCodFte,        vTipCli,          vNomEle
      FROM (SELECT COD_ELEMENTO,E.COD_TIPO_ELEMENTO,COD_TIPO_CLIENTE,COD_TIPO_FONTE,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_RETE,
                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
              FROM ELEMENTI_DEF
             INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
             INNER JOIN TIPI_ELEMENTO T ON  T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
             WHERE COD_ELEMENTO = pCodEle
            )
      WHERE ORD = 1;

    IF pFonteDef IS NOT NULL THEN
        vCodFte := pFonteDef;
    END IF;
    IF vTipRte IS NULL THEN
        PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Rete non definito' ,FALSE,gkAnagrIncompl,pLog);
pkg_logs.tracelog('---->  2a.    tipo rete per = '|| pMisure(1).GEST_ELEM|| '    non definoto - errore '||gkAnagrIncompl,PKG_UtlGlb.gcTrace_VRB);
        RETURN;
    END IF;
    IF vCodFte IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
        ELSE
            vCodFte := PKG_Mago.gcRaggrFonteNonDisp;
        END IF;
--        IF pTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
--            PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Fonte non definito' ,FALSE,gkAnagrIncompl);
--            RETURN;
--        ELSE
--            vCodFte := PKG_Mago.gcRaggrFonteNonDisp;
--        END IF;
    END IF;
    IF vTipCli IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vTipCli := PKG_Mago.gcClientePrdNonApplic;
        ELSE
            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
        END IF;
--        IF pTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
--            PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Produttore non definito' ,FALSE,gkAnagrIncompl);
--            RETURN;
--        ELSE
--            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
--        END IF;
    END IF;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP
        IF vCodMisST   <> pMisure(i).TIPO_MISURA THEN
           vCodMisST   := pMisure(i).TIPO_MISURA;
            vTipMisOK  := GetMisuraOut(pOrigineMis,pTipEle,pMisure(i).TIPO_MISURA,vTipMis,vFattore,vFlagSplit);
            vTrtEle    := NULL;
            vTrtEleSec := NULL;
            IF vTipMisOK THEN
                vTrtEle := GetTrattamentoElemento(pCodEle,
                                                  pTipEle,
                                                  vTipMis,
                                                  vCodFte,
                                                  vTipRte,
                                                  vTipCli,
                                                  PKG_Mago.gcOrganizzazELE,
                                                  PKG_Mago.gcStatoNullo);
                IF pFonteSec IS NOT NULL THEN
                    IF pFonteDef = PKG_Mago.gcRaggrFonteNonOmog THEN
                        IF vTipMis = PKG_Mago.gcPotenzaAttScambiata THEN
                            vTrtEleSec := GetTrattamentoElemento(pCodEle,
                                                                 pTipEle,
                                                                 vTipMis,
                                                                 pFonteSec,
                                                                 vTipRte,
                                                                 vTipCli,
                                                                 PKG_Mago.gcOrganizzazELE,
                                                                 PKG_Mago.gcStatoNullo);
                        END IF;
                    END IF;
                END IF;
            --ELSE
            --    PKG_Logs.TraceLog(CHR(9)||'Warning: Non trovata associazione con Tipo Misura '||pMisure(i).TIPO_MISURA||'. '||
            --                              'Le misure di tipo '||pMisure(i).TIPO_MISURA||' vengono scartate!',PKG_UtlGlb.gcTrace_ERR);
            END IF;
        END IF;
        IF vTipMisOK THEN
            AddMisuraAcq(pTipoProcesso,
                         vTrtEle,
                         pMisure(i).DATA_MIS,
                         vTipMis,
                         ROUND(pMisure(i).VALORE * vFattore,4),
                         pMisure(i).TIPO_RETE,
                         pAggrega,
                         vResult);
            CASE vResult
                WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                ELSE NULL;
            END CASE;
            IF vTrtEleSec IS NOT NULL THEN
                AddMisuraAcq(pTipoProcesso,
                             vTrtEleSec,
                             pMisure(i).DATA_MIS,
                             vTipMis,
                             ROUND(pMisure(i).VALORE * vFattore,4),
                             pMisure(i).TIPO_RETE,
                             pAggrega,
                             vResult);
                CASE vResult
                    WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                    WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                    ELSE NULL;
                END CASE;
            END IF;
        ELSE
            pNumKO := pNumKO + 1;
        END IF;
    END LOOP;

 END AddMisureGmeEle;

 FUNCTION CompletaSqlMisure(pSql              IN VARCHAR2,
                            pOrganizzazione   IN NUMBER,
                            pStatoRete        IN NUMBER,
                            pTable            IN VARCHAR2,
                            pRaggrMis         IN CHAR,
                            pTipiFonte        IN NUMBER,
                            pTipiRete         IN NUMBER,
                            pTipiClie         IN NUMBER,
                            pGerECS           IN NUMBER,
                            pTotTipFon        IN NUMBER) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce lo statement SQL per la selezione delle misure
-----------------------------------------------------------------------------------------------------------*/
    vSql            VARCHAR2(4000);
    vOrganizzazione INTEGER := pOrganizzazione;
    vStatoRete      INTEGER := pStatoRete;
 BEGIN
    vSql := REPLACE(pSql,'#AGR#',pRaggrMis);
    vSql := REPLACE(vSql,'#TAB#',pTable);

    IF pGerECS = 1 THEN
        vOrganizzazione  := PKG_Mago.gcOrganizzazELE;
    END IF;

    IF NVL(pTipiFonte,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_FONTI#','INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE) '||
                                                 'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_FONTI#','AND BITAND(:fte,ID_RAGGR_FONTE) = ID_RAGGR_FONTE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_FONTI#',' ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_FONTI#','AND :fte <> -1 ');
    END IF;

    IF NVL(pTipiRete,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#','INNER JOIN TIPI_RETE USING(COD_TIPO_RETE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND BITAND(:rte,ID_RETE) = ID_RETE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#',' ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND :rte <> -1 ');
    END IF;

    IF NVL(pTipiClie,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#','INNER JOIN TIPI_CLIENTE USING(COD_TIPO_CLIENTE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND BITAND(:cli,ID_CLIENTE) = ID_CLIENTE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#',' ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND :cli <> -1 ');
    END IF;

    --IF pTotTipFon = PKG_Misure.cMisTot THEN
    --    vSql := REPLACE(vSql,'#FONTE1#','''0'' COD_RAGGR_FONTE');
    --    vSql := REPLACE(vSql,'#FONTE2#',' ');
    --ELSE
    --    vSql := REPLACE(vSql,'#FONTE1#','COD_RAGGR_FONTE');
    --    vSql := REPLACE(vSql,'#FONTE2#',',COD_RAGGR_FONTE');
    --END IF;

    IF pTable = 'ACQUISITE' THEN
        vSql := REPLACE(vSql,'#FILT_ELEM_NULL#','');
    ELSE
        vSql := REPLACE(vSql,'#FILT_ELEM_NULL#','AND B.COD_ELEMENTO IS NOT NULL');
    END IF;

    IF vOrganizzazione IS NOT NULL AND pStatoRete IS NOT NULL THEN
--        IF PKG_Mago.IsMagoSTM AND vOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
--            vStatoRete  := PKG_Mago.gcStatoAttuale;
--        END IF;
        CASE vOrganizzazione
            WHEN PKG_Mago.gcOrganizzazELE THEN
                    vSql := REPLACE(vSql,'#GER#','IMP');
                    CASE vStatoRete
                        WHEN PKG_Mago.gcStatoAttuale THEN
                            vSql := REPLACE(vSql,'#STA#','_SA');
                        WHEN PKG_Mago.gcStatoNormale THEN
                            vSql := REPLACE(vSql,'#STA#','_SN');
                    END CASE;
            WHEN PKG_Mago.gcOrganizzazGEO THEN
                    vSql := REPLACE(vSql,'#GER#','GEO');
            WHEN PKG_Mago.gcOrganizzazAMM THEN
                    vSql := REPLACE(vSql,'#GER#','AMM');
        END CASE;
        vSql := REPLACE(vSql,'#STA#','');
    END IF;

    RETURN vSql;

 END CompletaSqlMisure;

-- ----------------------------------------------------------------------------------------------------------

-- FUNCTION GetTabMisure          (pCodElemento      IN ELEMENTI.COD_ELEMENTO%TYPE,
--                                 pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE)
--                          RETURN REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE AS
--/*-----------------------------------------------------------------------------------------------------------
--    determina su quale/i tabelle misure selezionare le curve per l'elemento e per i tipi misura definiti in GTTD
-------------------------------------------------------------------------------------------------------------*/
--    vTabMisure      REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;
-- BEGIN
--    IF pTipMisura IS NULL THEN
--        SELECT SUM(TAB_MISURE)
--          INTO vTabMisure
--          FROM (SELECT DISTINCT TAB_MISURE
--                 FROM REL_ELEMENTO_TIPMIS
--                WHERE COD_ELEMENTO = pCodElemento
--                  AND INSTR(gListaTipoMisura,'|'||COD_TIPO_MISURA||'|') > 0   
--               );
--        IF vTabMisure > PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate THEN
--            vTabMisure := PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate;
--        END IF;
--    ELSE
--        SELECT MAX(TAB_MISURE)
--          INTO vTabMisure
--         FROM REL_ELEMENTO_TIPMIS
--        WHERE COD_ELEMENTO = pCodElemento AND COD_TIPO_MISURA = pTipMisura;
--        IF vTabMisure IS NULL THEN
--            SELECT MAX(TIPO_AGGREGAZIONE)
--              INTO vTabMisure
--             FROM TRATTAMENTO_ELEMENTI
--            WHERE COD_ELEMENTO = pCodElemento AND COD_TIPO_MISURA = pTipMisura;
--            IF vTabMisure = 0 THEN
--                vTabMisure := PKG_Mago.gcTabAcquisite;
--            ELSE
--                vTabMisure := PKG_Mago.gcTabAggregate;
--            END IF;
--        END IF;
--    END IF;
--    RETURN vTabMisure;
-- EXCEPTION
--    WHEN NO_DATA_FOUND THEN RETURN 3;  -- 3=default acquisite + aggregate
----    WHEN OTHERS THEN RAISE;
-- END GetTabMisure;

 FUNCTION GetTabMisure          (pCodElemento      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pMisStatica       IN BOOLEAN,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pGerECS           IN NUMBER,
                                 pTipoAggr1        IN INTEGER,
                                 pTipoAggr2        IN INTEGER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE)
                          RETURN REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    determina su quale/i tabelle misure selezionare le curve per l'elemento e per i tipi misura definiti in GTTD
-----------------------------------------------------------------------------------------------------------*/
    vTabMisure REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE := NULL;
    vNum       INTEGER;
    vSql       VARCHAR2(800);
    vSqlStd    VARCHAR2(250) := 'SELECT 1 '                                               ||
                                  'FROM MISURE_#TAB# M '                                  ||
                                 'WHERE M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM ' ||
                                   'AND M.DATA BETWEEN :dtDa AND :dtA ';
    vSqlSta    VARCHAR2(250) := 'SELECT 1 '                                               ||
                                  'FROM MISURE_#TAB#_STATICHE M '                         ||
                                 'WHERE M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM ' ||
                                   'AND DATA_DISATTIVAZIONE >= :DtDa '                    ||
                                   'AND DATA_ATTIVAZIONE    <= :Dta ';
    vSqlMis    VARCHAR2(800) := 'SELECT COUNT(*) '                                        ||
                                  'FROM TRATTAMENTO_ELEMENTI T '                          ||
                                  '#JOIN_TIPO_FONTI# '                                    ||
                                  '#JOIN_TIPO_RETE# '                                     ||
                                  '#JOIN_TIPO_CLIENTE# '                                  ||
                                 'WHERE COD_ELEMENTO = :ce '                              ||
                                   'AND COD_TIPO_MISURA = :tm '                           ||
                                   'AND TIPO_AGGREGAZIONE = :ta '                         ||
                                   '#FILTRO_TIPO_FONTI# '                                 ||
                                   '#FILTRO_TIPO_RETE# '                                  ||
                                   '#FILTRO_TIPO_CLIENTE# '                               ||
                                   'AND EXISTS (#SQLMIS#)';
 BEGIN
 
    IF pTipMisura IS NULL THEN
        BEGIN 
            SELECT SUM(TAB_MISURE)
              INTO vTabMisure
              FROM (SELECT DISTINCT TAB_MISURE
                     FROM REL_ELEMENTO_TIPMIS
                    INNER JOIN (SELECT ALF1 COD_TIPO_MISURA
                                  FROM GTTD_VALORI_TEMP 
                                 WHERE TIP = PKG_Mago.gcTmpTipMisKey
                               ) USING(COD_TIPO_MISURA)
                    WHERE COD_ELEMENTO = pCodElemento
                   );
            IF vTabMisure > PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate THEN
                vTabMisure := PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate;
            END IF;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN NULL;
        END;
    ELSE

        IF NOT pMisStatica THEN
            vSqlMis := REPLACE(vSqlMis,'#SQLMIS#',vSqlStd); -- misure standard
        ELSE
            vSqlMis := REPLACE(vSqlMis,'#SQLMIS#',vSqlSta); -- misure statiche
        END IF;

        vSql := CompletaSqlMisure(vSqlMis,NULL,NULL,'ACQUISITE',NULL,
                                  pTipiFonte,pTipiRete,pTipiClie,pGerECS,NULL);
        EXECUTE IMMEDIATE vSql INTO vNum USING pCodElemento,pTipMisura,pTipoAggr1,
                                               pTipiFonte,pTipiRete,pTipiClie,pDataDa,pDataA;
        IF vNum <> 0 THEN 
            vTabMisure := NVL(vTabMisure,0) + PKG_Mago.gcTabAcquisite; -- misure acquisite
        END IF;

        vSql := CompletaSqlMisure(vSqlMis,NULL,NULL,'AGGREGATE',NULL,
                                  pTipiFonte,pTipiRete,pTipiClie,pGerECS,NULL);
        EXECUTE IMMEDIATE vSql INTO vNum USING pCodElemento,pTipMisura,pTipoAggr2,
                                               pTipiFonte,pTipiRete,pTipiClie,pDataDa,pDataA;
        IF vNum <> 0 THEN 
            vTabMisure := NVL(vTabMisure,0) + PKG_Mago.gcTabAggregate; -- misure aggregate
        END IF;

    END IF;

    IF vTabMisure IS NULL THEN
        vTabMisure := PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate;  -- 3=default acquisite + aggregate
    END IF;

    RETURN vTabMisure;
 
 END GetTabMisure;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetTrattamentoElemento(pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pTipEle        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                pTipFon        IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                pTipCli        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                pOrganizzaz    IN NUMBER,
                                pTipoAggreg    IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce il trattamento elemento - Se non presente lo crea
-----------------------------------------------------------------------------------------------------------*/

    vTrtEle         TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;
    vTipEle         TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipEle;
    vGerEcs         TIPI_ELEMENTO.GER_ECS%TYPE;
--    vTipoAggreg     NUMBER := pTipoAggreg;
    vOrganizzazione NUMBER(1);

         PROCEDURE SetRel_Ele_TipMis (pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                      pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                      pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                      vTipTabMisure  IN INTEGER) AS
        /*---------------------------------------------------------------------------------------------------
            Inserisce/Modifica la relazione Elemento / Tipo Misura indicando se le misure saranno reperibili
            in tabella MISURE_ACQUISITE, i tablella MISURE_AGGREGATE o in entrambe e il tipo rete interessato
        ---------------------------------------------------------------------------------------------------*/
            vTabMisure    REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;
            vReteAT       REL_ELEMENTO_TIPMIS.RETE_AT%TYPE := 0;
            vReteMT       REL_ELEMENTO_TIPMIS.RETE_MT%TYPE := 0;
            vReteBT       REL_ELEMENTO_TIPMIS.RETE_BT%TYPE := 0;
            vres NUMBER;
         BEGIN
            CASE pTipRet
                WHEN PKG_Mago.gcTipReteAT THEN vReteAT := 1;
                WHEN PKG_Mago.gcTipReteMT THEN vReteMT := 1;
                WHEN PKG_Mago.gcTipReteBT THEN vReteBT := 1;
            END CASE;
            BEGIN
                SELECT TAB_MISURE
                  INTO vTabMisure
                  FROM REL_ELEMENTO_TIPMIS
                 WHERE COD_ELEMENTO = pCodEle
                   AND COD_TIPO_MISURA = pTipMis;
                IF NVL(BITAND(vTabMisure,vTipTabMisure),-1) <> vTipTabMisure THEN
                    UPDATE REL_ELEMENTO_TIPMIS SET TAB_MISURE = TAB_MISURE + vTipTabMisure
                     WHERE COD_ELEMENTO = pCodEle
                       AND COD_TIPO_MISURA = pTipMis;
                END IF;
                CASE pTipRet
                    WHEN PKG_Mago.gcTipReteAT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_AT = vReteAT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_AT <> vReteAT;
                    WHEN PKG_Mago.gcTipReteMT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_MT = vReteMT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_MT <> vReteMT;
                    WHEN PKG_Mago.gcTipReteBT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_BT = vReteBT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_BT <> vReteBT;
                END CASE;
            EXCEPTION
             WHEN NO_DATA_FOUND THEN
                 INSERT INTO REL_ELEMENTO_TIPMIS (COD_ELEMENTO, COD_TIPO_MISURA, TAB_MISURE,    RETE_AT, RETE_MT, RETE_BT )
                                          VALUES (pCodEle,      pTipMis,         vTipTabMisure, vReteAT, vReteMT, vReteBT );
            END;
         END SetRel_Ele_TipMis;
BEGIN

    IF vTipEle IS NULL THEN
        SELECT COD_TIPO_ELEMENTO
          INTO vTipEle
          FROM ELEMENTI
         WHERE COD_ELEMENTO = pCodEle;
    END IF;

--    IF pTipoAggreg <> PKG_Mago.gcStatoNullo THEN
--        -- eseguo forzatura per evitare il proliferare di misure aggregate
--        -- a livello di gerarchie di Stato normale e Attuale:
--        -- per elementi in gerarchia di CS uso solo lo stato normale
--        SELECT GER_ECS,
--               CASE GER_ECS
--                    WHEN 1 THEN PKG_Mago.gcStatoNormale
--                    ELSE        vTipoAggreg
--               END
--          INTO vGerEcs, vTipoAggreg
--          FROM TIPI_ELEMENTO
--         WHERE COD_TIPO_ELEMENTO = vTipEle;
--    ELSE
--        SELECT GER_ECS
--          INTO vGerEcs
--          FROM TIPI_ELEMENTO
--         WHERE COD_TIPO_ELEMENTO = vTipEle;
--    END IF;

    SELECT GER_ECS
      INTO vGerEcs
      FROM TIPI_ELEMENTO
     WHERE COD_TIPO_ELEMENTO = vTipEle;
    IF vGerEcs = 1 THEN
        -- eseguo forzatura per evitare il proliferare di misure aggregate
        -- a livello di gerarchie di Stato normale e Attuale:
        -- per elementi in gerarchia di CS uso solo l'organizazzione Eletrtrica
        vOrganizzazione := PKG_Mago.gcOrganizzazELE;
    ELSE
        vOrganizzazione := pOrganizzaz;
    END IF;

    BEGIN
        SELECT COD_TRATTAMENTO_ELEM
          INTO vTrtEle
          FROM TRATTAMENTO_ELEMENTI
         WHERE COD_ELEMENTO = pCodEle
           AND COD_TIPO_MISURA = pTipMis
           AND COD_TIPO_FONTE = pTipFon
           AND COD_TIPO_RETE = pTipRet
           AND COD_TIPO_CLIENTE = pTipCli
           AND ORGANIZZAZIONE = pOrganizzaz
           AND TIPO_AGGREGAZIONE = pTipoAggreg;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO TRATTAMENTO_ELEMENTI (COD_ELEMENTO,
                                              COD_TIPO_ELEMENTO,
                                              COD_TIPO_MISURA,
                                              COD_TIPO_FONTE,
                                              COD_TIPO_RETE,
                                              COD_TIPO_CLIENTE,
                                              ORGANIZZAZIONE,
                                              TIPO_AGGREGAZIONE  )
                                      VALUES (pCodEle,
                                              vTipEle,
                                              pTipMis,
                                              pTipFon,
                                              pTipRet,
                                              pTipCli,
                                              pOrganizzaz,
                                              pTipoAggreg)
                                   RETURNING COD_TRATTAMENTO_ELEM INTO vTrtEle;
        WHEN OTHERS THEN RAISE;
    END;
    IF pTipoAggreg = 0 THEN
        SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAcquisite);
    ELSE
        SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAggregate);
    END IF;
    RETURN vTrtEle;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt('Codici         : '||'Ele:'||NVL(TO_CHAR(pCodEle),'<null>')||'-'||
                                                    'Tip:'||NVL(pTipEle,'<null>')||'-'||
                                                    'Mis:'||NVL(pTipMis,'<null>')||'-'||
                                                    'Fon:'||NVL(pTipFon,'<null>')||'-'||
                                                    'Ret:'||NVL(pTipRet,'<null>')||'-'||
                                                    'Cli:'||NVL(pTipCli,'<null>')||'-'||
                                                    'Org:'||NVL(TO_CHAR(pOrganizzaz),'<null>')||'('||NVL(TO_CHAR(vOrganizzazione),'<null>')||')-'||
                                                    'Agr:'||NVL(TO_CHAR(pTipoAggreg),'<null>') /*||'('||NVL(TO_CHAR(vTipoAggreg),'<null>')||')'*/
                              ,TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetTrattamentoElemento;

---- ----------------------------------------------------------------------------------------------------------

FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE,pTemporalAggreg IN NUMBER) RETURN DATE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la pCurrentDate 'arrotondata' temporalmente a partire dalla data di partenza (DateFrom)
    in base alla aggregazione temporale (pTemporalAggreg) richiesta
-----------------------------------------------------------------------------------------------------------*/
    vUtcTs         TIMESTAMP;
    vUtcCurr       TIMESTAMP;
    vDiffDate      NUMBER;
    vTz            INTEGER; -- := PKG_Date_Java.getTimeOffset(pCurrentDate);

 BEGIN

    IF   pDateFrom       IS NULL
      OR pCurrentDate    IS NULL
      OR pTemporalAggreg IS NULL THEN
      RETURN NULL;
    END IF;

    BEGIN
        vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    EXCEPTION
        WHEN OTHERS THEN
             vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate + 1/24,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    END;

    --vUtcTs         := SYS_EXTRACT_UTC(TO_TIMESTAMP(TRUNC(pDateFrom)));
    -- set pDateFrom value into vUtcTs variable
    vUtcTs    := FROM_TZ(CAST(pDateFrom AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    vUtcCurr  := FROM_TZ(CAST(pCurrentDate AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    SELECT EXTRACT (DAY FROM (vUtcCurr-vUtcTs))*24*60+
           EXTRACT (HOUR FROM (vUtcCurr-vUtcTs))*60+
           EXTRACT (MINUTE FROM (vUtcCurr-vUtcTs))
      INTO vDiffDate
      FROM DUAL;

    vDiffDate := TRUNC( vDiffDate / pTemporalAggreg );

    RETURN CAST(vUtcTs AS DATE) + (vDiffDate*pTemporalAggreg/60/24) + (vTz/24);

 EXCEPTION
    WHEN OTHERS THEN
        IF SQLCODE = -01878 THEN
            RETURN NULL;
        ELSE
            RAISE;
        END IF;

 END GetRoundDate;

 ----------------------------------------------------------------------------------------------------------

-- FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE, pTemporalAggreg IN NUMBER) RETURN DATE AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna la pCurrentDate 'arrotondata' temporalmente a partire dalla data di partenza (DateFrom)
--    in base alla aggregazione temporale (pTemporalAggreg) richiesta
-------------------------------------------------------------------------------------------------------------*/
--    cDataStart      CONSTANT DATE := TO_DATE('1970-01-01','yyyy-mm-dd');
--    vDateFrom       NUMBER := TO_NUMBER(pDateFrom    - cDataStart) * (24 * 60 * 60 * 1000);
--    vCurrentDate    NUMBER := TO_NUMBER(pCurrentDate - cDataStart) * (24 * 60 * 60 * 1000);
-- BEGIN
--    IF   pDateFrom       IS NULL
--      OR pCurrentDate    IS NULL
--      OR pTemporalAggreg IS NULL THEN
--      RETURN NULL;
--    END IF;
--    RETURN cDataStart+((vCurrentDate - MOD(vCurrentDate - vDateFrom, pTemporalAggreg*60*1000))/1000)/60/60/24;

-- END GetRoundDate;

 ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetProduttoriGME     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodCo            IN VARCHAR2,
                                 pTipiFonte        IN VARCHAR2,
                                 pDate             IN DATE DEFAULT SYSDATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori per i quali devono essere generate le misure GME per TEST
-----------------------------------------------------------------------------------------------------------*/

    vFlgNull    NUMBER(1) := -1;
    cTipFon     CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPFON';
 BEGIN
    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = cTipFon;
    PKG_Mago.TrattaListaCodici(pTipiFonte, cTipFon, vFlgNull);
    OPEN pRefCurs FOR SELECT NVL(S1.COD_GEST_ELEMENTO,S2.COD_GEST_ELEMENTO) COD_CO,
                             ID_ELEMENTO           COD_POD_CLIENTE,
                             E.COD_GEST_ELEMENTO   COD_GEST_AUI,
                             ID_ELEMENTO           COD_ENELTEL,
                             A.COD_RAGGR_FONTE       FONTE,
                             A.POTENZA_INSTALLATA,
                             COD_CITTA
                        FROM (SELECT A.COD_ELEMENTO, A.COD_RAGGR_FONTE, SUM(A.POTENZA_INSTALLATA) POTENZA_INSTALLATA
                                FROM (SELECT CASE E.COD_TIPO_ELEMENTO
                                                WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteAT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                                WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteMT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                                WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteBT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                             END COD_ELEMENTO,
                                             COD_RAGGR_FONTE,
                                             NVL(POTENZA_INSTALLATA,0) * NVL(FATTORE,1) POTENZA_INSTALLATA
                                        FROM ELEMENTI E
                                       INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                                       INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE)
                                       WHERE pDate BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                         AND E.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT)
                                      ) A
                                GROUP BY A.COD_ELEMENTO, COD_RAGGR_FONTE
                             ) A
                       INNER JOIN ELEMENTI      E  ON A.COD_ELEMENTO  = E.COD_ELEMENTO
                       INNER JOIN ELEMENTI_DEF  D  ON A.COD_ELEMENTO  = D.COD_ELEMENTO
                       INNER JOIN GTTD_VALORI_TEMP ON ALF1 = COD_RAGGR_FONTE
                       LEFT OUTER JOIN ELEMENTI S1 ON S1.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcEsercizio,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN ELEMENTI S2 ON S2.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcEsercizio,pDate,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN ELEMENTI C  ON C.COD_ELEMENTO  = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcComune,pDate,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN METEO_REL_ISTAT ON COD_ISTAT   = NVL(SUBSTR(C.COD_GEST_ELEMENTO,INSTR(C.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1),C.COD_GEST_ELEMENTO)
                      WHERE NVL(S1.COD_GEST_ELEMENTO,S2.COD_GEST_ELEMENTO) = pCodCo
                        AND COD_CITTA IS NOT NULL
                      ORDER BY E.COD_GEST_ELEMENTO, COD_RAGGR_FONTE;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.TraceLog(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END GetProduttoriGME;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GMEcompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                pFinishTimestamp IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
     Riceve l'indicazione di fine caricamento misure da GME
     Lancia la richiesta di elaborazione aggregate
-----------------------------------------------------------------------------------------------------------*/
  vLog  PKG_Logs.t_StandardLog;
  vTxt  VARCHAR2(300);
BEGIN
   vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_Meteo.GMEcompleted',
                               pDataRif    => pFinishTimestamp);

   DBMS_SCHEDULER.RUN_JOB('MAGO_INS_REQ_AGG_GME',FALSE);

   OPEN pRefCurs FOR  SELECT 'OK' MESSAGE FROM DUAL;

   PKG_Logs.StdLogPrint (vLog);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vTxt := SQLERRM;
         PKG_Logs.StdLogAddTxt(vTxt||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR  SELECT vTxt MESSAGE FROM DUAL;
         RETURN;
END GMEcompleted;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGme(pMisure  IN T_MISURA_GME_ARRAY,
                        pAggrega IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure provenienti da GME/SMILE
-----------------------------------------------------------------------------------------------------------*/

  vLog           PKG_Logs.t_StandardLog;

  vMisureSplit   T_MISURA_GME_ARRAY := T_MISURA_GME_ARRAY();
  vMisureNoSplit T_MISURA_GME_ARRAY := T_MISURA_GME_ARRAY();


  vNumIns        INTEGER := 0;
  vNumMod        INTEGER := 0;
  vNumKO         INTEGER := 0;
  vNumKO2        INTEGER := 0;

  vOrigineMis    TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'GME_';

  vNum           INTEGER;

  vFntNonOmog    BOOLEAN := FALSE;
  vElemPadre     ELEMENTI.COD_ELEMENTO%TYPE;
  vCodFte        ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
  vFteSolare     NUMBER(1);
  vFteEolica     NUMBER(1);
  vFteIdraulicA  NUMBER(1);
  vFteTermica    NUMBER(1);
  vFteRinnovab   NUMBER(1);
  vFteConvenz    NUMBER(1);

  vFonteSec      ELEMENTI_DEF.COD_TIPO_FONTE%TYPE := NULL;

  vCodEle        ELEMENTI.COD_ELEMENTO%TYPE := 0;
  vNomEle        ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vIdElemento    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
  vTipEle        ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE := NULL;
  vTipCli        ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE := NULL;

  vPrevMisIn     TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_IN%TYPE := '_';
  vCodTipMisOut  TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit     TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;
  vFattore       NUMBER;

  FUNCTION f_TipEle RETURN VARCHAR2 AS
    BEGIN
        IF vTipEle IS NOT NULL THEN
            RETURN vTipEle;
        ELSE
            CASE pMisure(1).TIPO_RETE
                WHEN PKG_Mago.gcTipReteAT THEN RETURN PKG_Mago.gcSecondarioDiTrasf;
                WHEN PKG_Mago.gcTipReteMT THEN
                        CASE SUBSTR(pMisure(1).GEST_ELEM,1,2)
                           WHEN 'IT'   /* si lo so � una forzatura terribile ma serve solo ai fini di riportare l'info sui log */
                                THEN RETURN 'POD';
                                ELSE  RETURN PKG_Mago.gcClienteMT;
                         END CASE;
                WHEN PKG_Mago.gcTipReteMT THEN RETURN PKG_Mago.gcTrasformMtBt;
                ELSE RETURN NULL;
            END CASE;
        END IF;
        RETURN NULL;
    END f_TipEle;

  FUNCTION f_CheckCodInput RETURN BOOLEAN AS
    BEGIN

        BEGIN
            SELECT COD_ELEMENTO, COD_GEST_ELEMENTO,  NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE
              INTO vCodEle,      vIdElemento,        vNomEle,       vTipEle,           vTipCli
              FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_CLIENTE,
                           ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                      FROM ELEMENTI E
                     INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                     WHERE COD_GEST_ELEMENTO = vIdElemento
                   )
             WHERE ORD = 1;
            PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE
                      INTO vCodEle,      vIdElemento,       vNomEle,       vTipEle,           vTipCli
                      FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_CLIENTE,
                                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                             WHERE RIF_ELEMENTO = vIdElemento
                           )
                     WHERE ORD = 1;
                    PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                          PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                          PKG_Logs.StdLogAddTxt('Non presente in anagrafica',FALSE,gkAnagrNonPres,vLog);
                          RETURN FALSE;
                    WHEN OTHERS THEN
                          PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                          IF SQLCODE = -01422 THEN
                              PKG_Logs.StdLogAddTxt('Associato a piu'' di un Elemento',FALSE,gkAnagrIncong,vLog);
                              RETURN FALSE;
                          ELSE
                             RAISE;
                          END IF;
                END;
        END;
        IF vTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
            SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO
              INTO vIdElemento,       vNomEle,       vTipEle
              FROM (SELECT ID_ELEMENTO COD_GEST_ELEMENTO,
                           ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                      FROM ELEMENTI_DEF
                     INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                     WHERE COD_GEST_ELEMENTO = vIdElemento
                    ) P
             INNER JOIN ELEMENTI      E USING(COD_GEST_ELEMENTO)
             INNER JOIN ELEMENTI_DEF  D USING(COD_ELEMENTO)
             WHERE ORD = 1;
        END IF;
        RETURN TRUE;
    END f_CheckCodInput;

  PROCEDURE p_Elabora(pMisure   IN T_MISURA_GME_ARRAY,
                      pAggrega  IN BOOLEAN,
                      pMisSplit IN BOOLEAN) AS
    BEGIN
        CASE

            WHEN (vTipCli = PKG_Mago.gcCliente)      -- Cliente (non produttore)
              OR (NOT pMisSplit)                THEN -- Misura da NON splittare
                 vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
                 AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                 vTipEle, pMisure(1).TIPO_RETE, vCodFte, NULL, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);

            WHEN vTipEle IN (PKG_Mago.gcClienteAT,
                             PKG_Mago.gcClienteMT,
                             PKG_Mago.gcClienteBT,
                             PKG_Mago.gcTrasformMtBt)  THEN   -- Produttori/ Trasformatori MT/BT
                BEGIN
                    SELECT TIPO_FONTE,NUM_FONTI,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteSolareID)   =PKG_Mago.gcRaggrFonteSolareID    THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteEolicaID)   =PKG_Mago.gcRaggrFonteEolicaID    THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteIdraulicaID)=PKG_Mago.gcRaggrFonteIdraulicaID THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteTermicaID)  =PKG_Mago.gcRaggrFonteTermicaID   THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteRinnovabID) =PKG_Mago.gcRaggrFonteRinnovabID  THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteConvenzID)  =PKG_Mago.gcRaggrFonteConvenzID   THEN 1 ELSE 0 END
                      INTO vCodFte, vNum, vFteSolare, vFteEolica, vFteIdraulica, vFteTermica, vFteRinnovab, vFteConvenz
                      FROM (SELECT CASE
                                     WHEN MAX(COD_TIPO_FONTE) IS NULL OR
                                          MIN(COD_TIPO_FONTE) IS NULL THEN NULL
                                     WHEN MAX(COD_TIPO_FONTE) = MIN(COD_TIPO_FONTE)
                                         THEN MAX(COD_TIPO_FONTE)
                                         ELSE PKG_Mago.gcRaggrFonteNonOmog
                                  END TIPO_FONTE,
                                  COUNT(*) NUM_FONTI,
                                  BITMAP_FONTI
                             FROM (SELECT COD_TIPO_FONTE,
                                          SUM(ID_RAGGR_FONTE) OVER (PARTITION BY 1) BITMAP_FONTI
                                     FROM (SELECT DISTINCT COD_TIPO_FONTE
                                             FROM (SELECT COD_TIPO_FONTE,
                                                          ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                                     FROM ELEMENTI_DEF A
                                                    WHERE ID_ELEMENTO  = vIdElemento   --  codice gestionale cliente
                                                  )
                                            WHERE ORD = 1
                                          )
                                    INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE)
                                    INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE)
                                  )
                             GROUP BY BITMAP_FONTI
                           );
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN vCodFte := NULL;
                                            vFteSolare := 0;
                                            vFteEolica := 0;
                                            vFteIdraulica := 0;
                                            vFteTermica := 0;
                                            vFteRinnovab := 0;
                                            vFteConvenz := 0;
                    WHEN OTHERS THEN RAISE;
                END;
                IF vCodFte = PKG_Mago.gcRaggrFonteNonOmog THEN
                    PKG_Logs.StdLogAddTxt('I Generatori associati non hanno fonte omogenea',FALSE,gkWarning,vLog);
                ELSE
                    IF vCodFte IS NULL THEN
                        IF vNum = 0 THEN
                            PKG_Logs.StdLogAddTxt('Nessun Generatore associato',FALSE,gkWarning,vLog);
                        ELSE
                            PKG_Logs.StdLogAddTxt('Il Tipo fonte per uno o piu'' dei Generatori associati non e'' valorizzato',FALSE,gkWarning,vLog);
                        END IF;
                        vCodFte := PKG_Mago.gcRaggrFonteNonOmog;
                    END IF;
                END IF;

                vFonteSec := NULL;

                IF vFlagSplit = 0 THEN
                    vLog.STORE_ON_FILE := TRUE;
                    vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
                    PKG_Logs.StdLogInfo('Le misure ' || pMisure(1).TIPO_MISURA || ' non sono splittabili e sono attribuite a '||PKG_Elementi.DecodeTipElem(vTipEle),vLog);
                    AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, vFonteSec, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);
                ELSIF vCodFte = PKG_Mago.gcRaggrFonteNonOmog THEN
                    IF (vFteSolare + vFteEolica) > 0 THEN
                        -- solare e/o eolico
                        CASE (vFteIdraulica + vFteTermica + vFteRinnovab + vFteConvenz)
                            WHEN 0 THEN
                                --nessuna altra fonte sola altra fonte
                                NULL;
                            WHEN 1 THEN
                                --una sola altra fonte
                                CASE
                                    WHEN vFteIdraulica = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteIdraulica);
                                    WHEN vFteTermica   = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteTermica);
                                    WHEN vFteRinnovab  = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteRinnovab);
                                    WHEN vFteConvenz   = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteConvenz);
                                END CASE;
                            ELSE
                                PKG_Logs.StdLogAddTxt('Produttore con fonti alternative > 1',FALSE,gkWarning,vLog);
                        END CASE;
                    END IF;
                    vLog.STORE_ON_FILE := TRUE;
                    PKG_Logs.StdLogInfo('Le misure sono attribuite a '||PKG_Elementi.DecodeTipElem(vTipEle),vLog);
                    AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, vFonteSec, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);
                ELSE
                    SELECT COUNT(*)
                      INTO vNum
                      FROM ELEMENTI_DEF
                     WHERE ID_ELEMENTO = vIdElemento
                       AND POTENZA_INSTALLATA IS NULL;
                    IF vNum > 0 THEN
                        vLog.STORE_ON_FILE := TRUE;
                        PKG_Logs.StdLogAddTxt('La Potenza Installata per '||vNum||' dei Generatori associati non e'' valorizzata',FALSE,gkAnagrIncompl,vLog);
                        PKG_Logs.StdLogPrint (vLog);
                        RETURN;
                    END IF;
                    AddMisureGmeFgl('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);
                END IF;

            WHEN vTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT)  THEN   -- Generatori
                AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                vTipEle, pMisure(1).TIPO_RETE, NULL, NULL, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);

            ELSE   -- Altri elementi
                AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                vTipEle, pMisure(1).TIPO_RETE, NULL, NULL, pAggrega, vNumIns, vNumMod, vNumKO, pMisure, vLog);
        END CASE;
    END;

BEGIN

    IF pMisure.LAST IS NULL THEN
        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_Misure.AddMisureGme',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    vOrigineMis := vOrigineMis||pMisure(1).TIPO_RETE;
    vIdElemento := pMisure(1).GEST_ELEM;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME||'_'||pMisure(1).TIPO_RETE,
                                pFunzione     => 'PKG_Misure.AddMisureGme',
                                pStoreOnFile  => FALSE,
                                pCodice       => vIdElemento,
                                pTipo         => f_TipEle,
                                pDataRif      => pMisure(pMisure.FIRST).DATA_MIS,
                                pDataRif_fine => pMisure(pMisure.LAST).DATA_MIS);

    SELECT COUNT(*) INTO vNum FROM TIPI_RETE WHERE COD_TIPO_RETE = pMisure(1).TIPO_RETE;
    IF vNum = 0 THEN
        vLog.DESCRIZIONE := SUBSTR('Tipo Rete '||NVL(pMisure(1).TIPO_RETE,'<null>')||' non definito',1,100);
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    IF NOT f_CheckCodInput THEN
        PKG_Logs.StdLogPrint (vLog);
        RETURN;
    END IF;

    vLog.DESCRIZIONE := 'Origine Misura: '||vOrigineMis;
    vLog.CLASSE      := PKG_Mago.gcJobClassMIS||'.'||vOrigineMis;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP
        IF vPrevMisIn <> pMisure(i).TIPO_MISURA THEN
            vPrevMisIn := pMisure(i).TIPO_MISURA;
            BEGIN
                SELECT FLG_SPLIT
                  INTO vFlagSplit
                  FROM TIPI_MISURA_CONV_ORIG
                 WHERE ORIGINE = vOrigineMis
                   AND COD_TIPO_MISURA_IN = pMisure(i).TIPO_MISURA;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    vFlagSplit := NULL;
            END;
        END IF;
        CASE
            WHEN vFlagSplit IS NULL THEN vNumKO2 := vNumKO2 + 1;
            WHEN vFlagSplit = 1     THEN vMisureSplit.EXTEND;
                                         vMisureSplit(vMisureSplit.LAST) := pMisure(i);
            WHEN vFlagSplit = 0     THEN vMisureNoSplit.EXTEND;
                                         vMisureNoSplit(vMisureNoSplit.LAST) := pMisure(i);
        END CASE;
    END LOOP;

    IF vMisureSplit.LAST IS NOT NULL THEN
        p_Elabora(vMisureSplit,pAggrega,TRUE);
    END IF;

    IF vMisureNoSplit.LAST IS NOT NULL THEN
        p_Elabora(vMisureNoSplit,pAggrega,FALSE);
    END IF;

    vNumKO := vNumKO + vNumKO2;

    PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
    PKG_Logs.StdLogInfo('Misure inserite/modificate '||TO_CHAR(vNumIns + vNumMod),vLog);
    PKG_Logs.StdLogAddTxt('Misure Ricevute     : ' ||pMisure.LAST,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Inserite     : ' ||vNumIns,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Modificate   : ' ||vNumMod,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure gia'' presenti: '||TO_CHAR(pMisure.LAST - vNumIns - vNumMod - vNumKO),TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Scartate     : ' ||vNumKO,TRUE,NULL,vLog);

    PKG_Logs.StdLogPrint(vLog);

    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END AddMisureGme;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisure            (pMisure        IN T_MISURA_GME_ARRAY) AS
  BEGIN
        AddMisureGme(pMisure);
  END AddMisure;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE CalcMisureStatiche   (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE)AS
/*-----------------------------------------------------------------------------------------------------------
    Sottomette il calcolo delle misure statiche previste
-----------------------------------------------------------------------------------------------------------*/
  BEGIN
        AddMisurePI (pData,pOrganizzazione,pStato,pAggrega);
        AddMisureNRI(pData,pOrganizzazione,pStato,pAggrega);
        --AddMisurePC (pData);
  --EXCEPTION
  --      WHEN OTHERS THEN RAISE;
  END CalcMisureStatiche;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisurePI          (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE,
                                 pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure di potenza installata per i generatori AT, MT, BT presenti
-----------------------------------------------------------------------------------------------------------*/

     vLog          PKG_Logs.t_StandardLog;

     vResult       INTEGER;
     vNumIns       INTEGER := 0;
     vNumMod       INTEGER := 0;
     vNumKO        INTEGER := 0;
     vCount        NUMBER  := 0;

     vTipoMisura   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE := PKG_Mago.gcPotenzaInstallata;
     vTipoGen      ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;

     vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'POT_INST';
     vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;
     vCodTipoRete  TIPI_RETE.COD_TIPO_RETE%TYPE;

     vCodEleGen    ELEMENTI_DEF.COD_ELEMENTO%TYPE;
     vEleNome      ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
     vCodTipoElem  ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
     vCodTipoFonte ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
     vCodTipoClie  ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
     vPotInst      ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;

     vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

     vCodTipoMis   TIPI_MISURA.COD_TIPO_MISURA%TYPE;
     vFattore      NUMBER;

     vValore       MISURE_ACQUISITE.VALORE%TYPE;

     CURSOR cGen IS  SELECT COD_TIPO_ELEMENTO
                       FROM TIPI_ELEMENTO
                      WHERE COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,
                                                  PKG_Mago.gcGeneratoreMT,
                                                  PKG_Mago.gcGeneratoreBT,
                                                  PKG_Mago.gcTrasformMtBtDett);

     CURSOR cPot(vData DATE, pTipEleGen ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE) IS
                     SELECT A.COD_ELEMENTO,
                            NOME_ELEMENTO,
                            A.COD_TIPO_FONTE,
                            COD_TIPO_CLIENTE,
                            A.POTENZA_INSTALLATA
                       FROM (SELECT PKG_Elementi.GetElementoPadre(COD_ELEMENTO,
                                                                  CASE E.COD_TIPO_ELEMENTO
                                                                    WHEN PKG_Mago.gcGeneratoreAT     THEN PKG_Mago.gcClienteAT
                                                                    WHEN PKG_Mago.gcGeneratoreMT     THEN PKG_Mago.gcClienteMT
                                                                    WHEN PKG_Mago.gcGeneratoreBT     THEN PKG_Mago.gcClienteBT
                                                                    WHEN PKG_Mago.gcTrasformMtBtDett THEN PKG_Mago.gcTrasformMtBt
                                                                  END,
                                                                  vData,
                                                                  pOrganizzazione,pStato) COD_ELEMENTO_CLIENTE,
                                    COD_ELEMENTO,
                                    COD_TIPO_FONTE,
                                    (NVL(POTENZA_INSTALLATA,0) * NVL(FATTORE,1)) POTENZA_INSTALLATA,
                                    ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                               FROM ELEMENTI E
                              INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                              WHERE vData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                AND COD_TIPO_FONTE IS NOT NULL
                                AND E.COD_TIPO_ELEMENTO = pTipEleGen
                                AND NVL(POTENZA_INSTALLATA,0) > 0
                            ) A
                      INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = COD_ELEMENTO_CLIENTE
                      WHERE vData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                        AND A.ORD = 1;

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassCAL,
                                pFunzione     => 'PKG_Misure.AddMisurePI',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pDataRif      => pData,
                                pStoreOnFile  => FALSE);

    IF NOT GetMisuraOut(vOrigineMis,NULL,vTipoMisura,vCodTipoMis,vFattore,vFlagSplit) THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Associazione Origine della Misura / Tipo elemento ('||vOrigineMis||'/'||vCodTipoMis||') non definita!');
    END IF;

    -- disattiva tutte le misure di potenza installata attive.
    -- se ancora presenti saranno riattivate durante l'elaborazione
    UPDATE (SELECT DATA_DISATTIVAZIONE
              FROM MISURE_ACQUISITE_STATICHE
             INNER JOIN TRATTAMENTO_ELEMENTI  USING(COD_TRATTAMENTO_ELEM)
             WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
               AND COD_TIPO_MISURA = vTipoMisura
               AND TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
           ) M
       SET M.DATA_DISATTIVAZIONE = pData - (1 / 86400);

    OPEN cGen;
    LOOP
        FETCH cGen INTO vTipoGen;
        EXIT WHEN cGen%NOTFOUND;
        BEGIN
            SELECT COD_TIPO_RETE
              INTO vCodTipoRete
              FROM TIPI_ELEMENTO
             WHERE COD_TIPO_ELEMENTO = vTipoGen;
        EXCEPTION
             WHEN NO_DATA_FOUND THEN
                  RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Tipo Rete non definito per Tipo Elemento: '||vTipoGen);
        END;

        OPEN cPot(pData, vTipoGen);
        LOOP
            FETCH cPot INTO vCodEleGen, vEleNome, vCodTipoFonte, vCodTipoClie, vPotInst;
            EXIT WHEN cPot%NOTFOUND;
            vCodTratElem := GetTrattamentoElemento(vCodEleGen,
                                                   vTipoGen,
                                                   vCodTipoMis,
                                                   vCodTipoFonte,
                                                   vCodTipoRete,
                                                   vCodTipoClie,
                                                   PKG_Mago.gcOrganizzazELE,
                                                   PKG_Mago.gcStatoNullo);
            AddMisuraAcq(NULL,
                         vCodTratElem,
                         pData,
                         vCodTipoMis,
                         vPotInst * vFattore,
                         vCodTipoRete,
                         pAggrega,
                         vResult);
            CASE vResult
                 WHEN PKG_Mago.gcInserito   THEN vNumIns := vNumIns + 1;
                 WHEN PKG_Mago.gcModificato THEN vNumMod := vNumMod + 1;
                 ELSE NULL;
            END CASE;

            vCount := vCount + 1;

        END LOOP;

        CLOSE cPot;

        --COMMIT;

   END LOOP;

   CLOSE cGen;

   PKG_Logs.StdLogAddTxt('Tot. Elementi:     '||vCount,TRUE,NULL,vLog);
   IF PKG_Misure.IsMisuraStatica(vTipoMisura) = PKG_UtlGlb.gkFlagOn THEN
       PKG_Logs.StdLogAddTxt('Misure Trattate:   '||vNumIns,TRUE,NULL,vLog);
   ELSE
       PKG_Logs.StdLogAddTxt('Misure Inserite:   '||vNumIns,TRUE,NULL,vLog);
       PKG_Logs.StdLogAddTxt('Misure Modificate: '||vNumMod,TRUE,NULL,vLog);
   END IF;
   PKG_Logs.StdLogPrint(vLog);

   --COMMIT;

 --EXCEPTION
 --   WHEN OTHERS THEN
 --        ROLLBACK;
 --        PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
 --        PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
 --        RAISE;

 END AddMisurePI;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureNRI          (pData             IN DATE DEFAULT SYSDATE,
                                  pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                  pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                  pAggrega          IN BOOLEAN DEFAULT TRUE,
                                  pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza il numero di generatori per ogi SbarraAT, CabinaSecondaria, LineaBt
-----------------------------------------------------------------------------------------------------------*/

     vLog          PKG_Logs.t_StandardLog;

     vResult       INTEGER;
     vNumIns       INTEGER := 0;
     vNumMod       INTEGER := 0;
     vNumKO        INTEGER := 0;
     vCount        NUMBER  := 0;

     vTipoMisura   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE := PKG_Mago.gcNumeroImpianti;
     vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'NUM_IMP';
     vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

     vCodEle_prev  ELEMENTI_DEF.COD_ELEMENTO%TYPE := 0;
     vCodEle       ELEMENTI_DEF.COD_ELEMENTO%TYPE;
     vTipFon       ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
     vTipCli       ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
     vTipEle       ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
     vTipRet       TIPI_RETE.COD_TIPO_RETE%TYPE;
     vNumero       ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;

     vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

     vCodTipoMis   TIPI_MISURA.COD_TIPO_MISURA%TYPE;
     vFattore      NUMBER;

     vValore       MISURE_ACQUISITE.VALORE%TYPE;

     CURSOR cEle IS SELECT A.COD_ELEMENTO,A.COD_TIPO_FONTE,D.COD_TIPO_CLIENTE,T.COD_TIPO_RETE,T.COD_TIPO_ELEMENTO,1 NUM_IMPIANTI
                      FROM (SELECT PKG_Elementi.GetElementoPadre(E.COD_ELEMENTO,
                                                                 CASE E.COD_TIPO_ELEMENTO
                                                                   WHEN PKG_Mago.gcGeneratoreAT     THEN PKG_Mago.gcClienteAT
                                                                   WHEN PKG_Mago.gcGeneratoreMT     THEN PKG_Mago.gcClienteMT
                                                                   WHEN PKG_Mago.gcGeneratoreBT     THEN PKG_Mago.gcClienteBT
                                                                 END,
                                                                 pData,
                                                                 pOrganizzazione,pStato) COD_ELEMENTO,
                                   D.COD_TIPO_FONTE,
                                   ROW_NUMBER() OVER (PARTITION BY D.COD_ELEMENTO ORDER BY D.COD_ELEMENTO,D.DATA_ATTIVAZIONE DESC) ORD
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO
                             WHERE pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                               AND D.COD_TIPO_FONTE IS NOT NULL
                               AND E.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,
                                                           PKG_Mago.gcGeneratoreMT,
                                                           PKG_Mago.gcGeneratoreBT)
                               AND NVL(POTENZA_INSTALLATA,0) > 0
                           ) A
                     INNER JOIN ELEMENTI_DEF D ON A.COD_ELEMENTO = D.COD_ELEMENTO
                     INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = A.COD_ELEMENTO
                     INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                     WHERE pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                       AND A.ORD = 1
                     GROUP BY A.COD_ELEMENTO,A.COD_TIPO_FONTE,D.COD_TIPO_CLIENTE,T.COD_TIPO_RETE,T.COD_TIPO_ELEMENTO
                    UNION
                    SELECT A.COD_ELEMENTO,A.COD_TIPO_FONTE,D.COD_TIPO_CLIENTE,T.COD_TIPO_RETE,T.COD_TIPO_ELEMENTO,SUM(A.NUM_IMPIANTI) NUM_IMPIANTI
                      FROM (SELECT PKG_Elementi.GetElementoPadre(E.COD_ELEMENTO,PKG_Mago.gcTrasformMtBt,pData,1,1) COD_ELEMENTO,D.COD_TIPO_FONTE,NUM_IMPIANTI,
                                   ROW_NUMBER() OVER (PARTITION BY D.COD_ELEMENTO ORDER BY D.COD_ELEMENTO,D.DATA_ATTIVAZIONE DESC) ORD
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                             WHERE pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                               AND D.COD_TIPO_FONTE IS NOT NULL
                               AND E.COD_TIPO_ELEMENTO =  PKG_Mago.gcTrasformMtBtDett
                               AND NVL(POTENZA_INSTALLATA,0) > 0
                           ) A
                     INNER JOIN ELEMENTI_DEF D ON A.COD_ELEMENTO = D.COD_ELEMENTO
                     INNER JOIN ELEMENTI E  ON E.COD_ELEMENTO = A.COD_ELEMENTO
                     INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                     WHERE pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                       AND A.ORD = 1
                       AND A.COD_ELEMENTO IS NOT NULL
                     GROUP BY A.COD_ELEMENTO,A.COD_TIPO_FONTE,D.COD_TIPO_CLIENTE,T.COD_TIPO_RETE,T.COD_TIPO_ELEMENTO;

 BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassCAL,
                                pFunzione     => 'PKG_Misure.AddMisureNRI',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pDataRif      => pData,
                                pStoreOnFile  => FALSE);

    IF NOT GetMisuraOut(vOrigineMis,NULL,vTipoMisura,vCodTipoMis,vFattore,vFlagSplit) THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Associazione Origine della Misura / Tipo elemento ('||
                                     vOrigineMis||'/'||vCodTipoMis||') non definita!');
    END IF;

    -- disattiva tutte le misure relative a numero impianti attive.
    -- se ancora presenti saranno riattivate durante l'elaborazione
    UPDATE (SELECT DATA_DISATTIVAZIONE
              FROM MISURE_ACQUISITE_STATICHE
             INNER JOIN TRATTAMENTO_ELEMENTI  USING(COD_TRATTAMENTO_ELEM)
             WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
               AND COD_TIPO_MISURA = vTipoMisura
               AND TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
           ) M
       SET M.DATA_DISATTIVAZIONE = pData - (1 / 86400);

    OPEN cEle;
    LOOP
        FETCH cEle INTO vCodEle, vTipFon, vTipCli, vTipRet, vTipEle, vNumero;
        EXIT WHEN cEle%NOTFOUND;
        vCodTratElem := GetTrattamentoElemento(vCodEle,
                                               vTipEle,
                                               vCodTipoMis,
                                               vTipFon,
                                               vTipRet,
                                               vTipCli,
                                               PKG_Mago.gcOrganizzazELE,
                                               PKG_Mago.gcStatoNullo);
        AddMisuraAcq(NULL,
                     vCodTratElem,
                     pData,
                     vCodTipoMis,
                     vNumero * vFattore,
                     vTipRet,
                     pAggrega,
                     vResult);

        CASE vResult
             WHEN PKG_Mago.gcInserito   THEN vNumIns := vNumIns + 1;
             WHEN PKG_Mago.gcModificato THEN vNumMod := vNumMod + 1;
             ELSE NULL;
        END CASE;

        IF vCodEle <> vCodEle_prev THEN
           vCount := vCount + 1;
           vCodEle_prev := vCodEle;
        END IF;

    END LOOP;

    CLOSE cEle;

    PKG_Logs.StdLogAddTxt('Tot. Elementi:     '||vCount,TRUE,NULL,vLog);
    IF PKG_Misure.IsMisuraStatica(vTipoMisura) = PKG_UtlGlb.gkFlagOn THEN
        PKG_Logs.StdLogAddTxt('Misure Trattate:   '||vNumIns,TRUE,NULL,vLog);
    ELSE
        PKG_Logs.StdLogAddTxt('Misure Inserite:   '||vNumIns,TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Misure Modificate: '||vNumMod,TRUE,NULL,vLog);
    END IF;
    PKG_Logs.StdLogPrint(vLog);

    --COMMIT;

 --EXCEPTION
 --   WHEN OTHERS THEN
 --        ROLLBACK;
 --        PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
 --        PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
 --        RAISE;

 END AddMisureNRI;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureMeteo       (pMisureMeteo    IN T_MISMETEO_ARRAY,
                                 pAggrega        IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure Meteo ricevute.
-----------------------------------------------------------------------------------------------------------*/

  vLog          PKG_Logs.t_StandardLog;

  vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'METEO';
  vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

  vCodTipoMis   TIPI_MISURA.COD_TIPO_MISURA%TYPE;
  vFattore      NUMBER;
  vTipMis       TIPI_MISURA.COD_TIPO_MISURA%TYPE := '_';
  vCodElem      ELEMENTI.COD_ELEMENTO%TYPE := 0;
  vCodEleNome   ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vCodTipoElem  ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
  vCodTipoRete  TIPI_RETE.COD_TIPO_RETE%TYPE;
  vCodTipoClie  ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vDataMin      DATE := TO_DATE('01013000','ddmmyyyy');
  vDataMax      DATE := TO_DATE('01011900','ddmmyyyy');

  vResult       INTEGER;
  vNumIns       INTEGER := 0;
  vNumMod       INTEGER := 0;

  vTot          NUMBER := 0;

 BEGIN

    IF pMisureMeteo.FIRST IS NULL THEN
        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                    pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                    pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                    pStoreOnFile  => FALSE);
        PKG_Logs.StdLogAddTxt('Nessuna misura ricevuta',TRUE,NULL,vLog);
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pStoreOnFile  => FALSE);

    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP
        IF vDataMin > pMisureMeteo(i).DATA THEN
            vDataMin := pMisureMeteo(i).DATA;
        END IF;
        IF vDataMax < pMisureMeteo(i).DATA THEN
            vDataMax := pMisureMeteo(i).DATA;
        END IF;
    END LOOP;
    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pDataRif      => vDataMin,
                                pDataRif_fine => vDataMax,
                                pStoreOnFile  => FALSE);

    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP

        IF pMisureMeteo(i).COD_TIPO_MISURA  <> vTipMis THEN
            IF NOT GetMisuraOut(vOrigineMis,NULL,pMisureMeteo(i).COD_TIPO_MISURA,vCodTipoMis,vFattore,vFlagSplit) THEN
                PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Tipo Misura IN  : '||NVL(pMisureMeteo(i).COD_TIPO_MISURA,'<null>'),FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Tipo Misura OUT : '||vCodTipoMis,FALSE,NULL,vLog);
                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Associazione OrigineMisura/TipoMisura non definita!');
            END IF;
        END IF;

        IF pMisureMeteo(i).COD_ELEMENTO <> vCodElem THEN
            BEGIN
                SELECT NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE, COD_TIPO_RETE
                  INTO vCodEleNome,   vCodTipoElem,      vCodTipoClie,        vCodTipoRete
                  FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO,
                               NVL(COD_TIPO_CLIENTE,PKG_Mago.gcClientePrdNonDeterm) COD_TIPO_CLIENTE,
                               ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                          FROM ELEMENTI
                         INNER JOIN ELEMENTI_DEF E USING(COD_ELEMENTO)
                         WHERE COD_ELEMENTO = pMisureMeteo(i).COD_ELEMENTO
                           AND pMisureMeteo(i).DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE)
                 INNER JOIN TIPI_ELEMENTO E USING(COD_TIPO_ELEMENTO)
                 WHERE ORD = 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                      PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Codice Elemento : '||NVL(TO_CHAR(pMisureMeteo(i).COD_ELEMENTO),'<null>'),FALSE,NULL,vLog);
                      RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Codice Elemento ' || pMisureMeteo(i).COD_ELEMENTO || ' non definito!');
                WHEN OTHERS THEN
                      PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Codice Elemento : '||NVL(TO_CHAR(pMisureMeteo(i).COD_ELEMENTO),'<null>'),FALSE,NULL,vLog);
                      RAISE;
            END;
            vCodElem := pMisureMeteo(i).COD_ELEMENTO;
        END IF;
        vCodTratElem := GetTrattamentoElemento(pMisureMeteo(i).COD_ELEMENTO,
                                               vCodTipoElem,
                                               vCodTipoMis,
                                               pMisureMeteo(i).COD_TIPO_FONTE,
                                               vCodTipoRete,
                                               vCodTipoClie,
                                               PKG_Mago.gcOrganizzazELE,
                                               PKG_Mago.gcStatoNullo);
        AddMisuraAcq('MET',
                     vCodTratElem,
                     pMisureMeteo(i).DATA,
                     pMisureMeteo(i).COD_TIPO_MISURA,
                     pMisureMeteo(i).VALORE * vFattore,
                     vCodTipoRete,
                     pAggrega,
                     vResult);

        vTot := vTot + 1;

        CASE vResult
            WHEN PKG_Mago.gcInserito   THEN vNumIns := vNumIns + 1;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - inserita');
            WHEN PKG_Mago.gcModificato THEN vNumMod := vNumMod + 1;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - modificata');
            ELSE NULL;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - invariata');
        END CASE;

    END LOOP;

    PKG_Logs.StdLogAddTxt('Misure Meteo:   Tot='||vTot||'   Ins='||vNumIns||'   Mod='||vNumMod,TRUE,NULL,vLog);
    PKG_Logs.StdLogPrint(vLog);

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END AddMisureMeteo;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoFonti          (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi fonte seperati da | restituisce l'ID globale dei tipo fonti ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN

    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    IF INSTR(pLista,'!') <> 0 THEN
        vLista := PKG_UtlGlb.SplitString(SUBSTR(pLista,3),PKG_Mago.cSepCharLst);
    ELSE
        vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    END IF;
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_RAGGR_FONTE)
              INTO vNum
             FROM RAGGRUPPAMENTO_FONTI
            WHERE COD_RAGGR_FONTE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoFonti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoReti          (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi rete seperati da | restituisce l'ID globale dei tipi rete ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN
    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_RETE)
              INTO vNum
             FROM TIPI_RETE
            WHERE COD_TIPO_RETE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoReti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoClienti    (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi cliente seperati da | restituisce l'ID globale dei tipi cliente ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN
    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_CLIENTE )
              INTO vNum
             FROM TIPI_CLIENTE
            WHERE COD_TIPO_CLIENTE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoClienti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMisuraStatica       (pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE) RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna 1 se il tipo misura riche la gestione della misura di tipo statico
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pTipMisura IN (PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcNumeroImpianti/*,PKG_Mago.gcPotenzaContrattuale*/) THEN
        RETURN PKG_UtlGlb.gkFlagOn;
    ELSE
        RETURN PKG_UtlGlb.gkFlagOff;
    END IF;
 END IsMisuraStatica;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisure            (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce un cursore con le curve misura richieste partendo da codice elemento numerico
-----------------------------------------------------------------------------------------------------------*/
    vGerEcs     TIPI_ELEMENTO.GER_ECS%TYPE;
    vIdFonti    NUMBER;
    vIdReti     NUMBER;
    vIdClie     NUMBER;
    vFlgNull    NUMBER(1)     := -1;
    vTotTipFon  NUMBER(1)     := -1;
    vTipiMisura VARCHAR2(300) := PKG_Mago.cSepCharLst||pTipiMisura||PKG_Mago.cSepCharLst;
    vAgrTemp    NUMBER  := pAgrTemporale;
    vTipMis     VARCHAR2(20);


--    vDataDa     DATE := pDataDa + (PKG_Date_Java.getTimeOffset(pDataDa) / 24);
--    vDataA      DATE := pDataA  + (PKG_Date_Java.getTimeOffset(pDataA)  / 24);

    vData       DATE;
    vDataDa     DATE := pDataDa;
    vDataA      DATE := pDataA;


    PROCEDURE fLog(pTxt IN VARCHAR2,pSqlCode NUMBER) AS
            vLevel INTEGER;
            vTxt   VARCHAR2(1000);
        BEGIN
            IF pSqlCode IS NOT NULL THEN
               vLevel := PKG_UtlGlb.gcTrace_ERR;
               vTxt := 'Errore '||pTxt||CHR(10);
            ELSE
               vLevel := PKG_UtlGlb.gcTrace_VRB;
               vTxt := '';
            END IF;
            IF pTxt IS NOT NULL THEN
                vTxt := pTxt || CHR(10) || vTxt || CHR(10) ;
            END IF;
            vTxt := vTxt||
                    'Periodo/Aggreg.    '||Pkg_Mago.StdOutDate(pDataDa)||' - '||Pkg_Mago.StdOutDate(pDataA)||' / <'||vAgrTemp||'>'||CHR(10)||
                    'Elem/Org/Stato    <'||NVL(TO_CHAR(pCodElem),'<null>')||'> / <'||NVL(TO_CHAR(pOrganizzazione),'<null>')||'> / <'||NVL(TO_CHAR(pStatoRete),'<null>')||'>'||CHR(10)||
                    'TipRete/TipMisura <'||NVL(pTipologiaRete,'<null>')||'> / <'||NVL(pTipiMisura,'<null>')||'>'||CHR(10)||
                    'TipFonte/TipClie  <'||NVL(pFonte,'<null>')||'> / <'||NVL(pTipoClie,'<null>')||'>';
            PKG_Logs.StdLogAddTxt(vTxt,FALSE,pSqlCode);
            PKG_Logs.StdLogPrint(vLevel);
        END;

 BEGIN

    IF vAgrTemp > 1440 THEN
        vAgrTemp := 1440;
    END IF;

    vIdFonti := GetIdTipoFonti(pFonte);
    IF INSTR(pFonte,'!') <> 0 THEN
        CASE SUBSTR(pFonte,1,1)
            WHEN '0' THEN vTotTipFon := PKG_Misure.cMisTot;
            WHEN '1' THEN vTotTipFon := PKG_Misure.cMisDettTot;
        END CASE;
    ELSE
        vTotTipFon := PKG_Misure.cMisDett;
    END IF;
    vIdReti  := GetIdTipoReti(pTipologiaRete);
    vIdClie  := GetIdTipoClienti(pTipoClie);

    -- pulisce tabelle temporanee
    DELETE GTTD_VALORI_TEMP;
    DELETE GTTD_MISURE;

    -- produce i filtri elenchi dei codici filtro richiesti
    PKG_Mago.TrattaListaCodici(vTipiMisura,PKG_Mago.gcTmpTipMisKey, vFlgNull);    -- non gestito il flag
	gListaTipoMisura := NULL;
    FOR i IN (SELECT ALF1 COD_TIPO_MISURA FROM GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipMisKey) LOOP
        gListaTipoMisura := gListaTipoMisura || '|' || i.COD_TIPO_MISURA || '|';
    END LOOP;
	
    -- Completa parametri Tipo_misura ----------------------------------

    FOR i IN (SELECT A.COD_TIPO_MISURA,A.RISOLUZIONE_FE,A.MISURA_STATICA,
                     CASE WHEN COUNT(acq.TIPO_AGGREGAZIONE) > 0 THEN PKG_UtlGlb.gkFlagOn
                                                                ELSE PKG_UtlGlb.gkFlagOff
                     END ACQ,
                     CASE WHEN COUNT(agr.TIPO_AGGREGAZIONE) > 0 THEN PKG_UtlGlb.gkFlagOn
                                                                ELSE PKG_UtlGlb.gkFlagOff
                     END AGR
                FROM (SELECT A.COD_TIPO_MISURA,
                             CASE MISURA_STATICA
                               WHEN 1 THEN RISOLUZIONE_FE
                               ELSE CASE WHEN vAgrTemp < RISOLUZIONE_FE
                                     THEN RISOLUZIONE_FE
                                     ELSE vAgrTemp
                                    END
                             END RISOLUZIONE_FE,
                             MISURA_STATICA
                        FROM (SELECT COD_TIPO_MISURA, RISOLUZIONE_FE,PKG_MIsure.IsMisuraStatica(COD_TIPO_MISURA) MISURA_STATICA
                                FROM GTTD_VALORI_TEMP
                               INNER JOIN TIPI_MISURA ON ALF1 = COD_TIPO_MISURA
                               WHERE TIP = PKG_Mago.gcTmpTipMisKey
                              ) A
                     ) A
                LEFT OUTER JOIN TRATTAMENTO_ELEMENTI acq ON acq.COD_ELEMENTO = pCodElem
                                                        AND acq.COD_TIPO_MISURA = A.COD_TIPO_MISURA
                                                        AND acq.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                LEFT OUTER JOIN TRATTAMENTO_ELEMENTI agr ON agr.COD_ELEMENTO = pCodElem
                                                        AND agr.COD_TIPO_MISURA = A.COD_TIPO_MISURA
                                                        AND agr.TIPO_AGGREGAZIONE IN (PKG_Mago.gcStatoNormale,PKG_Mago.gcStatoAttuale)
               GROUP BY A.COD_TIPO_MISURA,A.RISOLUZIONE_FE,A.MISURA_STATICA
             ) LOOP
        IF i.MISURA_STATICA = PKG_UtlGlb.gkFlagOn THEN
            vData := pDataA;
        ELSE
            vData := NULL;
            IF i.ACQ = PKG_UtlGlb.gkFlagOn AND i.AGR = PKG_UtlGlb.gkFlagOff THEN
                SELECT MAX(DATA) INTO vData
                  FROM MISURE_ACQUISITE
                 INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                WHERE COD_ELEMENTO = pCodElem AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
            END IF;
            IF i.ACQ = PKG_UtlGlb.gkFlagOff AND i.AGR = PKG_UtlGlb.gkFlagOn THEN
                SELECT MAX(DATA) INTO vData
                  FROM (SELECT MAX(DATA) DATA
                          FROM MISURE_AGGREGATE
                         INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                        WHERE COD_ELEMENTO = pCodElem AND COD_TIPO_MISURA = i.COD_TIPO_MISURA
                        UNION
                        SELECT MAX(DATARIF) DATA
                          FROM SCHEDULED_JOBS
                         WHERE COD_TIPO_MISURA = i.COD_TIPO_MISURA
                           AND ORGANIZZAZIONE = pOrganizzazione
                           AND STATO = pStatoRete
                       );
            END IF;
            IF i.ACQ = PKG_UtlGlb.gkFlagON AND i.AGR = PKG_UtlGlb.gkFlagOn THEN
                SELECT MAX(DATA) INTO vData
                  FROM (SELECT MAX(DATA) DATA
                          FROM MISURE_ACQUISITE
                         INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                         WHERE COD_ELEMENTO = pCodElem AND COD_TIPO_MISURA = i.COD_TIPO_MISURA
                        UNION ALL
                        SELECT MAX(DATA) DATA
                          FROM (SELECT MAX(DATA) DATA
                                  FROM MISURE_AGGREGATE
                                 INNER JOIN TRATTAMENTO_ELEMENTI USING(COD_TRATTAMENTO_ELEM)
                                WHERE COD_ELEMENTO = pCodElem AND COD_TIPO_MISURA = i.COD_TIPO_MISURA
                                UNION
                                SELECT MAX(DATARIF) DATA
                                  FROM SCHEDULED_JOBS
                                 WHERE COD_TIPO_MISURA = i.COD_TIPO_MISURA
                                   AND ORGANIZZAZIONE = pOrganizzazione
                                   AND STATO = pStatoRete
                               )
                       );
            END IF;
            vdata := NVL(vData,pDataA);
            IF vData > pDataA THEN
                vData := pDataA;
            END IF;
        END IF;
        UPDATE GTTD_VALORI_TEMP SET NUM1 = i.RISOLUZIONE_FE,
                                    NUM2 = i.MISURA_STATICA,
                                    DAT1 = vData             -- data ultimo valore per il tipo misura
         WHERE TIP = PKG_Mago.gcTmpTipMisKey
           AND ALF1 = i.COD_TIPO_MISURA;
    END LOOP;

    -- CURSORE ----------------------------------

    IF vTotTipFon = PKG_Misure.cMisDettTot THEN
        -- Dettaglio + Totale Fonti
        -- calcola prima il dettaglio e lo inserisce in GTTD e quindi calcola il totale dal dettaglio
        INSERT INTO GTTD_MISURE (COD_TIPO_MISURA, DATA, VALORE, COD_TIPO_FONTE)
                    SELECT M.COD_TIPO_MISURA, M.DATA, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE
                      FROM (SELECT T.ALF1 COD_TIPO_MISURA, S.DATA_AL_VOLO, T.NUM1 RISOLUZIONE_FE, DAT1 DATA_ULT_MISURA
                              FROM GTTD_VALORI_TEMP T
                              LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                                      MIN(DATARIF) DATA_AL_VOLO
                                                 FROM SCHEDULED_JOBS
                                                WHERE COD_TIPO_MISURA IS NOT NULL
                                                  AND ORGANIZZAZIONE = pOrganizzazione
                                                  AND STATO = pStatoRete
                                                  AND (   DATARIF BETWEEN vDataDa AND vDataA
                                                       OR vDataDa > DATARIF)
                                                GROUP BY COD_TIPO_MISURA
                                              ) S ON S.COD_TIPO_MISURA = T.ALF1
                             WHERE TIP = PKG_Mago.gcTmpTipMisKey
                           ) A,
                           TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                             A.COD_TIPO_MISURA,
                                                             vIdFonti ,
                                                             vIdReti,
                                                             vIdClie,
                                                             vDataDa,
                                                             vDataA,
                                                             pOrganizzazione,
                                                             pStatoRete,
                                                             PKG_Misure.cMisDett,
                                                             A.RISOLUZIONE_FE,
                                                             A.DATA_AL_VOLO
                                                            )
                                 ) M
                     GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
                     ORDER BY M.COD_TIPO_MISURA, M.DATA;
        OPEN pRefCurs FOR SELECT COD_TIPO_MISURA,DATA,VALORE,COD_TIPO_FONTE
                            FROM GTTD_MISURE
                          UNION ALL
                          SELECT COD_TIPO_MISURA,DATA,SUM(VALORE),'0' COD_TIPO_FONTE
                            FROM GTTD_MISURE
                           GROUP BY COD_TIPO_MISURA,DATA
                           ORDER BY COD_TIPO_MISURA, COD_TIPO_FONTE, DATA;
    ELSE
        -- Dettaglio o Totale Fonti
        OPEN pRefCurs FOR SELECT M.COD_TIPO_MISURA, M.DATA, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE
                            FROM (SELECT T.ALF1 COD_TIPO_MISURA, S.DATA_AL_VOLO, T.NUM1 RISOLUZIONE_FE, DAT1 DATA_ULT_MISURA
                                         FROM GTTD_VALORI_TEMP T
                                    LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                                            MIN(DATARIF) DATA_AL_VOLO
                                                       FROM SCHEDULED_JOBS
                                                      WHERE COD_TIPO_MISURA IS NOT NULL
                                                        AND ORGANIZZAZIONE = pOrganizzazione
                                                        AND STATO = pStatoRete
                                                        AND (   DATARIF BETWEEN vDataDa AND vDataA
                                                             OR vDataDa > DATARIF)
                                                      GROUP BY COD_TIPO_MISURA
                                                    ) S ON S.COD_TIPO_MISURA = T.ALF1
                                   WHERE TIP = PKG_Mago.gcTmpTipMisKey
                                 ) A,
                                 TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                                   A.COD_TIPO_MISURA,
                                                                   vIdFonti ,
                                                                   vIdReti,
                                                                   vIdClie,
                                                                   vDataDa,
                                                                   vDataA,
                                                                   pOrganizzazione,
                                                                   pStatoRete,
                                                                   vTotTipFon,
                                                                   A.RISOLUZIONE_FE,
                                                                   A.DATA_AL_VOLO
                                                                  )
                                       ) M
                           GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
                           ORDER BY M.COD_TIPO_MISURA, M.DATA;
    END IF;

    PKG_Logs.TraceLog('GetMisure - Periodo: '||PKG_Mago.StdOutDate(vDataDa)||'-'||PKG_Mago.StdOutDate(vDataA)||
                               ' - Elemento='||pCodElem||' - Org/Sta='||pOrganizzazione||'/'||pStatoRete||
                                    ' - Mis='||pTipiMisura||' - Ret='||pTipologiaRete||' - Fon='||pFonte||' - Cli='||pTipoClie,
                      PKG_UtlGlb.gcTrace_VRB);
--    fLog(NULL,NULL);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         fLog(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,SQLCODE);
         --PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         --PKG_Logs.StdLogPrint(PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetMisure;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureCG          (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pGestElem         IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce un cursore con le curve misura richieste partendo da codice gestionale
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    GetMisure(pRefCurs,
              PKG_Elementi.GetCodElemento(pGestElem),
              pDataDa,
              pDataA,
              pTipiMisura,
              pOrganizzazione,
              pStatoRete,
              pTipologiaRete,
              pFonte,
              pTipoClie,
              pAgrTemporale);
 END GetMisureCG;

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetMisureTabMisStat  (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAgrTemporale     IN INTEGER,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTable            IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pTipoAggreg1      IN INTEGER,
                                 pTipoAggreg2      IN INTEGER,
                                 pGerECS           IN NUMBER,
                                 pTotTipFon        IN NUMBER) RETURN t_Misure PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le misure richieste in formato pipeline per i tipi misura statici
-----------------------------------------------------------------------------------------------------------*/

    vMis     t_Misura;

    vDataDa  DATE;
    vDataA   DATE;

    c1       PKG_UtlGlb.t_query_cur;
    c2       PKG_UtlGlb.t_query_cur;


    vSql1    VARCHAR2(2200) :=
              'SELECT PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                      || -- pDataDa,pAgrTemporale
--              'SELECT DATA,'    ||
                     'COD_RAGGR_FONTE, MIN(DATA) MIN_DATA, MAX(DATA) MAX_DATA '           ||
                'FROM (SELECT DISTINCT '                                                  ||
                             'CASE WHEN DATA_ATTIVAZIONE < :DtDa THEN :DtDa '             || -- pDataDa,pDataDa
                                  'ELSE DATA_ATTIVAZIONE '                                ||
                             'END DATA '                                                  ||
                        'FROM MISURE_#TAB#_STATICHE '                                     ||
                       'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '    ||
                       'INNER JOIN '                                                      ||
                             '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                    ||
                                'FROM TIPO_FONTI '                                        ||
                               'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '  ||
                                'WHERE 1=1 #FILTRO_TIPO_FONTI# '                          ||
                             ') USING(COD_TIPO_FONTE) '                                   ||
                       '#JOIN_TIPO_RETE# '                                                ||
                       '#JOIN_TIPO_CLIENTE# '                                          ||
                       'WHERE COD_ELEMENTO = :ce '                                        || -- pCodElem
                         'AND COD_TIPO_MISURA = :tm '                                     || -- pTipMis
                         'AND ORGANIZZAZIONE = :org '                                     || -- pOrganizzazione
                         'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                          || -- pTipoAggreg1,pTipoAggreg2
                         '#FILTRO_TIPO_RETE# '                                            ||
                         '#FILTRO_TIPO_CLIENTE# '                                      ||
                         'AND DATA_DISATTIVAZIONE >= :DtDa '                              || -- pDataDa
                         'AND DATA_ATTIVAZIONE    <= :Dta '                               || -- pDataA
                     '), '                                                                ||
                     '(SELECT DISTINCT COD_RAGGR_FONTE '                                  ||
                        'FROM MISURE_#TAB#_STATICHE '                                     ||
                       'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '    ||
                       'INNER JOIN '                                                      ||
                             '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                    ||
                                'FROM TIPO_FONTI '                                        ||
                               'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '  ||
                                'WHERE 1=1 #FILTRO_TIPO_FONTI# '                          ||
                             ') USING(COD_TIPO_FONTE) '                                   ||
                       '#JOIN_TIPO_RETE# '                                                ||
                       '#JOIN_TIPO_CLIENTE# '                                          ||
                       'WHERE COD_ELEMENTO = :ce '                                        || -- pCodElem
                         'AND COD_TIPO_MISURA = :tm '                                     || -- pTipMis
                         'AND ORGANIZZAZIONE = :org '                                     || -- pOrganizzazione
                         'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                          || -- pTipoAggreg1,pTipoAggreg2
                         '#FILTRO_TIPO_RETE# '                                            ||
                         '#FILTRO_TIPO_CLIENTE# '                                      ||
                         'AND DATA_DISATTIVAZIONE >= :DtDa '                              || -- pDataDa
                         'AND DATA_ATTIVAZIONE    <= :Dta '                               || -- pDataA
                     ') '                                                                 ||
--               'GROUP BY DATA, COD_RAGGR_FONTE '                                          ||
               'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag), COD_RAGGR_FONTE '       || -- pDataDa,pAgrTemporale
               'ORDER BY 1,2';

    vSql2    VARCHAR2(900) :=
              'SELECT SUM(VALORE) '                                                       ||
                'FROM MISURE_#TAB#_STATICHE '                                             ||
               'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '            ||
               'INNER JOIN '                                                              ||
                     '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                            ||
                        'FROM TIPO_FONTI '                                                ||
                       'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '          ||
                        'WHERE 1=1 #FILTRO_TIPO_FONTI# '                                  ||
                     ') USING(COD_TIPO_FONTE) '                                           ||
               '#JOIN_TIPO_RETE# '                                                        ||
               '#JOIN_TIPO_CLIENTE# '                                                  ||
               'WHERE COD_ELEMENTO = :ce '                                                || -- pCodElem
                 'AND COD_TIPO_MISURA = :tm '                                             || -- pTipMis
                 'AND ORGANIZZAZIONE = :org '                                             || -- pOrganizzazione
                 'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                                  || -- pTipoAggreg1,pTipoAggreg2
                 '#FILTRO_TIPO_RETE# '                                                    ||
                 '#FILTRO_TIPO_CLIENTE# '                                              ||
                 'AND :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '              || -- vMis.DATA
--                 'AND DATA_DISATTIVAZIONE >= :DtDa '                                      || -- vDataDa
--                 'AND DATA_ATTIVAZIONE    <= :Dta '                                       || -- vDataA
                 'AND COD_RAGGR_FONTE = :fo ';                                               -- vMis.COD_TIPO_FONTE;

 BEGIN

    vMis.COD_TIPO_MISURA := pTipMis;

    vSql1 := CompletaSqlMisure(vSql1,NULL,NULL,pTable,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,pGerECS,pTotTipFon);
    vSql2 := CompletaSqlMisure(vSql2,NULL,NULL,pTable,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,pGerECS,pTotTipFon);

    OPEN c1 FOR vSql1
          USING pDataDa,pAgrTemporale,
                pDataDa,pDataDa,
                pTipiFonte,pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie,pDataDa,pDataA,
                pTipiFonte,pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie,pDataDa,pDataA,
                pDataDa,pAgrTemporale;
    LOOP
        FETCH c1 INTO vMis.DATA,vMis.COD_TIPO_FONTE,vDataDa,vDataA;
        EXIT WHEN c1%NOTFOUND;
        --PRINT(TO_CHAR(vDataA,'dd/mm/yyyy hh24:mi:ss')||' - '||vMis.COD_TIPO_FONTE);
        OPEN c2 FOR vSql2
              USING pTipiFonte,pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie,vDataA,vMis.COD_TIPO_FONTE;
--              USING pTipiFonte,pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie,vMis.DATA,vMis.COD_TIPO_FONTE;
        LOOP
            FETCH c2 INTO vMis.VALORE;
            EXIT WHEN c2%NOTFOUND;
            --PRINT(CHR(9)||CHR(9)||vMis.VALORE);
            PIPE ROW(vMis);
        END LOOP;
        IF c2%ROWCOUNT = 0 THEN
            vMis.VALORE := 0;
            --PRINT(CHR(9)||CHR(9)||vMis.VALORE||' non trovato - forzato 0');
            PIPE ROW(vMis);
        END IF;
        CLOSE c2;
    END LOOP;
    CLOSE c1;

 END GetMisureTabMisStat;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetMisureTabMis      (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTotTipFon        IN INTEGER,
                                 pAgrTemporale     IN INTEGER,
                                 pDataAlVolo       IN DATE,
                                 pForceEmpyRow     IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOff) RETURN t_Misure PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le misure richieste in formato pipeline per il tipo misura / fonti ricevute
-----------------------------------------------------------------------------------------------------------*/
    vOrganizz        INTEGER := pOrganizzazione;
    vStatoRete       INTEGER := pStatoRete;
    vCount           NUMBER  := 0;
    vFoundPI         BOOLEAN := FALSE;

    TYPE             t_TipMisBase IS VARRAY(12) OF INTEGER;
    vTipMisBase      t_TipMisBase :=  t_TipMisBase();

    vRaggrMis        CHAR(3);
    vGerECS          TIPI_ELEMENTO.GER_ECS%TYPE;
    vRisoluzPI       TIPI_MISURA.RISOLUZIONE_FE%TYPE;

    --vDiffDate        NUMBER  := 0;
    --vIntervallo      NUMBER;

    vTipoAggreg1     NUMBER(1):= -1;
    vTipoAggreg2     NUMBER(1):= -1;

    vMisStatica      BOOLEAN  := FALSE;
    vMisNormale      BOOLEAN  := FALSE;
    vTabMisure       REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;

    vMisAggregate    BOOLEAN  := FALSE;
    vMisAcquisite    BOOLEAN  := FALSE;

    vDatAcqDa        DATE     := NULL;
    vDatAcqA         DATE     := NULL;
    vDatAgrDa        DATE     := NULL;
    vDatAgrA         DATE     := NULL;

    vDatAcqStatDa    DATE     := NULL;
    vDatAcqStatA     DATE     := NULL;
    vDatAgrStatDa    DATE     := NULL;
    vDatAgrStatA     DATE     := NULL;

    vDatAcqTotDa     DATE     := NULL;
    vDatAcqTotA      DATE     := NULL;
    vDatAgrTotDa     DATE     := NULL;
    vDatAgrTotA      DATE     := NULL;

    vDatAcqStatTotDa DATE     := NULL;
    vDatAcqStatTotA  DATE     := NULL;
    vDatAgrStatTotDa DATE     := NULL;
    vDatAgrStatTotA  DATE     := NULL;

    vDatAlVoloDa     DATE     := NULL;
    vDatAlVoloA      DATE     := NULL;
    vDatAlVoloTotDa  DATE     := NULL;
    vDatAlVoloTotA   DATE     := NULL;

    vCur             PKG_UtlGlb.t_query_cur;

    vMis             t_Misura;

    vDataDa          DATE    := pDataDa;
    vDataA           DATE    := pDataA;
    vTabella         VARCHAR2(10);

    vSqlMisStat      VARCHAR2(350) :=
                    'SELECT COD_TIPO_MISURA,DATA,'                                                              ||
                           --'SUM(VALORE), #FONTE1# '                                                             ||
                           'SUM(VALORE) VALORE, COD_TIPO_FONTE '                                                ||
                      'FROM (SELECT * FROM TABLE(PKG_MISURE.GetMisureTabMisStat(:ce,:tm,:ag,'                   || -- pCodElem, pTipMis, pAgrTemporale

                                                                               ':fte,:rte,:cli,'                || -- pTipiFonte,pTipiRete,pTipiClie
                                                                               ':DtDa,:DtA,:tab,'               || -- vDat...Da, vDat...A,tabella
                                                                               ':org,:ta1,:ta2,'                || -- vOrganizz,vTipoAggreg1,vTipoAggreg2,
                                                                               ':ecs,:tot))) '                  || -- vGerEcs,pTotTipFon
                     --'GROUP BY COD_TIPO_MISURA,DATA #FONTE2# ';
                     'GROUP BY COD_TIPO_MISURA,DATA, COD_TIPO_FONTE ';

    vSqlMisBase      VARCHAR2(1400) :=
                     'SELECT COD_TIPO_MISURA,PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                     || -- vDat...Da, pAgrTemporale
                            --'#AGR#(VALORE) VALORE, #FONTE1# '                                                   ||
                            '#AGR#(VALORE) VALORE, COD_RAGGR_FONTE '                                            ||
                      'FROM (SELECT COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE,'                                      ||
                                   'SUM(CASE WHEN B.COD_ELEMENTO IS NULL THEN 0 ELSE VALORE END) VALORE '       ||
                              'FROM MISURE_#TAB# '                                                              ||
                              'INNER JOIN (SELECT COD_TRATTAMENTO_ELEM,COD_TIPO_MISURA,COD_ELEMENTO,'           ||
                                                 'COD_RAGGR_FONTE '                                             ||
                                            'FROM TRATTAMENTO_ELEMENTI '                                        ||
                                          '#JOIN_TIPO_FONTI# '                                                  ||
                                          '#JOIN_TIPO_RETE# '                                                   ||

                                          '#JOIN_TIPO_CLIENTE# '                                             ||
                                          'WHERE COD_ELEMENTO = :ce '                                           || -- pCodElem
                                            'AND ORGANIZZAZIONE = :org '                                        || -- vOrganizz
                                            'AND COD_TIPO_MISURA = :tm '                                        || -- pTipMis
                                            'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                             || -- vTipoAggreg1,vTipoAggreg2
                                            '#FILTRO_TIPO_FONTI# '                                              ||
                                            '#FILTRO_TIPO_RETE# '                                               ||

                                            '#FILTRO_TIPO_CLIENTE# '                                         ||
                                        ') A USING (COD_TRATTAMENTO_ELEM) '                                     ||
                             'LEFT OUTER JOIN GERARCHIA_#GER##STA# B '                                          ||
                                              'ON (    A.COD_ELEMENTO = B.COD_ELEMENTO '                        ||
                                                  'AND DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) ' ||
                             'WHERE DATA BETWEEN :DtDa AND :Dta '                                               || -- vDat...Da, vDat...A
                               '#FILT_ELEM_NULL# '                                                              ||
                               'AND B.COD_ELEMENTO IS NOT NULL '                                                ||
                             'GROUP BY COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE '                                   ||
                           ') '                                                                                 ||
                     'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag),'                                        || -- vDat...Da, pAgrTemporale
                              --'COD_TIPO_MISURA #FONTE2# ';
                              'COD_TIPO_MISURA, COD_RAGGR_FONTE ';
    vSqlMisBase2     VARCHAR2(1400);

    vSqlMisAlVolo    VARCHAR2(1300) :=
                    'SELECT COD_TIPO_MISURA,PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                      || -- vDat...Da, pAgrTemporale
                           --'#AGR#(VALORE) VALORE, #FONTE1# '                                                    ||
                           '#AGR#(VALORE) VALORE, COD_RAGGR_FONTE '                                             ||
                      'FROM (SELECT COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE,'                                      ||
                                   'SUM(CASE WHEN B.COD_ELEMENTO IS NULL THEN 0 ELSE VALORE END) VALORE '       ||
                              'FROM MISURE_ACQUISITE '                                                          ||
                             'INNER JOIN (SELECT DISTINCT COD_TRATTAMENTO_ELEM,COD_RAGGR_FONTE,'                ||
                                                'COD_TIPO_MISURA,COD_ELEMENTO '                                 ||
                                           'FROM TRATTAMENTO_ELEMENTI '                                         ||
                                          'INNER JOIN TABLE(PKG_Elementi.LeggiGerarchiaLineare(:DtDa,:DtA,'     || -- vDat...Da,vDat...A
                                                                                              ':org,:sr,:ce)) ' || -- vOrganizz,vStatoRete,pCodElem
                                                                 'USING(COD_ELEMENTO) '                         ||
                                          '#JOIN_TIPO_FONTI# '                                                  ||
                                          '#JOIN_TIPO_RETE# '                                                   ||

                                          '#JOIN_TIPO_CLIENTE# '                                                ||
                                          'WHERE ORGANIZZAZIONE = :org '                                        || -- PKG_Mago.gcOrganizzazELE
                                            'AND COD_TIPO_MISURA = :tm '                                        || -- pTipMis
                                            'AND TIPO_AGGREGAZIONE = 0 '                                        || -- costante 0 perch� legge solo acquisite
                                            '#FILTRO_TIPO_FONTI# '                                              ||
                                            '#FILTRO_TIPO_RETE# '                                               ||

                                            '#FILTRO_TIPO_CLIENTE# '                                            ||
                                        ') A USING (COD_TRATTAMENTO_ELEM) '                                     ||
                             --'LEFT OUTER JOIN GERARCHIA_#GER##STA# B '                                          ||
                             'INNER JOIN GERARCHIA_#GER##STA# B '                                               ||
                                              'ON (    A.COD_ELEMENTO = B.COD_ELEMENTO '                        ||
                                                  'AND DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) ' ||
                             'WHERE DATA BETWEEN :DtDa AND :Dta '                                               || -- vDat...Da, vDat...A
                             'GROUP BY COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE '                                   ||
                           ')'                                                                                  ||
                     'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag),'                                        || -- vDat...Da, pAgrTemporale
                              --'COD_TIPO_MISURA #FONTE2# ';
                              'COD_TIPO_MISURA, COD_RAGGR_FONTE ';

 BEGIN

    IF PKG_Misure.IsMisuraStatica(pTipMis) = PKG_UtlGlb.gkFlagON THEN
        vMisStatica := TRUE;
    ELSE
        vMisNormale := TRUE;
        SELECT RISOLUZIONE_FE
          INTO vRisoluzPI
          FROM TIPI_MISURA
         WHERE COD_TIPO_MISURA = Pkg_Mago.gcPotenzaInstallata;
    END IF;

    --IF pAgrTemporale >= 45 THEN
    --    vRaggrMis := 'MAX';
    --    IF PKG_Mago.IsMagoDGF THEN
    --        IF pAgrTemporale = 60 THEN
    --            vRaggrMis := 'AVG';
    --        END IF;
    --    END IF;
    --ELSE
    --    vRaggrMis := 'AVG';
    --END IF;

    IF pAgrTemporale <= 60 THEN
        vRaggrMis := 'AVG';
    ELSE
        vRaggrMis := 'MAX';
    END IF;

    vTipoAggreg1 := vStatoRete;
    vTipoAggreg2 := 0;

    IF PKG_Mago.IsMagoDGF THEN
        SELECT CASE WHEN GER_ECP = 0 THEN GER_ECS ELSE 0 END INTO vGerECS
         FROM ELEMENTI
        INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
        WHERE COD_ELEMENTO = pCodElem;
    ELSE
       SELECT GER_ECS INTO vGerECS
         FROM ELEMENTI
        INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
        WHERE COD_ELEMENTO = pCodElem;
    END IF;

    IF vGerECS = PKG_UtlGlb.gkFlagOn THEN
        IF vOrganizz != PKG_Mago.gcOrganizzazELE THEN
            vOrganizz := PKG_Mago.gcOrganizzazELE;
        END IF;
    END IF;

    IF vDataDa < PKG_Misure.GetRoundDate(pDataDa,PKG_Mago.GetStartDate(vDataDa),pAgrTemporale) THEN
        vDataDa := PKG_Misure.GetRoundDate(pDataDa,PKG_Mago.GetStartDate(vDataDa),pAgrTemporale);
        IF vDataDa > vDataA THEN
            vDataA := vDataDa;
        END IF;
    END IF;

   -- determina su quale/i tabelle misure selezionare le curve
    vTabMisure   := GetTabMisure(pCodElem,pTipMis,vMisStatica,pTipiFonte,pTipiRete,
                                 pTipiClie,vGerECS,vTipoAggreg1,vTipoAggreg2,vDataDa,vDataA);

    IF BITAND(vTabMisure,PKG_Mago.gcTabAcquisite) = PKG_Mago.gcTabAcquisite THEN
        vMisAcquisite := TRUE;
        -- imposta Range Date per misure acquisite
        vDatAcqDa := vDataDa;
        vDatAcqA  := vDataA;
        IF vMisStatica THEN
            IF pTotTipFon IN (cMisDettTot,cMisDett) THEN
                vDatAcqStatDa    := vDataDa;
                vDatAcqStatA     := vDataA;
            END IF;
            IF pTotTipFon IN (cMisDettTot,cMisTot) THEN
                vDatAcqStatTotDa := vDataDa;
                vDatAcqStatTotA  := vDataA;
            END IF;
        ELSE
            vTipMisBase.EXTEND;
            vTipMisBase(vTipMisBase.LAST) := PKG_Mago.gcTabAcquisite;
        END IF;
    END IF;

    IF BITAND(vTabMisure,PKG_Mago.gcTabAggregate) = PKG_Mago.gcTabAggregate THEN
       vMisAggregate := TRUE;
        -- imposta Range Date per misure aggregate
        vDatAgrDa := vDataDa;
        vDatAgrA  := vDataA;
        IF vMisStatica THEN
            vDatAgrStatDa := vDataDa;
            vDatAgrStatA  := vDataA;
        ELSE
            vTipMisBase.EXTEND;
            vTipMisBase(vTipMisBase.LAST) := PKG_Mago.gcTabAggregate;
        END IF;
    END IF;

    CASE pTotTipFon
        WHEN cMisDettTot THEN         -- Sono richiesti sia il dettaglio fonti che il totale
                IF vMisAcquisite THEN
                    vDatAcqTotDa     := vDatAcqDa;
                    vDatAcqTotA      := vDatAcqA;
                    vDatAcqStatTotDa := vDatAcqDa;
                    vDatAcqStatTotA  := vDatAcqA;
                END IF;
                IF vMisAggregate THEN
                    vDatAgrTotDa     := vDatAgrDa;
                    vDatAgrTotA      := vDatAgrA;
                    vDatAgrStatTotDa := vDatAgrDa;
                    vDatAgrStatTotA  := vDatAgrA;
                END IF;
        WHEN cMisTot THEN         -- E' richiesto il solo totale
                IF vMisAcquisite THEN
                    vDatAcqTotDa     := vDatAcqDa;
                    vDatAcqTotA      := vDatAcqA;
                    vDatAcqStatTotDa := vDatAcqDa;
                    vDatAcqStatTotA  := vDatAcqA;
                END IF;
                IF vMisAggregate THEN
                    vDatAgrTotDa     := vDatAgrDa;
                    vDatAgrTotA      := vDatAgrA;
                    vDatAgrStatTotDa := vDatAgrDa;
                    vDatAgrStatTotA  := vDatAgrA;
                END IF;
                vDatAcqDa            := NULL;
                vDatAcqA             := NULL;
                vDatAgrDa            := NULL;
                vDatAgrA             := NULL;
                vDatAgrStatDa        := NULL;
                vDatAgrStatA         := NULL;
        ELSE NULL;       -- E' richiesto solo il Dettaglio fonti
    END CASE;

    IF NOT vMisNormale THEN
        -- solo misure statiche
        vDatAcqDa        := NULL;
        vDatAcqA         := NULL;
        vDatAgrDa        := NULL;
        vDatAgrA         := NULL;
        vDatAcqTotDa     := NULL;
        vDatAcqTotA      := NULL;
        vDatAgrTotDa     := NULL;
        vDatAgrTotA      := NULL;
    END IF;

    IF NOT vMisStatica THEN
        -- solo misure non statiche
        vDatAcqStatDa    := NULL;
        vDatAcqStatA     := NULL;
        vDatAgrStatDa    := NULL;
        vDatAgrStatA     := NULL;
        vDatAcqStatTotDa := NULL;
        vDatAcqStatTotA  := NULL;
        vDatAgrStatTotDa := NULL;
        vDatAgrStatTotA  := NULL;
    END IF;

    IF pDataAlVolo IS NOT NULL THEN
        IF vMisAggregate AND vMisNormale THEN
            IF pDataAlVolo < vDatAgrDa THEN
                vDatAlVoloDa    := vDatAgrDa;
                vDatAlVoloA     := vDatAgrA;
                vDatAgrDa       := NULL;
                vDatAgrA        := NULL;
            END IF;
            IF pDataAlVolo < vDatAgrTotDa THEN
                vDatAlVoloTotDa := vDatAgrTotDa;
                vDatAlVoloTotA  := vDatAgrTotA;
                vDatAgrTotDa    := NULL;
                vDatAgrTotA     := NULL;
            END IF;
            IF pDataAlVolo BETWEEN vDatAgrDa AND vDatAgrA THEN
                vDatAlVoloA     := vDatAgrA;
                vDatAlVoloDa    := pDataAlVolo;
                vDatAgrA        := pDataAlVolo - cUnSecondo;
            END IF;
            IF pDataAlVolo BETWEEN vDatAgrTotDa AND vDatAgrTotA THEN
                vDatAlVoloTotA  := vDatAgrTotA;
                vDatAlVoloTotDa := pDataAlVolo;
                vDatAgrTotA     := pDataAlVolo - cUnSecondo;
            END IF;
        END IF;
    END IF;

    IF vDatAgrTotDa > vDatAgrTotA THEN
        vDatAgrTotDa := NULL;
        vDatAgrTotA  := NULL;
    END IF;
    IF vDatAgrDa > vDatAgrA THEN
        vDatAgrDa := NULL;
        vDatAgrA  := NULL;
    END IF;

--    PRINT('vOrganizz     '||RPAD(vOrganizz,10)    ||'vStatoRete  '||RPAD(vStatoRete,10));
--    PRINT('pAgrTemporale '||RPAD(pAgrTemporale,10)||'vTipoAggreg '||RPAD(vTipoAggreg1||'/'||vTipoAggreg2,10)||'pTotTipFon  '||CASE pTotTipFon WHEN PKG_Misure.cMisTot THEN 'Totale' ELSE 'Dettaglio' END);
--    PRINT('pCodElem      '||RPAD(pCodElem,10)     ||'pTipMis     '||RPAD(pTipMis,10)   ||'vTabMisure  '||RPAD(vTabMisure,10) ||'vGerECS     '||vGerECS);
--    PRINT('pTipiFonte    '||RPAD(pTipiFonte,10)   ||'pTipiRete   '||RPAD(pTipiRete,10) ||'pTipiClie   '||RPAD(pTipiClie,10));
--    PRINT('vMisNormale   '||RPAD(CASE WHEN vMisNormale THEN 'SI' ELSE 'NO' END,10)||'vMisStatica '||CASE WHEN vMisStatica THEN 'SI' ELSE 'NO' END);
--    PRINT('pData (input)  da '||PKG_Mago.StdOutDate(pDataDa         )||'  a '||PKG_Mago.StdOutDate(pDataA         )||' - pDataAlVolo '||PKG_Mago.StdOutDate(pDataAlVolo));
--    PRINT('vDatAcq        da '||PKG_Mago.StdOutDate(vDatAcqDa       )||'  a '||PKG_Mago.StdOutDate(vDatAcqA       ));
--    PRINT('vDatAgr        da '||PKG_Mago.StdOutDate(vDatAgrDa       )||'  a '||PKG_Mago.StdOutDate(vDatAgrA       ));
--    PRINT('vDatAcqStat    da '||PKG_Mago.StdOutDate(vDatAcqStatDa   )||'  a '||PKG_Mago.StdOutDate(vDatAcqStatA   ));
--    PRINT('vDatAgrStat    da '||PKG_Mago.StdOutDate(vDatAgrStatDa   )||'  a '||PKG_Mago.StdOutDate(vDatAgrStatA   ));
--    PRINT('vDatAcqTot     da '||PKG_Mago.StdOutDate(vDatAcqTotDa    )||'  a '||PKG_Mago.StdOutDate(vDatAcqTotA    ));
--    PRINT('vDatAgrTot     da '||PKG_Mago.StdOutDate(vDatAgrTotDa    )||'  a '||PKG_Mago.StdOutDate(vDatAgrTotA    ));
--    PRINT('vDatAcqStatTot da '||PKG_Mago.StdOutDate(vDatAcqStatTotDa)||'  a '||PKG_Mago.StdOutDate(vDatAcqStatTotA));
--    PRINT('vDatAgrStatTot da '||PKG_Mago.StdOutDate(vDatAgrStatTotDa)||'  a '||PKG_Mago.StdOutDate(vDatAgrStatTotA));
--    PRINT('vDatAlVolo     da '||PKG_Mago.StdOutDate(vDatAlVoloDa    )||'  a '||PKG_Mago.StdOutDate(vDatAlVoloA    ));
--    PRINT('vDatAlVoloTot  da '||PKG_Mago.StdOutDate(vDatAlVoloTotDa )||'  a '||PKG_Mago.StdOutDate(vDatAlVoloTotA ));

    IF vMisStatica THEN
        -- Misure Statiche
        IF vDatAcqStatDa IS NOT NULL THEN
            vDataDa := vDatAcqStatDa;
            vDataA  := vDatAcqStatA;
            vTabella:= 'ACQUISITE';
        END IF;
        IF vDatAcqStatTotDa IS NOT NULL THEN
            vDataDa := vDatAcqStatTotDa;
            vDataA  := vDatAcqStatTotA;
            vTabella:= 'ACQUISITE';
        END IF;
        IF vDatAgrStatDa IS NOT NULL THEN
            vDataDa := vDatAgrStatDa;
            vDataA  := vDatAgrStatA;
            vTabella:= 'AGGREGATE';
        END IF;
        IF vDatAgrStatTotDa IS NOT NULL THEN
            vDataDa := vDatAgrStatTotDa;
            vDataA  := vDatAgrStatTotA;
            vTabella:= 'AGGREGATE';
        END IF;
        --IF pTotTipFon = PKG_Misure.cMisTot THEN
        --    vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE1#','''0'' COD_TIPO_FONTE');
        --    vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE2#',' ');
        --ELSE
        --    vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE1#','COD_TIPO_FONTE');
        --    vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE2#',',COD_TIPO_FONTE');
        --END IF;
        IF pTotTipFon = PKG_Misure.cMisTot THEN
            vSqlMisStat := 'SELECT COD_TIPO_MISURA,DATA,SUM(VALORE) VALORE,''0'' COD_TIPO_FONTE FROM('||vSqlMisStat||') GROUP BY COD_TIPO_MISURA,DATA';
        END IF;
        OPEN vCur FOR vSqlMisStat
                USING pCodElem,pTipMis,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,vDataDa,vDataA,
                      vTabella,vOrganizz,vTipoAggreg1,vTipoAggreg2,vGerEcs,pTotTipFon;
        LOOP
            FETCH vCur INTO vMis;
            EXIT WHEN vCur%NOTFOUND;
            vCount := vCount + 1;
            PIPE ROW(vMis);
        END LOOP;
        CLOSE vCur;
    ELSE
        IF vDatAcqDa    IS NOT NULL OR
           vDatAcqTotDa IS NOT NULL OR
           vDatAgrDa    IS NOT NULL OR
           vDatAgrTotDa IS NOT NULL THEN
            FOR i IN vTipMisBase.FIRST .. vTipMisBase.LAST LOOP
                CASE vTipMisBase(i)
                    WHEN PKG_Mago.gcTabAggregate THEN
                        -- Misure Aggregate Standard
                        IF vDatAgrDa IS NOT NULL THEN
                            vDataDa := vDatAgrDa;
                            vDataA  := vDatAgrA;
                        END IF;
                        IF vDatAgrTotDa IS NOT NULL THEN
                            vDataDa := vDatAgrTotDa;
                            vDataA  := vDatAgrTotA;
                        END IF;
                        vTabella := 'AGGREGATE';
                    WHEN PKG_Mago.gcTabAcquisite THEN
                        -- Misure Acquisite Standard
                        IF vDatAcqDa IS NOT NULL THEN
                            vDataDa := vDatAcqDa;
                            vDataA  := vDatAcqA;
                        END IF;
                        IF vDatAcqTotDa IS NOT NULL THEN
                            vDataDa := vDatAcqTotDa;
                            vDataA  := vDatAcqTotA;
                        END IF;
                        vTabella := 'ACQUISITE';
                END CASE;
                vSqlMisBase2 := CompletaSqlMisure(vSqlMisBase,pOrganizzazione,pStatoRete,vTabella,vRaggrMis,pTipiFonte,pTipiRete,pTipiClie,vGerECS,pTotTipFon);
                IF pTotTipFon = PKG_Misure.cMisTot THEN
                    vSqlMisBase2 := 'SELECT COD_TIPO_MISURA,DATA,SUM(VALORE) VALORE,''0'' COD_TIPO_FONTE FROM('||vSqlMisBase2||') GROUP BY COD_TIPO_MISURA,DATA';
                END IF;
                IF vTipMisBase(i) = PKG_Mago.gcTabAggregate THEN
                    vFoundPI := FALSE;
                    FOR i IN (SELECT DATA_DA, NVL(DATA_A,vDataA) DATA_A, VALORE VAL_PI
                                FROM (SELECT DATA DATA_DA, LEAD(DATA) OVER (ORDER BY DATA) - cUnSecondo DATA_A, VALORE
                                        FROM (SELECT DATA, SUM(VALORE) VALORE
                                                FROM TABLE(PKG_Misure.GetMisureTabMisStat(pCodElem,Pkg_Mago.gcPotenzaInstallata,vRisoluzPI,
                                                                                          pTipiFonte,pTipiRete,pTipiClie,vDataDa,vDataA,
                                                                                          'AGGREGATE',pOrganizzazione,vTipoAggreg1,vTipoAggreg2,vGerEcs,pTotTipFon))
                                               GROUP BY DATA
                                             )
                                     )
                             ) LOOP
                        vFoundPI := TRUE;
                        --PRINT('mis standard '||vTabella||' - '||pTipMis||' - dal '||PKG_Mago.StdOutDate(i.DATA_DA)||' - al '||PKG_Mago.StdOutDate(i.DATA_A)||' - valore '||i.VAL_PI);
                        IF NVL(i.VAL_PI,0) >= 0 OR  (vTabella = 'ACQUISITE') THEN
                            OPEN vCur FOR vSqlMisBase2
                                    USING vDataDa,pAgrTemporale,  -- i.DATA_DA,pAgrTemporale,
                                          pCodElem,vOrganizz,pTipMis,vTipoAggreg1,vTipoAggreg2,
                                          pTipiFonte,pTipiRete,pTipiClie,
                                          i.DATA_DA,i.DATA_A,
                                          vDataDa,pAgrTemporale;  -- i.DATA_DA,pAgrTemporale;
                            LOOP
                                FETCH vCur INTO vMis;
                                EXIT WHEN vCur%NOTFOUND;
                                vCount := vCount + 1;
                                PIPE ROW(vMis);
                            END LOOP;
                            CLOSE vCur;
                        --ELSE
                        --    IF PKG_Misure.GetRoundDate(vDataDa,i.DATA_DA,pAgrTemporale) <> PKG_Misure.GetRoundDate(vDataDa,i.DATA_A,pAgrTemporale) THEN
                        --        FOR x IN (SELECT DATAVAL
                        --                    FROM TABLE (PKG_UtlGlb.GetCalendario(PKG_Misure.GetRoundDate(vDataDa,i.DATA_DA,1440/pAgrTemporale),
                        --                                                         PKG_Misure.GetRoundDate(vDataDa,i.DATA_A,1440/pAgrTemporale),1440/pAgrTemporale))) LOOP
                        --           PRINT('.   > 0 in '||TO_CHAR(x.DATAVAL,'dd/mm/yyyy hh24:mi:ss'));
                        --        END LOOP;
                        --    END IF;
                        END IF;
                    END LOOP;
                END IF;
                IF (vTipMisBase(i) = PKG_Mago.gcTabAcquisite) OR
                   (vFoundPI       = FALSE)                   THEN
                    OPEN vCur FOR vSqlMisBase2
                            USING vDataDa,pAgrTemporale,
                                  pCodElem,vOrganizz,pTipMis,vTipoAggreg1,vTipoAggreg2,
                                  pTipiFonte,pTipiRete,pTipiClie,
                                  vDataDa,vDataA,
                                  vDataDa,pAgrTemporale;
                    LOOP
                        FETCH vCur INTO vMis;
                        EXIT WHEN vCur%NOTFOUND;
                        vCount := vCount + 1;
                        PIPE ROW(vMis);
                    END LOOP;
                    CLOSE vCur;
                END IF;
            END LOOP;
        END IF;
        IF vDatAlVoloDa    IS NOT NULL OR
           vDatAlVoloTotDa IS NOT NULL THEN
            -- Aggregate AlVolo
            IF vDatAlVoloDa IS NOT NULL THEN
                vDataDa := vDatAlVoloDa;
                vDataA  := vDatAlVoloA;
            END IF;
            IF vDatAlVoloTotDa IS NOT NULL THEN
                vDataDa := vDatAlVoloTotDa;
                vDataA  := vDatAlVoloTotA;
            END IF;
            vTabella:= 'AGGREGATE';
            vSqlMisAlVolo := CompletaSqlMisure(vSqlMisAlVolo,pOrganizzazione,pStatoRete,'null',vRaggrMis,pTipiFonte,pTipiRete,pTipiClie,vGerEcs,pTotTipFon);
            IF pTotTipFon = PKG_Misure.cMisTot THEN
                vSqlMisAlVolo := 'SELECT COD_TIPO_MISURA,DATA,SUM(VALORE) VALORE,''0'' COD_TIPO_FONTE FROM('||vSqlMisAlVolo||') GROUP BY COD_TIPO_MISURA,DATA';
            END IF;

            FOR i IN (SELECT DATA_DA, NVL(DATA_A,vDataA) DATA_A, VALORE VAL_PI
                        FROM (SELECT DATA DATA_DA, LEAD(DATA) OVER (ORDER BY DATA) - cUnSecondo DATA_A, VALORE
                                FROM (SELECT DATA, SUM(VALORE) VALORE
                                        FROM TABLE(PKG_Misure.GetMisureTabMisStat(pCodElem,Pkg_Mago.gcPotenzaInstallata,vRisoluzPI,
                                                                                  pTipiFonte,pTipiRete,pTipiClie,vDataDa,vDataA,
                                                                                  'AGGREGATE',pOrganizzazione,vTipoAggreg1,vTipoAggreg2,vGerEcs,pTotTipFon))
                                       GROUP BY DATA
                                     )
                             )
                     ) LOOP
                --PRINT('std. al volo '||vTabella||' - '||pTipMis||' - dal '||PKG_Mago.StdOutDate(i.DATA_DA)||' - al '||PKG_Mago.StdOutDate(i.DATA_A)||' - valore '||i.VAL_PI);
                IF NVL(i.VAL_PI,0) >= 0 THEN
                    OPEN vCur FOR vSqlMisAlVolo
                            USING vDataDa,pAgrTemporale, -- i.DATA_DA,pAgrTemporale,
                                  i.DATA_DA,i.DATA_A,vOrganizz,vStatoRete,pCodElem,PKG_Mago.gcOrganizzazELE,pTipMis,
                                  pTipiFonte,pTipiRete,pTipiClie,i.DATA_DA,i.DATA_A,
                                  vDataDa,pAgrTemporale; --i.DATA_DA,pAgrTemporale;
                    LOOP
                        FETCH vCur INTO vMis;
                        EXIT WHEN vCur%NOTFOUND;
                        vCount := vCount + 1;
                        PIPE ROW(vMis);
                    END LOOP;
                    CLOSE vCur;
                END IF;
            END LOOP;
        END IF;
    END IF;

    IF pForceEmpyRow = PKG_UtlGlb.gkFlagON THEN
        IF vCount = 0 THEN
            --PRINT('non trovato');
            vMis := NULL;
            vMis.DATA := pDataDa;
            vMis.COD_TIPO_MISURA := pTipMis;
            PIPE ROW(vMis);
        END IF;
    END IF;

    RETURN;

 END GetMisureTabMis;

-- ----------------------------------------------------------------------------------------------------------
-- Implementazioni versione 1.6a
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureOfflineSessionID  (pSessionID     OUT NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce un nuovo identificativo di sessione Forecast
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
        SELECT OFFLINE_MEASURE_REQUEST_IDSEQ.NEXTVAL INTO pSessionID FROM DUAL;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Misure.GetMeasureOfflineSessionID'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetMeasureOfflineSessionID;

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

--PROCEDURE GetClienti(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
--                     pData           IN DATE,
--                     pTipiElemento   IN VARCHAR2 DEFAULT PKG_Mago.cSepCharLst) IS
--

--    vTmp           PKG_UtlGlb.t_SplitTbl;

--    vTipClieA       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
--    vTipClieB       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
--    vTipClieM       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;

-- BEGIN

--    vTipClieA := NULL;
--    vTipClieB := NULL;
--
--    vTmp := PKG_UtlGlb.SplitString(pTipiElemento,PKG_Mago.cSepCharLst);

--    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
--        IF vTmp(i) = PKG_Mago.gcClienteAT THEN
--            vTipClieA := vTmp(i);
--        END IF;
--        IF vTmp(i) = PKG_Mago.gcClienteBT THEN
--            vTipClieB := vTmp(i);
--        END IF;
--        IF vTmp(i) = PKG_Mago.gcClienteMT THEN
--            vTipClieM := vTmp(i);
--        END IF;
--    END LOOP;



--    OPEN pRefCurs FOR
--        WITH
--        eleP AS (SELECT * FROM ELEMENTI WHERE COD_TIPO_ELEMENTO IN (vTipClieA,vTipClieB,vTipClieM)),
--        eleD AS (SELECT * FROM ELEMENTI_DEF WHERE pData BETWEEN data_attivazione AND data_disattivazione)
--              SELECT E.cod_gest_elemento  cod_gest,
--                     cod_elemento,
--                     E.cod_tipo_elemento,
--                     d.rif_elemento pod
--               FROM eleD d JOIN eleP E USING (cod_elemento) ;
--
--
--        /*
--        guarda il pod dei padri?
--        WITH
--        eleP AS (SELECT * FROM ELEMENTI WHERE COD_TIPO_ELEMENTO IN ('CMT','CAT','CBT')),
--        eleD AS (SELECT * FROM elementi_Def WHERE sysdate BETWEEN data_attivazione AND data_disattivazione)
--          SELECT e.cod_gest_elemento  cod_gest,
--                 cod_elemento,
--                 e.cod_tipo_elemento,
--                 NVL(d.rif_elemento,pd.rif_elemento) pod
--          FROM eleD d join eleP e using (cod_elemento)
--          left join eleD pd on (e.cod_gest_elemento=pd.id_elemento) ;
--      */
--


--END GetClienti;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Misure',
                          pStoreOnFile => FALSE);

END PKG_Misure;
/


SHOW ERRORS;


