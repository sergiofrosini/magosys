Prompt Package Body PKG_LOGS;
--
-- PKG_LOGS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY "PKG_LOGS" AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.i
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gLogCommonArea     t_StandardLog;

 gcLogDir           CONSTANT VARCHAR2(30) := PKG_UtlGlb.gv_DirLog;

 gTraceLevel        INTEGER;
 gSessionID         VARCHAR(30);

 gLogInitialized    BOOLEAN := FALSE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetLogFile (pClasse IN VARCHAR2) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - ritorna l'id del log relativo alla classe ricevuta
-----------------------------------------------------------------------------------------------------------*/
    vPos INTEGER;
    vCls VARCHAR2(100) := SUBSTR(pClasse,1,100);
    x VARCHAR2(19);
BEGIN
    vPos := INSTR(vCls,'.');
    IF vPos > 0 THEN
        vCls := SUBSTR(vCls,1,vPos - 1);
    END IF;
    IF vCls IS NOT NULL THEN
        RETURN SUBSTR(INITCAP(LOWER(REPLACE(vCls,' ',''))),1,11);
    END IF;
    RETURN 'Generic';
    
END GetLogFile;

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

FUNCTION StdLogInit          (pClasseFunz    IN VARCHAR2,
                              pFunzione      IN VARCHAR2,
                              pDataRif       IN DATE     DEFAULT NULL,
                              pDataRif_fine  IN DATE     DEFAULT NULL,
                              pCodice        IN VARCHAR2 DEFAULT NULL,
                              pTipo          IN VARCHAR2 DEFAULT NULL,
                              pNome          IN VARCHAR2 DEFAULT NULL,
                              pDescrizione   IN VARCHAR2 DEFAULT NULL,
                              pStoreOnFile   IN BOOLEAN  DEFAULT TRUE,
                              pForceLogFile  IN BOOLEAN  DEFAULT TRUE) RETURN t_StandardLog AS
    vLog     t_StandardLog := NULL;
    vNum     NUMBER;
    vPref    CHAR(4);
BEGIN

    IF PKG_Mago.IsmagoDGF THEN
        vPref := 'Dgf_';
    ELSE
        vPref := 'Mag_';
    END IF;

    vLog.INIZIALIZZATO := 1;

    vLog.LOG_FILE_NAME     := PKG_UtlGlb.GetLogFileName(vPref||GetLogFile(pClasseFunz));
    vLog.LOG_FILE_NAME_VRB := PKG_UtlGlb.GetLogFileName(vPref||'Verbose');
    
    vLog.FORCE_LOG_FILE    := pForceLogFile;

    --IF pLogArea.RUN_ID IS NULL THEN
        vLog.CODICI := t_TabLogCodici();
        vLog.INIZIO := CURRENT_TIMESTAMP;
    --END IF;

    vLog.STORE_ON_FILE := pStoreOnFile;
    vLog.CLASSE := SUBSTR(pClasseFunz,1,20);
    vLog.FUNZIONE := SUBSTR(pFunzione,1,100);
    vLog.DESCRIZIONE := SUBSTR(pDescrizione,1,100);
    vLog.MESSAGGIO := NULL;
    vLog.DATARIF := pDataRif;
    vLog.DATARIF_FINE := pDataRif_fine;
    vLog.ERRORE := NULL;

--    IF  pLogArea.DATARIF IS NOT NULL THEN
--        IF pLogArea.DATARIF = NVL(pLogArea.DATARIF_FINE,pLogArea.DATARIF) THEN
--            StdLogAddTxt('Data/Ora',StdOutDate(pLogArea.DATARIF),NULL,pLogArea);
--        ELSE
--            StdLogAddTxt('Periodo',StdOutDate(pLogArea.DATARIF)||' - '||StdOutDate(pLogArea.DATARIF_FINE),NULL,pLogArea);
--        END IF;
--    END IF;

    IF pCodice IS NOT NULL THEN
        StdLogAddTxt(pTipo,pCodice,pNome,vLog);
    END IF;

    IF vLog.HEADTXT IS NULL THEN
        IF LENGTH(NVL(vLog.FUNZIONE,'< no name >'||' - '||vLog.DESCRIZIONE)) > 70 OR vLog.DESCRIZIONE IS NULL THEN
            vLog.HEADTXT := RPAD(NVL(vLog.FUNZIONE,'< no name >'),70,' ');
            IF vLog.DESCRIZIONE IS NOT NULL THEN
                vLog.HEADTXT := vLog.HEADTXT || vLog.DESCRIZIONE;
            END IF;
        ELSE
            vLog.HEADTXT := RPAD(NVL(vLog.FUNZIONE,'< no name >')||' - '||vLog.DESCRIZIONE,70,' ');
        END IF;
        --IF gTraceLevel = PKG_UtlGlb.gcTrace_VRB THEN
        --    TraceLog(TRIM(vLog.HEADTXT)||' - session '||gSessionID,PKG_UtlGlb.gcTrace_VRB);
        --END IF;
    END IF;

    RETURN vLog;

EXCEPTION
    WHEN OTHERS THEN
--         ROLLBACK;
         TraceLog(pLogArea => vLog,
                  pTrace   => 'PKG_Logs.StdLogInit - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                  pLevel   => PKG_UtlGlb.gcTrace_ERR);
         RETURN vLog;

END StdLogInit;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE StdLogInit         (pClasseFunz    IN VARCHAR2,
                              pFunzione      IN VARCHAR2,
                              pDataRif       IN DATE     DEFAULT NULL,
                              pDataRif_fine  IN DATE     DEFAULT NULL,
                              pCodice        IN VARCHAR2 DEFAULT NULL,
                              pTipo          IN VARCHAR2 DEFAULT NULL,
                              pNome          IN VARCHAR2 DEFAULT NULL,
                              pDescrizione   IN VARCHAR2 DEFAULT NULL,
                              pStoreOnFile   IN BOOLEAN  DEFAULT TRUE,
                              pForceLogFile  IN BOOLEAN  DEFAULT TRUE) AS
BEGIN
 IF gLogCommonArea.INIZIALIZZATO = 1 THEN
    RETURN;
 END IF;
 gLogCommonArea := StdLogInit (pClasseFunz,
                               pFunzione,
                               pDataRif,
                               pDataRif_fine,
                               pCodice,
                               pTipo,
                               pNome,
                               pDescrizione,
                               pStoreOnFile,
                               pForceLogFile);
END StdLogInit;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ResetLogCommonArea AS
BEGIN
    gLogCommonArea := NULL;
END ResetLogCommonArea;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE StdLogAddTxt       (pTipo          IN VARCHAR2,
                              pCodice        IN VARCHAR2,
                              pNome          IN VARCHAR2 DEFAULT NULL,
                              pLogArea   IN OUT t_StandardLog) AS
    vLog   t_StandardLog;
BEGIN

    IF pCodice IS NULL THEN
        RETURN;
    END IF;

    IF pLogArea.CODICI.LAST IS NULL THEN
        pLogArea.CODICI.EXTEND;
        pLogArea.CODICI(pLogArea.CODICI.LAST).CODICE := SUBSTR(TRIM(pCodice),1,100);
        pLogArea.CODICI(pLogArea.CODICI.LAST).TIPO := SUBSTR(TRIM(pTipo),1,100);
        pLogArea.CODICI(pLogArea.CODICI.LAST).NOME := SUBSTR(TRIM(pNome),1,100);
        RETURN;
    END IF;
    FOR i IN pLogArea.CODICI.FIRST .. pLogArea.CODICI.LAST LOOP
        IF pLogArea.CODICI(i).CODICE = pCodice THEN
            pLogArea.CODICI(i).TIPO := SUBSTR(TRIM(pTipo),1,100);
            pLogArea.CODICI(i).NOME := SUBSTR(TRIM(pNome),1,100);
            RETURN;
        END IF;
    END LOOP;
    pLogArea.CODICI.EXTEND;
    pLogArea.CODICI(pLogArea.CODICI.LAST).CODICE := SUBSTR(TRIM(pCodice),1,100);
    pLogArea.CODICI(pLogArea.CODICI.LAST).TIPO := SUBSTR(TRIM(pTipo),1,100);
    pLogArea.CODICI(pLogArea.CODICI.LAST).NOME := SUBSTR(TRIM(pNome),1,100);

EXCEPTION
    WHEN OTHERS THEN
          IF SQLCODE = -06531 THEN -- Reference to uninitialized collection
              pLogArea := StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL,
                                     pFunzione     => 'PKG_Logs.StdLogAddTxt',
                                     pStoreOnFile  => TRUE,
                                     pDescrizione  => 'Area non inizializzata');
              StdLogAddTxt(pTipo,pCodice,pNome,pLogArea);
           ELSE
--            ROLLBACK;
--            vLog := StdLogInit(pClasseFunz => gcJobClassUTL||gcJobSubClassSRV,
--                               pFunzione   => 'PKG_Logs.StdLogAddTxt',
--                               pLogArea    => vLog);
--            StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
--            StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
              TraceLog(pLogArea => pLogArea,
                       pTrace   => 'PKG_Logs.StdLogAddTxt - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                       pLevel   => PKG_UtlGlb.gcTrace_ERR);
            RETURN;
          END IF;

END StdLogAddTxt;

PROCEDURE StdLogAddTxt       (pTipo          IN VARCHAR2,
                              pCodice        IN VARCHAR2,
                              pNome          IN VARCHAR2 DEFAULT NULL) AS
BEGIN

    StdLogAddTxt(pTipo,pCodice,pNome,gLogCommonArea);
    
END StdLogAddTxt;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE StdLogAddTxt       (pTesto         IN VARCHAR2,
                              pIndent        IN BOOLEAN  DEFAULT FALSE,
                              pErrore        IN NUMBER   DEFAULT NULL,
                              pLogArea   IN OUT t_StandardLog) AS
    vLog   t_StandardLog;
    vTesto VARCHAR2(4000) := SUBSTR(pTesto,1,4000);
BEGIN
    IF pTesto IS NULL THEN
        RETURN;
    END IF;
    IF NVL(pErrore,0) <> 0 THEN
        pLogArea.ERRORE := pErrore;
        IF pErrore < 0 THEN
            IF ABS(pErrore) >= 20000 THEN
                vTesto := SUBSTR(vTesto,INSTR(vTesto,': ')+2);
            END IF;
        END IF;
        StdLogInfo(vTesto,pLogArea,TRUE);
    ELSE
        IF NVL(INSTR(pLogArea.MESSAGGIO,vTesto),0) = 0 THEN
            IF pIndent THEN
                pLogArea.MESSAGGIO := SUBSTR( pLogArea.MESSAGGIO || CHR(10) || CHR(9) || vTesto, 1, 4000);
            ELSE
                pLogArea.MESSAGGIO := SUBSTR( pLogArea.MESSAGGIO || CHR(10) || vTesto, 1, 4000);
            END IF;
        ELSE
            pLogArea.MESSAGGIO := vTesto;
        END IF;
    END IF;

EXCEPTION
    WHEN OTHERS THEN
--         ROLLBACK;
--         vLog := StdLogInit(pClasseFunz => gcJobClassUTL||gcJobSubClassSRV,
--                            pFunzione   => 'PKG_Logs.StdLogAddTxt',
--                            pLogArea    => vLog);
--         StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
--         StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
           TraceLog(pLogArea => pLogArea,
                    pTrace   => 'PKG_Logs.StdLogAddTxt - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                    pLevel   => PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END StdLogAddTxt;

PROCEDURE StdLogAddTxt       (pTesto         IN VARCHAR2,
                              pIndent        IN BOOLEAN  DEFAULT FALSE,
                              pErrore        IN NUMBER   DEFAULT NULL) AS
BEGIN

    StdLogAddTxt(pTesto,pIndent,pErrore,gLogCommonArea);

END StdLogAddTxt;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE StdLogInfo         (pTesto         IN VARCHAR2,
                               pLogArea   IN OUT t_StandardLog,
                               pAggiungi      IN BOOLEAN DEFAULT TRUE) AS
    vLog      t_StandardLog;
    vErrDescr VARCHAR2(4000);
BEGIN
    IF pAggiungi THEN
        IF pLogArea.INFO IS NOT NULL THEN
            pLogArea.INFO := SUBSTR(pLogArea.INFO || CHR(10) || pTesto,1,4000);
        ELSE
            pLogArea.INFO := SUBSTR(pTesto,1,4000);
        END IF;
    ELSE
        pLogArea.INFO := SUBSTR(pTesto,1,4000);
    END IF;

EXCEPTION
    WHEN OTHERS THEN
--         ROLLBACK;
--         vLog := StdLogInit(pClasseFunz => gcJobClassUTL||gcJobSubClassSRV,
--                            pFunzione   => 'PKG_Logs.StdLogInfo',
--                            pLogArea    => vLog);
--         StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
--         StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
           TraceLog(pLogArea => pLogArea,
                    pTrace   => 'PKG_Logs.StdLogInfo - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                    pLevel   => PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END StdLogInfo;

PROCEDURE StdLogInfo         (pTesto         IN VARCHAR2,
                               pAggiungi      IN BOOLEAN DEFAULT TRUE) AS
BEGIN

    StdLogInfo(pTesto,gLogCommonArea,pAggiungi);

END StdLogInfo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE StdLogPrint        (pLogArea       IN t_StandardLog,
                              pLevel         IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF) AS
    --vLog         t_StandardLog := pLogArea;
    vNum         NUMBER;
    vTesto       VARCHAR2(32676) := '';
    vFunzione    LOG_HISTORY.PROC_NAME%TYPE := UPPER(TRIM(pLogArea.FUNZIONE));
    vStoreOnFile BOOLEAN;
    pLogAreaStep     INTEGER := 0;

    PROCEDURE PrintLogTxt (ptxt VARCHAR2) AS
            vtxt       VARCHAR2(32676) := ptxt;
        BEGIN
            vtxt := REPLACE(vtxt,CHR(10)||CHR(10)||CHR(10),CHR(10));
            vtxt := REPLACE(vtxt,CHR(10)||CHR(10),CHR(10));

            IF SUBSTR(vtxt,LENGTH(vtxt),1) = CHR(10) THEN
                TraceLog(pLogArea => pLogArea,
                         pTrace   => SUBSTR(vtxt,1,LENGTH(vtxt)-1),
                         pLevel   => pLevel);
            ELSE
                TraceLog(pLogArea => pLogArea,
                         pTrace   => vtxt,
                         pLevel   => pLevel);
            END IF;
        END;

    FUNCTION CompilaHeadLogTxt RETURN VARCHAR2 AS
        BEGIN
            --IF pLevel = PKG_UtlGlb.gcTrace_VRB THEN
            --    RETURN pLogArea.HEADTXT||gSessionID||' - '||SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - pLogArea.INIZIO),12,13)||CHR(10);
            --ELSE
                RETURN pLogArea.HEADTXT||' - '||SUBSTR(TO_CHAR(CURRENT_TIMESTAMP - pLogArea.INIZIO),12,13)||CHR(10);
            --END IF;
        END;

    PROCEDURE AddToLogText(ptxt VARCHAR2) AS
        BEGIN
           IF (LENGTH(vTesto) + LENGTH(ptxt)) > 25000 THEN
              pLogAreaStep := pLogAreaStep + 1;
              PrintLogTxt(vTesto||CHR(10)||'... continua con trace '||TO_CHAR(pLogAreaStep)||' ...');
              vTesto := CompilaHeadLogTxt || CHR(10) || '... trace '||TO_CHAR(pLogAreaStep)||' ...' || CHR(10) || SUBSTR(vTesto,30001) || ptxt || CHR(10);
           ELSE
              vTesto := vTesto || ptxt || CHR(10);
           END IF;
        END;
BEGIN

    vStoreOnFile := pLogArea.STORE_ON_FILE;

    vTesto := CompilaHeadLogTxt;

    IF  pLogArea.DATARIF IS NOT NULL THEN
        IF pLogArea.DATARIF = NVL(pLogArea.DATARIF_FINE,pLogArea.DATARIF) THEN
            AddToLogText(CHR(9)||RPAD('Data/Ora',20,' ')||': '||PKG_Mago.StdOutDate(pLogArea.DATARIF));
        ELSE
            AddToLogText(CHR(9)||RPAD('Periodo',20,' ') ||': '||PKG_Mago.StdOutDate(pLogArea.DATARIF)||' - '||PKG_Mago.StdOutDate(pLogArea.DATARIF_FINE));
        END IF;
    END IF;

    IF pLogArea.CODICI.LAST IS NOT NULL THEN
        FOR i IN pLogArea.CODICI.FIRST .. pLogArea.CODICI.LAST LOOP
            IF NVL(pLogArea.CODICI(i).NOME,'.') <> '.' THEN
                AddToLogText(CHR(9)||RPAD(SUBSTR(NVL(pLogArea.CODICI(i).TIPO,'Codice'),1,20),20,' ')||': '||
                             NVL(pLogArea.CODICI(i).CODICE,'<null>')||' (' || pLogArea.CODICI(i).NOME || ')');
            ELSE
                AddToLogText(CHR(9)||RPAD(SUBSTR(NVL(pLogArea.CODICI(i).TIPO,'Codice'),1,20),20,' ')||': '||
                             NVL(pLogArea.CODICI(i).CODICE,'<null>'));
            END IF;
        END LOOP;
    END IF;

    IF pLogArea.INFO IS NOT NULL THEN
        IF pLogArea.MESSAGGIO IS NOT NULL THEN
            AddToLogText(pLogArea.MESSAGGIO || CHR(10) || pLogArea.INFO);
        ELSE
            AddToLogText(pLogArea.INFO);
        END IF;
    ELSE
        IF pLogArea.MESSAGGIO IS NOT NULL THEN
            AddToLogText(pLogArea.MESSAGGIO);
        END IF;
    END IF;

    PrintLogTxt(vTesto);

    IF pLogArea.ERRORE IS NOT NULL THEN
        vStoreOnFile := TRUE;
    ELSE
        IF pLevel = PKG_UtlGlb.gcTrace_VRB THEN
            vStoreOnFile := FALSE;
        END IF;
    END IF;

    IF vStoreOnFile THEN
        SELECT LOG_SEQUENCE.NEXTVAL INTO vNum FROM DUAL;
--        pLogArea.RUN_ID := vNum;

--        UPDATE APPLICATION_RUN  SET RUN_ID   = vNum,
--                                    ERR_FLG  = DECODE(pLogArea.ERRORE,NULL,1,0),
--                                    LAST_UPD = pLogArea.INIZIO
--         WHERE PROC_NAME = vFunzione;
--        IF SQL%ROWCOUNT = 0 THEN
--            INSERT INTO APPLICATION_RUN (RUN_ID,
--                                         PROC_NAME,
--                                         ERR_FLG,
--                                         LAST_UPD)
--                                 VALUES (vNum,
--                                         vFunzione,
--                                         DECODE(pLogArea.ERRORE,NULL,1,0),
--                                         pLogArea.INIZIO);
--        END IF;

        INSERT INTO LOG_HISTORY      (RUN_ID,
                                      LOGDATE,
                                      PROC_NAME,
                                      MESSAGE,
                                      ERROR_NUM,
                                      CLASSE,
                                      ELAPSED,
                                      DATA_RIF,
                                      DATA_RIF_TO)
                              VALUES (vNum,
                                      pLogArea.INIZIO,
                                      vFunzione,
                                      NVL(pLogArea.INFO,pLogArea.DESCRIZIONE),
                                      pLogArea.ERRORE,
                                      pLogArea.CLASSE,
                                      CURRENT_TIMESTAMP - pLogArea.INIZIO,
                                      pLogArea.DATARIF,
                                      pLogArea.DATARIF_FINE);
        IF pLogArea.CODICI.LAST IS NOT NULL THEN
            FOR i IN pLogArea.CODICI.FIRST .. pLogArea.CODICI.LAST LOOP
                INSERT INTO LOG_HISTORY_INFO (RUN_ID,
                                              CODICE,
                                              TIPO,
                                              NOME,
                                              SEQ)
                                      VALUES (vNum,
                                              pLogArea.CODICI(i).CODICE,
                                              NVL(pLogArea.CODICI(i).TIPO,'<null>'),
                                              pLogArea.CODICI(i).NOME,
                                              i);
            END LOOP;
        END IF;
        --COMMIT;
    END IF;

EXCEPTION
    WHEN OTHERS THEN
--         ROLLBACK;
--         vLog := StdLogInit(pClasseFunz => gcJobClassUTL||gcJobSubClassSRV,
--                            pFunzione   => 'PKG_Logs.StdLogPrint',
--                            pLogArea    => vLog);
--         StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
--         StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
           TraceLog(pLogArea => pLogArea,
                    pTrace   => 'PKG_Logs.StdLogPrint - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,
                    pLevel   => PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END StdLogPrint;

PROCEDURE StdLogPrint        (pLevel         IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF) AS
BEGIN

    StdLogPrint (gLogCommonArea,pLevel);

END StdLogPrint;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE TraceLog           (pLogArea       IN t_StandardLog,
                              pTrace         IN VARCHAR2,
                              pLevel         IN INTEGER  DEFAULT PKG_UtlGlb.gcTrace_INF,
                              pIndent        IN BOOLEAN  DEFAULT FALSE) AS
/*-----------------------------------------------------------------------------------------------------------
    Traccia un messaggio generico sul file di log
-----------------------------------------------------------------------------------------------------------*/
    vTxt   VARCHAR2(32676);
    vFile  VARCHAR2(50);
BEGIN

    IF NOT gVerboseLog THEN
        IF pLevel =  PKG_UtlGlb.gcTrace_VRB THEN
            RETURN;
        END IF;
    END IF;

    IF pLevel = PKG_UtlGlb.gcTrace_VRB THEN
        vFile := pLogArea.LOG_FILE_NAME_VRB;
    ELSE
        vFile := pLogArea.LOG_FILE_NAME;
    END IF;
        
    IF pIndent THEN
        vTxt := SUBSTR('   '||pTrace,1,32676);
    ELSE
        vTxt := SUBSTR(pTrace,1,32676);
    END IF;

    IF pLevel <> PKG_UtlGlb.gcTrace_VRB THEN
        PRINT(pTrace);
    END IF;

    PKG_UtlGlb.TraceLog(p_dirname  => gcLogDir,
                        p_filename => vFile,
                        p_trace    => vTxt,
                        p_level    => pLevel);
EXCEPTION
    WHEN OTHERS THEN
--         ROLLBACK;
--         vLog := StdLogInit(pClasseFunz => gcJobClassUTL||gcJobSubClassSRV,
--                            pFunzione   => 'PKG_Logs.StdLogPrint',
--                            pLogArea    => vLog);
--         StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
--         StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         TraceLog(pLogArea,'PKG_Logs.TraceLog - '||SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,PKG_UtlGlb.gcTrace_ERR);
         RETURN;

END TraceLog;

PROCEDURE TraceLog           (pTrace         IN VARCHAR2,
                              pLevel         IN INTEGER  DEFAULT PKG_UtlGlb.gcTrace_INF,
                              pIndent        IN BOOLEAN  DEFAULT FALSE) AS
BEGIN

    TraceLog(gLogCommonArea,pTrace,pLevel,pIndent);

END TraceLog;

-- ----------------------------------------------------------------------------------------------------------


/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    gTraceLevel := PKG_UtlGlb.GetParamGen(PKG_UtlGlb.gParamKeyTraceLevel,TO_CHAR(PKG_UtlGlb.gcTrace_INF));
    gSessionID := 'Sid,Serial#:  '|| SYS_CONTEXT('USERENV','SID') ||','|| DBMS_DEBUG_JDWP.CURRENT_SESSION_SERIAL;
    
    IF PKG_Mago.IsmagoDGF THEN
        PKG_UtlGlb.gv_FilLog    := PKG_UtlGlb.GetLogFileName('Dgf_'||GetLogFile('_')); 
        PKG_UtlGlb.gv_FilLogVrb := PKG_UtlGlb.GetLogFileName('Dgf_Verbose');
    ELSE
        PKG_UtlGlb.gv_FilLog    := PKG_UtlGlb.GetLogFileName('Mag_'||GetLogFile('_')); 
        PKG_UtlGlb.gv_FilLogVrb := PKG_UtlGlb.GetLogFileName('Mag_Verbose');
    END IF;

    IF PKG_UtlGlb.GetParamGen(PKG_UtlGlb.gParamKeyTraceLevel,'1') = '0'  THEN   -- 0 = Verbose
        gVerboseLog := TRUE;  -- Attivato modo verbose
    ELSE
        gVerboseLog := FALSE;
    END IF;


END PKG_Logs;
/

SHOW ERRORS;


