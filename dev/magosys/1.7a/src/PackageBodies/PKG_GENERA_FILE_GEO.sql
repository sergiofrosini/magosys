PROMPT  pkg_genera_file_geo

CREATE OR REPLACE PACKAGE BODY pkg_genera_file_geo AS
PROCEDURE genera_file_geo(v_filename IN VARCHAR2) IS
   v_outfile  UTL_FILE.FILE_TYPE;
   v_amount   INTEGER := 32767;
   v_dirname VARCHAR2(30) := 'TO_METEODATA_DIR' ;
   CURSOR c_users IS 
          SELECT username 
            FROM all_users 
           WHERE username LIKE 'MAGO_%'
           MINUS
          SELECT username 
            FROM user_users;
   rec_users c_users%ROWTYPE;
   TYPE filerowtype IS TABLE OF VARCHAR2(300);
   rec_filerowtype filerowtype;
   v_query_src VARCHAR2(1000) := 'select cod_geo || '';''' ||
                             ' || tipo_punto || '';''' ||
                             ' || tipo_previsione || '';''' ||
                             ' || replace(coord_lat,'','',''.'') || '';''' ||
                             ' || replace(coord_long,'','',''.'') || '';''' ||
                             ' || replace(alt,'','',''.'') || '';''' ||
                             ' || replace(altezza_sul_terreno,'','',''.'') || '';''' ||
                             ' from <OWNER>.v_punti_geo ' ||
                             ' order by 1 ';
   v_header VARCHAR2(1000) := 'COD_GEO;TIPO_PUNTO;TIPO_PREVISIONE;COORD_LAT;COORD_LONG;ALT;ALTEZZA_SUL_TERRENO;';
   v_query VARCHAR2(1000);
   i NUMBER;
  BEGIN
    -- Open the destination file.
    v_outfile := UTL_FILE.FOPEN(v_dirname, v_filename, 'w',v_amount);
    UTL_FILE.PUT_LINE(v_outfile ,v_header, TRUE);
    IF pkg_mago.isnazionale THEN
       FOR rec_users IN c_users LOOP
          BEGIN
             v_query := REPLACE (v_query_src,'<OWNER>',rec_users.username);
             EXECUTE IMMEDIATE v_query BULK COLLECT INTO rec_filerowtype;
             FOR i in 1 .. rec_filerowtype.COUNT LOOP
                UTL_FILE.PUT_LINE(v_outfile ,rec_filerowtype(i), TRUE);
             END LOOP;
          EXCEPTION WHEN OTHERS THEN NULL;
          END;
       END LOOP;
    ELSE
             v_query := REPLACE (v_query_src,'<OWNER>.','');
             EXECUTE IMMEDIATE v_query BULK COLLECT INTO rec_filerowtype;
             FOR i in 1 .. rec_filerowtype.COUNT LOOP
                UTL_FILE.PUT_LINE(v_outfile ,rec_filerowtype(i), TRUE);
             END LOOP;    
    END IF;
    -- Close the file.
    UTL_FILE.FCLOSE(v_outfile );
    EXCEPTION
      WHEN OTHERS THEN
        UTL_FILE.FCLOSE(v_outfile );
  END; 
PROCEDURE genera_file_correlazione(v_filename IN VARCHAR2) IS
   v_outfile  UTL_FILE.FILE_TYPE;
   v_amount   INTEGER := 32767;
   v_dirname VARCHAR2(30) := 'TO_METEODATA_DIR' ;
   CURSOR c_users IS 
          SELECT username 
            FROM all_users 
           WHERE username LIKE 'MAGO_%'
           MINUS
          SELECT username 
            FROM user_users;
   rec_users c_users%ROWTYPE;
   TYPE filerowtype IS TABLE OF VARCHAR2(300);
   rec_filerowtype filerowtype;
   v_query_src VARCHAR2(1000) := 'select distinct cod_geo || '';''' ||
                             ' || cod_geo_a || '';''' ||
                             ' FROM <OWNER>.elementi_def ' ||
                             ' WHERE cod_geo IS NOT NULL ' ||
                             ' AND data_disattivazione = to_date(''01013000'',''ddmmyyyy'')' ||
                             ' ORDER BY 1 ';
   v_header VARCHAR2(1000) := 'COD_GEO_P;COD_GEO_A;';
   v_query VARCHAR2(1000);
   i NUMBER;
  BEGIN
    -- Open the destination file.
    v_outfile := UTL_FILE.FOPEN(v_dirname, v_filename, 'w',v_amount);
    UTL_FILE.PUT_LINE(v_outfile ,v_header, TRUE);
    IF pkg_mago.isnazionale THEN
       FOR rec_users IN c_users LOOP
          BEGIN
             v_query := REPLACE (v_query_src,'<OWNER>',rec_users.username);
             EXECUTE IMMEDIATE v_query BULK COLLECT INTO rec_filerowtype;
             FOR i in 1 .. rec_filerowtype.COUNT LOOP
                UTL_FILE.PUT_LINE(v_outfile ,rec_filerowtype(i), TRUE);
             END LOOP;
          EXCEPTION WHEN OTHERS THEN NULL;
          END;
       END LOOP;
    ELSE
       v_query := REPLACE (v_query_src,'<OWNER>.','');
       EXECUTE IMMEDIATE v_query BULK COLLECT INTO rec_filerowtype;
       FOR i in 1 .. rec_filerowtype.COUNT LOOP
          UTL_FILE.PUT_LINE(v_outfile ,rec_filerowtype(i), TRUE);
       END LOOP;   
    END IF;
    -- Close the file.
    UTL_FILE.FCLOSE(v_outfile );
    EXCEPTION
      WHEN OTHERS THEN
        UTL_FILE.FCLOSE(v_outfile );
  END; 
END;
/

SHOW ERRORS;
