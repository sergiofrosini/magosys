Prompt Package Body PKG_STATS;
--
-- PKG_STATS  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_STATS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.6.a
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gStartDate         DATE;

 gTipoInstallazione DEFAULT_CO.TIPO_INST%TYPE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckSessions AS
/*-----------------------------------------------------------------------------------------------------------
    Controlla le sessioni ancora aperte ed aggiorna lo stato di avanzamento delle aggregazioni
-----------------------------------------------------------------------------------------------------------*/
    vLog                  PKG_Logs.t_StandardLog;
    vCount                NUMBER;
    vStatusAggregProgress CONSTANT SESSION_STATISTICS.STATUS %TYPE := 'AGGREGATION_PROGRESS';
    vStatusAggregDone     CONSTANT SESSION_STATISTICS.STATUS %TYPE := 'DONE';
    vKeyOfflineParam      CONSTANT MEASURE_OFFLINE_PARAMETERS.KEY%TYPE := 'STWebRegistry-WS.measure.offline.forecast_info_mds_serviceid';  
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassSRV,
                                pFunzione     => 'PKG_Stats.CheckSessions',
                                pForceLogFile => TRUE);

    FOR i IN (SELECT * FROM SESSION_STATISTICS 
               WHERE AGGREG_COMPLETED = 0
                 AND SERVICE_ID = (SELECT VALUE FROM MEASURE_OFFLINE_PARAMETERS WHERE KEY = vKeyOfflineParam)
                 AND LEAF_END_DATE IS NOT NULL
             ) LOOP

        IF i.AGGREG_START_DATE IS NULL THEN
            
            PKG_Logs.StdLogAddTxt(i.SERVICE_ID||' '||i.SERVICE_REF||' '||i.COD_TIPO_MISURA||' :  '||vStatusAggregDone,TRUE,NULL,vLog);
        
            UPDATE SESSION_STATISTICS SET AGGREG_START_DATE = SYSDATE,
                                          STATUS = vStatusAggregProgress
             WHERE COD_SESSIONE = i.COD_SESSIONE
               AND DATE_FROM = i.DATE_FROM
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;

        SELECT COUNT(*) INTO vCount
          FROM SCHEDULED_JOBS
         WHERE DATARIF BETWEEN i.DATE_FROM AND i.DATE_TO
           AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;

        IF vCount = 0 THEN

            PKG_Logs.StdLogAddTxt(i.SERVICE_ID||' '||i.SERVICE_REF||' '||i.COD_TIPO_MISURA||' :  '||vStatusAggregProgress,TRUE,NULL,vLog);

            UPDATE SESSION_STATISTICS SET AGGREG_END_DATE = SYSDATE,
                                          STATUS = vStatusAggregDone,
                                          AGGREG_COMPLETED = 1
             WHERE COD_SESSIONE = i.COD_SESSIONE
               AND DATE_FROM = i.DATE_FROM
               AND COD_TIPO_MISURA = i.COD_TIPO_MISURA;
        END IF;

        COMMIT;

    END LOOP;

    PKG_Logs.StdLogPrint(vLog);

 EXCEPTION
    WHEN OTHERS THEN
         -- annulla gli aggiornamenti non ancora committati.
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         COMMIT;
         RAISE;

END CheckSessions;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetStatisticSessionID  (pSessionID     OUT NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce un nuovo identificativo di sessione Forecast
-----------------------------------------------------------------------------------------------------------*/
BEGIN
        SELECT SESSION_STATISTIC_IDSEQ.NEXTVAL INTO pSessionID FROM DUAL;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Stats.GetStatisticSessionID'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetStatisticSessionID;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitStatisticSession (pCodSession    SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                pServiceID     SESSION_STATISTICS.SERVICE_ID%TYPE,
                                pServiceRef    SESSION_STATISTICS.SERVICE_REF%TYPE,
                                pTipMisura     SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                pStatus        SESSION_STATISTICS.STATUS%TYPE,
                                pDateFrom      SESSION_STATISTICS.DATE_FROM%TYPE,
                                pDateTo        SESSION_STATISTICS.DATE_TO%TYPE,
                                pLeafStartDate SESSION_STATISTICS.LEAF_START_DATE%TYPE DEFAULT NULL,
                                pLeafEndDate   SESSION_STATISTICS.LEAF_END_DATE%TYPE DEFAULT NULL,
                                pAggrStartDate SESSION_STATISTICS.AGGREG_START_DATE%TYPE DEFAULT NULL,
                                pAggrEndDate   SESSION_STATISTICS.AGGREG_END_DATE%TYPE DEFAULT NULL,
                                pAggrCompleted SESSION_STATISTICS.AGGREG_COMPLETED%TYPE DEFAULT NULL
                               )  AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza/Aggiorna una tupla di statistica sessione
-----------------------------------------------------------------------------------------------------------*/
BEGIN

    MERGE INTO SESSION_STATISTICS S
         USING
              ( SELECT pCodSession    COD_SESSIONE,
                       pServiceID     SERVICE_ID,
                       pServiceRef    SERVICE_REF,
                       pTipMisura     COD_TIPO_MISURA,
                       pStatus        STATUS,
                       pDateFrom      DATE_FROM,
                       pDateTo        DATE_TO,
                       pLeafStartDate LEAF_START_DATE,
                       pLeafEndDate   LEAF_END_DATE,
                       pAggrStartDate AGGREG_START_DATE,
                       pAggrEndDate   AGGREG_END_DATE,
                       pAggrCompleted AGGREG_COMPLETED
                  FROM DUAL
              ) X
            ON (    S.COD_SESSIONE      = X.COD_SESSIONE
                AND S.COD_TIPO_MISURA   = X.COD_TIPO_MISURA
                AND S.DATE_FROM         = X.DATE_FROM)
          WHEN MATCHED THEN
        UPDATE SET  S.SERVICE_ID        = X.SERVICE_ID,
                    S.SERVICE_REF       = X.SERVICE_REF,
                    S.STATUS            = X.STATUS,
                    S.DATE_TO           = X.DATE_TO,
                    S.LEAF_START_DATE   = X.LEAF_START_DATE,
                    S.LEAF_END_DATE     = X.LEAF_END_DATE,
                    S.AGGREG_START_DATE = X.AGGREG_START_DATE,
                    S.AGGREG_END_DATE   = X.AGGREG_END_DATE,
                    S.AGGREG_COMPLETED  = X.AGGREG_COMPLETED
          WHEN NOT MATCHED THEN
        INSERT (COD_SESSIONE, SERVICE_ID, SERVICE_REF, COD_TIPO_MISURA, STATUS,
                DATE_FROM, DATE_TO, LEAF_START_DATE, LEAF_END_DATE,
                AGGREG_START_DATE, AGGREG_END_DATE, AGGREG_COMPLETED)
        VALUES (X.COD_SESSIONE, X.SERVICE_ID, X.SERVICE_REF, X.COD_TIPO_MISURA, X.STATUS,
                X.DATE_FROM, X.DATE_TO, X.LEAF_START_DATE, X.LEAF_END_DATE,
                X.AGGREG_START_DATE, X.AGGREG_END_DATE, X.AGGREG_COMPLETED);

    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Stats.InitStatisticSession'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END InitStatisticSession;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Stats;
/

SHOW ERRORS;


