ALTER TABLE ELEMENTI DISABLE CONSTRAINT TPELE_ELE;
ALTER TABLE TRATTAMENTO_ELEMENTI DISABLE CONSTRAINT TPELE_TRTELE;
ALTER TABLE ELEMENTI_DEF DISABLE CONSTRAINT TPELE_ELEDEF;
ALTER TABLE GRUPPI_MISURA DISABLE CONSTRAINT TPELE_GRPMIS;
ALTER TRIGGER BEF_IUR_TRATTAMENTO_ELEMENTI DISABLE;

UPDATE TIPI_ELEMENTO  SET COD_TIPO_ELEMENTO = CASE COD_TIPO_ELEMENTO
                                      WHEN 'PAT' THEN 'CAT' 
                                      WHEN 'PMT' THEN 'CMT' 
                                      WHEN 'PBT' THEN 'CBT'
                                   END 
 WHERE COD_TIPO_ELEMENTO IN ('PAT', 'PMT', 'PBT');                                                 

UPDATE ELEMENTI  SET COD_TIPO_ELEMENTO = CASE COD_TIPO_ELEMENTO
                                      WHEN 'PAT' THEN 'CAT' 
                                      WHEN 'PMT' THEN 'CMT' 
                                      WHEN 'PBT' THEN 'CBT'
                                   END 
 WHERE COD_TIPO_ELEMENTO IN ('PAT', 'PMT', 'PBT');                                                 

UPDATE TRATTAMENTO_ELEMENTI  SET COD_TIPO_ELEMENTO = CASE COD_TIPO_ELEMENTO
                                      WHEN 'PAT' THEN 'CAT' 
                                      WHEN 'PMT' THEN 'CMT' 
                                      WHEN 'PBT' THEN 'CBT'
                                   END 
 WHERE COD_TIPO_ELEMENTO IN ('PAT', 'PMT', 'PBT');                                                 

UPDATE ELEMENTI_DEF SET COD_TIPO_ELEMENTO = CASE COD_TIPO_ELEMENTO
                                      WHEN 'PAT' THEN 'CAT' 
                                      WHEN 'PMT' THEN 'CMT' 
                                      WHEN 'PBT' THEN 'CBT'
                                   END 
 WHERE COD_TIPO_ELEMENTO IN ('PAT', 'PMT', 'PBT');                                                 

UPDATE GRUPPI_MISURA SET COD_TIPO_ELEMENTO = CASE COD_TIPO_ELEMENTO
                                      WHEN 'PAT' THEN 'CAT' 
                                      WHEN 'PMT' THEN 'CMT' 
                                      WHEN 'PBT' THEN 'CBT'
                                   END 
 WHERE COD_TIPO_ELEMENTO IN ('PAT', 'PMT', 'PBT');                                                 

COMMIT;

ALTER TABLE ELEMENTI ENABLE CONSTRAINT TPELE_ELE;
ALTER TABLE TRATTAMENTO_ELEMENTI ENABLE CONSTRAINT TPELE_TRTELE;
ALTER TABLE ELEMENTI_DEF ENABLE CONSTRAINT TPELE_ELEDEF;
ALTER TABLE GRUPPI_MISURA ENABLE CONSTRAINT TPELE_GRPMIS;
ALTER TRIGGER BEF_IUR_TRATTAMENTO_ELEMENTI ENABLE;

ALTER TABLE TIPI_PRODUTTORE RENAME TO TIPI_CLIENTE;
ALTER TABLE TIPI_CLIENTE RENAME COLUMN COD_TIPO_PRODUTTORE TO COD_TIPO_CLIENTE;
ALTER TABLE TIPI_CLIENTE RENAME COLUMN ID_PRODUTTORE TO ID_CLIENTE;
ALTER TABLE TIPI_CLIENTE ADD (FORNITORE  NUMBER(1));
ALTER TABLE TIPI_CLIENTE ADD CHECK (FORNITORE IN (0,1));
ALTER INDEX TIPI_PRODUTTORE_PK RENAME TO TIPI_CLIENTE_PK;
UPDATE TIPI_CLIENTE SET DESCRIZIONE = 'Produttore '||DESCRIZIONE, FORNITORE = 1;
INSERT INTO TIPI_CLIENTE (COD_TIPO_CLIENTE, DESCRIZIONE, ID_CLIENTE, FORNITORE) VALUES ('C','Cliente',16,0);
UPDATE TIPI_CLIENTE SET DESCRIZIONE = 'Produttore Puro' WHERE COD_TIPO_CLIENTE = 'A';
UPDATE TIPI_CLIENTE SET DESCRIZIONE = 'AutoProduttore'  WHERE COD_TIPO_CLIENTE = 'B';
COMMIT; 
ALTER TABLE TIPI_CLIENTE MODIFY(FORNITORE  NOT NULL);
ALTER TABLE TIPI_CLIENTE MODIFY(FORNITORE  DEFAULT 0);

COMMENT ON TABLE TIPI_CLIENTE IS 'Tabella dei tipi Cliente previsti';
COMMENT ON COLUMN TIPI_CLIENTE.COD_TIPO_CLIENTE IS 'Codice Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.DESCRIZIONE IS 'Descrizione del Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.ID_CLIENTE IS 'Idefntificativo del Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.FORNITORE IS '1=Fornitore';

ALTER TABLE ELEMENTI_DEF RENAME COLUMN COD_TIPO_PRODUTTORE TO COD_TIPO_CLIENTE;
ALTER TABLE ELEMENTI_DEF RENAME CONSTRAINT TPPRD_ELEDEF TO TPCLI_ELEDEF;
COMMENT ON COLUMN ELEMENTI_DEF.COD_TIPO_CLIENTE IS 'Tipo Cliente dell''elemento (se applicabile)';
COMMENT ON COLUMN ELEMENTI_DEF.ID_ELEMENTO IS 'Codice gestionale dell''elemento da cui dipende - per Generatori, Clienti e Sbarre di CS ';

ALTER TABLE TRATTAMENTO_ELEMENTI RENAME COLUMN COD_TIPO_PRODUTTORE TO COD_TIPO_CLIENTE;
ALTER TABLE TRATTAMENTO_ELEMENTI RENAME CONSTRAINT TPPRD_TRTELE TO TPCLI_TRTELE;
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_CLIENTE IS 'Codice Tipo Cliente';

ALTER TABLE GTTD_IMPORT_GERARCHIA RENAME COLUMN COD_TIPO_PRODUTTORE TO COD_TIPO_CLIENTE;

