Prompt Package PKG_ANAGRAFICHE;
--
-- PKG_ANAGRAFICHE  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   STANDARD (Package)
--   DBMS_STANDARD (Package)
--   USER_VIEWS (Synonym)
--   DBMS_UTILITY (Synonym)
--   DBMS_LOCK (Synonym)
--   DBMS_OUTPUT (Synonym)
--   DBMS_SCHEDULER (Synonym)
--   DBMS_MVIEW (Synonym)
--   UNITA_TERRITORIALI (Table)
--   PKG_GESTANAGR (Synonym)
--   UPDATE_AUI_TLC (Table)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   ESERCIZI (Table)
--   DATE_IMPORT_CONSISTENZA (Table)
--   AVVOLGIMENTI_SA (Table)
--   UTILIZZO_TLC (Table)
--   TRASFORMATORIAT_SA (Table)
--   SEMAFORO (Table)
--   GENERATORI_TLC (Table)
--   TRASF_PROD_BT_TLC (Table)
--   MISURE_AGGREGATE (Table)
--   ELEMENTI_DEF (Table)
--   REL_ELEMENTI_GEO (Table)
--   REL_ELEMENTI_AMM (Table)
--   REL_ELEMENTI_ECP_SA (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_RETE (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   ELEMENTI (Table)
--   ELEMENTI (Table)
--   TIPI_MISURA (Table)
--   DEFAULT_CO (Table)
--   REL_ELEMENTI_ECS_SA (Table)
--   STORICO_IMPORT (Table)
--   SCHEDULED_TMP_GEN (Table)
--   GTTD_IMPORT_GERARCHIA (Table)
--   GTTD_MOD_ASSETTO_RETE_SA (Table)
--   GTTD_VALORI_TEMP (Table)
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--   PKG_MAGO (Package)
--   PKG_ELEMENTI (Package)
--   PKG_AGGREGAZIONI (Package)
--   PKG_MISURE (Package)
--   PKG_SCHEDULER (Package)
--   PKG_LOGS (Package)
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--   DBMS_SCHEDULER ()
--   PKG_GESTANAGR ()
--   USER_VIEWS ()
--   V_GERARCHIA_GEOGRAFICA (View)
--   V_GERARCHIA_AMMINISTRATIVA (View)
--   DBMS_MVIEW ()
--   V_MOD_ASSETTO_RETE_SA (View)
--   DBMS_LOCK ()
--
CREATE OR REPLACE PACKAGE "PKG_ANAGRAFICHE" AS

/* ***********************************************************************************************************
   NAME:       PKG_Anagrafiche
   PURPOSE:    Utilities e Definizioni schema CORELE (SOLO modalita' STM)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      21/02/2012  Moretti C.       Created this package.
   1.0.d      20/04/2012  Moretti C.       Avanzamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.1.c      13/05/2013  Moretti C.       Controllo codici gestionali
   1.1.h      14/03/2014  Moretti C.       Eliminata forzatura introdotta in attesa della gestione
                                           file nea  IMTRAP.nea 
   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitApplMagoSTM     (pCodGestESE     IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                               pData           IN DATE DEFAULT TRUNC(SYSDATE));

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetListaAttualizzazioni (pRefCurs   OUT PKG_UtlGlb.t_query_cur) ;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElementsCheckReport (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pStato          IN INTEGER,
                               pData           IN DATE);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckCodGestCorele  (pStato          IN INTEGER,
                               pBlockOnly      IN BOOLEAN,
                               pData           IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AllineaAnagrafica  (pElabSincrona   IN INTEGER DEFAULT PKG_UtlGlb.gkFlagON,
                               pForzaCompleta  IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOFF);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraGerarchia     (pData          IN DATE,
                                pStato         IN INTEGER,
                                pModAssetto    IN BOOLEAN,
                                pAUI           IN BOOLEAN,
                                pElabSincrona  IN BOOLEAN,
                                pForzaCompleta IN BOOLEAN);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Anagrafiche;
/

SHOW ERRORS;


