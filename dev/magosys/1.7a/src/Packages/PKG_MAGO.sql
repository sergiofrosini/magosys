Prompt Package PKG_MAGO;
--
-- PKG_MAGO  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_ELEMENTI (Package)
--   PKG_LOGS (Package)
--   PKG_MISURE (Package)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   DUAL ()
--   DBMS_UTILITY ()
--   PLITBLM ()
--   TIPI_CLIENTE (Table)
--   TIPI_CLIENTE (Table)
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--   GTTD_VALORI_TEMP (Table)
--   GTTD_VALORI_TEMP (Table)
--   DBMS_OUTPUT ()
--   RAGGRUPPAMENTO_FONTI (Table)
--   TIPI_MISURA (Table)
--   TIPI_MISURA (Table)
--   TIPI_GRUPPI_MISURA (Table)
--   TIPI_RETE (Table)
--   TIPI_RETE (Table)
--   TIPO_FONTI (Table)
--   TIPO_FONTI (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_MISURA_CONV_UM (Table)
--   ELEMENTI (Table)
--   ELEMENTI (Table)
--   DEFAULT_CO (Table)
--   DEFAULT_CO (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   GRUPPI_MISURA (Table)
--
CREATE OR REPLACE PACKAGE "PKG_MAGO" AS

/* ***********************************************************************************************************
   NAME:       PKG_Mago
   PURPOSE:    Definizioni e Servizi specifici dell'applicazione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      19/09/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Versione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.7.a.2    11/04/2014  Moretti C.       .....

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_Misura              IS RECORD (COD_TRATTAMENTO_ELEM NUMBER,
                                       DATA                 DATE,
                                       QUALITA              RAW(2),
                                       VALORE               NUMBER);
 TYPE t_TabMisure           IS TABLE OF t_Misura;

-- ----------------------------------------------------------------------------------------------------------

 cSepCharLst        CONSTANT CHAR(1)   := CHR(124);  -- |
 cSeparatore                 CHAR(1)   := CHR(167);  -- �

-- ----------------------------------------------------------------------------------------------------------

 gcON               CONSTANT NUMBER(1) := 1;
 gcOFF              CONSTANT NUMBER(1) := 0;

 gcNullNumCode      CONSTANT NUMBER(1) := -1;

 gcNoOper           CONSTANT NUMBER(1) := 0;
 gcInserito         CONSTANT NUMBER(1) := 1;
 gcModificato       CONSTANT NUMBER(1) := 2;

 gcOrganizzazELE    CONSTANT NUMBER(1) := 1;        -- organizzazione elettrica
 gcOrganizzazGEO    CONSTANT NUMBER(1) := 2;        -- organizzazione geografica (Istat)
 gcOrganizzazAMM    CONSTANT NUMBER(1) := 3;        -- organizzazione amministrativa

 gcStatoNullo       CONSTANT NUMBER(1) := 0;        -- stato non applicabile
 gcStatoNormale     CONSTANT NUMBER(1) := 1;        -- stato normale
 gcStatoAttuale     CONSTANT NUMBER(1) := 2;        -- stato attuale

 gcTabAcquisite     CONSTANT NUMBER(1) := 1;        -- Misure in tabella MISURE_ACQUISITE
 gcTabAggregate     CONSTANT NUMBER(1) := 2;        -- Misure in tabella MISURE_AGGREGATE

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Installazione
-- ----------------------------------------------------------------------------------------------------------

 gcMagoNazionale    CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 1;
 gcMagoSTM          CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 2;
 gcMagoDGF          CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 4;

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Elemento
-- ----------------------------------------------------------------------------------------------------------

 gcNazionale              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'NAZ'; -- nazionale
 gcRegione                CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'REG'; -- regione
 gcProvincia              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'PRV'; -- provincia
 gcComune                 CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'COM'; -- comune
 gcCFT                    CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CFT'; -- CFT
 gcZona                   CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ZNA'; -- zona
 gcCentroOperativo        CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'COP'; -- centro operativo
 gcEsercizio              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ESE'; -- esercizio

 gcCabinaPrimaria         CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CPR'; -- Cabina primaria
 gcSbarraAT               CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SAT'; -- Sbarra AT
 gcTrasfCabPrim           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRF'; -- Trasformatore di cabina primaria
 gcSecondarioDiTrasf      CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRS'; -- Secondario di trasformatore
 gcTerziarioDiTrasf       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRT'; -- Terziario di trasformatore
 gcSbarraMT               CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SMT'; -- Sbarra MT
 gcLineaMT                CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'LMT'; -- Linea MT
 gcCabinaSecondaria       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CSE'; -- cabina secondaria
 gcSbarraCabSec           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SCS'; -- sbarra di cabina secondaria
 gcTrasformMtBt           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRM'; -- Trasformatore BT/BT
 gcTrasformMtBtDett       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRX'; -- Dettaglio di Trasformatore BT/BT

 gcClienteAT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CAT'; -- Cliente alta tensione
 gcClienteMT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CMT'; -- Cliente media tensione
 gcClienteBT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CBT'; -- Cliente bassa tensione

 gcGeneratoreAT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GAT'; -- Generatore alta tensione
 gcGeneratoreMT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GMT'; -- Generatore media tensione
 gcGeneratoreBT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GBT'; -- Generatore bassa tensione

 gcAreaSconnessa          CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ZSC'; -- Area Sconnessa

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Misura
-- ----------------------------------------------------------------------------------------------------------

 gcPotenzaAttScambiata    CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAS'; -- Potenza Attiva Scambiata
 gcPotenzaAttGenerata     CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAG'; -- Potenza Attiva Generata
 gcPotenzaInstallata      CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PI';  -- Potenza Installata
 --gcPotenzaContrattuale    CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PC';  -- Potenza Contrattuale
 gcPotenzaMeteoPrevisto   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PMP'; -- Potenza Meteo Previsto
 gcNumeroImpianti         CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'NRI'; -- Numero Impianti

-- ----------------------------------------------------------------------------------------------------------
-- Raggruppamento Fonti
-- ----------------------------------------------------------------------------------------------------------
 gcRaggrFonteSolare       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'S';  -- Solare
 gcRaggrFonteSolareID     CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 1;    -- Solare
 gcRaggrFonteEolica       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'E';  -- Elolica
 gcRaggrFonteEolicaID     CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 2;    -- Elolica
 gcRaggrFonteIdraulica    CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'I';  -- Idraulica
 gcRaggrFonteIdraulicaID  CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 4;    -- Idraulica
 gcRaggrFonteTermica      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'T';  -- Termica
 gcRaggrFonteTermicaID    CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 8;    -- Termica
 gcRaggrFonteRinnovab     CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'R';  -- Altra Rinnovavbile
 gcRaggrFonteRinnovabID   CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 16;   -- Altra Rinnovavbile
 gcRaggrFonteConvenz      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'C';  -- Altra Convenzionale
 gcRaggrFonteConvenzID    CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 32;   -- Altra Convenzionale

 gcRaggrFonteNonOmog      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '1';  -- Non Omogenea
 gcRaggrFonteNonAppl      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '2';  -- Non Applicabile
 gcRaggrFonteNonDisp      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '3';  -- Non Disponibile

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Rete
-- ----------------------------------------------------------------------------------------------------------

 gcTipReteAT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'A';   -- Rete AT
 gcTipReteMT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'M';   -- Rete MT
 gcTipReteBT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'B';   -- Rete BT

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Cliente/Fornitore
-- ----------------------------------------------------------------------------------------------------------

 gcClienteProdPuro     CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'A';   -- Produttore puro
 gcClienteAutoProd     CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'B';   -- Autoproduttore
 gcClientePrdNonDeterm CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'X';   -- Non determinato
 gcClientePrdNonApplic CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'Z';   -- Non applicabile
 gcCliente             CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'C';   -- Cliente

-- ----------------------------------------------------------------------------------------------------------
-- Tipi CLASSI Job (eleborazioni)
-- ----------------------------------------------------------------------------------------------------------

 gcJobClassUTL         CONSTANT VARCHAR2(20) := 'Utility';
 gcJobClassMIS         CONSTANT VARCHAR2(20) := 'Misure';
 gcJobClassANA         CONSTANT VARCHAR2(20) := 'Anagraf';
 gcJobClassAGR         CONSTANT VARCHAR2(20) := 'Aggregaz';
 gcJobClassMET         CONSTANT VARCHAR2(20) := 'Meteo';
 gcJobClassREP         CONSTANT VARCHAR2(20) := 'Report';
 gcJobClassFE          CONSTANT VARCHAR2(20) := 'FrontEnd';
 gcJobClassBE          CONSTANT VARCHAR2(20) := 'BackEnd';

 gcJobSubClassFE       CONSTANT VARCHAR2(7) := '.FE' ; -- FrontEnd
 gcJobSubClassBE       CONSTANT VARCHAR2(7) := '.BE' ; -- BackEnd
 gcJobSubClassELE      CONSTANT VARCHAR2(7) := '.ELE'; -- Elementi
 gcJobSubClassMIS      CONSTANT VARCHAR2(7) := '.MIS'; -- Misure
 gcJobSubClassMET      CONSTANT VARCHAR2(7) := '.MET'; -- Misure
 gcJobSubClassGME      CONSTANT VARCHAR2(7) := '.GME'; -- Misure GME
 gcJobSubClassSRV      CONSTANT VARCHAR2(7) := '.SRV'; -- Servizio
 gcJobSubClassINI      CONSTANT VARCHAR2(7) := '.INI'; -- Inizializzazione
 gcJobSubClassAGG      CONSTANT VARCHAR2(7) := '.AGG'; -- Aggiornamento
 gcJobSubClassINS      CONSTANT VARCHAR2(7) := '.INS'; -- Inserimento
 gcJobSubClassCAL      CONSTANT VARCHAR2(7) := '.CAL'; -- Calcolo
 gcJobSubClassLIN      CONSTANT VARCHAR2(7) := '.LIN'; -- Linearizzazione
 gcJobSubClassSCH      CONSTANT VARCHAR2(7) := '.SCH'; -- Schedulatore

-- ----------------------------------------------------------------------------------------------------------
-- Tipi GTTD_VALORI_TEMP
-- ----------------------------------------------------------------------------------------------------------

 gcTmpTipEleKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'CODELE';
 gcTmpTipMisKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPMIS';
 gcTmpTipRetKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPRET';
 gcTmpTipFonKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPFON';
 gcTmpTipCliKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPCLI';

/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

 gInizializzazione_In_Corso  BOOLEAN := FALSE;

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetEcho            (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pEchoString     IN VARCHAR2);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitMagoSession;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION StdOutDate          (pData          IN DATE) RETURN VARCHAR2;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetVersion        (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMagoSTM          RETURN BOOLEAN;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMagoDGF          RETURN BOOLEAN;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsNazionale        RETURN BOOLEAN;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetStartDate        (pData IN DATE DEFAULT NULL) RETURN DATE;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE TrattaListaCodici  (pLista         IN VARCHAR2,
                               pTempTip       IN GTTD_VALORI_TEMP.TIP%TYPE,
                               pAggrega      OUT NUMBER);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetGruppiMisura   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetTipiMisura     (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetTrattamentoElemento(pCodEle      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipEle      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                 pTipMis      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pTipFon      IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                 pTipRet      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 pTipCli      IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                 pOrganizzaz  IN NUMBER,
                                 pTipoAggreg  IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE,
                               pStatoRete     IN INTEGER DEFAULT PKG_Mago.gcStatoAttuale);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pElemTypes      IN VARCHAR2 DEFAULT NULL,
                              pCompFlagProd   IN INTEGER  DEFAULT gcON);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN VARCHAR2,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Mago;
/
SHOW ERRORS;


