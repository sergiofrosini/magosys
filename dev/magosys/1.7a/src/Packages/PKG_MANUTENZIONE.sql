Prompt Package PKG_MANUTENZIONE;
--
-- PKG_MANUTENZIONE  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   METEO_JOB_STATIC_CONFIG (Table)
--   METEO_FILE_XML (Table)
--   METEO_FILE_ZIP (Table)
--   LOG_HISTORY (Table)
--   LOG_HISTORY_INFO (Table)
--   PKG_UTLGLB ()
--   PKG_MAGO (Package)
--   PKG_LOGS (Package)
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--
CREATE OR REPLACE PACKAGE "PKG_MANUTENZIONE" AS

/* ***********************************************************************************************************
   NAME:       PKG_Manutenzione
   PURPOSE:    Servizi per la manutenzione dell'applicazione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PuliziaGiornaliera;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Manutenzione;
/

SHOW ERRORS;


