PROMPT  pkg_genera_file_geo

CREATE OR REPLACE PACKAGE pkg_genera_file_geo AS
/* ***********************************************************************************************************
   NAME:       pkg_genera_file_geo
   PURPOSE:    Servizi per la creazione Anagrafica punti geolocalizzati
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      15/03/2013  Paolo Campi      Created this package.
   NOTES:
*********************************************************************************************************** */
   PROCEDURE genera_file_geo(v_filename IN VARCHAR2);
   PROCEDURE genera_file_correlazione(v_filename IN VARCHAR2);
END;
/

SHO ERRORS;
