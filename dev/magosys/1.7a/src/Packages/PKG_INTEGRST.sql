Prompt Package PKG_INTEGRST;
--
-- PKG_INTEGRST  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Package)
--   T_MISREQ_OBJ (Type)
--   T_MISREQ_ARRAY (Type)
--   T_MISREQ_ARRAY (Type)
--   ELEMENTI_DEF (Table)
--   ELEMENTI (Table)
--   TIPI_MISURA (Table)
--   SCHEDULED_JOBS (Table)
--   GTTD_VALORI_TEMP (Table)
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_LOGS (Package)
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--   DUAL ()
--   PLITBLM ()
--
CREATE OR REPLACE PACKAGE PKG_IntegrST AS

/* ***********************************************************************************************************
   NAME:       PKG_IntegrST
   PURPOSE:    Servizi di integrazione specifici con sistema ST

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.f      24/07/2012  Moretti C.       Created this package.
   1.0.g      07/09/2012  Moretti C.       Standardizzazione param pMisReq.TIPI_FONTE (GetMeasureReq)
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureReq        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pMisReq         IN T_MISREQ_ARRAY);

-- ----------------------------------------------------------------------------------------------------------

END PKG_IntegrST;
/

SHOW ERRORS;


