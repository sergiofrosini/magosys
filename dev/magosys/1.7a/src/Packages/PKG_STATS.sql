Prompt Package PKG_STATS;
--
-- PKG_STATS  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   STANDARD (Package)
--   DBMS_UTILITY (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   DEFAULT_CO (Table)
--   SCHEDULED_JOBS (Table)
--   SESSION_STATISTIC_IDSEQ (Sequence)
--   SESSION_STATISTICS (Table)
--   SESSION_STATISTICS (Table)
--   PKG_LOGS (Package)
--   PKG_UTLGLB ()
--   DUAL ()
--   DBMS_OUTPUT ()
--   DBMS_UTILITY ()
--
CREATE OR REPLACE PACKAGE PKG_STATS AS

/* ***********************************************************************************************************
   NAME:       PKG_Stats
   PURPOSE:    Gestione delle statistiche

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.6.a      14/01/2014  Moretti C.       Implementazioni per versione 1.6a

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 PROCEDURE CheckSessions;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetStatisticSessionID  (pSessionID     OUT NUMBER);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitStatisticSession (pCodSession    SESSION_STATISTICS.COD_SESSIONE%TYPE,
                                 pServiceID     SESSION_STATISTICS.SERVICE_ID%TYPE,
                                 pServiceRef    SESSION_STATISTICS.SERVICE_REF%TYPE,
                                 pTipMisura     SESSION_STATISTICS.COD_TIPO_MISURA%TYPE,
                                 pStatus        SESSION_STATISTICS.STATUS%TYPE,
                                 pDateFrom      SESSION_STATISTICS.DATE_FROM%TYPE,
                                 pDateTo        SESSION_STATISTICS.DATE_TO%TYPE,
                                 pLeafStartDate SESSION_STATISTICS.LEAF_START_DATE%TYPE DEFAULT NULL,
                                 pLeafEndDate   SESSION_STATISTICS.LEAF_END_DATE%TYPE DEFAULT NULL,
                                 pAggrStartDate SESSION_STATISTICS.AGGREG_START_DATE%TYPE DEFAULT NULL,
                                 pAggrEndDate   SESSION_STATISTICS.AGGREG_END_DATE%TYPE DEFAULT NULL,
                                 pAggrCompleted SESSION_STATISTICS.AGGREG_COMPLETED%TYPE DEFAULT NULL
                                );

-- ----------------------------------------------------------------------------------------------------------


END PKG_Stats;
/
SHOW ERRORS;


