**********************************************************************
IMPORTANTE! 
**********************************************************************

Solo per istallazioni particolari eseguire lo script METEO_JOB_STATIC_CONFIG_PostInstall.sql
per attribuire i parametri specifici alle configurazioni Meteo

----------------------------------------------------------------------
Aggiornamento schema Oracle TMESX_2 (DB1)
----------------------------------------------------------------------

Dalla directory \rel_17a8\Tmesx_2 collegarsi allo schema Oracle TMESX_2@arcdb1 
e quindi eseguire:

		SQL> @PKG_MAGO.sql




