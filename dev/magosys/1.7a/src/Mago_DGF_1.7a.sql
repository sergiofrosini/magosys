spool rel_14a_DGF.log

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO_DGF <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT TRIGGERS
PROMPT
@./Triggers/BEF_IUR_TRATTAMENTO_ELEMENTI.sql

@./Conv_Prod-to-Clie.sql

WHENEVER SQLERROR CONTINUE

WHENEVER SQLERROR CONTINUE


PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT
@./Sequences/SESSION_STATISTIC_IDSEQ.sql;
@./Sequences/OFFLINE_MEASURE_REQUEST_IDSEQ.sql;

PROMPT _______________________________________________________________________________________
PROMPT Types 
PROMPT

@./Types/T_PARAM_PREV_OBJ.sql;
@./Types/T_PARAM_PREV_ARRAY.sql;
@./Types/T_PARAM_EOLIC_OBJ.sql;
@./Types/T_PARAM_EOLIC_ARRAY.sql;
@./Types/T_TRTMISLV_OBJ.sql
@./Types/T_TRTMISMVCLI_OBJ.sql
@./Types/T_TRTMISMVTRD_OBJ.sql
@./Types/T_TRTMISLVPROFILE_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT TABLES
PROMPT
@./Tables/METEO_JOB_RUNTIME_CONFIG.sql
@./Tables/TIPI_MISURA_CONV_ORIG.sql
@./Tables/TIPI_MISURA.sql
@./Tables/GTTD_VALORI_REP.sql;
@./Tables/ANAGRAFICA_PUNTI.sql;
@./Tables/ELEMENTI_DEF.sql;
@./Tables/GTTD_FORECAST_ELEMENTS.sql;
@./Tables/MEASURE_OFFLINE_PARAMETERS.sql;
@./Tables/SESSION_STATISTICS.sql;
@./Tables/TRT_MIS_LV.sql
@./Tables/TRT_MIS_MV_CLIENTI.sql
@./Tables/TRT_MIS_MV_TREND.sql
@./Tables/TRT_MIS_LV_PROFILE.sql


PROMPT _______________________________________________________________________________________
PROMPT INIT TABLE

@./InitTables/METEO_JOB_STATIC_CONFIG.sql
@./InitTables/METEO_JOB_RUNTIME_CONFIG.sql
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPI_CLIENTE.sql
@./InitTables/SCHEDULED_JOBS_DEF.sql
@./InitTables/SCHEDULED_JOBS.sql
@./InitTables/CANCELLAZIONE_MISURA.sql

@./InitTables/TIPI_MISURA.sql;
@./InitTables/TIPI_MISURA_CONV_UM.sql;
@./InitTables/GRUPPI_MISURA.sql;
@./InitTables/MEASURE_OFFLINE_PARAMETERS.sql;
@./InitTables/VERSION.sql

PROMPT _______________________________________________________________________________________
PROMPT PACKAGES
PROMPT
@./Packages/PKG_MAGO.sql
@./Packages/PKG_AGGREGAZIONI.sql
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_LOGS.sql
@./Packages/PKG_MANUTENZIONE.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_REPORTS.sql
@./Packages/PKG_SCHEDULER.sql
@./Packages/PKG_TRT_MIS.sql
@./Packages/PKG_GENERA_FILE_GEO.sql;
@./Packages/PKG_LOCALIZZA_GEO_DGF.sql;
@./Packages/PKG_STATS.sql;


PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ELEMENTI.sql
@./Views/V_ESERCIZI.sql
@./Views/V_LOG_HISTORY_MAGO.sql
@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./Views/V_MOD_ASSETTO_RETE_SA.sql
@./Views/V_PARAMETRI_APPLICATIVI.sql
@./Views/V_SCHEDULED_JOBS.sql
@./Views/V_TIPI_MISURA.sql
@./Views/V_ELEMENTI_DGF.sql
@./Views/V_PUNTI_GEO_DGF.sql;


PROMPT _______________________________________________________________________________________
PROMPT PackageBody 
PROMPT
@./PackageBodies/PKG_AGGREGAZIONI.sql;
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_LOGS.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MANUTENZIONE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_SCHEDULER.sql
@./PackageBodies/PKG_TRT_MIS.sql
@./PackageBodies/PKG_MAGO_DGF.sql
@./PackageBodies/PKG_GENERA_FILE_GEO.sql;
@./PackageBodies/PKG_LOCALIZZA_GEO_DGF.sql;
@./PackageBodies/PKG_STATS.sql;

PROMPT _______________________________________________________________________________________
PROMPT SchedulerJobs 
PROMPT
@./SchedulerJobs/CHECK_SESSION_STATISTICS.sql;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.7.a
PROMPT

spool off

