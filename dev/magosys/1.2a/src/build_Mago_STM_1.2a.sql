spool rel_12a_STM.log
conn mago/mago@arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

@./Tables/VERSION.sql;
@./Tables/GTTD_MISURE.sql;
@./Tables/GTTD_REP_ENERGIA_POTENZA.sql;
@./Tables/TIPI_MISURA.sql;
@./Tables/TIPI_MISURA_CONV_UM.sql;
@./Tables/TIPI_MISURA_CONV_ORIG.sql;
@./Tables/GRUPPI_MISURA.sql;
@./Tables/TRATTAMENTO_ELEMENTI.sql;
@./Tables/REL_ELEMENTO_TIPMIS.sql;
@./Tables/SCHEDULED_TMP_GEN.sql;
@./Tables/SCHEDULED_TMP_MET.sql;
@./Tables/SCHEDULED_TMP_GME.sql;
@./Tables/SCHEDULED_JOBS.sql;

@./Views/V_CURRENT_VERSION.sql;

@./Types/T_MISURA_GME_OBJ.sql;
@./Types/T_MISMETEO_OBJ.sql;

@./InitTables/TIPI_MISURA.sql;
@./InitTables/TIPI_MISURA_CONV_UM.sql;
@./InitTables/VERSION.sql;
--@./InitTables/GRUPPI_MISURA.sql;

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione Schema > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT fine Upgrade MAGO 1.2.a
PROMPT

spool off

