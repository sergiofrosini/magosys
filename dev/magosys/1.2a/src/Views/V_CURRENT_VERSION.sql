CREATE OR REPLACE VIEW V_CURRENT_VERSION
AS 
SELECT VERSION
      ,DATA
      ,CONTENT
      ,description
  FROM
      (SELECT 'DB=MAGO_' || major || '.' || minor || '/' || patch || ' (' || TO_CHAR(DATA,'dd/mm/yyyy') ||')' VERSION 
             ,DATA
             ,CONTENT
             ,description
             ,ROW_NUMBER() OVER( ORDER BY DATA DESC) num 
         FROM VERSION v
      ) 
 WHERE num = 1
/
