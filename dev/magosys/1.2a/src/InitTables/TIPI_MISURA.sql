PROMPT Insert Table TIPI_MISURA;

MERGE INTO tipi_misura t
     USING (SELECT 'PCTCT' cod_tipo_misura , NULL codifica_st , 'Previsione Carico totale in Potenza Cielo Terso' descrizione , 0 mis_generazione , 15 risoluzione_fe , 'kW' cod_um_standard, 0 rilevazioni_nagative, 0 split_fonte_energia, '2' tipo_interpolazione_freq_camp, '2' tipo_interpolazione_buchi_camp
              FROM DUAL 
             UNION ALL
            SELECT 'PPATCT',NULL,'Previsione della potenza attiva Transitante Cielo Terso',0,15,'kW',1,0,'2','2' 
              FROM DUAL
            ) s
        ON (t.cod_tipo_misura = s.cod_tipo_misura)
      WHEN MATCHED THEN
    UPDATE SET 
               t.codifica_st = s.codifica_st
              ,t.descrizione = s.descrizione
              ,t.mis_generazione = s.mis_generazione
              ,t.risoluzione_fe = s.risoluzione_fe
              ,t.cod_um_standard = s.cod_um_standard
              ,t.rilevazioni_nagative = s.rilevazioni_nagative
              ,t.split_fonte_energia = s.split_fonte_energia
              ,t.tipo_interpolazione_freq_camp = s.tipo_interpolazione_freq_camp
              ,t.tipo_interpolazione_buchi_camp = s.tipo_interpolazione_buchi_camp
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_tipo_misura,codifica_st,descrizione,mis_generazione,risoluzione_fe,cod_um_standard,rilevazioni_nagative,split_fonte_energia,tipo_interpolazione_freq_camp,tipo_interpolazione_buchi_camp) 
    VALUES (s.cod_tipo_misura, s.codifica_st, s.descrizione, s.mis_generazione, s.risoluzione_fe, s.cod_um_standard, s.rilevazioni_nagative, s.split_fonte_energia, s.tipo_interpolazione_freq_camp, s.tipo_interpolazione_buchi_camp)
;

          
COMMIT;
