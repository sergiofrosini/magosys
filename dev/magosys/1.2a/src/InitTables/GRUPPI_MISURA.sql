Prompt Insert Table GRUPPI_MISURA;

MERGE INTO gruppi_misura t
     USING ( --PCTCT 
            SELECT 9999999999 cod_gruppo ,'CFT' cod_tipo_elemento ,'PCTCT' cod_tipo_misura ,0 flag_attivo ,4 seq_ord,0 disattivato 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'COM','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'CPR','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'CPR','PCTCT',0,4,0 
              FROM DUAL
             UNION ALL
            SELECT 1,'ESE','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'ESE','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'LMT','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'LMT','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'PRV','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'SMT','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'SMT','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRF','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRF','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRS','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRS','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRT','PCTCT',1,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRT','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'ZNA','PCTCT',0,4,0 
              FROM DUAL 
             UNION ALL
            --PPATCT
            SELECT 1,'CPR','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'CPR','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'LMT','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'LMT','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'SMT','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'SMT','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRF','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRF','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRS','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRS','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'TRT','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'TRT','PPATCT',0,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 1,'ESE','PPATCT',1,8,0 
              FROM DUAL 
             UNION ALL
            SELECT 9999999999,'ESE','PPATCT',0,8,0 
              FROM DUAL 
            ) s
        ON (t.cod_gruppo =s.cod_gruppo AND t.cod_tipo_elemento = s.cod_tipo_elemento AND s.cod_tipo_misura = t.cod_tipo_misura)
      WHEN MATCHED THEN
    UPDATE SET 
               t.flag_attivo = s.flag_Attivo
              ,t.seq_ord = s.seq_ord
              ,t.disattivato = s.disattivato
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_gruppo,cod_tipo_elemento,cod_tipo_misura,flag_attivo,seq_ord,disattivato) 
    VALUES (s.cod_gruppo, s.cod_tipo_elemento, s.cod_tipo_misura, s.flag_attivo, s.seq_ord, s.disattivato)
;

COMMIT;