RENAME versions TO versions_bck;

INSERT INTO version(data,major, minor, patch, revision)
SELECT data , version, substr(release,1,CASE WHEN instr(release,'.') != 0 THEN instr(release,'.')-1 END),CASE WHEN instr(release,'.')!= 0 THEN substr(release,instr(release,'.')+1)END,patch
  FROM versions_bck;

COMMIT;

DROP TABLE versions_bck;

