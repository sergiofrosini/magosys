Prompt Insert Table TIPI_MISURA_CONV_UM;

MERGE INTO tipi_misura_conv_um t
     USING ( --PCTCT 
            SELECT 'PCTCT' cod_tipo_misura , 'kW' tipo_misura_conv, 1000 fattore_moltiplicativo, NULL formula
             FROM DUAL 
            UNION ALL
           SELECT 'PCTCT','W',1,NULL 
             FROM DUAL 
            UNION ALL
            --PPATCT
           SELECT 'PPATCT','kW',1000,NULL 
             FROM DUAL 
            UNION ALL
           SELECT 'PPATCT','W',1,NULL 
             FROM DUAL
            ) s
        ON (t.cod_tipo_misura = s.cod_tipo_misura AND t.tipo_misura_conv = s.tipo_misura_conv)
      WHEN MATCHED THEN
    UPDATE SET 
               t.fattore_moltiplicativo = s.fattore_moltiplicativo
              ,t.formula = s.formula
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_tipo_misura,tipo_misura_conv,fattore_moltiplicativo,formula) 
    VALUES (s.cod_tipo_misura, s.tipo_misura_conv, s.fattore_moltiplicativo, s.formula) 
;

          
COMMIT;

