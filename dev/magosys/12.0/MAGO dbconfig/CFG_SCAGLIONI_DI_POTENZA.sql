MERGE INTO SCAGLIONI_DI_POTENZA
 T USING (
SELECT 399 SCAGLIONE_POT,'Scaglione da 0 a 399 (KW)' DESCRIZIONE,0 POT_DA,400000 POT_A
 FROM dual
 UNION ALL
 SELECT 1000 SCAGLIONE_POT,'Scaglione da 400 a 1000 (KW)' DESCRIZIONE,400000 POT_DA,1000000 POT_A
 FROM dual
 UNION ALL
 SELECT 999999999 SCAGLIONE_POT,'Scaglione da  1001 ed oltre  (KW)' DESCRIZIONE,1000000 POT_DA,999999999999 POT_A
 FROM dual
) S ON (
 s.scaglione_pot = t.scaglione_pot AND s.descrizione = t.descrizione AND s.pot_da = t.pot_da AND s.pot_a = t.pot_a
)
 WHEN NOT MATCHED THEN 
 INSERT (
 scaglione_pot , descrizione , pot_da , pot_a
) VALUES (
 s.scaglione_pot , s.descrizione , s.pot_da , s.pot_a
); 

COMMIT;
