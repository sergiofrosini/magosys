MERGE INTO SPV_DIZIONARIO_ALLARMI
 T USING (
SELECT 1 ID_DIZIONARIO_ALLARME,'Controllo Codice gestionale esercizio abilitato' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 2 ID_DIZIONARIO_ALLARME,'CG duplicati' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 3 ID_DIZIONARIO_ALLARME,'CG  non definiti' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 4 ID_DIZIONARIO_ALLARME,'CG minuscoli in codice gestionale' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 5 ID_DIZIONARIO_ALLARME,'CG non standard' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 6 ID_DIZIONARIO_ALLARME,'Produttori MT e TR MT/BT definiti in AUI e non presenti in STM' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 7 ID_DIZIONARIO_ALLARME,'CG in STM non trovato in AUI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 8 ID_DIZIONARIO_ALLARME,'Produttori MT trasmessi da STM a MAGO che risultano �dismessi� in AUI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 9 ID_DIZIONARIO_ALLARME,'Previsioni meteo non disponibili.' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 10 ID_DIZIONARIO_ALLARME,'Assenza di PPAGmp o PPAGct a livello di intero CO  per il giorno corrente+72H.' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 11 ID_DIZIONARIO_ALLARME,'Produttore senza PPAGmp o PPAGct  calcolata  per il giorno corrente' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 12 ID_DIZIONARIO_ALLARME,'Produttore MT o TR MT/BT senza comune meteo associato' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 13 ID_DIZIONARIO_ALLARME,'Produttore con almeno il 50%dei giorni con PPAGmp o PPAGct > PI ' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 14 ID_DIZIONARIO_ALLARME,'Produttore con PPAGmp o PPAGct troppo bassa rispetto a PAS (in %)  per il giorno corrente+72H' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 15 ID_DIZIONARIO_ALLARME,'Produttore con PPAGct > PPAGmp  per il giorno corrente+72H' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 16 ID_DIZIONARIO_ALLARME,'Assenza coordinate lat, log per la SBCS cui � sotteso un produttore MT o TR MT/BT' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 17 ID_DIZIONARIO_ALLARME,'Parametri modello forecast singola �foglia� non definiti' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 18 ID_DIZIONARIO_ALLARME,'Mancanza coordinata altitudine singola foglia' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 19 ID_DIZIONARIO_ALLARME,'Mancanza dato altezza sul terreno singola foglia' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 20 ID_DIZIONARIO_ALLARME,'Assenza di PAS o PRI a livello di intero CO' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 21 ID_DIZIONARIO_ALLARME,'Produttori senza PAS o PRI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 22 ID_DIZIONARIO_ALLARME,'Produttore scartato in fase di acquisizione della PAS o PRI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 23 ID_DIZIONARIO_ALLARME,'Produttore con PAS>PI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 24 ID_DIZIONARIO_ALLARME,'Produttore solare omogeneo con PAS non nulla di notte' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 25 ID_DIZIONARIO_ALLARME,'Produttore con PAS>PPAGct' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 26 ID_DIZIONARIO_ALLARME,'Produttore solare omogeneo con PAS>PPAGct' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 27 ID_DIZIONARIO_ALLARME,'Produttore con PAS troppo bassa rispetto a PI  (in %)  per il giorno corrente-72H' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 28 ID_DIZIONARIO_ALLARME,'Produttore senza generatori sottesi' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 29 ID_DIZIONARIO_ALLARME,'Generatore MT con Potenza Installata non definita' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 30 ID_DIZIONARIO_ALLARME,'Generatore MT o TR MT/BT con fonte d�energia definita ma con Potenza Installata minore od uguale a z' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 31 ID_DIZIONARIO_ALLARME,'La PI del produttore � diverso dalla somma PI dei generatori sottesi' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 32 ID_DIZIONARIO_ALLARME,'Gruppo con potenza superiore a 6MW senza linea MT dedicata' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 33 ID_DIZIONARIO_ALLARME,'Fonte di energia per GENERATORE MT o TR MT/BT non definita in AUI' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 
35 ID_DIZIONARIO_ALLARME,'Produttori MT o TR MT/BT senza valorizzazione campo Tipo Produttore' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 36 ID_DIZIONARIO_ALLARME,'POD0 ai quali corrispondono pi� di un Produttore MT' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 37 ID_DIZIONARIO_ALLARME,'Clienti MT marcati come �non produttori� ma con generatori associati' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 38 ID_DIZIONARIO_ALLARME,'Clienti MT o TR MT/BT con cos(phi)<0 che darebbero luogo a PI<=0' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 39 ID_DIZIONARIO_ALLARME,'Montanti di linea MT marcati erroneamente come clienti MT' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 40 ID_DIZIONARIO_ALLARME,'Sbarre MT di CS per le quali non sono disponibili le coordinate lat/log' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 41 ID_DIZIONARIO_ALLARME,'Sbarre MT di CS non agganciate in tutte le gerarchie ' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 42 ID_DIZIONARIO_ALLARME,'Produttori MT o TR MT/BT senza comune ISTAT associato' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 43 ID_DIZIONARIO_ALLARME,'Produttori MT con PI>0 ed il campo Tipo Produttore non specificato' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 45 ID_DIZIONARIO_ALLARME,'CG non trovato in output DMS o LVprofile.xls' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 46 ID_DIZIONARIO_ALLARME,'Assenza per il giorno corrente+72H, di PCTP e/ o PCTQ  a livello di intero CO' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 47 ID_DIZIONARIO_ALLARME,'Cliente MT o TR MT/BT, che non ricade nel criterio n.1,  senza PCTP o PCTQ  associata per il giorno' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 48 ID_DIZIONARIO_ALLARME,'Cliente MT con misura SMILE GME  con (b)!=0' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 49 ID_DIZIONARIO_ALLARME,'Verifica trasmissione output del DMS al superclient DMS' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 50 ID_DIZIONARIO_ALLARME,'Assenza di CCP e/o CCQ   a livello di intero CO per il mese precedente' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 51 ID_DIZIONARIO_ALLARME,'Clienti MT senza CCP o CCQ' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 52 ID_DIZIONARIO_ALLARME,'Clienti MT  scartati in fase di acquisizione della misura di CCP o CCQ' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 53 ID_DIZIONARIO_ALLARME,'Intero Esercizio principale del CO senza la PAT per il giorno corrente' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 54 ID_DIZIONARIO_ALLARME,'CG richiesti da un sistema terzo e non trovati da MAGO' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 55 ID_DIZIONARIO_ALLARME,'CG  richiesti da un sistema terzo per i quali MAGO non ha trovato le grandezze richieste' DESCRIZIONE
 FROM dual
) S ON (
 s.id_dizionario_allarme = t.id_dizionario_allarme
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione
 WHEN NOT MATCHED THEN 
 INSERT (
 id_dizionario_allarme , descrizione
) VALUES (
 s.id_dizionario_allarme , s.descrizione
); 

COMMIT;
