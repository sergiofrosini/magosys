MERGE INTO SPV_LIVELLO_ALLARMI
 T USING (
SELECT 1 ID_LIVELLO,'GRAVE' DESCRIZIONE,'Rilevate delle anomalie bloccanti che richiedono un intervento urgente per chiarire e risolvere la situazione' NOTA
 FROM dual
 UNION ALL
 SELECT 2 ID_LIVELLO,'MEDIO' DESCRIZIONE,'Rilevate anomalie che richiedono una discreta attenzione ma non necessariamente richiedono un intervento' NOTA
 FROM dual
 UNION ALL
 SELECT 3 ID_LIVELLO,'NESSUNA' DESCRIZIONE,'Non ci sono anomalie rilevate, tutto procede in modo regolare in funzione dei criteri di allarme definiti' NOTA
 FROM dual
) S ON (
 s.id_livello = t.id_livello
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione , t.nota=s.nota
 WHEN NOT MATCHED THEN 
 INSERT (
 id_livello , descrizione , nota
) VALUES (
 s.id_livello , s.descrizione , s.nota
); 

COMMIT;
