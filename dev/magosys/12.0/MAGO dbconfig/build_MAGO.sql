SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED
SET LINE 300
SET CONCAT |

SPOOL &spool_all APPEND

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sar_admin/sar_admin_dba&tns_ARCDB1

PROMPT ==========================================
PROMPT Init Configuration
PROMPT __________________________________________

PROMPT ==========================================
PROMPT Parametri generali
PROMPT __________________________________________

@./CFG_ADM_PARAMETRI_GENERALI.sql

conn mago/mago&tns_ARCDB2

PROMPT ==========================================
PROMPT Meteo_Job_Static_Config
PROMPT __________________________________________

@./CFG_METEO_JOB_STATIC_CONFIG.sql

PROMPT ==========================================
PROMPT Measure offline Parameters
PROMPT __________________________________________

@./CFG_MEASURE_OFFLINE_PARAMETERS.sql

PROMPT ==========================================
PROMPT Meteo_Rel_Istat
PROMPT __________________________________________

@./CFG_METEO_REL_ISTAT.sql

PROMPT ==========================================
PROMPT Raggruppamento_Fonti
PROMPT __________________________________________

@./CFG_RAGGRUPPAMENTO_FONTI.sql

PROMPT ==========================================
PROMPT Run_Step
PROMPT __________________________________________

@./CFG_RUN_STEP.sql

PROMPT ==========================================
PROMPT Scheduled_Jobs_Def
PROMPT __________________________________________

@./CFG_SCHEDULED_JOBS_DEF.sql

PROMPT ==========================================
PROMPT Tipi_Cliente
PROMPT __________________________________________

@./CFG_TIPI_CLIENTE.sql

PROMPT ==========================================
PROMPT Tipi_Gruppi_Misura
PROMPT __________________________________________

@./CFG_TIPI_GRUPPI_MISURA.sql

PROMPT ==========================================
PROMPT Tipi_Misura
PROMPT __________________________________________

@./CFG_TIPI_MISURA.sql

PROMPT ==========================================
PROMPT Tipi_Rete
PROMPT __________________________________________

@./CFG_TIPI_RETE.sql

PROMPT ==========================================
PROMPT Tipo_Tecnologia_Solare
PROMPT __________________________________________

@./CFG_TIPO_TECNOLOGIA_SOLARE.sql

PROMPT ==========================================
PROMPT Tipi_Elemento
PROMPT __________________________________________

@./CFG_TIPI_ELEMENTO.sql

PROMPT ==========================================
PROMPT Tipi_Misura_Conv_Um
PROMPT __________________________________________

@./CFG_TIPI_MISURA_CONV_UM.sql

PROMPT ==========================================
PROMPT Tipo_Fonti
PROMPT __________________________________________

@./CFG_TIPO_FONTI.sql

PROMPT ==========================================
PROMPT Gruppi_Misura
PROMPT __________________________________________

@./CFG_GRUPPI_MISURA.sql

PROMPT ==========================================
PROMPT Tipi_Misura_Conv_Orig
PROMPT __________________________________________

@./CFG_TIPI_MISURA_CONV_ORIG.sql

PROMPT ==========================================
PROMPT End Configuration
PROMPT __________________________________________

SPOOL OFF

