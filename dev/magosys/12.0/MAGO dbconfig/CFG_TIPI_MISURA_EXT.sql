MERGE INTO TIPI_MISURA_EXT
 T USING (
SELECT 'A' COD_TIPO_MISURA_EXT,'' CODIFICA_ST,'Corrente' DESCRIZIONE,'TMESX_2' EXT_APPLIC,0 MIS_GENERAZIONE,10 RISOLUZIONE_FE,'A' COD_UM_STANDARD,1 RILEVAZIONI_NAGATIVE,0 SPLIT_FONTE_ENERGIA,'2' TIPO_INTERPOLAZIONE_FREQ_CAMP,'2' TIPO_INTERPOLAZIONE_BUCHI_CAMP,1 FLG_MANUTENZIONE,1 PRIORITA_AGGR,0 FLG_MIS_CARICO,0 FLG_MIS_GENERAZIONE,0 FLG_APPLY_FILTER,0 FLG_KPI,0 FLG_VAR_PI,0 FLG_VAR_PIMP
 FROM dual
 UNION ALL
 SELECT 'P' COD_TIPO_MISURA_EXT,'' CODIFICA_ST,'Potenza Attiva' DESCRIZIONE,'TMESX_2' EXT_APPLIC,0 MIS_GENERAZIONE,10 RISOLUZIONE_FE,'W' COD_UM_STANDARD,1 RILEVAZIONI_NAGATIVE,0 SPLIT_FONTE_ENERGIA,'2' TIPO_INTERPOLAZIONE_FREQ_CAMP,'2' TIPO_INTERPOLAZIONE_BUCHI_CAMP,1 FLG_MANUTENZIONE,1 PRIORITA_AGGR,0 FLG_MIS_CARICO,0 FLG_MIS_GENERAZIONE,0 FLG_APPLY_FILTER,0 FLG_KPI,0 FLG_VAR_PI,0 FLG_VAR_PIMP
 FROM dual
 UNION ALL
 SELECT 'Q' COD_TIPO_MISURA_EXT,'' CODIFICA_ST,'Potenza reattiva' DESCRIZIONE,'TMESX_2' EXT_APPLIC,0 MIS_GENERAZIONE,10 RISOLUZIONE_FE,'W' COD_UM_STANDARD,1 RILEVAZIONI_NAGATIVE,0 SPLIT_FONTE_ENERGIA,'2' TIPO_INTERPOLAZIONE_FREQ_CAMP,'2' TIPO_INTERPOLAZIONE_BUCHI_CAMP,1 FLG_MANUTENZIONE,1 PRIORITA_AGGR,0 FLG_MIS_CARICO,0 FLG_MIS_GENERAZIONE,0 FLG_APPLY_FILTER,0 FLG_KPI,0 FLG_VAR_PI,0 FLG_VAR_PIMP
 FROM dual
 UNION ALL
 SELECT 'V' COD_TIPO_MISURA_EXT,'' CODIFICA_ST,'Tensione' DESCRIZIONE,'TMESX_2' EXT_APPLIC,0 MIS_GENERAZIONE,10 RISOLUZIONE_FE,'V' COD_UM_STANDARD,1 RILEVAZIONI_NAGATIVE,0 SPLIT_FONTE_ENERGIA,'2' TIPO_INTERPOLAZIONE_FREQ_CAMP,'2' TIPO_INTERPOLAZIONE_BUCHI_CAMP,1 FLG_MANUTENZIONE,1 PRIORITA_AGGR,0 FLG_MIS_CARICO,0 FLG_MIS_GENERAZIONE,0 FLG_APPLY_FILTER,0 FLG_KPI,0 FLG_VAR_PI,0 FLG_VAR_PIMP
 FROM dual
) S ON (
 s.cod_tipo_misura_ext = t.cod_tipo_misura_ext
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.codifica_st=s.codifica_st , t.cod_um_standard=s.cod_um_standard , t.descrizione=s.descrizione , t.ext_applic=s.ext_applic , t.flg_apply_filter=s.flg_apply_filter , t.flg_kpi=s.flg_kpi , t.flg_manutenzione=s.flg_manutenzione , t.flg_mis_carico=s.flg_mis_carico , t.flg_mis_generazione=s.flg_mis_generazione , t.flg_var_pi=s.flg_var_pi , t.flg_var_pimp=s.flg_var_pimp , t.mis_generazione=s.mis_generazione , t.priorita_aggr=s.priorita_aggr , t.rilevazioni_nagative=s.rilevazioni_nagative , t.risoluzione_fe=s.risoluzione_fe , t.split_fonte_energia=s.split_fonte_energia , t.tipo_interpolazione_buchi_camp=s.tipo_interpolazione_buchi_camp , t.tipo_interpolazione_freq_camp=s.tipo_interpolazione_freq_camp
 WHEN NOT MATCHED THEN 
 INSERT (
 cod_tipo_misura_ext , codifica_st , descrizione , ext_applic , mis_generazione , risoluzione_fe , cod_um_standard , rilevazioni_nagative , split_fonte_energia , tipo_interpolazione_freq_camp , tipo_interpolazione_buchi_camp , flg_manutenzione , priorita_aggr , flg_mis_carico , flg_mis_generazione , flg_apply_filter , flg_kpi , flg_var_pi , flg_var_pimp
) VALUES (
 s.cod_tipo_misura_ext , s.codifica_st , s.descrizione , s.ext_applic , s.mis_generazione , s.risoluzione_fe , s.cod_um_standard , s.rilevazioni_nagative , s.split_fonte_energia , s.tipo_interpolazione_freq_camp , s.tipo_interpolazione_buchi_camp , s.flg_manutenzione , s.priorita_aggr , s.flg_mis_carico , s.flg_mis_generazione , s.flg_apply_filter , s.flg_kpi , s.flg_var_pi , s.flg_var_pimp
); 

COMMIT;
