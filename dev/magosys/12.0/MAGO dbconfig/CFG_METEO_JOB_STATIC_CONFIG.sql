MERGE INTO METEO_JOB_STATIC_CONFIG
 T USING (
SELECT 'AUI.remote.log.filename' KEY,'Meteo_AAMM.log' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.elementforecast.referencingdate.validity.seconds' KEY,'3600' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.prediction.days.to.save' KEY,'4' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.prediction.start.day' KEY,'0' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.prediction.start.hour.firstsuffix' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.prediction.start.hour.secondsuffix' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.prediction.timezone' KEY,'Europe/Rome' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.recovery.enabled' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MDS.statistics.enabled' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.enabled.suppliers' KEY,'1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.ftp.timeout.close' KEY,'300' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.ftp.timeout.connection' KEY,'300' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.ftp.timeout.read' KEY,'300' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.ftp.use.for.finaltrasfer' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.local.download.retry' KEY,'3' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.local.mantaining.day' KEY,'30' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.local.recovery.days' KEY,'7' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.suffix.folder.elaboration' KEY,'EL' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.suffix.folder.extraction' KEY,'EX' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.conventional.name.1' KEY,'Ilmeteo' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.conventional.name.2' KEY,'I-EM' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.conventional.name.3' KEY,'Nowcast' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.folder.source.path.1' KEY,'/usr/NEW/magosys/meteofile/ilmeteo' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.folder.source.path.2' KEY,'/usr/NEW/magosys/meteofile/iem' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.acquired.curve.name.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.acquired.curve.name.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.clearsky.curve.name.1' KEY,'PMC' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.clearsky.curve.name.2' KEY,'PMC.P1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.curve.name.1' KEY,'PMP' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.forecast.curve.name.2' KEY,'PMP.P1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.destination.path.for.finaltrasfer.1' KEY,'ilmeteo' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.destination.path.for.finaltrasfer.2' KEY,'iem' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.URL.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.URL.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useFtp.1' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useFtp.2' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useSftp.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useSftp.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useURL.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.download.useURL.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.host.1' KEY,'pkg_ARCDB2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.host.2' KEY,'pkg_ARCDB2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.password.1' KEY,'Magosys' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.password.2' KEY,'Magosys' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.port.1' KEY,'21' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.port.2' KEY,'21' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyHost.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyHost.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 
'MFM.supplier.ftp.source.proxyPass.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyPass.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyPort.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyPort.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyUser.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.proxyUser.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.useProxy.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.useProxy.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.username.1' KEY,'magosys' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.source.username.2' KEY,'magosys' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.upload.useSftp.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.upload.useSftp.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.use.for.finaltrasfer.1' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.ftp.use.for.finaltrasfer.2' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.local.destination.path.1' KEY,'./tmp1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.local.destination.path.2' KEY,'./tmp2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.meteo.irr.curve.name.3' KEY,'IRR.N1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.meteo.tmp.curve.name.3' KEY,'TMP.N1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.national.skip.elaboration.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.national.skip.elaboration.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.point.type.1' KEY,'c' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.point.type.2' KEY,'P' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.point.type.3' KEY,'N' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.remote.mantaining.day.1' KEY,'900' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.remote.mantaining.day.2' KEY,'900' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.skip.state.3' KEY,'2,3,4,6,7,8' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.available.hour.first.1' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.available.hour.first.2' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.available.hour.second.1' KEY,'08' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.available.hour.second.2' KEY,'08' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.extension.1' KEY,'.zip' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.extension.2' KEY,'.zip' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.match.first.1' KEY,'_01_xml' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.match.first.2' KEY,'_01_xml' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.match.second.1' KEY,'_00_xml' VALUE
 FROM dual
 UNION ALL
 SELECT 'MFM.supplier.suffix.zip.file.match.second.2' KEY,'_00_xml' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.enable.generator' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.enable.producer' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.enable.transformator' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.force' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.measure.days.number' KEY,'7' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.measure.interval.days' KEY,'7' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.measure.week.number' KEY,'1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.sampling.minutes' KEY,'60' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.types.network' KEY,'M' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.types.producer' KEY,'A' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.estimation.types.sources' KEY,'S|E' VALUE
 FROM dual
 UNION ALL
 SELECT 
'MPS.estimation.ws.wsdl.url' KEY,'http://localhost:80/STWebRegistry-WS/registry?wsdl' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.customer.enabled' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.customer.save.groupsize' KEY,'50' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.customer.save.thread' KEY,'1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.elaboration.chunk.size' KEY,'500' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.elaboration.default.forward.days' KEY,'2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.enabled' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.external.measure.conversion.factor' KEY,'0.001' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.install.forward.hour' KEY,'72' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.recovery.day.amount' KEY,'3' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.recovery.enable' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.stardard.forward.hour' KEY,'24' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.stlpe.addMisureSTLPE.filterCodElement' KEY,'-1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.transformer.enabled' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.transformer.save.groupsize' KEY,'50' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.transformer.save.thread' KEY,'2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.types.network' KEY,'A|M|B' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.types.producer' KEY,'A|B|X|Z|C' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.load.forecast.types.sources' KEY,'S|E|I|T|R|C|3' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.enable.clearsky' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.enable.generator' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.enable.meteo.acquired' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.enable.producer' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.enable.transformator' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.eolic.clearSky.gain' KEY,'0.5' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.export.exportAtGeneratorLevel' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.export.exportAtProducerLevel' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.export.exportAtTransformerLevel' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.export.ftpRetryDelay' KEY,'60000' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.forward.hour' KEY,'72' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.prediction.sampling.minutes' KEY,'60' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.involvedMeasureType.1' KEY,'PAS' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.involvedMeasureType.2' KEY,'PAS' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.involvedMeasureType.3' KEY,'PAS' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.forecasted.1' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.forecasted.2' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.forecasted.3' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.measured.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.measured.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.estimation.weather.measured.3' KEY,'true' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.daysToKeepZipFile.1' KEY,'30' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.daysToKeepZipFile.2' KEY,'30' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.enabled.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.enabled.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 
'MPS.supplier.prediction.export.forward.hour.1' KEY,'48' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.forward.hour.2' KEY,'48' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.host.1' KEY,'127.0.0.1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.host.2' KEY,'127.0.0.1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.password.1' KEY,'ftpuser' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.password.2' KEY,'ftpuser' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.path.1' KEY,'/1' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.path.2' KEY,'/2' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.host.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.host.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.password.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.password.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.port.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.port.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.username.1' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.proxy.username.2' KEY,'' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.useProxy.1' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.useProxy.2' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.username.1' KEY,'ftpuser' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.supplier.prediction.export.ftp.username.2' KEY,'ftpuser' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.thread.number' KEY,'5' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.types.eolic.generator' KEY,'CAT|CMT|CBT|TRF|TRM' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.types.network' KEY,'A|M|B' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.types.producer' KEY,'A|B|X|Z' VALUE
 FROM dual
 UNION ALL
 SELECT 'MPS.types.sources' KEY,'S|E|I|T|R|C' VALUE
 FROM dual
) S ON (
 s.key = t.key
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.value=s.value
 WHEN NOT MATCHED THEN 
 INSERT (
 key , value
) VALUES (
 s.key , s.value
); 

COMMIT;
