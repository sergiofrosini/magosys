MERGE INTO TIPI_ELEMENTO
 T USING (
SELECT 'CAT' COD_TIPO_ELEMENTO,'CLAT' CODIFICA_ST,'ProduttoreAT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,1 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'CBT' COD_TIPO_ELEMENTO,'CLBT' CODIFICA_ST,'ProduttoreBT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,1 FLAG_PRODUZIONE,'B' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'CFT' COD_TIPO_ELEMENTO,'CFT' CODIFICA_ST,'CFT' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,1 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'CMT' COD_TIPO_ELEMENTO,'CLMT' CODIFICA_ST,'ProduttoreMT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,1 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'COM' COD_TIPO_ELEMENTO,'COM' CODIFICA_ST,'Comune' DESCRIZIONE,0 GER_ECP,0 GER_ECS,1 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'COP' COD_TIPO_ELEMENTO,'CO' CODIFICA_ST,'Centro Operativo' DESCRIZIONE,1 GER_ECP,0 GER_ECS,1 GER_GEO,1 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'CPR' COD_TIPO_ELEMENTO,'CPAT' CODIFICA_ST,'Cabina Primaria' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'CSE' COD_TIPO_ELEMENTO,'CSMT' CODIFICA_ST,'Cabina Secondaria' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'DIR' COD_TIPO_ELEMENTO,'' CODIFICA_ST,'Direttrice MT' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'DTR' COD_TIPO_ELEMENTO,'DIRZ' CODIFICA_ST,'DTR' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'ESE' COD_TIPO_ELEMENTO,'ESER' CODIFICA_ST,'Esercizio' DESCRIZIONE,1 GER_ECP,0 GER_ECS,1 GER_GEO,1 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'GAT' COD_TIPO_ELEMENTO,'GENAT' CODIFICA_ST,'GeneratoreAT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'GBT' COD_TIPO_ELEMENTO,'GENBT' CODIFICA_ST,'GeneratoreBT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'B' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'GMT' COD_TIPO_ELEMENTO,'GENMT' CODIFICA_ST,'GeneratoreMT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,
1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'LAT' COD_TIPO_ELEMENTO,'LINAT' CODIFICA_ST,'Montante di linea AT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'LBT' COD_TIPO_ELEMENTO,'LINBT' CODIFICA_ST,'Linea BT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'B' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'LMT' COD_TIPO_ELEMENTO,'LINMT' CODIFICA_ST,'Montante di linea MT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'MAR' COD_TIPO_ELEMENTO,'MACRA' CODIFICA_ST,'Macro aree' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'NAZ' COD_TIPO_ELEMENTO,'NAZIO' CODIFICA_ST,'Nazionale' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'PRV' COD_TIPO_ELEMENTO,'PROV' CODIFICA_ST,'Provincia' DESCRIZIONE,0 GER_ECP,0 GER_ECS,1 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'REG' COD_TIPO_ELEMENTO,'REGIO' CODIFICA_ST,'Regione' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,0 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,0 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'SAT' COD_TIPO_ELEMENTO,'SBCP' CODIFICA_ST,'Montante di Sbarra' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'SCS' COD_TIPO_ELEMENTO,'SBCS' CODIFICA_ST,'Sbarra di Cabina Secondaria' DESCRIZIONE,1 GER_ECP,1 GER_ECS,1 GER_GEO,1 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'SMT' COD_TIPO_ELEMENTO,'SBMT' CODIFICA_ST,'Sbarra MT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRF' COD_TIPO_ELEMENTO,'TRASF' CODIFICA_ST,'Trasformatore AT' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'A' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRM' COD_TIPO_ELEMENTO,'TRBT' CODIFICA_ST,'Trasf MT/BT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'B' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRP' COD_TIPO_ELEMENTO,'PRIM' CODIFICA_ST,'Primario di trasformatore' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRS' COD_TIPO_ELEMENTO,'SECN' CODIFICA_ST,'Secondario di trasformatore' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,
2 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRT' COD_TIPO_ELEMENTO,'TERZ' CODIFICA_ST,'Terziario di trasformatore' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,1 FONTE_NON_APPLICABILE,1 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'TRX' COD_TIPO_ELEMENTO,'' CODIFICA_ST,'Dettaglio di Trasf MT/BT' DESCRIZIONE,0 GER_ECP,1 GER_ECS,0 GER_GEO,0 GER_AMM,2 FLAG_PRODUZIONE,'B' COD_TIPO_RETE,1 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'ZNA' COD_TIPO_ELEMENTO,'ZONA' CODIFICA_ST,'Zona' DESCRIZIONE,0 GER_ECP,0 GER_ECS,0 GER_GEO,1 GER_AMM,0 FLAG_PRODUZIONE,'' COD_TIPO_RETE,0 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
 UNION ALL
 SELECT 'ZSC' COD_TIPO_ELEMENTO,'' CODIFICA_ST,'Area Sconnessa' DESCRIZIONE,1 GER_ECP,0 GER_ECS,0 GER_GEO,0 GER_AMM,0 FLAG_PRODUZIONE,'M' COD_TIPO_RETE,1 NASCOSTO,1 FONTE_NON_OMOGENEA,0 FONTE_NON_APPLICABILE,0 PRODUTTORE_NON_APPLICABILE,1 PRODUTTORE_NON_DISPONIBILE
 FROM dual
) S ON (
 s.cod_tipo_elemento = t.cod_tipo_elemento
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.codifica_st=s.codifica_st , t.cod_tipo_rete=s.cod_tipo_rete , t.descrizione=s.descrizione , t.flag_produzione=s.flag_produzione , t.fonte_non_applicabile=s.fonte_non_applicabile , t.fonte_non_omogenea=s.fonte_non_omogenea , t.ger_amm=s.ger_amm , t.ger_ecp=s.ger_ecp , t.ger_ecs=s.ger_ecs , t.ger_geo=s.ger_geo , t.nascosto=s.nascosto , t.produttore_non_applicabile=s.produttore_non_applicabile , t.produttore_non_disponibile=s.produttore_non_disponibile
 WHEN NOT MATCHED THEN 
 INSERT (
 cod_tipo_elemento , codifica_st , descrizione , ger_ecp , ger_ecs , ger_geo , ger_amm , flag_produzione , cod_tipo_rete , nascosto , fonte_non_omogenea , fonte_non_applicabile , produttore_non_applicabile , produttore_non_disponibile
) VALUES (
 s.cod_tipo_elemento , s.codifica_st , s.descrizione , s.ger_ecp , s.ger_ecs , s.ger_geo , s.ger_amm , s.flag_produzione , s.cod_tipo_rete , s.nascosto , s.fonte_non_omogenea , s.fonte_non_applicabile , s.produttore_non_applicabile , s.produttore_non_disponibile
); 

COMMIT;
