MERGE INTO SCHEDULED_JOBS_DEF
 T USING (
SELECT 1 TIPO_JOB,'Linearizzazione Gerarchia per Calcolo Aggregate' DESCRIZIONE,0 RITARDO_HH
 FROM dual
 UNION ALL
 SELECT 2 TIPO_JOB,'Calcolo Misure Statiche' DESCRIZIONE,0 RITARDO_HH
 FROM dual
 UNION ALL
 SELECT 3 TIPO_JOB,'Calcolo Aggregazioni Misure Statiche' DESCRIZIONE,0 RITARDO_HH
 FROM dual
 UNION ALL
 SELECT 4 TIPO_JOB,'Calcolo Aggregazioni Standard' DESCRIZIONE,0 RITARDO_HH
 FROM dual
 UNION ALL
 SELECT 5 TIPO_JOB,'Richiesta Aggregazioni Standard Priorita'' media' DESCRIZIONE,0 RITARDO_HH
 FROM dual
 UNION ALL
 SELECT 6 TIPO_JOB,'Richiesta Aggregazioni Standard Priorita'' bassa' DESCRIZIONE,0 RITARDO_HH
 FROM dual
) S ON (
 s.tipo_job = t.tipo_job
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione , t.ritardo_hh=s.ritardo_hh
 WHEN NOT MATCHED THEN 
 INSERT (
 tipo_job , descrizione , ritardo_hh
) VALUES (
 s.tipo_job , s.descrizione , s.ritardo_hh
); 

COMMIT;
