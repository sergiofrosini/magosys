MERGE INTO MEASURE_OFFLINE_PARAMETERS
 T USING (
SELECT 'STWebRegistry-WS.measure.extraction.execution_timeout_seconds' KEY,'600' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.extraction.max_gest_code_items' KEY,'1000' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.estimated_db_effort_perc' KEY,'95' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.estimated_polling_interval_factor' KEY,'0.1' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.forecast_info_involved_measures' KEY,'PMP' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.forecast_info_mds_serviceid' KEY,'MDS-service' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.max_concurrent_csv_composer_threads' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.max_concurrent_measure_offline_threads' KEY,'20' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.max_gest_code_per_request' KEY,'2000' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.offline_thread_timeout_seconds' KEY,'86400' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.polling_interval_threshold' KEY,'60' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.purge_session_after_session_ok_closed' KEY,'false' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.subscribe_max_concurrent_session_timestamp_threads' KEY,'1' VALUE
 FROM dual
 UNION ALL
 SELECT 'STWebRegistry-WS.measure.offline.subscribe_thread_timeout_seconds' KEY,'172800' VALUE
 FROM dual
) S ON (
 s.key = t.key
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.value=s.value
 WHEN NOT MATCHED THEN 
 INSERT (
 key , value
) VALUES (
 s.key , s.value
); 

COMMIT;
