MERGE INTO CFG_APPLICATION_PARAMETER
 T USING (
SELECT 'MIS.DUPL' PARAMETER_NAME,'SUBSTITUTE' PAR_VALUE_CHAR,NULL PAR_VALUE_NUM, to_date('','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
 UNION ALL
 SELECT 'MIS.NOELEM' PARAMETER_NAME,'SKIP' PAR_VALUE_CHAR,NULL PAR_VALUE_NUM, to_date('','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
 UNION ALL
 SELECT 'MIS.NOMIS' PARAMETER_NAME,'SKIP' PAR_VALUE_CHAR,NULL PAR_VALUE_NUM, to_date('','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
 UNION ALL
 SELECT 'RUN.DATA' PARAMETER_NAME,'' PAR_VALUE_CHAR,NULL PAR_VALUE_NUM, to_date('01/01/1970 00.00.00','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
 UNION ALL
 SELECT 'RUN.EXEC_MODE' PARAMETER_NAME,'' PAR_VALUE_CHAR,1 PAR_VALUE_NUM, to_date('','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
 UNION ALL
 SELECT 'RUN.MIS_INTERVAL' PARAMETER_NAME,'' PAR_VALUE_CHAR,10 PAR_VALUE_NUM, to_date('','dd/mm/yyyy hh24.mi.ss') PAR_VALUE_DATE
 FROM dual
) S ON (
 s.parameter_name = t.parameter_name
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.par_value_char=s.par_value_char , t.par_value_date=s.par_value_date , t.par_value_num=s.par_value_num
 WHEN NOT MATCHED THEN 
 INSERT (
 parameter_name , par_value_char , par_value_num , par_value_date
) VALUES (
 s.parameter_name , s.par_value_char , s.par_value_num , s.par_value_date
); 

COMMIT;
