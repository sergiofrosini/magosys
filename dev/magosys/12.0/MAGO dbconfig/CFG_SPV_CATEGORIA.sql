MERGE INTO SPV_CATEGORIA
 T USING (
SELECT 1 ID_CATEGORIA,'Supervisione delle previsioni meteo' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 2 ID_CATEGORIA,'Supervisione misure di generazione' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 3 ID_CATEGORIA,'Supervisione misure di carico' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 4 ID_CATEGORIA,'Supervisione consistenza ed anagrafica della rete elettica' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 5 ID_CATEGORIA,'Supervisione previsioni generaz.' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 6 ID_CATEGORIA,'Supervisione previsione di carico' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 7 ID_CATEGORIA,'Supervisione parametri forecast' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 8 ID_CATEGORIA,'Supervisione della procedura di previsione di carico' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 9 ID_CATEGORIA,'Supervisione scambio dati con DMS' DESCRIZIONE
 FROM dual
 UNION ALL
 SELECT 10 ID_CATEGORIA,'Supervisione richieste altri sistemi' DESCRIZIONE
 FROM dual
) S ON (
 s.id_categoria = t.id_categoria
)
 WHEN MATCHED THEN 
 UPDATE SET 
 t.descrizione=s.descrizione
 WHEN NOT MATCHED THEN 
 INSERT (
 id_categoria , descrizione
) VALUES (
 s.id_categoria , s.descrizione
); 

COMMIT;
