CREATE OR REPLACE PACKAGE BODY PKG_GERARCHIA_DGF AS


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.0.2
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/



proc_name VARCHAR2(50);
err_code NUMBER;
err_msg VARCHAR2(250);
PROCEDURE load_gerarchia_ecs(pData IN DATE DEFAULT SYSDATE, pflag_geo IN NUMBER) AS
BEGIN
proc_name := CASE WHEN pflag_geo = 0 THEN 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECS_AMM' ELSE 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECS_GEO' END;
IF pkg_mago_utl.checkRun(proc_name) = 0 THEN
   pkg_mago_utl.setRun(proc_name,1,0);
   COMMIT;
   --Controllo se � presente una gerarchia PADRE-FIGLIO con una data successiva e conseguente a quella che si sta inserENDo
   --In caso affermativo provvedo a spostare indietro la vecchia data di inizio validit�
   MERGE INTO REL_ELEMENTI_ECS_SN T
        USING (
               SELECT prod.prod_cod
                     ,prod.gen_cod
                     ,prod.data_attivazione
                     ,prod.data_disattivazione
                     ,prod.flag_geo
                FROM
                     (
                      SELECT prod_cod
                            ,gen_cod
                            ,pflag_geo flag_geo
                            ,pdata data_attivazione
                            ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                            ,prod_gst
                            ,gen_gst
                            ,ROW_NUMBER() OVER ( PARTITION BY gen_cod, SUBSTR(prod_gst,INSTR(prod_gst,pkg_mago_utl.gcSeparatoreElem)+1)
                                                 ORDER BY INSTR(prod_gst,pkg_mago_utl.gcSeparatoreElem) DESC ) num
                        FROM
                             (
                              SELECT DEF.cod_elemento prod_cod
                                    ,el.cod_gest_elemento prod_gst
                                    ,DEF.nome_elemento prod_nom
                                    ,DEF.coordinata_x prod_x
                                    ,DEF.coordinata_y prod_y
                                    ,DEF.data_attivazione prod_data_attivazione
                                    ,DEF.data_disattivazione prod_data_disattivazione
                                FROM ELEMENTI_DEF DEF
                                    ,ELEMENTI el
                                    ,TIPI_ELEMENTO T
                               WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                                 AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                 AND el.cod_elemento = DEF.cod_elemento
                             ) p_el
                            ,
                            (
                             SELECT DEF.cod_elemento gen_cod
                                   ,el.cod_gest_elemento gen_gst
                                   ,DEF.nome_elemento gen_nom
                                   ,DEF.coordinata_x gen_x
                                   ,DEF.coordinata_y gen_y
                                   ,DEF.data_attivazione gen_data_attivazione
                                   ,DEF.data_disattivazione gen_data_disattivazione
                               FROM ELEMENTI_DEF DEF
                                   ,ELEMENTI el
                                   ,TIPI_ELEMENTO T
                              WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcGeneratoreAT , PKG_MAGO.gcGeneratoreMT , PKG_MAGO.gcGeneratoreBT )
                                AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                AND el.cod_elemento = DEF.cod_elemento
                            ) g_el
                            ,PRODUTTORI_TMP prod
                       WHERE  p_el.prod_gst IN (prod.cg_produttore, CASE WHEN pflag_geo = 1 THEN prod.cod_comune || pkg_mago_utl.gcSeparatoreElem ||prod.cg_produttore ELSE NULL END)
                         AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
                         AND   g_el.gen_gst = prod.cg_generatore
                         AND  pdata BETWEEN g_el.gen_data_attivazione AND g_el.gen_data_disattivazione
                     ) prod
                     ,
                     (
                      SELECT OLD.*
                            ,NVL(LAG(data_attivazione) OVER( PARTITION BY cod_elemento_padre,cod_elemento_figlio ORDER BY data_attivazione),TO_DATE('01011970','ddmmyyyy')) data_attivazione_prev
                        FROM REL_ELEMENTI_ECS_SN OLD
                       WHERE flag_geo = pflag_geo) OLD
                       WHERE OLD.cod_elemento_padre = prod.prod_cod
                         AND OLD.cod_elemento_figlio = prod.gen_cod
                         AND OLD.flag_geo = prod.flag_geo
                         AND pdata < OLD.data_attivazione
                         AND pdata > OLD.data_attivazione_prev
                         AND prod.num = 1 -- permette di selezionare l'elemento produttore � comune anzich� l'elemento produttore
                     )  s
           ON (T.cod_elemento_padre = s.prod_cod AND T.cod_elemento_figlio = s.gen_cod AND T.data_disattivazione = s.data_disattivazione AND T.flag_geo = s.flag_geo)
         WHEN MATCHED THEN
       UPDATE SET
                  data_attivazione = s.data_attivazione
         WHEN NOT MATCHED THEN
       INSERT
              (cod_elemento_padre)
       VALUES
              (NULL) ;
   --Inserisco tutte le gerarchie che non sono presenti o che hanno validit� differenti da quella impostata
   INSERT INTO REL_ELEMENTI_ECS_SN
          ( cod_elemento_padre
           ,cod_elemento_figlio
           ,data_attivazione
           ,data_disattivazione
           ,flag_geo
          )
   SELECT prod.prod_cod
         ,prod.gen_cod
         ,prod.data_attivazione
         ,prod.data_disattivazione
         ,prod.flag_geo
     FROM
         (
          SELECT prod_cod
                ,gen_cod
                ,pflag_geo flag_geo
                ,pdata data_attivazione
                ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                ,prod_gst
                ,gen_gst
                ,ROW_NUMBER() OVER ( PARTITION BY gen_cod, SUBSTR(prod_gst,INSTR(prod_gst,pkg_mago_utl.gcSeparatoreElem)+1) ORDER BY INSTR(prod_gst,pkg_mago_utl.gcSeparatoreElem) DESC ) num
            FROM
                (
                 SELECT DEF.cod_elemento prod_cod
                       ,el.cod_gest_elemento prod_gst
                       ,DEF.nome_elemento prod_nom
                       ,DEF.coordinata_x prod_x
                       ,DEF.coordinata_y prod_y
                       ,DEF.data_attivazione prod_data_attivazione
                       ,DEF.data_disattivazione prod_data_disattivazione
                   FROM ELEMENTI_DEF DEF
                       ,ELEMENTI el
                       ,TIPI_ELEMENTO T
                  WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                    AND el.cod_tipo_elemento = T.cod_tipo_elemento
                    AND el.cod_elemento = DEF.cod_elemento
                ) p_el
                ,
                (
                 SELECT DEF.cod_elemento gen_cod
                       ,el.cod_gest_elemento gen_gst
                       ,DEF.nome_elemento gen_nom
                       ,DEF.coordinata_x gen_x
                       ,DEF.coordinata_y gen_y
                       ,DEF.data_attivazione gen_data_attivazione
                       ,DEF.data_disattivazione gen_data_disattivazione
                   FROM ELEMENTI_DEF DEF
                       ,ELEMENTI el
                       ,TIPI_ELEMENTO T
                  WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcGeneratoreAT , PKG_MAGO.gcGeneratoreMT , PKG_MAGO.gcGeneratoreBT )
                    AND el.cod_tipo_elemento = T.cod_tipo_elemento
                    AND el.cod_elemento = DEF.cod_elemento
                ) g_el
                , PRODUTTORI_TMP prod
           WHERE  p_el.prod_gst IN (prod.cg_produttore, CASE WHEN pflag_geo = 1 THEN prod.cod_comune || pkg_mago_utl.gcSeparatoreElem ||prod.cg_produttore ELSE NULL END)
             AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
             AND   g_el.gen_gst = prod.cg_generatore
             AND  pdata BETWEEN g_el.gen_data_attivazione AND g_el.gen_data_disattivazione
         ) prod
         ,( SELECT OLD.*
                  ,NVL(LAG(data_attivazione) OVER( PARTITION BY cod_elemento_padre,cod_elemento_figlio ORDER BY data_attivazione),TO_DATE('01011970','ddmmyyyy')) data_attivazione_prev
              FROM REL_ELEMENTI_ECS_SN OLD
              WHERE flag_geo = pflag_geo
          ) OLD
    WHERE OLD.cod_elemento_padre (+) = prod.prod_cod
      AND OLD.cod_elemento_figlio (+) = prod.gen_cod
      AND OLD.flag_geo(+) = prod.flag_geo
      AND pdata BETWEEN OLD.data_attivazione (+) AND OLD.data_disattivazione (+)
      AND prod.num = 1 -- permette di selezionare l'elemento produttore � comune anzich� l'elemento produttore
      AND OLD.cod_elemento_figlio IS NULL;
   --Riallineo le date di disattivazione
   MERGE INTO REL_ELEMENTI_ECS_SN T
        USING (
               SELECT cod_elemento_padre
                     ,cod_elemento_figlio
                     ,flag_geo
                     ,data_attivazione
                     ,NVL(LEAD(data_attivazione) OVER (PARTITION BY cod_elemento_figlio ORDER BY data_attivazione)-1/(24*60*60),TO_DATE('01013000','ddmmyyyy')) data_disattivazione_new
                 FROM REL_ELEMENTI_ECS_SN
                WHERE flag_geo = pflag_geo
                  AND data_disattivazione = TO_DATE('01013000','ddmmyyyy')
              ) s
           ON (T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_attivazione = s.data_attivazione AND T.flag_geo = s.flag_geo)
         WHEN MATCHED THEN
       UPDATE
          SET T.data_disattivazione = s.data_disattivazione_new
         WHEN NOT MATCHED THEN
       INSERT
             (cod_elemento_figlio)
       VALUES
             (NULL);
   --Chiudo gli elementi non presenti
   MERGE INTO REL_ELEMENTI_ECS_SN T
        USING (
               SELECT old.cod_elemento_padre
                     ,old.cod_elemento_figlio
                     ,old.flag_geo
                     ,old.data_attivazione
                     ,pdata -1/(24*60*60) data_disattivazione_new
                 FROM REL_ELEMENTI_ECS_SN old
                     ,produttori_tmp new
                     ,elementi ele
                WHERE old.flag_geo = pflag_geo
                  AND old.cod_elemento_figlio = ele.cod_elemento
                  AND ele.cod_gest_elemento = new.cg_generatore (+)
                  AND new.cg_generatore IS NULL
                  AND old.data_disattivazione = TO_DATE('01013000','ddmmyyyy')
              ) s
           ON (T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_attivazione = s.data_attivazione AND T.flag_geo = s.flag_geo)
         WHEN MATCHED THEN
       UPDATE
          SET T.data_disattivazione = s.data_disattivazione_new
         WHEN NOT MATCHED THEN
       INSERT
             (cod_elemento_figlio)
       VALUES
             (NULL);
   pkg_mago_utl.insLog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setRun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.insLog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.insLog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setRun(proc_name,0,1);
     COMMIT;
END load_gerarchia_ecs;
PROCEDURE load_gerarchia_ecs_geo(pData IN DATE DEFAULT SYSDATE) AS
BEGIN
load_gerarchia_ecs(pData,1);
END;
PROCEDURE load_gerarchia_ecs_amm(pData IN DATE DEFAULT SYSDATE) AS
BEGIN
load_gerarchia_ecs(pData,0);
END;
PROCEDURE LOAD_GERARCHIA_GEO(pData IN DATE DEFAULT SYSDATE) AS
BEGIN
proc_name := 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_GEO';
IF pkg_mago_utl.checkRun(proc_name) = 0 THEN
   pkg_mago_utl.setRun(proc_name,1,0);
   COMMIT;
   --Controllo se � presente una gerarchia PADRE-FIGLIO con una data successiva e conseguente a quella che si sta inserENDo
   --In caso affermativo provvedo a spostare indietro la vecchia data di inizio validit�
    MERGE INTO REL_ELEMENTI_GEO T
        USING (
               WITH GERARCHIA_GEO AS
                   (
                    SELECT co_cod
                          ,prv_cod
                          ,com_cod
                          ,prod_cod
                          ,prv_gst
                          ,prod_gst
                          ,pdata data_attivazione
                          ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                      FROM
                          (
                           SELECT co.cod_elemento_co co_cod
                                 ,co.start_date co_data_attivazione
                             FROM DEFAULT_CO co
                            WHERE co.flag_primario = 1
                           ) co
                           ,
                           (
                            SELECT el.cod_elemento prv_cod
                                  ,el.cod_tipo_elemento prv_tipo_el
                                  ,el.cod_gest_elemento prv_gst
                                  ,DEF.nome_elemento prv_nom
                                  ,DEF.coordinata_x prv_x
                                  ,DEF.coordinata_y prv_y
                                  ,DEF.data_attivazione prv_data_attivazione
                                  ,DEF.data_disattivazione prv_data_disattivazione
                              FROM ELEMENTI el
                                  ,ELEMENTI_DEF DEF
                             WHERE el.cod_tipo_elemento = PKG_MAGO.gcProvincia
                               AND el.cod_elemento = DEF.cod_elemento
                            ) prv
                           ,
                           (
                            SELECT el.cod_elemento com_cod
                                  ,el.cod_gest_elemento com_gst
                                  ,prv.sigla prv_sigla
                                  ,DEF.nome_elemento com_nom
                                  ,DEF.coordinata_x com_x
                                  ,DEF.coordinata_y com_y
                                  ,DEF.data_attivazione com_data_attivazione
                                  ,DEF.data_disattivazione com_data_disattivazione
                              FROM ELEMENTI el
                                  ,ELEMENTI_DEF DEF
                                  ,sar_admin.comuni com
                                  ,sar_admin.province prv
                             WHERE el.cod_tipo_elemento = PKG_MAGO.gcComune
                               AND el.cod_elemento = DEF.cod_elemento
                               AND com.cod_istat_comune = el.cod_gest_elemento
                               AND prv.cod_istat_prov = com.cod_istat_prov
                            ) com
                            ,
                            (
                             SELECT DEF.cod_elemento prod_cod
                                   ,el.cod_gest_elemento prod_gst
                                   ,DEF.nome_elemento prod_nom
                                   ,DEF.coordinata_x prod_x
                                   ,DEF.coordinata_y prod_y
                                   ,DEF.data_attivazione prod_data_attivazione
                                   ,DEF.data_disattivazione prod_data_disattivazione
                               FROM ELEMENTI_DEF DEF
                                   ,ELEMENTI el
                                   ,TIPI_ELEMENTO T
                              WHERE T.cod_tipo_elemento IN ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                                AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                AND el.cod_elemento = DEF.cod_elemento
                             ) p_el
                             ,PRODUTTORI_TMP prod
                     WHERE  p_el.prod_gst = prod.cg_produttore
                       AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
                       AND  (/*prv.prv_tipo_el || pkg_mago_utl.gcSeparatoreElem || */com.prv_sigla = prv.prv_gst OR com.prv_sigla IS NULL)
                       AND  pdata BETWEEN prv.prv_data_attivazione(+) AND prv.prv_data_disattivazione(+)
                       AND  com.com_gst(+) = prod.cod_comune
                       AND  pdata BETWEEN com.com_data_attivazione(+) AND com.com_data_disattivazione(+)
                    )
               SELECT ger.cod_elemento_padre
                     ,ger.cod_elemento_figlio
                     ,ger.data_attivazione
                     ,ger.data_disattivazione
                FROM
                     (
                      SELECT co_cod cod_elemento_padre
                            --,COALESCE(prv_cod, com_cod, prod_cod) cod_elemento_figlio -- La gerarchia deve essere sempre tutti i livelli /co/provincia/comune/produttore
                            ,prv_cod cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY co_cod,/* COALESCE(prv_cod, com_cod, prod_cod)*/prv_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                       UNION ALL
                      SELECT --COALESCE(prv_cod, com_cod, prod_cod) -- La gerarchia deve essere sempre tutti i livelli /co/provincia/comune/produttore
                            --,COALESCE(com_cod, prod_cod) -- La gerarchia deve essere sempre tutti i livelli /co/provincia/comune/produttore
                            prv_cod cod_elemento_padre
                            ,com_cod cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY /*COALESCE(prv_cod, com_cod, prod_cod), COALESCE(com_cod, prod_cod)*/ prv_cod, com_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                       UNION
                      SELECT com_cod cod_elemento_padre
                            ,prod_cod cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY com_cod, prod_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                     ) ger
                     ,
                     (
                      SELECT OLD.*
                            ,NVL(LAG(data_attivazione) OVER( PARTITION BY cod_elemento_padre,cod_elemento_figlio ORDER BY data_attivazione),TO_DATE('01011970','ddmmyyyy')) data_attivazione_prev
                        FROM REL_ELEMENTI_GEO OLD ) OLD
                 WHERE OLD.cod_elemento_padre = ger.cod_elemento_padre
                   AND OLD.cod_elemento_figlio = ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre != ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre IS NOT NULL
                   AND pdata < OLD.data_attivazione
                   AND pdata > OLD.data_attivazione_prev
                   AND ger.num = 1
              )s
           ON (T.cod_elemento_padre = s.cod_elemento_padre AND T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_disattivazione = s.data_disattivazione)
         WHEN MATCHED THEN
       UPDATE SET
                  data_attivazione = s.data_attivazione
         WHEN NOT MATCHED THEN
       INSERT
              (cod_elemento_padre)
       VALUES
              (NULL) ;
   --Inserisco tutte le gerarchie che non sono presenti o che hanno validit� differenti da quella impostata
   INSERT INTO REL_ELEMENTI_GEO
          ( cod_elemento_padre
           ,cod_elemento_figlio
           ,data_attivazione
           ,data_disattivazione
          )
               WITH GERARCHIA_GEO AS
                   (
                    SELECT co_cod
                          ,prv_cod
                          ,com_cod
                          ,prod_cod
                          ,prv_gst
                          ,prv_gst
                          ,prod_gst
                          ,pdata data_attivazione
                          ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                      FROM
                          (
                           SELECT co.cod_elemento_co co_cod
                                 ,co.start_date co_data_attivazione
                             FROM DEFAULT_CO co
                            WHERE co.flag_primario = 1
                           ) co
                           ,
                           (
                            SELECT el.cod_elemento prv_cod
                                  ,el.cod_tipo_elemento prv_tipo_el
                                  ,el.cod_gest_elemento prv_gst
                                  ,DEF.nome_elemento prv_nom
                                  ,DEF.coordinata_x prv_x
                                  ,DEF.coordinata_y prv_y
                                  ,DEF.data_attivazione prv_data_attivazione
                                  ,DEF.data_disattivazione prv_data_disattivazione
                              FROM ELEMENTI el
                                  ,ELEMENTI_DEF DEF
                             WHERE el.cod_tipo_elemento = PKG_MAGO.gcProvincia
                               AND el.cod_elemento = DEF.cod_elemento
                            ) prv
                           ,
                           (
                            SELECT el.cod_elemento com_cod
                                  ,el.cod_gest_elemento com_gst
                                  ,prv.sigla prv_sigla
                                  ,DEF.nome_elemento com_nom
                                  ,DEF.coordinata_x com_x
                                  ,DEF.coordinata_y com_y
                                  ,DEF.data_attivazione com_data_attivazione
                                  ,DEF.data_disattivazione com_data_disattivazione
                              FROM ELEMENTI el
                                  ,ELEMENTI_DEF DEF
                                  ,sar_admin.comuni com
                                  ,sar_admin.province prv
                             WHERE el.cod_tipo_elemento = PKG_MAGO.gcComune
                               AND el.cod_elemento = DEF.cod_elemento
                               AND com.cod_istat_comune = el.cod_gest_elemento
                               AND prv.cod_istat_prov = com.cod_istat_prov
                            ) com
                            ,
                            (
                             SELECT DEF.cod_elemento prod_cod
                                   ,el.cod_gest_elemento prod_gst
                                   ,DEF.nome_elemento prod_nom
                                   ,DEF.coordinata_x prod_x
                                   ,DEF.coordinata_y prod_y
                                   ,DEF.data_attivazione prod_data_attivazione
                                   ,DEF.data_disattivazione prod_data_disattivazione
                               FROM ELEMENTI_DEF DEF
                                   ,ELEMENTI el
                                   ,TIPI_ELEMENTO T
                              WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                                AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                AND el.cod_elemento = DEF.cod_elemento
                             ) p_el
                             ,(SELECT prod.*
                                     ,COUNT(*) OVER ( PARTITION BY cg_produttore) tot
                                     ,COUNT(*) OVER (PARTITION BY cg_produttore, cod_comune) totcom
                                     ,prod.cod_comune || pkg_mago_utl.gcSeparatoreElem || prod.cg_produttore CG_PRODUTTORE_COM
                                 FROM PRODUTTORI_TMP prod
                              ) prod
                     WHERE  p_el.prod_gst = CASE WHEN prod.tot = prod.totcom THEN prod.cg_produttore ELSE cg_produttore_com END
                       AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
                       AND  (/*prv.prv_tipo_el || pkg_mago_utl.gcSeparatoreElem || */ com.prv_sigla = prv.prv_gst OR com.prv_sigla IS NULL)
                       AND  pdata BETWEEN prv.prv_data_attivazione(+) AND prv.prv_data_disattivazione(+)
                       AND  com.com_gst(+) = prod.cod_comune
                       AND  pdata BETWEEN com.com_data_attivazione(+) AND com.com_data_disattivazione(+)
                    )
               SELECT ger.cod_elemento_padre
                     ,ger.cod_elemento_figlio
                     ,ger.data_attivazione
                     ,ger.data_disattivazione
                FROM
                     (
                      SELECT co_cod cod_elemento_padre
                            ,COALESCE(prv_cod, com_cod, prod_cod) cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY co_cod, COALESCE(prv_cod, com_cod, prod_cod) ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                       UNION ALL
                      SELECT COALESCE(prv_cod, com_cod, prod_cod)
                            ,COALESCE(com_cod, prod_cod)
                            ,ROW_NUMBER() OVER (PARTITION BY COALESCE(prv_cod, com_cod, prod_cod), COALESCE(com_cod, prod_cod) ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                       UNION
                      SELECT com_cod
                            ,prod_cod
                            ,ROW_NUMBER() OVER (PARTITION BY com_cod, prod_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_GEO
                     ) ger
                ,REL_ELEMENTI_GEO OLD
           WHERE OLD.cod_elemento_padre (+) = ger.cod_elemento_padre
             AND OLD.cod_elemento_figlio (+) = ger.cod_elemento_figlio
             AND ger.cod_elemento_padre != ger.cod_elemento_figlio
             AND ger.cod_elemento_padre IS NOT NULL
             AND pdata BETWEEN OLD.data_attivazione (+) AND OLD.data_disattivazione (+)
             AND OLD.cod_elemento_figlio IS NULL
             AND num = 1;
   --Riallineo le date di disattivazione
   MERGE INTO REL_ELEMENTI_GEO T
        USING (
               SELECT cod_elemento_padre
                     ,cod_elemento_figlio
                     ,data_attivazione
                     ,NVL(LEAD(data_attivazione) OVER (PARTITION BY cod_elemento_figlio ORDER BY data_attivazione)-1/(24*60*60),TO_DATE('01013000','ddmmyyyy')) data_disattivazione_new
                 FROM REL_ELEMENTI_GEO
                WHERE data_disattivazione = TO_DATE('01013000','ddmmyyyy')
              ) s
           ON (T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_attivazione = s.data_attivazione)
         WHEN MATCHED THEN
       UPDATE
          SET T.data_disattivazione = s.data_disattivazione_new
         WHEN NOT MATCHED THEN
       INSERT
             (cod_elemento_figlio)
       VALUES
             (NULL);
   --Chiudo gli elementi senza figli
   LOOP
      MERGE INTO rel_elementi_geo t
      USING (
             SELECT DISTINCT data_disattivazione_max , cod_elemento_padre
               FROM (
                     SELECT MAX(data_disattivazione) data_disattivazione_max , cod_elemento_padre
                       FROM (
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_ecs_sn
                              WHERE flag_geo = 1
                              UNION ALL
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_geo
                            )
                      GROUP BY cod_elemento_padre
                     HAVING MAX(data_disattivazione) != TO_DATE('01/01/3000 00.00.00','dd/mm/yyyy hh24.mi.ss')
                    ) p
                    ,
                    (
                     SELECT cod_elemento_figlio , data_disattivazione
                       FROM rel_elementi_geo
                    ) f
              WHERE p.cod_elemento_padre = f.cod_elemento_figlio
                AND p.data_disattivazione_max != f.data_disattivazione
            ) s
         ON ( s.cod_elemento_padre = t.cod_elemento_figlio )
       WHEN matched then
     UPDATE
        SET t.data_disattivazione = s.data_disattivazione_max;

   EXIT WHEN SQL%ROWCOUNT = 0;
   END LOOP;
   pkg_mago_utl.insLog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setRun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.insLog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.insLog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setRun(proc_name,0,1);
     COMMIT;
END load_gerarchia_geo;
PROCEDURE LOAD_GERARCHIA_AMM(pData IN DATE DEFAULT SYSDATE) AS
BEGIN
proc_name := 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_AMM';
IF pkg_mago_utl.checkRun(proc_name) = 0 THEN
   pkg_mago_utl.setRun(proc_name,1,0);
   COMMIT;
   --Controllo se � presente una gerarchia PADRE-FIGLIO con una data successiva e conseguente a quella che si sta inserENDo
   --In caso affermativo provvedo a spostare indietro la vecchia data di inizio validit�
   MERGE INTO REL_ELEMENTI_AMM T
        USING (
               WITH GERARCHIA_AMM AS
                   (
                    SELECT co_cod
                          ,ese_cod
                          ,zna_cod
                          ,prod_cod
                          ,zna_gst
                          ,prod_gst
                          ,pdata data_attivazione
                          ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                      FROM
                          (
                           SELECT co.cod_elemento_co co_cod
                                 ,co.cod_elemento_ese ese_cod
                                 ,co.start_date co_data_attivazione
                             FROM DEFAULT_CO co
                            WHERE co.flag_primario = 1
                           ) E
                           ,
                           (
                            SELECT el.cod_elemento zna_cod
                                  ,el.cod_gest_elemento zna_gst
                                  ,DEF.nome_elemento zna_nom
                                  ,DEF.coordinata_x zna_x
                                  ,DEF.coordinata_y zna_y
                                  ,DEF.data_attivazione zna_data_attivazione
                                  ,DEF.data_disattivazione zna_data_disattivazione
                              FROM ELEMENTI el
                                  ,ELEMENTI_DEF DEF
                             WHERE el.cod_tipo_elemento = PKG_MAGO.gcZona
                               AND el.cod_elemento = DEF.cod_elemento
                            ) z
                            ,
                            (
                             SELECT DEF.cod_elemento prod_cod
                                   ,el.cod_gest_elemento prod_gst
                                   ,DEF.nome_elemento prod_nom
                                   ,DEF.coordinata_x prod_x
                                   ,DEF.coordinata_y prod_y
                                   ,DEF.data_attivazione prod_data_attivazione
                                   ,DEF.data_disattivazione prod_data_disattivazione
                               FROM ELEMENTI_DEF DEF
                                   ,ELEMENTI el
                                   ,TIPI_ELEMENTO T
                              WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                                AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                AND el.cod_elemento = DEF.cod_elemento
                             ) p_el
                             ,PRODUTTORI_TMP prod
                     WHERE  p_el.prod_gst = prod.cg_produttore
                       AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
                       AND   z.zna_gst(+) = prod.cod_gruppo
                       AND  pdata BETWEEN z.zna_data_attivazione(+) AND z.zna_data_disattivazione(+)
                    )
               SELECT ger.cod_elemento_padre
                     ,ger.cod_elemento_figlio
                     ,ger.data_attivazione
                     ,ger.data_disattivazione
                FROM
                     (
                      SELECT co_cod cod_elemento_padre
                            ,COALESCE(ese_cod,zna_cod,prod_cod) cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY co_cod, COALESCE(ese_cod,zna_cod,prod_cod) ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_AMM
                       UNION ALL
                      SELECT COALESCE(ese_cod,zna_cod,prod_cod)
                            ,COALESCE(zna_cod,prod_cod)
                            ,ROW_NUMBER() OVER (PARTITION BY COALESCE(ese_cod,zna_cod,prod_cod) ,COALESCE(zna_cod,prod_cod) ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_AMM
                       UNION ALL
                      SELECT zna_cod
                            ,prod_cod
                            ,ROW_NUMBER() OVER (PARTITION BY zna_cod, prod_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM GERARCHIA_AMM
                     ) ger
                     ,
                     (
                      SELECT OLD.*
                            ,NVL(LAG(data_attivazione) OVER( PARTITION BY cod_elemento_padre,cod_elemento_figlio ORDER BY data_attivazione),TO_DATE('01011970','ddmmyyyy')) data_attivazione_prev
                        FROM REL_ELEMENTI_AMM OLD ) OLD
                 WHERE OLD.cod_elemento_padre = ger.cod_elemento_padre
                   AND OLD.cod_elemento_figlio = ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre != ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre IS NOT NULL
                   AND pdata < OLD.data_attivazione
                   AND pdata > OLD.data_attivazione_prev
                   AND ger.num = 1
              )s
           ON (T.cod_elemento_padre = s.cod_elemento_padre AND T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_disattivazione = s.data_disattivazione)
         WHEN MATCHED THEN
       UPDATE SET
                  data_attivazione = s.data_attivazione
         WHEN NOT MATCHED THEN
       INSERT
              (cod_elemento_padre)
       VALUES
              (NULL) ;
   --Inserisco tutte le gerarchie che non sono presenti o che hanno validit� differenti da quella impostata
   INSERT INTO REL_ELEMENTI_AMM
          ( cod_elemento_padre
           ,cod_elemento_figlio
           ,data_attivazione
           ,data_disattivazione
          )
      WITH GERARCHIA_AMM AS
          (
           SELECT co_cod
                 ,ese_cod
                 ,zna_cod
                 ,prod_cod
                 ,zna_gst
                 ,prod_gst
                 ,pdata data_attivazione
                 ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
             FROM
                 (
                  SELECT co.cod_elemento_co co_cod
                        ,co.cod_elemento_ese ese_cod
                        ,co.start_date co_data_attivazione
                    FROM DEFAULT_CO co
                   WHERE co.flag_primario = 1
                  ) E
                  ,
                  (
                   SELECT el.cod_elemento zna_cod
                         ,el.cod_gest_elemento zna_gst
                         ,DEF.nome_elemento zna_nom
                         ,DEF.coordinata_x zna_x
                         ,DEF.coordinata_y zna_y
                         ,DEF.data_attivazione zna_data_attivazione
                         ,DEF.data_disattivazione zna_data_disattivazione
                     FROM ELEMENTI el
                         ,ELEMENTI_DEF DEF
                    WHERE el.cod_tipo_elemento = PKG_MAGO.gcZona
                      AND el.cod_elemento = DEF.cod_elemento
                   ) z
                   ,
                   (
                    SELECT DEF.cod_elemento prod_cod
                          ,el.cod_gest_elemento prod_gst
                          ,DEF.nome_elemento prod_nom
                          ,DEF.coordinata_x prod_x
                          ,DEF.coordinata_y prod_y
                          ,DEF.data_attivazione prod_data_attivazione
                          ,DEF.data_disattivazione prod_data_disattivazione
                      FROM ELEMENTI_DEF DEF
                          ,ELEMENTI el
                          ,TIPI_ELEMENTO T
                     WHERE T.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                       AND el.cod_tipo_elemento = T.cod_tipo_elemento
                       AND el.cod_elemento = DEF.cod_elemento
                    ) p_el
                    ,PRODUTTORI_TMP prod
            WHERE  p_el.prod_gst = prod.cg_produttore
              AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
              AND   z.zna_gst(+) = prod.cod_gruppo
              AND  pdata BETWEEN z.zna_data_attivazione(+) AND z.zna_data_disattivazione(+)
          )
          SELECT ger.cod_elemento_padre
                ,ger.cod_elemento_figlio
                ,ger.data_attivazione
                ,ger.data_disattivazione
            FROM
                (
                 SELECT co_cod cod_elemento_padre
                       ,COALESCE(ese_cod,zna_cod,prod_cod) cod_elemento_figlio
                       ,ROW_NUMBER() OVER (PARTITION BY co_cod, COALESCE(ese_cod,zna_cod,prod_cod) ORDER BY data_attivazione) num
                       ,data_attivazione
                       ,data_disattivazione
                   FROM GERARCHIA_AMM
                  UNION ALL
                 SELECT COALESCE(ese_cod,zna_cod,prod_cod)
                       ,COALESCE(zna_cod,prod_cod)
                       ,ROW_NUMBER() OVER (PARTITION BY COALESCE(ese_cod,zna_cod,prod_cod) ,COALESCE(zna_cod,prod_cod) ORDER BY data_attivazione) num
                       ,data_attivazione
                       ,data_disattivazione
                   FROM GERARCHIA_AMM
                  UNION
                 SELECT zna_cod
                       ,prod_cod
                       ,ROW_NUMBER() OVER (PARTITION BY zna_cod, prod_cod ORDER BY data_attivazione) num
                       ,data_attivazione
                       ,data_disattivazione
                   FROM GERARCHIA_AMM
                ) ger
                ,REL_ELEMENTI_AMM OLD
           WHERE OLD.cod_elemento_padre (+) = ger.cod_elemento_padre
             AND OLD.cod_elemento_figlio (+) = ger.cod_elemento_figlio
             AND ger.cod_elemento_padre != ger.cod_elemento_figlio
             AND ger.cod_elemento_padre IS NOT NULL
             AND pdata BETWEEN OLD.data_attivazione (+) AND OLD.data_disattivazione (+)
             AND OLD.cod_elemento_figlio IS NULL
             AND num = 1;
   --Riallineo le date di disattivazione
   MERGE INTO REL_ELEMENTI_AMM T
        USING (
               SELECT cod_elemento_padre
                     ,cod_elemento_figlio
                     ,data_attivazione
                     ,NVL(LEAD(data_attivazione) OVER (PARTITION BY cod_elemento_figlio ORDER BY data_attivazione)-1/(24*60*60),TO_DATE('01013000','ddmmyyyy')) data_disattivazione_new
                 FROM REL_ELEMENTI_AMM
                WHERE data_disattivazione = TO_DATE('01013000','ddmmyyyy')
              ) s
           ON (T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_attivazione = s.data_attivazione)
         WHEN MATCHED THEN
       UPDATE
          SET T.data_disattivazione = s.data_disattivazione_new
         WHEN NOT MATCHED THEN
       INSERT
             (cod_elemento_figlio)
       VALUES
             (NULL);

   --Chiudo gli elementi senza figli
   LOOP
      MERGE INTO rel_elementi_amm t
      USING (
             SELECT DISTINCT data_disattivazione_max , cod_elemento_padre
               FROM (
                     SELECT MAX(data_disattivazione) data_disattivazione_max , cod_elemento_padre
                       FROM (
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_ecs_sn
                              WHERE flag_geo = 0
                              UNION ALL
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_amm
                            )
                      GROUP BY cod_elemento_padre
                     HAVING MAX(data_disattivazione) != TO_DATE('01/01/3000 00.00.00','dd/mm/yyyy hh24.mi.ss')
                    ) p
                    ,
                    (
                     SELECT cod_elemento_figlio , data_disattivazione
                       FROM rel_elementi_amm
                    ) f
              WHERE p.cod_elemento_padre = f.cod_elemento_figlio
                AND p.data_disattivazione_max != f.data_disattivazione
            ) s
         ON ( s.cod_elemento_padre = t.cod_elemento_figlio )
       WHEN matched then
     UPDATE
        SET t.data_disattivazione = s.data_disattivazione_max;

   EXIT WHEN SQL%ROWCOUNT = 0;
   END LOOP;
   pkg_mago_utl.insLog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setRun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.insLog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.insLog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setRun(proc_name,0,1);
     COMMIT;
END load_gerarchia_amm;
PROCEDURE LOAD_GERARCHIA_ECP(pData IN DATE DEFAULT SYSDATE) AS
BEGIN
proc_name := 'PKG_GERARCHIA_DGF.LOAD_GERARCHIA_ECP';
IF pkg_mago_utl.checkRun(proc_name) = 0 THEN
   pkg_mago_utl.setRun(proc_name,1,0);
   COMMIT;
   --Controllo se � presente una gerarchia PADRE-FIGLIO con una data successiva e conseguente a quella che si sta inserENDo
   --In caso affermativo provvedo a spostare indietro la vecchia data di inizio validit�
   MERGE INTO REL_ELEMENTI_ECP_SN T
        USING (
               WITH gerarchia_ecp AS
                   (
                    SELECT co_cod
                          ,ese_cod
                          ,prod_cod
                          ,prod_gst
                          ,pdata data_attivazione
                          ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
                      FROM
                          (
                           SELECT co.cod_elemento_co co_cod
                                 ,co.cod_elemento_ese ese_cod
                                 ,co.start_date co_data_attivazione
                             FROM DEFAULT_CO co
                            WHERE co.flag_primario = 1
                           ) E
                           ,
                            (
                             SELECT DEF.cod_elemento prod_cod
                                   ,el.cod_gest_elemento prod_gst
                                   ,DEF.nome_elemento prod_nom
                                   ,DEF.coordinata_x prod_x
                                   ,DEF.coordinata_y prod_y
                                   ,DEF.data_attivazione prod_data_attivazione
                                   ,DEF.data_disattivazione prod_data_disattivazione
                               FROM ELEMENTI_DEF DEF
                                   ,ELEMENTI el
                                   ,TIPI_ELEMENTO T
                              WHERE T.cod_tipo_elemento IN ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                                AND el.cod_tipo_elemento = T.cod_tipo_elemento
                                AND el.cod_elemento = DEF.cod_elemento
                             ) p_el
                             ,PRODUTTORI_TMP prod
                     WHERE  p_el.prod_gst = prod.cg_produttore
                       AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
                    )
               SELECT ger.cod_elemento_padre
                     ,ger.cod_elemento_figlio
                     ,ger.data_attivazione
                     ,ger.data_disattivazione
                FROM
                     (
                      SELECT co_cod cod_elemento_padre
                            ,COALESCE(ese_cod, prod_cod) cod_elemento_figlio
                            ,ROW_NUMBER() OVER (PARTITION BY co_cod, COALESCE(ese_cod, prod_cod) ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM gerarchia_ecp
                       UNION ALL
                      SELECT ese_cod
                            ,prod_cod
                            ,ROW_NUMBER() OVER (PARTITION BY ese_cod, prod_cod ORDER BY data_attivazione) num
                            ,data_attivazione
                            ,data_disattivazione
                        FROM gerarchia_ecp
                     ) ger
                     ,
                     (
                      SELECT OLD.*
                            ,NVL(LAG(data_attivazione) OVER( PARTITION BY cod_elemento_padre,cod_elemento_figlio ORDER BY data_attivazione),TO_DATE('01011970','ddmmyyyy')) data_attivazione_prev
                        FROM REL_ELEMENTI_ECP_SN OLD ) OLD
                 WHERE OLD.cod_elemento_padre = ger.cod_elemento_padre
                   AND OLD.cod_elemento_figlio = ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre != ger.cod_elemento_figlio
                   AND ger.cod_elemento_padre IS NOT NULL
                   AND pdata < OLD.data_attivazione
                   AND pdata > OLD.data_attivazione_prev
                   AND ger.num = 1
              )s
           ON (T.cod_elemento_padre = s.cod_elemento_padre AND T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_disattivazione = s.data_disattivazione)
         WHEN MATCHED THEN
       UPDATE SET
                  data_attivazione = s.data_attivazione
         WHEN NOT MATCHED THEN
       INSERT
              (cod_elemento_padre)
       VALUES
              (NULL) ;
   --Inserisco tutte le gerarchie che non sono presenti o che hanno validit� differenti da quella impostata
   INSERT INTO REL_ELEMENTI_ECP_SN
          ( cod_elemento_padre
           ,cod_elemento_figlio
           ,data_attivazione
           ,data_disattivazione
          )
      WITH gerarchia_ecp AS
          (
           SELECT co_cod
                 ,ese_cod
                 ,prod_cod
                 ,prod_gst
                 ,pdata data_attivazione
                 ,TO_DATE('01013000','ddmmyyyy') data_disattivazione
             FROM
                 (
                  SELECT co.cod_elemento_co co_cod
                        ,co.cod_elemento_ese ese_cod
                        ,co.start_date co_data_attivazione
                    FROM DEFAULT_CO co
                   WHERE co.flag_primario = 1
                  ) E
                  ,
                  (
                    SELECT DEF.cod_elemento prod_cod
                          ,el.cod_gest_elemento prod_gst
                          ,DEF.nome_elemento prod_nom
                          ,DEF.coordinata_x prod_x
                          ,DEF.coordinata_y prod_y
                          ,DEF.data_attivazione prod_data_attivazione
                          ,DEF.data_disattivazione prod_data_disattivazione
                      FROM ELEMENTI_DEF DEF
                          ,ELEMENTI el
                          ,TIPI_ELEMENTO T
                     WHERE T.cod_tipo_elemento IN ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                       AND el.cod_tipo_elemento = T.cod_tipo_elemento
                       AND el.cod_elemento = DEF.cod_elemento
                   ) p_el
                    ,PRODUTTORI_TMP prod
            WHERE  p_el.prod_gst = prod.cg_produttore
              AND  pdata BETWEEN p_el.prod_data_attivazione AND p_el.prod_data_disattivazione
          )
          SELECT ger.cod_elemento_padre
                ,ger.cod_elemento_figlio
                ,ger.data_attivazione
                ,ger.data_disattivazione
            FROM
                (
                 SELECT co_cod cod_elemento_padre
                       ,COALESCE(ese_cod, prod_cod) cod_elemento_figlio
                       ,ROW_NUMBER() OVER (PARTITION BY co_cod, COALESCE(ese_cod, prod_cod) ORDER BY data_attivazione) num
                       ,data_attivazione
                       ,data_disattivazione
                   FROM gerarchia_ecp
                  UNION ALL
                 SELECT ese_cod
                       ,prod_cod
                       ,ROW_NUMBER() OVER (PARTITION BY ese_cod, prod_cod ORDER BY data_attivazione) num
                       ,data_attivazione
                       ,data_disattivazione
                   FROM gerarchia_ecp
                ) ger
                ,REL_ELEMENTI_ECP_SN OLD
           WHERE OLD.cod_elemento_padre (+) = ger.cod_elemento_padre
             AND OLD.cod_elemento_figlio (+) = ger.cod_elemento_figlio
             AND pdata BETWEEN OLD.data_attivazione (+) AND OLD.data_disattivazione (+)
             AND ger.cod_elemento_padre != ger.cod_elemento_figlio
             AND ger.cod_elemento_padre IS NOT NULL
             AND OLD.cod_elemento_figlio IS NULL
             AND num = 1;
   --Riallineo le date di disattivazione
   MERGE INTO REL_ELEMENTI_ECP_SN T
        USING (
               SELECT cod_elemento_padre
                     ,cod_elemento_figlio
                     ,data_attivazione
                     ,NVL(LEAD(data_attivazione) OVER (PARTITION BY cod_elemento_figlio ORDER BY data_attivazione)-1/(24*60*60),TO_DATE('01013000','ddmmyyyy')) data_disattivazione_new
                 FROM REL_ELEMENTI_ECP_SN
                WHERE data_disattivazione = TO_DATE('01013000','ddmmyyyy')
              ) s
           ON (T.cod_elemento_figlio = s.cod_elemento_figlio AND T.data_attivazione = s.data_attivazione)
         WHEN MATCHED THEN
       UPDATE
          SET T.data_disattivazione = s.data_disattivazione_new
         WHEN NOT MATCHED THEN
       INSERT
             (cod_elemento_figlio)
       VALUES
             (NULL);
    --Chiudo gli elementi che non hanno figli
   LOOP
      MERGE INTO rel_elementi_ecp_sn t
      USING (
             SELECT DISTINCT data_disattivazione_max , cod_elemento_padre
               FROM (
                     SELECT MAX(data_disattivazione) data_disattivazione_max , cod_elemento_padre
                       FROM (
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_ecs_sn
                              UNION ALL
                             SELECT cod_elemento_padre , data_disattivazione
                               FROM rel_elementi_ecp_sn
                            )
                      GROUP BY cod_elemento_padre
                     HAVING MAX(data_disattivazione) != TO_DATE('01/01/3000 00.00.00','dd/mm/yyyy hh24.mi.ss')
                    ) p
                   ,
                   ( SELECT cod_elemento_figlio , data_disattivazione
                       FROM rel_elementi_ecs_sn
                      UNION ALL
                     SELECT cod_elemento_figlio , data_disattivazione
                       FROM rel_elementi_ecp_sn
                   ) f
              WHERE p.cod_elemento_padre = f.cod_elemento_figlio
                AND p.data_disattivazione_max != f.data_disattivazione
            ) s
         ON ( s.cod_elemento_padre = t.cod_elemento_figlio )
       WHEN MATCHED THEN
     UPDATE
        SET t.data_disattivazione = s.data_disattivazione_max;

   EXIT WHEN SQL%ROWCOUNT = 0;
   END LOOP;
   pkg_mago_utl.insLog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setRun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.insLog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.insLog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setRun(proc_name,0,1);
     COMMIT;
END load_gerarchia_ecp;
PROCEDURE linearizza_gerarchia(pTipoGer IN VARCHAR2) AS
v_sql VARCHAR2(4000);
v_tab_rel VARCHAR2(30);
v_tab_ger VARCHAR2(30);
BEGIN
proc_name := 'PKG_GERARCHIA_DGF.LINEARIZZA_GERARCHIA';
proc_name := proc_name || '_' || pTipoGer;
IF pkg_mago_utl.checkRun(proc_name) = 0 THEN
   pkg_mago_utl.setRun(proc_name,1,0);
   COMMIT;
   CASE pTipoGer
        WHEN 'GEO' THEN
           v_tab_rel := 'REL_ELEMENTI_GEO';
           v_tab_ger := 'GERARCHIA_GEO';
        WHEN 'AMM' THEN
           v_tab_rel := 'REL_ELEMENTI_AMM';
           v_tab_ger := 'GERARCHIA_AMM';
        WHEN 'IMP' THEN
           v_tab_rel := 'REL_ELEMENTI_ECP_SN';
           v_tab_ger := 'GERARCHIA_IMP_SN';
   END CASE;
   EXECUTE IMMEDIATE 'TRUNCATE TABLE ' || v_tab_ger;
   v_sql:= 'INSERT INTO ' || v_tab_ger || ' (cod_elemento,data_attivazione,data_disattivazione,l01,l02,l03,l04,l05,l06,l07,l08,l09,l10,l11,l12,l13) '
         || 'SELECT cod_elemento_figlio,data_attivazione, data_disattivazione '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,1)!= 0 THEN INSTR(cod_el,''#'',1,1)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,2),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,2)-1)-(INSTR(cod_el,''#'',1,1))) liv1 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,2)!= 0 THEN INSTR(cod_el,''#'',1,2)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,3),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,3)-1)-(INSTR(cod_el,''#'',1,2))) liv2 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,3)!= 0 THEN INSTR(cod_el,''#'',1,3)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,4),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,4)-1)-(INSTR(cod_el,''#'',1,3))) liv3 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,4)!= 0 THEN INSTR(cod_el,''#'',1,4)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,5),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,5)-1)-(INSTR(cod_el,''#'',1,4))) liv4 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,5)!= 0 THEN INSTR(cod_el,''#'',1,5)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,6),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,6)-1)-(INSTR(cod_el,''#'',1,5))) liv5 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,6)!= 0 THEN INSTR(cod_el,''#'',1,6)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,7),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,7)-1)-(INSTR(cod_el,''#'',1,6))) liv6 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,7)!= 0 THEN INSTR(cod_el,''#'',1,7)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,8),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,8)-1)-(INSTR(cod_el,''#'',1,7))) liv7 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,8)!= 0 THEN INSTR(cod_el,''#'',1,8)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,9),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,9)-1)-(INSTR(cod_el,''#'',1,8))) liv8 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,9)!= 0 THEN INSTR(cod_el,''#'',1,9)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,10),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,10)-1)-(INSTR(cod_el,''#'',1,9))) liv9 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,10)!= 0 THEN INSTR(cod_el,''#'',1,10)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,11),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,11)-1)-(INSTR(cod_el,''#'',1,10))) liv10 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,11)!= 0 THEN INSTR(cod_el,''#'',1,11)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,12),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,12)-1)-(INSTR(cod_el,''#'',1,11))) liv11 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,12)!= 0 THEN INSTR(cod_el,''#'',1,12)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,13),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,13)-1)-(INSTR(cod_el,''#'',1,12))) liv12 '
         || '       ,SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''#'',1,13)!= 0 THEN INSTR(cod_el,''#'',1,13)+1 ELSE NULL END,DECODE(INSTR(cod_el,''#'',1,14),0,LENGTH(cod_el),INSTR(cod_el,''#'',1,14)-1)-(INSTR(cod_el,''#'',1,13))) liv13 '
         || 'FROM (  '
         || '      SELECT SYS_CONNECT_BY_PATH(cod_elemento_figlio,''#'') cod_el,cod_elemento_figlio,data_attivazione, data_disattivazione, LEVEL LIV '
         || '            ,ROW_NUMBER() OVER ( PARTITION BY cod_elemento_figlio, data_attivazione order by data_attivazione desc) SEQ '
         || '        FROM (SELECT cod_elemento_padre, cod_elemento_figlio, data_attivazione, data_disattivazione '
         || '                FROM #TAB# '
         || '               UNION ALL '
         || '              SELECT cod_elemento_padre, cod_elemento_figlio, data_attivazione, data_disattivazione '
         || '                FROM rel_elementi_ecs_sn '
         || '               WHERE flag_geo = CASE WHEN ''' ||  pTipoGer || ''' = ''GEO'' THEN 1 ELSE 0 END '
         || '               UNION ALL '
         || '              SELECT NULL,cod_elemento '
         || '                     ,to_date(''01011970'',''ddmmyyyy''),to_date(''01013000'',''ddmmyyyy'') '
         || '                FROM elementi WHERE cod_tipo_elemento =''COP'' '
         || '             ) '
         || '       START WITH cod_elemento_padre is NULL '
         || '     CONNECT BY NOCYCLE PRIOR cod_elemento_figlio = cod_elemento_padre '
         || '     ) WHERE seq = 1 '
         || '     ORDER BY liv ';
   v_sql := REPLACE (v_sql, '#TAB#',  v_tab_rel);
--   DBMS_OUTPUT.PUT_LINE(v_sql);
   EXECUTE IMMEDIATE v_sql;
/*
   v_sql:= 'INSERT INTO ' || v_tab_ger
           || '(cod_elemento '
           || ',data_attivazione '
           || ',data_disattivazione '
           || ',l01,l02,l03,l04,l05,l06,l07,l08,l09,l10,l11,l12,l13 '
           || ')'
           || 'SELECT man.cod_elemento '
           || '      ,man.data_fine_last,ger.data_disattivazione '
           || '      ,l01,l02,l03,l04,l05,l06,l07,l08,l09,l10,l11,l12,l13 '
           || '  FROM '
           || '      ('
           || '       SELECT man.cod_elemento '
           || '             ,man.data_inizio '
           || '             ,man.data_fine '
           || '             ,CASE WHEN LEAD(data_inizio) OVER (PARTITION BY man.cod_elemento ORDER BY data_inizio) = data_fine THEN null ELSE data_fine END data_fine_last '
           || '         FROM manutenzione man '
           || '        WHERE percentuale = 0'
           || ') man '
           || ',' || v_tab_ger || ' ger '
           || 'WHERE ger.cod_elemento = man.cod_elemento '
           || '  AND man.data_fine_last >= data_attivazione AND man.data_fine_last< data_disattivazione ';

   --DBMS_OUTPUT.PUT_LINE(v_sql);
   EXECUTE IMMEDIATE v_sql;

   v_sql:= 'MERGE INTO ' || v_tab_ger || ' t USING '
           || '( '
           || 'SELECT cod_elemento, data_attivazione,data_inizio_first '
           || '  FROM '
           || '      ('
           || '       SELECT  ger.cod_elemento,ger.data_attivazione,man.data_inizio, man.data_inizio_first, ger.data_disattivazione '
           || '              ,ROW_NUMBER() OVER( PARTITION BY ger.cod_elemento, ger.data_attivazione ORDER BY data_inizio_first ) num '
           || '         FROM '
           || '             ( '
           || '              SELECT man.cod_elemento '
           || '                    ,man.data_inizio '
           || '                    ,CASE WHEN LAG(NVL(data_fine,TO_DATE(''01013000'',''ddmmyyyy''))) OVER (PARTITION BY man.cod_elemento ORDER BY data_inizio ) = data_inizio THEN null ELSE data_inizio END data_inizio_first '
           || '                FROM manutenzione man '
           || '               WHERE percentuale = 0'
           || '      ) man '
           || ',' || v_tab_ger || ' ger '
           || 'WHERE ger.cod_elemento = man.cod_elemento '
           || ' AND man.data_inizio_first >= data_attivazione AND man.data_inizio_first < data_disattivazione '
           || ' AND data_inizio_first IS NOT NULL '
           || ') WHERE num = 1 '
           || ') s '
           || 'ON (s.cod_elemento = t.cod_elemento and s.data_attivazione = t.data_attivazione) '
           || ' WHEN MATCHED THEN '
           || ' UPDATE SET '
           || ' data_disattivazione = data_inizio_first '
           || ' WHEN NOT MATCHED THEN '
           || ' INSERT '
           || ' (t.cod_elemento) '
           || ' VALUES '
           || '(NULL)';

   --DBMS_OUTPUT.PUT_LINE(v_sql);
   EXECUTE IMMEDIATE v_sql;
*/
--Commentato perch� se si mette in manutenzione tutto il produttore non viene aggregata la misura
   pkg_mago_utl.insLog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setRun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.insLog(proc_name,'Procedure running',99999);
   COMMIT;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.insLog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setRun(proc_name,0,1);
     COMMIT;
END linearizza_gerarchia;
PROCEDURE linearizza_gerarchia_geo AS
BEGIN
    linearizza_gerarchia('GEO');
END;
PROCEDURE linearizza_gerarchia_amm AS
BEGIN
    linearizza_gerarchia('AMM');
END;
PROCEDURE linearizza_gerarchia_imp AS
BEGIN
    linearizza_gerarchia('IMP');
END;
END pkg_gerarchia_dgf;
/
