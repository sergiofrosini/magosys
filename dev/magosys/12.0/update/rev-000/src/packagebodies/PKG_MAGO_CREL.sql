CREATE OR REPLACE PACKAGE BODY PKG_MAGO_CREL
AS

    FUNCTION GetParentsCGTApp(
            pAppDefaults  IN VARCHAR2,
            pElemento     IN VARCHAR2,
            pTipoElemento IN VARCHAR2,
            pData         IN DATE,
            pGerarchia    IN NUMBER,
            pAttributiOut IN INTEGER ,
            pUsaGerEstesa IN INTEGER DEFAULT NULL,
            pTipEleHide   IN VARCHAR2 DEFAULT NULL,
            pTipEleExcl   IN VARCHAR2 DEFAULT NULL)
        RETURN t_GerTable PIPELINED
    IS
    vCursore PKG_UtlGlb.t_query_cur;
    vGerElem t_GerElem;
    vLIV        NUMBER;
    vID_PADRE   NUMBER;
    vID_FIGLIO  NUMBER;
    vSG_COD     VARCHAR(2000);
    vTIPO_ELEM  VARCHAR(2000);
    vNOME_ELEM  VARCHAR(2000);
    vDT_DISCONN DATE;
    vSEQ        NUMBER;
    vFLAGS      VARCHAR(16);
    vDT_INI     DATE;
    vDT_FIN     DATE;
    BEGIN
        CREL.PKG_CREL.GetParentsCGTApp(vCursore, pAppDefaults,
        pElemento, pTipoElemento, pData, pGerarchia, pAttributiOut,
        pUsaGerEstesa, pTipEleHide, pTipEleExcl );
        LOOP
            FETCH
                vCursore
            INTO
                vLIV ,
                vID_PADRE ,
                vID_FIGLIO ,
                vSG_COD ,
                vTIPO_ELEM ,
                vNOME_ELEM ,
                vDT_DISCONN ,
                vSEQ ,
                vFLAGS ,
                vDT_INI ,
                vDT_FIN ;
        EXIT  WHEN vCursore%NOTFOUND;

            vGerElem.LIV        := vLIV ;
            vGerElem.ID_PADRE   := vID_PADRE ;
            vGerElem.ID_FIGLIO  := vID_FIGLIO ;
            vGerElem.SG_COD     := vSG_COD ;
            vGerElem.TIPO_ELEM  := vTIPO_ELEM ;
            vGerElem.NOME_ELEM  := vNOME_ELEM ;
            vGerElem.DT_DISCONN := vDT_DISCONN;
            vGerElem.SEQ        := vSEQ ;
            vGerElem.FLAGS      := vFLAGS ;
            vGerElem.DT_INI     := vDT_INI ;
            vGerElem.DT_FIN     := vDT_FIN ;

            PIPE ROW(vGerElem);

        END LOOP;
        CLOSE vCursore;
        RETURN;
    END GetParentsCGTApp;


    FUNCTION GetGerarchiaCGTApp(
            pAppDefaults   IN VARCHAR2,
            pElementoStart IN VARCHAR2,
            pTipoElemento  IN VARCHAR2,
            pData          IN DATE,
            pGerarchia     IN NUMBER,
            pAttributiOut  IN INTEGER DEFAULT gcAttribOutOff,
            pTipiScelti    IN VARCHAR DEFAULT gcListSeparator,
            pUsaGerEstesa  IN INTEGER DEFAULT NULL,
            pTipEleHide    IN VARCHAR2 DEFAULT NULL,
            pTipEleExcl    IN VARCHAR2 DEFAULT NULL)
        RETURN t_GerTable PIPELINED
    IS
    vCursore PKG_UtlGlb.t_query_cur;
    vGerElem t_GerElem;
    vLIV        NUMBER;
    vID_PADRE   NUMBER;
    vID_FIGLIO  NUMBER;
    vSG_COD     VARCHAR(2000);
    vTIPO_ELEM  VARCHAR(2000);
    vNOME_ELEM  VARCHAR(2000);
    vDT_DISCONN DATE;
    vSEQ        NUMBER;
    vFLAGS      VARCHAR(16);
    vDT_INI     DATE;
    vDT_FIN     DATE;
    BEGIN
        CREL.PKG_CREL.GetGerarchiaCGTApp(vCursore,pAppDefaults,
        pElementoStart,pTipoElemento,pData,pGerarchia,pAttributiOut,pTipiScelti
        ,pUsaGerEstesa,pTipEleHide,pTipEleExcl);
        LOOP
            FETCH
                vCursore
            INTO
                vLIV ,
                vID_PADRE ,
                vID_FIGLIO ,
                vSG_COD ,
                vTIPO_ELEM ,
                vNOME_ELEM ,
                vDT_DISCONN ,
                vSEQ ,
                vFLAGS ,
                vDT_INI ,
                vDT_FIN ;
        EXIT  WHEN vCursore%NOTFOUND;

            vGerElem.LIV        := vLIV ;
            vGerElem.ID_PADRE   := vID_PADRE ;
            vGerElem.ID_FIGLIO  := vID_FIGLIO ;
            vGerElem.SG_COD     := vSG_COD ;
            vGerElem.TIPO_ELEM  := vTIPO_ELEM ;
            vGerElem.NOME_ELEM  := vNOME_ELEM ;
            vGerElem.DT_DISCONN := vDT_DISCONN;
            vGerElem.SEQ        := vSEQ ;
            vGerElem.FLAGS      := vFLAGS ;
            vGerElem.DT_INI     := vDT_INI ;
            vGerElem.DT_FIN     := vDT_FIN ;

            PIPE ROW(vGerElem);

        END LOOP;
        CLOSE vCursore;
        RETURN;
    END GetGerarchiaCGTApp;

    FUNCTION GetGerarchia(
            pElementoStart IN number,
            pData          IN DATE,
            pGerarchia     IN number,
            pAttributiOut IN INTEGER DEFAULT gcAttribOutOff,
            pTipiScelti   IN VARCHAR DEFAULT gcListSeparator,
            pUsaGerEstesa IN INTEGER DEFAULT NULL,
            pTipEleHide   IN VARCHAR2 DEFAULT NULL,
            pTipEleExcl   IN VARCHAR2 DEFAULT NULL,
            pAppDefaults  IN VARCHAR2 DEFAULT 'MAGO')
        RETURN t_GerTable PIPELINED
    IS
    vCursore PKG_UtlGlb.t_query_cur;
    vGerElem t_GerElem;
    vLIV        NUMBER;
    vID_PADRE   NUMBER;
    vID_FIGLIO  NUMBER;
    vSG_COD     VARCHAR(2000);
    vTIPO_ELEM  VARCHAR(2000);
    vNOME_ELEM  VARCHAR(2000);
    vDT_DISCONN DATE;
    vSEQ        NUMBER;
    vFLAGS      VARCHAR(16);
    vDT_INI     DATE;
    vDT_FIN     DATE;
    BEGIN
        CREL.PKG_CREL.GetGerarchia(vCursore, pElementoStart,pData,
        pGerarchia, pAttributiOut, pTipiScelti, pUsaGerEstesa, pTipEleHide,
        pTipEleExcl, pAppDefaults);
        LOOP
            FETCH
                vCursore
            INTO
                vLIV ,
                vID_PADRE ,
                vID_FIGLIO ,
                vSG_COD ,
                vTIPO_ELEM ,
                vNOME_ELEM ,
                vDT_DISCONN ,
                vSEQ ,
                vFLAGS ,
                vDT_INI ,
                vDT_FIN ;
        EXIT  WHEN vCursore%NOTFOUND;

            vGerElem.LIV        := vLIV ;
            vGerElem.ID_PADRE   := vID_PADRE ;
            vGerElem.ID_FIGLIO  := vID_FIGLIO ;
            vGerElem.SG_COD     := vSG_COD ;
            vGerElem.TIPO_ELEM  := vTIPO_ELEM ;
            vGerElem.NOME_ELEM  := vNOME_ELEM ;
            vGerElem.DT_DISCONN := vDT_DISCONN;
            vGerElem.SEQ        := vSEQ ;
            vGerElem.FLAGS      := vFLAGS ;
            vGerElem.DT_INI     := vDT_INI ;
            vGerElem.DT_FIN     := vDT_FIN ;

            PIPE ROW(vGerElem);

        END LOOP;
        CLOSE vCursore;
        RETURN;
    END;
END PKG_MAGO_CREL;
/
