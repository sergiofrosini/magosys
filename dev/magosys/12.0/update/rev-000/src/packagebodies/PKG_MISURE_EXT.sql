CREATE OR REPLACE PACKAGE BODY PKG_MISURE_EXT AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.11.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.10.2     28/04/2016  Moretti C.       Definizione Package
                                           Lettura Curva Misure da TMESX_2

=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

    gSql       VARCHAR2(500);

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetSql_Tmesx2   (pGstElem          IN TMX2_ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                            pTipoMis          IN TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE,
                            pDataDa       IN OUT DATE,
                            pDataA        IN OUT DATE,
                            pAgrTemp      IN OUT NUMBER,
                            pTrattElem       OUT TMX2_TRATT_ELEM.COD_TRATTAMENTO_ELEM%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza i Valori e Aree per recipero misure da TMESX2
-----------------------------------------------------------------------------------------------------------*/
    vTplAcq TMX2_TRATT_ELEM.COD_TIPOLOGIA_MISURA%TYPE  := 'A'; --TMX2_PKG_APPSERV.gcMIS_ACQUISITA
    vTplAgr TMX2_TRATT_ELEM.COD_TIPOLOGIA_MISURA%TYPE  := 'G'; --TMX2_PKG_APPSERV.gcMIS_AGGREGATA
    vTipAgr TMX2_TRATT_ELEM.COD_TIPO_AGGREGAZIONE%TYPE := 'A'; --TMX2_PKG_APPSERV.gcAGG_PUNTUALE
    vTipRis TMX2_TRATT_ELEM.COD_TIPO_RISOLUZIONE%TYPE  := 'A'; --TMX2_PKG_APPSERV.gcRIS_10MINUTI
    vCodEle TMX2_TRATT_ELEM.COD_ELEMENTO%TYPE;
 BEGIN

    pDataDa  := TMX2_PKG_APPSERV.NormalizzaData_Date(pDataDa);
    pDataA   := TMX2_PKG_APPSERV.NormalizzaData_Date(pDataA);

    BEGIN
         -- Da CodGest recupera il Codice Elemento Elemento TMESX_2
        SELECT MAX(COD_ELEMENTO)  INTO vCodEle
          FROM TMX2_ELEMENTI
         WHERE COD_GEST_ELEMENTO = pGstElem
           AND (    DATA_DISATTIVAZIONE  > pDataDa
                AND DATA_ATTIVAZIONE    <= pDataDa);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN vCodEle := NULL;
    END;

    IF pAgrTemp < 10 THEN
        pAgrTemp := 10;
    END IF;

    -- Ottengo il Trattamento Elemento
    SELECT MAX(COD_TRATTAMENTO_ELEM) INTO pTrattElem
      FROM TMX2_TRATT_ELEM
     WHERE COD_ELEMENTO          = vCodEle
       AND COD_TIPO_MISURA       = pTipoMis
       AND COD_TIPO_AGGREGAZIONE = vTipAgr
       AND COD_TIPO_RISOLUZIONE  = vTipRis
       AND COD_TIPOLOGIA_MISURA IN (vTplAcq,vTplAgr);

    gSql := 'SELECT :TipoMis,'                                                                              || -- pTipoMis
                   'PKG_Misure.GetRoundDate(:DataDa,DataMis,:AgrTemp) DataMis,'                             || -- pDataDa,pAgrTemp
                   'CASE WHEN :AgrTemp <= 60 '                                                              || -- pAgrTemp
                         'THEN AVG(ValMis) '                                                                ||
                         'ELSE MAX(ValMis) '                                                                ||
                   'END ValMis,'||PKG_Mago.gcRaggrFonteNonAppl||' '                                         ||
             'FROM (SELECT DataMis + 1/86400 DataMis, ValMis '                                              ||
                     'FROM (SELECT TMX2_PKG_APPSERV.NormalizzaData_Date(M.DATA) DataMis, M.VALORE ValMis '  ||
                             'FROM TMX2_MISURE M '                                                          ||
                            'WHERE M.COD_TRATTAMENTO_ELEM = :TrattElem '                                    || -- pTrattElem
                              'AND M.DATA BETWEEN :DataDa AND :DataA '                                      || -- pDataDa,pDataA
                          ')'                                                                               ||
                  ')'                                                                                       ||
            'GROUP BY PKG_Misure.GetRoundDate(:DataDa,DataMis,:AgrTemp) '                                   || -- pDataDa,pAgrTemp
            'ORDER BY 1';

 END GetSql_Tmesx2;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureTmesx2(pTipoMis          IN TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE,
                           pGstElem          IN TMX2_ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pDataDa           IN DATE,
                           pDataA            IN DATE,
                           pAgrTemp          IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Recupera le Misure (Curve misura) da TMESX_2
    inserisce le tuple in GTTD_MISURE con codice Tipo Fonte = 2 (Fonte Non Applicabile)
    - Questa funzionalita' e' stata Implementata per il popolamento della Global Temporary Table GTTD_MISURE
      in PKG_MISURE.GetMisure; GTTD_MISURE, pertanto e' inizializzata dal chiamante ....
-----------------------------------------------------------------------------------------------------------*/
    vAgrTemp   NUMBER := pAgrTemp;
    vDataDa    DATE   := pDataDa;
    vDataA     DATE   := pDataA;
    vTrattElem TMX2_TRATT_ELEM.COD_TRATTAMENTO_ELEM%TYPE;
 BEGIN
    -- Inizializzo i parametri
    GetSql_Tmesx2(pGstElem,pTipoMis,vDataDa,vDataA,vAgrTemp,vTrattElem);
    -- Inserisco le misure in Global temporary Table
    EXECUTE IMMEDIATE 'INSERT INTO GTTD_MISURE (COD_TIPO_MISURA,DATA,VALORE,COD_TIPO_FONTE) '||gSql
                USING pTipoMis,vDataDa,vAgrTemp,vAgrTemp,vTrattElem,vDataDa,vDataA,vDataDa,vAgrTemp;
 END GetMisureTmesx2;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureTmesx2(pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                           pTipoMis          IN TMX2_TRATT_ELEM.COD_TIPO_MISURA%TYPE,
                           pGstElem          IN TMX2_ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pDataDa           IN DATE,
                           pDataA            IN DATE,
                           pAgrTemp          IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Recupera le Misure (Curve misura) da TMESX_2 e restituisce il relatovo cirsore la cui chiusura
    � a carico del chiamante
-----------------------------------------------------------------------------------------------------------*/
    vAgrTemp   NUMBER := pAgrTemp;
    vDataDa    DATE   := pDataDa;
    vDataA     DATE   := pDataA;
    vTrattElem TMX2_TRATT_ELEM.COD_TRATTAMENTO_ELEM%TYPE;
 BEGIN
    -- Inizializzo i parametri
    GetSql_Tmesx2(pGstElem,pTipoMis,vDataDa,vDataA,vAgrTemp,vTrattElem);
    -- Apro il cursore
    OPEN pRefCurs FOR gSql
                USING pTipoMis,vDataDa,vAgrTemp,vAgrTemp,vTrattElem,vDataDa,vDataA,vDataDa,vAgrTemp;
 END GetMisureTmesx2;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_MISURE_EXT;
/
