CREATE OR REPLACE PACKAGE BODY PKG_TRT_MIS AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.i
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gStartDate         DATE;

 gTipoInstallazione DEFAULT_CO.TIPO_INST%TYPE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AddMisureLV           (pMisureLV      IN t_TRTMISLV_aRRAY) IS

/*-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------*/
    vLog     PKG_Logs.t_StandardLog;

BEGIN

    IF pMisureLV.LAST IS NULL THEN

        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_TRT_MIS.AddMisureLV',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');

        PKG_Logs.StdLogPrint(vLog);
        RETURN;

    END IF;


  DELETE FROM TRT_MIS_LV;


  FOR iObj IN pMisureLV.first..pMisureLV.last loop


    INSERT INTO TRT_MIS_LV
    (
      COLNUMBER           ,
      AUINUMBER           ,
      TRANSFNUMBERNODE    ,
      ACTTRANSFNUMBERNODE ,
      AUICODE             ,
      VAL_POTENZA         ,
      VAL_CLIENTI         ,
      CTYPE_INDEX
    )
    VALUES
    (
      pMisureLV(iObj).COLNUMBER           ,
      pMisureLV(iObj).AUINUMBER           ,
      pMisureLV(iObj).TRANSFNUMBERNODE    ,
      pMisureLV(iObj).ACTTRANSFNUMBERNODE ,
      pMisureLV(iObj).AUICODE            ,
      pMisureLV(iObj).VAL_POTENZA      ,
      pMisureLV(iObj).VAL_CLIENTI      ,
      pMisureLV(iObj).CTYPE_INDEX
    );

  end loop;

--  commit;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.AddMisureLV'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddMisureLV;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AddMisureMVCli        (pMisureMVCli   IN t_TRTMISMVCLI_aRRAY) IS

/*-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------*/

    vLog     PKG_Logs.t_StandardLog;

BEGIN

    IF pMisureMVCli.LAST IS NULL THEN

        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_TRT_MIS.AddMisureMVCli',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');

        PKG_Logs.StdLogPrint(vLog);
        RETURN;

    END IF;

  DELETE FROM TRT_MIS_MV_CLIENTI;

  FOR iObj IN pMisureMVCli.first..pMisureMVCli.last loop


    INSERT INTO TRT_MIS_MV_CLIENTI
    (
      COLNUMBER  ,
      POD        ,
      AUI        ,
      IND_LCURVE ,
      VAL_AVGAP  ,
      VAL_AVGAQ
    )
    VALUES
    (
      pMisureMVCli(iObj).COLNUMBER  ,
      pMisureMVCli(iObj).POD        ,
      pMisureMVCli(iObj).AUI        ,
      pMisureMVCli(iObj).IND_LCURVE ,
      pMisureMVCli(iObj).VAL_AVGAP  ,
      pMisureMVCli(iObj).VAL_AVGAQ
    );

  end loop;

--  commit;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.AddMisureLV'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddMisureMVCli;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AddMisureMVTrend      (pMisureMVTrend IN t_TRTMISMVTRD_aRRAY)  IS

/*-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------*/

    vLog     PKG_Logs.t_StandardLog;

BEGIN


    IF pMisureMVTrend.LAST IS NULL THEN

        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_TRT_MIS.AddMisureMVTrend',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');

        PKG_Logs.StdLogPrint(vLog);
        RETURN;

    END IF;


  FOR iObj IN pMisureMVTrend.first..pMisureMVTrend.last loop

    BEGIN

        INSERT INTO TRT_MIS_MV_TREND
        (
          IND_LCURVE   ,
          STAGIONE     ,
          GIORNO       ,
          CAMPIONE     ,
          TIPO_POTENZA ,
          VALORE
        )
        VALUES
        (
          pMisureMVTrend(iObj).IND_LCURVE   ,
          pMisureMVTrend(iObj).STAGIONE     ,
          pMisureMVTrend(iObj).GIORNO       ,
          pMisureMVTrend(iObj).CAMPIONE     ,
          pMisureMVTrend(iObj).TIPO_POTENZA ,
          pMisureMVTrend(iObj).VALORE
        );

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN

        UPDATE TRT_MIS_MV_TREND
        SET
          VALORE = pMisureMVTrend(iObj).VALORE
        WHERE IND_LCURVE   = pMisureMVTrend(iObj).IND_LCURVE
          AND STAGIONE     = pMisureMVTrend(iObj).STAGIONE
          AND GIORNO       = pMisureMVTrend(iObj).GIORNO
          AND CAMPIONE     = pMisureMVTrend(iObj).CAMPIONE
          AND TIPO_POTENZA = pMisureMVTrend(iObj).TIPO_POTENZA;

    END;

  end loop;

  commit;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.AddMisureLV'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddMisureMVTrend;

-- ----------------------------------------------------------------------------------------------------------

procedure GetClienti(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                     pData           IN DATE,
                     pTipiElemento   IN VARCHAR2 DEFAULT PKG_Mago.cSepCharLst) IS


    vTmp           PKG_UtlGlb.t_SplitTbl;

    vTipProdA       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
    vTipProdB       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
    vTipProdM       ELEMENTI.COD_TIPO_ELEMENTO%TYPE;

 BEGIN

    vTipProdA := NULL;
    vTipProdB := NULL;

    vTmp := PKG_UtlGlb.SplitString(pTipiElemento,PKG_Mago.cSepCharLst);

    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcClienteAT THEN
            vTipProdA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcClienteBT THEN
            vTipProdB := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcclienteMT THEN
            vTipProdM := vTmp(i);
        END IF;
    END LOOP;



    OPEN pRefCurs FOR
        with
        eleP as (SELECT * FROM ELEMENTI WHERE COD_TIPO_ELEMENTO IN (vTipProdA,vTipProdB,vTipProdM)),
        eleD AS (SELECT * FROM elementi_Def WHERE pData BETWEEN data_attivazione AND data_disattivazione)
              SELECT e.cod_gest_elemento  cod_gest,
                     cod_elemento,
                     e.cod_tipo_elemento,
                     d.rif_elemento pod,
                     d.cod_tipo_cliente
               FROM eleD d join eleP e using (cod_elemento) ;


        /*
        guarda il pod dei padri?
               with
eleP as (SELECT * FROM ELEMENTI WHERE COD_TIPO_ELEMENTO IN ('PMT','PAT','PBT')),
eleD AS (SELECT * FROM elementi_Def WHERE sysdate BETWEEN data_attivazione AND data_disattivazione)
      SELECT e.cod_gest_elemento  cod_gest,
             cod_elemento,
             e.cod_tipo_elemento,
             NVL(d.rif_elemento,pd.rif_elemento) pod
      FROM eleD d join eleP e using (cod_elemento)
      left join eleD pd on (e.cod_gest_elemento=pd.id_elemento) ;
      */



END;


-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AddMisureLVProfile      (pMisureLvProfile IN t_TRTMISLVPROFILE_ARRAY)  IS

/*-----------------------------------------------------------------------------------------------------------
-----------------------------------------------------------------------------------------------------------*/

    vLog     PKG_Logs.t_StandardLog;

BEGIN


    IF pMisureLvProfile.LAST IS NULL THEN

        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_TRT_MIS.AddMisureLVProfile',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');

        PKG_Logs.StdLogPrint(vLog);
        RETURN;

    END IF;


  FOR iObj IN pMisureLvProfile.first..pMisureLvProfile.last loop

    BEGIN

        INSERT INTO TRT_MIS_LV_PROFILE
        (
          STAGIONE     ,
          GIORNO       ,
          CAMPIONE     ,
          CTYPE        ,
          VALORE
        )
        VALUES
        (
          pMisureLvProfile(iObj).STAGIONE     ,
          pMisureLvProfile(iObj).GIORNO       ,
          pMisureLvProfile(iObj).CAMPIONE     ,
          pMisureLvProfile(iObj).CTYPE        ,
          pMisureLvProfile(iObj).VALORE
        );

    EXCEPTION
      WHEN DUP_VAL_ON_INDEX THEN

        UPDATE TRT_MIS_LV_PROFILE
        SET
          VALORE = pMisureLvProfile(iObj).VALORE
        WHERE STAGIONE     = pMisureLvProfile(iObj).STAGIONE
          AND GIORNO       = pMisureLvProfile(iObj).GIORNO
          AND CAMPIONE     = pMisureLvProfile(iObj).CAMPIONE
          AND CTYPE     = pMisureLvProfile(iObj).CTYPE;

    END;

  end loop;

  commit;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.AddMisureLVProfile'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddMisureLVProfile;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetFileElaborated(pFileinfo T_FILE_INFO_OBJ) IS

BEGIN

  INSERT INTO FILE_PROCESSED
  (
    NOME_FILE,
    TIPO_FILE,
    DATA_ELAB,
    PROCESSO
  )
  VALUES
  (
    pFileinfo.NOME_FILE,
    pFileinfo.TIPO_FILE,
    pFileinfo.DATA_ELAB,
    pFileinfo.PROCESSO
  );

  COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_TRT_MIS.SetFileElaborated'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetFileElaborated(pRefCurs  OUT PKG_UtlGlb.t_query_cur,
                           pNomefile   IN VARCHAR2 DEFAULT NULL,
                           pTipofile   IN VARCHAR2 DEFAULT NULL) IS

BEGIN

    OPEN pRefCurs FOR
        SELECT *
        FROM FILE_PROCESSED
        WHERE NOME_FILE = NVL(pNomefile,NOME_FILE)
        AND TIPO_FILE = NVL(pTipofile,TIPO_FILE)
        ORDER BY NOME_FILE, TIPO_FILE;

END;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN

PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Misure',
                          pStoreOnFile => FALSE);


END PKG_TRT_MIS;
/
