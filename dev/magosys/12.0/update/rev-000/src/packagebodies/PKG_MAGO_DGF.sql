CREATE OR REPLACE PACKAGE BODY pkg_mago_dgf AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_Dgf
   PURPOSE:    Servizi per la gestione del caricamento dai Eolico e Solare
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      20/04/2012  Paolo Campi      Created this package.
   2.1.9      07/09/2017  Stefano Forno    Utilizzo anche senza produttori_tmp
   										   MAGO-1327 - Misure Aggregate anche in stato attuale
   										   MAGO-1311 - Supportare sia PG che PAG nei file .csv per il caricamento delle misure PG/QG
   										   Fattore di conversione del dato ingresso preso dalla tabella TIPI_MISURA_CONV_ORIG
   										   -- Spec. 2.1.9
   2.1.10     19/09/2017  Stefano Forno    fix load misure valori ammessi da '999999D999' a '999999999D999'
   NOTES:
*********************************************************************************************************** */
proc_name VARCHAR2(50);
--vLog         PKG_Mago.t_StandardLog;
err_code NUMBER;
err_msg VARCHAR2(250);
read_err exception;
pragma exception_init(read_err, -29913);
PROCEDURE initMagoDGF AS
BEGIN
 pkg_mago_utl.run_application_group('INIT');
 pkg_mago_utl.run_application_group('ANAGRAFICA');
 pkg_mago_utl.run_application_group('MISURE');
END;
PROCEDURE set_dataval AS
BEGIN
   UPDATE cfg_application_parameter SET par_value_date = TRUNC(sysdate,'mi')
    WHERE parameter_name = 'RUN.DATA';

   COMMIT;
END;
FUNCTION checkMis( checktype IN VARCHAR2 ) RETURN NUMBER AS
   valcheck VARCHAR2(50);
   vrunid NUMBER;
BEGIN
   SELECT par_value_char
     INTO valcheck
     FROM cfg_application_parameter
    WHERE parameter_name = checktype;
   SELECT run_id
     INTO vrunid
     FROM application_run;
     IF valcheck = 'NONE' THEN
                RETURN 1;
     END IF;
   CASE checktype
      WHEN 'MIS.NOELEM' THEN
         INSERT INTO misure_err
            (run_id
             ,cg_impianto
             ,errore)
         SELECT vrunid
               ,substr(cg_impianto,0,14) cg_impianto
               ,checktype
           FROM (
                 SELECT TRIM(cg_impianto) cg_impianto
                   FROM misure_tmp
                  MINUS
                 SELECT cod_gest_elemento
                   FROM elementi
                  --WHERE cod_tipo_elemento IN ('GAT','GMT','GBT')
                )
          WHERE cg_impianto IS NOT NULL;
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Elementi Misure failed',3);
             IF valcheck = 'ERROR' THEN
                RETURN 0;
             END IF;
         END IF;
         RETURN 1;
      WHEN 'MIS.NOMIS' THEN
         INSERT INTO misure_err
            (run_id
             ,cg_impianto
             ,errore)
         SELECT vrunid
              -- ,cod_gest_elemento cg_impianto
               ,substr(cod_gest_elemento,0,14) cg_impianto
               ,checktype
           FROM elementi
          --WHERE cod_tipo_elemento IN ('GAT','GMT','GBT')
          MINUS
         SELECT vrunid
               ,TRIM(cg_impianto) cg_impianto
               ,checktype
           FROM misure_tmp;
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Misure Elementi failed',4);
             IF valcheck = 'ERROR' THEN
                RETURN 0;
             END IF;
         END IF;
         RETURN 1;
      WHEN 'MIS.DUPL' THEN
         INSERT INTO misure_err
         (run_id
          ,cg_impianto
          ,data
          ,errore
         )
         SELECT vrunid, substr(cg_impianto,0,14) cg_impianto , TO_DATE(TO_CHAR(data,'dd/mm/yyyy') || ora, 'dd/mm/yyyyhh24.mi.ss') , 'MIS.DUPL_SAME'
           FROM misure_tmp
           WHERE TRIM(cg_impianto) IS NOT NULL
          GROUP BY cg_impianto , TO_DATE(TO_CHAR(data,'dd/mm/yyyy') || ora, 'dd/mm/yyyyhh24.mi.ss'), COD_TIPO_MISURA,  'MIS.DUPL_SAME'  -- MAGO-1298
         HAVING COUNT (*) > 1;
         IF SQL%ROWCOUNT > 0 THEN
            pkg_mago_utl.insLog(proc_name,'Check Misure Duplicate Same File failed',2);
            DELETE FROM misure_tmp
             WHERE cg_impianto IN ( SELECT DISTINCT cg_impianto
                                      FROM misure_err
                                      WHERE ERRORE = 'MIS.DUPL_SAME'
                                        AND run_id = vrunid);
         END IF;
         INSERT INTO misure_err
         ( run_id
          ,cod_trattamento_elem
          ,cg_impianto
          ,cod_tipo_misura
          ,data
          ,errore
         )
         SELECT vrunid,tr.COD_TRATTAMENTO_ELEM,el.cod_gest_elemento,tr.cod_tipo_misura, data,checktype  || ':' || valcheck
           FROM misure_acquisite mis
                ,trattamento_elementi tr
                ,elementi el
          WHERE mis.cod_trattamento_elem = tr.cod_trattamento_elem
            AND el.cod_elemento = tr.cod_elemento
            AND el.cod_elemento in  (SELECT distinct el.cod_elemento from misure_tmp mis,trattamento_elementi tr,elementi el where mis.cg_impianto = el.cod_gest_elemento and el.cod_elemento = tr.cod_elemento)
            --AND tr.cod_tipo_elemento IN ('GAT','GMT','GBT')
            --AND tr.cod_tipo_misura IN ('VVE','DVE','TMP','PAG','IRR')
      INTERSECT
         SELECT vrunid,tr.COD_TRATTAMENTO_ELEM,el.cod_gest_elemento,tr.cod_tipo_misura
               ,TO_DATE(TO_CHAR(data,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24.mi.ss'),checktype  || ':' || valcheck
           from misure_tmp mis
               ,trattamento_elementi tr
               ,elementi el
          where mis.cg_impianto = el.cod_gest_elemento
            and el.cod_elemento = tr.cod_elemento;
            --and el.cod_tipo_elemento in ('GAT','GMT','GBT')
            --and tr.cod_tipo_misura IN ('VVE','DVE','TMP','PAG','IRR');
          IF SQL%ROWCOUNT> 0 THEN
             pkg_mago_utl.insLog(proc_name,'Check Misure Duplicate failed',5);
             IF valcheck = 'SKIP' THEN
                DELETE FROM misure_tmp
                 WHERE (cg_impianto, data)
                   IN (SELECT cg_impianto, data
                          FROM misure_err
                         WHERE errore = checktype || ':' || valcheck
                           AND run_id = vrunid
                       );
             ELSIF valcheck = 'SUBSTITUTE' THEN
                DELETE FROM misure_acquisite
                 WHERE (cod_trattamento_elem, data)
                    IN (SELECT cod_trattamento_elem, data
                          FROM misure_err
                         WHERE errore = checktype || ':' || valcheck
                           AND run_id = vrunid
                      );
             ELSE
                RETURN 0;
             END IF;
          END IF;
          RETURN 1;
     END CASE;
EXCEPTION WHEN NO_DATA_FOUND THEN
   pkg_mago_utl.insLog(proc_name,'Check Misure No CFG ' || checktype,1);
   return 1;
END;
PROCEDURE LOAD_PRODUTTORI AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_PRODUTTORI';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table produttori_tmp';
   INSERT INTO produttori_tmp
          ( cod_gruppo
           ,descr_gruppo
           ,cod_contatore_fis
           ,cg_produttore
           ,descr_produttore
           ,cod_contatore_sc
           ,cg_generatore
           ,comune
           ,cod_comune
           ,tipo_coord_p
           ,coord_nord_p
           ,coord_est_p
           ,coord_up_p
           ,tipo_coord_a
           ,coord_nord_a
           ,coord_est_a
           ,coord_up_a
           ,h_anem
           ,tipo_rete
           ,fonte
          )
   SELECT UPPER(REPLACE(cod_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,descr_gruppo
           ,UPPER(REPLACE(cod_contatore_fis,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,descr_produttore cg_produttore -- tolta la generazione concordata del Codice Gestionale dal Nome Produttore
           --,CASE WHEN descr_produttore IS NOT NULL THEN UPPER( SUBSTR(REPLACE(descr_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem),1,3) || UPPER( SUBSTR(REPLACE(descr_gruppo,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem),-3,3) || '-' || SUBSTR(descr_produttore,1,3) || SUBSTR(descr_produttore,-3,3))) END cg_produttore
           ,descr_produttore
           ,UPPER(REPLACE(cod_contatore_sc,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,UPPER(REPLACE(cg_generatore,pkg_mago_utl.gcSeparatoreElem,pkg_mago_utl.gcSostitutSeparatoreElem))
           ,UPPER(comune)
           ,cod_comune
           ,tipo_coord_p
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_nord_p,',','.'),chr(176),' '),'"',' '),'''',' '),'  ',' ')
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_est_p,',','.'),chr(176),' '),'"',' '),'''',' '),'  ',' ')
           ,TO_NUMBER(coord_up_p,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,tipo_coord_a
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_nord_a,',','.'),chr(176),' '),'"',' '),'''',' '),'  ',' ')
           ,REPLACE(REPLACE(REPLACE(REPLACE(REPLACE(coord_est_a,',','.'),chr(176),' '),'"',' '),'''',' '),'  ',' ')
           ,TO_NUMBER(coord_up_a,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,TO_NUMBER(h_anem,'9999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
           ,tipo_rete
           ,fonte
     FROM produttori_tmp_ext
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when read_err then
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_PRODUTTORI;
PROCEDURE LOAD_DEF_EOLICO AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_DEF_EOLICO';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table elem_def_eolico_tmp';
   INSERT INTO elem_def_eolico_tmp
          ( cg_generatore
           ,potenza_installata
           ,marca_turbina
           ,modello_turbina
           ,h_mozzo
           ,vel_cutin
           ,vel_cutoff
           ,vel_max
           ,vel_recutin
           ,delta_cutoff
           ,delta_recutin
           ,wind_sect_manager
           ,start_wind_sect
           ,stop_wind_sect
           ,vel_cutoff_shdown
           ,vel_recutin_shdown
          )
   SELECT UPPER(cg_generatore)
         ,TO_NUMBER(potenza_installata,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''') * 1000
         ,marca_turbina
         ,modello_turbina
         ,TO_NUMBER(altezza_mozzo,'9999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutoff,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_max,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_recutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(delta_cutoff,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(delta_recutin,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,wind_sect_manager
         ,TO_NUMBER(start_wind_sect,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(stop_wind_sect,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_cutoff_shdown,'99999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(vel_recutin_shdown,'99999D99','NLS_NUMERIC_CHARACTERS = ''.,''')
     FROM ELEM_DEF_EOLICO_TMP_EXT
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when read_err then
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_DEF_EOLICO;
PROCEDURE LOAD_DEF_SOLARE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_DEF_SOLARE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table elem_def_solare_tmp';
   INSERT INTO elem_def_solare_tmp
          ( cg_generatore
           ,potenza_installata
           ,id_tecnologia
           ,flag_tracking
           ,orientazione
           ,inclinazione
           ,superficie
          )
   SELECT UPPER(cg_generatore)
         ,TO_NUMBER(potenza_installata,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,id_tecnologia
         ,flag_tracking
         ,TO_NUMBER(orientazione,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(inclinazione,'999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
         ,TO_NUMBER(superficie,'999999D999','NLS_NUMERIC_CHARACTERS = ''.,''')
     FROM ELEM_DEF_SOLARE_TMP_EXT
     WHERE cg_generatore IS NOT NULL;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when read_err then
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_DEF_SOLARE;
PROCEDURE LOAD_MISURE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.LOAD_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   EXECUTE IMMEDIATE 'truncate table misure_tmp';

   -- Fattore di conversione del dato ingresso preso dalla tabella TIPI_MISURA_CONV_ORIG
   INSERT INTO misure_tmp
          ( cg_impianto
           ,data
           ,ora
           ,valore
           ,cod_tipo_misura
          )
   SELECT  cg_impianto
           ,to_date(data,'dd/mm/yyyy')
           ,lpad(substr(replace (ora, ':','.'),1,instr(replace (ora, ':','.'),'.')-1),2,'0') || substr(replace (ora, ':','.'),instr(replace (ora, ':','.'),'.'))
           ,TO_NUMBER(substr(replace(valore,'.',','),1,instr(replace(valore,'.',','),',')+3),'999999999D999','NLS_NUMERIC_CHARACTERS = '',.''') *
           PKG_MISURE.GetMisuraConv('METEO',null,decode(tipo_misura ,'PG','PAG' , tipo_misura),decode(tipo_misura ,'PG','PAG' , tipo_misura))
           ,decode(tipo_misura ,'PG','PAG' , tipo_misura)  tipo_misura   -- MAGO - 1311
     FROM misure_tmp_ext;

   /*INSERT INTO misure_tmp
          ( cg_impianto
           ,data
           ,ora
           ,valore
           ,cod_tipo_misura
          )
   SELECT  cg_impianto
           ,to_date(data,'dd/mm/yyyy')
           ,lpad(substr(replace (ora, ':','.'),1,instr(replace (ora, ':','.'),'.')-1),2,'0') || substr(replace (ora, ':','.'),instr(replace (ora, ':','.'),'.'))
           ,TO_NUMBER(substr(replace(valore,'.',','),1,instr(replace(valore,'.',','),',')+3),'999999D999','NLS_NUMERIC_CHARACTERS = '',.''')*1000
           ,decode(tipo_misura ,'PG','PAG' , tipo_misura)  tipo_misura   -- MAGO - 1311
     FROM misure_tmp_ext;
     */
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when read_err then
      ROLLBACK;
      pkg_mago_utl.inslog(proc_name,'File non standard',-29913);
      pkg_mago_utl.setrun(proc_name,0,1);
      COMMIT;
  when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_MISURE;
PROCEDURE INSERT_ELEMENT AS
com_lost NUMBER;
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_ELEMENT';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   SELECT count(*)
     INTO com_lost
     FROM produttori_tmp prod
         ,sar_admin.comuni com
    WHERE com.cod_istat_comune (+) = prod.cod_comune
      AND com.cod_istat_comune IS NULL;
   IF com_lost > 0 THEN
      RAISE_APPLICATION_ERROR(-20001,'ISTAT Comune not Found');
   END IF;
   INSERT INTO elementi
          ( cod_gest_elemento
           ,cod_tipo_elemento
          )
   SELECT new.cod_gest_elemento
         ,new.cod_tipo_elemento
     FROM (
           --ELEMENTO PER GERARCHIA AMMINISTRATIVA
           select distinct
                   cod_gruppo cod_gest_elemento
                   ,tprod.cod_tipo_elemento
              from produttori_tmp prod
                 ,tipi_elemento tprod
            WHERE tprod.cod_tipo_elemento = PKG_MAGO.gcZona
            UNION ALL
           --ELEMENTI PER GERARCHIA DI CAB SEC
           SELECT DISTINCT
                  prod.cg_produttore cod_gest_elemento
                 ,tprod.cod_tipo_elemento
             FROM produttori_tmp prod
                 ,tipi_elemento tprod
                 ,tipi_rete tret
            WHERE tprod.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
              AND cg_produttore IS NOT NULL
              AND tret.cod_tipo_rete = tprod.cod_tipo_rete
              AND prod.tipo_rete = tret.descrizione
            UNION ALL
           SELECT prod.cg_generatore
                 ,tprod.cod_tipo_elemento
             FROM produttori_tmp prod
                 ,tipi_elemento tprod
                 ,tipi_rete tret
            WHERE tprod.cod_tipo_elemento in ( PKG_MAGO.gcGeneratoreAT , PKG_MAGO.gcGeneratoreMT , PKG_MAGO.gcGeneratoreBT )
              AND tret.cod_tipo_rete = tprod.cod_tipo_rete
              AND prod.tipo_rete = tret.descrizione
            UNION ALL
            --ELEMENTI PER GERARCHIA GEOGRAFICA (COMUNE, PROVINCIA, PRODUTTORE@COMUNE)
           SELECT DISTINCT
                  cod_comune cod_gest_elemento
                 ,tprod.cod_tipo_elemento
             FROM produttori_tmp prod
                 ,tipi_elemento tprod
                 ,sar_admin.comuni com
            WHERE tprod.cod_tipo_elemento = PKG_MAGO.gcComune
              AND com.cod_istat_comune = prod.cod_comune
            UNION ALL
           SELECT DISTINCT
                  prv.sigla
                 ,tprod.cod_tipo_elemento
             FROM produttori_tmp prod
                 ,tipi_elemento tprod
                 ,sar_admin.comuni com
                 ,sar_admin.province prv
            WHERE  com.cod_istat_prov = prv.cod_istat_prov
              AND com.cod_istat_comune = prod.cod_comune
              AND tprod.cod_tipo_elemento = PKG_MAGO.gcProvincia
            UNION ALL
           SELECT cod_gest_elemento , cod_tipo_elemento
             FROM (
                  SELECT cod_gest_elemento , cod_tipo_elemento
                        ,COUNT(*) OVER( PARTITION BY cg_produttore ) tot
                   FROM(
                        SELECT DISTINCT
                               prod.cod_comune || pkg_mago_utl.gcSeparatoreElem || prod.cg_produttore cod_gest_elemento
                              ,tprod.cod_tipo_elemento
                              ,prod.cg_produttore
                          FROM produttori_tmp prod
                              ,tipi_elemento tprod
                              ,tipi_rete tret
                              ,sar_admin.comuni com
                         WHERE tprod.cod_tipo_elemento in ( PKG_MAGO.gcClienteAT , PKG_MAGO.gcClienteMT , PKG_MAGO.gcClienteBT /*'CMT' , 'CBT', 'CAT' */ )
                           AND  prod.cg_produttore IS NOT NULL
                           AND tret.cod_tipo_rete = tprod.cod_tipo_rete
                           AND prod.tipo_rete = tret.descrizione
                           AND com.cod_istat_comune = prod.cod_comune
                       )
                  )
            WHERE tot > 1
          ) new
          ,elementi old
    WHERE new.cod_gest_elemento = old.cod_gest_elemento(+)
      AND old.cod_gest_elemento IS NULL ;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_ELEMENT;
PROCEDURE INSERT_DEF (pData IN DATE) AS
BEGIN
/* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
   Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce
   ma si spostera' la data validita' attraverso le merge successive
*/
proc_name := 'PKG_MAGO_DGF.INSERT_DEF';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO elementi_def
          ( cod_elemento
           ,nome_elemento
           ,cod_tipo_elemento
           ,cod_tipo_fonte
           ,cod_tipo_cliente
           ,potenza_installata
           ,fattore
           ,rif_elemento
           ,id_elemento
           ,cod_geo
           ,cod_geo_a
           ,coordinata_x
           ,coordinata_y
           ,altitudine
           ,h_anem
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (SELECT v.*
          FROM v_elementi_dgf v
       )
      , old_data as
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              elementi_def el
      )
   SELECT cod_elemento_new
         ,nome_elemento_new
         ,cod_tipo_elemento_new
         ,cod_tipo_fonte_new
         ,cod_tipo_cliente_new
         ,potenza_installata_new
         ,fattore_new
         ,rif_elemento_new
         ,id_elemento_new
         ,cod_geo_new
         ,cod_geo_a_new
         ,coordinata_x_new
         ,coordinata_y_new
         ,altitudine_new
         ,h_anem_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento_new) tot_diff -- E' possibile che ci siano pi� record di cui uno uguale e uno no
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento_new ORDER BY data_attivazione_new) num
                 ,tot
                 ,cod_elemento_new
                 ,nome_elemento_new
                 ,cod_tipo_elemento_new
                 ,cod_tipo_fonte_new
                 ,cod_tipo_cliente_new
                 ,potenza_installata_new
                 ,fattore_new
                 ,rif_elemento_new
                 ,id_elemento_new
                 ,cod_geo_new
                 ,cod_geo_a_new
                 ,coordinata_x_new
                 ,coordinata_y_new
                 ,altitudine_new
                 ,h_anem_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY new.cod_elemento) tot
                         ,new.cod_elemento cod_elemento_new
                         ,new.descrizione nome_elemento_new
                         ,new.cod_tipo_elemento cod_tipo_elemento_new
                         ,new.cod_tipo_cliente cod_tipo_cliente_new
                         ,new.fonte cod_tipo_fonte_new
                         ,new.potenza_installata potenza_installata_new
                         ,new.fattore fattore_new
                         ,new.cod_comune rif_elemento_new
                         ,new.impianto id_elemento_new
                         ,new.cod_geo cod_geo_new
                         ,new.cod_geo_a cod_geo_a_new
                         ,new.coordinata_x coordinata_x_new
                         ,new.coordinata_y coordinata_y_new
                         ,new.altitudine altitudine_new
                         ,new.h_anem h_anem_new
                         ,pdata data_attivazione_new
                         ,to_date('01013000','ddmmyyyy') data_disattivazione_new
                         ,old.nome_elemento nome_elemento_old
                         ,old.cod_tipo_elemento cod_tipo_elemento_old
                         ,old.cod_tipo_fonte cod_tipo_fonte_old
                         ,old.cod_tipo_cliente cod_tipo_cliente_old
                         ,old.potenza_installata potenza_installata_old
                         ,old.fattore fattore_old
                         ,old.rif_elemento rif_elemento_old
                         ,old.id_elemento id_elemento_old
                         ,old.cod_geo cod_geo_old
                         ,old.cod_geo_a cod_geo_a_old
                         ,old.coordinata_x coordinata_x_old
                         ,old.coordinata_y coordinata_y_old
                         ,old.altitudine altitudine_old
                         ,old.h_anem h_anem_old
                         ,nvl(old.prev_data_att,old.data_attivazione) data_attivazione_recalc
                         ,nvl(old.next_data_dis,old.data_disattivazione) data_disattivazione_recalc
                         ,old.data_disattivazione data_disattivazione_old
                         ,old.data_attivazione data_attivazione_old
                     FROM
                          (SELECT * FROM new_data) new
                         ,(SELECT * FROM old_data) old
                    WHERE old.cod_elemento(+) = new.cod_elemento
                      AND pdata BETWEEN NVL(old.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))
                      AND old.data_disattivazione(+) > pdata
                  )
            WHERE NVL(nome_elemento_old,' ') != NVL(nome_elemento_new,' ')
               OR NVL(cod_tipo_elemento_old,' ') != NVL(cod_tipo_elemento_new,' ')
               OR NVL(cod_tipo_fonte_old,' ') != NVL(cod_tipo_fonte_new, ' ')
               OR NVL(cod_tipo_cliente_old,' ') != NVL(cod_tipo_cliente_new, ' ')
               OR NVL(potenza_installata_old,0) != NVL(potenza_installata_new,0)
               OR NVL(fattore_old,0) != NVL(fattore_new,0)
               OR NVL(rif_elemento_old,' ') != NVL(rif_elemento_new,' ')
               OR NVL(id_elemento_old,' ') != NVL(id_elemento_new,' ')
               OR NVL(cod_geo_old,' ') != NVL(cod_geo_new,' ')
               OR NVL(cod_geo_a_old,' ') != NVL(cod_geo_a_new,' ')
               OR NVL(coordinata_x_old,0) != NVL(coordinata_x_new,0)
               OR NVL(coordinata_y_old,0) != NVL(coordinata_y_new,0)
               OR NVL(altitudine_old,0) != NVL(altitudine_new,0)
               OR NVL(h_anem_old,0) != NVL(h_anem_new,0)
          )
     WHERE tot = tot_diff
       and num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e,
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO elementi_def t
        USING
       (
        WITH new_data AS
            (
             SELECT v.*
               FROM v_elementi_dgf v
            )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                elementi_def el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT new.cod_elemento cod_elemento
                      ,new.descrizione nome_elemento_new
                      ,new.cod_tipo_elemento cod_tipo_elemento_new
                      ,new.fonte cod_tipo_fonte_new
                      ,new.cod_tipo_cliente cod_tipo_cliente_new
                      ,new.potenza_installata potenza_installata_new
                      ,new.fattore fattore_new
                      ,new.cod_comune rif_elemento_new
                      ,new.cod_geo cod_geo_new
                      ,new.cod_geo_a cod_geo_a_new
                      ,new.coordinata_x coordinata_x_new
                      ,new.coordinata_y coordinata_y_new
                      ,new.altitudine altitudine_new
                      ,new.h_anem h_anem_new
                      ,pdata data_attivazione_new
                      ,nvl(old.next_data_dis,least(pdata,old.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,old.nome_elemento nome_elemento_old
                      ,old.cod_tipo_elemento cod_tipo_elemento_old
                      ,old.cod_tipo_fonte cod_tipo_fonte_old
                      ,old.cod_tipo_cliente cod_tipo_cliente_old
                      ,old.potenza_installata potenza_installata_old
                      ,old.fattore fattore_old
                      ,old.rif_elemento rif_elemento_old
                      ,old.cod_geo cod_geo_old
                      ,old.cod_geo_a cod_geo_a_old
                      ,old.coordinata_x coordinata_x_old
                      ,old.coordinata_y coordinata_y_old
                      ,old.altitudine altitudine_old
                      ,old.h_anem h_anem_old
                      ,old.data_disattivazione data_disattivazione_old
                      ,old.data_attivazione data_attivazione_old
                      ,old.prev_data_att data_attivazione_prec
                      ,old.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) new
                       ,(SELECT * FROM old_data) old
                 WHERE old.cod_elemento = new.cod_elemento
                   AND NVL(old.nome_elemento,' ') = NVL(new.descrizione,' ')
                   AND NVL(old.cod_tipo_elemento,' ') = NVL(new.cod_tipo_elemento,' ')
                   AND NVL(old.cod_tipo_fonte,' ') = NVL(new.fonte, ' ')
                   AND NVL(old.cod_tipo_cliente,' ') = NVL(new.cod_tipo_cliente, ' ')
                   AND NVL(old.potenza_installata,0) = NVL(new.potenza_installata,0)
                   AND NVL(old.fattore,0) = NVL(new.fattore,0)
                   AND NVL(old.rif_elemento,' ') = NVL(new.cod_comune,' ')
                   AND NVL(old.id_elemento,' ') = NVL(new.impianto,' ')
                   AND NVL(old.h_anem,0) = NVL(new.h_anem,0)
                   AND NVL(old.cod_geo,' ') = NVL(new.cod_geo,' ')
                   AND NVL(old.cod_geo_a,' ') = NVL(new.cod_geo_a,' ')
                   AND NVL(old.coordinata_x,0) = NVL(new.coordinata_x,0)
                   AND NVL(old.coordinata_y,0) = NVL(new.coordinata_y,0)
                   AND NVL(old.altitudine,0) = NVL(new.altitudine,0)
                   AND pdata BETWEEN NVL(old.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (pdata  <= old.data_attivazione or pdata > old.data_disattivazione)
                   AND NVL(old.next_data_dis,old.prev_data_att) IS NOT NULL
               )
     ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Cancello tutti quei record che hanno perso validita a causa di variazioni successive arrivate per la stessa data*/
   DELETE FROM elementi_def WHERE data_attivazione >= data_disattivazione;
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO elementi_def t
        USING
       (
        SELECT *
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy')/*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM elementi_def el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,el.num
                                  FROM v_elementi_dgf el
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF;
PROCEDURE INSERT_DEF_EOLICO (pData in DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_DEF_EOLICO';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   /* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
      Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce
      ma si spostera' la data validita' attraverso le merge successive
   */
   INSERT INTO elementi_gdf_eolico
          ( cod_elemento
           ,marca_turbina
           ,modello_turbina
           ,h_mozzo
           ,vel_cutin
           ,vel_cutoff
           ,vel_max
           ,vel_recutin
           ,delta_cutoff
           ,delta_recutin
           ,wind_sect_manager
           ,start_wind_sect
           ,stop_wind_sect
           ,vel_cutoff_shdown
           ,vel_recutin_shdown
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,eol.cg_generatore
               ,eol.marca_turbina
               ,eol.modello_turbina
               ,eol.h_mozzo
               ,eol.vel_cutin
               ,eol.vel_recutin
               ,eol.vel_cutoff
               ,eol.vel_max
               ,eol.delta_cutoff
               ,eol.delta_recutin
               ,CASE WHEN eol.wind_sect_manager = 'SI' THEN 1 else 0 end wind_sect_manager
               ,eol.start_wind_sect
               ,eol.stop_wind_sect
               ,eol.vel_cutoff_shdown
               ,eol.vel_recutin_shdown
          FROM
                elem_def_eolico_tmp eol
               ,elementi el
         WHERE el.cod_gest_elemento = eol.cg_generatore
       )
      , old_data as
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              elementi_gdf_eolico el
      )
   SELECT cod_elemento_new
         ,marca_turbina_new
         ,modello_turbina_new
         ,h_mozzo_new
         ,vel_cutin_new
         ,vel_cutoff_new
         ,vel_max_new
         ,vel_recutin_new
         ,delta_cutoff_new
         ,delta_recutin_new
         ,wind_sect_manager_new
         ,start_wind_sect_new
         ,stop_wind_sect_new
         ,vel_cutoff_shdown_new
         ,vel_recutin_shdown_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento_new) tot_diff
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento_new ORDER BY data_attivazione_new) num
                 ,tot
                 ,cod_elemento_new
                 ,marca_turbina_new
                 ,modello_turbina_new
                 ,h_mozzo_new
                 ,vel_cutin_new
                 ,vel_cutoff_new
                 ,vel_max_new
                 ,vel_recutin_new
                 ,delta_cutoff_new
                 ,delta_recutin_new
                 ,wind_sect_manager_new
                 ,start_wind_sect_new
                 ,stop_wind_sect_new
                 ,vel_cutoff_shdown_new
                 ,vel_recutin_shdown_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY new.cod_elemento) tot
                          ,pdata data_attivazione_new
                          ,new.cod_elemento cod_elemento_new
                          ,new.marca_turbina marca_turbina_new
                          ,new.modello_turbina modello_turbina_new
                          ,new.h_mozzo h_mozzo_new
                          ,new.vel_cutin vel_cutin_new
                          ,new.vel_recutin vel_recutin_new
                          ,new.vel_cutoff vel_cutoff_new
                          ,new.vel_max vel_max_new
                          ,new.delta_cutoff delta_cutoff_new
                          ,new.delta_recutin delta_recutin_new
                          ,new.wind_sect_manager wind_sect_manager_new
                          ,new.start_wind_sect start_wind_sect_new
                          ,new.stop_wind_sect stop_wind_sect_new
                          ,new.vel_cutoff_shdown vel_cutoff_shdown_new
                          ,new.vel_recutin_shdown vel_recutin_shdown_new
                          ,to_date('01013000','ddmmyyyy') data_disattivazione_new
                          ,old.marca_turbina marca_turbina_old
                          ,old.modello_turbina modello_turbina_old
                          ,old.h_mozzo h_mozzo_old
                          ,old.vel_cutin vel_cutin_old
                          ,old.vel_recutin vel_recutin_old
                          ,old.vel_cutoff vel_cutoff_old
                          ,old.vel_max vel_max_old
                          ,old.delta_cutoff delta_cutoff_old
                          ,old.delta_recutin delta_recutin_old
                          ,old.wind_sect_manager wind_sect_manager_old
                          ,old.start_wind_sect start_wind_sect_old
                          ,old.stop_wind_sect stop_wind_sect_old
                          ,old.vel_cutoff_shdown vel_cutoff_shdown_old
                          ,old.vel_recutin_shdown vel_recutin_shdown_old
                          ,nvl(old.prev_data_att,old.data_attivazione) data_attivazione_recalc
                          ,nvl(old.next_data_dis,old.data_disattivazione) data_disattivazione_recalc
                          ,old.data_disattivazione data_disattivazione_old
                          ,old.data_attivazione data_attivazione_old
                     FROM
                          (SELECT * FROM new_data) new
                         ,(SELECT * FROM old_data) old
                    WHERE old.cod_elemento(+) = new.cod_elemento
                      AND new.num = 1
                      AND pdata BETWEEN NVL(old.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))
                      AND old.data_disattivazione(+) > pdata
                  )
            WHERE NVL(marca_turbina_old,' ') != NVL(marca_turbina_new,' ')
               OR NVL(modello_turbina_old,' ') != NVL(modello_turbina_new,' ')
               OR NVL(h_mozzo_old,0) != NVL(h_mozzo_new,0)
               OR NVL(vel_cutin_old,0) != NVL(vel_cutin_new,0)
               OR NVL(vel_recutin_old,0) != NVL(vel_recutin_new,0)
               OR NVL(vel_cutoff_old,0) != NVL(vel_cutoff_new,0)
               OR NVL(vel_max_old,0) != NVL(vel_max_new,0)
               OR NVL(delta_cutoff_old,0) != NVL(delta_cutoff_new,0)
               OR NVL(delta_recutin_old,0) != NVL(delta_recutin_new,0)
               OR NVL(wind_sect_manager_old,0) != NVL(wind_sect_manager_new,0)
               OR NVL(start_wind_sect_old,0) != NVL(start_wind_sect_new,0)
               OR NVL(stop_wind_sect_old,0) != NVL(stop_wind_sect_new,0)
               OR NVL(vel_cutoff_shdown_old,0) != NVL(vel_cutoff_shdown_new,0)
               OR NVL(vel_recutin_shdown_old,0) != NVL(vel_recutin_shdown_new,0)
          )
     WHERE tot = tot_diff
       and num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e,
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO elementi_gdf_eolico t
        USING
       (
        WITH new_data AS
            (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,eol.cg_generatore
               ,eol.marca_turbina
               ,eol.modello_turbina
               ,eol.h_mozzo
               ,eol.vel_cutin
               ,eol.vel_recutin
               ,eol.vel_cutoff
               ,eol.vel_max
               ,eol.delta_cutoff
               ,eol.delta_recutin
               ,CASE WHEN eol.wind_sect_manager = 'SI' THEN 1 else 0 end wind_sect_manager
               ,eol.start_wind_sect
               ,eol.stop_wind_sect
               ,eol.vel_cutoff_shdown
               ,eol.vel_recutin_shdown
          FROM
                elem_def_eolico_tmp eol
               ,elementi el
         WHERE el.cod_gest_elemento = eol.cg_generatore
             )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                elementi_gdf_eolico el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT new.cod_elemento cod_elemento
                      ,new.marca_turbina marca_turbina_new
                      ,new.modello_turbina modello_turbina_new
                      ,new.h_mozzo h_mozzo_new
                      ,new.vel_cutin vel_cutin_new
                      ,new.vel_recutin vel_recutin_new
                      ,new.vel_cutoff vel_cutoff_new
                      ,new.vel_max vel_max_new
                      ,new.delta_cutoff delta_cutoff_new
                      ,new.delta_recutin delta_recutin_new
                      ,new.wind_sect_manager wind_sect_manager_new
                      ,new.start_wind_sect start_wind_sect_new
                      ,new.stop_wind_sect stop_wind_sect_new
                      ,new.vel_cutoff_shdown vel_cutoff_shdown_new
                      ,new.vel_recutin_shdown vel_recutin_shdown_new
                      ,pdata data_attivazione_new
                      ,nvl(old.next_data_dis,least(new.data_attivazione,old.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,old.marca_turbina marca_turbina_old
                      ,old.modello_turbina modello_turbina_old
                      ,old.h_mozzo h_mozzo_old
                      ,old.vel_cutin vel_cutin_old
                      ,old.vel_recutin vel_recutin_old
                      ,old.vel_cutoff vel_cutoff_old
                      ,old.vel_max vel_max_old
                      ,old.delta_cutoff delta_cutoff_old
                      ,old.delta_recutin delta_recutin_old
                      ,old.wind_sect_manager wind_sect_manager_old
                      ,old.start_wind_sect start_wind_sect_old
                      ,old.stop_wind_sect stop_wind_sect_old
                      ,old.vel_cutoff_shdown vel_cutoff_shdown_old
                      ,old.vel_recutin_shdown vel_recutin_shdown_old
                      ,old.data_attivazione data_attivazione_old
                      ,old.data_disattivazione data_disattivazione_old
                      ,old.prev_data_att data_attivazione_prec
                      ,old.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) new
                       ,(SELECT * FROM old_data) old
                 WHERE old.cod_elemento = new.cod_elemento
                   AND new.num = 1
                   AND NVL(old.marca_turbina,' ') = NVL(new.marca_turbina,' ')
                   AND NVL(old.modello_turbina,' ') = NVL(new.modello_turbina,' ')
                   AND NVL(old.h_mozzo,0) = NVL(new.h_mozzo,0)
                   AND NVL(old.vel_cutin,0) = NVL(new.vel_cutin,0)
                   AND NVL(old.vel_recutin,0) = NVL(new.vel_recutin,0)
                   AND NVL(old.vel_cutoff,0) = NVL(new.vel_cutoff,0)
                   AND NVL(old.vel_max,0) = NVL(new.vel_max,0)
                   AND NVL(old.delta_cutoff,0) = NVL(new.delta_cutoff,0)
                   AND NVL(old.delta_recutin,0) = NVL(new.delta_recutin,0)
                   AND NVL(old.wind_sect_manager,0) = NVL(new.wind_sect_manager,0)
                   AND NVL(old.start_wind_sect,0) = NVL(new.start_wind_sect,0)
                   AND NVL(old.stop_wind_sect,0) = NVL(new.stop_wind_sect,0)
                   AND NVL(old.vel_cutoff_shdown,0) = NVL(new.vel_cutoff_shdown,0)
                   AND NVL(old.vel_recutin_shdown,0) = NVL(new.vel_recutin_shdown,0)
                   AND new.data_attivazione BETWEEN NVL(old.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (new.data_attivazione  <= old.data_attivazione or new.data_attivazione > old.data_disattivazione)
                   AND NVL(old.next_data_dis,old.prev_data_att) IS NOT NULL
               )
     ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO elementi_gdf_eolico t
        USING
       (
        SELECT *
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy') /*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM elementi_gdf_eolico el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,ROW_NUMBER() OVER(PARTITION BY el.cod_elemento ORDER BY pData) num
                                  FROM elem_def_eolico_tmp eol
                                      ,elementi el
                                 WHERE eol.cg_generatore = el.cod_gest_elemento
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                           AND new_data.num = 1
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
EXCEPTION WHEN OTHERS THEN
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF_EOLICO;
PROCEDURE INSERT_DEF_SOLARE (pData IN DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_DEF_SOLARE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   /* Si inseriscono i record che hanno un valore differente dal record corrente, precedente o successivo
      Nel caso in cui il record � differente dal corrente ma � uguale al precedente o successivo non si inserisce
      ma si spostera' la data validita' attraverso le merge successive
   */
   INSERT INTO elementi_gdf_solare
          ( cod_elemento
           ,orientazione
           ,id_tecnologia
           ,flag_tracking
           ,inclinazione
           ,superficie
           ,data_attivazione
           ,data_disattivazione
          )
   WITH new_data AS
       (
        SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
               ,pData data_attivazione
               ,el.cod_elemento
               ,sol.cg_generatore
               ,sol.orientazione
               ,CASE WHEN sol.flag_tracking = 'SI' THEN 1 else 0 end flag_tracking
               ,sol.id_tecnologia
               ,sol.inclinazione
               ,sol.superficie
          FROM
                elem_def_solare_tmp sol
               ,elementi el
         WHERE el.cod_gest_elemento = sol.cg_generatore
       )
      , old_data as
      (
       SELECT el.*
             ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
             ,LEAD(data_disattivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_disattivazione) next_data_dis
         FROM
              elementi_gdf_solare el
      )
   SELECT cod_elemento
         ,orientazione_new
         ,id_tecnologia_new
         ,flag_tracking_new
         ,inclinazione_new
         ,superficie_new
         ,data_attivazione_new
         ,data_disattivazione_new
     FROM (
           SELECT COUNT(*) OVER( PARTITION BY cod_elemento) tot_diff
                 ,ROW_NUMBER() OVER( PARTITION BY cod_elemento ORDER BY data_attivazione_new) num
                 ,tot
                 ,cod_elemento
                 ,id_tecnologia_new
                 ,flag_tracking_new
                 ,orientazione_new
                 ,inclinazione_new
                 ,superficie_new
                 ,data_attivazione_new
                 ,data_disattivazione_new
             FROM (
                   SELECT COUNT(*) OVER(PARTITION BY new.cod_elemento) tot
                         ,new.cod_elemento cod_elemento
                         ,new.data_attivazione data_attivazione_new
                         ,new.id_tecnologia id_tecnologia_new
                         ,new.flag_tracking flag_tracking_new
                         ,new.orientazione orientazione_new
                         ,new.inclinazione inclinazione_new
                         ,new.superficie superficie_new
                         ,to_date('01013000','ddmmyyyy') data_disattivazione_new
                         ,old.id_tecnologia id_tecnologia_old
                         ,old.flag_tracking flag_tracking_old
                         ,old.orientazione orientazione_old
                         ,old.inclinazione inclinazione_old
                         ,old.superficie superficie_old
                         ,nvl(old.prev_data_att,old.data_attivazione) data_attivazione_recalc
                         ,nvl(old.next_data_dis,old.data_disattivazione) data_disattivazione_recalc
                         ,old.data_disattivazione data_disattivazione_old
                         ,old.data_attivazione data_attivazione_old
                     FROM
                          (SELECT * FROM new_data) new
                         ,(SELECT * FROM old_data) old
                    WHERE old.cod_elemento(+) = new.cod_elemento
                      AND new.num = 1
                      AND new.data_attivazione BETWEEN NVL(old.prev_data_att(+),TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis(+),TO_DATE('01013000','ddmmyyyy'))
                      AND old.data_disattivazione(+) > new.data_attivazione
                  )
            WHERE NVL(orientazione_old,0) != NVL(orientazione_new,0)
               OR NVL(id_tecnologia_old,' ') != NVL(id_tecnologia_new,' ')
               OR NVL(flag_tracking_old,0) != NVL(flag_tracking_new,0)
               OR NVL(inclinazione_old,0) != NVL(inclinazione_new,0)
               OR NVL(superficie_old,0) != NVL(superficie_new,0)
          )
     WHERE tot = tot_diff
       and num = 1;
   /* Eseguo la Merge per spostare indietro la data_disattivazione, nel caso in cui arrivi un record di storico
      Si seleziona la data_attivazione successiva( meno 1 secondo) ; la si confronta con la data dello storico e,
      nel caso questa sia minore si memorizza al posto della data precedente ( solo nei casi con data_attivazione > della data_disattivazione )
   */
   MERGE INTO elementi_gdf_solare t
        USING
       (
        WITH new_data AS
            (
             SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY pData) num
                   ,pData data_attivazione
                   ,el.cod_elemento
                   ,sol.cg_generatore
                   ,sol.orientazione
                   ,CASE WHEN sol.flag_tracking = 'SI' THEN 1 else 0 end flag_tracking
                   ,sol.id_tecnologia
                   ,sol.inclinazione
                   ,sol.superficie
          FROM
                elem_def_solare_tmp sol
               ,elementi el
         WHERE el.cod_gest_elemento = sol.cg_generatore
             )
        , old_data AS
        (
         SELECT el.*
               ,LAG(data_attivazione) OVER (PARTITION BY cod_elemento ORDER BY data_attivazione) prev_data_Att
               ,LEAD(data_attivazione) OVER ( PARTITION BY cod_elemento ORDER BY data_attivazione) next_data_dis
           FROM
                elementi_gdf_solare el
        )
        SELECT cod_elemento
               ,data_disattivazione_new
               ,data_attivazione_prec
               ,data_attivazione_old
          FROM (
                SELECT new.cod_elemento cod_elemento
                      ,new.orientazione orientazione_new
                      ,new.id_tecnologia id_tecnologia_new
                      ,new.flag_tracking flag_tracking_new
                      ,new.inclinazione inclinazione_new
                      ,new.superficie superficie_new
                      ,new.data_attivazione data_attivazione_new
                      ,nvl(old.next_data_dis,least(new.data_attivazione,old.data_attivazione))-1/(24*60*60) data_disattivazione_new
                      ,old.orientazione orientazione_old
                      ,old.id_tecnologia id_tecnologia_old
                      ,old.flag_tracking flag_tracking_old
                      ,old.inclinazione inclinazione_old
                      ,old.superficie superficie_old
                      ,old.data_attivazione data_attivazione_old
                      ,old.data_disattivazione data_disattivazione_old
                      ,old.prev_data_att data_attivazione_prec
                      ,old.next_data_dis data_disattivazione_succ
                  FROM
                       (SELECT * FROM new_data) new
                       ,(SELECT * FROM old_data) old
                 WHERE old.cod_elemento = new.cod_elemento
                   AND new.num = 1
                   AND NVL(old.orientazione,0) = NVL(new.orientazione,0)
                   AND NVL(old.id_tecnologia,' ') = NVL(new.id_tecnologia,' ')
                   AND NVL(old.flag_tracking,0) = NVL(new.flag_tracking,0)
                   AND NVL(old.inclinazione,0) = NVL(new.inclinazione,0)
                   AND NVL(old.superficie,0) = NVL(new.superficie,0)
                   AND new.data_attivazione BETWEEN NVL(old.prev_data_att,TO_DATE('01011970','ddmmyyyy')) AND NVL(old.next_data_dis,TO_DATE('01013000','ddmmyyyy'))
                   AND (new.data_attivazione  <= old.data_attivazione or new.data_attivazione > old.data_disattivazione)
                   AND NVL(old.next_data_dis,old.prev_data_att) IS NOT NULL
               )
     ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_attivazione = NVL(s.data_attivazione_prec,s.data_attivazione_old) )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_disattivazione = s.data_disattivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   /* Eseguo la Merge per spostare indietro la data di attivazione nel caso in cui arrivi un record di storico
      selezionando la data disattivazione precedente e aggiungendo un secondo
   */
   MERGE INTO elementi_gdf_solare t
        USING
       (
        SELECT *
          FROM (
                SELECT cod_elemento
                      ,data_attivazione
                      ,CASE WHEN num = 1 THEN TO_DATE('01011970','ddmmyyyy') /*LEAST(data_attivazione,data_attivazione_new)*/ ELSE data_attivazione_recalc END data_attivazione_new
                      ,data_disattivazione_old
                  FROM (
                        SELECT el.cod_elemento
                              ,el.data_attivazione
                              ,new_data.data_attivazione_new
                              ,el.data_disattivazione data_disattivazione_old
                              ,LAG(el.data_disattivazione) OVER ( PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione)+1/(24*60*60) data_attivazione_recalc
                              ,ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY el.data_disattivazione) num
                          FROM elementi_gdf_solare el
                              ,(
                                SELECT pData data_attivazione_new
                                      ,el.cod_elemento
                                      ,ROW_NUMBER() OVER(PARTITION BY el.cod_elemento ORDER BY pData) num
                                  FROM elem_def_solare_tmp eol
                                      ,elementi el
                                 WHERE eol.cg_generatore = el.cod_gest_elemento
                                ) new_data
                         WHERE new_data.cod_elemento = el.cod_elemento
                           AND new_data.num = 1
                       )
               )
         WHERE data_attivazione != data_attivazione_new
       ) s
     ON (t.cod_elemento = s.cod_elemento AND t.data_disattivazione = s.data_disattivazione_old )
   WHEN MATCHED THEN
   UPDATE SET
          t.data_attivazione = s.data_attivazione_new
   WHEN NOT MATCHED THEN
   INSERT (cod_elemento)
   VALUES (NULL);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_DEF_SOLARE;
PROCEDURE INSERT_TRATTAMENTO_ELEMENTI (pData IN DATE) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_TRATTAMENTO_ELEMENTI';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO trattamento_elementi
         ( cod_elemento
          ,cod_tipo_elemento
          ,cod_tipo_fonte
          ,cod_tipo_rete
          ,cod_tipo_cliente
          ,organizzazione
          ,tipo_aggregazione
          ,cod_tipo_misura
         )
   WITH data_el AS
        (
         SELECT /*+ use_hash (el def tp) use_nl(new el)*/
                el.cod_elemento
               ,def.cod_tipo_elemento
               ,def.cod_tipo_fonte
               ,tp.cod_tipo_rete
               ,def.cod_tipo_cliente
               ,1 organizzazione
               ,0 tipo_aggregazione
           FROM elementi el
               ,elementi_def def
               ,tipi_elemento tp
--               ,(
--                 SELECT DISTINCT cg_produttore cg_elemento
--                   FROM produttori_tmp
--                  UNION ALL
--                 SELECT cg_generatore
--                   FROM produttori_tmp
--                ) new  -- MAGO-1298
                ,
                (select cg_elemento from  (
SELECT  cg_elemento  ,ROW_NUMBER() OVER (PARTITION BY cg_elemento ORDER BY cg_elemento) num
 FROM (
                 --SELECT DISTINCT cg_produttore cg_elemento     FROM produttori_tmp
                 select  cg_produttore cg_elemento from (SELECT cg_produttore, ROW_NUMBER() OVER (PARTITION BY cg_produttore ORDER BY cg_produttore) num   FROM produttori_tmp)  where Num = 1
                  UNION ALL
                 SELECT cg_generatore cg_elemento
                   FROM produttori_tmp
                   UNION ALL
                   select  cg_impianto from (SELECT cg_impianto, ROW_NUMBER() OVER (PARTITION BY cg_impianto ORDER BY cg_impianto) num   FROM misure_tmp)  where Num = 1
)
)where num =1 ) new
          WHERE el.cod_elemento = def.cod_elemento
            AND tp.cod_tipo_elemento = def.cod_tipo_elemento
            AND new.cg_elemento = el.cod_gest_elemento
            AND pdata BETWEEN def.data_attivazione AND def.data_disattivazione
        )
   SELECT new.*
     FROM (
           SELECT el.*
                 ,mis.cod_tipo_misura cod_tipo_misura
             FROM data_el el
                 ,(SELECT distinct cod_tipo_misura cod_tipo_misura
                     FROM misure_tmp
                  ) mis
          ) new
         , trattamento_elementi old
    WHERE new.cod_elemento = old.cod_elemento(+)
      AND new.cod_tipo_elemento = old.cod_tipo_elemento(+)
      AND new.cod_tipo_fonte = old.cod_tipo_fonte(+)
      AND new.cod_tipo_rete = old.cod_tipo_rete(+)
      AND new.cod_tipo_cliente = old.cod_tipo_cliente(+)
      AND new.organizzazione = old.organizzazione(+)
      AND new.tipo_aggregazione = old.tipo_aggregazione(+)
      AND new.cod_tipo_misura = old.cod_tipo_misura(+)
      AND old.cod_elemento is null;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_TRATTAMENTO_ELEMENTI;
PROCEDURE INSERT_MISURE (pMin IN NUMBER) AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   --EXECUTE IMMEDIATE 'TRUNCATE TABLE misure_err';
   EXECUTE IMMEDIATE 'TRUNCATE TABLE offset';
   INSERT INTO offset
   SELECT ((LEVEL) * (1 /(1440/pMin))-(1 / (1440/pMin))) AS offset_start
           ,(LEVEL) * (1 / (1440/pMin)) as offset_stop
           ,LEVEL num
     FROM DUAL
   CONNECT BY LEVEL <= (1440/pMin);
   IF pkg_mago_dgf.checkmis('MIS.NOELEM') = 1 AND pkg_mago_dgf.checkmis('MIS.NOMIS') = 1 AND pkg_mago_dgf.checkmis('MIS.DUPL') = 1 THEN
      FOR rec_data in ( SELECT TRUNC(data,'mm') data
                          FROM misure_tmp
                         WHERE data IS NOT NULL
                         GROUP BY TRUNC(data,'mm')
                         ORDER BY 1  ) LOOP
      INSERT INTO misure_acquisite
             ( cod_trattamento_elem
              ,data
              ,valore
             )
      WITH min_data AS
           (
             SELECT /*+ use_nl(offset tmp) */
                    MIN(data_val) min_data_val
                   ,cg_impianto
                   ,num
               FROM offset
                   ,
                   ( SELECT TO_DATE(TO_CHAR(data,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') data_val
                           ,cg_impianto
                           ,valore
                           ,cod_tipo_misura
                       FROM misure_tmp tmp
                      WHERE trunc(data,'mm') = rec_data.data
                   ) tmp
              WHERE tmp.data_val >= TRUNC(tmp.data_val) + offset_start AND tmp.data_val < TRUNC(tmp.data_val) + offset_stop
              GROUP BY cg_impianto , trunc(tmp.data_val), num
           )
      SELECT /*+ use_hash(mis el tr) full(mis) full(el) full(tr)*/
             tr.cod_trattamento_elem
            ,mis.min_data_val
            ,mis.valore
        FROM
            (
             SELECT
                    min_data.cg_impianto
                   ,min_data.min_data_val
                   ,val.cod_tipo_misura
                   ,val.valore
               FROM min_data
                   ,misure_tmp val
              WHERE val.cg_impianto = min_data.cg_impianto
                AND TO_DATE(TO_CHAR(data,'dd/mm/yyyy') || ora,'dd/mm/yyyy hh24:mi:ss') = min_data.min_data_val
                AND val.valore IS NOT NULL
            ) mis
           ,
           (
            SELECT /*+ use_hash (el def tp) */
                   el.cod_elemento
                  ,el.cod_gest_elemento
                  ,def.cod_tipo_elemento
                  ,def.cod_tipo_fonte
                  ,tp.cod_tipo_rete
                  ,def.cod_tipo_cliente
                  ,1 organizzazione
                  ,0 tipo_aggregazione
                  ,def.data_attivazione
                  ,def.data_disattivazione
              FROM elementi el
                  ,elementi_def def
                  ,tipi_elemento tp
             WHERE el.cod_elemento = def.cod_elemento
               AND tp.cod_tipo_elemento = def.cod_tipo_elemento
           ) el
           , trattamento_elementi tr
      WHERE mis.cg_impianto = el.cod_gest_elemento
         AND mis.min_data_val BETWEEN el.data_attivazione AND el.data_disattivazione
         AND el.cod_elemento = tr.cod_elemento
         AND el.cod_tipo_elemento = tr.cod_tipo_elemento
         AND el.cod_tipo_fonte = tr.cod_tipo_fonte
         AND el.cod_tipo_rete = tr.cod_tipo_rete
         AND nvl(el.cod_tipo_cliente,99999) = nvl(tr.cod_tipo_cliente,99999)
         --AND el.cod_tipo_cliente = tr.cod_tipo_cliente
         AND el.organizzazione = tr.organizzazione
         AND el.tipo_aggregazione = tr.tipo_aggregazione
         AND mis.cod_tipo_misura = tr.cod_tipo_misura;
      COMMIT;
         FOR rec_job IN (
         SELECT DISTINCT
         --mis.data data,
         TO_DATE(TO_CHAR(mis.data,'dd/mm/yyyy') || mis.ora ,'dd/mm/yyyy hh24:mi:ss') data,
         mis.cod_tipo_misura , tr.cod_tipo_rete, 1 stato
                             FROM misure_tmp mis
                                 ,trattamento_elementi tr
                            WHERE mis.cod_tipo_misura = tr.cod_tipo_misura
                              AND trunc(data,'mm') = rec_data.data
        UNION ALL
        SELECT DISTINCT --    MAGO-1327
                 --mis.data data,
                 TO_DATE(TO_CHAR(mis.data,'dd/mm/yyyy') || mis.ora ,'dd/mm/yyyy hh24:mi:ss') data,
                 mis.cod_tipo_misura , tr.cod_tipo_rete, 2 stato
                                     FROM misure_tmp mis
                                         ,trattamento_elementi tr
                                    WHERE mis.cod_tipo_misura = tr.cod_tipo_misura
                                      AND trunc(data,'mm') = rec_data.data
                            ORDER BY data
                         )  LOOP
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,1, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,2, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
             pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,3, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
         END LOOP;
         --PKG_Mago.StdLogInit  (NULL,'PKG_Mago_DGF.Insert_Misure',NULL,vLog);
         --PKG_Mago.StdLogAddTxt('Loaded Mese: '||NVL(TO_CHAR(rec_data.data,'mm/yyyy'),'<null>'),TRUE,vLog);
         pkg_mago_utl.inslog(proc_name,'Procedure loaded month: '||NVL(TO_CHAR(rec_data.data,'mm/yyyy'),'<null>') ,null);
         --PKG_Mago.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_INF);
      END LOOP;
         --PKG_Mago.StdLogInit  (NULL,'PKG_Mago_DGF.Insert_Misure',NULL,vLog);
         --PKG_Mago.StdLogAddTxt('End Proc',TRUE,vLog);
         --PKG_Mago.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_INF);
   END IF;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
   ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_MISURE;
PROCEDURE INSERT_REL_MISURE AS
BEGIN
proc_name := 'PKG_MAGO_DGF.INSERT_REL_MISURE';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   MERGE INTO rel_elemento_tipmis t
        USING
       (
        SELECT tr.cod_elemento
              ,tr.cod_tipo_misura
              ,NVL(old.tab_misure,0) tab_misure
              ,case when tr.cod_tipo_rete='A' then 1 else 0 end rete_at
              ,case when tr.cod_tipo_rete='M' then 1 else 0 end rete_mt
              ,case when tr.cod_tipo_rete='B' then 1 else 0 end rete_bt
          FROM trattamento_elementi tr
              ,rel_elemento_tipmis old
         WHERE tr.cod_elemento = old.cod_elemento (+)
           AND tr.cod_tipo_misura = old.cod_tipo_misura(+)
           AND tr.tipo_aggregazione = 0
        ) s
     ON (t.cod_elemento = s.cod_elemento AND t.cod_tipo_misura = s.cod_tipo_misura )
   WHEN MATCHED THEN
   UPDATE SET
          t.tab_misure = case when s.tab_misure = 0 then 1 when s.tab_misure = 2 then 3 else t.tab_misure end
   WHEN NOT MATCHED THEN
   INSERT
         ( t.cod_elemento
            ,t.cod_tipo_misura
            ,t.tab_misure
            ,t.rete_at
            ,t.rete_mt
            ,t.rete_bt)
   VALUES
         ( s.cod_elemento
            ,s.cod_tipo_misura
            ,1
            ,s.rete_at
            ,s.rete_mt
            ,s.rete_bt);
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END INSERT_REL_MISURE;
PROCEDURE insert_manutenzione(pEle IN T_ELEMAN_OBJ) AS
tipo_elemento ELEMENTI.COD_TIPO_ELEMENTO%TYPE;
BEGIN
   proc_name := 'PKG_MAGO_DGF.INSERT_MANUTENZIONE';
   SELECT cod_tipo_elemento
     INTO tipo_elemento
     FROM elementi
    WHERE cod_elemento = pEle.cod_elemento;
   IF pEle.oper = 'D' THEN
      DELETE FROM tmp_manutenzione
       WHERE cod_elemento = pEle.cod_elemento
         AND data_inizio >= pele.datafrom
         AND data_fine <= pele.datato;
   ELSE
      IF pEle.datatoold IS NOT NULL THEN
         UPDATE tmp_manutenzione
            SET percentuale = pele.percentuale
               ,data_fine = pele.datato
          WHERE cod_elemento = pele.cod_elemento
            AND data_inizio >= pele.datafrom
            AND data_fine <= pele.datatoold;
      ELSE
         INSERT INTO tmp_manutenzione
            (id_manutenzione
             ,cod_elemento
             ,tipo_elemento
             ,data_inizio
             ,data_fine
             ,percentuale
            )
         VALUES
            (manutenzione_seq.nextval
             ,pele.cod_elemento
             ,tipo_elemento
             ,pele.datafrom
             ,pele.datato
             ,pele.percentuale
            );
      END IF;
   END IF;
   COMMIT;
END INSERT_MANUTENZIONE;
PROCEDURE elabora_manutenzione AS
CURSOR c_data IS
      SELECT data_inizio,data_fine,data_fine_old
        FROM
         (
         SELECT DISTINCT man.data_inizio
              , NVL(manold.data_fine,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))) data_fine
              , man.data_fine_old data_fine_old
           FROM manutenzione man
               ,manutenzione manold
               ,tipi_elemento tip
          WHERE man.elaborato = 0
            AND man.cod_elemento = manold.cod_elemento(+)
            AND man.data_fine_old > manold.data_fine(+)
            AND man.data_fine = manold.data_inizio(+)
            AND manold.elaborato(+) = 1
            AND tip.cod_tipo_elemento = man.tipo_elemento
            AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
         )
         ORDER BY data_inizio, data_fine ;
rec_data c_data%ROWTYPE;
type tdate  IS TABLE OF DATE;
rec_pi tdate;
i NUMBER;
BEGIN
proc_name := 'PKG_MAGO_DGF.ELABORA_MANUTENZIONE';
   pkg_gerarchia_dgf.linearizza_gerarchia('AMM');
   pkg_gerarchia_dgf.linearizza_gerarchia('GEO');
     --Impostazione della nuova percentuale di misura per valori di manutenzione != 0 per quei record cancellati
   MERGE INTO misure_acquisite t
     USING (
            SELECT mis.data, mis.cod_trattamento_elem,
                   (mis.valore * 100 / NVL(man.percentuale_old,man.percentuale))  valore
              FROM misure_acquisite mis
                  ,tipi_misura tmis
                  ,trattamento_elementi el
                  ,manutenzione man
             WHERE man.cod_elemento = el.cod_elemento
               AND man.flg_deleted = 1
               AND NVL(man.percentuale_old,man.percentuale) != 0
               AND mis.cod_trattamento_elem = el.cod_trattamento_elem
               AND el.cod_tipo_misura = tmis.cod_tipo_misura
               AND tmis.flg_manutenzione = 1
               AND data >= man.data_inizio
             AND data < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))
             ) s
           ON (s.data = t.data and s.cod_trattamento_elem = t.cod_trattamento_elem)
         WHEN MATCHED THEN
       UPDATE SET
                 t.valore = s.valore
         WHEN NOT MATCHED THEN
       INSERT
             (cod_trattamento_elem)
       VALUES
             (NULL);

   FOR rec_data IN c_data LOOP
      BEGIN
         SELECT DISTINCT data_attivazione data_attivazione
           BULK COLLECT INTO rec_pi
           FROM misure_acquisite_statiche mis
               ,trattamento_elementi tr
               ,manutenzione man
          WHERE cod_tipo_misura = 'PI'
            AND tr.cod_elemento = man.cod_elemento
            AND man.elaborato = 0
            AND data_attivazione > rec_data.data_inizio;

         delete from misure_acquisite_statiche
           WHERE data_attivazione > rec_data.data_inizio
             AND data_disattivazione < GREATEST(NVL(rec_data.data_fine_old,to_date('01011970','ddmmyyyy')),NVL(rec_data.data_fine,to_date('01013000','ddmmyyyy')))
             AND cod_trattamento_elem  IN ( SELECT cod_trattamento_elem
                                              FROM trattamento_elementi tr
                                                  ,manutenzione man
                                                  ,tipi_elemento tip
                                             WHERE cod_tipo_misura = 'PI'
                                               AND tr.cod_elemento = man.cod_elemento
                                               AND man.elaborato = 0
                                               AND tip.cod_tipo_elemento = man.tipo_elemento
                                               AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                                           );
         pkg_misure.addmisurepi(rec_data.data_inizio);
      EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
         DELETE FROM misure_acquisite_statiche
          WHERE data_attivazione = rec_data.data_inizio;
          pkg_misure.addmisurepi(rec_data.data_inizio);
      END;
     IF rec_data.data_fine IS NOT NULL THEN
         BEGIN
            pkg_misure.addmisurepi(rec_data.data_fine);
         EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
            delete from misure_acquisite_statiche
             where data_attivazione = rec_data.data_fine;
            pkg_misure.addmisurepi(rec_data.data_fine);
         END;
      END IF;
         FOR i IN rec_pi.first .. rec_pi.last LOOP
             BEGIN
                pkg_misure.addmisurepi(rec_pi(i));
             EXCEPTION WHEN DUP_VAL_ON_INDEX THEN
                DELETE FROM misure_acquisite_statiche
                 WHERE data_attivazione = rec_pi(i);
              pkg_misure.addmisurepi(rec_pi(i));
             END;
        END LOOP;
      --Ripristino le misure settate a 0 nei run precedenti
      MERGE INTO misure_acquisite t
      USING (
          SELECT mis.data, mis.cod_trattamento_elem, mis.valore
            FROM misure_acquisite_manutenzione mis
                ,tipi_misura tmis
                ,trattamento_elementi el
                ,manutenzione man
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND man.elaborato = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND data >= CASE WHEN ((NVL(man.percentuale_old,100) = 0 AND man.percentuale != 0) OR ( NVL(man.percentuale_old,man.percentuale) = 0 AND man.flg_deleted = 1)) THEN man.data_inizio ELSE NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) END
             AND data < NVL(man.data_fine_old,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')))
         ) s
      ON (s.data = t.data and s.cod_trattamento_elem = t.cod_trattamento_elem)
       WHEN MATCHED THEN
     UPDATE SET
            t.valore = s.valore
       WHEN NOT MATCHED THEN
     INSERT
           (cod_trattamento_elem)
     VALUES
           (NULL);

      --Memorizzo le misure che imposter� a 0 per rollback successivi
      INSERT INTO misure_acquisite_manutenzione
                      (data,cod_trattamento_elem, valore)
          SELECT mis.data, mis.cod_trattamento_elem
                ,(mis.valore * 100 / CASE WHEN mis.data > NVL(data_fine_old,to_date('01011970','ddmmyyyy')) AND man.percentuale_old IS NULL THEN 100 ELSE NVL(man.percentuale_old,100) END )valore
            FROM misure_acquisite mis
                ,tipi_misura tmis
                ,trattamento_elementi el
                ,manutenzione man
                ,misure_acquisite_manutenzione misman
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND (NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) > NVL(man.data_fine_old,TO_DATE('01011970','ddmmyyyy')) OR man.percentuale_old IS NOT NULL )
             AND man.percentuale = 0
             AND man.elaborato = 0
             AND man.flg_deleted = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND mis.cod_trattamento_elem = misman.cod_trattamento_elem (+)
             AND mis.data = misman.data (+)
             AND misman.cod_trattamento_elem IS NULL
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND mis.data >= man.data_inizio
             AND mis.data < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'));

      --Impostazione della nuova percentuale di misura
      MERGE INTO misure_acquisite t
      USING (
          SELECT mis.data, mis.cod_trattamento_elem,
                 (mis.valore * 100 / CASE WHEN NVL(man.percentuale_old,0) = 0 OR data > man.data_fine_old THEN 100 ELSE man.percentuale_old END )
                 * (CASE WHEN data >= NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) THEN 100 ELSE man.percentuale END / 100) valore
            FROM misure_acquisite mis
                ,tipi_misura tmis
                ,trattamento_elementi el
                ,manutenzione man
           WHERE man.data_inizio = rec_data.data_inizio
             AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
             AND man.elaborato = 0
             AND man.flg_deleted = 0
             AND man.cod_elemento = el.cod_elemento
             AND mis.cod_trattamento_elem = el.cod_trattamento_elem
             AND el.cod_tipo_misura = tmis.cod_tipo_misura
             AND tmis.flg_manutenzione = 1
             AND data >= man.data_inizio
             AND data < GREATEST(NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')),NVL(man.data_fine_old,NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))))
             ) s
      ON (s.data = t.data and s.cod_trattamento_elem = t.cod_trattamento_elem)
       WHEN MATCHED THEN
     UPDATE SET
            t.valore = s.valore
       WHEN NOT MATCHED THEN
     INSERT
           (cod_trattamento_elem)
     VALUES
           (NULL);

   IF NVL(rec_data.data_fine_old,TO_DATE('01011970','ddmmyyyy')) > NVL(rec_data.data_fine,TO_DATE('01013000','ddmmyyyy')) THEN
      DELETE FROM misure_acquisite_manutenzione
       WHERE cod_trattamento_elem IN (SELECT tr.cod_trattamento_elem
                                        FROM trattamento_elementi tr
                                            ,manutenzione man
                                            ,tipi_elemento tip
                                       WHERE tr.cod_elemento = man.cod_elemento
                                         AND man.elaborato = 0
                                         AND tip.cod_tipo_elemento = man.tipo_elemento
                                         AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                                     )
         AND data >= NVL(rec_data.data_fine,TO_DATE('01013000','ddmmyyyy'))
         AND data < NVL(rec_data.data_fine_old,TO_DATE('01013000','ddmmyyyy'));
   END IF;
MERGE INTO misure_acquisite_manutenzione t
  USING
  ( SELECT tr.cod_trattamento_elem, man.data_inizio, man.data_fine, man.data_fine_old --mis.DATA
       FROM manutenzione man
           ,trattamento_elementi tr
           ,tipi_elemento tip
      WHERE tr.cod_elemento = man.cod_elemento
        AND ((man.percentuale != 0 AND NVL (man.percentuale_old, 100) = 0)
              OR (man.flg_deleted = 1 AND NVL(man.percentuale_old,man.percentuale) = 0)
            )
        AND tip.cod_tipo_elemento = man.tipo_elemento
        AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
        AND man.elaborato = 0
   ) s
    ON (t.cod_trattamento_elem = s.cod_trattamento_elem
        AND t.data >= s.data_inizio AND t.DATA < NVL (s.data_fine_old,NVL (s.data_fine,TO_DATE ('01013000', 'ddmmyyyy')))
       )
  WHEN MATCHED THEN
    UPDATE SET t.valore = 0
    DELETE WHERE 1 = 1;

     FOR rec_job IN ( SELECT DISTINCT mis.data data, tmis.cod_tipo_misura , el.cod_tipo_rete, 1 stato
                        FROM misure_acquisite mis
                            ,tipi_misura tmis
                            ,trattamento_elementi el
                            ,manutenzione man
                            ,tipi_elemento tip
                       WHERE man.data_inizio = rec_data.data_inizio
                         AND NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
                         AND man.elaborato = 0
                         AND man.cod_elemento = el.cod_elemento
                         AND tip.cod_tipo_elemento = man.tipo_elemento
                         AND CASE WHEN tip.ger_ecp = 1 THEN 0 ELSE tip.ger_ecs END = 1
                         AND mis.cod_trattamento_elem = el.cod_trattamento_elem
                         AND el.cod_tipo_misura = tmis.cod_tipo_misura
                         AND tmis.flg_manutenzione = 1
                         AND data >= man.data_inizio
                         AND data < NVL(man.data_fine,TO_DATE('01013000','ddmmyyyy'))
                       ORDER BY data
                    )  LOOP
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,1, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,2, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
                       pkg_scheduler.AddJobCalcAggregazione(NULL,rec_job.data,3, rec_job.stato,rec_job.cod_tipo_rete, rec_job.cod_tipo_misura,SYSDATE);
     END LOOP;
     UPDATE manutenzione
        SET elaborato = 1
           ,data_fine_old = NULL
           ,percentuale_old = null
      WHERE data_inizio = rec_data.data_inizio
        AND NVL(data_fine,TO_DATE('01013000','ddmmyyyy')) = rec_data.data_fine
        AND elaborato = 0;
     COMMIT;
  END LOOP;
  DELETE FROM manutenzione
   WHERE elaborato = 1
     AND flg_deleted = 1;
  COMMIT;
  pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);
  COMMIT;
END ELABORA_MANUTENZIONE;

END;
/
