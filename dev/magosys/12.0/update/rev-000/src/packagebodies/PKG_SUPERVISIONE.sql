CREATE OR REPLACE PACKAGE BODY      PKG_SUPERVISIONE AS

/* ***********************************************************************************************************
   NAME:       PKG_SUPERVISIONE
   PURPOSE:    Modulo Supervisione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1      17/11/2016  Favatella F.     Created this package.        -- Spec. Version 2.2.1
   2.2.1      11/01/2017  Favatella F.       create AddSpvCollectorMeasures e AddSpvCollectorInfos, gestione fileBlob,
                                             aggiornata procedura GetSpvDettaglioParametri  -- Spec. Version 2.2.1
   2.2.1      27/03/2017  Favatella F.      created AddSpvCollectorInfoWithPod
                                            F_getSpvDettaglioParametri
                                            F_getSpvSoglie                -- Spec. Version 2.2.1
   2.2.2    12/05/2017    Frosini     eliminate dbms_output
   2.2.3    12/05/2017    Favatella F.     MAGO-1166 modificata procedura DecrementSpvDettaglioParametri per evitare contatori negativi
   2.2.6    27/07/2017    Favatella F.     MAGO-1126 modificata procedura GetSpvSistema per ultima riga selezionabile
   2.2.7    03/08/2017    Forno S.         MAGO-1216  SPV -  notifiche mail         -- Spec. Version 2.2.7
   2.2.8    25/09/2017    Favatella F.     MAGO-1239 modificata gestione aggregazioni nella procedura GetSpvDettaglioParametri
   2.2.12   24/10/2017    Forno S.	       MAGO-1466 Date aggregazioni non congruenti con data acquisizione PAS
  2.2.13   30/11/2017    Frosini S.       MAGO-1518 modificato AddcollectorInfo per merge all.22
 -- spec 2.2.16
   2.2.16   01/03/2018    Frosini S.       MAGO-1636 creata spv_CleanCollMeas per pulizia giornaliera SPV_COLLECTOR_MEASURE
   2.3.0    23/11/2017    Favatella F.     Date aggregazioni CCP e CCQ
   2.3.1    15/12/2017    Frosini          MAGO-1463/1490 mantiene le info di un mese per caso CCP/q  e PCTP/Q all. 47-52
   2.3.5    22/10/2018    Forno		       SPV [foglie.tempoElaborazione] - conversione numero con formato esponenziale
*********************************************************************************************************** */

-- =============================================================================

FUNCTION Read_Soglia      ( p_SISTEMA        NUMBER,
                            p_CHIAVE         VARCHAR2) RETURN VARCHAR2 IS

    vResult VARCHAR2(2000);

BEGIN

    SELECT trim(s.VALORE)
    INTO vResult
    FROM spv_soglie s
    WHERE s.id_sistema = p_SISTEMA
    AND  trim(s.id_chiave) = trim(p_CHIAVE);

    RETURN vResult;

EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.Read_Soglia sist,key >'||p_SISTEMA||'< >'||p_CHIAVE||'<'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END Read_Soglia;

-- =============================================================================

FUNCTION Read_Soglia_Num  ( p_SISTEMA        NUMBER,
                            p_CHIAVE         VARCHAR2) RETURN NUMBER IS

BEGIN
    RETURN TO_NUMBER(Read_SOglia(p_SISTEMA, p_CHIAVE));
END Read_Soglia_Num;

-- =============================================================================

 PROCEDURE GetSpvSistema      (pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              ) AS
 BEGIN

    OPEN pRefCurs FOR
    WITH A AS(
      SELECT ss.id_sistema ID_SISTEMA
      , sa.id_livello id_livello
      , count(*) count_livello
      FROM spv_sistema ss
      JOIN spv_rel_sistema_dizionario srsd ON srsd.id_sistema = ss.id_sistema
      JOIN spv_allarmi sa ON sa.id_rel_sistema_dizionario = srsd.id_rel_sistema_dizionario
      GROUP BY ss.id_sistema, sa.id_livello
    ) , b AS (
      SELECT ss.*, soglie.valore, sla.id_livello
      FROM spv_sistema ss
      LEFT JOIN spv_soglie soglie  ON soglie.id_sistema = ss.id_sistema
                                  AND soglie.id_chiave  LIKE '%.supervision_enabled',
      spv_livello_allarmi sla
    )
    SELECT  b.id_sistema
          ,nvl(SUM(DECODE(b.id_livello, 2, A.count_livello)),0) YELLOW_ALARMS_NUMBER
          ,nvl(SUM(DECODE(b.id_livello, 3, A.count_livello)),0) RED_ALARMS_NUMBER
          ,b.DESCRIZIONE DESCRIZIONE
          , (
            CASE
              WHEN nvl(SUM(DECODE(b.id_livello, 3, A.count_livello)),0) >0 THEN 3
              WHEN nvl(SUM(DECODE(b.id_livello, 2, A.count_livello)),0) >0 THEN 2
              ELSE 1
            END
          )ALARM_STATE
          , sysdate LAST_UPDATE_DATE
          , '' SUPERVISION_CATEGORY_ID
          , '' SISTEMI_COINVOLTI
          , '' DIRECTION
          , 1 FREQUENCY
          ,  CASE b.valore
              WHEN 'true' THEN 1
              ELSE 0
            END ENABLED
          , b.VISIBLE VISIBLE
    FROM b
    LEFT JOIN A ON b.id_sistema = A.id_sistema AND b.id_livello = A.id_livello
    GROUP BY  b.id_sistema, b.DESCRIZIONE, b.valore, b.VISIBLE
    ORDER BY b.id_sistema;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvSistema'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetSpvSistema;




 PROCEDURE GetSpvDizionarioAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                        , pID_SISTEMA NUMBER
                              )
AS
BEGIN
 OPEN pRefCurs FOR
    SELECT  sda.id_dizionario_allarme,
            sda.descrizione
    FROM spv_dizionario_allarmi sda
    JOIN spv_rel_sistema_dizionario srsd ON srsd.id_dizionario_allarme = sda.id_dizionario_allarme
    JOIN spv_sistema ss ON ss.id_sistema = srsd.id_sistema
    WHERE ss.id_sistema = pID_SISTEMA
    ORDER BY sda.id_dizionario_allarme
    ;


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvDizionarioAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvDizionarioAllarmi;

 PROCEDURE GetSpvAllarmi      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                              , pID_SISTEMA NUMBER
--                              , pID_DIZIONARIO_ALLARME varchar2
--                              , pID_LIVELLO_ALLARME varchar2
                              )AS
BEGIN
 OPEN pRefCurs FOR
  SELECT
    sa.ID_ALLARME ID_ALLARME
			,srsd.id_dizionario_allarme ID_DIZIONARIO_ALLARME
			,sa.ID_LIVELLO ID_LIVELLO_ALLARME
			,sa.DATA_ALLARME DATA_ALLARME
			,sa.DESCRIZIONE_TIPO DESCRIZIONE_TIPO
			,sa.DESCRIZIONE_ELEMENTO DESCRIZIONE_ELEMENTO
			,sa.DESCRIZIONE_ALTRO1 DESCRIZIONE_ALTRO1
			,sa.DESCRIZIONE_ALTRO2 DESCRIZIONE_ALTRO2
			,sa.DESCRIZIONE_ALTRO3 DESCRIZIONE_ALTRO3
			,sa.INFO INFO
      ,sa.PARAMETRI_DESCRIZIONE
	FROM spv_allarmi sa
  JOIN spv_rel_sistema_dizionario srsd ON srsd.id_dizionario_allarme = sa.id_rel_sistema_dizionario
  WHERE srsd.id_sistema = pID_SISTEMA
--  and (pID_DIZIONARIO_ALLARME is null or (pID_DIZIONARIO_ALLARME is not null and srsd.id_dizionario_allarme in (
--    select regexp_substr(pID_DIZIONARIO_ALLARME,'[^,]+', 1, level) from dual
--    connect by regexp_substr(pID_DIZIONARIO_ALLARME, '[^,]+', 1, level) is not null
--  )))
--  and (pID_LIVELLO_ALLARME is null or (pID_LIVELLO_ALLARME is not null and sa.ID_LIVELLO in (
--    select regexp_substr(pID_LIVELLO_ALLARME,'[^,]+', 1, level) from dual
--    connect by regexp_substr(pID_LIVELLO_ALLARME, '[^,]+', 1, level) is not null
--  )))

  ORDER BY sa.ID_LIVELLO desc, sa.data_allarme
  ;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvAllarmi;

PROCEDURE GetSpvDettaglioParametri ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                    , pID_SISTEMA NUMBER
                              )
AS
  p_dal VARCHAR2(20);
  p_al VARCHAR2(20);
  p_inizio_elaborazione VARCHAR2(20);
  p_fine_elaborazione VARCHAR2(20);
  p_tempo_richiesto VARCHAR2(20);
  p_stato_elaborazione VARCHAR2(20);
  p_numAggregazioni VARCHAR2(20);
BEGIN


    IF (pID_SISTEMA IN (2,4,7)) THEN

--   if(pID_SISTEMA = 2)then
    SELECT
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(dal) END ) dal
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(al) END ) al
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    , CASE
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN NULL
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN 'IN CORSO'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN 'CONCLUSA'
      END  stato_elaborazione
    INTO  p_dal, p_al, p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    FROM (
    SELECT
    ss.date_from dal
    ,ss.date_to al

    , ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    FROM session_statistics ss
    WHERE PKG_MISURE.f_MisureMonitorAggr(pID_SISTEMA,ss.cod_tipo_misura) = 1
    AND ss.service_ref = (SELECT max(to_number(service_ref)) FROM session_statistics
    WHERE PKG_MISURE.f_MisureMonitorAggr(pID_SISTEMA,cod_tipo_misura) = 1 )  -- MAGO-1466
    ORDER BY ss.cod_sessione
    );

    SELECT count(*)
    INTO p_numAggregazioni
    FROM scheduled_jobs
    WHERE  PKG_MISURE.f_MisureMonitorAggr(pID_SISTEMA,cod_tipo_misura) = 1
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , NULL );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , NULL );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , NULL );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , NULL );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, NULL );




      IF(pID_SISTEMA = 7)THEN

        pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoDal' 			, p_dal , NULL);
        pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoAl' 			, p_al , NULL );

       END IF;

    END IF;

    /*
 OPEN pRefCurs FOR
   SELECT sdp.id_sistema
        , sdp.id_chiave
        , sdp.valore
        , sdp.valore_blob
   FROM spv_dettaglio_parametri sdp
   WHERE sdp.id_sistema = pID_SISTEMA;*/

OPEN pRefCurs FOR   --2.3.4
   SELECT sdp.id_sistema
        , sdp.id_chiave
        , CASE
        WHEN  sdp.id_chiave = 'foglie.tempoElaborazione' AND sdp.id_sistema = '4' THEN to_char(TO_NUMBER(REPLACE( sdp.valore, '.' ,',' )))   -- conversione numero con formato esponenziale
        ELSE sdp.valore
      END  valore
        , sdp.valore_blob
   FROM spv_dettaglio_parametri sdp
   WHERE sdp.id_sistema = pID_SISTEMA;


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvDettaglioParametri;

/*

PROCEDURE old_GetSpvDettaglioParametri ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                    , pID_SISTEMA number
                                    )
AS
  p_dal varchar2(20);
  p_al varchar2(20);
  p_inizio_elaborazione varchar2(20);
  p_fine_elaborazione varchar2(20);
  p_tempo_richiesto varchar2(20);
  p_stato_elaborazione varchar2(20);
  p_numAggregazioni varchar2(20);
BEGIN


   if(pID_SISTEMA = 2)then
    select
    --pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(dal) END ) dal
    --, pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(al) END ) al
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    --, CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0 THEN 'CONCLUSA'
    --ELSE 'IN CORSO'  END  stato_elaborazione
    , CASE
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN null
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN 'IN CORSO'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN 'CONCLUSA'
      END  stato_elaborazione
    into p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    from (
    select
    ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    from session_statistics ss
 --   where ss.cod_tipo_misura in ('PAS')
    where PKG_MISURE.f_MisureMonitorAggr(ss.cod_tipo_misura) = 1
    and ss.service_ref = (select max(to_number(service_ref)) from session_statistics
 --   where cod_tipo_misura in ('PAS'))  -- MAGO-1466
    where PKG_MISURE.f_MisureMonitorAggr(cod_tipo_misura) = 1 )  -- MAGO-1466
    order by ss.cod_sessione
    );

    select count(*)
    into p_numAggregazioni
    from scheduled_jobs
    where
       ( (pID_SISTEMA = 4) AND cod_tipo_misura in ('CC-P','CC-Q'))
    OR ( (pID_SISTEMA <> 4) AND cod_tipo_misura in ('PAS'))
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, null );


  end if;

  if(pID_SISTEMA = 4)then
    select
    --pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(dal) END ) dal
    --, pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
    --                 THEN MAX(al) END ) al
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    --, CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0 THEN 'CONCLUSA'
    --ELSE 'IN CORSO'  END  stato_elaborazione
    , CASE
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN '0'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN '1'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN '2'
      END  stato_elaborazione
    into p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    from (
    select
    ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    from session_statistics ss
    where ss.cod_tipo_misura in ('CCP', 'CCQ')
    and ss.service_ref = (select max(to_number(service_ref)) from session_statistics where cod_tipo_misura in ('CCP', 'CCQ'))  -- MAGO-1466
    order by ss.cod_sessione
    );

    select count(*)
    into p_numAggregazioni
    from scheduled_jobs
    where cod_tipo_misura in ('CCP', 'CCQ')
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, null );
  end if;

  if(pID_SISTEMA = 7)then
    select
    pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN dal IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(dal) END ) dal
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN al IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(al) END ) al
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN inizio_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(inizio_elaborazione) END ) inizio_elaborazione
    , pkg_supervisione.getTimestamp( CASE WHEN MAX(CASE WHEN fine_elaborazione IS NULL THEN 1 ELSE 0 END) = 0
                     THEN MAX(fine_elaborazione) END ) fine_elaborazione
    , CASE WHEN MAX(CASE WHEN tempo_richiesto IS NULL THEN 1 ELSE 0 END) = 0
    THEN MAX(tempo_richiesto) END  tempo_richiesto
    ,  CASE
        WHEN MAX(inizio_elaborazione) IS NULL AND MAX(fine_elaborazione) IS NULL THEN null
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NULL THEN 'IN CORSO'
        WHEN MAX(inizio_elaborazione) IS NOT NULL AND MAX(fine_elaborazione) IS NOT NULL THEN 'CONCLUSA'
      END  stato_elaborazione
    into p_dal, p_al, p_inizio_elaborazione, p_fine_elaborazione, p_tempo_richiesto, p_stato_elaborazione
    from (
    select
    ss.date_from dal
    ,ss.date_to al

    ,ss.aggreg_start_date inizio_elaborazione
    ,ss.aggreg_end_date fine_elaborazione

    ,trunc(24 * 60 * 60 * (ss.aggreg_end_date-ss.aggreg_start_date))*1000 tempo_richiesto
    from session_statistics ss
    where ss.cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1')
    and ss.service_ref = (select max(to_number(service_ref)) from session_statistics where cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1'))  -- MAGO-1466
    order by ss.cod_sessione
    )
    ;

    select count(*)
    into p_numAggregazioni
    from scheduled_jobs
    where cod_tipo_misura in ('PMP','PMC', 'PMP.P1', 'PMC.P1')
    ;

    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoDal' 			, p_dal , null);
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.periodoAl' 			, p_al , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.inizioElaborazione' 	, p_inizio_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.fineElaborazione' 		, p_fine_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.tempoElaborazione' 	, p_tempo_richiesto , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.statoElaborazione' 	, p_stato_elaborazione , null );
    pkg_supervisione.SetSpvDettaglioParametri( pID_SISTEMA, 'aggregazioni.numAggregazioni' 	, p_numAggregazioni, null );
  end if;

 OPEN pRefCurs FOR
   select sdp.id_sistema
        , sdp.id_chiave
        , sdp.valore
        , sdp.valore_blob
   from spv_dettaglio_parametri sdp
   where sdp.id_sistema = pID_SISTEMA;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END old_GetSpvDettaglioParametri;

*/

PROCEDURE AddSpvAllarmi(pID_SISTEMA NUMBER
                      , pID_DIZIONARIO_ALLARME NUMBER
											, pDATA_ALLARME DATE
											, pDESCRIZIONE_TIPO VARCHAR2
											, pDESCRIZIONE_ELEMENTO VARCHAR2
											, pDESCRIZIONE_ALTRO1 VARCHAR2
											, pDESCRIZIONE_ALTRO2 VARCHAR2
											, pDESCRIZIONE_ALTRO3 VARCHAR2
											, pID_LIVELLO NUMBER
											, pINFO VARCHAR2
                      , pPARAMETRI_DESCRIZIONE VARCHAR2

                              )
AS
  pID_REL_SISTEMA_DIZIONARIO NUMBER;
BEGIN
  NULL; --dbms_output.put_line('AddSpvAllarmi');

  SELECT srsd.id_rel_sistema_dizionario INTO pID_REL_SISTEMA_DIZIONARIO
  FROM spv_rel_sistema_dizionario srsd
  WHERE srsd.id_sistema = pID_SISTEMA
  AND srsd.id_dizionario_allarme = pID_DIZIONARIO_ALLARME
  ;

  INSERT INTO SPV_ALLARMI (ID_ALLARME,ID_REL_SISTEMA_DIZIONARIO,DATA_ALLARME,DESCRIZIONE_TIPO,DESCRIZIONE_ELEMENTO,DESCRIZIONE_ALTRO1,DESCRIZIONE_ALTRO2,DESCRIZIONE_ALTRO3,ID_LIVELLO,INFO,PARAMETRI_DESCRIZIONE)
  VALUES (SPV_ALLARMI_SEQ.NEXTVAL,pID_REL_SISTEMA_DIZIONARIO,pDATA_ALLARME,pDESCRIZIONE_TIPO,pDESCRIZIONE_ELEMENTO,pDESCRIZIONE_ALTRO1,pDESCRIZIONE_ALTRO2,pDESCRIZIONE_ALTRO3,pID_LIVELLO,pINFO,pPARAMETRI_DESCRIZIONE);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvAllarmi'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvAllarmi;

-- =============================================================================

PROCEDURE CleanSpvAllarmi(pID_SISTEMA NUMBER) AS

    vSoglia_CC  NUMBER := 0;
    vSoglia_PCT NUMBER := 0;

    c_ID_ALL_CC  CONSTANT NUMBER := 52;
    c_ID_ALL_PCT CONSTANT NUMBER := 47;

    c_SISTEMA_CC  CONSTANT NUMBER := 4;
    c_SISTEMA_PCT CONSTANT NUMBER := 8;

BEGIN

    NULL; --dbms_output.put_line('CleanSpvAllarmi');

    /*
    IF (pID_SISTEMA = c_SISTEMA_CC) THEN
        vSoglia_CC  :=  Read_Soglia_Num(pID_SISTEMA, 'loadMeasure.giorni_vita_info_CC');
    ELSIF (pID_SISTEMA = c_SISTEMA_PCT) THEN
        vSoglia_PCT :=  Read_Soglia_Num(pID_SISTEMA, 'loadForecast.giorni_vita_info_PCT');
    END IF;
*/

    DELETE
    FROM
        SPV_ALLARMI
    WHERE
        id_rel_sistema_dizionario IN
        (
            SELECT
                srsd.id_rel_sistema_dizionario
            FROM
                spv_rel_sistema_dizionario srsd
            WHERE
                srsd.id_sistema = pID_SISTEMA
        )
/*        AND
        (
            (NOT pID_SISTEMA IN (c_sistema_CC, c_sistema_PCT))
            OR (
                pID_SISTEMA = c_sistema_CC
                AND ( id_rel_sistema_dizionario <> c_ID_ALL_CC
                      OR TRUNC(data_allarme + vSoglia_CC) < TRUNC(sysdate))
            )
            OR (
                pID_SISTEMA = c_sistema_PCT
                AND ( id_rel_sistema_dizionario <> c_ID_ALL_PCT
                      OR TRUNC(data_allarme + vSoglia_PCT) < TRUNC(sysdate))
            )
        )
        */
        ;

EXCEPTION

WHEN OTHERS THEN
    ROLLBACK;
    PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||
    'Funzione PKG_SUPERVISIONE.CleanSpvAllarmi'||CHR(10)||
    DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
    PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
    RAISE;

END CleanSpvAllarmi;

-- =============================================================================

PROCEDURE SetSpvDettaglioParametri( pID_SISTEMA NUMBER
                                  , pID_CHIAVE VARCHAR2
                                  , pValore VARCHAR2
                                  , pVALORE_BLOB BLOB
                             )
AS
BEGIN
NULL; --dbms_output.put_line('SetSpvDettaglioParametri');

  UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE = pVALORE
      , VALORE_BLOB = pVALORE_BLOB
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE
      ;

  IF ( SQL%ROWCOUNT = 0 )
    THEN
        INSERT INTO SPV_DETTAGLIO_PARAMETRI (ID_SISTEMA,ID_CHIAVE,VALORE)
        VALUES (pID_SISTEMA,pID_CHIAVE,pVALORE);
  END IF;


EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.SetSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END SetSpvDettaglioParametri;

PROCEDURE DecrementSpvDettaglioParametri( pID_SISTEMA NUMBER
                                        , pID_CHIAVE VARCHAR2
                                        , pID_CHIAVE_ELENCO VARCHAR2
                                        , pCOD_GESTIONALE VARCHAR2
                                        )
AS
  blob_contains NUMBER;
BEGIN
NULL; --dbms_output.put_line('DecrementSpvDettaglioParametri');

  BEGIN
   -- verifico se il codGest � presente
     SELECT
      CASE WHEN valore_blob IS NULL
      THEN 0
      ELSE dbms_lob.instr(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE)
      END
     INTO blob_contains
    FROM SPV_DETTAGLIO_PARAMETRI
    WHERE ID_SISTEMA = pID_SISTEMA
    AND ID_CHIAVE = pID_CHIAVE_ELENCO
     ;
    -- se il codGest � presente
    IF blob_contains > 0 THEN

      -- decremento il contatore
      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE = VALORE-1
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE
     ;

      -- pulisco la lista
      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE||','))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
     ;

      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),','||pCOD_GESTIONALE))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
    ;

      UPDATE SPV_DETTAGLIO_PARAMETRI
      SET VALORE_BLOB = pkg_supervisione.convert_to_blob(
                           REPLACE(pkg_supervisione.convert_to_clob(valore_blob),pCOD_GESTIONALE))
      WHERE ID_SISTEMA = pID_SISTEMA
      AND ID_CHIAVE = pID_CHIAVE_ELENCO
    ;
    END IF;



  EXCEPTION
        WHEN OTHERS
        THEN NULL; NULL; --dbms_output.put_line(SQLCODE);
  END;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.DecrementSpvDettaglioParametri'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END DecrementSpvDettaglioParametri;

PROCEDURE GetSpvCollectorInfo      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                   , pID_SISTEMA        NUMBER

                              )
AS
BEGIN
  NULL; --dbms_output.put_line('GetSpvCollectorInfo');
  OPEN pRefCurs FOR
    SELECT sci.ID_INFO
    ,sci.COLLECTOR_NAME
    ,sci.DATA_INSERIMENTO
    ,srsd.ID_SISTEMA
    ,srsd.ID_DIZIONARIO_ALLARME
    ,sci.SUPPLIER_NAME
    ,sci.COD_GEST_ELEMENTO
    ,sci.COD_TIPO_MISURA
    ,sci.COD_TIPO_FONTE
    ,sci.VALORE
    ,sci.INFORMAZIONI
    FROM spv_collector_info sci
    JOIN spv_rel_sistema_dizionario srsd ON srsd.id_rel_sistema_dizionario = sci.id_rel_sistema_dizionario
    WHERE srsd.id_sistema = pID_SISTEMA
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvCollectorInfo;

-- =============================================================================

PROCEDURE CleanSpvCollectorInfo(pID_SISTEMA NUMBER, pID_DIZIONARIO_ALLARME NUMBER DEFAULT -1)
AS

    vSoglia_CC  NUMBER := 0;
    vSoglia_PCT NUMBER := 0;

--   c_ID_ALL_CC  CONSTANT NUMBER := 52;
--    c_ID_ALL_PCT CONSTANT NUMBER := 47;



    c_SISTEMA_CC  CONSTANT NUMBER := 4;
    c_SISTEMA_PCT CONSTANT NUMBER := 8;


BEGIN
    NULL; --dbms_output.put_line('CleanSpvAllarmi');

/*
    IF (pID_SISTEMA = c_SISTEMA_CC) THEN
        vSoglia_CC  :=  Read_Soglia_Num(pID_SISTEMA, 'loadMeasure.giorni_vita_info_CC');
    ELSIF (pID_SISTEMA = c_SISTEMA_PCT) THEN
        vSoglia_PCT :=  Read_Soglia_Num(pID_SISTEMA, 'loadForecast.giorni_vita_info_PCT');
    END IF;


    DELETE
    FROM
        SPV_COLLECTOR_INFO
    WHERE
        id_rel_sistema_dizionario IN
        (
            SELECT
                srsd.id_rel_sistema_dizionario
            FROM
                spv_rel_sistema_dizionario srsd
            WHERE
                srsd.id_sistema = pID_SISTEMA
        )
        AND
        (
            (NOT pID_SISTEMA IN (c_sistema_CC, c_sistema_PCT))
            OR (
                pID_SISTEMA = c_sistema_CC
                AND ( NOT id_rel_sistema_dizionario IN (52)
                      OR TRUNC(data_inserimento + vSoglia_CC) < TRUNC(sysdate))
            )
            OR (
                pID_SISTEMA = c_sistema_PCT
                AND ( NOT id_rel_sistema_dizionario IN (48,49)
                      OR TRUNC(data_inserimento + vSoglia_PCT) < TRUNC(sysdate))
            )
        );
*/

    DELETE
    FROM
        SPV_COLLECTOR_INFO
    WHERE
        id_rel_sistema_dizionario IN
        (
            SELECT
                srsd.id_rel_sistema_dizionario
            FROM
                spv_rel_sistema_dizionario srsd
            WHERE
                srsd.id_sistema = pID_SISTEMA
        )
    AND (
          (pID_DIZIONARIO_ALLARME=-1
            AND NOT id_rel_sistema_dizionario IN (48,49,52))
        OR
         (pID_DIZIONARIO_ALLARME<>-1
            AND id_rel_sistema_dizionario=pID_DIZIONARIO_ALLARME)
        );

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.CleanSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END CleanSpvCollectorInfo;

-- =============================================================================

PROCEDURE AddSpvCollectorInfo(	pCOLLECTOR_NAME				        VARCHAR2
                              , pDATA_INSERIMENTO				      DATE
                              , pID_SISTEMA                   number
                              , pID_DIZIONARIO_ALLARME        number
                              , pSUPPLIER_NAME					      VARCHAR2
                              , pCOD_GEST_ELEMENTO			      VARCHAR2
                              , pCOD_TIPO_MISURA				      VARCHAR2
                              , pCOD_TIPO_FONTE				        VARCHAR2
                              , pVALORE						            NUMBER
                              , pINFORMAZIONI					        VARCHAR2
                              )
AS
  pID_REL_SISTEMA_DIZIONARIO number;
BEGIN
--  null; --dbms_output.put_line('AddSpvCollectorInfo');

  select srsd.id_rel_sistema_dizionario into pID_REL_SISTEMA_DIZIONARIO from spv_rel_sistema_dizionario srsd
  --join spv_sistema ss on ss.id_sistema = srsd.id_sistema
  --join spv_dizionario_allarmi sda on sda.id_dizionario_allarme = srsd.id_dizionario_allarme
  where srsd.id_sistema = pID_SISTEMA
  and srsd.id_dizionario_allarme = pID_DIZIONARIO_ALLARME
  ;

    IF pID_REL_SISTEMA_DIZIONARIO <> 22 THEN

      Insert into SPV_COLLECTOR_INFO (ID_INFO,COLLECTOR_NAME,DATA_INSERIMENTO,ID_REL_SISTEMA_DIZIONARIO,SUPPLIER_NAME,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE,INFORMAZIONI)
      values (SPV_COLLECTOR_INFO_SEQ.nextval,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pID_REL_SISTEMA_DIZIONARIO,pSUPPLIER_NAME,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE,pINFORMAZIONI);

    ELSe

    MERGE INTO SPV_COLLECTOR_INFO s
    USING
    (SELECT
    pCOLLECTOR_NAME COLLECTOR_NAME,
    pDATA_INSERIMENTO DATA_INSERIMENTO,
    pID_REL_SISTEMA_DIZIONARIO ID_REL_SISTEMA_DIZIONARIO,
    pSUPPLIER_NAME SUPPLIER_NAME,
    pCOD_GEST_ELEMENTO COD_GEST_ELEMENTO,
    pCOD_TIPO_MISURA COD_TIPO_MISURA,
    pCOD_TIPO_FONTE COD_TIPO_FONTE,
    pVALORE VALORE,
    pINFORMAZIONI INFORMAZIONI
    FROM DUAL) t
     ON (s.COLLECTOR_NAME = t.COLLECTOR_NAME
     and s.ID_REL_SISTEMA_DIZIONARIO = t.ID_REL_SISTEMA_DIZIONARIO
     and s.COD_GEST_ELEMENTO = t.COD_GEST_ELEMENTO
     and s.INFORMAZIONI=t.INFORMAZIONI
     and trunc(s.data_inserimento)=trunc(t.data_inserimento))
    WHEN MATCHED THEN
     UPDATE SET
    --s.DATA_INSERIMENTO=case when s.DATA_INSERIMENTO > t.data_inserimento THEN s.DATA_INSERIMENTO ELSE t.data_inserimento END,
    s.SUPPLIER_NAME=t.SUPPLIER_NAME,
    s.COD_TIPO_MISURA=t.COD_TIPO_MISURA,
    s.COD_TIPO_FONTE=t.COD_TIPO_FONTE,
    s.VALORE=t.VALORE
      WHEN NOT MATCHED THEN
      Insert  (
    ID_INFO,COLLECTOR_NAME,DATA_INSERIMENTO,ID_REL_SISTEMA_DIZIONARIO,SUPPLIER_NAME,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE,INFORMAZIONI)
      values (SPV_COLLECTOR_INFO_SEQ.nextval,
    t.COLLECTOR_NAME,t.DATA_INSERIMENTO,t.ID_REL_SISTEMA_DIZIONARIO,t.SUPPLIER_NAME,t.COD_GEST_ELEMENTO,t.COD_TIPO_MISURA,t.COD_TIPO_FONTE,t.VALORE,t.INFORMAZIONI);

 END IF;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorInfo;


PROCEDURE AddSpvCollectorInfoWithPod(	pCOLLECTOR_NAME				        VARCHAR2
                              , pDATA_INSERIMENTO				      DATE
                              , pID_SISTEMA                   NUMBER
                              , pID_DIZIONARIO_ALLARME        NUMBER
                              , pSUPPLIER_NAME					      VARCHAR2
                              , pCOD_POD			                VARCHAR2
                              , pCOD_TIPO_MISURA				      VARCHAR2
                              , pCOD_TIPO_FONTE				        VARCHAR2
                              , pVALORE						            NUMBER
                              , pINFORMAZIONI					        VARCHAR2
                             )
AS
  pID_REL_SISTEMA_DIZIONARIO NUMBER;
  pGest VARCHAR2(100);
  pResult NUMBER;
BEGIN
  NULL; --dbms_output.put_line('AddSpvCollectorInfoWithPod');

  pResult := pkg_elementi.f_GetGestFromPod(pCOD_POD,pGest);

  AddSpvCollectorInfo(	pCOLLECTOR_NAME, pDATA_INSERIMENTO, pID_SISTEMA, pID_DIZIONARIO_ALLARME, pSUPPLIER_NAME, pGest, pCOD_TIPO_MISURA, pCOD_TIPO_FONTE, pVALORE, pINFORMAZIONI);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfoWithPod'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorInfoWithPod;

PROCEDURE AddSpvCollectorInfos       (p_Array    IN T_SPVCOLLECTORINFO_ARRAY)
AS
  pID_REL_SISTEMA_DIZIONARIO NUMBER;
BEGIN
	NULL; --dbms_output.put_line('AddSpvCollectorInfos');
    IF p_Array.FIRST IS NULL THEN
      NULL; --dbms_output.put_line('AddSpvCollectorInfos: SpvCollectorInfos non trovate');
      RETURN;
    END IF;


    FOR i IN p_Array.FIRST .. p_Array.LAST LOOP
      SELECT srsd.id_rel_sistema_dizionario INTO pID_REL_SISTEMA_DIZIONARIO FROM spv_rel_sistema_dizionario srsd
      --join spv_sistema ss on ss.id_sistema = srsd.id_sistema
      --join spv_dizionario_allarmi sda on sda.id_dizionario_allarme = srsd.id_dizionario_allarme
      WHERE srsd.id_sistema = p_Array(i).ID_SISTEMA
      AND srsd.id_dizionario_allarme = p_Array(i).ID_DIZIONARIO_ALLARME
      ;

      INSERT INTO SPV_COLLECTOR_INFO (ID_INFO,COLLECTOR_NAME,DATA_INSERIMENTO,ID_REL_SISTEMA_DIZIONARIO,SUPPLIER_NAME,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE,INFORMAZIONI)
      VALUES (SPV_COLLECTOR_INFO_SEQ.NEXTVAL,p_Array(i).COLLECTOR_NAME,p_Array(i).DATA_INSERIMENTO,pID_REL_SISTEMA_DIZIONARIO,p_Array(i).SUPPLIER_NAME,p_Array(i).COD_GEST_ELEMENTO,p_Array(i).COD_TIPO_MISURA,p_Array(i).COD_TIPO_FONTE,p_Array(i).VALORE,p_Array(i).INFORMAZIONI);

    END LOOP;
	NULL; --dbms_output.put_line('AddSpvCollectorInfos: SpvCollectorInfos inserite');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorInfos'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddSpvCollectorInfos;

PROCEDURE GetSpvSoglie      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                            , pID_SISTEMA        NUMBER)
AS
BEGIN
  NULL; --dbms_output.put_line('GetSpvSoglie');
  OPEN pRefCurs FOR
    SELECT s.ID_SOGLIA
    ,s.ID_SISTEMA
    ,s.ID_CHIAVE
    ,s.VALORE
    FROM spv_soglie s
    WHERE s.id_sistema = pID_SISTEMA
    ORDER BY s.id_chiave
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvSoglie'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvSoglie;

PROCEDURE AddSpvSoglie       (pMisureMeteo    IN T_SPVSOGLIA_ARRAY)
AS

BEGIN
	NULL; --dbms_output.put_line('AddSpvSoglie');
    IF pMisureMeteo.FIRST IS NULL THEN
      NULL; --dbms_output.put_line('AddSpvSoglie: soglie non trovate');
      RETURN;
    END IF;


    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP
	  UPDATE SPV_SOGLIE
		  SET VALORE = pMisureMeteo(i).VALORE
		  WHERE ID_SOGLIA = pMisureMeteo(i).ID_SOGLIA
		  AND ID_SISTEMA = pMisureMeteo(i).ID_SISTEMA
		  AND ID_CHIAVE = pMisureMeteo(i).ID_CHIAVE
		  ;
    END LOOP;
	NULL; --dbms_output.put_line('AddSpvSoglie: soglie inserite');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvSoglie'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END AddSpvSoglie;

PROCEDURE GetSpvCollectorMeasure      ( pRefCurs          OUT PKG_UtlGlb.t_query_cur
                                      , pCOLLECTOR_NAME		VARCHAR2
                                      , pSTART_DATE	DATE
                                       )
AS
BEGIN
  NULL; --dbms_output.put_line('GetSpvCollectorMeasure');
  OPEN pRefCurs FOR
    SELECT M.ID_MEASURE
    ,M.COLLECTOR_NAME
    ,M.DATA_INSERIMENTO
    ,M.DATA_MISURA
    ,M.COD_GEST_ELEMENTO
    ,M.COD_TIPO_MISURA
    ,M.COD_TIPO_FONTE
    ,M.VALORE
    FROM spv_collector_measure M
    WHERE M.collector_name = pCOLLECTOR_NAME
    AND M.data_misura >= pSTART_DATE
     ORDER BY M.data_misura
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetSpvCollectorMeasure'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetSpvCollectorMeasure;

PROCEDURE AddSpvCollectorMeasure(	pCOLLECTOR_NAME				        VARCHAR2
                                , pDATA_INSERIMENTO				      DATE
                                , pDATA_MISURA    				      DATE
                                , pCOD_GEST_ELEMENTO			      VARCHAR2
                                , pCOD_TIPO_MISURA				      VARCHAR2
                                , pCOD_TIPO_FONTE				        VARCHAR2
                                , pVALORE						            NUMBER
                               )
AS
BEGIN
  NULL; --dbms_output.put_line('AddSpvCollectorMeasure');



--  UPDATE SPV_COLLECTOR_MEASURE
--      SET DATA_INSERIMENTO = pDATA_INSERIMENTO,
--          VALORE = pVALORE
--      WHERE COLLECTOR_NAME = pCOLLECTOR_NAME
--      AND DATA_MISURA = pDATA_MISURA
--      AND COD_GEST_ELEMENTO = pCOD_GEST_ELEMENTO
--      AND COD_TIPO_MISURA = pCOD_TIPO_MISURA
--      AND COD_TIPO_FONTE = pCOD_TIPO_FONTE
--      ;
--
--  IF ( sql%rowcount = 0 )
--    THEN
--        Insert into SPV_COLLECTOR_MEASURE (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
--        values (SPV_COLLECTOR_MEASURE_SEQ.nextval,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pDATA_MISURA,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE);
--
--  END IF;

  MERGE INTO SPV_COLLECTOR_MEASURE T
  USING (SELECT pDATA_INSERIMENTO DATA_INSERIMENTO ,pVALORE VALORE,  pDATA_MISURA DATA_MISURA ,
                       pCOLLECTOR_NAME  COLLECTOR_NAME,pCOD_GEST_ELEMENTO  COD_GEST_ELEMENTO ,
                       pCOD_TIPO_MISURA COD_TIPO_MISURA, pCOD_TIPO_FONTE COD_TIPO_FONTE
                      FROM dual
          )  ORIG
  ON (           T.COLLECTOR_NAME = ORIG.COLLECTOR_NAME
                    AND T.DATA_MISURA = ORIG.DATA_MISURA
                    AND T.COD_GEST_ELEMENTO = ORIG.COD_GEST_ELEMENTO
                    AND T.COD_TIPO_MISURA = ORIG.COD_TIPO_MISURA
                    AND T.COD_TIPO_FONTE = ORIG.COD_TIPO_FONTE
                  )
  WHEN MATCHED THEN
                    UPDATE SET DATA_INSERIMENTO = pDATA_INSERIMENTO,  VALORE = pVALORE
  WHEN NOT MATCHED THEN
                    INSERT  (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
                    VALUES (SPV_COLLECTOR_MEASURE_SEQ.NEXTVAL,pCOLLECTOR_NAME,pDATA_INSERIMENTO,pDATA_MISURA,pCOD_GEST_ELEMENTO,pCOD_TIPO_MISURA,pCOD_TIPO_FONTE,pVALORE);



EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorMeasure'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorMeasure;

PROCEDURE AddSpvCollectorMeasures(	p_Array    IN T_SPVCOLLECTORMEASURE_ARRAY)
AS
  ptmp VARCHAR2(10);
BEGIN
	NULL; --dbms_output.put_line('AddSpvCollectorMeasures');
    IF p_Array.FIRST IS NULL THEN
      NULL; --dbms_output.put_line('AddSpvCollectorMeasures: SpvCollectorMeasures non trovate');
      RETURN;
    END IF;


    FOR i IN p_Array.FIRST .. p_Array.LAST LOOP

      UPDATE SPV_COLLECTOR_MEASURE
      SET DATA_INSERIMENTO = p_Array(i).DATA_INSERIMENTO,
          VALORE = p_Array(i).VALORE
      WHERE COLLECTOR_NAME = p_Array(i).COLLECTOR_NAME
      AND DATA_MISURA = p_Array(i).DATA_MISURA
      AND COD_GEST_ELEMENTO = p_Array(i).COD_GEST_ELEMENTO
      AND COD_TIPO_MISURA = p_Array(i).COD_TIPO_MISURA
      AND COD_TIPO_FONTE = p_Array(i).COD_TIPO_FONTE
      ;

      IF ( SQL%ROWCOUNT = 0 )
        THEN
            INSERT INTO SPV_COLLECTOR_MEASURE (ID_MEASURE,COLLECTOR_NAME,DATA_INSERIMENTO,DATA_MISURA,COD_GEST_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,VALORE)
            VALUES (SPV_COLLECTOR_MEASURE_SEQ.NEXTVAL
            ,p_Array(i).COLLECTOR_NAME
            ,p_Array(i).DATA_INSERIMENTO
            ,p_Array(i).DATA_MISURA
            ,p_Array(i).COD_GEST_ELEMENTO
            ,p_Array(i).COD_TIPO_MISURA
            ,p_Array(i).COD_TIPO_FONTE
            ,p_Array(i).VALORE
            );

      END IF;

--select '1' into ptmp from dual;
    END LOOP;
	NULL; --dbms_output.put_line('AddSpvCollectorMeasures: SpvCollectorMeasures inserite');
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvCollectorMeasures'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END AddSpvCollectorMeasures;

PROCEDURE getMeteoJobStaticConfig      ( pRefCurs           OUT PKG_UtlGlb.t_query_cur
                                    , pKEY               VARCHAR2)
AS
BEGIN
  NULL; --dbms_output.put_line('getMeteoJobStaticConfig');
  OPEN pRefCurs FOR
    SELECT *
    FROM meteo_job_static_config
    WHERE KEY = pKEY--'MFM.enabled.suppliers'
    ;
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.getMeteoJobStaticConfig'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END getMeteoJobStaticConfig;

FUNCTION getTimestamp(pDate IN DATE)
   RETURN NUMBER
   IS p_unix_time NUMBER;
   BEGIN
SELECT trunc(extract(DAY    FROM (pDate - TIMESTAMP '1970-01-01 00:00:00')) * 86400000
     + extract(HOUR   FROM (pDate - TIMESTAMP '1970-01-01 00:00:00')) * 3600000
     + extract(MINUTE FROM (pDate - TIMESTAMP '1970-01-01 00:00:00')) * 60000
     + extract(SECOND FROM (pDate - TIMESTAMP '1970-01-01 00:00:00')) * 1000) unix_time

     INTO p_unix_time
FROM dual;

      RETURN(p_unix_time);
    END;

FUNCTION convert_to_clob(l_blob BLOB) RETURN CLOB IS
     l_clob         CLOB;
     l_dest_offset  NUMBER := 1;
     l_src_offset   NUMBER := 1;
     l_lang_context NUMBER := dbms_lob.default_lang_ctx;
     l_warning      NUMBER;
  BEGIN
     dbms_lob.createtemporary(l_clob, TRUE);
     dbms_lob.converttoclob(dest_lob     => l_clob,
                            src_blob     => l_blob,
                            amount       => dbms_lob.lobmaxsize,
                            dest_offset  => l_dest_offset,
                            src_offset   => l_src_offset,
                            blob_csid    => nls_charset_id('AL32UTF8'),
                            lang_context => l_lang_context,
                            warning      => l_warning);
     RETURN l_clob;
  END convert_to_clob;

FUNCTION convert_to_blob(l_clob CLOB) RETURN BLOB IS
   l_blob         BLOB;
   l_dest_offset  NUMBER := 1;
   l_src_offset   NUMBER := 1;
   l_lang_context NUMBER := dbms_lob.default_lang_ctx;
   l_warning      NUMBER;
BEGIN
   dbms_lob.createtemporary(l_blob, TRUE);
   dbms_lob.converttoblob(dest_lob     => l_blob,
                          src_clob     => l_clob,
                          amount       => dbms_lob.lobmaxsize,
                          dest_offset  => l_dest_offset,
                          src_offset   => l_src_offset,
                          blob_csid    => nls_charset_id('AL32UTF8'),
                          lang_context => l_lang_context,
                          warning      => l_warning);
   RETURN l_blob;
END convert_to_blob;

	-- Restituisce il campo valore della tabella SPV_DETTAGLIO_PARAMETRI
	FUNCTION F_getSpvDettaglioParametri ( PID_SISTEMA IN spv_dettaglio_parametri.ID_SISTEMA%TYPE
									   ,pID_CHIAVE IN spv_dettaglio_parametri.ID_CHIAVE%TYPE
									                                )
	RETURN spv_dettaglio_parametri.VALORE%TYPE
	IS
	P_VALORE spv_dettaglio_parametri.VALORE%TYPE;
	BEGIN

	  P_VALORE := 'XXX';
	  BEGIN
		 SELECT sdp.valore INTO P_VALORE
		 --sdp.id_sistema, sdp.id_chiave, sdp.valore , sdp.valore_blob
	   FROM spv_dettaglio_parametri sdp
	   WHERE sdp.id_sistema = PID_SISTEMA
	   AND sdp.id_chiave = pID_CHIAVE
       ;

		EXCEPTION
		WHEN OTHERS THEN
			NULL;NULL; --dbms_output.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	  END;
	RETURN P_VALORE;
	END F_getSpvDettaglioParametri;

	-- Restituisce il campo valore della tabella SPV_SOGLIE
	FUNCTION F_getSpvSoglie ( PID_SISTEMA IN spv_soglie.ID_SISTEMA%TYPE
							 ,PID_CHIAVE IN spv_soglie.ID_CHIAVE%TYPE
							)
	RETURN spv_soglie.VALORE%TYPE
	IS
	P_VALORE spv_soglie.VALORE%TYPE;
	BEGIN
	P_VALORE := '-1';
	BEGIN
	  SELECT s.VALORE INTO P_VALORE
	  --s.ID_SOGLIA,s.ID_SISTEMA,s.ID_CHIAVE,s.VALORE
	FROM spv_soglie s
	WHERE s.id_sistema = PID_SISTEMA
	AND s.id_chiave = PID_CHIAVE
	;
	EXCEPTION
	WHEN OTHERS THEN

		 NULL;NULL; --dbms_output.PUT_LINE('The error code is ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
	END;
	RETURN P_VALORE;

	END F_getSpvSoglie;

-- NOTIFICHE MAIL 2.2.7
    PROCEDURE  AddSpvconfignotifica(pFormato IN CHAR ,pDestinatari IN VARCHAR2  ,pNome IN VARCHAR2  ,pOggetto IN VARCHAR2   , pTesto IN VARCHAR2 , pNotifiche IN T_SISLIV_ARRAY)
    AS
    IdNextConfigNotif INTEGER;
    BEGIN

     SELECT SPV_CONFIG_NOTIF_SEQ.NEXTVAL INTO IdNextConfigNotif FROM dual;

    -- SPV_CONFIG_NOTIFICA
    INSERT INTO SPV_CONFIG_NOTIFICA  (ID_SPVCONFIGNOTIFICA, DATA_CREAZIONE, NOME, TIPO_ALLEGATO, OGGETTO_MAIL, TESTO_MAIL, MAIL_LIST)
    VALUES (IdNextConfigNotif, sysdate, pNome, pFormato, pOggetto, pTesto, pDestinatari);


    -- SPV_CONFIG_NOTIFICA_ALARMLIST
    IF pNotifiche.FIRST IS NULL THEN
          RETURN;
    END IF;

    FOR i IN pNotifiche.FIRST .. pNotifiche.LAST LOOP
        INSERT INTO SPV_CONFIG_NOTIFICA_ALARMLIST (ID_SPVCONFIGNOTIFICA,  ID_SISTEMA,  ID_LIVELLO)
        VALUES (IdNextConfigNotif,   pNotifiche(i).ID_SISTEMA, pNotifiche(i).ID_LIVELLO);
    END LOOP;


    EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.AddSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END AddSpvconfignotifica;


    PROCEDURE GetListSpvconfignotifica(pRefCurs OUT PKG_UtlGlb.t_query_cur)
    AS
        sql_query VARCHAR2(3000) := 'select nome, ID_SPVCONFIGNOTIFICA , OGGETTO_MAIL, TESTO_MAIL , MAIL_LIST,TIPO_ALLEGATO ';

    BEGIN
        FOR x IN (SELECT DISTINCT ID_SISTEMA FROM SPV_SISTEMA) --  where visible =1) per estrarre solo i sistemi abilitati
        LOOP
            sql_query := sql_query ||
              ' , sum(case when ID_SISTEMA = '''||x.ID_SISTEMA||''' then id_livello else 0 end) as '||  'spv_sistema'|| x.ID_SISTEMA;--REPLACE(REPLACE(x.DESCRIZIONE,' ', '_'),'+','_') ;

               -- dbms_output.put_line(sql_query);
        END LOOP;

        sql_query := sql_query || ' from (SELECT NOME, SPV_SISTEMA.ID_SISTEMA,  SPV_CONFIG_NOTIFICA_ALARMLIST.ID_LIVELLO,  SPV_CONFIG_NOTIFICA.ID_SPVCONFIGNOTIFICA, SPV_CONFIG_NOTIFICA.OGGETTO_MAIL,SPV_CONFIG_NOTIFICA.TESTO_MAIL,SPV_CONFIG_NOTIFICA.MAIL_LIST,SPV_CONFIG_NOTIFICA.TIPO_ALLEGATO
          FROM SPV_CONFIG_NOTIFICA
               INNER JOIN SPV_CONFIG_NOTIFICA_ALARMLIST
                  ON (SPV_CONFIG_NOTIFICA.ID_SPVCONFIGNOTIFICA =
                         SPV_CONFIG_NOTIFICA_ALARMLIST.ID_SPVCONFIGNOTIFICA)
               INNER JOIN SPV_SISTEMA
                  ON (SPV_SISTEMA.ID_SISTEMA =
                         SPV_CONFIG_NOTIFICA_ALARMLIST.ID_SISTEMA)
         WHERE VISIBLE = 1) group by nome, ID_SPVCONFIGNOTIFICA, OGGETTO_MAIL, TESTO_MAIL , MAIL_LIST,TIPO_ALLEGATO';

        OPEN pRefCurs FOR sql_query;
     EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.GetListSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END GetListSpvconfignotifica;

    PROCEDURE DeleteSpvconfignotifica(pid_confignotifica IN  NUMBER)
    AS
    BEGIN

    DELETE FROM SPV_CONFIG_NOTIFICA_ALARMLIST T
    WHERE T.ID_SPVCONFIGNOTIFICA = pid_confignotifica;

    DELETE FROM SPV_CONFIG_NOTIFICA T
    WHERE T.ID_SPVCONFIGNOTIFICA = pid_confignotifica;


    EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_SUPERVISIONE.DeleteSpvconfignotifica'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
    END DeleteSpvconfignotifica;

/*============================================================================*/

PROCEDURE sp_CleanCollMeas IS

    vSogliaGME NUMBER :=  PKG_SUPERVISIONE.F_getSpvSoglie ( 2, 27) + 24;

    vSogliaPCT NUMBER :=  PKG_SUPERVISIONE.F_getSpvSoglie ( 8, 67) + 1;
    vSogliaCC  NUMBER :=  PKG_SUPERVISIONE.F_getSpvSoglie ( 4, 66) + 1;

BEGIN

    DELETE FROM SPV_COLLECTOR_MEASURE
    WHERE
    (COLLECTOR_NAME='SMILE_GME' AND (SYSDATE - DATA_MISURA)*24 > vSogliaGME)
    OR
    (COD_TIPO_MISURA IN ('PCT-P','PCT-Q') AND SYSDATE - DATA_MISURA > vSogliaPCT)
    OR
    (COD_TIPO_MISURA IN ('CC-P','CC-Q')  AND SYSDATE - DATA_MISURA > vSogliaCC);

END;


END PKG_SUPERVISIONE;
/
