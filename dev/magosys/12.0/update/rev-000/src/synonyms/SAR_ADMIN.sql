CREATE OR REPLACE SYNONYM sar_cft FOR sar_admin.cft
/
CREATE OR REPLACE SYNONYM sar_comuni FOR sar_admin.comuni
/
CREATE OR REPLACE SYNONYM sar_esercizi FOR sar_admin.esercizi
/
CREATE OR REPLACE SYNONYM sar_esercizi_abilitati FOR sar_admin.esercizi_abilitati
/
CREATE OR REPLACE SYNONYM sar_macro_aree FOR sar_admin.macro_aree
/
CREATE OR REPLACE SYNONYM sar_province FOR sar_admin.province
/
CREATE OR REPLACE SYNONYM sar_regioni FOR sar_admin.regioni
/
CREATE OR REPLACE SYNONYM sar_rel_cft_comuni FOR sar_admin.rel_cft_comuni
/
CREATE OR REPLACE SYNONYM sar_retesar_esercizi FOR sar_admin.retesar_esercizi
/
CREATE OR REPLACE SYNONYM sar_pkg_calendario FOR sar_admin.sar_pkg_calendario
/
CREATE OR REPLACE SYNONYM sar_unita_territoriali FOR sar_admin.unita_territoriali
/
CREATE OR REPLACE SYNONYM v_operatori FOR sar_admin.v_operatori_mago
/
CREATE OR REPLACE SYNONYM sar_zone FOR sar_admin.zone
/
