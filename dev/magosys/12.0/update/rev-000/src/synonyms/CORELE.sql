CREATE OR REPLACE SYNONYM corele_avvolgimenti_sa FOR corele.avvolgimenti_sa
/
CREATE OR REPLACE SYNONYM corele_avvolgimenti_sn FOR corele.avvolgimenti_sn
/
CREATE OR REPLACE SYNONYM corele_cavi_sa FOR corele.cavi_sa
/
CREATE OR REPLACE SYNONYM corele_cavi_sn FOR corele.cavi_sn
/
CREATE OR REPLACE SYNONYM corele_clientiat_sa FOR corele.clientiat_sa
/
CREATE OR REPLACE SYNONYM corele_clientiat_sn FOR corele.clientiat_sn
/
CREATE OR REPLACE SYNONYM corele_clientimt_sa FOR corele.clientimt_sa
/
CREATE OR REPLACE SYNONYM corele_clientimt_sn FOR corele.clientimt_sn
/
CREATE OR REPLACE SYNONYM corele_congiuntorimt_sn FOR corele.congiuntorimt_sn
/
CREATE OR REPLACE SYNONYM corele_congiuntori_sa FOR corele.congiuntori_sa
/
CREATE OR REPLACE SYNONYM corele_congiuntori_sn FOR corele.congiuntori_sn
/
CREATE OR REPLACE SYNONYM corele_consegne_sa FOR corele.consegne_sa
/
CREATE OR REPLACE SYNONYM corele_consegne_sn FOR corele.consegne_sn
/
CREATE OR REPLACE SYNONYM corele_date_import_consistenza FOR corele.date_import_consistenza
/
CREATE OR REPLACE SYNONYM corele_esercizi FOR corele.esercizi
/
CREATE OR REPLACE SYNONYM corele_impiantiat_sa FOR corele.impiantiat_sa
/
CREATE OR REPLACE SYNONYM corele_impiantiat_sn FOR corele.impiantiat_sn
/
CREATE OR REPLACE SYNONYM corele_impiantimt_sa FOR corele.impiantimt_sa
/
CREATE OR REPLACE SYNONYM corele_impiantimt_sn FOR corele.impiantimt_sn
/
CREATE OR REPLACE SYNONYM corele_lineebt_sa FOR corele.lineebt_sa
/
CREATE OR REPLACE SYNONYM corele_lineebt_sn FOR corele.lineebt_sn
/
CREATE OR REPLACE SYNONYM corele_montantiat_sa FOR corele.montantiat_sa
/
CREATE OR REPLACE SYNONYM corele_montantiat_sn FOR corele.montantiat_sn
/
CREATE OR REPLACE SYNONYM corele_montantimt_sa FOR corele.montantimt_sa
/
CREATE OR REPLACE SYNONYM corele_montantimt_sn FOR corele.montantimt_sn
/
CREATE OR REPLACE SYNONYM corele_nea_intcli FOR corele.nea_intcli
/
CREATE OR REPLACE SYNONYM corele_nodimt_sn FOR corele.nodimt_sn
/
CREATE OR REPLACE SYNONYM corele_paralleli_sa FOR corele.paralleli_sa
/
CREATE OR REPLACE SYNONYM corele_paralleli_sn FOR corele.paralleli_sn
/
CREATE OR REPLACE SYNONYM corele_rifasatori_sa FOR corele.rifasatori_sa
/
CREATE OR REPLACE SYNONYM corele_rifasatori_sn FOR corele.rifasatori_sn
/
CREATE OR REPLACE SYNONYM corele_sbarre_sa FOR corele.sbarre_sa
/
CREATE OR REPLACE SYNONYM corele_sbarre_sn FOR corele.sbarre_sn
/
CREATE OR REPLACE SYNONYM corele_sezionatorimt_sn FOR corele.sezionatorimt_sn
/
CREATE OR REPLACE SYNONYM corele_sezionatori_sa FOR corele.sezionatori_sa
/
CREATE OR REPLACE SYNONYM corele_sezionatori_sn FOR corele.sezionatori_sn
/
CREATE OR REPLACE SYNONYM corele_sezionibt_sa FOR corele.sezionibt_sa
/
CREATE OR REPLACE SYNONYM corele_sezionibt_sn FOR corele.sezionibt_sn
/
CREATE OR REPLACE SYNONYM corele_trasformatoriat_sa FOR corele.trasformatoriat_sa
/
CREATE OR REPLACE SYNONYM corele_trasformatoriat_sn FOR corele.trasformatoriat_sn
/
CREATE OR REPLACE SYNONYM corele_trasformatoribt_sa FOR corele.trasformatoribt_sa
/
CREATE OR REPLACE SYNONYM corele_trasformatoribt_sn FOR corele.trasformatoribt_sn
/
CREATE OR REPLACE SYNONYM corele_traslatori_sa FOR corele.traslatori_sa
/
CREATE OR REPLACE SYNONYM corele_traslatori_sn FOR corele.traslatori_sn
/
CREATE OR REPLACE SYNONYM corele_file_esercizi FOR corele.v_file_esercizi
/
CREATE OR REPLACE SYNONYM corele_v_mod_assetto_rete FOR corele.v_mod_assetto_rete
/
