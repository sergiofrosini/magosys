CREATE OR REPLACE procedure SP_STORICO_REGISTRO_INVIO
 as

  RetVal NUMBER;
  v_num_par  number;

BEGIN

    BEGIN
        select to_number(valore_parametro)
        into v_num_par
        from SAR_ADMIN.PARAMETRI_GENERALI
        where COD_APPLICAZIONE = 'MAGO'
        and nome_parametro = 'RES_STORICO_REG_INVIO' ;

    Exception when others then
       v_num_par := 60 ;
    END ;

    BEGIN

    delete from  REGISTRO_RES_INVIO
    where DATA_MISURA < trunc(sysdate - v_num_par);

    Exception when others then
      rollback;
      return;
    END ;

 COMMIT;
END;
/
