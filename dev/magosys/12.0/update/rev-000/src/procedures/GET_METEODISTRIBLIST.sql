CREATE OR REPLACE PROCEDURE GET_METEODISTRIBLIST (p_out_cur OUT SYS_REFCURSOR) AS

  v_osusr       VARCHAR2(16) := 'magosys';
  k_appl_mago   CONSTANT VARCHAR2(6) := 'MAGO';
  k_mago_fildir CONSTANT VARCHAR2(60) := '/usr/NEW/magosys/meteofile';
BEGIN
  open p_out_cur FOR
     'SELECT distinct u.codifica_utr|| e.codifica_esercizio cod_gest_CO, e.nome nome_CO,'||
                    ' m.cod_citta, m.nome nome_citta,rs.ip_pkg2 host_name, v_osusr OSusr, INITCAP(v_osusr) Ospassw,'||
                    ' CASE WHEN ab.cod_applicazione= k_appl_mago THEN ''LOCAL''  ELSE ''REMOTE'' END site_type,'||
                    ' k_mago_fildir fildir'||
               ' FROM sar_admin.unita_territoriali u '||
                 ' INNER JOIN sar_admin.esercizi e '||
                 '   USING(cod_utr) '||
                 ' INNER JOIN sar_admin.rel_cft_comuni rel '||
                 '     USING(cod_utr,cod_esercizio) '||
                 ' INNER JOIN sar_admin.retesar_esercizi rs '||
                 '    USING(cod_utr,cod_esercizio) '||
                 ' INNER JOIN meteo_rel_istat m '||
                 '  ON rel.cod_istat_comune = m.cod_istat '||
                 ' LEFT OUTER JOIN ( SELECT cod_utr,cod_esercizio,cod_applicazione '||
                                    '  FROM sar_admin.esercizi_abilitati '||
                                    ' WHERE cod_applicazione=k_appl_mago)  ab '||
                   ' USING (cod_utr,cod_esercizio) '||
                   ' WHERE cod_utr between 1 and 20 '||
                   '   AND cod_esercizio between 1 and 20 ';
   EXCEPTION
     WHEN others THEN null;

END;
/
