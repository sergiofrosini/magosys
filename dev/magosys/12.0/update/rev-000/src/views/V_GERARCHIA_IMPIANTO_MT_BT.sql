CREATE OR REPLACE FORCE VIEW v_gerarchia_impianto_mt_bt(
   l1,
   l2,
   l3 ) AS 
SELECT   DISTINCT SCS_ID L1, P.COD_ELEMENTO L2, CASE WHEN D.COD_TIPO_CLIENTE = 'C' THEN NULL ELSE F.COD_ELEMENTO END L3
        FROM (SELECT COD_ELEMENTO SCS_ID, CODICE_ST CSE_ID, COD_GEST_SBARRA SCS_COD, SUBSTR (COD_GEST, 1, 4) COD_ORD_NODO
                   , SUBSTR (COD_GEST, 5, 1) SER_NODO, SUBSTR (COD_GEST, 6) NUM_NODO
                FROM CORELE_IMPIANTIMT_SA INNER JOIN ELEMENTI ON COD_GEST_ELEMENTO = COD_GEST_SBARRA
               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE)
             LEFT OUTER JOIN (SELECT COD_GEST ELE_COD, COD_ENTE ELE_TIP, CODICEST_IMPMT CSE_ID
                                FROM CORELE_TRASFORMATORIBT_SA
                               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                              UNION ALL
                              SELECT COD_GEST ELE_COD, COD_ENTE ELE_TIP, CODICEST_PADRE CSE_ID
                                FROM CORELE_CLIENTIMT_SA A
                                     INNER JOIN AUI_CLIENTI_TLC B
                                         ON (COD_ORG_NODO = SUBSTR (COD_GEST, 1, 4)
                                         AND SER_NODO = SUBSTR (COD_GEST, 5, 1)
                                         AND NUM_NODO = SUBSTR (COD_GEST, 6, 6)
                                         AND TIPO_ELEMENTO = SUBSTR (COD_GEST, 12, 1)
                                         AND ID_CLIENTE = SUBSTR (COD_GEST, 13))
                               WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                 AND A.STATO = 'CH'
                                 AND TRATTAMENTO = 0
                                 AND B.STATO = 'E'
                                 AND SER_NODO = '2') USING
                 USING (CSE_ID)
             LEFT OUTER JOIN ELEMENTI P ON COD_GEST_ELEMENTO = ELE_COD
             LEFT OUTER JOIN (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO GMT_COD, ID_ELEMENTO ELE_COD
                                FROM ELEMENTI E INNER JOIN ELEMENTI_DEF USING (COD_ELEMENTO)
                               WHERE E.COD_TIPO_ELEMENTO IN ('GMT', 'TRX')
                                 AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) F
                 USING (ELE_COD)
             LEFT OUTER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = P.COD_ELEMENTO
    ORDER BY L1 NULLS FIRST, L2 NULLS FIRST, L3 NULLS FIRST
/

