CREATE OR REPLACE FORCE VIEW v_gerarchia_geografica(
   ese_cod,
   ese_gst,
   ese_nom,
   reg_gst,
   reg_nom,
   reg_x,
   reg_y,
   prv_gst,
   prv_nom,
   prv_x,
   prv_y,
   com_gst,
   com_nom,
   com_x,
   com_y,
   cft_gst,
   cft_nom,
   cft_x,
   cft_y,
   scs_gst,
   scs_nom,
   scs_x,
   scs_y,
   cod_istat_prov,
   cod_istat_comune,
   codifica_cft,
   ese_primario ) AS 
SELECT COD_ESE                 ESE_COD,
       E.GST_ESE               ESE_GST,
       E.NOM_ESE               ESE_NOM,
       r.COD_ISTAT_REG         REG_GST,
       SUBSTR(R.NOME,1,60)     REG_NOM,
       R.LONGITUDINE_GPS       REG_X,
       R.LATITUDINE_GPS        REG_Y,
       P.SIGLA                 PRV_GST,
       SUBSTR(P.NOME,1,60)     PRV_NOM,
       P.LONGITUDINE_GPS       PRV_X,
       P.LATITUDINE_GPS        PRV_Y,
       c.COD_ISTAT_COMUNE      COM_GST,
       SUBSTR(C.NOME,1,60)     COM_NOM,
       C.LONGITUDINE_GPS       COM_X,
       C.LATITUDINE_GPS        COM_Y,
       x.CODIFICA_COMPLETA_CFT CFT_GST,
       SUBSTR(F.NOME,1,60)     CFT_NOM,
       F.LONGITUDINE_GPS       CFT_X,
       F.LATITUDINE_GPS        CFT_Y,
       COD_GEST_ELEMENTO       SCS_GST,
       NOME_ELEMENTO           SCS_NOM,
       COORDINATA_X            SCS_X,
       COORDINATA_Y            SCS_Y,
       c.COD_ISTAT_PROV,
       c.COD_ISTAT_COMUNE,
       CODIFICA_CFT,
       ESE_PRIMARIO
  FROM SAR_CFT F
 INNER JOIN SAR_REL_CFT_COMUNI X ON X.CODIFICA_COMPLETA_CFT = F.CODIFICA_COMPLETA_CFT
 INNER JOIN SAR_COMUNI         C ON C.COD_ISTAT_COMUNE      = X.COD_ISTAT_COMUNE
 INNER JOIN SAR_PROVINCE       P ON P.COD_ISTAT_PROV        = C.COD_ISTAT_PROV
 INNER JOIN SAR_REGIONI        R ON R.COD_ISTAT_REG         = P.COD_ISTAT_REG
 INNER JOIN V_ESERCIZI         E ON E.COD_UTR               = F.COD_UTR
                                AND E.COD_ESERCIZIO         = F.COD_ESERCIZIO
 LEFT OUTER JOIN
      (SELECT COD_ELEMENTO SCS_COD,COD_GEST_ELEMENTO,RIF_ELEMENTO CFT_GST,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
          FROM ELEMENTI E
         INNER JOIN ELEMENTI_DEF     D USING (COD_ELEMENTO)
         INNER JOIN GERARCHIA_IMP_SA R USING (COD_ELEMENTO)
         WHERE E.COD_TIPO_ELEMENTO = 'SCS'
           AND SYSDATE BETWEEN       D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
           AND SYSDATE BETWEEN       R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE
       ) S ON F.CODIFICA_COMPLETA_CFT = S.CFT_GST
 ORDER BY NVL(ESE_PRIMARIO,-1) DESC, R.COD_ISTAT_REG, C.COD_ISTAT_PROV, C.COD_ISTAT_COMUNE, S.COD_GEST_ELEMENTO
/

