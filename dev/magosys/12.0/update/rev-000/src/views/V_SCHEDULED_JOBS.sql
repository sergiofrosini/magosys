CREATE OR REPLACE FORCE VIEW v_scheduled_jobs(
   elabora,
   jobmin,
   jobmax,
   datarif,
   tipo_job,
   organizzazione,
   stato,
   data_elab,
   id_rete,
   cod_tipo_misura,
   misura_statica,
   num,
   datains ) AS 
SELECT DECODE(ELABORA,1,'Si','No') ELABORA,
       JOBMIN,JOBMAX,DATARIF,TIPO_JOB,ORGANIZZAZIONE,STATO,DATA_ELAB,ID_RETE,COD_TIPO_MISURA,MISURA_STATICA,NUM,DATAINS
  FROM (SELECT A.JOBMIN,A.JOBMAX,A.DATARIF,A.TIPO_JOB,A.ORGANIZZAZIONE,A.STATO,A.DATA_ELAB,A.ID_RETE,A.COD_TIPO_MISURA,A.MISURA_STATICA,A.NUM,DATAINS,
               CASE
                  WHEN TIPO_JOB IN (4,5,6) THEN  -- 4=gcAggregMisureStd,5=gcAggregMisureStdPrMed,6=gcAggregMisureStdPrLow
                       CASE WHEN PKG_UtlGlb.GetParamGenNum('ELAB_AGG_FUTURE',1) = 0
                              THEN CASE WHEN DATARIF > SYSDATE + (1/1440*30)
                                             THEN 0
                                             ELSE 1
                                   END
                              ELSE 1
                       END
               END ELABORA
          FROM (SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                       CASE
                          WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                          ELSE DATARIF + (1 / 24 * RITARDO_HH)
                       END data_elab, NULL DATAINS,
                       ID_RETE, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                  FROM SCHEDULED_JOBS
                 INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                  LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                 WHERE TIPO_JOB IN (1, 2)   -- 1=gcLinearizzazione, 2=gcCalcolaMisureStat
                   AND DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH ))
                 GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, ID_RETE, MISURA_STATICA, RITARDO_HH
                UNION ALL
                SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                       CASE
                          WHEN DATARIF < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                          ELSE DATARIF + (1 / 24 * RITARDO_HH)
                       END data_elab, NULL DATAINS,
                       NULL, NULL  COD_TIPO_MISURA, MISURA_STATICA, COUNT(*) num
                  FROM SCHEDULED_JOBS
                 INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                  LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                 WHERE TIPO_JOB = 3 -- 3=gcAggregMisureStat
                   AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                 GROUP BY TIPO_JOB, DATARIF, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH
                UNION ALL
                SELECT MIN(JOB_NUMBER) JobMin, MAX(JOB_NUMBER) JobMax, MIN(DATARIF) DATARIF, TIPO_JOB, ORGANIZZAZIONE, STATO,
                       CASE
                          WHEN MAX(DATAINS)  < (SYSDATE - (1 / 24 * RITARDO_HH)) THEN NULL
                          ELSE MAX(DATAINS)  + (1 / 24 * RITARDO_HH)
                       END data_elab, MAX(DATAINS),
                       NULL, NULL, MISURA_STATICA, COUNT(*) num
                  FROM SCHEDULED_JOBS
                 INNER JOIN SCHEDULED_JOBS_DEF USING(TIPO_JOB)
                  LEFT OUTER JOIN V_TIPI_MISURA USING(COD_TIPO_MISURA)
                 WHERE TIPO_JOB IN (4,5,6) -- 4=gcAggregMisureStd,5=gcAggregMisureStdPrMed,6=gcAggregMisureStdPrLow
                   AND SYSDATE >= (DATAINS + (1 / 24 * RITARDO_HH ))
                   AND PKG_Mago.IsReplayFL = 0 -- Replay (PKG_Mago.IsReplayFL = 1) non aggrega
                 GROUP BY TIPO_JOB, ORGANIZZAZIONE, STATO, MISURA_STATICA, RITARDO_HH, DATARIF
               ) A
       )
 --WHERE ELABORA = 1
 ORDER BY TIPO_JOB,
          CASE
             --WHEN TIPO_JOB IN (4,5,6) THEN -- 4=gcAggregMisureStd,5=gcAggregMisureStdPrMed,6=gcAggregMisureStdPrLow
             WHEN TIPO_JOB = 4 THEN -- 4=gcAggregMisureStd
               CASE
                   WHEN TRUNC(SYSDATE,'HH24')        = TRUNC(DATARIF,'HH24')
                     OR TRUNC(SYSDATE + 1/24,'HH24') = TRUNC(DATARIF,'HH24')
                        THEN TIPO_JOB + (10*TIPO_JOB)  -- se in ora corrente o successiva aggreg prioritaria
                        ELSE TIPO_JOB + (100*TIPO_JOB) -- altrimenti aggreg non prioritaria
               END
             ELSE TIPO_JOB
          END,
          CASE
             WHEN DATA_ELAB IS NULL THEN 0
             ELSE 1
          END,
          DATARIF, STATO DESC, ORGANIZZAZIONE, ID_RETE, MISURA_STATICA DESC, COD_TIPO_MISURA
/

