CREATE OR REPLACE FORCE VIEW v_mod_assetto_rete_sa(
   datarif,
   stato,
   azione,
   cod_elemento,
   gest_elemento,
   tipo_elemento,
   cod_padre,
   gest_padre,
   tipo_padre,
   data_rel,
   tipo_rete,
   ger_ecp_ele,
   ger_ecs_ele,
   ger_amm_ele,
   ger_geo_ele,
   ger_ecp_padre,
   ger_ecs_padre,
   ger_amm_padre,
   ger_geo_padre ) AS 
SELECT DATARIF,
       STATO,
       AZIONE,
       COD_ELEMENTO,
       GEST_ELEMENTO,
       TIPO_ELEMENTO,
       COD_PADRE,
       GEST_PADRE,
       TIPO_PADRE,
       DATA_REL,
       TIPO_RETE,
       GER_ECP_ELE,
       GER_ECS_ELE,
       GER_AMM_ELE,
       GER_GEO_ELE,
       GER_ECP_PADRE,
       GER_ECS_PADRE,
       GER_AMM_PADRE,
       GER_GEO_PADRE
  FROM (SELECT DATARIF,
               STATO,
               CASE STATO
                  WHEN 'AP' THEN 2
                  WHEN 'CH' THEN 1
                            ELSE NULL
               END AZIONE,
               ELE.COD_ELEMENTO         COD_ELEMENTO,
               ELE.COD_GEST_ELEMENTO    GEST_ELEMENTO,
               ELE.COD_TIPO_ELEMENTO    TIPO_ELEMENTO,
               PDR.COD_ELEMENTO         COD_PADRE,
               PDR.COD_GEST_ELEMENTO    GEST_PADRE,
               PDR.COD_TIPO_ELEMENTO    TIPO_PADRE,
               REL.DATA_ATTIVAZIONE     DATA_REL,
               TEL.COD_TIPO_RETE        TIPO_RETE,
               TEL.GER_ECP              GER_ECP_ELE,
               TEL.GER_ECS              GER_ECS_ELE,
               TEL.GER_ECS              GER_AMM_ELE,
               TEL.GER_ECS              GER_GEO_ELE,
               TPA.GER_ECP              GER_ECP_PADRE,
               TPA.GER_ECS              GER_ECS_PADRE,
               TPA.GER_ECS              GER_AMM_PADRE,
               TPA.GER_ECS              GER_GEO_PADRE,
               CASE WHEN STATO = 'CH' THEN
                      CASE WHEN REL.DATA_ATTIVAZIONE IS NOT NULL
                              THEN NULL
                              ELSE 1
                      END
                    WHEN STATO = 'AP' THEN
                      CASE WHEN REL.DATA_ATTIVAZIONE IS NULL
                              THEN NULL
                              ELSE 1
                      END
               END DA_ELABORARE
          FROM CORELE_V_MOD_ASSETTO_RETE NEA
         INNER JOIN ELEMENTI             ELE ON NEA.ELEMENTO = ELE.COD_GEST_ELEMENTO
         INNER JOIN ELEMENTI             PDR ON NEA.PADRE = PDR.COD_GEST_ELEMENTO
         INNER JOIN TIPI_ELEMENTO        TEL ON TEL.COD_TIPO_ELEMENTO = ELE.COD_TIPO_ELEMENTO
         INNER JOIN TIPI_ELEMENTO        TPA ON TPA.COD_TIPO_ELEMENTO = PDR.COD_TIPO_ELEMENTO
          LEFT OUTER JOIN (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO, DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE FROM REL_ELEMENTI_ECP_SA
                           UNION ALL
                           SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO, DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE FROM REL_ELEMENTI_ECS_SA
                          ) REL ON (    PDR.COD_ELEMENTO = REL.COD_ELEMENTO_PADRE
                                    AND ELE.COD_ELEMENTO = REL.COD_ELEMENTO_FIGLIO
                                    AND DATARIF BETWEEN REL.DATA_ATTIVAZIONE AND REL.DATA_DISATTIVAZIONE
                                   )
       )
 WHERE DA_ELABORARE IS NOT NULL
 ORDER BY DATARIF, GEST_PADRE, GEST_ELEMENTO
/

