CREATE OR REPLACE FORCE VIEW v_current_version(
   version,
   data,
   content,
   description ) AS 
SELECT VERSION,DATA,CONTENT,DESCRIPTION
  FROM (SELECT VERSION,DATA,CONTENT,DESCRIPTION,ROW_NUMBER() OVER( ORDER BY MAJOR DESC, MINOR DESC, PATCH DESC) NUM
          FROM (SELECT 'DB=MAGO_'||NVL(TRIM(LEADING '0' FROM TRIM(MAJOR)),'0')||'.'||NVL(TRIM(LEADING '0' FROM TRIM(MINOR)),'0')||'/'||NVL(TRIM(LEADING '0' FROM TRIM(PATCH)),'0')||' ('||TO_CHAR(DATA,'dd/mm/yyyy')||')' VERSION,
                       DATA, MAJOR, MINOR, PATCH, REVISION, CONTENT, DESCRIPTION
                  FROM (SELECT DATA,
                               LPAD(REGEXP_REPLACE(MAJOR,'[^[:digit:]]'),4,'0')||REGEXP_REPLACE(MAJOR,'[^[:alpha:]]') MAJOR,
                               LPAD(REGEXP_REPLACE(MINOR,'[^[:digit:]]'),4,'0')||REGEXP_REPLACE(MINOR,'[^[:alpha:]]') MINOR,
                               LPAD(REGEXP_REPLACE(PATCH,'[^[:digit:]]'),4,'0')||REGEXP_REPLACE(PATCH,'[^[:alpha:]]') PATCH,
                               REVISION,CONTENT,DESCRIPTION
                          FROM VERSION V
                       )
               )
       )
 WHERE NUM = 1
/

