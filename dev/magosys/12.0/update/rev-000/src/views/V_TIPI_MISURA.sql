CREATE OR REPLACE FORCE VIEW v_tipi_misura(
   cod_tipo_misura,
   codifica_st,
   descrizione,
   mis_generazione,
   misura_statica,
   cod_um_standard,
   risoluzione_fe,
   rilevazioni_nagative,
   split_fonte_energia,
   tipo_interpolazione_freq_camp,
   tipo_interpolazione_buchi_camp,
   usata ) AS 
SELECT COD_TIPO_MISURA,
       CODIFICA_ST,
       DESCRIZIONE,
       MIS_GENERAZIONE,
       PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) MISURA_STATICA,
       COD_UM_STANDARD,
       RISOLUZIONE_FE,
       RILEVAZIONI_NAGATIVE,
       SPLIT_FONTE_ENERGIA,
       TIPO_INTERPOLAZIONE_FREQ_CAMP,
       TIPO_INTERPOLAZIONE_BUCHI_CAMP,
       CASE
           WHEN COD_TIPO_MISURA IN (SELECT DISTINCT COD_TIPO_MISURA FROM GRUPPI_MISURA) THEN 1
           ELSE 0
       END USATA
  FROM TIPI_MISURA
 WHERE FLG_KPI = 0
 ORDER BY USATA DESC ,
          COD_TIPO_MISURA
/

