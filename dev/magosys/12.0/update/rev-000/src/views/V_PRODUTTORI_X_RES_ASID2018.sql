CREATE OR REPLACE FORCE VIEW v_produttori_x_res_asid2018(
   gest_prod_id,
   dt_ini_p,
   dt_fin_p,
   fonte,
   pot_inst,
   stato_attuale_id,
   categoria_prod_id,
   dt_ini_r,
   dt_fin_r ) AS 
SELECT                                    -- dati da tabella PRODUTTORI_DEF
         --P.GEST_PROD_ID,
         s.cod_gest_old as gest_prod_id,
          v.DT_INI_P,
          v.DT_FIN_P,
          v.FONTE,
          v.pot_inst,
          -- dati da tabella PRODUTTORI_RIGEDI
          v.STATO_ATTUALE_ID,
          v.CATEGORIA_PROD_ID,
          v.DT_INI_R,
          v.DT_FIN_R
from V_PRODUTTORI_X_RES@PKG1_RIGEDI.IT v
join ricodifica_elementi@pkg1_stmaui.it s
on (s.cod_gest_new=v.gest_prod_id
and
s.tipo_ricodifica='ASID2018')
/

