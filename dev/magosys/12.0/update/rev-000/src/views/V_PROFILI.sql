CREATE OR REPLACE FORCE VIEW v_profili(
   cod_elemento,
   cod_gest_elemento,
   cod_tipo_misura,
   anno,
   mese,
   tipo,
   daytime,
   valore,
   dt_misure_inizio,
   dt_misure_fine ) AS 
SELECT COD_ELEMENTO,
       COD_GEST_ELEMENTO,
       COD_TIPO_MISURA,
       ANNO,
       MESE,
       TIPO,
       SUBSTR(DAYTIME,1,2)||':'||SUBSTR(DAYTIME,3,2) DAYTIME,
       VALORE,
       DT_MISURE_INIZIO,
       DT_MISURE_FINE
  FROM PROFILI
  JOIN PROFILI_DEF USING(COD_PROFILO)
  JOIN PROFILI_VAL USING(COD_PROFILO,ANNO,MESE)
  JOIN ELEMENTI USING(COD_ELEMENTO)
/

