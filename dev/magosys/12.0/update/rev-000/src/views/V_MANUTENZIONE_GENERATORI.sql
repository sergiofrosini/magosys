CREATE OR REPLACE FORCE VIEW v_manutenzione_generatori(
   cod_elemento,
   tipo_elemento,
   percentuale,
   data_inizio,
   data_fine,
   cod_gest_elemento,
   potenza_installata,
   id_manutenzione,
   flg_warning,
   num ) AS 
WITH manutenzione_gen AS
(
SELECT man.cod_elemento,man.tipo_elemento,man.percentuale,man.data_inizio,man.data_fine
      ,el.cod_gest_elemento, mis.valore potenza_installata,man.id_manutenzione
  FROM
      (SELECT MIN(id_manutenzione) id_manutenzione
             ,cod_elemento
             ,tipo_elemento
             ,percentuale
             ,data_inizio
             ,data_fine
         FROM
             (SELECT id_manutenzione
                    ,man.cod_elemento
                    ,tipo_elemento
                    ,percentuale
                    ,NVL(x_data_inizio, LAG(x_data_inizio) OVER (PARTITION BY man.cod_elemento ORDER BY rn)) data_inizio
                    ,NVL(x_data_fine, LEAD(x_data_fine) OVER (PARTITION BY man.cod_elemento ORDER BY rn NULLS FIRST)) data_fine
                FROM (
                      SELECT cod_elemento,tipo_elemento,percentuale, data_inizio, data_fine
                            ,CASE WHEN data_inizio = LAG(data_fine) OVER (PARTITION BY cod_elemento ORDER BY data_inizio) AND percentuale = LAG(percentuale) OVER (PARTITION BY cod_elemento ORDER BY data_inizio) THEN NULL ELSE data_inizio END x_data_inizio
                            ,CASE WHEN data_fine = LEAD(data_inizio) OVER (PARTITION BY cod_elemento ORDER BY data_inizio) AND percentuale = LEAD(percentuale) OVER (PARTITION BY cod_elemento ORDER BY data_inizio) THEN NULL ELSE data_fine END x_data_fine
                            ,ROW_NUMBER() OVER (PARTITION BY cod_elemento ORDER BY data_inizio) rn
                            ,id_manutenzione
                        FROM manutenzione
                     ) man
                WHERE x_data_inizio IS NOT NULL OR x_data_fine IS NOT NULL
             ) man
       GROUP BY cod_elemento,tipo_elemento,percentuale,data_inizio,data_fine
      ) man
      ,elementi el
      ,(
        SELECT mis.*, tr.cod_elemento, tr.cod_tipo_misura
          FROM misure_acquisite_statiche mis
              ,trattamento_elementi tr
         WHERE tr.cod_trattamento_elem = mis.cod_trattamento_elem
           AND cod_tipo_misura = 'PI'
       ) mis
 WHERE el.cod_elemento = man.cod_elemento
   AND mis.cod_elemento(+) = man.cod_elemento
   AND man.data_inizio = mis.data_attivazione(+)
)
SELECT "COD_ELEMENTO","TIPO_ELEMENTO","PERCENTUALE","DATA_INIZIO","DATA_FINE","COD_GEST_ELEMENTO","POTENZA_INSTALLATA","ID_MANUTENZIONE","FLG_WARNING","NUM" FROM
(
SELECT man.*
      ,CASE WHEN no_def.cod_elemento IS NOT NULL THEN 1 ELSE 0 END flg_warning
      ,ROW_NUMBER() OVER ( PARTITION BY man.cod_elemento,man.tipo_elemento,man.percentuale,man.data_inizio,man.data_fine ORDER BY man.data_inizio) num
  FROM manutenzione_gen man
      ,
      (
       SELECT attesi.cod_elemento
             ,attesi.tipo_elemento
             ,attesi.percentuale
             ,attesi.data_inizio
             ,attesi.data_fine
         FROM
             (
              SELECT cod_elemento, cod_elemento_figlio, tipo_elemento, percentuale, data_inizio, data_fine
               FROM
                   (
                    SELECT ger.cod_elemento cod_elemento_figlio
                          ,man.*
                      FROM manutenzione_gen man
                          ,gerarchia_amm ger
                     WHERE man.cod_elemento IN (l01,l02,l03,l04,l05,l06,l07,l08,l09,l10,l11,l12,l13)
                       AND data_attivazione <= data_inizio
                       AND data_disattivazione >= data_fine
                   )
             ) attesi
            ,manutenzione_gen man
       WHERE man.cod_elemento(+) = attesi.cod_elemento_figlio
         AND man.percentuale(+) = attesi.percentuale
         AND attesi.data_inizio >= man.data_inizio(+)
         AND attesi.data_fine <= man.data_fine(+)
         AND man.cod_elemento IS NULL
      ) no_def
 WHERE man.cod_elemento = no_def.cod_elemento(+)
   AND man.tipo_elemento = no_def.tipo_elemento(+)
   AND man.percentuale = no_def.percentuale(+)
   AND man.data_inizio = no_def.data_inizio(+)
   AND man.data_fine = no_def.data_fine(+)
)
WHERE num = 1
/

