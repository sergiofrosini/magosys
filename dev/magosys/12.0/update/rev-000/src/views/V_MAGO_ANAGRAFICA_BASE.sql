CREATE OR REPLACE FORCE VIEW v_mago_anagrafica_base(
   cod_gest,
   nome,
   cod_ente,
   rif_elem,
   id_elem,
   centrosatellite,
   nodo_rif ) AS 
SELECT /*
             VISTA per generazione anagrafica MAGO
             ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                         NON eliminare prefissi di schema !!!!
           */
          COD_GEST, NOME, COD_ENTE, RIF_ELEM, ID_ELEM, CENTROSATELLITE, NODO_RIF
      FROM (SELECT es.CODICE_ST
                 , es.COD_GEST
                 , es.NOME, es.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
              FROM CORELE_ESERCIZI es
             WHERE SYSDATE BETWEEN es.DATA_INIZIO AND es.DATA_FINE
            UNION ALL
            SELECT cp.CODICE_ST, cp.COD_GEST, cp.NOME, cp.COD_ENTE, cp.CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
              FROM CORELE_IMPIANTIAT_SA cp
             WHERE SYSDATE BETWEEN cp.DATA_INIZIO AND cp.DATA_FINE
            UNION ALL
            SELECT ta.CODICE_ST, ta.COD_GEST, ta.NOME, ta.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM
                 , NULL NODO_RIF
              FROM CORELE_TRASFORMATORIAT_SA ta
             WHERE SYSDATE BETWEEN ta.DATA_INIZIO AND ta.DATA_FINE
            UNION ALL
            SELECT av.CODICE_ST, av.COD_GEST, NOME_TRASF NOME, av.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, NULL ID_ELEM
                 , NULL NODO_RIF
              FROM CORELE_AVVOLGIMENTI_SA av
             WHERE av.COD_ENTE <> 'PRIM'
               AND SYSDATE BETWEEN av.DATA_INIZIO AND av.DATA_FINE
            UNION ALL
            SELECT sb.CODICE_ST, sb.COD_GEST, sb.NOME, sb.COD_ENTE, NULL CENTROSATELLITE, cp.COD_GEST RIF_ELEM, NULL ID_ELEM
                 , NULL NODO_RIF
              FROM CORELE_SBARRE_SA sb
                   INNER JOIN CORELE_IMPIANTIAT_SA cp
                       ON cp.CODICE_ST = sb.CODICEST_IMPAT
                      AND SYSDATE BETWEEN cp.DATA_INIZIO AND cp.DATA_FINE
             WHERE SYSDATE BETWEEN sb.DATA_INIZIO AND sb.DATA_FINE
               AND sb.TIPO = 'MT'
            UNION ALL
            SELECT mm.CODICE_ST, mm.COD_GEST, mm.NOME, mm.COD_ENTE, NULL CENTROSATELLITE
                 , CASE mm.COD_GEST_DIRMT WHEN 'ND' THEN NULL ELSE mm.COD_GEST_DIRMT END RIF_ELEM, NULL ID_ELEM, NULL NODO_RIF
              FROM CORELE_MONTANTIMT_SA mm
             WHERE SYSDATE BETWEEN mm.DATA_INIZIO AND mm.DATA_FINE
            UNION ALL
            SELECT cs.CODICE_ST, cs.COD_GEST_SBARRA COD_GEST, cs.NOME, 'SBCS' COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM
                 , cs.COD_GEST ID_ELEM, NULL NODO_RIF
              FROM CORELE_IMPIANTIMT_SA cs
             WHERE SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
            UNION ALL
            SELECT cm.CODICE_ST, cm.COD_GEST, cm.NOME, cm.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, cm.COD_GEST_SBARRA ID_ELEM
                 , cs.COD_GEST NODO_RIF
              FROM CORELE_CLIENTIMT_SA cm
                   INNER JOIN CORELE_IMPIANTIMT_SA cs
                       ON cs.CODICE_ST = cm.CODICEST_PADRE
                      AND SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
             WHERE SYSDATE BETWEEN cm.DATA_INIZIO AND cm.DATA_FINE
            UNION ALL
            SELECT tm.CODICE_ST, tm.COD_GEST, tm.NOME, tm.COD_ENTE, NULL CENTROSATELLITE, NULL RIF_ELEM, tm.COD_GEST_SBARRA ID_ELEM
                 , cs.COD_GEST NODO_RIF
              FROM CORELE_TRASFORMATORIBT_SA tm
                   INNER JOIN CORELE_IMPIANTIMT_SA cs
                       ON cs.CODICE_ST = tm.CODICEST_IMPMT
                      AND SYSDATE BETWEEN cs.DATA_INIZIO AND cs.DATA_FINE
             WHERE SYSDATE BETWEEN tm.DATA_INIZIO AND tm.DATA_FINE)
/

