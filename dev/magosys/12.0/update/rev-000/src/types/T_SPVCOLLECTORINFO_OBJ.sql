CREATE OR REPLACE TYPE "T_SPVCOLLECTORINFO_OBJ" AS OBJECT(
 ID_INFO                   			NUMBER
,COLLECTOR_NAME                     VARCHAR2(20)
,DATA_INSERIMENTO                   DATE
,ID_SISTEMA          				NUMBER
,ID_DIZIONARIO_ALLARME          	NUMBER
,SUPPLIER_NAME                      VARCHAR2(20)
,COD_GEST_ELEMENTO                  VARCHAR2(20)
,COD_TIPO_MISURA                    VARCHAR2(6)
,COD_TIPO_FONTE                     VARCHAR2(2)
,VALORE                             NUMBER
,INFORMAZIONI                       VARCHAR2(1000)
);
/

