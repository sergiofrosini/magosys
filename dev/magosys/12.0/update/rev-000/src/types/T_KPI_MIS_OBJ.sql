CREATE OR REPLACE TYPE T_KPI_MIS_OBJ AS OBJECT
      (COD_ELEMENTO         NUMBER
      ,COD_GEST_ELEMENTO    VARCHAR2(20)
      ,COD_INDICE           VARCHAR2(6)
      ,COD_TIPO_FONTE       VARCHAR2(2)
      ,DATA                 DATE
      ,VALORE               NUMBER
      )
/

