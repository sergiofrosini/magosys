CREATE OR REPLACE TYPE T_TRTMISLV_OBJ as object
  (
    COLNUMBER           NUMBER,
    AUINUMBER           NUMBER,
    TRANSFNUMBERNODE    NUMBER,
    ACTTRANSFNUMBERNODE NUMBER,
    AUICODE             VARCHAR2(20 BYTE),
    VAL_POTENZA         NUMBER(20,9),
    VAL_CLIENTI         NUMBER(20,9),
    CTYPE_INDEX         NUMBER(2,0)
  )
/

