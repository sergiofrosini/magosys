CREATE OR REPLACE TYPE  t_RowGerarchia        IS object (COD_ELEMENTO         NUMBER,
                                       COD_GEST_ELEMENTO    VARCHAR2(20),
                                      COD_TIPO_ELEMENTO    VARCHAR2(3),
                                       LIVELLO              INTEGER,
                                       SEQUENZA             INTEGER);
/

