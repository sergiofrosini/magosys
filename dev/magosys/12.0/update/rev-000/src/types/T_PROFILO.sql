CREATE OR REPLACE TYPE T_PROFILO  AS OBJECT
                       (MESE                 NUMBER(2),
                        TIPO                 NUMBER(1),
                        DAYTIME              VARCHAR2(5),
                        VALORE               NUMBER
                      );
/

