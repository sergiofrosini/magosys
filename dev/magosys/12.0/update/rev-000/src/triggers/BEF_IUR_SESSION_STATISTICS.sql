CREATE OR REPLACE TRIGGER BEF_IUR_SESSION_STATISTICS
BEFORE INSERT OR UPDATE
ON SESSION_STATISTICS
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:      BEF_IUR_SESSION_STATISTICS
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.2     16/09/2015   Moretti C.      Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        :NEW.DATE_START := SYSDATE;
    END IF;
    IF :NEW.STATUS = PKG_Stats.gSessionStatusDone THEN
        :NEW.DATE_END := SYSDATE;
    END IF;
END BEF_IUR_SESSION_STATISTICS;
/
