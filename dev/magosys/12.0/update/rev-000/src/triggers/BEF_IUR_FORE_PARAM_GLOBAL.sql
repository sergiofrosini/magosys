CREATE OR REPLACE TRIGGER BEF_IUR_FORE_PARAM_GLOBAL
BEFORE INSERT OR UPDATE
ON FORECAST_PARAM_GLOBAL REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_FORE_PARAM_GLOBAL
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      08/08/2016   FORNO S.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.ID_PROGRESS IS NULL THEN
            SELECT FORECAST_PARAM_GLOBAL_PKSEQ.NEXTVAL INTO :NEW.ID_PROGRESS FROM DUAL;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.ID_PROGRESS <> :NEW.ID_PROGRESS THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su ID_PROGRESS in tabella ''FORECAST_PARAM_GLOBAL''');
        END IF;
    END IF;

END BEF_IUR_FORE_PARAM_GLOBAL;
/
