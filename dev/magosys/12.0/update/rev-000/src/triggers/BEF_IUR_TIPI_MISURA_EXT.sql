CREATE OR REPLACE TRIGGER BEF_IUR_TIPI_MISURA_EXT
BEFORE INSERT OR UPDATE
ON TIPI_MISURA_EXT
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE
/* *****************************************************************************
   NAME:       BEF_IUR_TIPI_MISURA_EXT
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      13/06/2016   Moretti C.      Created this trigger.

   NOTES:

***************************************************************************** */
    vCount INTEGER;
BEGIN
    SELECT COUNT(*)
      INTO vCount
      FROM TIPI_MISURA
     WHERE COD_TIPO_MISURA = :NEW.COD_TIPO_MISURA_EXT;
     IF vCount > 0 THEN
        RAISE_APPLICATION_ERROR(-20111,'Il codice Tipo Misura '''||:NEW.COD_TIPO_MISURA_EXT||''' e'' gia'' usato in tabella TIPI_MISURA');
     END IF;
END BEF_IUR_TIPI_MISURA_EXT;
/
