CREATE OR REPLACE TRIGGER BEF_IUR_REL_ELEMENTI_ECS_SN
BEFORE INSERT OR UPDATE
ON REL_ELEMENTI_ECS_SN REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_REL_ELEMENTI_ECS_SN
   PURPOSE:    Controllare la validita' dei dati in inserimento o modifica
               e gestire in modo automatico la versione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0        07/04/2009   Moretti C.      1. Created this trigger.

   NOTES:

***************************************************************************** */
 --vDataAttiv     DATE  := NULL;
 --vDataDisatt    DATE  := NULL;

 BEGIN
    IF UPDATING THEN
        IF :NEW.DATA_ATTIVAZIONE <> :OLD.DATA_ATTIVAZIONE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su campo Data Attivazione');
        END IF;

        IF :NEW.COD_ELEMENTO_PADRE <> :OLD.COD_ELEMENTO_PADRE THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su Codice Elemento Padre');
        END IF;

        IF :NEW.COD_ELEMENTO_FIGLIO <> :OLD.COD_ELEMENTO_FIGLIO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Modifica non ammessa su Codice Elemento Figlio');
        END IF;
    END IF;

    IF :NEW.COD_ELEMENTO_PADRE = :NEW.COD_ELEMENTO_FIGLIO THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'I Codici Padre e Figlio non devono coincidere');
    END IF;

--    IF INSERTING THEN
--        BEGIN
--            SELECT DATA_ATTIVAZIONE INTO vDataAttiv
--              FROM REL_ELEMENTI_ECS_SN
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND :NEW.DATA_ATTIVAZIONE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
--            -- Relazione gia' presente - non procedere con l'inserimento
--            RAISE PKG_UtlGlb.geNothingToDo;
--            EXCEPTION
--                WHEN NO_DATA_FOUND THEN NULL;
--                WHEN OTHERS        THEN RAISE;
--        END;

--        BEGIN
--            /* cerco se e' presente una relazione compatibile con data
--               Data attivazione superiore alla Data attivazione in inserimento  */
--            SELECT DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE
--              INTO vDataAttiv,       vDataDisatt
--              FROM REL_ELEMENTI_ECS_SN
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE = (SELECT MIN(DATA_ATTIVAZIONE)
--                                         FROM REL_ELEMENTI_ECS_SN
--                                        WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--                                          AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--                                          AND DATA_ATTIVAZIONE   >= :NEW.DATA_ATTIVAZIONE);
--            /* poiche' il trigger di REL_ELEMENTI_ECS_SN non consernte di modificare la Data di attivazione
--               la relazione viene cancellata e reinserita con la nuova Data di attivazione */
--            DELETE REL_ELEMENTI_ECS_SN
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE    = vDataAttiv;
--            /* il record in inserimento assume ora la Data di disattivazione
--               del record precedentemente cancellato */
--            :NEW.DATA_DISATTIVAZIONE  := vDataDisatt;
--            EXCEPTION
--                WHEN NO_DATA_FOUND THEN NULL;
--                WHEN OTHERS        THEN RAISE;
--        END;

--        BEGIN
--            /* cerco se e' presente una relazione compatibile con data
--               Data disattivazione inferiore alla Data attivazione in inserimento  */
--            SELECT DATA_ATTIVAZIONE, DATA_DISATTIVAZIONE
--              INTO vDataAttiv,       vDataDisatt
--              FROM REL_ELEMENTI_ECS_SN
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_DISATTIVAZIONE = (SELECT MAX(DATA_DISATTIVAZIONE)
--                                            FROM REL_ELEMENTI_ECS_SN
--                                           WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--                                             AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--                                             AND DATA_DISATTIVAZIONE < :NEW.DATA_ATTIVAZIONE);
--            -- la Data di attivazieme dell'elemento e' quella della relazione trovata
--            :NEW.DATA_ATTIVAZIONE := vDataAttiv;
--            -- elimino la relazione trovata che sara' sostituita dalla relazione in inserimento!
--            DELETE REL_ELEMENTI_ECS_SN
--             WHERE COD_ELEMENTO_PADRE  = :NEW.COD_ELEMENTO_PADRE
--               AND COD_ELEMENTO_FIGLIO = :NEW.COD_ELEMENTO_FIGLIO
--               AND DATA_ATTIVAZIONE    = vDataAttiv
--               AND DATA_DISATTIVAZIONE = vDataDisatt;
--        EXCEPTION
--            WHEN NO_DATA_FOUND THEN NULL;
--            WHEN OTHERS        THEN RAISE;
--        END;
--    END IF;

    IF :NEW.DATA_DISATTIVAZIONE > PKG_UtlGlb.gkDataTappo THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,  'Data Disattivazione non deve eccedere la Data limite ' ||
                                                        '(' || TO_CHAR(PKG_UtlGlb.gkDataTappo,'yyyy/mm/dd') || ')');
    END IF;

--    IF :NEW.DATA_ATTIVAZIONE >= :NEW.DATA_DISATTIVAZIONE THEN
--        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR, 'Cod Elemento: Padre:'||:NEW.COD_ELEMENTO_PADRE||
--                                                                   ', Figlio:'||:NEW.COD_ELEMENTO_FIGLIO||
--                                                        ' - Data Attivazione deve essere inferiore a Data disattivazione '||
--                                                        TO_CHAR(:NEW.DATA_ATTIVAZIONE,'dd/mm/yyyy hh24:mi.ss')|| ' - '||
--                                                        TO_CHAR(:NEW.DATA_DISATTIVAZIONE,'dd/mm/yyyy hh24:mi.ss'));
--    END IF;

 EXCEPTION
    WHEN OTHERS THEN RAISE;

 END BEF_IUR_REL_ELEMENTI_ECS_SN;
/
