CREATE OR REPLACE TRIGGER BEF_IUR_INTERV_TRAINING
BEFORE INSERT OR UPDATE
ON FORECAST_PARAM_INTERV_TRAINING REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_INTERV_TRAINING
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      01/07/2016   FORNO S.      1. Created this trigger.

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.ID_INTERV_TRAINING IS NULL THEN
            SELECT INTERV_TRAINING_PKSEQ.NEXTVAL INTO :NEW.ID_INTERV_TRAINING FROM DUAL;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.ID_INTERV_TRAINING <> :NEW.ID_INTERV_TRAINING THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su ID_INTERV_TRAINING in tabella ''FORECAST_PARAM_INTERV_TRAINING''');
        END IF;
    END IF;

END BEF_IUR_INTERV_TRAINING;
/
