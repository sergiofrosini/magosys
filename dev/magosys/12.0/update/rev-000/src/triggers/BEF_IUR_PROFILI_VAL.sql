CREATE OR REPLACE TRIGGER BEF_IUR_PROFILI_VAL
BEFORE INSERT OR UPDATE
ON PROFILI_VAL
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_PROFILI_VAL
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      07/06/2016   Moretti C.      Created this trigger.

   NOTES:

***************************************************************************** */
    vDAYTIME  PROFILI_VAL.DAYTIME%TYPE;
BEGIN
    BEGIN
        vDAYTIME := TO_CHAR(TO_DATE(:NEW.DAYTIME,'hh24mi'),'hh24mi');
    EXCEPTION
        WHEN OTHERS THEN RAISE_APPLICATION_ERROR(-20201,'DAYTIME '||:NEW.DAYTIME||' non valido per il profilo');
    END;
   :NEW.DAYTIME := vDAYTIME;
END BEF_IUR_PROFILI_VAL;
/
