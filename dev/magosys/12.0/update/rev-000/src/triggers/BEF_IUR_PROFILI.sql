CREATE OR REPLACE TRIGGER BEF_IUR_PROFILI
BEFORE INSERT OR UPDATE
ON PROFILI
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_PROFILI
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.9c.0     08/06/2016   Moretti C.      definizione trigger

   NOTES:

***************************************************************************** */
BEGIN
    IF INSERTING THEN
        IF :NEW.COD_PROFILO IS NULL THEN
            -- per impredire che in caso di rigenerazione della Sequence (Replay) il trigger si invalidi uso sql dinamico
            EXECUTE IMMEDIATE 'SELECT PROFILI_PKSEQ.NEXTVAL FROM DUAL' INTO :NEW.COD_PROFILO ;
        END IF;
    END IF;
    IF UPDATING THEN
        IF :NEW.COD_PROFILO <> :NEW.COD_PROFILO THEN
            RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Modifica non ammessa su codice Profili in tabella ''PROFILI''');
        END IF;
    END IF;

END BEF_IUR_PROFILI;
/
