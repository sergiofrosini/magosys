SET VER OFF
SET SERVEROUTPUT ON SIZE UNLIMITED
SET LINE 300
SET CONCAT |

SPOOL  &spool_all APPEND

WHENEVER SQLERROR EXIT SQL.SQLCODE

conn sys/sys_dba&tns_arcdb2 as sysdba

COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;
SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;

GRANT SELECT  ON SAR_ADMIN.CALENDARIO_FESTIVITA TO MAGO;
GRANT EXECUTE ON SAR_ADMIN.PKG_CALENDARIO       TO MAGO;

PROMPT ==========================================
PROMPT Tablespaces
PROMPT __________________________________________

--@./tablespaces/MAGO_DATA.sql
--@./tablespaces/MAGO_IDX.sql
--@./tablespaces/MAGO_IOT.sql

PROMPT ==========================================
PROMPT User
PROMPT __________________________________________

@./users/MAGO.sql

PROMPT ==========================================
PROMPT Directories
PROMPT __________________________________________

@./directories/MAGO_LOGDIR_&distro.sql
@./directories/EXTDATA_DIR.sql

PROMPT ==========================================
PROMPT Grant To_Mago
PROMPT __________________________________________

@./grants/Grant_TO_MAGO.sql

PROMPT ==========================================
PROMPT Grant Corele_To_Mago
PROMPT __________________________________________

@./grants/Grant_CORELE_TO_MAGO.sql

PROMPT ==========================================
PROMPT Grant Crel_To_Mago
PROMPT __________________________________________

@./grants/Grant_CREL_TO_MAGO.sql

PROMPT ==========================================
PROMPT Grant Sar_Admin_To_Mago
PROMPT __________________________________________

conn mago/MAGO&tns_arcdb2

PROMPT ==========================================
PROMPT Tablespaces
PROMPT __________________________________________

--@./tablespaces/MAGO_DATA.sql
--@./tablespaces/MAGO_IDX.sql
--@./tablespaces/MAGO_IOT.sql

PROMPT ==========================================
PROMPT Database Link
PROMPT __________________________________________

@./dblinks/PKG1_RIGEDI.IT.sql
@./dblinks/PKG1_STMAUI.IT.sql
@./dblinks/PKG1_TMESX_2.IT.sql
@./dblinks/RES_DB_NAZ.ENEL.IT.sql

PROMPT ==========================================
PROMPT Sequence
PROMPT __________________________________________

@./sequences/ELEMENTI_PKSEQ.sql
@./sequences/FILEINFO_SEQUENCE.sql
@./sequences/FORECAST_PARAM_GLOBAL_PKSEQ.sql
@./sequences/INTERV_TRAINING_PKSEQ.sql
@./sequences/KPI_PKSEQ.sql
@./sequences/LOG_SEQUENCE.sql
@./sequences/MANUTENZIONE_SEQ.sql
@./sequences/METEO_FILE_XML_SEQ.sql
@./sequences/METEO_FILE_ZIP_SEQ.sql
@./sequences/METEO_JOB_SEQ.sql
@./sequences/OFFLINE_MEASURE_REQUEST_IDSEQ.sql
@./sequences/PROFILI_PKSEQ.sql
@./sequences/SCHEDULED_JOBS_PKSEQ.sql
@./sequences/SESSION_STATISTIC_IDSEQ.sql
@./sequences/SPV_ALLARMI_SEQ.sql
@./sequences/SPV_COLLECTOR_INFO_SEQ.sql
@./sequences/SPV_COLLECTOR_MEASURE_SEQ.sql
@./sequences/SPV_CONFIG_NOTIF_SEQ.sql
@./sequences/TRATTAMENTO_ELEMENTI_PKSEQ.sql

PROMPT ==========================================
PROMPT Other Synonym
PROMPT __________________________________________

@./synonyms/ANAGRAFICA_PUNTI.sql
@./synonyms/CORELE.sql
@./synonyms/SAR_ADMIN.sql
@./synonyms/AUI_CLIENTI_TLC.sql
@./synonyms/AUI_GENERATORI_TLC.sql
@./synonyms/AUI_NODI_TLC.sql
@./synonyms/AUI_TRASF_PROD_BT_TLC.sql
@./synonyms/AUI_UPDATE_AUI_TLC.sql
@./synonyms/AUI_UTILIZZO_TLC.sql
@./synonyms/TMX2_ELEMENTI.sql
@./synonyms/TMX2_MISURE.sql
@./synonyms/TMX2_PKG_APPSERV.sql
@./synonyms/TMX2_TIPI_MISURA.sql
@./synonyms/TMX2_TRATT_ELEM.sql

PROMPT ==========================================
PROMPT Table
PROMPT __________________________________________

@./tables/TIPI_CLIENTE.sql
@./tables/TIPI_GRUPPI_MISURA.sql
@./tables/TIPI_RETE.sql
@./tables/RAGGRUPPAMENTO_FONTI.sql
@./tables/TIPO_FONTI.sql
@./tables/TIPO_TECNOLOGIA_SOLARE.sql
@./tables/TIPI_MISURA.sql
@./tables/TIPI_ELEMENTO.sql
@./tables/TIPI_MISURA_CONV_UM.sql
@./tables/TIPI_MISURA_EXT.sql
@./tables/TIPI_MISURA_CONV_ORIG.sql
@./tables/ANAGRAFICA_PUNTI.sql
@./tables/APPLICATION_RUN.sql
@./tables/CFG_APPLICATION_PARAMETER.sql
@./tables/ELEMENTI.sql
@./tables/ELEMENTI_CFG.sql
@./tables/ELEMENTI_DEF.sql
@./tables/ELEMENTI_GDF_EOLICO.sql
@./tables/ELEMENTI_GDF_SOLARE.sql
@./tables/ELEM_DEF_EOLICO_TMP.sql
@./tables/ELEM_DEF_EOLICO_TMP_EXT.sql
@./tables/ELEM_DEF_SOLARE_TMP.sql
@./tables/ELEM_DEF_SOLARE_TMP_EXT.sql
@./tables/DEFAULT_CO.sql
@./tables/FILE_PROCESSED.sql
@./tables/FORECAST_PARAMETRI.sql
@./tables/FORECAST_PARAM_GLOBAL.sql
@./tables/FORECAST_PARAM_INTERV_TRAINING.sql
@./tables/GERARCHIA_AMM.sql
@./tables/GERARCHIA_GEO.sql
@./tables/GERARCHIA_IMP_SA.sql
@./tables/GERARCHIA_IMP_SN.sql
@./tables/GRUPPI_MISURA.sql
@./tables/GTTD_CALC_GERARCHIA.sql
@./tables/GTTD_FORECAST_ELEMENTS.sql
@./tables/GTTD_GET_MISURE.sql
@./tables/GTTD_IMPORT_GERARCHIA.sql
@./tables/GTTD_KPI_MISURE.sql
@./tables/GTTD_MISURE.sql
@./tables/GTTD_MIS_SCHEMA_FRONT.sql
@./tables/GTTD_MOD_ASSETTO_RETE_SA.sql
@./tables/GTTD_PARAMETER_CHANGE.sql
@./tables/GTTD_REP_ENERGIA_POTENZA_&ORATYPE.sql
@./tables/GTTD_VALORI_REP.sql
@./tables/GTTD_VALORI_TEMP.sql
@./tables/TRATTAMENTO_ELEMENTI_&ORATYPE.sql
@./tables/KPI_RICHIESTA.sql
@./tables/KPI_ELEMENTI.sql
@./tables/KPI_INDICI_&ORATYPE.sql
@./tables/KPI_PARAMETRI.sql
@./tables/LOG_HISTORY.sql
@./tables/LOG_HISTORY_INFO_&ORATYPE.sql
@./tables/MANUTENZIONE.sql
@./tables/MEASURE_OFFLINE_PARAMETERS.sql
@./tables/METEO_CITTA.sql
@./tables/METEO_FILE_LETTO.sql
@./tables/METEO_JOB.sql
@./tables/METEO_FILE_ZIP.sql
@./tables/METEO_FILE_XML.sql
@./tables/METEO_JOB_RUNTIME_CONFIG.sql
@./tables/METEO_JOB_STATIC_CONFIG.sql
@./tables/METEO_PREVISIONE_&ORATYPE.sql
@./tables/METEO_REL_ISTAT.sql
@./tables/MISURE_ACQUISITE_&ORATYPE.sql
@./tables/MISURE_ACQUISITE_MANUTENZIONE.sql
@./tables/MISURE_ACQUISITE_STATICHE.sql
@./tables/MISURE_AGGREGATE_&ORATYPE.sql
@./tables/MISURE_AGGREGATE_STATICHE.sql
@./tables/MISURE_ERR.sql
@./tables/MISURE_TMP.sql
@./tables/MISURE_TMP_EXT.sql
@./tables/NOWCAST_CENTRALINE.sql
@./tables/OFFSET.sql
@./tables/OLD_KPI_INDICI.sql
@./tables/PRODUTTORI_TMP.sql
@./tables/PRODUTTORI_TMP_EXT.sql
@./tables/PROFILI.sql
@./tables/PROFILI_DEF.sql
@./tables/PROFILI_VAL_&ORATYPE.sql
@./tables/REGISTRO_RES_INVIO.sql
@./tables/REGISTRO_RES_RITARDO.sql
@./tables/REL_ELEMENTI_AMM.sql
@./tables/REL_ELEMENTI_ECP_SA.sql
@./tables/REL_ELEMENTI_ECP_SN.sql
@./tables/REL_ELEMENTI_ECS_SA.sql
@./tables/REL_ELEMENTI_ECS_SN.sql
@./tables/REL_ELEMENTI_GEO.sql
@./tables/REL_ELEMENTO_TIPMIS.sql
@./tables/RUN_STEP.sql
@./tables/SCAGLIONI_DI_POTENZA.sql
@./tables/SCHEDULED_JOBS_DEF.sql
@./tables/SCHEDULED_JOBS_&ORATYPE.sql
@./tables/SCHEDULED_TMP_GEN.sql
@./tables/SCHEDULED_TMP_GME.sql
@./tables/SCHEDULED_TMP_MET.sql
@./tables/SERVIZIO_MAGO.sql
@./tables/SESSION_STATISTICS_&ORATYPE.sql
@./tables/SPV_LIVELLO_ALLARMI.sql
@./tables/SPV_SISTEMA.sql
@./tables/SPV_DIZIONARIO_ALLARMI.sql
@./tables/SPV_REL_SISTEMA_DIZIONARIO.sql
@./tables/SPV_ALLARMI.sql
@./tables/SPV_CATEGORIA.sql
@./tables/SPV_COLLECTOR_INFO.sql
@./tables/SPV_COLLECTOR_MEASURE.sql
@./tables/SPV_CONFIG_NOTIFICA.sql
@./tables/SPV_CONFIG_NOTIFICA_ALARMLIST.sql
@./tables/SPV_DETTAGLIO_PARAMETRI.sql
@./tables/SPV_MACRO.sql
@./tables/SPV_SOGLIE.sql
@./tables/SPV_REL_CATEGORIA_SISTEMA.sql
@./tables/SPV_REL_MACRO_CATEGORIA.sql
@./tables/SPV_REL_SOGLIE_DIZIONARIO.sql
@./tables/SPV_SISTEMA_SAVE.sql
@./tables/SPV_SOGLIE_SAVE.sql
@./tables/STORICO_IMPORT.sql
@./tables/TMP_CONV_TRATT_ELEM.sql
@./tables/TMP_MANUTENZIONE.sql
@./tables/TRT_MIS_LV.sql
@./tables/TRT_MIS_LV_PROFILE.sql
@./tables/TRT_MIS_MV_CLIENTI.sql
@./tables/TRT_MIS_MV_TREND.sql
@./tables/VERSION.sql

PROMPT ==========================================
PROMPT Packages
PROMPT __________________________________________

@./packages/PKG_AGGREGAZIONI.sql
@./packages/PKG_ANAGRAFICHE.sql
@./packages/PKG_ASID_UTL.sql
@./packages/PKG_ELEMENTI.sql
@./packages/PKG_GENERA_FILE_GEO.sql
@./packages/PKG_GERARCHIA_DGF.sql
@./packages/PKG_LOCALIZZA_GEO.sql
@./packages/PKG_LOGS.sql
@./packages/PKG_MAGO.sql
@./packages/PKG_MAGO_CREL.sql
@./packages/PKG_MAGO_DGF.sql
@./packages/PKG_MAGO_UTL.sql
@./packages/PKG_MANUTENZIONE.sql
@./packages/PKG_MISURE_EXT.sql
@./packages/PKG_REPORTS.sql
@./packages/PKG_SCHEDULER.sql
@./packages/PKG_SPV_ANAG_AUI.sql
@./packages/PKG_SPV_LOAD_MIS.sql
@./packages/PKG_SPV_RETE_ELE.sql
@./packages/PKG_SPV_SMILE_GME.sql
@./packages/PKG_SPV_UTL.sql
@./packages/PKG_STATS.sql
@./packages/PKG_TRANSFER_RES.sql
@./packages/PKG_INTEGRST.sql
@./packages/PKG_KPI.sql
@./packages/PKG_METEO.sql
@./packages/PKG_METEO_OLD.sql
@./packages/PKG_MISURE.sql
@./packages/PKG_PROFILI.sql
@./packages/PKG_SPV_ELEMENT.sql
@./packages/PKG_SUPERVISIONE.sql
@./packages/PKG_TRT_MIS.sql

PROMPT ==========================================
PROMPT Types
PROMPT __________________________________________

@./types/T_COD_GEST_OBJ.sql
@./types/T_ELEMAN_OBJ.sql
@./types/T_FILE_INFO_OBJ.sql
@./types/T_KPI_MIS_OBJ.sql
@./types/T_KPI_RICHIESTA_OBJ.sql
@./types/T_MISMETEO_OBJ.sql
@./types/T_MISREQ_OBJ.sql
@./types/T_MISURA_GME_OBJ.sql
@./types/T_PARAM_EOLIC_OBJ.sql
@./types/T_PARAM_INTERV_TRAINING_OBJ.sql
@./types/T_PARAM_PREV_OBJ.sql
@./types/T_PROFILO.sql
@./types/T_ROWGERARCHIA.sql
@./types/T_SISLIV_OBJ.sql
@./types/T_SPVCOLLECTORINFO_OBJ.sql
@./types/T_SPVCOLLECTORMEASURE_OBJ.sql
@./types/T_SPVELEMENT.sql
@./types/T_SPVSOGLIA_OBJ.sql
@./types/T_TRTMISLVPROFILE_OBJ.sql
@./types/T_TRTMISLV_OBJ.sql
@./types/T_TRTMISMVCLI_OBJ.sql
@./types/T_TRTMISMVTRD_OBJ.sql
@./types/T_COD_GEST_ARRAY.sql
@./types/T_FILE_INFO_ARRAY.sql
@./types/T_KPI_MIS_ARRAY.sql
@./types/T_MISMETEO_ARRAY.sql
@./types/T_MISREQ_ARRAY.sql
@./types/T_MISURA_GME_ARRAY.sql
@./types/T_PARAM_EOLIC_ARRAY.sql
@./types/T_PARAM_INTERV_TRAINING_ARRAY.sql
@./types/T_PARAM_PREV_ARRAY.sql
@./types/T_PROFILI.sql
@./types/T_SISLIV_ARRAY.sql
@./types/T_SPVCOLLECTORINFO_ARRAY.sql
@./types/T_SPVCOLLECTORMEASURE_ARRAY.sql
@./types/T_SPVSOGLIA_ARRAY.sql
@./types/T_SPVTABLE.sql
@./types/T_TABGERARCHIA.sql
@./types/T_TRTMISLVPROFILE_ARRAY.sql
@./types/T_TRTMISLV_ARRAY.sql
@./types/T_TRTMISMVCLI_ARRAY.sql
@./types/T_TRTMISMVTRD_ARRAY.sql

PROMPT ==========================================
PROMPT Materialized Views
PROMPT __________________________________________

@./materializedviews/V_SEARCH_ELEMENTS.sql

PROMPT ==========================================
PROMPT Package Bodies
PROMPT __________________________________________

@./packagebodies/PKG_AGGREGAZIONI.sql
@./packagebodies/PKG_ASID_UTL.sql
@./packagebodies/PKG_GERARCHIA_DGF.sql
@./packagebodies/PKG_LOGS.sql
@./packagebodies/PKG_MAGO.sql
@./packagebodies/PKG_MAGO_CREL.sql
@./packagebodies/PKG_MAGO_UTL.sql
@./packagebodies/PKG_MISURE_EXT.sql
@./packagebodies/PKG_SPV_RETE_ELE.sql
@./packagebodies/PKG_STATS.sql
@./packagebodies/PKG_TRANSFER_RES.sql
@./packagebodies/PKG_ELEMENTI.sql
@./packagebodies/PKG_GENERA_FILE_GEO.sql
@./packagebodies/PKG_LOCALIZZA_GEO.sql
@./packagebodies/PKG_MAGO_DGF.sql
@./packagebodies/PKG_SCHEDULER.sql
@./packagebodies/PKG_SPV_ANAG_AUI.sql
@./packagebodies/PKG_ANAGRAFICHE.sql
@./packagebodies/PKG_INTEGRST.sql
@./packagebodies/PKG_KPI.sql
@./packagebodies/PKG_MANUTENZIONE.sql
@./packagebodies/PKG_METEO.sql
@./packagebodies/PKG_METEO_OLD.sql
@./packagebodies/PKG_MISURE.sql
@./packagebodies/PKG_PROFILI.sql
@./packagebodies/PKG_REPORTS.sql
@./packagebodies/PKG_SPV_ELEMENT.sql
@./packagebodies/PKG_SPV_LOAD_MIS.sql
@./packagebodies/PKG_SPV_SMILE_GME.sql
@./packagebodies/PKG_SPV_UTL.sql
@./packagebodies/PKG_SUPERVISIONE.sql
@./packagebodies/PKG_TRT_MIS.sql

PROMPT ==========================================
PROMPT Procedures
PROMPT __________________________________________

@./procedures/GET_METEODISTRIBLIST.sql
@./procedures/SP_STORICO_REGISTRO_INVIO.sql

PROMPT ==========================================
PROMPT Triggers
PROMPT __________________________________________

@./triggers/AFTI_APPLICATION_RUN.sql
@./triggers/AFT_IUR_ELEMENTI_DEF.sql
@./triggers/BEF_IUR_ELEMENTI.sql
@./triggers/BEF_IUR_FORE_PARAM_GLOBAL.sql
@./triggers/BEF_IUR_INTERV_TRAINING.sql
@./triggers/BEF_IUR_PROFILI.sql
@./triggers/BEF_IUR_PROFILI_VAL.sql
@./triggers/BEF_IUR_REL_ELEMENTI_AMM.sql
@./triggers/BEF_IUR_REL_ELEMENTI_ECP_SA.sql
@./triggers/BEF_IUR_REL_ELEMENTI_ECP_SN.sql
@./triggers/BEF_IUR_REL_ELEMENTI_ECS_SA.sql
@./triggers/BEF_IUR_REL_ELEMENTI_ECS_SN.sql
@./triggers/BEF_IUR_REL_ELEMENTI_GEO.sql
@./triggers/BEF_IUR_SCHEDULED_JOBS.sql
@./triggers/BEF_IUR_SESSION_STATISTICS.sql
@./triggers/BEF_IUR_TIPI_MISURA.sql
@./triggers/BEF_IUR_TIPI_MISURA_EXT.sql
@./triggers/BEF_IUR_TRATTAMENTO_ELEMENTI.sql
@./triggers/BEF_SCHEDULED_TMP_GEN.sql
@./triggers/BEF_SCHEDULED_TMP_GME.sql
@./triggers/BEF_SCHEDULED_TMP_MET.sql
@./triggers/FILEINFO_ID_PK.sql
@./triggers/AFTIUR_TMP_MANUTENZIONE.sql
@./triggers/BEF_IUR_KPI_RICHIESTA.sql

PROMPT ==========================================
PROMPT Views
PROMPT __________________________________________

@./views/V_ANAGRAFICA_IMPIANTO.sql
@./views/V_CURRENT_VERSION.sql
@./views/V_ELEMENTI.sql
@./views/V_ELEMENTI_DGF.sql
@./views/V_ESERCIZI.sql
@./views/V_ESERCIZI_ANTEASID.sql
@./views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./views/V_GERARCHIA_IMPIANTO_MT_BT.sql
@./views/V_LOG_HISTORY_MAGO.sql
@./views/V_MAGO_ANAGRAFICA_BASE.sql
@./views/V_MAGO_REL_IMP_AT_MT.sql
@./views/V_MANUTENZIONE_GENERATORI.sql
@./views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./views/V_MOD_ASSETTO_RETE_SA.sql
@./views/V_PARAMETRI_APPLICATIVI.sql
@./views/V_PRODUTTORI_X_RES_ASID2018.sql
@./views/V_PROFILI.sql
@./views/V_PUNTI_GEO.sql
@./views/V_TIPI_MISURA.sql
@./views/V_GERARCHIA_AMMINISTRATIVA.sql
@./views/V_GERARCHIA_GEOGRAFICA.sql
@./views/V_SCHEDULED_JOBS.sql

PROMPT ==========================================
PROMPT Scheduled jobs
PROMPT __________________________________________

@./scheduledjobs/ALLINEA_ANAGRAFICA.sql
@./scheduledjobs/CHECK_SESSION_STATISTICS.sql
@./scheduledjobs/GENERA_FILES_GEO.sql
@./scheduledjobs/MAGO_INS_REQ_AGGREG.sql
@./scheduledjobs/MAGO_INS_REQ_AGG_GME.sql
@./scheduledjobs/MAGO_INS_REQ_AGG_METEO.sql
@./scheduledjobs/MAGO_SCHEDULER.sql
@./scheduledjobs/MAGO_SPV_CLEAN_COLL_MIS.sql
@./scheduledjobs/MAGO_SPV_LOAD_AUI.sql
@./scheduledjobs/MAGO_SPV_LOAD_LOAD_MIS.sql
@./scheduledjobs/MAGO_SPV_LOAD_MEASURE_SMILE.sql
@./scheduledjobs/MAGO_SPV_LOAD_RETE_ELE.sql
@./scheduledjobs/PERIODIC_MAINTENANCE.sql
@./scheduledjobs/SINCRO_TRANSFER_RES.sql
@./scheduledjobs/SINCRO_TRANSFER_RES_R.sql
@./scheduledjobs/STORICO_REGISTRO_INVIO.sql

PROMPT ==========================================
PROMPT Grants mago to
PROMPT __________________________________________

@./grants/Grant_MAGO_TO_POWER_QUALITY.sql
@./grants/Grant_MAGO_TO_PUBLIC.sql

PROMPT ==========================================
PROMPT Build Version: Vers: 1.2.0 - Gestione Delle Connection E Parametri Sar
PROMPT __________________________________________