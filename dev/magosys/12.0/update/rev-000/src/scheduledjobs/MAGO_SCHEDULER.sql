BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'MAGO_SCHEDULER', job_type => 'STORED_PROCEDURE', job_action => 'PKG_Scheduler.ElaboraJobs', number_of_arguments => '0', start_date => to_timestamp_tz('22-10-2015 00:00:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=HOURLY; BYMINUTE=00,10,20,30,40,50', enabled => TRUE, auto_drop => FALSE, comments => 'Esegue i jobs schedulati');
END;
/
