BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'MAGO_SPV_CLEAN_COLL_MIS', job_type => 'STORED_PROCEDURE', job_action => 'PKG_SUPERVISIONE.SPV_CLEANCOLLMEAS', number_of_arguments => '0', start_date => to_timestamp_tz('07-10-2016 23:30:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=DAILY; BYHOUR=1;', enabled => TRUE, auto_drop => TRUE, comments => 'Ripulisce SPC collector mis. dalle misure pi�� vecchie');
END;
/
