BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'GENERA_FILES_GEO', job_type => 'STORED_PROCEDURE', job_action => 'pkg_genera_file_geo.genera_files', number_of_arguments => '0', start_date => to_timestamp_tz('22-10-2015 21:00:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=DAILY; INTERVAL=1', enabled => TRUE, auto_drop => FALSE, comments => 'Eseguo la generazione dei files di anagrafica punti e correlazione');
END;
/
