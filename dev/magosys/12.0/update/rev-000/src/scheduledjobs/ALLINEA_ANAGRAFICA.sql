BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'ALLINEA_ANAGRAFICA', job_type => 'STORED_PROCEDURE', job_action => 'PKG_Anagrafiche.AllineaAnagrafica', number_of_arguments => '0', start_date => NULL, repeat_interval => NULL, enabled => FALSE, auto_drop => FALSE, comments => 'Esegue allineamento anagrafica mago se ci sono stati modifiche su quelle di AUI o di CORELE');
END;
/
