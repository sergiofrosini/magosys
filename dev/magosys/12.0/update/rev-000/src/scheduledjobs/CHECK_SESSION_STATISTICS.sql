BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'CHECK_SESSION_STATISTICS', job_type => 'STORED_PROCEDURE', job_action => 'PKG_Stats.CheckSessions', number_of_arguments => '0', start_date => to_timestamp_tz('22-10-2015 00:00:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=MINUTELY;INTERVAL=1', enabled => TRUE, auto_drop => FALSE, comments => 'Chiude le statistiche di sessione per le quali non sono piu'' presenti elaborazioni');
END;
/
