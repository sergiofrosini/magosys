BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'SINCRO_TRANSFER_RES_R', job_type => 'STORED_PROCEDURE', job_action => 'PKG_TRANSFER_RES.SP_TRANSFER_MIS_R', number_of_arguments => '0', start_date => to_timestamp_tz('20-02-2019 00:00:00.000000 +01:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=HOURLY; BYMINUTE=00,15,30,45;', enabled => TRUE, auto_drop => FALSE, comments => 'Esegue trasferimento dati sistema RIGEDI verso RES nazionale');
END;
/
