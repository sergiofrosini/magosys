BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'MAGO_SPV_LOAD_RETE_ELE', job_type => 'STORED_PROCEDURE', job_action => 'pkg_spv_rete_ele.Elespvmain', number_of_arguments => '0', start_date => to_timestamp_tz('09-03-2017 00:05:00.000000 +01:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=DAILY;INTERVAL=1;BYHOUR=3', enabled => TRUE, auto_drop => FALSE, comments => 'Recupera le misure necessarie per il calcolo della Supervisione del modulo Smile GME');
END;
/
