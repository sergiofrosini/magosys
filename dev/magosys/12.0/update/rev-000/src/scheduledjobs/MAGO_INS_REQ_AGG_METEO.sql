BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'MAGO_INS_REQ_AGG_METEO', job_type => 'STORED_PROCEDURE', job_action => 'PKG_Scheduler.ConsolidaJobAggrMETEO', number_of_arguments => '0', start_date => NULL, repeat_interval => NULL, enabled => FALSE, auto_drop => FALSE, comments => 'Da tabelle temporanee SCHEDULED_TMP_MET inserisce le richieste di aggregazione');
END;
/
