BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'PERIODIC_MAINTENANCE', job_type => 'STORED_PROCEDURE', job_action => 'PKG_Manutenzione.PeriodicMaintenance', number_of_arguments => '0', start_date => to_timestamp_tz('22-10-2015 04:00:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=HOURLY; INTERVAL=6', enabled => TRUE, auto_drop => FALSE, comments => 'Esegue manutenzioni periodiche');
END;
/
