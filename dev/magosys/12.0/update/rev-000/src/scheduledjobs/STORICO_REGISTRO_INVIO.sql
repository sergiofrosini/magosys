BEGIN
DBMS_SCHEDULER.CREATE_JOB( job_name => 'STORICO_REGISTRO_INVIO', job_type => 'STORED_PROCEDURE', job_action => 'PKG_TRANSFER_RES.SP_STORICO_REGISTRO_INVIO', number_of_arguments => '0', start_date => to_timestamp_tz('07-10-2016 23:30:00.000000 +02:00','DD-MM-YYYY HH24:MI:SS.FF TZH:TZM'), repeat_interval => 'FREQ=DAILY', enabled => TRUE, auto_drop => TRUE, comments => 'Esegue la storicizzazione delle procedure in scambio dati ST che hanno tot giorni');
END;
/
