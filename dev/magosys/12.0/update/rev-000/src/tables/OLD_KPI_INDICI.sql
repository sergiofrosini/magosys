CREATE TABLE old_kpi_indici (
   cod_trattamento_elem           NUMBER NOT NULL ,
   risoluzione                    NUMBER NOT NULL ,
   data                           DATE NOT NULL ,
   valore                         NUMBER ,CONSTRAINT old_kpi_indici_pk 
   PRIMARY KEY
 (cod_trattamento_elem, risoluzione, data)
  ENABLE VALIDATE
)
ORGANIZATION INDEX
TABLESPACE MAGO_DATA
PARTITION BY RANGE (DATA)
( PARTITION gen2014 VALUES LESS THAN (TO_DATE(' 2014-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2014 VALUES LESS THAN (TO_DATE(' 2014-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2014 VALUES LESS THAN (TO_DATE(' 2014-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2014 VALUES LESS THAN (TO_DATE(' 2014-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2014 VALUES LESS THAN (TO_DATE(' 2014-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2014 VALUES LESS THAN (TO_DATE(' 2014-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2014 VALUES LESS THAN (TO_DATE(' 2014-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2014 VALUES LESS THAN (TO_DATE(' 2014-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2014 VALUES LESS THAN (TO_DATE(' 2014-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2014 VALUES LESS THAN (TO_DATE(' 2014-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2014 VALUES LESS THAN (TO_DATE(' 2014-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2014 VALUES LESS THAN (TO_DATE(' 2014-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2015 VALUES LESS THAN (TO_DATE(' 2015-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2015 VALUES LESS THAN (TO_DATE(' 2015-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2015 VALUES LESS THAN (TO_DATE(' 2015-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2015 VALUES LESS THAN (TO_DATE(' 2015-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2015 VALUES LESS THAN (TO_DATE(' 2015-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2015 VALUES LESS THAN (TO_DATE(' 2015-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2015 VALUES LESS THAN (TO_DATE(' 2015-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2015 VALUES LESS THAN (TO_DATE(' 2015-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2015 VALUES LESS THAN (TO_DATE(' 2015-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2015 VALUES LESS THAN (TO_DATE(' 2015-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2015 VALUES LESS THAN (TO_DATE(' 2015-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2015 VALUES LESS THAN (TO_DATE(' 2015-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2016 VALUES LESS THAN (TO_DATE(' 2016-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2016 VALUES LESS THAN (TO_DATE(' 2016-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2016 VALUES LESS THAN (TO_DATE(' 2016-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2016 VALUES LESS THAN (TO_DATE(' 2016-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2016 VALUES LESS THAN (TO_DATE(' 2016-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2016 VALUES LESS THAN (TO_DATE(' 2016-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2016 VALUES LESS THAN (TO_DATE(' 2016-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2016 VALUES LESS THAN (TO_DATE(' 2016-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2016 VALUES LESS THAN (TO_DATE(' 2016-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2016 VALUES LESS THAN (TO_DATE(' 2016-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2016 VALUES LESS THAN (TO_DATE(' 2016-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2016 VALUES LESS THAN (TO_DATE(' 2016-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2017 VALUES LESS THAN (TO_DATE(' 2017-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2017 VALUES LESS THAN (TO_DATE(' 2017-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2017 VALUES LESS THAN (TO_DATE(' 2017-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2017 VALUES LESS THAN (TO_DATE(' 2017-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2017 VALUES LESS THAN (TO_DATE(' 2017-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2017 VALUES LESS THAN (TO_DATE(' 2017-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2017 VALUES LESS THAN (TO_DATE(' 2017-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2017 VALUES LESS THAN (TO_DATE(' 2017-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2017 VALUES LESS THAN (TO_DATE(' 2017-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2017 VALUES LESS THAN (TO_DATE(' 2017-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2017 VALUES LESS THAN (TO_DATE(' 2017-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2017 VALUES LESS THAN (TO_DATE(' 2017-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2018 VALUES LESS THAN (TO_DATE(' 2018-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2018 VALUES LESS THAN (TO_DATE(' 2018-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2018 VALUES LESS THAN (TO_DATE(' 2018-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2018 VALUES LESS THAN (TO_DATE(' 2018-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2018 VALUES LESS THAN (TO_DATE(' 2018-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2018 VALUES LESS THAN (TO_DATE(' 2018-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2018 VALUES LESS THAN (TO_DATE(' 2018-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2018 VALUES LESS THAN (TO_DATE(' 2018-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2018 VALUES LESS THAN (TO_DATE(' 2018-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2018 VALUES LESS THAN (TO_DATE(' 2018-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2018 VALUES LESS THAN (TO_DATE(' 2018-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2018 VALUES LESS THAN (TO_DATE(' 2018-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2019 VALUES LESS THAN (TO_DATE(' 2019-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2019 VALUES LESS THAN (TO_DATE(' 2019-02-28 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2019 VALUES LESS THAN (TO_DATE(' 2019-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2019 VALUES LESS THAN (TO_DATE(' 2019-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2019 VALUES LESS THAN (TO_DATE(' 2019-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2019 VALUES LESS THAN (TO_DATE(' 2019-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2019 VALUES LESS THAN (TO_DATE(' 2019-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2019 VALUES LESS THAN (TO_DATE(' 2019-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2019 VALUES LESS THAN (TO_DATE(' 2019-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2019 VALUES LESS THAN (TO_DATE(' 2019-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2019 VALUES LESS THAN (TO_DATE(' 2019-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2019 VALUES LESS THAN (TO_DATE(' 2019-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION gen2020 VALUES LESS THAN (TO_DATE(' 2020-01-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION feb2020 VALUES LESS THAN (TO_DATE(' 2020-02-29 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mar2020 VALUES LESS THAN (TO_DATE(' 2020-03-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION apr2020 VALUES LESS THAN (TO_DATE(' 2020-04-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mag2020 VALUES LESS THAN (TO_DATE(' 2020-05-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION giu2020 VALUES LESS THAN (TO_DATE(' 2020-06-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION lug2020 VALUES LESS THAN (TO_DATE(' 2020-07-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ago2020 VALUES LESS THAN (TO_DATE(' 2020-08-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION set2020 VALUES LESS THAN (TO_DATE(' 2020-09-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION ott2020 VALUES LESS THAN (TO_DATE(' 2020-10-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION nov2020 VALUES LESS THAN (TO_DATE(' 2020-11-30 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION dic2020 VALUES LESS THAN (TO_DATE(' 2020-12-31 23:59:59', 'SYYYY-MM-DD HH24:MI:SS', 'NLS_CALENDAR=GREGORIAN'))
, PARTITION mis_next_all VALUES LESS THAN (MAXVALUE)
);


ALTER TABLE old_kpi_indici ADD CONSTRAINT old_trtele_kpival FOREIGN KEY (cod_trattamento_elem) REFERENCES trattamento_elementi(cod_trattamento_elem) ENABLE;
