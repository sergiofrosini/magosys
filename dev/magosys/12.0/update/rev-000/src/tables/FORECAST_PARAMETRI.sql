CREATE TABLE forecast_parametri (
   cod_elemento                   NUMBER NOT NULL ,
   cod_tipo_fonte                 VARCHAR2(2) NOT NULL ,
   data_ultimo_agg                DATE,
   parametro1                     NUMBER,
   parametro2                     NUMBER,
   parametro3                     NUMBER,
   parametro4                     NUMBER,
   parametro5                     NUMBER,
   parametro6                     NUMBER,
   parametro7                     NUMBER,
   parametro8                     NUMBER,
   parametro9                     NUMBER,
   parametro10                    NUMBER,
   v_cut_in                       NUMBER,
   v_cut_off                      NUMBER,
   v_max_power                    NUMBER,
   cod_tipo_coord                 VARCHAR2(1) DEFAULT 'C'  NOT NULL ,
   cod_prev_meteo                 VARCHAR2(1) DEFAULT 0  NOT NULL ,
   dt_inizio                      DATE NOT NULL ,
   dt_fine                        DATE,
   dt_inizio_calcolo              DATE,
   dt_fine_calcolo                DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX forecast_parametri_pk ON forecast_parametri ( cod_elemento, cod_tipo_fonte, cod_tipo_coord, cod_prev_meteo, dt_inizio ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE forecast_parametri ADD CONSTRAINT ele_forpar FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE forecast_parametri ADD CONSTRAINT forecast_parametri_pk PRIMARY KEY (cod_elemento, cod_tipo_fonte, cod_tipo_coord, cod_prev_meteo, dt_inizio) ENABLE;
