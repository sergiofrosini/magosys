CREATE TABLE scheduled_jobs_def (
   tipo_job                       NUMBER NOT NULL ,
   descrizione                    VARCHAR2(60),
   ritardo_hh                     NUMBER  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX scheduled_jobs_def_pk ON scheduled_jobs_def ( tipo_job ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE scheduled_jobs_def ADD CONSTRAINT scheduled_jobs_def_pk PRIMARY KEY (tipo_job) ENABLE;
