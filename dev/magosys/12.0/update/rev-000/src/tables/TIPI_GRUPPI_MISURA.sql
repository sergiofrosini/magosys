CREATE TABLE tipi_gruppi_misura (
   cod_gruppo                     NUMBER NOT NULL ,
   descrizione                    VARCHAR2(60)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_GRUPPI_MISURA IS 'Definizione dei tipi Gruppi Misura';

COMMENT ON COLUMN TIPI_GRUPPI_MISURA.COD_GRUPPO IS 'Identificativo del Gruppo Misura';
COMMENT ON COLUMN TIPI_GRUPPI_MISURA.DESCRIZIONE IS 'Descrizione del Gruppo Misura';

CREATE UNIQUE INDEX tipi_gruppi_misura_pk ON tipi_gruppi_misura ( cod_gruppo ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_gruppi_misura ADD CONSTRAINT tipi_gruppi_misura_pk PRIMARY KEY (cod_gruppo) ENABLE;
