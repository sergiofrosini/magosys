CREATE TABLE spv_config_notifica (
   id_spvconfignotifica           NUMBER NOT NULL ,
   data_creazione                 DATE,
   nome                           VARCHAR2(100),
   tipo_allegato                  CHAR(5),
   oggetto_mail                   VARCHAR2(200),
   testo_mail                     VARCHAR2(3000),
   mail_list                      VARCHAR2(3000)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_notifiche_pk ON spv_config_notifica ( id_spvconfignotifica ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE spv_config_notifica ADD CONSTRAINT spv_notifiche_pk PRIMARY KEY (id_spvconfignotifica) ENABLE;
