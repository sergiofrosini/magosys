CREATE TABLE file_processed (
   id_file                        NUMBER,
   nome_file                      VARCHAR2(200) NOT NULL ,
   tipo_file                      VARCHAR2(200) NOT NULL ,
   data_elab                      DATE NOT NULL ,
   processo                       VARCHAR2(500) NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX file_processed_pk ON file_processed ( nome_file, tipo_file ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE file_processed ADD CONSTRAINT file_processed_pk PRIMARY KEY (nome_file, tipo_file) ENABLE;
