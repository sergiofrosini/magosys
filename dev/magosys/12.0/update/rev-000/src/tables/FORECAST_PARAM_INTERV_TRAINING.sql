CREATE TABLE forecast_param_interv_training (
   id_interv_training             NUMBER NOT NULL ,
   cod_elemento                   NUMBER NOT NULL ,
   cod_tipo_fonte                 VARCHAR2(2) NOT NULL ,
   cod_tipo_coord                 VARCHAR2(1) DEFAULT 'C'  NOT NULL ,
   cod_prev_meteo                 VARCHAR2(1) DEFAULT 0  NOT NULL ,
   dt_inizio                      DATE,
   dt_fine                        DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.ID_INTERV_TRAINING IS 'ID INTERVALLO ADDESTRAMENTO';
COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.DT_INIZIO IS 'DATA INIZIO INTERVALLO ADDESTRAMENTO';
COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.DT_FINE IS 'DATA FINE INTERVALLO ADDESTRAMENTO';

CREATE UNIQUE INDEX forecast_parameter_interv_t_pk ON forecast_param_interv_training ( id_interv_training, cod_elemento, cod_tipo_fonte, cod_tipo_coord, cod_prev_meteo ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE forecast_param_interv_training ADD CONSTRAINT forecast_parameter_interv_t_pk PRIMARY KEY (id_interv_training, cod_elemento, cod_tipo_fonte, cod_tipo_coord, cod_prev_meteo) ENABLE;
