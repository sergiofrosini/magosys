CREATE TABLE spv_sistema (
   id_sistema                     NUMBER NOT NULL ,
   descrizione                    VARCHAR2(100),
   visible                        NUMBER DEFAULT 0   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN SPV_SISTEMA.ID_SISTEMA IS 'PK SISTEMA';
COMMENT ON COLUMN SPV_SISTEMA.DESCRIZIONE IS 'Descrizione dei sistema  della supervisione (METEO, SMILE, MAGO ....';

CREATE UNIQUE INDEX spv_sistema_pk ON spv_sistema ( id_sistema ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_sistema ADD CONSTRAINT sys_c0062641 PRIMARY KEY (id_sistema) ENABLE;
