CREATE TABLE trattamento_elementi (
   cod_trattamento_elem           NUMBER NOT NULL ,
   cod_elemento                   NUMBER,
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_misura                VARCHAR2(6),
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_rete                  CHAR(1),
   cod_tipo_cliente               CHAR(1),
   organizzazione                 NUMBER(1,0),
   tipo_aggregazione              NUMBER(1,0)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM IS 'PK del trattamento elemento';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_ELEMENTO IS 'Codice elemento di riferimento';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_ELEMENTO IS 'Codice tipo elemento';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_MISURA IS 'Codice tipo misura';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_FONTE IS 'Codice tipo fonte';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_RETE IS 'Codice tipo rete  (A=AT,  M=MT,  B=BT)';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.COD_TIPO_CLIENTE IS 'Codice Tipo Cliente';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.ORGANIZZAZIONE IS 'Organizzazione base di riferimento: 1=Elettrica, 2=Geografica, 3=Amministrativa (NB. gli elementi di CS fanno sempre tutti parte dell''organizzazione elettrica)';
COMMENT ON COLUMN TRATTAMENTO_ELEMENTI.TIPO_AGGREGAZIONE IS 'E'' lo Stato Rete - 0=Mis.Acquisita, 1=Aggr.SN, 2=Aggr.SA, 3=KPI - Per Elementi di CS sempre = 1';

CREATE UNIQUE INDEX trattamento_elementi_ak1 ON trattamento_elementi ( cod_elemento, cod_tipo_misura, cod_tipo_fonte, cod_tipo_rete, cod_tipo_cliente, organizzazione, tipo_aggregazione ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX trattamento_elementi_if0 ON trattamento_elementi ( cod_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX trattamento_elementi_if1 ON trattamento_elementi ( cod_tipo_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX trattamento_elementi_if2 ON trattamento_elementi ( cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX trattamento_elementi_if3 ON trattamento_elementi ( cod_tipo_fonte ) LOGGING TABLESPACE MAGO_IDX;
CREATE BITMAP INDEX trattamento_elementi_if4 ON trattamento_elementi ( cod_tipo_rete ) LOGGING TABLESPACE MAGO_IDX;
CREATE BITMAP INDEX trattamento_elementi_if5 ON trattamento_elementi ( cod_tipo_cliente ) LOGGING TABLESPACE MAGO_IDX;
CREATE BITMAP INDEX trattamento_elementi_if6 ON trattamento_elementi ( organizzazione ) LOGGING TABLESPACE MAGO_IDX;
CREATE BITMAP INDEX trattamento_elementi_if7 ON trattamento_elementi ( tipo_aggregazione ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX trattamento_elementi_pk ON trattamento_elementi ( cod_trattamento_elem ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE trattamento_elementi ADD CONSTRAINT ele_trtele FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT tpcli_trtele FOREIGN KEY (cod_tipo_cliente) REFERENCES tipi_cliente(cod_tipo_cliente) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT tpele_trtele FOREIGN KEY (cod_tipo_elemento) REFERENCES tipi_elemento(cod_tipo_elemento) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT tpfon_trtele FOREIGN KEY (cod_tipo_fonte) REFERENCES tipo_fonti(cod_tipo_fonte) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT tpmis_trtele FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT tpret_trtele FOREIGN KEY (cod_tipo_rete) REFERENCES tipi_rete(cod_tipo_rete) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT trattamento_elementi_c01 CHECK (ORGANIZZAZIONE IN (1,2,3)) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT trattamento_elementi_c02 CHECK (TIPO_AGGREGAZIONE IN (0,1,2,3)) ENABLE;
ALTER TABLE trattamento_elementi ADD CONSTRAINT trattamento_elementi_pk PRIMARY KEY (cod_trattamento_elem) ENABLE;
