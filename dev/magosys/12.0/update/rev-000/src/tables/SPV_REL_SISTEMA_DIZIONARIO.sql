CREATE TABLE spv_rel_sistema_dizionario (
   id_rel_sistema_dizionario      NUMBER NOT NULL ,
   id_sistema                     NUMBER NOT NULL ,
   id_dizionario_allarme          NUMBER NOT NULL   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE SPV_REL_SISTEMA_DIZIONARIO IS 'Tabella di relazione tra i valori di Sistema e il dizionario (allarmi)';

COMMENT ON COLUMN SPV_REL_SISTEMA_DIZIONARIO.ID_REL_SISTEMA_DIZIONARIO IS 'PK REL_SISTEMA_DIZIONARIO; ID GENERATO SARA utilizzato nella tabella ALLARMI';

CREATE UNIQUE INDEX spv_rel_sistema_dizionario_pk ON spv_rel_sistema_dizionario ( id_rel_sistema_dizionario ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_rel_sistema_dizionario ADD CONSTRAINT fk_rel_f1 FOREIGN KEY (id_sistema) REFERENCES spv_sistema(id_sistema) ENABLE;
ALTER TABLE spv_rel_sistema_dizionario ADD CONSTRAINT fk_rel_f2 FOREIGN KEY (id_dizionario_allarme) REFERENCES spv_dizionario_allarmi(id_dizionario_allarme) ENABLE;
ALTER TABLE spv_rel_sistema_dizionario ADD CONSTRAINT sys_c0062645 PRIMARY KEY (id_rel_sistema_dizionario) ENABLE;
