CREATE TABLE spv_dettaglio_parametri (
   id_sistema                     NUMBER NOT NULL ,
   id_chiave                      VARCHAR2(100) NOT NULL ,
   valore                         VARCHAR2(4000),
   valore_blob                    BLOB  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE SPV_DETTAGLIO_PARAMETRI IS 'Tabella dei parmatri del sistema (utilizzati sul FE)';

COMMENT ON COLUMN SPV_DETTAGLIO_PARAMETRI.ID_SISTEMA IS 'ID del sistema ';
COMMENT ON COLUMN SPV_DETTAGLIO_PARAMETRI.ID_CHIAVE IS 'ID del campo da visualizzare sul FE ';
COMMENT ON COLUMN SPV_DETTAGLIO_PARAMETRI.VALORE IS 'valore del campo da visualizzare sul FE ';

CREATE UNIQUE INDEX spv_dettaglio_parametri_pk ON spv_dettaglio_parametri ( id_sistema, id_chiave ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_dettaglio_parametri ADD CONSTRAINT fk_dettaglio_f1 FOREIGN KEY (id_sistema) REFERENCES spv_sistema(id_sistema) ENABLE;
ALTER TABLE spv_dettaglio_parametri ADD CONSTRAINT sys_c0062669 PRIMARY KEY (id_sistema, id_chiave) ENABLE;
