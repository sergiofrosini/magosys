CREATE TABLE application_run (
   run_id                         NUMBER,
   proc_name                      VARCHAR2(50),
   run_flg                        NUMBER(1,0),
   err_flg                        NUMBER(1,0),
   last_upd                       DATE  ) 
 TABLESPACE MAGO_DATA;


