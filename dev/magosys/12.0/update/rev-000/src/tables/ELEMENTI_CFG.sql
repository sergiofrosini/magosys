CREATE TABLE elementi_cfg (
   cod_elemento                   NUMBER NOT NULL ,
   flg_pi                         NUMBER,
   flg_pg                         NUMBER,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN ELEMENTI_CFG.FLG_PI IS 'Interruttore distacco generazione';
COMMENT ON COLUMN ELEMENTI_CFG.FLG_PG IS 'Interruttore distacco generazione e carico';
COMMENT ON COLUMN ELEMENTI_CFG.DATA_ATTIVAZIONE IS 'data inizio validita''';
COMMENT ON COLUMN ELEMENTI_CFG.DATA_DISATTIVAZIONE IS 'data fine validita''';

CREATE UNIQUE INDEX elementi_cfg_pk ON elementi_cfg ( cod_elemento, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE elementi_cfg ADD CONSTRAINT elementi_cfg_pk PRIMARY KEY (cod_elemento, data_attivazione) ENABLE;
ALTER TABLE elementi_cfg ADD CONSTRAINT ele_elecfg FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
