CREATE TABLE rel_elementi_geo (
   cod_elemento_padre             NUMBER NOT NULL ,
   cod_elemento_figlio            NUMBER NOT NULL ,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE DEFAULT TO_DATE('01013000','ddmmyyyy') ,
   data_disattivazione_undisconn  DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE REL_ELEMENTI_GEO IS 'Gerarchia Geografica (Istat)';

COMMENT ON COLUMN REL_ELEMENTI_GEO.COD_ELEMENTO_PADRE IS 'Codice elemento Padre';
COMMENT ON COLUMN REL_ELEMENTI_GEO.COD_ELEMENTO_FIGLIO IS 'Codice elemento Figlio';
COMMENT ON COLUMN REL_ELEMENTI_GEO.DATA_ATTIVAZIONE IS 'data inizio validita''';
COMMENT ON COLUMN REL_ELEMENTI_GEO.DATA_DISATTIVAZIONE IS 'data fine validita''';
COMMENT ON COLUMN REL_ELEMENTI_GEO.DATA_DISATTIVAZIONE_UNDISCONN IS 'Data di disattivazione senza buchi di inattivita''';

CREATE  INDEX rel_ele_geo_if1 ON rel_elementi_geo ( cod_elemento_figlio ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX rel_ele_geo_if2 ON rel_elementi_geo ( cod_elemento_padre ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX rel_ele_geo_pk ON rel_elementi_geo ( cod_elemento_padre, cod_elemento_figlio, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE rel_elementi_geo ADD CONSTRAINT ele_relgeo__f FOREIGN KEY (cod_elemento_figlio) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_geo ADD CONSTRAINT ele_relgeo__p FOREIGN KEY (cod_elemento_padre) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_geo ADD CONSTRAINT rel_ele_geo_pk PRIMARY KEY (cod_elemento_padre, cod_elemento_figlio, data_attivazione) ENABLE;
