CREATE TABLE spv_collector_measure (
   id_measure                     NUMBER NOT NULL ,
   collector_name                 VARCHAR2(20),
   data_inserimento               DATE DEFAULT SYSDATE ,
   data_misura                    DATE,
   cod_gest_elemento              VARCHAR2(20),
   cod_tipo_misura                VARCHAR2(6),
   cod_tipo_fonte                 VARCHAR2(20),
   valore                         NUMBER(20,4)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_collector_measure_pk ON spv_collector_measure ( id_measure ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX spv_coll_meas_idx ON spv_collector_measure ( collector_name, cod_gest_elemento, data_misura, cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_collector_measure ADD CONSTRAINT spv_collector_measure_pk PRIMARY KEY (id_measure) ENABLE;
