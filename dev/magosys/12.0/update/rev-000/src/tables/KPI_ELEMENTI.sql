CREATE TABLE kpi_elementi (
   id_req_kpi                     NUMBER NOT NULL ,
   cod_elemento                   NUMBER NOT NULL ,
   stato                          NUMBER(1,0) DEFAULT 0 ,
   keytype                        VARCHAR2(5) NOT NULL  ,CONSTRAINT kpi_elementi_pk PRIMARY KEY (id_req_kpi, keytype, cod_elemento)  ) 
 ORGANIZATION INDEX  TABLESPACE MAGO_DATA;

COMMENT ON TABLE KPI_ELEMENTI IS 'Elementi (Produttori - Trasf MT/BT)  della richiesta KPI';

COMMENT ON COLUMN KPI_ELEMENTI.ID_REQ_KPI IS 'Codice identificativo della richiesta';
COMMENT ON COLUMN KPI_ELEMENTI.COD_ELEMENTO IS 'Codice dell''elemento compreso nella richiesta';
COMMENT ON COLUMN KPI_ELEMENTI.STATO IS '0=Da elaborare, 1=Elaborato';
COMMENT ON COLUMN KPI_ELEMENTI.KEYTYPE IS 'SEL=Elemento selezionato da gerarchia, POP=Elemento selezionato tramite popup';


ALTER TABLE kpi_elementi ADD CONSTRAINT kpi_ele_kpi_req FOREIGN KEY (id_req_kpi) REFERENCES kpi_richiesta(id_req_kpi)  ON DELETE CASCADE ENABLE;
