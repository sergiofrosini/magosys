CREATE GLOBAL TEMPORARY TABLE gttd_import_gerarchia (
   cod_gest_elemento              VARCHAR2(20),
   cod_gest_figlio                VARCHAR2(20),
   nome_elemento                  VARCHAR2(60),
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_cliente               CHAR(1),
   id_elemento                    VARCHAR2(20),
   rif_elemento                   VARCHAR2(20),
   flag                           NUMBER(1,0),
   num_impianti                   NUMBER,
   potenza_installata             NUMBER,
   potenza_contrattuale           NUMBER,
   fattore                        NUMBER,
   coordinata_x                   NUMBER,
   coordinata_y                   NUMBER  ) 
 ON COMMIT PRESERVE ROWS ;


