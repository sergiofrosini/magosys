CREATE TABLE gerarchia_imp_sa (
   cod_elemento                   NUMBER NOT NULL ,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE NOT NULL ,
   l01                            NUMBER NOT NULL ,
   l02                            NUMBER,
   l03                            NUMBER,
   l04                            NUMBER,
   l05                            NUMBER,
   l06                            NUMBER,
   l07                            NUMBER,
   l08                            NUMBER,
   l09                            NUMBER,
   l10                            NUMBER,
   l11                            NUMBER,
   l12                            NUMBER,
   l13                            NUMBER,
   data_disattivazione_undisconn  DATE,
   l14                            NUMBER,
   l15                            NUMBER,
   l16                            NUMBER,
   l17                            NUMBER,
   l18                            NUMBER,
   l19                            NUMBER,
   l20                            NUMBER,
   l21                            NUMBER,
   l22                            NUMBER,
   l23                            NUMBER,
   l24                            NUMBER,
   l25                            NUMBER  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN GERARCHIA_IMP_SA.COD_ELEMENTO IS 'Codice dell''elemento';
COMMENT ON COLUMN GERARCHIA_IMP_SA.DATA_ATTIVAZIONE IS 'data di inizio validita''';
COMMENT ON COLUMN GERARCHIA_IMP_SA.DATA_DISATTIVAZIONE IS 'data di fine validita''';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L01 IS 'livello geararchico 1';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L02 IS 'livello geararchico 2';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L03 IS 'livello geararchico 3';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L04 IS 'livello geararchico 4';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L05 IS 'livello geararchico 5';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L06 IS 'livello geararchico 6';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L07 IS 'livello geararchico 7';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L08 IS 'livello geararchico 8';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L09 IS 'livello geararchico 9';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L10 IS 'livello geararchico 10';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L11 IS 'livello geararchico 11';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L12 IS 'livello geararchico 12';
COMMENT ON COLUMN GERARCHIA_IMP_SA.L13 IS 'livello geararchico 13';
COMMENT ON COLUMN GERARCHIA_IMP_SA.DATA_DISATTIVAZIONE_UNDISCONN IS 'Data di disattivazione senza buchi di inattivita''';

CREATE UNIQUE INDEX gerarchia_imp_sa_pk ON gerarchia_imp_sa ( cod_elemento, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX gerarchia_imp_sa_sn_dt1 ON gerarchia_imp_sa ( data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE gerarchia_imp_sa ADD CONSTRAINT ele_gerimpsa FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE gerarchia_imp_sa ADD CONSTRAINT gerarchia_imp_sa_pk PRIMARY KEY (cod_elemento, data_attivazione) ENABLE;
