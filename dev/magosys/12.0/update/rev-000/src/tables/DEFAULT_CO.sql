CREATE TABLE default_co (
   cod_elemento_co                NUMBER,
   cod_elemento_ese               NUMBER,
   tipo_inst                      NUMBER(3,0) NOT NULL ,
   flag_primario                  NUMBER(1,0) DEFAULT 0 ,
   start_date                     DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE DEFAULT_CO IS 'Defaults Applicativi';

COMMENT ON COLUMN DEFAULT_CO.COD_ELEMENTO_CO IS 'Centro operarivo';
COMMENT ON COLUMN DEFAULT_CO.COD_ELEMENTO_ESE IS 'Esercizio';
COMMENT ON COLUMN DEFAULT_CO.TIPO_INST IS 'Utilizzare la logica a Bit: 1=Nazionale, 2=STM, 3=STMnaz, 4=DGF, 5=DGFnaz';
COMMENT ON COLUMN DEFAULT_CO.FLAG_PRIMARIO IS '1=Esercizio Primario - Valorizzare anche se presente un solo esercizio';
COMMENT ON COLUMN DEFAULT_CO.START_DATE IS 'Data prima di riverimento del promo caricamente anagrafiche';


ALTER TABLE default_co ADD CONSTRAINT ele_defco FOREIGN KEY (cod_elemento_co) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE default_co ADD CONSTRAINT ele_defese FOREIGN KEY (cod_elemento_ese) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE default_co ADD CONSTRAINT flag_primario_chk CHECK (FLAG_PRIMARIO IN (0,1)) ENABLE;
