CREATE TABLE kpi_richiesta (
   id_req_kpi                     NUMBER NOT NULL ,
   data_richiesta                 DATE,
   data_scadenza                  DATE,
   operatore                      VARCHAR2(20),
   periodo_inizio                 DATE,
   periodo_fine                   DATE,
   orario_inizio                  CHAR(5),
   orario_fine                    CHAR(5),
   risoluzione                    NUMBER,
   organizzazione                 NUMBER,
   stato_rete                     NUMBER,
   lista_fonti                    VARCHAR2(20),
   lista_tipi_rete                VARCHAR2(20),
   lista_tipi_clie                VARCHAR2(20),
   lista_indici_rich              VARCHAR2(200),
   lista_tipi_misura              VARCHAR2(100),
   lista_tipi_misura_forecast     VARCHAR2(100),
   freq_err_val_ini               NUMBER,
   freq_err_val_ini_perc          NUMBER,
   freq_err_val_fin               NUMBER,
   freq_err_val_fin_perc          NUMBER,
   freq_err_passo                 NUMBER,
   freq_err_passo_perc            NUMBER,
   soglia_colore                  NUMBER,
   soglia_colore_perc             NUMBER,
   valore_soglia                  NUMBER,
   delta                          NUMBER,
   tipo_notifica                  NUMBER,
   elab_inizio                    DATE,
   elab_fine                      DATE,
   elab_stato                     NUMBER,
   data_stato                     DATE,
   oper_stato                     VARCHAR2(20),
   elab_note                      VARCHAR2(4000)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE KPI_RICHIESTA IS 'Testata richiesta KPI - indici di errore';

COMMENT ON COLUMN KPI_RICHIESTA.ID_REQ_KPI IS 'Codice identificativo della richiesta';
COMMENT ON COLUMN KPI_RICHIESTA.DATA_RICHIESTA IS 'Data/Ora della richiesta';
COMMENT ON COLUMN KPI_RICHIESTA.DATA_SCADENZA IS 'Data di scadenza delle richiesta';
COMMENT ON COLUMN KPI_RICHIESTA.OPERATORE IS 'Operatore richiedente';
COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_INIZIO IS 'Inizio periodo temporale selezionato';
COMMENT ON COLUMN KPI_RICHIESTA.PERIODO_FINE IS 'Fine periodo temporale selezionato';
COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_INIZIO IS 'Orario di inizio (alba) - Formato HH:MI';
COMMENT ON COLUMN KPI_RICHIESTA.ORARIO_FINE IS 'Orario di fine (tramonto) - Formato HH:MI';
COMMENT ON COLUMN KPI_RICHIESTA.RISOLUZIONE IS 'Risoluzione temporale dell''analisi (minuti)';
COMMENT ON COLUMN KPI_RICHIESTA.ORGANIZZAZIONE IS 'Organizzazione selezionata';
COMMENT ON COLUMN KPI_RICHIESTA.STATO_RETE IS 'Stato della rete selezionata';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_FONTI IS 'Elenco delle fonti richieste (come ricevute in fase di inserimento)';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_RETE IS 'Elenco dei tipi rete richiesti (come ricevuti in fase di inserimento)';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_CLIE IS 'Elenco dei tipi cliente richiesti (come ricevuti in fase di inserimento)';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_INDICI_RICH IS 'Elenco degli indici da calcolare (come ricevuti in fase di inserimento)';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA IS 'Elencio dei tipi Misura Base';
COMMENT ON COLUMN KPI_RICHIESTA.LISTA_TIPI_MISURA_FORECAST IS 'Elencio dei tipi Misura di Forecast';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_INI IS 'Frequenza di errore - valore iniziale';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_INI_PERC IS 'Frequenza di errore - valore iniziale - percentuale';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_FIN IS 'Frequenza di errore - valore finale';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_VAL_FIN_PERC IS 'Frequenza di errore - valore finale - percentuale';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_PASSO IS 'Frequenza di errore - passo';
COMMENT ON COLUMN KPI_RICHIESTA.FREQ_ERR_PASSO_PERC IS 'Frequenza di errore - passo - percentuale';
COMMENT ON COLUMN KPI_RICHIESTA.SOGLIA_COLORE IS 'Soglia per Colorazione FE';
COMMENT ON COLUMN KPI_RICHIESTA.SOGLIA_COLORE_PERC IS 'Soglia per Colorazione FE - percentuale';
COMMENT ON COLUMN KPI_RICHIESTA.VALORE_SOGLIA IS 'Valore di soglia';
COMMENT ON COLUMN KPI_RICHIESTA.DELTA IS 'Delta';
COMMENT ON COLUMN KPI_RICHIESTA.ELAB_INIZIO IS 'Ora Inizio Elaborazione';
COMMENT ON COLUMN KPI_RICHIESTA.ELAB_FINE IS 'Ora Fine Elaborazione';
COMMENT ON COLUMN KPI_RICHIESTA.ELAB_STATO IS 'Stato avanzamento della richiesta; 0=da elaborare,1=elab. in corso,2=elaborazione conclusa, 3=elaborazione interrotta,-1=errore di elaborazione (dettaglio in ELAB_NOTE)';
COMMENT ON COLUMN KPI_RICHIESTA.DATA_STATO IS 'data del cambio di stato';
COMMENT ON COLUMN KPI_RICHIESTA.OPER_STATO IS 'Operatore/Funzione che ha determinato il cambio di stato';
COMMENT ON COLUMN KPI_RICHIESTA.ELAB_NOTE IS 'Note sull''elaborazione';

CREATE UNIQUE INDEX kpi_richiesta_pk ON kpi_richiesta ( id_req_kpi ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE kpi_richiesta ADD CONSTRAINT kpi_richiesta_pk PRIMARY KEY (id_req_kpi) ENABLE;
