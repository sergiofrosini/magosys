CREATE TABLE offset (
   offset_start                   NUMBER,
   offset_stop                    NUMBER,
   num                            NUMBER  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE OFFSET IS 'Tabella temporanea per calcolo range temporale misure';

COMMENT ON COLUMN OFFSET.OFFSET_START IS 'Range di minuti iniziale';
COMMENT ON COLUMN OFFSET.OFFSET_STOP IS 'Range di minuti finale';
COMMENT ON COLUMN OFFSET.NUM IS 'Identificativo del range temporale';


