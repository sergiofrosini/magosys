CREATE TABLE tipi_misura_conv_orig (
   origine                        VARCHAR2(8),
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_misura_in             VARCHAR2(6),
   cod_tipo_misura_out            VARCHAR2(6),
   formula                        VARCHAR2(200),
   flg_split                      NUMBER(1,0) DEFAULT 1   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX tipi_misura_conv_orig_uk1 ON tipi_misura_conv_orig ( origine, cod_tipo_misura_in, cod_tipo_elemento ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_misura_conv_orig ADD CONSTRAINT tpele_misorig FOREIGN KEY (cod_tipo_elemento) REFERENCES tipi_elemento(cod_tipo_elemento) ENABLE;
ALTER TABLE tipi_misura_conv_orig ADD CONSTRAINT tpmis_misorig__in FOREIGN KEY (cod_tipo_misura_in) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
ALTER TABLE tipi_misura_conv_orig ADD CONSTRAINT tpmis_misorig__out FOREIGN KEY (cod_tipo_misura_out) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
