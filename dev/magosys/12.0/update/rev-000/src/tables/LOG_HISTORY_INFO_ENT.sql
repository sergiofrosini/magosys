CREATE TABLE log_history_info (
   run_id                         NUMBER,
   codice                         VARCHAR2(100),
   tipo                           VARCHAR2(100),
   nome                           VARCHAR2(100),
   seq                            NUMBER DEFAULT 0   ) 
TABLESPACE mago_data
PARTITION BY HASH (RUN_ID)
PARTITIONS 1 
;


