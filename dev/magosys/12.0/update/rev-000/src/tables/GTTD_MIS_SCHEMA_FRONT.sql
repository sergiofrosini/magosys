CREATE GLOBAL TEMPORARY TABLE gttd_mis_schema_front (
   sg_cod                         VARCHAR2(50) NOT NULL ,
   cod_tipo_elemento              VARCHAR2(3) NOT NULL ,
   cod_tipo_misura                VARCHAR2(6) NOT NULL ,
   cod_tipo_fonte                 VARCHAR2(2) NOT NULL ,
   cod_tipo_rete                  CHAR(1),
   cod_tipo_cliente               CHAR(1),
   categoria_prod_id              NUMBER,
   stato_produttore_id            VARCHAR2(15),
   organizzazione                 NUMBER(1,0),
   tipo_aggregazione              NUMBER(1,0),
   sistema                        NUMBER,
   data_misura                    TIMESTAMP(3) NOT NULL ,
   valore_misura                  NUMBER NOT NULL ,
   scaglione_pot                  NUMBER NOT NULL ,
   numero_prod                    NUMBER NOT NULL ,
   flg_stato_record               VARCHAR2(1) NOT NULL ,
   log_errore_elaborazione        VARCHAR2(100),
   data_ricezione                 TIMESTAMP(3) NOT NULL   ) 
 ON COMMIT DELETE ROWS ;

COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.SG_COD IS 'Codice CO di riferimento';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.COD_TIPO_ELEMENTO IS 'Codice tipo elemento';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.COD_TIPO_MISURA IS 'Codice tipo misura';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.COD_TIPO_FONTE IS 'Codice tipo fonte';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.COD_TIPO_RETE IS 'Codice tipo rete  (A=AT,  M=MT,  B=BT)';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.COD_TIPO_CLIENTE IS 'Codice Tipo Cliente (vedi Tipo Rete)';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.STATO_PRODUTTORE_ID IS 'Stao del produttore di Rigedi (1 da commisionare; 2 Rigedi; 3 Rigedi Spspeo;...  ';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.ORGANIZZAZIONE IS 'Organizzazione base di riferimento: 1=Elettrica, 2=Geografica, 3=Amministrativa (NB. gli elementi di CS fanno sempre tutti parte dell organizzazione elettrica)';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.TIPO_AGGREGAZIONE IS 'Stato Rete - 0=Mis.Acquisita, 1=Aggr.SN, 2=Aggr.SA, 3=KPI - Per Elementi sistema Rigedi non significativa (default = 1)';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.SISTEMA IS '1=Rigedi, 2=Mago';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.DATA_MISURA IS 'Data della misura che arriva dal campo';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.VALORE_MISURA IS 'Valore della misura che arriva dal campo';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.SCAGLIONE_POT IS 'Scaglione delle potenze dei produttori  400Kw; 400Kw aa 1000Kw; oltre 1000kw ';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.NUMERO_PROD IS 'Numero dei produttori che arriva dal campo';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.FLG_STATO_RECORD IS 'FLG che identifica lo stato del RECORD R- Ricevuto; P - Processato ; E - Errore (se per qualche motivo il record non processato) ';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.LOG_ERRORE_ELABORAZIONE IS ' Messaggio In caso di errore di elaborazione ';
COMMENT ON COLUMN GTTD_MIS_SCHEMA_FRONT.DATA_RICEZIONE IS 'Data ricezione nel sistema RES';


ALTER TABLE gttd_mis_schema_front ADD CONSTRAINT mis_schema_front_c01 CHECK (ORGANIZZAZIONE IN (1,2,3)) ENABLE;
ALTER TABLE gttd_mis_schema_front ADD CONSTRAINT mis_schema_front_c02 CHECK (TIPO_AGGREGAZIONE IN (0,1,2,3)) ENABLE;
ALTER TABLE gttd_mis_schema_front ADD CONSTRAINT mis_schema_front_c03 CHECK (SISTEMA IN (1,2)) ENABLE;
ALTER TABLE gttd_mis_schema_front ADD CONSTRAINT mis_schema_front_c05 CHECK (FLG_STATO_RECORD IN ('R','P','E')) ENABLE;
