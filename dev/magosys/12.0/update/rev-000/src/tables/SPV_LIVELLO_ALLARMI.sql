CREATE TABLE spv_livello_allarmi (
   id_livello                     NUMBER NOT NULL ,
   descrizione                    VARCHAR2(30),
   nota                           VARCHAR2(150)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_livello_allarmi_pk ON spv_livello_allarmi ( id_livello ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_livello_allarmi ADD CONSTRAINT sys_c0062648 PRIMARY KEY (id_livello) ENABLE;
