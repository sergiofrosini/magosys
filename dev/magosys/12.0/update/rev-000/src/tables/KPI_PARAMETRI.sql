CREATE TABLE kpi_parametri (
   id_req_kpi                     NUMBER NOT NULL ,
   tipo                           VARCHAR2(10) NOT NULL ,
   parametro                      VARCHAR2(50) NOT NULL  ,CONSTRAINT kpi_parametri_pk PRIMARY KEY (id_req_kpi, tipo, parametro)  ) 
 ORGANIZATION INDEX  TABLESPACE MAGO_IDX;

COMMENT ON TABLE KPI_PARAMETRI IS 'Parametri della richiesta KPI';

COMMENT ON COLUMN KPI_PARAMETRI.ID_REQ_KPI IS 'Codice identificativo della richiesta';
COMMENT ON COLUMN KPI_PARAMETRI.TIPO IS 'Tipo parametro (Tipo Rete, Tipo Cliente, Tipo Fonte, Tipo Indice)';
COMMENT ON COLUMN KPI_PARAMETRI.PARAMETRO IS 'Valore del parametro';


ALTER TABLE kpi_parametri ADD CONSTRAINT kpi_param_kpi_req FOREIGN KEY (id_req_kpi) REFERENCES kpi_richiesta(id_req_kpi)  ON DELETE CASCADE ENABLE;
