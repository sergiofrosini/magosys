CREATE TABLE meteo_job_runtime_config (
   job_id                         VARCHAR2(50) NOT NULL ,
   start_date                     DATE,
   end_date                       DATE,
   enabled                        NUMBER(1,0),
   force_install                  NUMBER(1,0),
   last_forec_date                DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_job_runtime_config_pk ON meteo_job_runtime_config ( job_id ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_job_runtime_config ADD CONSTRAINT meteo_job_runtime_config_pk PRIMARY KEY (job_id) ENABLE;
