CREATE TABLE tipo_fonti (
   cod_tipo_fonte                 VARCHAR2(2) NOT NULL ,
   descrizione                    VARCHAR2(60),
   cod_raggr_fonte                VARCHAR2(2),
   flg_apply_filter               NUMBER(1,0) DEFAULT 0  ,
   cod_raggr_fonte_alfa_rif       VARCHAR2(2)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPO_FONTI IS 'Tabella dei tipi Fonti di Produzione previste';

COMMENT ON COLUMN TIPO_FONTI.COD_TIPO_FONTE IS 'Codice Tipo Fonte';
COMMENT ON COLUMN TIPO_FONTI.DESCRIZIONE IS 'Descrizione del Tipo Fonte';
COMMENT ON COLUMN TIPO_FONTI.COD_RAGGR_FONTE IS 'Idefntificativo del Raggruppamento Fonte';

CREATE UNIQUE INDEX tipi_fonte_pk ON tipo_fonti ( cod_tipo_fonte ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipo_fonti ADD CONSTRAINT tipo_fonti_pk PRIMARY KEY (cod_tipo_fonte) ENABLE;
ALTER TABLE tipo_fonti ADD CONSTRAINT tpfon_ragfon FOREIGN KEY (cod_raggr_fonte) REFERENCES raggruppamento_fonti(cod_raggr_fonte) ENABLE;
