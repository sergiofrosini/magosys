CREATE TABLE trt_mis_mv_trend (
   ind_lcurve                     NUMBER NOT NULL ,
   stagione                       NUMBER NOT NULL ,
   giorno                         NUMBER NOT NULL ,
   campione                       NUMBER NOT NULL ,
   tipo_potenza                   VARCHAR2(1) NOT NULL ,
   valore                         NUMBER(20,9)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX trt_mis_mv_trend_pk ON trt_mis_mv_trend ( ind_lcurve, stagione, giorno, campione, tipo_potenza ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE trt_mis_mv_trend ADD CONSTRAINT trt_mis_mv_trend_pk PRIMARY KEY (ind_lcurve, stagione, giorno, campione, tipo_potenza) ENABLE;
