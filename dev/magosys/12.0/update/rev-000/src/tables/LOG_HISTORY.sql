CREATE TABLE log_history (
   run_id                         NUMBER,
   logdate                        DATE,
   proc_name                      VARCHAR2(50),
   message                        VARCHAR2(4000),
   error_num                      NUMBER,
   data_rif                       DATE,
   data_rif_to                    DATE,
   classe                         VARCHAR2(20),
   elapsed                        INTERVAL DAY(2) TO SECOND(6)  ) 
 TABLESPACE MAGO_DATA;


