CREATE TABLE trt_mis_lv (
   colnumber                      NUMBER,
   auinumber                      NUMBER,
   transfnumbernode               NUMBER,
   acttransfnumbernode            NUMBER,
   auicode                        VARCHAR2(20),
   val_potenza                    NUMBER(20,9),
   val_clienti                    NUMBER(20,9),
   ctype_index                    NUMBER(2,0)  ) 
 TABLESPACE MAGO_DATA;


