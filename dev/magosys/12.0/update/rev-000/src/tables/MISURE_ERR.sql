CREATE TABLE misure_err (
   run_id                         NUMBER,
   cod_trattamento_elem           NUMBER,
   cg_impianto                    VARCHAR2(14),
   cod_tipo_misura                VARCHAR2(6),
   data                           DATE,
   valore                         NUMBER,
   errore                         VARCHAR2(30)  ) 
TABLESPACE mago_data
PARTITION BY HASH (RUN_ID)
PARTITIONS 1 
;


