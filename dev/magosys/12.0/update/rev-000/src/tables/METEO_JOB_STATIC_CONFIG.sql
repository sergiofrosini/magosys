CREATE TABLE meteo_job_static_config (
   key                            VARCHAR2(100) NOT NULL ,
   value                          VARCHAR2(255)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_job_static_config_pk ON meteo_job_static_config ( key ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_job_static_config ADD CONSTRAINT meteo_job_static_config_pk PRIMARY KEY (key) ENABLE;
