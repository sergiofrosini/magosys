CREATE TABLE nowcast_centraline (
   id_centralina                  VARCHAR2(10) NOT NULL ,
   meteo_provider_id              VARCHAR2(2) NOT NULL ,
   root_gest_code                 VARCHAR2(256) NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX nowcast_centraline_pk ON nowcast_centraline ( id_centralina ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE nowcast_centraline ADD CONSTRAINT nowcast_centraline_pk PRIMARY KEY (id_centralina) ENABLE;
