CREATE GLOBAL TEMPORARY TABLE gttd_calc_gerarchia (
   cod_elemento                   NUMBER,
   l01                            NUMBER,
   l02                            NUMBER,
   l03                            NUMBER,
   l04                            NUMBER,
   l05                            NUMBER,
   l06                            NUMBER,
   l07                            NUMBER,
   l08                            NUMBER,
   l09                            NUMBER,
   l10                            NUMBER,
   l11                            NUMBER,
   l12                            NUMBER,
   l13                            NUMBER,
   l14                            NUMBER,
   l15                            NUMBER,
   l16                            NUMBER,
   l17                            NUMBER,
   l18                            NUMBER,
   l19                            NUMBER,
   l20                            NUMBER,
   l21                            NUMBER,
   l22                            NUMBER,
   l23                            NUMBER,
   l24                            NUMBER,
   l25                            NUMBER  ) 
 ON COMMIT PRESERVE ROWS ;

CREATE  INDEX gttd_calc_gerarchia_pk ON gttd_calc_gerarchia ( cod_elemento );

