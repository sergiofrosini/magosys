CREATE TABLE produttori_tmp (
   cod_gruppo                     VARCHAR2(15),
   descr_gruppo                   VARCHAR2(30),
   cod_contatore_fis              VARCHAR2(14),
   cg_produttore                  VARCHAR2(14),
   descr_produttore               VARCHAR2(60),
   cod_contatore_sc               VARCHAR2(14),
   cg_generatore                  VARCHAR2(14) NOT NULL ,
   comune                         VARCHAR2(50),
   cod_comune                     VARCHAR2(6),
   tipo_coord_p                   VARCHAR2(5),
   coord_nord_p                   VARCHAR2(30),
   coord_est_p                    VARCHAR2(30),
   coord_up_p                     NUMBER,
   tipo_coord_a                   VARCHAR2(5),
   coord_nord_a                   VARCHAR2(30),
   coord_est_a                    VARCHAR2(30),
   coord_up_a                     NUMBER,
   h_anem                         NUMBER,
   tipo_rete                      VARCHAR2(2),
   fonte                          VARCHAR2(1)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX produttori_tmp_pk ON produttori_tmp ( cg_generatore ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE produttori_tmp ADD CONSTRAINT produttori_tmp_pk PRIMARY KEY (cg_generatore) ENABLE;
