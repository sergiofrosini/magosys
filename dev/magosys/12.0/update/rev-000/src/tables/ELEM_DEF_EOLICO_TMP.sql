CREATE TABLE elem_def_eolico_tmp (
   cg_generatore                  VARCHAR2(14) NOT NULL ,
   potenza_installata             NUMBER,
   marca_turbina                  VARCHAR2(50),
   modello_turbina                VARCHAR2(50),
   h_mozzo                        NUMBER(6,2),
   vel_cutin                      NUMBER(5,2),
   vel_max                        NUMBER(5,2),
   vel_cutoff                     NUMBER(5,2),
   vel_recutin                    NUMBER(5,2),
   delta_cutoff                   NUMBER(4,0),
   delta_recutin                  NUMBER(4,0),
   wind_sect_manager              VARCHAR2(2),
   start_wind_sect                NUMBER(5,2),
   stop_wind_sect                 NUMBER(5,2),
   vel_cutoff_shdown              NUMBER(5,2),
   vel_recutin_shdown             NUMBER(5,2)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX elem_def_eolico_tmp_pk ON elem_def_eolico_tmp ( cg_generatore ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE elem_def_eolico_tmp ADD CONSTRAINT elem_def_eolico_tmp_pk PRIMARY KEY (cg_generatore) ENABLE;
