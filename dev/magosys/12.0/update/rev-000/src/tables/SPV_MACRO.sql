CREATE TABLE spv_macro (
   id_macro                       NUMBER NOT NULL ,
   descrizione                    VARCHAR2(100)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN SPV_MACRO.ID_MACRO IS 'PK LIVELLO';
COMMENT ON COLUMN SPV_MACRO.DESCRIZIONE IS 'Descrizionedei livelli di allarme della supervisione (GRAVE, MEDIO ,NESSUNO)';

CREATE UNIQUE INDEX spv_macro_pk ON spv_macro ( id_macro ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_macro ADD CONSTRAINT sys_c0062671 PRIMARY KEY (id_macro) ENABLE;
