CREATE TABLE manutenzione (
   id_manutenzione                NUMBER,
   cod_elemento                   NUMBER NOT NULL ,
   tipo_elemento                  VARCHAR2(3),
   data_inizio                    DATE NOT NULL ,
   data_fine                      DATE,
   elaborato                      NUMBER(1,0),
   data_fine_old                  DATE,
   percentuale                    NUMBER,
   percentuale_old                NUMBER,
   flg_deleted                    NUMBER(1,0)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX manutenzione_pk ON manutenzione ( cod_elemento, data_inizio ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE manutenzione ADD CONSTRAINT manutenzione_pk PRIMARY KEY (cod_elemento, data_inizio) ENABLE;
