CREATE GLOBAL TEMPORARY TABLE gttd_rep_energia_potenza (
   tipo_report                    NUMBER(1,0),
   cod_elemento                   NUMBER,
   cod_raggr_fonte                VARCHAR2(2),
   cod_tipo_rete                  CHAR(1),
   cod_tipo_produttore            CHAR(1),
   cod_tipo_misura                VARCHAR2(6),
   segno                          CHAR(1),
   valore                         NUMBER  ) 
 ON COMMIT PRESERVE ROWS ;

CREATE  INDEX gttd_rep_engr_ptnz_k1 ON gttd_rep_energia_potenza ( cod_elemento );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k2 ON gttd_rep_energia_potenza ( tipo_report );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k3 ON gttd_rep_energia_potenza ( cod_tipo_misura );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k4 ON gttd_rep_energia_potenza ( cod_raggr_fonte );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k5 ON gttd_rep_energia_potenza ( cod_tipo_rete );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k6 ON gttd_rep_energia_potenza ( cod_tipo_produttore );
CREATE BITMAP INDEX gttd_rep_engr_ptnz_k7 ON gttd_rep_energia_potenza ( segno );

