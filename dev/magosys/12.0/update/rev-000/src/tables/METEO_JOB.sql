CREATE TABLE meteo_job (
   id                             NUMBER(8,0) NOT NULL ,
   job_id                         VARCHAR2(50) NOT NULL ,
   start_date                     DATE,
   end_date                       DATE,
   status                         VARCHAR2(100),
   description                    VARCHAR2(1024),
   timer_description              VARCHAR2(1024)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_job_pk ON meteo_job ( id ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_job ADD CONSTRAINT meteo_job_pk PRIMARY KEY (id) ENABLE;
