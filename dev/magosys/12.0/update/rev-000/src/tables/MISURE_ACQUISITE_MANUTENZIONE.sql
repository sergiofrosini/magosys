CREATE TABLE misure_acquisite_manutenzione (
   cod_trattamento_elem           NUMBER NOT NULL ,
   data                           DATE NOT NULL ,
   valore                         NUMBER(20,4)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX misure_acquisite_man_pk ON misure_acquisite_manutenzione ( cod_trattamento_elem, data ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE misure_acquisite_manutenzione ADD CONSTRAINT misure_acquisite_man_pk PRIMARY KEY (cod_trattamento_elem, data) ENABLE;
