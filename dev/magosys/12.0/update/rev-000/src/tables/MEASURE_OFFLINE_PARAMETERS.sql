CREATE TABLE measure_offline_parameters (
   key                            VARCHAR2(100) NOT NULL ,
   value                          VARCHAR2(255)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX measure_offline_parameters_pk ON measure_offline_parameters ( key ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE measure_offline_parameters ADD CONSTRAINT measure_offline_parameters_pk PRIMARY KEY (key) ENABLE;
