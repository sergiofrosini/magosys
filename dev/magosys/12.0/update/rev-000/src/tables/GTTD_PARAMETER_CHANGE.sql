CREATE GLOBAL TEMPORARY TABLE gttd_parameter_change (
   cod_gest_elemento              VARCHAR2(20),
   data_agg                       DATE,
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_produttore            CHAR(1),
   cod_tipo_rete                  CHAR(1),
   parametro1                     NUMBER,
   parametro2                     NUMBER,
   parametro3                     NUMBER,
   parametro4                     NUMBER,
   parametro5                     NUMBER,
   parametro6                     NUMBER,
   parametro7                     NUMBER,
   parametro8                     NUMBER,
   parametro9                     NUMBER,
   parametro10                    NUMBER,
   v_cut_in                       NUMBER,
   v_cut_off                      NUMBER,
   v_max_power                    NUMBER,
   j_max                          NUMBER,
   alfa_max                       NUMBER,
   alfa_min                       NUMBER  ) 
 ON COMMIT DELETE ROWS ;


