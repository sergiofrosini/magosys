CREATE TABLE gruppi_misura (
   cod_gruppo                     NUMBER NOT NULL ,
   cod_tipo_elemento              VARCHAR2(3) NOT NULL ,
   cod_tipo_misura                VARCHAR2(6) NOT NULL ,
   flag_attivo                    NUMBER(1,0),
   seq_ord                        NUMBER,
   disattivato                    NUMBER(1,0) DEFAULT 0   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE GRUPPI_MISURA IS 'Descrive i Gruppi Misura definiti per ogni Tipo Elemento';

COMMENT ON COLUMN GRUPPI_MISURA.COD_GRUPPO IS 'Identificativo del Gruppo';
COMMENT ON COLUMN GRUPPI_MISURA.COD_TIPO_ELEMENTO IS 'Tipo Elemento';
COMMENT ON COLUMN GRUPPI_MISURA.COD_TIPO_MISURA IS 'Tipo Misura';
COMMENT ON COLUMN GRUPPI_MISURA.FLAG_ATTIVO IS 'Attivo = 1';
COMMENT ON COLUMN GRUPPI_MISURA.SEQ_ORD IS 'Sequenza di ordinamento nell''ambito del Gruppo';
COMMENT ON COLUMN GRUPPI_MISURA.DISATTIVATO IS '1=Elemento non visibile';

CREATE UNIQUE INDEX gruppi_misura_pk ON gruppi_misura ( cod_gruppo, cod_tipo_elemento, cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE gruppi_misura ADD CONSTRAINT gruppi_misura_disattivato_chk CHECK (DISATTIVATO IN (0,1)) ENABLE;
ALTER TABLE gruppi_misura ADD CONSTRAINT gruppi_misura_flag_attivo_chk CHECK (FLAG_ATTIVO IN (0,1)) ENABLE;
ALTER TABLE gruppi_misura ADD CONSTRAINT gruppi_misura_pk PRIMARY KEY (cod_gruppo, cod_tipo_elemento, cod_tipo_misura) ENABLE;
ALTER TABLE gruppi_misura ADD CONSTRAINT tpele_grpmis FOREIGN KEY (cod_tipo_elemento) REFERENCES tipi_elemento(cod_tipo_elemento) ENABLE;
ALTER TABLE gruppi_misura ADD CONSTRAINT tpgms_grpmis FOREIGN KEY (cod_gruppo) REFERENCES tipi_gruppi_misura(cod_gruppo) ENABLE;
ALTER TABLE gruppi_misura ADD CONSTRAINT tpmis_grpmis FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
