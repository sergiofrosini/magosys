CREATE TABLE session_statistics (
   cod_sessione                   NUMBER NOT NULL ,
   service_id                     VARCHAR2(100),
   service_ref                    VARCHAR2(200),
   cod_tipo_misura                VARCHAR2(6),
   status                         VARCHAR2(20),
   date_from                      DATE NOT NULL ,
   date_to                        DATE,
   leaf_start_date                DATE,
   leaf_end_date                  DATE,
   aggreg_start_date              DATE,
   aggreg_end_date                DATE,
   aggreg_completed               NUMBER(1,0) DEFAULT 0 ,
   date_start                     DATE,
   date_end                       DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE SESSION_STATISTICS IS 'Statistiche di sessione';

COMMENT ON COLUMN SESSION_STATISTICS.COD_SESSIONE IS 'Identificativo della Sessione';
COMMENT ON COLUMN SESSION_STATISTICS.SERVICE_ID IS 'Identificativo del servizio';
COMMENT ON COLUMN SESSION_STATISTICS.SERVICE_REF IS 'Informazione di riferimento';
COMMENT ON COLUMN SESSION_STATISTICS.COD_TIPO_MISURA IS 'Codice Tipo Misura (piu'' tipi misura possono essere presenti per medesimo COD_SESSIONE / DATE_FROM';
COMMENT ON COLUMN SESSION_STATISTICS.STATUS IS 'Stato';
COMMENT ON COLUMN SESSION_STATISTICS.DATE_FROM IS 'Data inizio periodo di riferimento';
COMMENT ON COLUMN SESSION_STATISTICS.DATE_TO IS 'Data fine periodo di riferimento';
COMMENT ON COLUMN SESSION_STATISTICS.LEAF_START_DATE IS 'data inizio immissione Foglie';
COMMENT ON COLUMN SESSION_STATISTICS.LEAF_END_DATE IS 'data fine immissione Foglie';
COMMENT ON COLUMN SESSION_STATISTICS.AGGREG_START_DATE IS 'data inizio aggregazioni';
COMMENT ON COLUMN SESSION_STATISTICS.AGGREG_END_DATE IS 'data fine aggregazioni';
COMMENT ON COLUMN SESSION_STATISTICS.AGGREG_COMPLETED IS '1=Aggregazioni completate';
COMMENT ON COLUMN SESSION_STATISTICS.DATE_START IS 'data/ora di termine della sessione (Status=DONE)';

CREATE BITMAP INDEX session_statistics_aggr_compl ON session_statistics ( aggreg_completed ) LOGGING TABLESPACE MAGO_DATA;
CREATE  INDEX session_statistics_serv_id ON session_statistics ( service_id ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX session_statistics_serv_ref ON session_statistics ( service_ref ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX session_statistics_uk1 ON session_statistics ( cod_sessione, date_from, cod_tipo_misura ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE session_statistics ADD CONSTRAINT tipmis_sessstat FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
