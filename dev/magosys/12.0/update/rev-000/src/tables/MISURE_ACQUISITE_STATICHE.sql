CREATE TABLE misure_acquisite_statiche (
   cod_trattamento_elem           NUMBER NOT NULL ,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE,
   valore                         NUMBER(20,4) ,CONSTRAINT misure_acquisite_statiche_pk PRIMARY KEY (cod_trattamento_elem, data_attivazione)  ) 
 ORGANIZATION INDEX  TABLESPACE MAGO_IOT;


ALTER TABLE misure_acquisite_statiche ADD CONSTRAINT trtele_misacqstat FOREIGN KEY (cod_trattamento_elem) REFERENCES trattamento_elementi(cod_trattamento_elem) ENABLE;
