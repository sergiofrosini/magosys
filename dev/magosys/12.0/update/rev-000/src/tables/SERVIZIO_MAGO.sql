CREATE TABLE servizio_mago (
   nome                           VARCHAR2(100) NOT NULL ,
   ultima_esecuzione              DATE,
   prossima_esecuzione            DATE,
   intervallo                     NUMBER(8,0)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX servizio_mago_pk ON servizio_mago ( nome ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE servizio_mago ADD CONSTRAINT servizio_mago_pk PRIMARY KEY (nome) ENABLE;
