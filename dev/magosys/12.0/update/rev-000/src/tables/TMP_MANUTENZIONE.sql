CREATE TABLE tmp_manutenzione (
   id_manutenzione                NUMBER,
   cod_elemento                   NUMBER NOT NULL ,
   tipo_elemento                  VARCHAR2(3) NOT NULL ,
   data_inizio                    DATE NOT NULL ,
   data_fine                      DATE,
   percentuale                    NUMBER DEFAULT 1   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX tmp_manutenzione_pk ON tmp_manutenzione ( cod_elemento, data_inizio ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE tmp_manutenzione ADD CONSTRAINT tmp_manutenzione_pk PRIMARY KEY (cod_elemento, data_inizio) ENABLE;
