CREATE TABLE spv_collector_info (
   id_info                        NUMBER NOT NULL ,
   collector_name                 VARCHAR2(20),
   data_inserimento               DATE DEFAULT SYSDATE ,
   id_rel_sistema_dizionario      NUMBER,
   supplier_name                  VARCHAR2(20),
   cod_gest_elemento              VARCHAR2(20),
   cod_tipo_misura                VARCHAR2(6),
   cod_tipo_fonte                 VARCHAR2(2),
   valore                         NUMBER,
   informazioni                   VARCHAR2(1000)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_collector_info_pk ON spv_collector_info ( id_info ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_collector_info ADD CONSTRAINT spv_ci_rsd FOREIGN KEY (id_rel_sistema_dizionario) REFERENCES spv_rel_sistema_dizionario(id_rel_sistema_dizionario) ENABLE;
ALTER TABLE spv_collector_info ADD CONSTRAINT spv_collector_info_pk PRIMARY KEY (id_info) ENABLE;
