CREATE TABLE spv_rel_categoria_sistema (
   id_categoria                   NUMBER NOT NULL ,
   id_sistema                     NUMBER NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_rel_categoria_sistema_pk ON spv_rel_categoria_sistema ( id_categoria, id_sistema ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_rel_categoria_sistema ADD CONSTRAINT fk_categoria_rel_sistema FOREIGN KEY (id_categoria) REFERENCES spv_categoria(id_categoria) ENABLE;
ALTER TABLE spv_rel_categoria_sistema ADD CONSTRAINT fk_sistema_rel_categ FOREIGN KEY (id_sistema) REFERENCES spv_sistema(id_sistema) ENABLE;
ALTER TABLE spv_rel_categoria_sistema ADD CONSTRAINT sys_c0062672 PRIMARY KEY (id_categoria, id_sistema) ENABLE;
