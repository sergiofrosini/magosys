CREATE TABLE spv_allarmi (
   id_allarme                     NUMBER NOT NULL ,
   id_rel_sistema_dizionario      NUMBER NOT NULL ,
   data_allarme                   DATE NOT NULL ,
   descrizione_tipo               VARCHAR2(30),
   descrizione_elemento           VARCHAR2(100),
   descrizione_altro1             VARCHAR2(300),
   descrizione_altro2             VARCHAR2(300),
   descrizione_altro3             VARCHAR2(300),
   id_livello                     NUMBER,
   info                           VARCHAR2(1000),
   parametri_descrizione          VARCHAR2(1000)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE SPV_ALLARMI IS 'Tabella degli allarmi generati dai vari processi di verifica; sulla tabella deve esistere solo lutlimo allarme generato per il dizionario relativo (il precedente deve essere cancelllato';

COMMENT ON COLUMN SPV_ALLARMI.ID_ALLARME IS 'PK REL_SISTEMA_DIZIONARIO; genearto da sequence ';
COMMENT ON COLUMN SPV_ALLARMI.ID_REL_SISTEMA_DIZIONARIO IS 'ID RELAZIONE derivato dalla tabella SPV_REL_SISTEMA_DIZIONARIO ';
COMMENT ON COLUMN SPV_ALLARMI.DATA_ALLARME IS 'Data e ora in cui � stato generato lallarme';
COMMENT ON COLUMN SPV_ALLARMI.DESCRIZIONE_TIPO IS 'Tipo elemento sui � stato generato allarme ';
COMMENT ON COLUMN SPV_ALLARMI.DESCRIZIONE_ELEMENTO IS 'Codice elemento ';
COMMENT ON COLUMN SPV_ALLARMI.INFO IS 'Colonna alanumerica per registrare informazioni su chi ha genrato l allarme (id job) ';

CREATE UNIQUE INDEX spv_allarmi_pk ON spv_allarmi ( id_allarme ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_allarmi ADD CONSTRAINT fk_allarme_f1 FOREIGN KEY (id_livello) REFERENCES spv_livello_allarmi(id_livello) ENABLE;
ALTER TABLE spv_allarmi ADD CONSTRAINT fk_allarme_f2 FOREIGN KEY (id_rel_sistema_dizionario) REFERENCES spv_rel_sistema_dizionario(id_rel_sistema_dizionario) ENABLE;
ALTER TABLE spv_allarmi ADD CONSTRAINT sys_c0062652 PRIMARY KEY (id_allarme) ENABLE;
