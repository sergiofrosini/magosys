CREATE TABLE rel_elementi_ecp_sn (
   cod_elemento_padre             NUMBER NOT NULL ,
   cod_elemento_figlio            NUMBER NOT NULL ,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE DEFAULT TO_DATE('01013000','ddmmyyyy') ,
   data_disattivazione_undisconn  DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE REL_ELEMENTI_ECP_SN IS 'Gerarchia Elettrica di Cabina Primaria in stato Normale (include anche Centro Operarivo ed Esercizio) ';

COMMENT ON COLUMN REL_ELEMENTI_ECP_SN.COD_ELEMENTO_PADRE IS 'Codice elemento Padre';
COMMENT ON COLUMN REL_ELEMENTI_ECP_SN.COD_ELEMENTO_FIGLIO IS 'Codice elemento Figlio';
COMMENT ON COLUMN REL_ELEMENTI_ECP_SN.DATA_ATTIVAZIONE IS 'data inizio validita''';
COMMENT ON COLUMN REL_ELEMENTI_ECP_SN.DATA_DISATTIVAZIONE IS 'data fine validita''';
COMMENT ON COLUMN REL_ELEMENTI_ECP_SN.DATA_DISATTIVAZIONE_UNDISCONN IS 'Data di disattivazione senza buchi di inattivita''';

CREATE  INDEX rel_ele_cp_sn_if1 ON rel_elementi_ecp_sn ( cod_elemento_figlio ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX rel_ele_cp_sn_if2 ON rel_elementi_ecp_sn ( cod_elemento_padre ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX rel_ele_cp_sn_pk ON rel_elementi_ecp_sn ( cod_elemento_padre, cod_elemento_figlio, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE rel_elementi_ecp_sn ADD CONSTRAINT ele_relcp_sn__f FOREIGN KEY (cod_elemento_figlio) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_ecp_sn ADD CONSTRAINT ele_relcp_sn__p FOREIGN KEY (cod_elemento_padre) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_ecp_sn ADD CONSTRAINT rel_ele_cp_sn_pk PRIMARY KEY (cod_elemento_padre, cod_elemento_figlio, data_attivazione) ENABLE;
