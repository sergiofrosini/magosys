CREATE TABLE tipi_elemento (
   cod_tipo_elemento              VARCHAR2(3) NOT NULL ,
   codifica_st                    VARCHAR2(10),
   descrizione                    VARCHAR2(60),
   ger_ecp                        NUMBER(1,0) DEFAULT 0 ,
   ger_ecs                        NUMBER(1,0) DEFAULT 0 ,
   ger_geo                        NUMBER(1,0) DEFAULT 0 ,
   ger_amm                        NUMBER(1,0) DEFAULT 0 ,
   flag_produzione                NUMBER(1,0) DEFAULT 0 ,
   cod_tipo_rete                  CHAR(1),
   nascosto                       NUMBER(1,0) DEFAULT 0 ,
   fonte_non_omogenea             NUMBER(1,0) DEFAULT 0 ,
   fonte_non_applicabile          NUMBER(1,0) DEFAULT 0 ,
   produttore_non_applicabile     NUMBER(1,0) DEFAULT 0 ,
   produttore_non_disponibile     NUMBER(1,0) DEFAULT 0   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_ELEMENTO IS 'Tabella dei tipi elemento previsti';

COMMENT ON COLUMN TIPI_ELEMENTO.COD_TIPO_ELEMENTO IS 'Codice tipo Elemento interno';
COMMENT ON COLUMN TIPI_ELEMENTO.CODIFICA_ST IS 'Codice tipo Elemento da sistema Esterno (ST)';
COMMENT ON COLUMN TIPI_ELEMENTO.DESCRIZIONE IS 'Descrizione del tipo elemento';
COMMENT ON COLUMN TIPI_ELEMENTO.GER_ECP IS '>0 = Facente parte di gerarchia elettrica di Cabina Primaria - indica numero di livelli da avanzare per la selezione dell''elemento successivo per FE';
COMMENT ON COLUMN TIPI_ELEMENTO.GER_ECS IS '>0 = Facente parte di gerarchia elettrica di Cabina Secondaria - indica numero di livelli da avanzare per la selezione dell''elemento successivo per FE';
COMMENT ON COLUMN TIPI_ELEMENTO.GER_GEO IS '>0 = Facente parte di gerarchia Geografica (ISTAT) - indica numero di livelli da avanzare per la selezione dell''elemento successivo per FE';
COMMENT ON COLUMN TIPI_ELEMENTO.GER_AMM IS '>0 = Facente parte di gerarchia Organizzativa  - indica numero di livelli da avanzare per la selezione dell''elemento successivo per FE';
COMMENT ON COLUMN TIPI_ELEMENTO.FLAG_PRODUZIONE IS '1=Produttori associabili, 2=Misure associabili';
COMMENT ON COLUMN TIPI_ELEMENTO.COD_TIPO_RETE IS 'Tipo rete di competenza';
COMMENT ON COLUMN TIPI_ELEMENTO.NASCOSTO IS '1=L''elemento e'' nascosto a livello di FE';
COMMENT ON COLUMN TIPI_ELEMENTO.FONTE_NON_OMOGENEA IS '1=Utilizza Tipo Fonte Non Omogenea';
COMMENT ON COLUMN TIPI_ELEMENTO.FONTE_NON_APPLICABILE IS '1=Utilizza Tipo Fonte Non Applicabile';
COMMENT ON COLUMN TIPI_ELEMENTO.PRODUTTORE_NON_APPLICABILE IS '1=Utilizza Tipo Produttore Non Applicabile';
COMMENT ON COLUMN TIPI_ELEMENTO.PRODUTTORE_NON_DISPONIBILE IS '1=Utilizza Tipo Produttore Non Determinato';

CREATE UNIQUE INDEX tipi_elemento_i1 ON tipi_elemento ( codifica_st ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX tipi_elemento_pk ON tipi_elemento ( cod_tipo_elemento ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c01 CHECK ("FLAG_PRODUZIONE">=0 AND "FLAG_PRODUZIONE"<=2) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c02 CHECK (NASCOSTO in (1,0)) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c03 CHECK ("FONTE_NON_OMOGENEA"=0 OR "FONTE_NON_OMOGENEA"=1) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c04 CHECK ("FONTE_NON_APPLICABILE"=0 OR "FONTE_NON_APPLICABILE"=1) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c05 CHECK ("PRODUTTORE_NON_APPLICABILE"=0 OR "PRODUTTORE_NON_APPLICABILE"=1) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_c06 CHECK ("PRODUTTORE_NON_DISPONIBILE"=0 OR "PRODUTTORE_NON_DISPONIBILE"=1) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tipi_elemento_pk PRIMARY KEY (cod_tipo_elemento) ENABLE;
ALTER TABLE tipi_elemento ADD CONSTRAINT tprte_tpele FOREIGN KEY (cod_tipo_rete) REFERENCES tipi_rete(cod_tipo_rete) ENABLE;
