CREATE TABLE tipi_rete (
   cod_tipo_rete                  CHAR(1) NOT NULL ,
   descrizione                    VARCHAR2(60),
   id_rete                        NUMBER(3,0)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_RETE IS 'Tabella dei tipi Rete previste';

COMMENT ON COLUMN TIPI_RETE.COD_TIPO_RETE IS 'Codice Tipo Rete';
COMMENT ON COLUMN TIPI_RETE.DESCRIZIONE IS 'Descrizione del Tipo Rete';
COMMENT ON COLUMN TIPI_RETE.ID_RETE IS 'Idefntificativo del Tipo Rete';

CREATE UNIQUE INDEX tipi_rete_pk ON tipi_rete ( cod_tipo_rete ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_rete ADD CONSTRAINT tipi_rete_pk PRIMARY KEY (cod_tipo_rete) ENABLE;
