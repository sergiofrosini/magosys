CREATE TABLE raggruppamento_fonti (
   cod_raggr_fonte                VARCHAR2(2) NOT NULL ,
   descrizione                    VARCHAR2(60),
   id_raggr_fonte                 NUMBER  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE RAGGRUPPAMENTO_FONTI IS 'Tabella dei Raggruppamenti delle Fonti di Produzione previste';

COMMENT ON COLUMN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE IS 'Codice Raggruppamento Fonte';
COMMENT ON COLUMN RAGGRUPPAMENTO_FONTI.DESCRIZIONE IS 'Descrizione del Raggruppamento Fonte';
COMMENT ON COLUMN RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE IS 'Idefntificativo del Raggruppamento Fonte';

CREATE UNIQUE INDEX raggruppamenti_fonte_pk ON raggruppamento_fonti ( cod_raggr_fonte ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE raggruppamento_fonti ADD CONSTRAINT raggruppamento_fonti_pk PRIMARY KEY (cod_raggr_fonte) ENABLE;
