CREATE TABLE rel_elementi_ecs_sa (
   cod_elemento_padre             NUMBER NOT NULL ,
   cod_elemento_figlio            NUMBER NOT NULL ,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE DEFAULT TO_DATE('01013000','ddmmyyyy') ,
   flag_geo                       NUMBER(1,0) DEFAULT 2  NOT NULL ,
   data_disattivazione_undisconn  DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE REL_ELEMENTI_ECS_SA IS 'Gerarchia Elettrica di Cabina Secondaria in stato Attuale';

COMMENT ON COLUMN REL_ELEMENTI_ECS_SA.COD_ELEMENTO_PADRE IS 'Codice elemento Padre';
COMMENT ON COLUMN REL_ELEMENTI_ECS_SA.COD_ELEMENTO_FIGLIO IS 'Codice elemento Figlio';
COMMENT ON COLUMN REL_ELEMENTI_ECS_SA.DATA_ATTIVAZIONE IS 'data inizio validita''';
COMMENT ON COLUMN REL_ELEMENTI_ECS_SA.DATA_DISATTIVAZIONE IS 'data fine validita''';
COMMENT ON COLUMN REL_ELEMENTI_ECS_SA.DATA_DISATTIVAZIONE_UNDISCONN IS 'Data di disattivazione senza buchi di inattivita''';

CREATE  INDEX rel_ele_cs_sa_if1 ON rel_elementi_ecs_sa ( cod_elemento_figlio ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX rel_ele_cs_sa_if2 ON rel_elementi_ecs_sa ( cod_elemento_padre ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX rel_ele_cs_sa_pk ON rel_elementi_ecs_sa ( cod_elemento_padre, cod_elemento_figlio, data_attivazione, flag_geo ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE rel_elementi_ecs_sa ADD CONSTRAINT ele_relcs_sa__f FOREIGN KEY (cod_elemento_figlio) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_ecs_sa ADD CONSTRAINT ele_relcs_sa__p FOREIGN KEY (cod_elemento_padre) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elementi_ecs_sa ADD CONSTRAINT rel_ele_cs_sa_pk PRIMARY KEY (cod_elemento_padre, cod_elemento_figlio, data_attivazione, flag_geo) ENABLE;
