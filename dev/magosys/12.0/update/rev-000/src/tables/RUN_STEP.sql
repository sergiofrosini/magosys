CREATE TABLE run_step (
   proc_name                      VARCHAR2(50),
   proc_group                     VARCHAR2(50) NOT NULL ,
   stepid                         NUMBER NOT NULL ,
   param                          VARCHAR2(250)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX run_step_pk ON run_step ( proc_group, stepid ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE run_step ADD CONSTRAINT run_step_pk PRIMARY KEY (proc_group, stepid) ENABLE;
