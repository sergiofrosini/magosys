CREATE GLOBAL TEMPORARY TABLE gttd_valori_temp (
   tip                            VARCHAR2(10),
   num1                           NUMBER,
   num2                           NUMBER,
   num3                           NUMBER,
   num4                           NUMBER,
   num5                           NUMBER,
   alf1                           VARCHAR2(100),
   alf2                           VARCHAR2(100),
   alf3                           VARCHAR2(4000),
   alf4                           VARCHAR2(100),
   alf5                           VARCHAR2(100),
   alf6                           VARCHAR2(4000),
   dat1                           DATE,
   dat2                           DATE,
   dat3                           DATE,
   hex                            RAW(4)  ) 
 ON COMMIT PRESERVE ROWS ;

CREATE  INDEX gttd_valori_temp_ka1 ON gttd_valori_temp ( tip, alf1 );
CREATE  INDEX gttd_valori_temp_ka2 ON gttd_valori_temp ( tip, alf2 );
CREATE  INDEX gttd_valori_temp_key ON gttd_valori_temp ( tip );
CREATE  INDEX gttd_valori_temp_kn1 ON gttd_valori_temp ( tip, num1 );
CREATE  INDEX gttd_valori_temp_kn2 ON gttd_valori_temp ( tip, num2 );

