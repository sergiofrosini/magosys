CREATE TABLE elem_def_solare_tmp (
   cg_generatore                  VARCHAR2(14) NOT NULL ,
   potenza_installata             NUMBER,
   id_tecnologia                  VARCHAR2(1),
   flag_tracking                  VARCHAR2(2),
   orientazione                   NUMBER,
   inclinazione                   NUMBER,
   superficie                     NUMBER  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX elem_def_solare_tmp_pk ON elem_def_solare_tmp ( cg_generatore ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE elem_def_solare_tmp ADD CONSTRAINT elem_def_solare_tmp_pk PRIMARY KEY (cg_generatore) ENABLE;
