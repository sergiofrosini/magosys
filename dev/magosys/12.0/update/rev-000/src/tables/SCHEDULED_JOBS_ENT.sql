CREATE TABLE scheduled_jobs (
   job_number                     NUMBER NOT NULL ,
   tipo_job                       NUMBER,
   datarif                        DATE,
   datarif_to                     DATE,
   organizzazione                 NUMBER(1,0),
   stato                          NUMBER(1,0),
   id_rete                        NUMBER(1,0),
   cod_tipo_misura                VARCHAR2(6),
   datains                        DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE BITMAP INDEX scheduled_jobs_idx1 ON scheduled_jobs ( cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX scheduled_jobs_pk ON scheduled_jobs ( job_number ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX scheduled_jobs_uk1 ON scheduled_jobs ( tipo_job, datarif, organizzazione, stato, id_rete, cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE scheduled_jobs ADD CONSTRAINT jobs_tipo FOREIGN KEY (tipo_job) REFERENCES scheduled_jobs_def(tipo_job) ENABLE;
ALTER TABLE scheduled_jobs ADD CONSTRAINT scheduled_jobs_pk PRIMARY KEY (job_number) ENABLE;
ALTER TABLE scheduled_jobs ADD CONSTRAINT scheduled_jobs_r02 FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
