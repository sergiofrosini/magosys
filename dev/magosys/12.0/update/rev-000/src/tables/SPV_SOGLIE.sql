CREATE TABLE spv_soglie (
   id_soglia                      NUMBER NOT NULL ,
   id_sistema                     NUMBER NOT NULL ,
   id_chiave                      VARCHAR2(100) NOT NULL ,
   valore                         VARCHAR2(300) NOT NULL   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE SPV_SOGLIE IS 'Tabella relazione soglie con i singoli allarmi (dizionario descrizione)';

COMMENT ON COLUMN SPV_SOGLIE.ID_SOGLIA IS 'PK della soglia (progressivo)';
COMMENT ON COLUMN SPV_SOGLIE.ID_SISTEMA IS 'ID del sistema a cui � legato la soglia';
COMMENT ON COLUMN SPV_SOGLIE.ID_CHIAVE IS 'ID della soglia ';
COMMENT ON COLUMN SPV_SOGLIE.VALORE IS 'Valore del soglia da visualizzare sul FE ';

CREATE UNIQUE INDEX spv_soglie_i1 ON spv_soglie ( id_sistema, id_chiave ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX spv_soglie_pk ON spv_soglie ( id_soglia ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_soglie ADD CONSTRAINT fk_soglie_f1 FOREIGN KEY (id_sistema) REFERENCES spv_sistema(id_sistema) ENABLE;
ALTER TABLE spv_soglie ADD CONSTRAINT sys_c0062681 PRIMARY KEY (id_soglia) ENABLE;
