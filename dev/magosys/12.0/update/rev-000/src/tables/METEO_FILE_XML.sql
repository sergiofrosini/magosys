CREATE TABLE meteo_file_xml (
   id                             NUMBER(8,0) NOT NULL ,
   file_name                      VARCHAR2(100),
   creation_date                  DATE,
   status                         VARCHAR2(100),
   description                    VARCHAR2(400),
   meteo_file_zip_key             NUMBER(8,0)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_file_xml_pk ON meteo_file_xml ( id ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX mj_mfz_fk_idx ON meteo_file_xml ( meteo_file_zip_key ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_file_xml ADD CONSTRAINT meteo_file_xml_pk PRIMARY KEY (id) ENABLE;
ALTER TABLE meteo_file_xml ADD CONSTRAINT mfz_mfx FOREIGN KEY (meteo_file_zip_key) REFERENCES meteo_file_zip(id) ENABLE;
