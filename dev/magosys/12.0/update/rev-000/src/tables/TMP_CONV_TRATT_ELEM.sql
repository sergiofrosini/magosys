CREATE TABLE tmp_conv_tratt_elem (
   cod_trattamento_elem_new       NUMBER,
   cod_trattamento_elem           NUMBER,
   cod_elemento                   NUMBER,
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_misura                VARCHAR2(3),
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_rete                  CHAR(1),
   cod_tipo_produttore            CHAR(1),
   organizzazione                 NUMBER(1,0),
   tipo_aggregazione              NUMBER(1,0)  ) 
 TABLESPACE MAGO_DATA;


