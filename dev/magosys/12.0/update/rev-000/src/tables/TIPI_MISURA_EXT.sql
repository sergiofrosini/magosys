CREATE TABLE tipi_misura_ext (
   cod_tipo_misura_ext            VARCHAR2(6) NOT NULL ,
   codifica_st                    VARCHAR2(20),
   descrizione                    VARCHAR2(60),
   ext_applic                     VARCHAR2(20) NOT NULL ,
   mis_generazione                NUMBER(1,0) DEFAULT 0 ,
   risoluzione_fe                 NUMBER(3,0),
   cod_um_standard                VARCHAR2(6),
   rilevazioni_nagative           NUMBER(1,0) DEFAULT 0 ,
   split_fonte_energia            NUMBER(1,0) DEFAULT 0 ,
   tipo_interpolazione_freq_camp  CHAR(1) DEFAULT '0' ,
   tipo_interpolazione_buchi_camp CHAR(1) DEFAULT '0' ,
   flg_manutenzione               NUMBER DEFAULT 1 ,
   priorita_aggr                  NUMBER(1,0) DEFAULT 1 ,
   flg_mis_carico                 NUMBER(1,0) DEFAULT 0 ,
   flg_mis_generazione            NUMBER(1,0) DEFAULT 0 ,
   flg_apply_filter               NUMBER(1,0) DEFAULT 0 ,
   flg_kpi                        NUMBER DEFAULT 0 ,
   flg_var_pi                     NUMBER(1,0) DEFAULT 0 ,
   flg_var_pimp                   NUMBER(1,0) DEFAULT 0   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_MISURA_EXT IS 'Tabella di definizione dei tipi misura  non gestite direttamente da MAGO ma reperibili attraverso mago da sisteni esterni';

COMMENT ON COLUMN TIPI_MISURA_EXT.COD_TIPO_MISURA_EXT IS 'Codice Tipo Misura conosciuto dall''applicazione esterna';
COMMENT ON COLUMN TIPI_MISURA_EXT.CODIFICA_ST IS 'Codice Tipo Misura ST';
COMMENT ON COLUMN TIPI_MISURA_EXT.DESCRIZIONE IS 'Descrizione del tipo misura';
COMMENT ON COLUMN TIPI_MISURA_EXT.EXT_APPLIC IS 'Identificativo dell''applicazione esterna da cui reperire le misure';
COMMENT ON COLUMN TIPI_MISURA_EXT.MIS_GENERAZIONE IS '1=Misura proveniente da generazione';
COMMENT ON COLUMN TIPI_MISURA_EXT.RISOLUZIONE_FE IS 'Risoluzione di visualizzazione da parte del FE';
COMMENT ON COLUMN TIPI_MISURA_EXT.COD_UM_STANDARD IS 'Unita'' di misura standard';
COMMENT ON COLUMN TIPI_MISURA_EXT.RILEVAZIONI_NAGATIVE IS 'La misura puo'' avere rilevazioni negative: 0=false; 1=true';
COMMENT ON COLUMN TIPI_MISURA_EXT.TIPO_INTERPOLAZIONE_FREQ_CAMP IS 'Tipo di interpolazione da applicare alla curva per la frequenza di campionamento; 0=Default; 1=Lineare; 2=Spline; 3=Gradino';
COMMENT ON COLUMN TIPI_MISURA_EXT.TIPO_INTERPOLAZIONE_BUCHI_CAMP IS 'Tipo di interpolazione da applicare alla curva che presenta buchi di campionamento; 0=nessuna(Lascia il buco); 1=Lineare; 2=Spline; 3=Gradino';
COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_MANUTENZIONE IS '1 = Misura che varia in funzione degli elementi in manutenzione';
COMMENT ON COLUMN TIPI_MISURA_EXT.PRIORITA_AGGR IS 'Priorit¿¿ di aggregazione';
COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_APPLY_FILTER IS '1=Alla misura si applicano i filtri per fonti diverse da Solare/Eolico';
COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_KPI IS '1=La misura e'' un indice KPI';
COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_VAR_PI IS '1=La misura varia in funzione della Potenza Installata - Utilizzato per Replay';
COMMENT ON COLUMN TIPI_MISURA_EXT.FLG_VAR_PIMP IS '1=La misura varia in funzione della Potenza Impegnata - Utilizzato per Replay';

CREATE UNIQUE INDEX tipi_misura_ext_pk ON tipi_misura_ext ( cod_tipo_misura_ext ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_misura_ext ADD CONSTRAINT tipi_misura_ext_c01 CHECK (RILEVAZIONI_NAGATIVE IN (0,1)) ENABLE;
ALTER TABLE tipi_misura_ext ADD CONSTRAINT tipi_misura_ext_c02 CHECK (SPLIT_FONTE_ENERGIA IN (0,1)) ENABLE;
ALTER TABLE tipi_misura_ext ADD CONSTRAINT tipi_misura_ext_c03 CHECK (TIPO_INTERPOLAZIONE_BUCHI_CAMP IN ('0','1','2','3')) ENABLE;
ALTER TABLE tipi_misura_ext ADD CONSTRAINT tipi_misura_ext_c04 CHECK (TIPO_INTERPOLAZIONE_BUCHI_CAMP IN ('0','1','2','3')) ENABLE;
ALTER TABLE tipi_misura_ext ADD CONSTRAINT tipi_misura_ext_pk PRIMARY KEY (cod_tipo_misura_ext) ENABLE;
