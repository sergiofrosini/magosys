CREATE TABLE cfg_application_parameter (
   parameter_name                 VARCHAR2(50) NOT NULL ,
   par_value_char                 VARCHAR2(50),
   par_value_num                  NUMBER,
   par_value_date                 DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX cfg_appl_param_idx_pk ON cfg_application_parameter ( parameter_name ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE cfg_application_parameter ADD CONSTRAINT cfg_appl_param_idx_pk PRIMARY KEY (parameter_name) ENABLE;
