CREATE TABLE elementi_def (
   cod_elemento                   NUMBER NOT NULL ,
   nome_elemento                  VARCHAR2(60),
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_cliente               CHAR(1),
   id_elemento                    VARCHAR2(20),
   rif_elemento                   VARCHAR2(20),
   flag                           NUMBER(1,0),
   num_impianti                   NUMBER,
   potenza_installata             NUMBER,
   potenza_contrattuale           NUMBER,
   fattore                        NUMBER,
   coordinata_x                   NUMBER,
   coordinata_y                   NUMBER,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE,
   altitudine                     NUMBER,
   cod_geo                        VARCHAR2(10),
   cod_geo_a                      VARCHAR2(10),
   h_anem                         NUMBER  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE ELEMENTI_DEF IS 'Storicizzazione degli attributi di un Elemento';

COMMENT ON COLUMN ELEMENTI_DEF.COD_ELEMENTO IS 'Codice Elemento interno';
COMMENT ON COLUMN ELEMENTI_DEF.NOME_ELEMENTO IS 'Denominazione dell''elemento ';
COMMENT ON COLUMN ELEMENTI_DEF.COD_TIPO_ELEMENTO IS 'Codice Tipo Elemento';
COMMENT ON COLUMN ELEMENTI_DEF.COD_TIPO_FONTE IS 'Tipo Fonte dell''elemento (se applicabile)';
COMMENT ON COLUMN ELEMENTI_DEF.COD_TIPO_CLIENTE IS 'Tipo Cliente dell''elemento (se applicabile)';
COMMENT ON COLUMN ELEMENTI_DEF.ID_ELEMENTO IS 'Codice gestionale dell''elemento da cui dipende - per Generatori, Clienti e Sbarre di CS  - Codifica STM per gli Esercizi';
COMMENT ON COLUMN ELEMENTI_DEF.RIF_ELEMENTO IS 'Riferimento esterno: CFT per Sbarre di CS - POD Eneltel dell''elemento (se applicabile) - CPR fisica per Sbarre MT - Codifica STM dell''UTR per gli Esercizi';
COMMENT ON COLUMN ELEMENTI_DEF.FLAG IS 'ESE; 1=Principale, 0=Esercizio dell''UTR - CPR; 1=Centro Satellite';
COMMENT ON COLUMN ELEMENTI_DEF.NUM_IMPIANTI IS 'Numero impianti sottesi';
COMMENT ON COLUMN ELEMENTI_DEF.POTENZA_INSTALLATA IS 'KVa installati (se applicabile) - moltiplicare per FATTORE per ottenere KW';
COMMENT ON COLUMN ELEMENTI_DEF.POTENZA_CONTRATTUALE IS 'KVa contrattuali (se applicabile) - moltiplicare per FATTORE per ottenere KW';
COMMENT ON COLUMN ELEMENTI_DEF.FATTORE IS 'COSPHI o fattore conversione potenza';
COMMENT ON COLUMN ELEMENTI_DEF.COORDINATA_X IS 'Coordinata X (se applicabile)';
COMMENT ON COLUMN ELEMENTI_DEF.COORDINATA_Y IS 'Coordinata Y (se applicabile)';
COMMENT ON COLUMN ELEMENTI_DEF.DATA_ATTIVAZIONE IS 'data inizio validita''';
COMMENT ON COLUMN ELEMENTI_DEF.DATA_DISATTIVAZIONE IS 'data fine validita''';
COMMENT ON COLUMN ELEMENTI_DEF.ALTITUDINE IS 'Altitudine ubicazione elemento (se applicabile)';

CREATE  INDEX elementi_def_ak1 ON elementi_def ( cod_tipo_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX elementi_def_ak2 ON elementi_def ( cod_tipo_fonte ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX elementi_def_ak3 ON elementi_def ( cod_tipo_cliente ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX elementi_def_ak4 ON elementi_def ( rif_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX elementi_def_ak5 ON elementi_def ( id_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX elementi_def_pk ON elementi_def ( cod_elemento, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE elementi_def ADD CONSTRAINT elementi_def_pk PRIMARY KEY (cod_elemento, data_attivazione) ENABLE;
ALTER TABLE elementi_def ADD CONSTRAINT ele_eledef FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE elementi_def ADD CONSTRAINT tpcli_eledef FOREIGN KEY (cod_tipo_cliente) REFERENCES tipi_cliente(cod_tipo_cliente) ENABLE;
ALTER TABLE elementi_def ADD CONSTRAINT tpele_eledef FOREIGN KEY (cod_tipo_elemento) REFERENCES tipi_elemento(cod_tipo_elemento) ENABLE;
ALTER TABLE elementi_def ADD CONSTRAINT tpfon_eledef FOREIGN KEY (cod_tipo_fonte) REFERENCES tipo_fonti(cod_tipo_fonte) ENABLE;
