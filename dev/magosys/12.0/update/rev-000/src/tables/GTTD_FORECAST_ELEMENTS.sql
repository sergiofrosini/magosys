CREATE GLOBAL TEMPORARY TABLE gttd_forecast_elements (
   cod_elemento_padre             NUMBER,
   cod_gest_elemento_padre        VARCHAR2(20),
   cod_elemento                   NUMBER,
   cod_gest_elemento              VARCHAR2(20),
   cod_tipo_elemento              VARCHAR2(3),
   cod_tipo_fonte                 VARCHAR2(2),
   potenza_installata             NUMBER,
   cod_citta                      VARCHAR2(10),
   cod_tipo_produttore            CHAR(1),
   cod_tipo_rete                  CHAR(1),
   latitudine                     NUMBER,
   longitudine                    NUMBER,
   flg_disconnect                 NUMBER(1,0),
   parametro1                     NUMBER,
   parametro2                     NUMBER,
   parametro3                     NUMBER,
   parametro4                     NUMBER,
   parametro5                     NUMBER,
   parametro6                     NUMBER,
   parametro7                     NUMBER,
   parametro8                     NUMBER,
   parametro9                     NUMBER,
   parametro10                    NUMBER,
   data_ultimo_agg                DATE  ) 
 ON COMMIT DELETE ROWS ;


