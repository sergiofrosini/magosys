CREATE GLOBAL TEMPORARY TABLE gttd_kpi_misure (
   cod_trattamento_elem           NUMBER,
   data                           DATE,
   data_a                         DATE,
   valore                         NUMBER,
   valore2                        NUMBER,
   tipo_curva                     VARCHAR2(50),
   cod_tipo_misura                VARCHAR2(6),
   cod_tipo_fonte                 VARCHAR2(2),
   cod_tipo_rete                  CHAR(1),
   cod_tipo_cliente               CHAR(1),
   cod_elemento                   NUMBER,
   cod_tipo_elem                  VARCHAR2(3),
   inserimento                    NUMBER,
   modifica                       NUMBER,
   filter                         NUMBER,
   interpolazione                 NUMBER DEFAULT 0   ) 
 ON COMMIT DELETE ROWS ;

CREATE  INDEX gttd_kpi_misure_i1 ON gttd_kpi_misure ( cod_trattamento_elem, tipo_curva, data );
CREATE  INDEX gttd_kpi_misure_i2 ON gttd_kpi_misure ( cod_trattamento_elem );
CREATE  INDEX gttd_kpi_misure_i3 ON gttd_kpi_misure ( cod_trattamento_elem, tipo_curva );

