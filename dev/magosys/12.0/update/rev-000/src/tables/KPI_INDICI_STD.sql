Prompt Table KPI_INDICI;
--
-- KPI_INDICI  (Table) 
--

DECLARE

v_conto number;

BEGIN

	SELECT COUNT(*) INTO v_conto from user_tables where table_name='KPI_INDICI';

	IF v_conto > 0 then

		SELECT COUNT(*) INTO v_conto from user_tables where table_name='OLD_KPI_INDICI';

		IF v_conto = 0 then

			execute immediate 'ALTER TABLE KPI_INDICI RENAME CONSTRAINT KPI_INDICI_PK TO OLD_KPI_INDICI_PK';

			execute immediate 'ALTER TABLE KPI_INDICI RENAME CONSTRAINT TRTELE_KPIVAL TO OLD_TRTELE_KPIVAL';

			execute immediate 'ALTER INDEX KPI_INDICI_PK RENAME TO OLD_KPI_INDICI_PK';

			execute immediate 'alter table KPI_INDICI rename to OLD_KPI_INDICI';
			
		ELSE
		
			execute immediate 'drop table KPI_INDICI PURGE';
			
		END IF;

	END IF;

END;
/

  CREATE TABLE KPI_INDICI 
   (	COD_TRATTAMENTO_ELEM NUMBER, 
	TIPO_CURVA VARCHAR2(50 BYTE), 
	RISOLUZIONE NUMBER(*,0), 
	DATA DATE, 
	VALORE NUMBER
   ) TABLESPACE &TBS&DAT;
--------------------------------------------------------
--  DDL for Index KPI_INDICI_PK
--------------------------------------------------------

  CREATE UNIQUE INDEX KPI_INDICI_PK ON KPI_INDICI (COD_TRATTAMENTO_ELEM, TIPO_CURVA, RISOLUZIONE, DATA) 
  ;
--------------------------------------------------------
--  Constraints for Table KPI_INDICI
--------------------------------------------------------

  ALTER TABLE KPI_INDICI ADD CONSTRAINT KPI_INDICI_PK PRIMARY KEY (COD_TRATTAMENTO_ELEM, TIPO_CURVA, RISOLUZIONE, DATA) ENABLE;
--------------------------------------------------------
--  Ref Constraints for Table KPI_INDICI
--------------------------------------------------------

  ALTER TABLE KPI_INDICI ADD CONSTRAINT TRTELE_KPIVAL FOREIGN KEY (COD_TRATTAMENTO_ELEM)
	  REFERENCES TRATTAMENTO_ELEMENTI (COD_TRATTAMENTO_ELEM) ENABLE;

