CREATE TABLE spv_rel_soglie_dizionario (
   id_soglia                      NUMBER NOT NULL ,
   id_dizionario_allarme          NUMBER NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_rel_soglie_dizionario_pk ON spv_rel_soglie_dizionario ( id_soglia, id_dizionario_allarme ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_rel_soglie_dizionario ADD CONSTRAINT fk_rel_sd_f1 FOREIGN KEY (id_soglia) REFERENCES spv_soglie(id_soglia) ENABLE;
ALTER TABLE spv_rel_soglie_dizionario ADD CONSTRAINT fk_rel_sd_f2 FOREIGN KEY (id_dizionario_allarme) REFERENCES spv_dizionario_allarmi(id_dizionario_allarme) ENABLE;
ALTER TABLE spv_rel_soglie_dizionario ADD CONSTRAINT sys_c0062683 PRIMARY KEY (id_soglia, id_dizionario_allarme) ENABLE;
