CREATE TABLE elementi_gdf_solare (
   cod_elemento                   NUMBER NOT NULL ,
   orientazione                   NUMBER,
   id_tecnologia                  VARCHAR2(1),
   flag_tracking                  NUMBER(1,0),
   inclinazione                   NUMBER,
   superficie                     NUMBER,
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE NOT NULL   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE ELEMENTI_GDF_SOLARE IS 'Dettaglio di attributi di un Impianto Solare';

COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.COD_ELEMENTO IS 'Codice Elemento interno';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.ORIENTAZIONE IS 'Orientazione del pannello in gradi';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.ID_TECNOLOGIA IS 'Link verso la tipologia di pannelli solari';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.FLAG_TRACKING IS 'Flag che segnala se viene registrata l''irradiamento solare';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.INCLINAZIONE IS 'Inclinazione dei pannelli';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.SUPERFICIE IS 'Superficie dei pannelli';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.DATA_ATTIVAZIONE IS 'Data inizio validita''';
COMMENT ON COLUMN ELEMENTI_GDF_SOLARE.DATA_DISATTIVAZIONE IS 'Data fine validita''';

CREATE UNIQUE INDEX elementi_gdf_solare_pk ON elementi_gdf_solare ( cod_elemento, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE elementi_gdf_solare ADD CONSTRAINT elementi_gdf_solare_pk PRIMARY KEY (cod_elemento, data_attivazione) ENABLE;
ALTER TABLE elementi_gdf_solare ADD CONSTRAINT ele_elesol FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE elementi_gdf_solare ADD CONSTRAINT tiposol_elesol FOREIGN KEY (id_tecnologia) REFERENCES tipo_tecnologia_solare(id_tecnologia) ENABLE;
