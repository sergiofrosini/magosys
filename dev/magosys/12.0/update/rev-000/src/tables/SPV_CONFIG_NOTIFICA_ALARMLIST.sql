CREATE TABLE spv_config_notifica_alarmlist (
   id_spvconfignotifica           NUMBER NOT NULL ,
   id_sistema                     NUMBER NOT NULL ,
   id_livello                     NUMBER NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_config_alarmlist_pk ON spv_config_notifica_alarmlist ( id_livello, id_sistema, id_spvconfignotifica ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE spv_config_notifica_alarmlist ADD CONSTRAINT spv_config_alarmlist_pk PRIMARY KEY (id_livello, id_sistema, id_spvconfignotifica) ENABLE;
ALTER TABLE spv_config_notifica_alarmlist ADD CONSTRAINT spv_config_notifica_fk FOREIGN KEY (id_spvconfignotifica) REFERENCES spv_config_notifica(id_spvconfignotifica) ENABLE;
ALTER TABLE spv_config_notifica_alarmlist ADD CONSTRAINT spv_livello_allarmi_fk FOREIGN KEY (id_livello) REFERENCES spv_livello_allarmi(id_livello) ENABLE;
ALTER TABLE spv_config_notifica_alarmlist ADD CONSTRAINT spv_sistema_fk FOREIGN KEY (id_sistema) REFERENCES spv_sistema(id_sistema) ENABLE;
