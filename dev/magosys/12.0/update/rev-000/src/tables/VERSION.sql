CREATE TABLE version (
   data                           DATE NOT NULL ,
   major                          VARCHAR2(15) NOT NULL ,
   minor                          VARCHAR2(15) NOT NULL ,
   patch                          VARCHAR2(15) NOT NULL ,
   revision                       VARCHAR2(15) NOT NULL ,
   content                        VARCHAR2(255),
   description                    VARCHAR2(1000)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX version_pk ON version ( data, major, minor, patch, revision ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE version ADD CONSTRAINT version_pk PRIMARY KEY (data, major, minor, patch, revision) ENABLE;
