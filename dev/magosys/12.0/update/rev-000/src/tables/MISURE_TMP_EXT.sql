CREATE TABLE misure_tmp_ext (
   cg_impianto                    VARCHAR2(14),
   data                           VARCHAR2(10),
   ora                            VARCHAR2(10),
   tipo_misura                    VARCHAR2(6),
   valore                         VARCHAR2(30)  ) 
 ORGANIZATION EXTERNAL ( TYPE ORACLE_LOADER DEFAULT DIRECTORY "EXTDATA_DIR" ACCESS PARAMETERS ( RECORDS DELIMITED BY NEWLINE
        BADFILE 'misure.bad'
        SKIP 1
        FIELDS TERMINATED BY ";" OPTIONALLY ENCLOSED BY '"'
        MISSING FIELD VALUES ARE NULL
         ) LOCATION ('misure.csv')) ;


