CREATE TABLE elementi (
   cod_elemento                   NUMBER NOT NULL ,
   cod_gest_elemento              VARCHAR2(20),
   cod_tipo_elemento              VARCHAR2(3)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE ELEMENTI IS 'Identifica un elemento (elettrico, geografico, organizzativo)';

COMMENT ON COLUMN ELEMENTI.COD_ELEMENTO IS 'Codice Elemento interno';
COMMENT ON COLUMN ELEMENTI.COD_GEST_ELEMENTO IS 'Codice gestionale Elemento (ST)';
COMMENT ON COLUMN ELEMENTI.COD_TIPO_ELEMENTO IS 'Codice Tipo Elemento (ultimo valido)';

CREATE UNIQUE INDEX elementi_ak ON elementi ( cod_gest_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX elementi_pk ON elementi ( cod_elemento ) LOGGING TABLESPACE MAGO_IDX;
CREATE  INDEX elementi_tipo ON elementi ( cod_tipo_elemento ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE elementi ADD CONSTRAINT elementi_pk PRIMARY KEY (cod_elemento) ENABLE;
ALTER TABLE elementi ADD CONSTRAINT tpele_ele FOREIGN KEY (cod_tipo_elemento) REFERENCES tipi_elemento(cod_tipo_elemento) ENABLE;
