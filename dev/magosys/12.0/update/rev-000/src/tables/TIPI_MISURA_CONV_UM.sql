CREATE TABLE tipi_misura_conv_um (
   cod_tipo_misura                VARCHAR2(6) NOT NULL ,
   tipo_misura_conv               VARCHAR2(6) NOT NULL ,
   fattore_moltiplicativo         NUMBER DEFAULT 1 ,
   formula                        VARCHAR2(1000)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_MISURA_CONV_UM IS 'conversione da unita di misura del sistema internazionale a  unita di misura del sistema di TLC';

CREATE  INDEX tipi_misura_conv_um_if1 ON tipi_misura_conv_um ( cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX tipi_misura_conv_um_pk ON tipi_misura_conv_um ( cod_tipo_misura, tipo_misura_conv ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_misura_conv_um ADD CONSTRAINT tipi_misura_conv_um_pk PRIMARY KEY (cod_tipo_misura, tipo_misura_conv) ENABLE;
ALTER TABLE tipi_misura_conv_um ADD CONSTRAINT tipmis_conv_um FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
