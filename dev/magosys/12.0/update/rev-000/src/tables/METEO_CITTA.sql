CREATE TABLE meteo_citta (
   cod_citta                      VARCHAR2(10) NOT NULL ,
   nome                           VARCHAR2(100),
   lat                            NUMBER(32,5),
   lon                            NUMBER(32,5)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_citta_pk ON meteo_citta ( cod_citta ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_citta ADD CONSTRAINT meteo_citta_pk PRIMARY KEY (cod_citta) ENABLE;
