CREATE TABLE meteo_file_zip (
   id                             NUMBER(8,0) NOT NULL ,
   file_name                      VARCHAR2(100),
   creation_date                  DATE,
   status                         VARCHAR2(100),
   description                    VARCHAR2(400),
   meteo_job_key                  NUMBER(8,0),
   fornitore                      VARCHAR2(100),
   tipo_punto                     VARCHAR2(2)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_file_zip_pk ON meteo_file_zip ( id ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_file_zip ADD CONSTRAINT meteo_file_zip_pk PRIMARY KEY (id) ENABLE;
ALTER TABLE meteo_file_zip ADD CONSTRAINT mj_mfz FOREIGN KEY (meteo_job_key) REFERENCES meteo_job(id) ENABLE;
