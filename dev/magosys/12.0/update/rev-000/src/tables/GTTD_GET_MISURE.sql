CREATE GLOBAL TEMPORARY TABLE gttd_get_misure (
   cod_elemento                   NUMBER,
   cod_misura                     VARCHAR2(20),
   data_misura                    DATE,
   valore_misura                  NUMBER,
   tipo_fonte                     VARCHAR2(1),
   interpol                       NUMBER(1,0),
   cod_tipo_rete                  VARCHAR2(1),
   cod_tipo_cliente               VARCHAR2(1),
   quality                        VARCHAR2(20)  ) 
 ON COMMIT DELETE ROWS ;


