CREATE TABLE profili_def (
   cod_profilo                    NUMBER NOT NULL ,
   anno                           NUMBER(4,0) NOT NULL ,
   mese                           NUMBER(2,0) NOT NULL ,
   dt_misure_inizio               DATE,
   dt_misure_fine                 DATE  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE PROFILI_DEF IS 'Memorizzazione periodi di calcolo relative ai profili';

COMMENT ON COLUMN PROFILI_DEF.COD_PROFILO IS 'Identificativo del profilo';
COMMENT ON COLUMN PROFILI_DEF.ANNO IS 'Anno cui il profilo (COD_PROFILO) si riferisce';
COMMENT ON COLUMN PROFILI_DEF.MESE IS 'Mese a cui il profilo (COD_PROFILO) si riferisce';
COMMENT ON COLUMN PROFILI_DEF.DT_MISURE_INIZIO IS 'inizio periodo per il calcolo del profilo';
COMMENT ON COLUMN PROFILI_DEF.DT_MISURE_FINE IS 'fine periodo per il calcolo del profilo';

CREATE UNIQUE INDEX profili_def_pk ON profili_def ( cod_profilo, anno, mese ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE profili_def ADD CONSTRAINT profili_def_pk PRIMARY KEY (cod_profilo, anno, mese) ENABLE;
ALTER TABLE profili_def ADD CONSTRAINT sys_c0048065 CHECK (MESE BETWEEN 1 AND 12) ENABLE;
ALTER TABLE profili_def ADD CONSTRAINT sys_c0048067 FOREIGN KEY (cod_profilo) REFERENCES profili(cod_profilo) ENABLE;
