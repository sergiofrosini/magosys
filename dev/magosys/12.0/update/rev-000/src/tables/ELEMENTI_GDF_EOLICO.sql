CREATE TABLE elementi_gdf_eolico (
   cod_elemento                   NUMBER NOT NULL ,
   marca_turbina                  VARCHAR2(50),
   modello_turbina                VARCHAR2(50),
   vel_cutin                      NUMBER(5,2),
   vel_cutoff                     NUMBER(5,2),
   vel_max                        NUMBER(5,2),
   data_attivazione               DATE NOT NULL ,
   data_disattivazione            DATE NOT NULL ,
   vel_recutin                    NUMBER(5,2),
   delta_cutoff                   NUMBER(4,0),
   delta_recutin                  NUMBER(4,0),
   wind_sect_manager              NUMBER(1,0),
   start_wind_sect                NUMBER(5,2),
   stop_wind_sect                 NUMBER(5,2),
   vel_cutoff_shdown              NUMBER(5,2),
   vel_recutin_shdown             NUMBER(5,2),
   h_mozzo                        NUMBER(6,2)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE ELEMENTI_GDF_EOLICO IS 'Dettaglio di attributi di un Impianto Eolico';

COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.COD_ELEMENTO IS 'Codice Elemento interno';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.MARCA_TURBINA IS 'Marca della turbina';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.MODELLO_TURBINA IS 'Modello della turbina';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.VEL_CUTIN IS 'Velocita'' di cutin delle pale eoliche';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.VEL_CUTOFF IS 'Velocita'' di cutoff delle pale eoliche';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.VEL_MAX IS 'Velocita'' massima delle pale eoliche';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.DATA_ATTIVAZIONE IS 'Data inizio validita''';
COMMENT ON COLUMN ELEMENTI_GDF_EOLICO.DATA_DISATTIVAZIONE IS 'Data fine validita''';

CREATE UNIQUE INDEX elementi_gdf_eolico_pk ON elementi_gdf_eolico ( cod_elemento, data_attivazione ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE elementi_gdf_eolico ADD CONSTRAINT elementi_gdf_eolico_pk PRIMARY KEY (cod_elemento, data_attivazione) ENABLE;
ALTER TABLE elementi_gdf_eolico ADD CONSTRAINT ele_eleeol FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
