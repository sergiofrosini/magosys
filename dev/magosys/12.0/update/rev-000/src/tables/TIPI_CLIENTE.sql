CREATE TABLE tipi_cliente (
   cod_tipo_cliente               CHAR(1) NOT NULL ,
   descrizione                    VARCHAR2(60),
   id_cliente                     NUMBER,
   fornitore                      NUMBER(1,0) DEFAULT 0   NOT NULL   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE TIPI_CLIENTE IS 'Tabella dei tipi Cliente previsti';

COMMENT ON COLUMN TIPI_CLIENTE.COD_TIPO_CLIENTE IS 'Codice Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.DESCRIZIONE IS 'Descrizione del Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.ID_CLIENTE IS 'Idefntificativo del Tipo Cliente';
COMMENT ON COLUMN TIPI_CLIENTE.FORNITORE IS '1=Fornitore';

CREATE UNIQUE INDEX tipi_cliente_pk ON tipi_cliente ( cod_tipo_cliente ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipi_cliente ADD CONSTRAINT sys_c0026249 CHECK (FORNITORE IN (0,1)) ENABLE;
ALTER TABLE tipi_cliente ADD CONSTRAINT tipi_cliente_pk PRIMARY KEY (cod_tipo_cliente) ENABLE;
