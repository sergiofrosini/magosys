CREATE TABLE profili (
   cod_profilo                    NUMBER NOT NULL ,
   cod_elemento                   NUMBER,
   cod_tipo_misura                VARCHAR2(6),
   tipo                           NUMBER(1,0)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE PROFILI IS 'Definizione dei PROFILI Misura';

COMMENT ON COLUMN PROFILI.COD_PROFILO IS 'Identificativo del profilo';
COMMENT ON COLUMN PROFILI.COD_ELEMENTO IS 'Codice dell''elemento';
COMMENT ON COLUMN PROFILI.COD_TIPO_MISURA IS 'Codice Tipo Misura relativo ai valori del profilo';
COMMENT ON COLUMN PROFILI.TIPO IS 'tipo giorno (1=Feriale, 2=Prefestivo/Sabato, 3=Festivo/Domenica)';

CREATE UNIQUE INDEX profili_pk ON profili ( cod_profilo ) LOGGING TABLESPACE MAGO_IDX;
CREATE UNIQUE INDEX profili_uk1 ON profili ( cod_elemento, cod_tipo_misura, tipo ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE profili ADD CONSTRAINT profili_pk PRIMARY KEY (cod_profilo) ENABLE;
ALTER TABLE profili ADD CONSTRAINT sys_c0048062 CHECK (TIPO IN (1,2,3)) ENABLE;
ALTER TABLE profili ADD CONSTRAINT sys_c0048064 FOREIGN KEY (cod_tipo_misura) REFERENCES tipi_misura(cod_tipo_misura) ENABLE;
