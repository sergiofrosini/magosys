CREATE TABLE scheduled_tmp_gen (
   job_number                     NUMBER,
   datarif                        DATE NOT NULL ,
   organizzazione                 NUMBER(1,0) NOT NULL ,
   stato                          NUMBER(1,0) NOT NULL ,
   id_rete                        NUMBER(1,0) NOT NULL ,
   cod_tipo_misura                VARCHAR2(6) NOT NULL ,
   datains                        DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX scheduled_tmp_gen_pk1 ON scheduled_tmp_gen ( datarif, organizzazione, stato, id_rete, cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE scheduled_tmp_gen ADD CONSTRAINT scheduled_tmp_gen_pk1 PRIMARY KEY (datarif, organizzazione, stato, id_rete, cod_tipo_misura) ENABLE;
