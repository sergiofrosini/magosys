CREATE TABLE storico_import (
   origine                        VARCHAR2(10) NOT NULL ,
   data_import                    DATE NOT NULL ,
   tipo                           VARCHAR2(3) NOT NULL ,
   data_elab_ini                  TIMESTAMP(4),
   data_elab_fin                  TIMESTAMP(4)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE STORICO_IMPORT IS 'Elenca le operazioni di import eseguite e portate a buon fine';

COMMENT ON COLUMN STORICO_IMPORT.ORIGINE IS 'ORIGINE dei dati in import (es CORELE, AUI, ... ) ';
COMMENT ON COLUMN STORICO_IMPORT.DATA_IMPORT IS 'data di riferimento dell''import';
COMMENT ON COLUMN STORICO_IMPORT.TIPO IS 'SN=Stato Normale, SA=Stato Attuale, NEA=Evoluzione Assetto Rete, AUI=AUI';
COMMENT ON COLUMN STORICO_IMPORT.DATA_ELAB_INI IS 'Timestamp inizio elaborazione';
COMMENT ON COLUMN STORICO_IMPORT.DATA_ELAB_FIN IS 'Timestamp fine elaborazione';

CREATE UNIQUE INDEX storico_import_pk ON storico_import ( origine, data_import, tipo ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE storico_import ADD CONSTRAINT storico_import_pk PRIMARY KEY (origine, data_import, tipo) ENABLE;
