CREATE TABLE meteo_file_letto (
   nome                           VARCHAR2(100) NOT NULL ,
   data_lettura                   DATE  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_file_letto_pk ON meteo_file_letto ( nome ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_file_letto ADD CONSTRAINT meteo_file_letto_pk PRIMARY KEY (nome) ENABLE;
