CREATE TABLE spv_categoria (
   id_categoria                   NUMBER NOT NULL ,
   descrizione                    VARCHAR2(100)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN SPV_CATEGORIA.ID_CATEGORIA IS 'PK CATEGORIA';
COMMENT ON COLUMN SPV_CATEGORIA.DESCRIZIONE IS 'Descrizione delle categorie della supervisione (Supervisione delle previsioni metero ....';

CREATE UNIQUE INDEX spv_categoria_pk ON spv_categoria ( id_categoria ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_categoria ADD CONSTRAINT sys_c0062655 PRIMARY KEY (id_categoria) ENABLE;
