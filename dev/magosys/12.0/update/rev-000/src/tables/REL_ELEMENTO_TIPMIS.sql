CREATE TABLE rel_elemento_tipmis (
   cod_elemento                   NUMBER NOT NULL ,
   cod_tipo_misura                VARCHAR2(6) NOT NULL ,
   tab_misure                     NUMBER(1,0) DEFAULT 0 ,
   rete_at                        NUMBER(1,0) DEFAULT 0 ,
   rete_mt                        NUMBER(1,0) DEFAULT 0 ,
   rete_bt                        NUMBER(1,0) DEFAULT 0   ) 
 TABLESPACE MAGO_DATA;

COMMENT ON TABLE REL_ELEMENTO_TIPMIS IS 'Specifica per ogni associazione Elemento e TipoMisura su quali tabelle reperire le misure';

COMMENT ON COLUMN REL_ELEMENTO_TIPMIS.COD_ELEMENTO IS 'Codice interno dell''elemento';
COMMENT ON COLUMN REL_ELEMENTO_TIPMIS.COD_TIPO_MISURA IS 'Codice tipo Misura interno';
COMMENT ON COLUMN REL_ELEMENTO_TIPMIS.TAB_MISURE IS 'Tabelle Misure che il Tipo Elemento coinvolge: 0=nessuna, 1=Acquisite, 2=Aggregate, 3=Acquisite+Aggregate';

CREATE UNIQUE INDEX rel_ele_tipmis_pk ON rel_elemento_tipmis ( cod_elemento, cod_tipo_misura ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE rel_elemento_tipmis ADD CONSTRAINT ele_rel_ele_tpmis FOREIGN KEY (cod_elemento) REFERENCES elementi(cod_elemento) ENABLE;
ALTER TABLE rel_elemento_tipmis ADD CONSTRAINT rel_ele_tipmis_c01 CHECK ("TAB_MISURE">=0 AND "TAB_MISURE"<=3) ENABLE;
ALTER TABLE rel_elemento_tipmis ADD CONSTRAINT rel_ele_tipmis_pk PRIMARY KEY (cod_elemento, cod_tipo_misura) ENABLE;
