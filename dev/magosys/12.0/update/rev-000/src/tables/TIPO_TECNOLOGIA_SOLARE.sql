CREATE TABLE tipo_tecnologia_solare (
   id_tecnologia                  VARCHAR2(1) NOT NULL ,
   descrizione                    VARCHAR2(30)  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX tipo_tecnologia_solare_pk ON tipo_tecnologia_solare ( id_tecnologia ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE tipo_tecnologia_solare ADD CONSTRAINT tipo_tecnologia_solare_pk PRIMARY KEY (id_tecnologia) ENABLE;
