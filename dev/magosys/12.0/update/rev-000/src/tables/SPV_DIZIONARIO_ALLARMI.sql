CREATE TABLE spv_dizionario_allarmi (
   id_dizionario_allarme          NUMBER NOT NULL ,
   descrizione                    VARCHAR2(100)  ) 
 TABLESPACE MAGO_DATA;

COMMENT ON COLUMN SPV_DIZIONARIO_ALLARMI.ID_DIZIONARIO_ALLARME IS 'PK DIZONARIO';
COMMENT ON COLUMN SPV_DIZIONARIO_ALLARMI.DESCRIZIONE IS 'Descrizione dei singoli allarmi della supervisione (produttore sebna PPAGmp ....';

CREATE UNIQUE INDEX spv_dizionario_allarmi_pk ON spv_dizionario_allarmi ( id_dizionario_allarme ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_dizionario_allarmi ADD CONSTRAINT sys_c0062642 PRIMARY KEY (id_dizionario_allarme) ENABLE;
