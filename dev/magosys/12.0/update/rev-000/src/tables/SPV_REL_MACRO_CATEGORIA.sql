CREATE TABLE spv_rel_macro_categoria (
   id_macro                       NUMBER NOT NULL ,
   id_categoria                   NUMBER NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX spv_rel_macro_categ_pk ON spv_rel_macro_categoria ( id_macro, id_categoria ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE spv_rel_macro_categoria ADD CONSTRAINT fk_categoria_rel FOREIGN KEY (id_categoria) REFERENCES spv_categoria(id_categoria) ENABLE;
ALTER TABLE spv_rel_macro_categoria ADD CONSTRAINT fk_macro_rel FOREIGN KEY (id_macro) REFERENCES spv_macro(id_macro) ENABLE;
ALTER TABLE spv_rel_macro_categoria ADD CONSTRAINT sys_c0062675 PRIMARY KEY (id_macro, id_categoria) ENABLE;
