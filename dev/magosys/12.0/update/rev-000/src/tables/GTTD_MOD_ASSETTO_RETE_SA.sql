CREATE GLOBAL TEMPORARY TABLE gttd_mod_assetto_rete_sa (
   datarif                        DATE,
   stato                          CHAR(3),
   azione                         NUMBER,
   cod_elemento                   NUMBER,
   gest_elemento                  VARCHAR2(20),
   tipo_elemento                  VARCHAR2(3),
   cod_padre                      NUMBER,
   gest_padre                     VARCHAR2(20),
   tipo_padre                     VARCHAR2(3),
   data_rel                       DATE,
   tipo_rete                      CHAR(1),
   ger_ecp_ele                    NUMBER(1,0),
   ger_ecs_ele                    NUMBER(1,0),
   ger_amm_ele                    NUMBER(1,0),
   ger_geo_ele                    NUMBER(1,0),
   ger_ecp_padre                  NUMBER(1,0),
   ger_ecs_padre                  NUMBER(1,0),
   ger_amm_padre                  NUMBER(1,0),
   ger_geo_padre                  NUMBER(1,0)  ) 
 ON COMMIT PRESERVE ROWS ;


