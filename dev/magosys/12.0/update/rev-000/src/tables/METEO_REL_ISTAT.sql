CREATE TABLE meteo_rel_istat (
   nome                           VARCHAR2(50),
   cod_citta                      VARCHAR2(6),
   cod_istat                      VARCHAR2(6) NOT NULL   ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX meteo_rel_istat_pk ON meteo_rel_istat ( cod_istat ) LOGGING TABLESPACE MAGO_IDX;

ALTER TABLE meteo_rel_istat ADD CONSTRAINT meteo_rel_istat_pk PRIMARY KEY (cod_istat) ENABLE;
