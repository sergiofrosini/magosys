CREATE TABLE anagrafica_punti (
   cod_geo                        VARCHAR2(10) NOT NULL ,
   tipo_coord                     VARCHAR2(1) NOT NULL ,
   coordinata_x                   NUMBER,
   coordinata_y                   NUMBER,
   coordinata_h                   NUMBER  ) 
 TABLESPACE MAGO_DATA;

CREATE UNIQUE INDEX anagrafica_punti_pk ON anagrafica_punti ( cod_geo, tipo_coord ) LOGGING TABLESPACE MAGO_DATA;

ALTER TABLE anagrafica_punti ADD CONSTRAINT anagrafica_punti_pk PRIMARY KEY (cod_geo, tipo_coord) ENABLE;
