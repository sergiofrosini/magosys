CREATE OR REPLACE PACKAGE PKG_TRT_MIS AS


PROCEDURE AddMisureLV           (pMisureLV      IN t_TRTMISLV_aRRAY) ;
PROCEDURE AddMisureMVCli        (pMisureMVCli   IN t_TRTMISMVCLI_aRRAY) ;
PROCEDURE AddMisureMVTrend      (pMisureMVTrend IN t_TRTMISMVTRD_aRRAY) ;
PROCEDURE AddMisureLVProfile    (pMisureLvProfile IN t_TRTMISLVPROFILE_ARRAY) ;

procedure GetClienti(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                     pData           IN DATE,
                     pTipiElemento   IN VARCHAR2 DEFAULT PKG_Mago.cSepCharLst) ;

procedure SetFileElaborated(pFileinfo T_FILE_INFO_OBJ);

procedure GetFileElaborated(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                    pNomefile   IN VARCHAR2 DEFAULT NULL,
                           pTipofile   IN VARCHAR2 DEFAULT NULL) ;

END PKG_TRT_MIS;
/
