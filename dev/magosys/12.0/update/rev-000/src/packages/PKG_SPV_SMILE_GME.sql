CREATE OR REPLACE PACKAGE PKG_SPV_SMILE_GME AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/



	p_collectorName spv_collector_info.collector_name%type 	:='SMILE_GME';
	PID_SISTEMA spv_dettaglio_parametri.ID_SISTEMA%type 	:= 2;
	pID_CHIAVE_SPV_ENABLED VARCHAR2(100 BYTE) := 'smileGme.supervision_enabled';

  vMis_PI VARCHAR2(10)  := 'PI';
  vMis_PAS VARCHAR2(10)  := 'PAS';
  vMis_PPAGCT VARCHAR2(10)  := 'PMC';
  vMis_PRI VARCHAR2(10)  := 'PRI';

	--20	Assenza di PAS o PRI a livello di intero CO
	--21	Produttori senza PAS o PRI
	--22	Produttore scartato in fase di acquisizione della PAS o PRI
	--23	Produttore con PAS>PI
	--24	Produttore solare omogeneo con PAS non nulla di notte
	--25	Produttore con PAS>PPAGct
	--26	Produttore solare omogeneo con PAS>PPAGct
	--27	Produttore con PAS troppo bassa rispetto a PI  (in %)  per il giorno corrente-72H
	pID_DIZIONARIO_ALLARME_1 spv_dizionario_allarmi.id_dizionario_allarme%type := 20;
	pID_DIZIONARIO_ALLARME_2 spv_dizionario_allarmi.id_dizionario_allarme%type := 21;
	pID_DIZIONARIO_ALLARME_3 spv_dizionario_allarmi.id_dizionario_allarme%type := 22;
	pID_DIZIONARIO_ALLARME_4 spv_dizionario_allarmi.id_dizionario_allarme%type := 23;
	pID_DIZIONARIO_ALLARME_5 spv_dizionario_allarmi.id_dizionario_allarme%type := 24;
	pID_DIZIONARIO_ALLARME_6 spv_dizionario_allarmi.id_dizionario_allarme%type := 25;
	pID_DIZIONARIO_ALLARME_7 spv_dizionario_allarmi.id_dizionario_allarme%type := 26;
	pID_DIZIONARIO_ALLARME_8 spv_dizionario_allarmi.id_dizionario_allarme%type := 27;


  --- Parametri f_GetElement - BEGIN ---
  pTipologiaRete  VARCHAR2(20) := 'M|B';
  pFonte          VARCHAR2(20) := 'S|E|I|T|R|C|1';
  pTipoProd       VARCHAR2(20) := 'A|B|X';
  pTipoElementAll VARCHAR2(20) := 'TRM|CMT|GMT';
  pTipoElementCMT VARCHAR2(20) := 'CMT';
  pTipoElementTRM VARCHAR2(20) := 'TRM';
  pFlagPI         NUMBER := 1;
  pdisconnect     NUMBER := 1;
  --- Parametri f_GetElement - END ---

  procedure getDateFromAndTo (pDateFrom out date, pDateTo out date);

	-- Procedura che invoca la PKG_IntegrST.GetMeasureReq e recupera le misure per la supervisione SmileGme
	-- Scrive sulla tabella SPV_COLLECTOR_MEASURE
	-- Recupera 96 campioni per ogni misura all'interno delle 24 ore
	-- Da integrare con la GET_ELEMENT_FORECAST per recuperare i cod_gestionale da processare
	procedure smileGmeLoadMeasure(pDoAddSpvCollectorMeasure in number default 1);

	procedure smileGmeSpvCollector;

	procedure smileGmeSpvProcessor;

  procedure smileGmeSpvDetail;

  procedure smileGmeSpvMain(pDoDeleteOldSpvInfo in number default 1
							,pDoDeleteOldSpvMeasure in number default 1
							,pDoDeleteSpvAlarm in number default 1
							,pDoDeleteTodaySpvInfo in number default 1
              ,pDoLoadMeasure in number default 1
              ,pDoCollector in number default 1
              ,pDoProcessor in number default 1
              ,pDoDetail in number default 1
  );


END PKG_SPV_SMILE_GME;
/
