CREATE OR REPLACE PACKAGE "PKG_LOGS" AS

/* ***********************************************************************************************************
   NAME:       PKG_Logs
   PURPOSE:    Servizi per la gestione dei Log applicativi

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.9c.0     23/06/2015  Moretti C.       Utilizzo Sequence tramite SQL dinamico
   1.11       09/06/2016   Roberto Z.    Modifica per MAGONAZ

   NOTES:

*********************************************************************************************************** */

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 gVerboseLog   BOOLEAN := FALSE;

 TYPE t_LogCodici           IS RECORD (CODICE               VARCHAR2(100),
                                       TIPO                 VARCHAR2(100),
                                       NOME                 VARCHAR2(100));
 TYPE t_TabLogCodici        IS TABLE OF t_LogCodici;

 TYPE t_StandardLog         IS RECORD (INIZIALIZZATO        NUMBER,
                                       RUN_ID               NUMBER,
                                       STORE_ON_FILE        BOOLEAN,
                                       FORCE_LOG_FILE       BOOLEAN,
                                       LOG_FILE_NAME        VARCHAR2(30),
                                       LOG_FILE_NAME_VRB    VARCHAR2(30),
                                       HEADTXT              VARCHAR2(100),
                                       INIZIO               TIMESTAMP WITH TIME ZONE,
                                       CLASSE               VARCHAR2(20),
                                       FUNZIONE             VARCHAR2(100),
                                       DESCRIZIONE          VARCHAR2(100),
                                       DATARIF              DATE,
                                       DATARIF_FINE         DATE,
                                       CODICI               t_TabLogCodici,
                                       ERRORE               NUMBER,
                                       MESSAGGIO            VARCHAR2(4000),
                                       INFO                 VARCHAR2(4000));


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION StdLogInit          (pClasseFunz    IN VARCHAR2,
                               pFunzione      IN VARCHAR2,
                               pDataRif       IN DATE     DEFAULT NULL,
                               pDataRif_fine  IN DATE     DEFAULT NULL,
                               pCodice        IN VARCHAR2 DEFAULT NULL,
                               pTipo          IN VARCHAR2 DEFAULT NULL,
                               pNome          IN VARCHAR2 DEFAULT NULL,
                               pDescrizione   IN VARCHAR2 DEFAULT NULL,
                               pStoreOnFile   IN BOOLEAN  DEFAULT TRUE,
                               pForceLogFile  IN BOOLEAN  DEFAULT TRUE) RETURN t_StandardLog;

 PROCEDURE StdLogInit         (pClasseFunz    IN VARCHAR2,
                               pFunzione      IN VARCHAR2,
                               pDataRif       IN DATE     DEFAULT NULL,
                               pDataRif_fine  IN DATE     DEFAULT NULL,
                               pCodice        IN VARCHAR2 DEFAULT NULL,
                               pTipo          IN VARCHAR2 DEFAULT NULL,
                               pNome          IN VARCHAR2 DEFAULT NULL,
                               pDescrizione   IN VARCHAR2 DEFAULT NULL,
                               pStoreOnFile   IN BOOLEAN  DEFAULT TRUE,
                               pForceLogFile  IN BOOLEAN  DEFAULT TRUE);

 PROCEDURE ResetLogCommonArea;


 PROCEDURE StdLogAddTxt       (pTipo          IN VARCHAR2,
                               pCodice        IN VARCHAR2,
                               pNome          IN VARCHAR2 DEFAULT NULL,
                               pLogArea   IN OUT t_StandardLog);

 PROCEDURE StdLogAddTxt       (pTipo          IN VARCHAR2,
                               pCodice        IN VARCHAR2,
                               pNome          IN VARCHAR2 DEFAULT NULL);

 PROCEDURE StdLogAddTxt       (pTesto         IN VARCHAR2,
                               pIndent        IN BOOLEAN  DEFAULT FALSE,
                               pErrore        IN NUMBER   DEFAULT NULL,
                               pLogArea   IN OUT t_StandardLog);

 PROCEDURE StdLogAddTxt       (pTesto         IN VARCHAR2,
                               pIndent        IN BOOLEAN  DEFAULT FALSE,
                               pErrore        IN NUMBER   DEFAULT NULL);

 PROCEDURE StdLogInfo         (pTesto         IN VARCHAR2,
                               pLogArea   IN OUT t_StandardLog,
                               pAggiungi      IN BOOLEAN DEFAULT TRUE) ;

 PROCEDURE StdLogInfo         (pTesto         IN VARCHAR2,
                               pAggiungi      IN BOOLEAN DEFAULT TRUE) ;

 PROCEDURE StdLogPrint        (pLogArea       IN t_StandardLog,
                               pLevel         IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF);

 PROCEDURE StdLogPrint        (pLevel         IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE TraceLog           (pLogArea       IN t_StandardLog,
                               pTrace         IN VARCHAR2,
                               pLevel         IN INTEGER  DEFAULT PKG_UtlGlb.gcTrace_INF,
                               pIndent        IN BOOLEAN  DEFAULT FALSE);

 PROCEDURE TraceLog           (pTrace         IN VARCHAR2,
                               pLevel         IN INTEGER  DEFAULT PKG_UtlGlb.gcTrace_INF,
                               pIndent        IN BOOLEAN  DEFAULT FALSE);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Logs;
/
