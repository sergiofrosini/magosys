CREATE OR REPLACE PACKAGE pkg_genera_file_geo AS
/* ***********************************************************************************************************
   NAME:       pkg_genera_file_geo
   PURPOSE:    Servizi per la creazione Anagrafica punti geolocalizzati
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      15/03/2013  Paolo Campi      Created this package.
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   NOTES:
*********************************************************************************************************** */
   cfilenamegeo CONSTANT VARCHAR2(50) := 'Anagrafica-punti-geo-localizzati.csv';
   cfilenamecorr CONSTANT VARCHAR2(50) := 'Corrispondenza-punti-P-A.csv';
   PROCEDURE genera_file_geo(v_filename IN VARCHAR2);
   PROCEDURE genera_file_correlazione(v_filename IN VARCHAR2);
   PROCEDURE genera_files;
END;
/
