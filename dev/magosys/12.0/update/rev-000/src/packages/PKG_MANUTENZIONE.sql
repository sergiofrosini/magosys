CREATE OR REPLACE PACKAGE PKG_MANUTENZIONE AS

/* ***********************************************************************************************************
   NAME:       PKG_Manutenzione
   PURPOSE:    Servizi per la manutenzione dell'applicazione

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.9.a.8    19/12/2014  Moretti C.       Pulizia Richieste e Indici KPI
                                           Lancio PKG_KPI.PuliziaRichieste
   1.9.a.13   10/03/2015  Moretti C.       Chiude e rilancia i Jobs Oracle in esecuzione da oltre 12 ore
   1.9c.1     04/08/2015  Moretti C.       Riorganizza indici dopo la pulizia (delete)
   1.9c.2     01/09/2015  Moretti C.       Pulizia tabella SESSION_STATISTICS

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE RebuildIndexes (pTable IN VARCHAR2);

 PROCEDURE PeriodicMaintenance;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Manutenzione;
/
