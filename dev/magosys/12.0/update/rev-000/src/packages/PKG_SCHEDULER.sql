CREATE OR REPLACE PACKAGE PKG_SCHEDULER AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- PROCEDURE AddJobLoadAnagr          (pDataRif         DATE,
--                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
--                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobLinearizzazione    (pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.ID_RETE%TYPE);

 PROCEDURE AddJobCalcMisStatiche    (pDataRif         DATE);

 PROCEDURE ConsolidaJobAggregazione (pTipo            VARCHAR2 DEFAULT NULL,
                                     pUseLog          BOOLEAN DEFAULT FALSE);

 PROCEDURE ConsolidaJobAggrMETEO;

 PROCEDURE ConsolidaJobAggrGME;

 PROCEDURE AddJobCalcAggregazione   (pTipoProcesso    VARCHAR2,
                                     pDataRif         DATE,
                                     pOrganizzazione  SCHEDULED_JOBS.ORGANIZZAZIONE%TYPE,
                                     pStato           SCHEDULED_JOBS.STATO%TYPE,
                                     pTipoRete        TIPI_RETE.COD_TIPO_RETE%TYPE,
                                     pTipoMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                     pDataIns         DATE DEFAULT NULL);

 PROCEDURE ElaboraJobs;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Scheduler;
/
