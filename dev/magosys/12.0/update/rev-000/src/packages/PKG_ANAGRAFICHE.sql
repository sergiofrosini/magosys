CREATE OR REPLACE PACKAGE PKG_ANAGRAFICHE AS

/* ***********************************************************************************************************
   NAME:       PKG_Anagrafiche
   PURPOSE:    Utilities e Definizioni schema CORELE (SOLO modalita' STM)

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      21/02/2012  Moretti C.       Created this package.
   1.0.d      20/04/2012  Moretti C.       Avanzamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.1.c      13/05/2013  Moretti C.       Controllo codici gestionali
   1.1.h      14/03/2014  Moretti C.       Eliminata forzatura introdotta in attesa della gestione
                                           file nea  IMTRAP.nea
   1.8.a.1    28/04/2014  Moretti C.       Ottimizzazione
   1.9.a.0    05/08/2014  Moretti C.       Gestione Centri satellite primo step
   1.9.a.3    24/08/2014  Moretti C.       Gestione Centri satellite implementazione finale
   1.9.a.9    13/02/2015  Moretti C.       Correzione per integrazione
   1.9.a.12   11/02/2015  Moretti C.       Correzione per integrazione
   1.9.a.14   16/06/2015  Moretti C.       Non elabora l'arrivo di un aggiornamento AUI
              23/06/2015  Moretti C.       Non elabora in caso di Aggiornamento AUI in corso
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   1.9c.2     01/07/2015  Moretti C.       Differenzuazione parametri in open cursore di vista V_GERARCHIA_AMMINISTRATIVA  per Romania
              24/08/2015  Moretti C.       Per Installazione Replay genera solo Gerarchia Elettrica in stato Attuale
   1.9c.3     18/11/2015  Moretti C.       Revisione controllo di sincronizzazione con elaborazione in corso AUI
                                           Ripristinata elaborazione al ricevimento dati da AUI
   1.10.1     02/02/2016  Moretti C.       Standardizzazione Codice gestionale Esercizi
                                           Utilizzo sinonimi per tabelle/viste esterne allo schema
   2.1.2      14/03/2017  Forno S. / Moretti C.  correzzione per variazione di rete con flag esercizio attivo
   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitApplMagoSTM     (pCodGestESE     IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                               pData           IN DATE DEFAULT TRUNC(SYSDATE));

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetListaAttualizzazioni (pRefCurs   OUT PKG_UtlGlb.t_query_cur) ;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElementsCheckReport (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pStato          IN INTEGER,
                               pData           IN DATE);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CheckCodGestCorele  (pStato          IN INTEGER,
                               pBlockOnly      IN BOOLEAN,
                               pData           IN DATE DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AllineaAnagrafica  (pElabSincrona   IN INTEGER DEFAULT PKG_UtlGlb.gkFlagON,
                               pForzaCompleta  IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOFF);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraGerarchia     (pData          IN DATE,
                                pStato         IN INTEGER,
                                pModAssetto    IN BOOLEAN,
                                pAUI           IN BOOLEAN,
                                pElabSincrona  IN BOOLEAN,
                                pForzaCompleta IN BOOLEAN);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Anagrafiche;
/
