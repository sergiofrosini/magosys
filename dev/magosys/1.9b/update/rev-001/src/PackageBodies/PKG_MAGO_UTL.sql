Prompt Package PKG_MAGO_UTL;
CREATE OR REPLACE PACKAGE BODY pkg_mago_utl AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_utl
   PURPOSE:    Servizi di Utility
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      17/09/2012  Paolo Campi      Created this package.
   NOTES:
*********************************************************************************************************** 
*/
 proc_name VARCHAR2(50);
FUNCTION checkRun ( pproc_name IN VARCHAR2 ) RETURN NUMBER AS
vrun NUMBER;
BEGIN 
   SELECT CASE WHEN run_flg = 0 AND (pproc_name = proc_name OR err_flg = 0) THEN 0 ELSE 1 END CASE--  se c'� errore posso solo risottomettere la proc in errore
    INTO vrun
    FROM application_run;
   RETURN vrun;
END;
PROCEDURE startRun AS
BEGIN
   proc_name := 'PKG_MAGO_UTL.STARTRUN';
   UPDATE application_run 
     SET run_id = run_id +1
        ,last_upd = SYSDATE;
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',null);     
   pkg_mago_utl.setrun(proc_name,0,0);
END;
PROCEDURE setRun ( pproc_name IN VARCHAR2, flag_run IN NUMBER, flag_err IN NUMBER) AS
vrun NUMBER;
BEGIN
   UPDATE application_run 
     SET proc_name = pproc_name 
        ,run_flg = flag_run
        ,err_flg = flag_err
        ,last_upd = SYSDATE
   RETURNING run_id
     INTO vrun;
  UPDATE log_history
     SET elapsed = CURRENT_TIMESTAMP - (logdate)
   WHERE run_id = vrun
     AND proc_name =pproc_name
     AND elapsed IS NULL;
END;
PROCEDURE insLog( pproc_name IN VARCHAR2, pmex IN VARCHAR2, errnum IN NUMBER) AS
vrun NUMBER;
BEGIN
    SELECT run_id 
      INTO vrun
      FROM application_run;
    INSERT INTO LOG_HISTORY
                ( run_id
                 ,logdate
                 ,proc_name
                 ,message
                 ,error_num
                )
         VALUES
                ( vrun
                 ,SYSDATE
                 ,pproc_name
                 ,pmex
                 ,errnum
                );
END;
FUNCTION getproduttorepuro RETURN VARCHAR2 AS
BEGIN
  RETURN  pkg_mago_utl.gcProduttorePuro;
END;
FUNCTION getproduttorecliente RETURN VARCHAR2 AS
BEGIN
  RETURN  pkg_mago_utl.gcProduttoreCliente;
END;
FUNCTION getproduttorenondeterm RETURN VARCHAR2 AS
BEGIN
  RETURN  pkg_mago_utl.gcProduttoreNonDeterm;
END;
FUNCTION getSeparatore RETURN VARCHAR2 AS
BEGIN
  RETURN  pkg_mago_utl.gcSeparatoreElem;
END;
FUNCTION leggiparam(pparam_name IN VARCHAR2) RETURN VARCHAR2 AS
parchar VARCHAR2(50);
BEGIN
   SELECT COALESCE(TO_CHAR(par_value_num),TO_CHAR(par_value_date,'dd/mm/yyyy hh24.mi.ss'),par_value_char)
     INTO parchar
     FROM cfg_application_parameter
    WHERE parameter_name = pparam_name;
   RETURN parchar;
END;
FUNCTION spacchettaparam(pproc_name IN VARCHAR2,param_num IN NUMBER, last_param OUT NUMBER) RETURN VARCHAR2 AS 
parname VARCHAR2(30);
BEGIN
   SELECT TRIM(SUBSTR(param,
                 CASE WHEN param_num = 1 THEN 1 
                 ELSE 
                    CASE WHEN instr(param,',',1,param_num-1) = 0 THEN LENGTH(param) ELSE CASE WHEN param_num = 1 THEN 1 ELSE instr(param,',',1,param_num-1)+1 END END
                 END 
                ,CASE WHEN INSTR(param,',',1,param_num) = 0 THEN 
                    CASE WHEN param_num = 1 THEN LENGTH(param) 
                    ELSE 
                       CASE WHEN INSTR(param,',',1,param_num-1) = 0 THEN 0
                       ELSE LENGTH(param) - INSTR(param,',',1,param_num-1) 
                       END
                    END
                 ELSE instr(param,',',param_num,1)- CASE WHEN param_num = 1 THEN 1 ELSE instr(param,',',1,param_num-1) END 
                 END
               )) param
                ,CASE WHEN instr(param,',',1,param_num) = 0 THEN 1 ELSE 0 END
     INTO parname, last_param
     FROM run_step
    WHERE proc_name = pproc_name;
   RETURN parname;
END;
FUNCTION convertiparam(pproc_name IN VARCHAR2,param_num IN NUMBER, pparvalue IN VARCHAR2) RETURN VARCHAR2 AS
parvalue VARCHAR2(250);
pardatatype VARCHAR2(50);
BEGIN
   SELECT data_type 
     INTO pardatatype   
     FROM user_arguments
    WHERE package_name = SUBSTR(pproc_name,1,INSTR(pproc_name,'.')-1)
      AND object_name = SUBSTR(pproc_name,INSTR(pproc_name,'.')+1,LENGTH(pproc_name)-INSTR(pproc_name,'.')+1)
      AND position = param_num;
   parvalue := CASE pardatatype WHEN 'CHAR' THEN '''' || pparvalue || ''''
                               WHEN 'VARCHAR2' THEN '''' || pparvalue || ''''
                               WHEN 'NUMBER' THEN pparvalue
                               WHEN 'DATE' THEN 'TO_DATE(''' || pparvalue || ''',''' ||  'dd/mm/yyyy hh24.mi.ss' || ''')'
                               WHEN 'TIMESTAMP' THEN 'TO_TIMESTAMP(''' || pparvalue || ''',''' ||  'dd/mm/yyyy hh24.mi.ss.FF' || ''')'
                               WHEN 'TIMESTAMP(3)' THEN 'TO_TIMESTAMP(''' || pparvalue || ''',''' ||  'dd/mm/yyyy hh24.mi.ss.FF' || ''')'
                END;
   RETURN parvalue;
END;
PROCEDURE run_application_group(gruppo IN VARCHAR2) AS
CURSOR c_data (pgruppo IN VARCHAR2) IS
       SELECT proc_name, param
         FROM run_step
        WHERE proc_group = pgruppo
        ORDER BY stepid;
rec_data c_data%rowtype;
vrun NUMBER;
vsql VARCHAR2(4000) := null;
i NUMBER := 0;
vlast NUMBER;
par_name VARCHAR2(50);
par_value VARCHAR2(250);
BEGIN
   IF c_data%isopen THEN
      CLOSE c_data;
   END IF;
   OPEN c_data(gruppo);
   FETCH c_data INTO rec_data;
   LOOP
      i := 0;
      IF c_data%FOUND THEN 
         vrun := pkg_mago_utl.checkrun(rec_data.proc_name);
      END IF;
      IF vrun != 0 THEN
         pkg_mago_utl.inslog(rec_data.proc_name,'Procedure running',99999);
      COMMIT; 
      END IF;
      EXIT WHEN c_data%NOTFOUND or vrun != 0;
         vsql := rec_data.proc_name;
         par_name := NULL;
         IF rec_data.param IS NOT NULL THEN
            vsql := vsql || '(';
            LOOP
               i := i + 1;
               par_name := spacchettaparam(rec_data.proc_name,i,vlast);
               IF par_name IS NOT NULL THEN
                  par_value := leggiparam(par_name);
                  par_value := convertiparam(rec_data.proc_name,i,par_value);
                  vsql := vsql || par_value || ',' ; 
               END IF;
            EXIT WHEN vlast = 1;
            END LOOP;
            vsql := SUBSTR(vsql,1,LENGTH(vsql)-1) || ')';
         END IF;
         EXECUTE IMMEDIATE ('Begin ' || vsql || '; End; ');
         FETCH c_data INTO rec_data;
   END LOOP;
 /*PKG_MAGO_DGF.startrun;
 PKG_MAGO_DGF.load_produttori;
 PKG_MAGO_DGF.load_def_solare;
 PKG_MAGO_DGF.load_def_eolico;
 PKG_MAGO_DGF.insert_element; 
 PKG_MAGO_DGF.insert_def(pdata); 
 PKG_MAGO_DGF.insert_def_eolico(pdata);
 PKG_MAGO_DGF.insert_def_solare(pdata);  
 PKG_MAGO_DGF.insert_trattamento_elementi;
 PKG_GERARCHIA_DGF.load_gerarchia_ecs_amm(pdata);
 PKG_GERARCHIA_DGF.load_gerarchia_ecs_geo(pdata);
 PKG_GERARCHIA_DGF.load_gerarchia_ecp(pdata);
 PKG_GERARCHIA_DGF.load_gerarchia_amm(pdata);
 PKG_GERARCHIA_DGF.load_gerarchia_geo(pdata);
 PKG_GERARCHIA_DGF.linearizza_gerarchia_geo(pdata);
 PKG_GERARCHIA_DGF.linearizza_gerarchia_amm(pdata);
 PKG_GERARCHIA_DGF.linearizza_gerarchia_imp(pdata);
 PKG_MAGO_DGF.load_misure;
 PKG_MAGO_DGF.insert_misure(10);
 PKG_MAGO_DGF.insert_rel_misure;
 PKG_MISURE.AddMisurePI(pdata,1);
 */
END;
END;
/