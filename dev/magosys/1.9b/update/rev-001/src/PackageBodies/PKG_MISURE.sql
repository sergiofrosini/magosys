PROMPT PACKAGE BODY PKG_MISURE;
--
-- PKG_MISURE  (Package Body) 
--
--  Dependencies: 
--   PKG_MISURE (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_MISURE AS
/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9.a.13
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gMisura                   PKG_GestAnagr.t_DefAnagr;
 --gUltimoTipoMisura         TIPI_MISURA.COD_TIPO_MISURA%TYPE := '*';
 gPrevData                 DATE := PKG_UtlGlb.gkDataTappo;

 --cTotalizza       CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagON);   -- Totalizza misure per tipo codice
 --cNonTotalizzare  CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagOFF);  -- Per il tipo codice non totalizzare le misure

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisuraOut     (pOrigineMis    IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                            pTipele        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                            pTipMisIn      IN TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_IN%TYPE,
                            pTipMisOut    OUT TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE,
                            pFattore      OUT NUMBER,
                            pFlagSplit    OUT TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE) RETURN BOOLEAN AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna le informazioni relative alla misura da memorizzare
-----------------------------------------------------------------------------------------------------------*/
    vTrovato   BOOLEAN := FALSE;
    vFormula   TIPI_MISURA_CONV_ORIG.FORMULA%TYPE;
    vTipMisOut TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
 BEGIN
    IF pTipele IS NOT NULL THEN
        BEGIN
            SELECT FORMULA,  COD_TIPO_MISURA_OUT, FLG_SPLIT
              INTO vFormula, pTipMisOut,          pFlagSplit
              FROM TIPI_MISURA_CONV_ORIG
             WHERE ORIGINE = pOrigineMis
               AND COD_TIPO_MISURA_IN = pTipMisIn
               AND COD_TIPO_ELEMENTO = pTipele;
            vTrovato := TRUE;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vTrovato := FALSE;
            WHEN OTHERS THEN RAISE;
        END;
    END IF;
    IF NOT vTrovato THEN
        BEGIN
            SELECT FORMULA,  COD_TIPO_MISURA_OUT, FLG_SPLIT
              INTO vFormula, pTipMisOut,          pFlagSplit
              FROM TIPI_MISURA_CONV_ORIG
             WHERE ORIGINE = pOrigineMis
               AND COD_TIPO_MISURA_IN = pTipMisIn;
            vTrovato := TRUE;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vTrovato := FALSE;
            WHEN OTHERS THEN RAISE;
        END;
    END IF;
    IF vTrovato THEN
        IF vFormula IS NOT NULL THEN
            EXECUTE IMMEDIATE 'SELECT '||vFormula||' FROM DUAL' INTO pFattore USING 1;
        ELSE
            pFattore := 1;
        END IF;
    END IF;
    RETURN vTrovato;
 EXCEPTION
    WHEN OTHERS THEN RAISE;
 END GetMisuraOut;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisuraAcq         (pTipoProcesso  IN VARCHAR2,
                                 pCodTrtEle     IN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                 pDataMis       IN MISURE_ACQUISITE.DATA%TYPE,
                                 pTipoMis       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pValore        IN MISURE_ACQUISITE.VALORE%TYPE,
                                 pTipRete       IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 pAggrega       IN BOOLEAN,
                                 pResult       OUT INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza la misura:
     - Si inserisce la misura; se esiste si controlla se va aggiornata.
     - Si esegue il controllo sulle date da aggregare.
-----------------------------------------------------------------------------------------------------------*/
    vResult NUMBER;
    vValore NUMBER(20,4) := ROUND(pValore,4);
    vPerc   NUMBER(5,2):= 100;
 BEGIN

     pResult := PKG_Mago.gcNoOper;
     BEGIN
        IF PKG_Mago.IsMagoDGF THEN
            SELECT PERCENTUALE
              INTO vPerc
              FROM MANUTENZIONE man
                   , TRATTAMENTO_ELEMENTI tr
              WHERE tr.COD_TRATTAMENTO_ELEM = pCodTrtEle
                AND tr.COD_ELEMENTO = man.COD_ELEMENTO
                AND pDataMis >= man.DATA_INIZIO
                AND pDataMis < NVL(man.DATA_FINE,TO_DATE('01013000','ddmmyyyy'))
                AND NVL(man.FLG_DELETED,0) != 1;
        END IF;
     EXCEPTION WHEN NO_DATA_FOUND THEN
        vPerc := 100;
     END;
     IF PKG_Misure.IsMisuraStatica(pTipoMis) = PKG_UtlGlb.gkFlagOn THEN
         IF gPrevData <> pDataMis THEN
             gPrevData := pDataMis;
             gMisura := NULL;
             PKG_GestAnagr.InitTab(gMisura,pDataMis,USER,'MISURE_ACQUISITE_STATICHE','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
             PKG_GestAnagr.AddCol (gMisura,'COD_TRATTAMENTO_ELEM',PKG_GestAnagr.cColChiave);
             PKG_GestAnagr.AddCol (gMisura,'VALORE',PKG_GestAnagr.cColAttributo);
         END IF;
         PKG_GestAnagr.InitRow(gMisura);
         PKG_GestAnagr.AddVal (gMisura, 'COD_TRATTAMENTO_ELEM', pCodTrtEle);
         vValore := ROUND(vValore * (vperc / 100),4);
         PKG_GestAnagr.AddVal (gMisura, 'VALORE', vValore);
         PKG_GestAnagr.Elabora(gMisura,vResult);
         CASE vResult
              WHEN PKG_GestAnagr.cInsertito  THEN pResult := PKG_Mago.gcInserito;
              WHEN PKG_GestAnagr.cModificato THEN pResult := PKG_Mago.gcModificato;
              ELSE pResult := PKG_Mago.gcNoOper;
         END CASE;
     ELSE
        BEGIN
            IF vperc != 0 THEN
               vValore := ROUND(vValore * (vperc / 100),4);
            END IF;
            INSERT INTO MISURE_ACQUISITE(COD_TRATTAMENTO_ELEM
                                        ,DATA
                                        ,VALORE)
                                  VALUES(pCodTrtEle
                                        ,pDataMis
                                        ,vValore);
            pResult := PKG_Mago.gcInserito;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN
                 UPDATE MISURE_ACQUISITE
                    SET VALORE  = vValore
                  WHERE COD_TRATTAMENTO_ELEM = pCodTrtEle
                    AND DATA = pDataMis
                    AND VALORE != vValore;
                 IF SQL%ROWCOUNT > 0 THEN
                      pResult := PKG_Mago.gcModificato;
                 END IF;
            WHEN OTHERS THEN
             RAISE;
        END;
     END IF;

     IF pAggrega THEN
         IF pResult IN (PKG_Mago.gcModificato,PKG_Mago.gcInserito) THEN
             -- Richiesta di calcolo aggregate
             CASE
                WHEN PKG_Mago.IsMagoSTM THEN
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     IF pTipRete <> PKG_Mago.gcTipReteAT THEN
                         PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                         PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     END IF;
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                WHEN PKG_Mago.IsMagoDGF THEN
                     -- inizio : DGF non utilizza la gerarchia eletterica
                     -- PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     -- fine   : DGF non utilizza la gerarchia eletterica
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                ELSE
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoAttuale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
                     PKG_Scheduler.AddJobCalcAggregazione(pTipoProcesso,pDataMis,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale,pTipRete,pTipoMis);
             END CASE;
         END IF;
     END IF;
     RETURN;
 END AddMisuraAcq;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGmeFgl(pTipoProcesso IN VARCHAR2,
                           pOrigineMis   IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                           pCodEle       IN ELEMENTI.COD_ELEMENTO%TYPE,
                           pGstEle       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pTipEle       IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                           pTipRete      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                           pTipFte       IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pAggrega      IN BOOLEAN,
                           pNumIns      OUT INTEGER,
                           pNumMod      OUT INTEGER,
                           pNumPre      OUT INTEGER,
                           pNumKO       OUT INTEGER,
                           pMisure       IN T_MISURA_GME_ARRAY,
                           pLog      IN OUT PKG_Logs.t_StandardLog) AS
/*-----------------------------------------------------------------------------------------------------------
    Ripartisce la misura sui figli dell'elemento ricevuto
    . i contolli su omogeneita' del tipo fonte devono essere eseguiti a monte
-----------------------------------------------------------------------------------------------------------*/

  vResult    INTEGER;

  vNomEle    ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vTipEle    ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
  vTipRet    TIPI_RETE.COD_TIPO_RETE%TYPE;
  vTipCli    ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vPotInst   NUMBER;

  vTrtEle    TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vTipMisOK  BOOLEAN;
  vTipMis    TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;
  vFattore   NUMBER;

  vTxtLog    VARCHAR2(10);

  vSavMis1   TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vSavMis2   TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vSavCod    ELEMENTI.COD_ELEMENTO%TYPE := -1;

BEGIN

    pNumIns := 0;
    pNumMod := 0;
    pNumPre := 0;
    pNumKO  := 0;

    SELECT COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE, NOME_ELEMENTO, COD_TIPO_RETE
      INTO vTipEle,           vTipCli,             vNomEle,       vTipRet
      FROM (SELECT COD_ELEMENTO,D.COD_TIPO_ELEMENTO,COD_TIPO_CLIENTE,COD_TIPO_FONTE,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_RETE,
                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
              FROM ELEMENTI_DEF D
             INNER JOIN ELEMENTI USING(COD_ELEMENTO)
             INNER JOIN TIPI_ELEMENTO T ON  T.COD_TIPO_ELEMENTO = D.COD_TIPO_ELEMENTO
             WHERE COD_ELEMENTO = pCodEle
            )
      WHERE ORD = 1;

    IF vTipRet IS NULL THEN
        PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Rete non definito' ,FALSE,gkAnagrIncompl);
        RETURN;
    END IF;
    IF vTipCli IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vTipCli := PKG_Mago.gcClientePrdNonApplic;
        ELSE
            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
        END IF;
    END IF;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP

        IF vSavMis1 <> pMisure(i).TIPO_MISURA THEN
           vSavMis1 := pMisure(i).TIPO_MISURA;
            vTipMisOK := GetMisuraOut(pOrigineMis,pTipEle,pMisure(i).TIPO_MISURA,vTipMis,vFattore,vFlagSplit);
        END IF;

        IF vTipMisOK THEN
            FOR x IN (SELECT COD_GEST_ELEMENTO,
                             COD_ELEMENTO_FIGLIO COD_ELEMENTO,
                             POTENZA_INSTALLATA,
                             SUM(POTENZA_INSTALLATA) OVER (PARTITION BY COD_ELEMENTO_PADRE) POT_TOT
                        FROM (SELECT COD_ELEMENTO COD_ELEMENTO_FIGLIO,COD_ELEMENTO_PADRE,
                                     COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_GEST_ELEMENTO
                                FROM (SELECT COD_ELEMENTO COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO COD_GESTIONALE_PADRE,
                                             ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                        FROM ELEMENTI_DEF
                                       INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                                       WHERE COD_ELEMENTO = pCodEle
                                     ) P
                               INNER JOIN ELEMENTI_DEF ON ID_ELEMENTO = P.COD_GESTIONALE_PADRE
                               INNER JOIN ELEMENTI     USING(COD_ELEMENTO)
                               WHERE ORD = 1
                             )
                     )
            LOOP
                IF (vSavMis2 <> pMisure(i).TIPO_MISURA) OR (vSavCod <> x.COD_ELEMENTO) THEN
                    vSavMis2 := pMisure(i).TIPO_MISURA;
                    vSavCod  := x.COD_ELEMENTO;
                    vTrtEle := GetTrattamentoElemento(x.COD_ELEMENTO,
                                                      vTipEle,
                                                      vTipMis,
                                                      pTipFte,
                                                      vTipRet,
                                                      vTipCli,
                                                      PKG_Mago.gcOrganizzazELE,
                                                      PKG_Mago.gcStatoNullo);
                END IF;
                AddMisuraAcq(pTipoProcesso,
                             vTrtEle,
                             pMisure(i).DATA_MIS,
                             vTipMis,
                             ROUND(((pMisure(i).VALORE * X.POTENZA_INSTALLATA) / X.POT_TOT) * vFattore,4),
                             pMisure(i).TIPO_RETE,
                             pAggrega,
                             vResult);

                CASE vResult
                    WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                    WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                    ELSE pNumPre := pNumPre + 1;
                END CASE;

            END LOOP;
        ELSE
            pNumKO := pNumKO + 1;
--            PKG_Logs.StdLogAddTxt('Non trovata associazione con Tipo Misura '||pMisure(i).TIPO_MISURA||'. '||
--                                  'Le misure di tipo '||pMisure(i).TIPO_MISURA||' vengono scartate!',FALSE,gkWarning);
        END IF;

    END LOOP;
 END AddMisureGmeFgl;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGmeEle(pTipoProcesso IN VARCHAR2,
                           pOrigineMis   IN TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE,
                           pCodEle       IN ELEMENTI.COD_ELEMENTO%TYPE,
                           pGstEle       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                           pTipEle       IN ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                           pTipRete      IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                           pFonteDef     IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pFonteSec     IN ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                           pAggrega      IN BOOLEAN,
                           pNumIns      OUT INTEGER,
                           pNumMod      OUT INTEGER,
                           pNumPre      OUT INTEGER,
                           pNumKO       OUT INTEGER,
                           pMisure       IN T_MISURA_GME_ARRAY,
                           pLog      IN OUT PKG_Logs.t_StandardLog) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure GME/SMILE sull'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
  vCodMisST  TIPI_MISURA.CODIFICA_ST%TYPE := '_';
  vResult    INTEGER;
  vTipMisOK  BOOLEAN;

  vCodFte    ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
  vTipCli    ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vNomEle    ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vTipRte    TIPI_RETE.COD_TIPO_RETE%TYPE;

  vTrtEle    TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;
  vTrtEleSec TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vFattore   NUMBER;
  vTipMis    TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

BEGIN
    pNumIns := 0;
    pNumMod := 0;
    pNumPre := 0;
    pNumKO  := 0;

    SELECT COD_TIPO_RETE, COD_TIPO_FONTE, COD_TIPO_CLIENTE, NOME_ELEMENTO
      INTO vTipRte,       vCodFte,        vTipCli,          vNomEle
      FROM (SELECT COD_ELEMENTO,E.COD_TIPO_ELEMENTO,COD_TIPO_CLIENTE,COD_TIPO_FONTE,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_RETE,
                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
              FROM ELEMENTI_DEF
             INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
             INNER JOIN TIPI_ELEMENTO T ON  T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
             WHERE COD_ELEMENTO = pCodEle
            )
      WHERE ORD = 1;

    IF pFonteDef IS NOT NULL THEN
        vCodFte := pFonteDef;
    END IF;
    IF vTipRte IS NULL THEN
        PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Rete non definito' ,FALSE,gkAnagrIncompl,pLog);
        PKG_Logs.TraceLog('---->  2a.    tipo rete per = '|| pMisure(1).GEST_ELEM|| '    non definito - errore '||gkAnagrIncompl,PKG_UtlGlb.gcTrace_VRB);
        RETURN;
    END IF;
    IF vCodFte IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
        ELSE
            vCodFte := PKG_Mago.gcRaggrFonteNonDisp;
        END IF;
--        IF pTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
--            PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Fonte non definito' ,FALSE,gkAnagrIncompl);
--            RETURN;
--        ELSE
--            vCodFte := PKG_Mago.gcRaggrFonteNonDisp;
--        END IF;
    END IF;
    IF vTipCli IS NULL THEN
        IF pTipRete = PKG_Mago.gcTipReteAT THEN
            vTipCli := PKG_Mago.gcClientePrdNonApplic;
        ELSE
            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
        END IF;
--        IF pTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
--            PKG_Logs.StdLogAddTxt('Elemento '|| pMisure(1).GEST_ELEM ||'/'||pGstEle||' ('||vNomEle||') - Tipo Produttore non definito' ,FALSE,gkAnagrIncompl);
--            RETURN;
--        ELSE
--            vTipCli := PKG_Mago.gcClientePrdNonDeterm;
--        END IF;
    END IF;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP
        IF vCodMisST   <> pMisure(i).TIPO_MISURA THEN
           vCodMisST   := pMisure(i).TIPO_MISURA;
            vTipMisOK  := GetMisuraOut(pOrigineMis,pTipEle,pMisure(i).TIPO_MISURA,vTipMis,vFattore,vFlagSplit);
            vTrtEle    := NULL;
            vTrtEleSec := NULL;
            IF vTipMisOK THEN
                vTrtEle := GetTrattamentoElemento(pCodEle,
                                                  pTipEle,
                                                  vTipMis,
                                                  vCodFte,
                                                  vTipRte,
                                                  vTipCli,
                                                  PKG_Mago.gcOrganizzazELE,
                                                  PKG_Mago.gcStatoNullo);
                IF pFonteSec IS NOT NULL THEN
                    IF pFonteDef = PKG_Mago.gcRaggrFonteNonOmog THEN
                        IF vTipMis = PKG_Mago.gcPotenzaAttScambiata THEN
                            vTrtEleSec := GetTrattamentoElemento(pCodEle,
                                                                 pTipEle,
                                                                 vTipMis,
                                                                 pFonteSec,
                                                                 vTipRte,
                                                                 vTipCli,
                                                                 PKG_Mago.gcOrganizzazELE,
                                                                 PKG_Mago.gcStatoNullo);
                        END IF;
                    END IF;
                END IF;
            --ELSE
            --    PKG_Logs.TraceLog(CHR(9)||'Warning: Non trovata associazione con Tipo Misura '||pMisure(i).TIPO_MISURA||'. '||
            --                              'Le misure di tipo '||pMisure(i).TIPO_MISURA||' vengono scartate!',PKG_UtlGlb.gcTrace_ERR);
            END IF;
        END IF;
        IF vTipMisOK THEN
            AddMisuraAcq(pTipoProcesso,
                         vTrtEle,
                         pMisure(i).DATA_MIS,
                         vTipMis,
                         ROUND(pMisure(i).VALORE * vFattore,4),
                         pMisure(i).TIPO_RETE,
                         pAggrega,
                         vResult);
            CASE vResult
                WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                ELSE pNumPre := pNumPre + 1;
            END CASE;
            IF vTrtEleSec IS NOT NULL THEN
                AddMisuraAcq(pTipoProcesso,
                             vTrtEleSec,
                             pMisure(i).DATA_MIS,
                             vTipMis,
                             ROUND(pMisure(i).VALORE * vFattore,4),
                             pMisure(i).TIPO_RETE,
                             pAggrega,
                             vResult);
                CASE vResult
                    WHEN PKG_Mago.gcInserito   THEN pNumIns := pNumIns + 1;
                    WHEN PKG_Mago.gcModificato THEN pNumMod := pNumMod + 1;
                    ELSE pNumPre := pNumPre + 1;
                END CASE;
            END IF;
        ELSE
            pNumKO := pNumKO + 1;
        END IF;
    END LOOP;

 END AddMisureGmeEle;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION CompletaSqlMisure(pSql              IN VARCHAR2,
                            pOrganizzazione   IN NUMBER,
                            pStatoRete        IN NUMBER,
                            pTable            IN VARCHAR2,
                            pRaggrMis         IN CHAR,
                            pTipiFonte        IN NUMBER,
                            pTipiRete         IN NUMBER,
                            pTipiClie         IN NUMBER,
                            pGerECS           IN NUMBER,
                            pTotTipFon        IN NUMBER,
                            pDataDa           IN DATE,
                            pDataA            IN DATE) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce lo statement SQL per la selezione delle misure
-----------------------------------------------------------------------------------------------------------*/
    vSql            VARCHAR2(4000);
    vOrganizzazione INTEGER := pOrganizzazione;
    vStatoRete      INTEGER := pStatoRete;
 BEGIN
    vSql := REPLACE(pSql,'#AGR#',pRaggrMis);
    vSql := REPLACE(vSql,'#TAB#',pTable);

    IF pGerECS = 1 THEN
        vOrganizzazione  := PKG_Mago.gcOrganizzazELE;
    END IF;

    IF NVL(pTipiFonte,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_FONTI#','INNER JOIN TIPO_FONTI ft USING(COD_TIPO_FONTE) '||
                                                 'INNER JOIN RAGGRUPPAMENTO_FONTI rg USING(COD_RAGGR_FONTE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_FONTI#','AND BITAND(:fte,ID_RAGGR_FONTE) = ID_RAGGR_FONTE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_FONTI#','INNER JOIN TIPO_FONTI ft USING(COD_TIPO_FONTE) ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_FONTI#','AND :fte <> -1 ');
    END IF;

    IF NVL(pTipiRete,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#','INNER JOIN TIPI_RETE rt USING(COD_TIPO_RETE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND BITAND(:rte,ID_RETE) = ID_RETE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#',' ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND :rte <> -1 ');
    END IF;

    IF NVL(pTipiClie,0) <> 0 THEN
        vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#','INNER JOIN TIPI_CLIENTE cli USING(COD_TIPO_CLIENTE)');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND BITAND(:cli,ID_CLIENTE) = ID_CLIENTE ');
    ELSE
        vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#',' ');
        vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND :cli <> -1 ');
    END IF;

    IF pTotTipFon IS NOT NULL THEN
        IF pTotTipFon = PKG_Misure.cMisTot THEN
            vSql := REPLACE(vSql,'#FONTE1#','''0'' COD_RAGGR_FONTE');
            vSql := REPLACE(vSql,'#FONTE2#',' ');
        ELSE
            vSql := REPLACE(vSql,'#FONTE1#','COD_RAGGR_FONTE');
            vSql := REPLACE(vSql,'#FONTE2#',',COD_RAGGR_FONTE');
        END IF;
    END IF;

    --IF pTable = 'ACQUISITE' THEN
    --    vSql := REPLACE(vSql,'#FILT_ELEM_NULL#','');
    --ELSE
    --    vSql := REPLACE(vSql,'#FILT_ELEM_NULL#','AND B.COD_ELEMENTO IS NOT NULL');
    --END IF;

    IF vOrganizzazione IS NOT NULL AND pStatoRete IS NOT NULL THEN
        --IF PKG_Mago.IsMagoSTM AND vOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
        --    vStatoRete  := PKG_Mago.gcStatoAttuale;
        --END IF;
        CASE vOrganizzazione
            WHEN PKG_Mago.gcOrganizzazELE THEN
                    vSql := REPLACE(vSql,'#GER#','IMP');
                    CASE vStatoRete
                        WHEN PKG_Mago.gcStatoAttuale THEN
                            vSql := REPLACE(vSql,'#STA#','_SA');
                        WHEN PKG_Mago.gcStatoNormale THEN
                            vSql := REPLACE(vSql,'#STA#','_SN');
                    END CASE;
            WHEN PKG_Mago.gcOrganizzazGEO THEN
                    vSql := REPLACE(vSql,'#GER#','GEO');
            WHEN PKG_Mago.gcOrganizzazAMM THEN
                    vSql := REPLACE(vSql,'#GER#','AMM');
        END CASE;
        vSql := REPLACE(vSql,'#STA#','');
    END IF;

    IF (pDataDa IS NULL) OR (pDataA IS NULL) THEN
        vSql := REPLACE(vSql,'#PART#','');
    ELSE
        IF TO_CHAR(pDataDa,'yyyymm') = TO_CHAR(pDataA,'yyyymm') THEN
            vSql := REPLACE(vSql,'#PART#',PKG_UtlGlb.GetPartitionName (pDataDa, 'M', TRUE));
        ELSE
            vSql := REPLACE(vSql,'#PART#','');
        END IF;
    END IF;

    RETURN vSql;

 END CompletaSqlMisure;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetTabMisure          (pCodElemento      IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE
                                 )
                          RETURN REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    determina su quale/i tabelle misure selezionare le curve per l'elemento e per i tipi misura definiti in GTTD
-----------------------------------------------------------------------------------------------------------*/
    vTabMisure REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE := NULL;

 BEGIN

    IF pTipMisura IS NULL THEN
         SELECT NVL(SUM(TAB_MISURE),0)
           INTO vTabMisure
           FROM (SELECT DISTINCT TAB_MISURE
                   FROM REL_ELEMENTO_TIPMIS
                  INNER JOIN (SELECT ALF1 COD_TIPO_MISURA
                                FROM GTTD_VALORI_TEMP
                               WHERE TIP = PKG_Mago.gcTmpTipMisKey
                             ) USING(COD_TIPO_MISURA)
                 WHERE COD_ELEMENTO = pCodElemento
                );
            IF vTabMisure > PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate THEN
                vTabMisure := PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate;
            END IF;
    ELSE

         SELECT NVL(SUM(TAB_MISURE),0)
           INTO vTabMisure
           FROM (SELECT DISTINCT TAB_MISURE
                   FROM REL_ELEMENTO_TIPMIS
                  WHERE COD_ELEMENTO = pCodElemento
                    AND COD_TIPO_MISURA = pTipMisura
                );
            IF vTabMisure > PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate THEN
                vTabMisure := PKG_Mago.gcTabAcquisite + PKG_Mago.gcTabAggregate;
            END IF;
    END IF;

    RETURN vTabMisure;

 END GetTabMisure;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureAcqStatiche (pData             IN DATE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pOrganizzazione   IN INTEGER,
                                 pStato            IN INTEGER,
                                 pAggrega          IN BOOLEAN,
                                 pElabImmediata    IN INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure di potenza installata per i generatori AT, MT, BT presenti
-----------------------------------------------------------------------------------------------------------*/

     vLog          PKG_Logs.t_StandardLog;

     vResult       INTEGER;
     vNumIns       INTEGER := 0;
     vNumMod       INTEGER := 0;
     vNumKO        INTEGER := 0;
     vMisIni       INTEGER := 0;
     vNumEleCur    NUMBER  := 0;

     vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE;
     vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

     vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

     vTipMisOut    TIPI_MISURA.COD_TIPO_MISURA%TYPE;
     vFattore      NUMBER;

 BEGIN

    CASE pTipMis
        WHEN PKG_Mago.gcPotenzaInstallata THEN
             vOrigineMis := 'POT_INST';
        WHEN PKG_Mago.gcNumeroImpianti THEN
             vOrigineMis := 'NUM_IMP';
    END CASE;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassCAL,
                                pFunzione     => 'PKG_Misure.AddMisureAcqStatiche'||'/'||pTipMis ,
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pDataRif      => pData,
                                pStoreOnFile  => FALSE);

    IF NOT GetMisuraOut(vOrigineMis,NULL,pTipMis,vTipMisOut,vFattore,vFlagSplit) THEN
        RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Associazione Origine della Misura / Tipo elemento ('||vOrigineMis||'/'||vTipMisOut||') non definita!');
    END IF;

    SELECT COUNT(*)
      INTO vMisIni
      FROM TRATTAMENTO_ELEMENTI T
     INNER JOIN MISURE_ACQUISITE_STATICHE M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                           AND pData BETWEEN M.DATA_ATTIVAZIONE
                                                         AND M.DATA_DISATTIVAZIONE
     WHERE COD_TIPO_MISURA = pTipMis
       AND ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
       AND TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo;

    DELETE GTTD_MISURE;

    CASE pTipMis

        WHEN PKG_Mago.gcPotenzaInstallata THEN
            INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,COD_ELEMENTO,COD_TIPO_FONTE,
                                     COD_TIPO_CLIENTE,COD_TIPO_RETE,VALORE,COD_TIPO_ELEM)
                 SELECT T.COD_TRATTAMENTO_ELEM,
                        A.COD_ELEMENTO,
                        A.COD_TIPO_FONTE,
                        D.COD_TIPO_CLIENTE,
                        A.COD_TIPO_RETE,
                        A.POTENZA_INSTALLATA,
                        A.COD_TIPO_ELEMENTO
                   FROM (SELECT F.COD_ELEMENTO COD_ELEMENTO_PADRE,
                                E.COD_ELEMENTO,
                                D.COD_TIPO_FONTE,
                                R.COD_TIPO_RETE,
                                E.COD_TIPO_ELEMENTO,
                                (NVL(D.POTENZA_INSTALLATA,0) * NVL(D.FATTORE,1)) POTENZA_INSTALLATA,
                                ROW_NUMBER() OVER (PARTITION BY E.COD_ELEMENTO
                                                       ORDER BY E.COD_ELEMENTO,D.DATA_ATTIVAZIONE DESC) ORD
                           FROM ELEMENTI E
                          INNER JOIN ELEMENTI_DEF  D ON D.COD_ELEMENTO = E.COD_ELEMENTO
                                                    AND pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                          INNER JOIN ELEMENTI      F ON F.COD_GEST_ELEMENTO = NVL(D.ID_ELEMENTO, E.COD_GEST_ELEMENTO)
                          INNER JOIN TIPI_ELEMENTO R ON R.COD_TIPO_ELEMENTO = F.COD_TIPO_ELEMENTO
                          WHERE D.COD_TIPO_FONTE IS NOT NULL
                            AND E.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,
                                                        PKG_Mago.gcGeneratoreMT,
                                                        PKG_Mago.gcGeneratoreBT,
                                                        PKG_Mago.gcTrasformMtBtDett)
                            AND NVL(D.POTENZA_INSTALLATA,0) > 0
                        ) A
                  INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = COD_ELEMENTO_PADRE
                   LEFT OUTER JOIN TRATTAMENTO_ELEMENTI T ON T.COD_ELEMENTO = A.COD_ELEMENTO
                                                         AND T.COD_TIPO_MISURA = PKG_Mago.gcPotenzaInstallata
                                                         AND T.COD_TIPO_FONTE = A.COD_TIPO_FONTE
                                                         AND T.COD_TIPO_RETE  = A.COD_TIPO_RETE
                                                         AND T.COD_TIPO_CLIENTE = D.COD_TIPO_CLIENTE --- <-----------
                                                         AND T.ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
                                                         AND T.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                  WHERE pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                    AND A.ORD = 1;
            vNumEleCur := SQL%ROWCOUNT;

        WHEN PKG_Mago.gcNumeroImpianti THEN
            INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,COD_ELEMENTO,COD_TIPO_FONTE,
                                     COD_TIPO_CLIENTE,COD_TIPO_RETE,VALORE,COD_TIPO_ELEM)
                 SELECT R.COD_TRATTAMENTO_ELEM,
                        E.COD_ELEMENTO,
                        A.COD_TIPO_FONTE,
                        D.COD_TIPO_CLIENTE,
                        T.COD_TIPO_RETE,
                        SUM(A.NUM_IMPIANTI) NUM_IMPIANTI,
                        T.COD_TIPO_ELEMENTO
                   FROM (SELECT D.ID_ELEMENTO COD_GEST_ELEMENTO,
                                D.COD_TIPO_FONTE,
                                1 NUM_IMPIANTI
                           FROM ELEMENTI E
                          INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO
                          WHERE E.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,
                                                        PKG_Mago.gcGeneratoreMT,
                                                        PKG_Mago.gcGeneratoreBT)
                            AND pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                            AND D.COD_TIPO_FONTE IS NOT NULL
                            AND NVL(D.POTENZA_INSTALLATA,0) > 0
                         UNION ALL
                         SELECT D.ID_ELEMENTO COD_GEST_ELEMENTO,
                                D.COD_TIPO_FONTE,
                                D.NUM_IMPIANTI
                           FROM ELEMENTI E
                          INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                          WHERE E.COD_TIPO_ELEMENTO = PKG_Mago.gcTrasformMtBtDett
                            AND pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                            AND D.COD_TIPO_FONTE IS NOT NULL
                            AND NVL(D.NUM_IMPIANTI,0) > 0
                        ) A
                  INNER JOIN ELEMENTI E ON E.COD_GEST_ELEMENTO = A.COD_GEST_ELEMENTO
                  INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO
                                           AND pData BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                  INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                   LEFT OUTER JOIN TRATTAMENTO_ELEMENTI R ON R.COD_ELEMENTO = E.COD_ELEMENTO
                                                         AND R.COD_TIPO_MISURA = PKG_Mago.gcNumeroImpianti
                                                         AND R.COD_TIPO_FONTE = A.COD_TIPO_FONTE
                                                         AND R.COD_TIPO_RETE  = T.COD_TIPO_RETE
                                                         AND R.ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
                                                         AND R.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                  GROUP BY R.COD_TRATTAMENTO_ELEM, E.COD_ELEMENTO, A.COD_TIPO_FONTE,
                        D.COD_TIPO_CLIENTE, T.COD_TIPO_RETE, T.COD_TIPO_ELEMENTO;
            SELECT COUNT(*) INTO vNumEleCur FROM (SELECT DISTINCT COD_ELEMENTO FROM GTTD_MISURE);

    END CASE;

    -- attribuisce il trattamento elemento ai nuovi elementi
    FOR i IN (SELECT COD_ELEMENTO,
                     COD_TIPO_FONTE,
                     COD_TIPO_CLIENTE,
                     COD_TIPO_RETE,
                     VALORE,
                     COD_TIPO_ELEM
                FROM GTTD_MISURE
               WHERE COD_TRATTAMENTO_ELEM IS NULL) LOOP
            vCodTratElem := GetTrattamentoElemento(i.COD_ELEMENTO,
                                                   i.COD_TIPO_ELEM,
                                                   vTipMisOut,
                                                   i.COD_TIPO_FONTE,
                                                   i.COD_TIPO_RETE,
                                                   i.COD_TIPO_CLIENTE,
                                                   PKG_Mago.gcOrganizzazELE,
                                                   PKG_Mago.gcStatoNullo);
            UPDATE GTTD_MISURE SET COD_TRATTAMENTO_ELEM = vCodTratElem
             WHERE COD_ELEMENTO = i.COD_ELEMENTO;
    END LOOP;

    -- Gestisce i valori nuovi o modificati
    FOR i IN (SELECT COD_TRATTAMENTO_ELEM, COD_TIPO_RETE, VALORE
                FROM GTTD_MISURE
              MINUS
              SELECT T.COD_TRATTAMENTO_ELEM, COD_TIPO_RETE , M.VALORE
                FROM TRATTAMENTO_ELEMENTI T
               INNER JOIN MISURE_ACQUISITE_STATICHE M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                                       AND pData BETWEEN M.DATA_ATTIVAZIONE
                                                                     AND M.DATA_DISATTIVAZIONE
               WHERE T.COD_TIPO_MISURA = vTipMisOut
                 AND T.ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
                 AND T.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
             ) LOOP
        AddMisuraAcq(NULL,
                     i.COD_TRATTAMENTO_ELEM,
                     pData,
                     vTipMisOut,
                     i.VALORE * vFattore,
                     i.COD_TIPO_RETE,
                     pAggrega,
                     vResult);
        CASE vResult
             WHEN PKG_Mago.gcInserito   THEN vNumIns := vNumIns + 1;
             WHEN PKG_Mago.gcModificato THEN vNumMod := vNumMod + 1;
             ELSE NULL;
        END CASE;
    END LOOP;

    -- Disattiva i valori non piu' validi
    FOR i IN (SELECT T.COD_TRATTAMENTO_ELEM
                FROM TRATTAMENTO_ELEMENTI T
               INNER JOIN MISURE_ACQUISITE_STATICHE M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
                                                       AND pData BETWEEN M.DATA_ATTIVAZIONE
                                                                     AND M.DATA_DISATTIVAZIONE
               WHERE T.COD_TIPO_MISURA = vTipMisOut
                 AND T.ORGANIZZAZIONE = PKG_Mago.gcOrganizzazELE
                 AND T.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
              MINUS
              SELECT COD_TRATTAMENTO_ELEM
                FROM GTTD_MISURE
             ) LOOP
        UPDATE MISURE_ACQUISITE_STATICHE SET DATA_DISATTIVAZIONE = pData - (1 / 86400)
         WHERE COD_TRATTAMENTO_ELEM = i.COD_TRATTAMENTO_ELEM
           AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        vNumKO := vNumKO + 1;
    END LOOP;

   --PKG_Logs.StdLogAddTxt('Elementi Presenti:   '||vNumEleCur,TRUE,NULL,vLog);
   PKG_Logs.StdLogAddTxt('Misure   Iniziali:   '||vMisIni,TRUE,NULL,vLog);
   PKG_Logs.StdLogAddTxt('Misure   Inserite:   '||vNumIns,TRUE,NULL,vLog);
   PKG_Logs.StdLogAddTxt('Misure   Modificate: '||vNumMod,TRUE,NULL,vLog);
   PKG_Logs.StdLogAddTxt('Misure   Chiuse:     '||vNumKO,TRUE,NULL,vLog);
   PKG_Logs.StdLogPrint(vLog);


 END AddMisureAcqStatiche;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetTrattamentoElemento(pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pTipEle        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                                pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                pTipFon        IN TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                pTipCli        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                pOrganizzaz    IN NUMBER,
                                pTipoAggreg    IN NUMBER)
   RETURN TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce il trattamento elemento - Se non presente lo crea
-----------------------------------------------------------------------------------------------------------*/

    vTrtEle         TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;
    vTipEle         TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipEle;
    vGerEcs         TIPI_ELEMENTO.GER_ECS%TYPE;
--    vTipoAggreg     NUMBER := pTipoAggreg;
    vOrganizzazione NUMBER(1);

         PROCEDURE SetRel_Ele_TipMis (pCodEle        IN ELEMENTI.COD_ELEMENTO%TYPE,
                                      pTipMis        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                      pTipRet        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                      vTipTabMisure  IN INTEGER) AS
        /*---------------------------------------------------------------------------------------------------
            Inserisce/Modifica la relazione Elemento / Tipo Misura indicando se le misure saranno reperibili
            in tabella MISURE_ACQUISITE, i tablella MISURE_AGGREGATE o in entrambe e il tipo rete interessato
        ---------------------------------------------------------------------------------------------------*/
            vTabMisure    REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;
            vReteAT       REL_ELEMENTO_TIPMIS.RETE_AT%TYPE := 0;
            vReteMT       REL_ELEMENTO_TIPMIS.RETE_MT%TYPE := 0;
            vReteBT       REL_ELEMENTO_TIPMIS.RETE_BT%TYPE := 0;
            vres NUMBER;
         BEGIN
            CASE pTipRet
                WHEN PKG_Mago.gcTipReteAT THEN vReteAT := 1;
                WHEN PKG_Mago.gcTipReteMT THEN vReteMT := 1;
                WHEN PKG_Mago.gcTipReteBT THEN vReteBT := 1;
            END CASE;
            BEGIN
                SELECT TAB_MISURE
                  INTO vTabMisure
                  FROM REL_ELEMENTO_TIPMIS
                 WHERE COD_ELEMENTO = pCodEle
                   AND COD_TIPO_MISURA = pTipMis;
                IF NVL(BITAND(vTabMisure,vTipTabMisure),-1) <> vTipTabMisure THEN
                    UPDATE REL_ELEMENTO_TIPMIS SET TAB_MISURE = TAB_MISURE + vTipTabMisure
                     WHERE COD_ELEMENTO = pCodEle
                       AND COD_TIPO_MISURA = pTipMis;
                END IF;
                CASE pTipRet
                    WHEN PKG_Mago.gcTipReteAT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_AT = vReteAT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_AT <> vReteAT;
                    WHEN PKG_Mago.gcTipReteMT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_MT = vReteMT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_MT <> vReteMT;
                    WHEN PKG_Mago.gcTipReteBT THEN UPDATE REL_ELEMENTO_TIPMIS SET RETE_BT = vReteBT
                                                    WHERE COD_ELEMENTO = pCodEle
                                                      AND COD_TIPO_MISURA = pTipMis
                                                      AND RETE_BT <> vReteBT;
                END CASE;
            EXCEPTION
             WHEN NO_DATA_FOUND THEN
                 INSERT INTO REL_ELEMENTO_TIPMIS (COD_ELEMENTO, COD_TIPO_MISURA, TAB_MISURE,    RETE_AT, RETE_MT, RETE_BT )
                                          VALUES (pCodEle,      pTipMis,         vTipTabMisure, vReteAT, vReteMT, vReteBT );
            END;
         END SetRel_Ele_TipMis;
BEGIN

    IF vTipEle IS NULL THEN
        SELECT COD_TIPO_ELEMENTO
          INTO vTipEle
          FROM ELEMENTI
         WHERE COD_ELEMENTO = pCodEle;
    END IF;

    SELECT GER_ECS
      INTO vGerEcs
      FROM TIPI_ELEMENTO
     WHERE COD_TIPO_ELEMENTO = vTipEle;

    IF vGerEcs = 1 THEN
        -- eseguo forzatura per evitare il proliferare di misure aggregate
        -- a livello di gerarchie di Stato normale e Attuale:
        -- per elementi in gerarchia di CS uso solo l'organizazzione Elettrica
        vOrganizzazione := PKG_Mago.gcOrganizzazELE;
    ELSE
        vOrganizzazione := pOrganizzaz;
    END IF;

    BEGIN
        SELECT COD_TRATTAMENTO_ELEM
          INTO vTrtEle
          FROM TRATTAMENTO_ELEMENTI
         WHERE COD_ELEMENTO = pCodEle
           AND COD_TIPO_MISURA = pTipMis
           AND COD_TIPO_FONTE = pTipFon
           AND COD_TIPO_RETE = pTipRet
           AND COD_TIPO_CLIENTE = pTipCli
           AND ORGANIZZAZIONE = pOrganizzaz
           AND TIPO_AGGREGAZIONE = pTipoAggreg;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            INSERT INTO TRATTAMENTO_ELEMENTI (COD_ELEMENTO,
                                              COD_TIPO_ELEMENTO,
                                              COD_TIPO_MISURA,
                                              COD_TIPO_FONTE,
                                              COD_TIPO_RETE,
                                              COD_TIPO_CLIENTE,
                                              ORGANIZZAZIONE,
                                              TIPO_AGGREGAZIONE)
                                      VALUES (pCodEle,
                                              vTipEle,
                                              pTipMis,
                                              pTipFon,
                                              pTipRet,
                                              pTipCli,
                                              pOrganizzaz,
                                              pTipoAggreg)
                                   RETURNING COD_TRATTAMENTO_ELEM INTO vTrtEle;
        WHEN OTHERS THEN RAISE;
    END;

    CASE
        WHEN pTipoAggreg = PKG_Mago.gcStatoNullo
            THEN SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAcquisite);
        WHEN pTipoAggreg IN (PKG_Mago.gcStatoNormale,PKG_Mago.gcStatoAttuale)
            THEN SetRel_Ele_TipMis(pCodEle,pTipMis,pTipRet,PKG_Mago.gcTabAggregate);
        WHEN pTipoAggreg = gcModalitaKPI
            THEN NULL;
    END CASE;
    RETURN vTrtEle;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt('Codici         : '||'Ele='||NVL(TO_CHAR(pCodEle),'<null>')||' - '||
                                                    'Tip='||NVL(pTipEle,'<null>')||' - '||
                                                    'Mis='||NVL(pTipMis,'<null>')||' - '||
                                                    'Fon='||NVL(pTipFon,'<null>')||' - '||
                                                    'Ret='||NVL(pTipRet,'<null>')||' - '||
                                                    'Cli='||NVL(pTipCli,'<null>')||' - '||
                                                    'Org='||NVL(TO_CHAR(pOrganizzaz),'<null>')||'('||NVL(TO_CHAR(vOrganizzazione),'<null>')||') - '||
                                                    'Agr='||NVL(TO_CHAR(pTipoAggreg),'<null>') /*||'('||NVL(TO_CHAR(vTipoAggreg),'<null>')||')'*/
                              ,TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetTrattamentoElemento;

---- ----------------------------------------------------------------------------------------------------------

---- ----------------------------------------------------------------------------------------------------------

FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE,pTemporalAggreg IN NUMBER) RETURN DATE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la pCurrentDate 'arrotondata' temporalmente a partire dalla data di partenza (DateFrom)
    in base alla aggregazione temporale (pTemporalAggreg) richiesta
-----------------------------------------------------------------------------------------------------------*/
    vUtcTs         TIMESTAMP;
    vUtcCurr       TIMESTAMP;
    vDiffDate      NUMBER;
    vTz            INTEGER; -- := PKG_Date_Java.getTimeOffset(pCurrentDate);

 BEGIN

    IF   pDateFrom       IS NULL
      OR pCurrentDate    IS NULL
      OR pTemporalAggreg IS NULL THEN
      RETURN NULL;
    END IF;

    BEGIN
        vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    EXCEPTION
        WHEN OTHERS THEN
             vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate + 1/24,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    END;

    --vUtcTs         := SYS_EXTRACT_UTC(TO_TIMESTAMP(TRUNC(pDateFrom)));
    -- set pDateFrom value into vUtcTs variable
    vUtcTs    := FROM_TZ(CAST(pDateFrom AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    vUtcCurr  := FROM_TZ(CAST(pCurrentDate AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    SELECT EXTRACT (DAY FROM (vUtcCurr-vUtcTs))*24*60+
           EXTRACT (HOUR FROM (vUtcCurr-vUtcTs))*60+
           EXTRACT (MINUTE FROM (vUtcCurr-vUtcTs))
      INTO vDiffDate
      FROM DUAL;

    vDiffDate := TRUNC( vDiffDate / pTemporalAggreg );

    RETURN CAST(vUtcTs AS DATE) + (vDiffDate*pTemporalAggreg/60/24) + (vTz/24);

 EXCEPTION
    WHEN OTHERS THEN
        IF SQLCODE = -01878 THEN
            RETURN NULL;
        ELSE
            RAISE;
        END IF;

 END GetRoundDate;

 ----------------------------------------------------------------------------------------------------------

-- FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE, pTemporalAggreg IN NUMBER) RETURN DATE AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna la pCurrentDate 'arrotondata' temporalmente a partire dalla data di partenza (DateFrom)
--    in base alla aggregazione temporale (pTemporalAggreg) richiesta
-------------------------------------------------------------------------------------------------------------*/
--    cDataStart      CONSTANT DATE := TO_DATE('1970-01-01','yyyy-mm-dd');
--    vDateFrom       NUMBER := TO_NUMBER(pDateFrom    - cDataStart) * (24 * 60 * 60 * 1000);
--    vCurrentDate    NUMBER := TO_NUMBER(pCurrentDate - cDataStart) * (24 * 60 * 60 * 1000);
-- BEGIN
--    IF   pDateFrom       IS NULL
--      OR pCurrentDate    IS NULL
--      OR pTemporalAggreg IS NULL THEN
--      RETURN NULL;
--    END IF;
--    RETURN cDataStart+((vCurrentDate - MOD(vCurrentDate - vDateFrom, pTemporalAggreg*60*1000))/1000)/60/60/24;

-- END GetRoundDate;

 ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetProduttoriGME     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodCo            IN VARCHAR2,
                                 pTipiFonte        IN VARCHAR2,
                                 pDate             IN DATE DEFAULT SYSDATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori per i quali devono essere generate le misure GME per TEST
-----------------------------------------------------------------------------------------------------------*/

    vFlgNull    NUMBER(1) := -1;
    cTipFon     CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPFON';
 BEGIN
    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = cTipFon;
    PKG_Mago.TrattaListaCodici(pTipiFonte, cTipFon, vFlgNull);
    OPEN pRefCurs FOR SELECT NVL(S1.COD_GEST_ELEMENTO,S2.COD_GEST_ELEMENTO) COD_CO,
                             ID_ELEMENTO           COD_POD_CLIENTE,
                             E.COD_GEST_ELEMENTO   COD_GEST_AUI,
                             ID_ELEMENTO           COD_ENELTEL,
                             A.COD_RAGGR_FONTE       FONTE,
                             A.POTENZA_INSTALLATA,
                             COD_CITTA
                        FROM (SELECT A.COD_ELEMENTO, A.COD_RAGGR_FONTE, SUM(A.POTENZA_INSTALLATA) POTENZA_INSTALLATA
                                FROM (SELECT CASE E.COD_TIPO_ELEMENTO
                                                WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteAT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                                WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteMT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                                WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Elementi.GetElementoPadre(COD_ELEMENTO,PKG_Mago.gcClienteBT,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                                             END COD_ELEMENTO,
                                             COD_RAGGR_FONTE,
                                             NVL(POTENZA_INSTALLATA,0) * NVL(FATTORE,1) POTENZA_INSTALLATA
                                        FROM ELEMENTI E
                                       INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                                       INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE)
                                       WHERE pDate BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                         AND E.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT)
                                      ) A
                                GROUP BY A.COD_ELEMENTO, COD_RAGGR_FONTE
                             ) A
                       INNER JOIN ELEMENTI      E  ON A.COD_ELEMENTO  = E.COD_ELEMENTO
                       INNER JOIN ELEMENTI_DEF  D  ON A.COD_ELEMENTO  = D.COD_ELEMENTO
                       INNER JOIN GTTD_VALORI_TEMP ON ALF1 = COD_RAGGR_FONTE
                       LEFT OUTER JOIN ELEMENTI S1 ON S1.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcEsercizio,pDate,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN ELEMENTI S2 ON S2.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcEsercizio,pDate,PKG_Mago.gcOrganizzazAMM,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN ELEMENTI C  ON C.COD_ELEMENTO  = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcComune,pDate,PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale)
                       LEFT OUTER JOIN METEO_REL_ISTAT ON COD_ISTAT   = NVL(SUBSTR(C.COD_GEST_ELEMENTO,INSTR(C.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1),C.COD_GEST_ELEMENTO)
                      WHERE NVL(S1.COD_GEST_ELEMENTO,S2.COD_GEST_ELEMENTO) = pCodCo
                        AND COD_CITTA IS NOT NULL
                      ORDER BY E.COD_GEST_ELEMENTO, COD_RAGGR_FONTE;
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.TraceLog(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END GetProduttoriGME;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GMEcompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                pFinishTimestamp IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
     Riceve l'indicazione di fine caricamento misure da GME
     Lancia la richiesta di elaborazione aggregate
-----------------------------------------------------------------------------------------------------------*/
  vLog  PKG_Logs.t_StandardLog;
  vTxt  VARCHAR2(300);
BEGIN
   vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_Meteo.GMEcompleted',
                               pDataRif    => pFinishTimestamp);

   DBMS_SCHEDULER.RUN_JOB('MAGO_INS_REQ_AGG_GME',FALSE);

   OPEN pRefCurs FOR  SELECT 'OK' MESSAGE FROM DUAL;

   PKG_Logs.StdLogPrint (vLog);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vTxt := SQLERRM;
         PKG_Logs.StdLogAddTxt(vTxt||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR  SELECT vTxt MESSAGE FROM DUAL;
         RETURN;
END GMEcompleted;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureGme(pMisure  IN T_MISURA_GME_ARRAY,
                        pAggrega IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure provenienti da GME/SMILE
-----------------------------------------------------------------------------------------------------------*/

  vLog           PKG_Logs.t_StandardLog;

  vMisureSplit   T_MISURA_GME_ARRAY := T_MISURA_GME_ARRAY();
  vMisureNoSplit T_MISURA_GME_ARRAY := T_MISURA_GME_ARRAY();


  vNumIns        INTEGER := 0;
  vNumMod        INTEGER := 0;
  vNumPre        INTEGER := 0;
  vNumKO         INTEGER := 0;
  vNumKO2        INTEGER := 0;

  vOrigineMis    TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'GME_';

  vNum           INTEGER;

  vFntNonOmog    BOOLEAN := FALSE;
  vElemPadre     ELEMENTI.COD_ELEMENTO%TYPE;
  vCodFte        ELEMENTI_DEF.COD_TIPO_FONTE%TYPE;
  vFteSolare     NUMBER(1);
  vFteEolica     NUMBER(1);
  vFteIdraulicA  NUMBER(1);
  vFteTermica    NUMBER(1);
  vFteRinnovab   NUMBER(1);
  vFteConvenz    NUMBER(1);

  vFonteSec      ELEMENTI_DEF.COD_TIPO_FONTE%TYPE := NULL;

  vCodEle        ELEMENTI.COD_ELEMENTO%TYPE := 0;
  vNomEle        ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vIdElemento    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
  vTipEle        ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE := NULL;
  vTipCli        ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE := NULL;

  vPrevMisIn     TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_IN%TYPE := '_';
  vCodTipMisOut  TIPI_MISURA_CONV_ORIG.COD_TIPO_MISURA_OUT%TYPE;
  vFlagSplit     TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;
  vFattore       NUMBER;

  FUNCTION f_TipEle RETURN VARCHAR2 AS
    BEGIN
        IF vTipEle IS NOT NULL THEN
            RETURN vTipEle;
        ELSE
            CASE pMisure(1).TIPO_RETE
                WHEN PKG_Mago.gcTipReteAT THEN RETURN PKG_Mago.gcSecondarioDiTrasf;
                WHEN PKG_Mago.gcTipReteMT THEN
                        CASE SUBSTR(pMisure(1).GEST_ELEM,1,2)
                           WHEN 'IT'   /* si lo so e' una forzatura terribile ma serve solo ai fini di riportare l'info sui log */
                                THEN RETURN 'POD';
                                ELSE  RETURN PKG_Mago.gcClienteMT;
                         END CASE;
                WHEN PKG_Mago.gcTipReteMT THEN RETURN PKG_Mago.gcTrasformMtBt;
                ELSE RETURN NULL;
            END CASE;
        END IF;
        RETURN NULL;
    END f_TipEle;

  FUNCTION f_CheckCodInput RETURN BOOLEAN AS
    BEGIN

        BEGIN
            SELECT COD_ELEMENTO, COD_GEST_ELEMENTO,  NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE
              INTO vCodEle,      vIdElemento,        vNomEle,       vTipEle,           vTipCli
              FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_CLIENTE,
                           ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                      FROM ELEMENTI E
                     INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                     WHERE COD_GEST_ELEMENTO = vIdElemento
                   )
             WHERE ORD = 1;
            PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                BEGIN
                    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE
                      INTO vCodEle,      vIdElemento,       vNomEle,       vTipEle,           vTipCli
                      FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_CLIENTE,
                                   ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                              FROM ELEMENTI E
                             INNER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
                             WHERE RIF_ELEMENTO = vIdElemento
                           )
                     WHERE ORD = 1;
                    PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN
                          PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                          PKG_Logs.StdLogAddTxt('Non presente in anagrafica',FALSE,gkAnagrNonPres,vLog);
                          RETURN FALSE;
                    WHEN OTHERS THEN
                          PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
                          IF SQLCODE = -01422 THEN
                              PKG_Logs.StdLogAddTxt('Associato a piu'' di un Elemento',FALSE,gkAnagrIncong,vLog);
                              RETURN FALSE;
                          ELSE
                             RAISE;
                          END IF;
                END;
        END;
        IF vTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT) THEN
            SELECT COD_GEST_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO
              INTO vIdElemento,       vNomEle,       vTipEle
              FROM (SELECT ID_ELEMENTO COD_GEST_ELEMENTO,
                           ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                      FROM ELEMENTI_DEF
                     INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                     WHERE COD_GEST_ELEMENTO = vIdElemento
                    ) P
             INNER JOIN ELEMENTI      E USING(COD_GEST_ELEMENTO)
             INNER JOIN ELEMENTI_DEF  D USING(COD_ELEMENTO)
             WHERE ORD = 1;
        END IF;
        RETURN TRUE;
    END f_CheckCodInput;

  PROCEDURE p_Elabora(pMisure   IN T_MISURA_GME_ARRAY,
                      pAggrega  IN BOOLEAN,
                      pMisSplit IN BOOLEAN) AS
    BEGIN
        CASE

            WHEN (vTipCli = PKG_Mago.gcCliente)      -- Cliente (non produttore)
              OR (NOT pMisSplit)                THEN -- Misura da NON splittare
                 vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
                 AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                 vTipEle, pMisure(1).TIPO_RETE, vCodFte, NULL, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);

            WHEN vTipEle IN (PKG_Mago.gcClienteAT,
                             PKG_Mago.gcClienteMT,
                             PKG_Mago.gcClienteBT,
                             PKG_Mago.gcTrasformMtBt)  THEN   -- Produttori/ Trasformatori MT/BT
                BEGIN
                    SELECT TIPO_FONTE,NUM_FONTI,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteSolareID)   =PKG_Mago.gcRaggrFonteSolareID    THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteEolicaID)   =PKG_Mago.gcRaggrFonteEolicaID    THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteIdraulicaID)=PKG_Mago.gcRaggrFonteIdraulicaID THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteTermicaID)  =PKG_Mago.gcRaggrFonteTermicaID   THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteRinnovabID) =PKG_Mago.gcRaggrFonteRinnovabID  THEN 1 ELSE 0 END,
                           CASE WHEN BITAND(BITMAP_FONTI,PKG_Mago.gcRaggrFonteConvenzID)  =PKG_Mago.gcRaggrFonteConvenzID   THEN 1 ELSE 0 END
                      INTO vCodFte, vNum, vFteSolare, vFteEolica, vFteIdraulica, vFteTermica, vFteRinnovab, vFteConvenz
                      FROM (SELECT CASE
                                     WHEN MAX(COD_TIPO_FONTE) IS NULL OR
                                          MIN(COD_TIPO_FONTE) IS NULL THEN NULL
                                     WHEN MAX(COD_TIPO_FONTE) = MIN(COD_TIPO_FONTE)
                                         THEN MAX(COD_TIPO_FONTE)
                                         ELSE PKG_Mago.gcRaggrFonteNonOmog
                                  END TIPO_FONTE,
                                  COUNT(*) NUM_FONTI,
                                  BITMAP_FONTI
                             FROM (SELECT COD_TIPO_FONTE,
                                          SUM(ID_RAGGR_FONTE) OVER (PARTITION BY 1) BITMAP_FONTI
                                     FROM (SELECT DISTINCT COD_TIPO_FONTE
                                             FROM (SELECT COD_TIPO_FONTE,
                                                          ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                                                     FROM ELEMENTI_DEF A
                                                    WHERE ID_ELEMENTO  = vIdElemento   --  codice gestionale cliente
                                                  )
                                            WHERE ORD = 1
                                          )
                                    INNER JOIN TIPO_FONTI USING(COD_TIPO_FONTE)
                                    INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE)
                                  )
                             GROUP BY BITMAP_FONTI
                           );
                EXCEPTION
                    WHEN NO_DATA_FOUND THEN vCodFte := NULL;
                                            vFteSolare := 0;
                                            vFteEolica := 0;
                                            vFteIdraulica := 0;
                                            vFteTermica := 0;
                                            vFteRinnovab := 0;
                                            vFteConvenz := 0;
                    WHEN OTHERS THEN RAISE;
                END;
                IF vCodFte = PKG_Mago.gcRaggrFonteNonOmog THEN
                    PKG_Logs.StdLogAddTxt('I Generatori associati non hanno fonte omogenea',FALSE,gkWarning,vLog);
                ELSE
                    IF vCodFte IS NULL THEN
                        IF vNum = 0 THEN
                            PKG_Logs.StdLogAddTxt('Nessun Generatore associato',FALSE,gkWarning,vLog);
                        ELSE
                            PKG_Logs.StdLogAddTxt('Il Tipo fonte per uno o piu'' dei Generatori associati non e'' valorizzato',FALSE,gkWarning,vLog);
                        END IF;
                        vCodFte := PKG_Mago.gcRaggrFonteNonOmog;
                    END IF;
                END IF;

                vFonteSec := NULL;

                IF vFlagSplit = 0 THEN
                    vLog.STORE_ON_FILE := TRUE;
                    vCodFte := PKG_Mago.gcRaggrFonteNonAppl;
                    PKG_Logs.StdLogInfo('Le misure ' || pMisure(1).TIPO_MISURA || ' non sono splittabili e sono attribuite a '||PKG_Elementi.DecodeTipElem(vTipEle),vLog);
                    AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, vFonteSec, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);
                ELSIF vCodFte = PKG_Mago.gcRaggrFonteNonOmog THEN
                    IF (vFteSolare + vFteEolica) > 0 THEN
                        -- solare e/o eolico
                        CASE (vFteIdraulica + vFteTermica + vFteRinnovab + vFteConvenz)
                            WHEN 0 THEN
                                --nessuna altra fonte sola altra fonte
                                NULL;
                            WHEN 1 THEN
                                --una sola altra fonte
                                CASE
                                    WHEN vFteIdraulica = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteIdraulica);
                                    WHEN vFteTermica   = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteTermica);
                                    WHEN vFteRinnovab  = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteRinnovab);
                                    WHEN vFteConvenz   = 1 THEN vFonteSec := LOWER(PKG_Mago.gcRaggrFonteConvenz);
                                END CASE;
                            ELSE
                                PKG_Logs.StdLogAddTxt('Produttore con fonti alternative > 1',FALSE,gkWarning,vLog);
                        END CASE;
                    END IF;
                    vLog.STORE_ON_FILE := TRUE;
                    PKG_Logs.StdLogInfo('Le misure sono attribuite a '||PKG_Elementi.DecodeTipElem(vTipEle),vLog);
                    AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, vFonteSec, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);
                ELSE
                    SELECT COUNT(*)
                      INTO vNum
                      FROM ELEMENTI_DEF
                     WHERE ID_ELEMENTO = vIdElemento
                       AND POTENZA_INSTALLATA IS NULL;
                    IF vNum > 0 THEN
                        vLog.STORE_ON_FILE := TRUE;
                        PKG_Logs.StdLogAddTxt('La Potenza Installata per '||vNum||' dei Generatori associati non e'' valorizzata',FALSE,gkAnagrIncompl,vLog);
                        PKG_Logs.StdLogPrint (vLog);
                        RETURN;
                    END IF;
                    AddMisureGmeFgl('GME',vOrigineMis, vCodEle, vIdElemento,
                                    vTipEle, pMisure(1).TIPO_RETE, vCodFte, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);
                END IF;

            WHEN vTipEle IN (PKG_Mago.gcGeneratoreAT,PKG_Mago.gcGeneratoreMT,PKG_Mago.gcGeneratoreBT)  THEN   -- Generatori
                AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                vTipEle, pMisure(1).TIPO_RETE, NULL, NULL, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);

            ELSE   -- Altri elementi
                AddMisureGmeEle('GME',vOrigineMis, vCodEle, vIdElemento,
                                vTipEle, pMisure(1).TIPO_RETE, NULL, NULL, pAggrega, vNumIns, vNumMod, vNumPre, vNumKO, pMisure, vLog);
        END CASE;
    END;

BEGIN

    IF pMisure.LAST IS NULL THEN
        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME,
                                    pFunzione     => 'PKG_Misure.AddMisureGme',
                                    pStoreOnFile  => FALSE,
                                    pDescrizione  => 'Lista misure vuota');
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    vOrigineMis := vOrigineMis||pMisure(1).TIPO_RETE;
    vIdElemento := pMisure(1).GEST_ELEM;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassMIS||PKG_Mago.gcJobSubClassGME||'_'||pMisure(1).TIPO_RETE,
                                pFunzione     => 'PKG_Misure.AddMisureGme',
                                pStoreOnFile  => FALSE,
                                pCodice       => vIdElemento,
                                pTipo         => f_TipEle,
                                pDataRif      => pMisure(pMisure.FIRST).DATA_MIS,
                                pDataRif_fine => pMisure(pMisure.LAST).DATA_MIS);

    SELECT COUNT(*) INTO vNum FROM TIPI_RETE WHERE COD_TIPO_RETE = pMisure(1).TIPO_RETE;
    IF vNum = 0 THEN
        vLog.DESCRIZIONE := SUBSTR('Tipo Rete '||NVL(pMisure(1).TIPO_RETE,'<null>')||' non definito',1,100);
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    IF NOT f_CheckCodInput THEN
        PKG_Logs.StdLogPrint (vLog);
        RETURN;
    END IF;

    vLog.DESCRIZIONE := 'Origine Misura: '||vOrigineMis;
    vLog.CLASSE      := PKG_Mago.gcJobClassMIS||'.'||vOrigineMis;

    FOR i IN pMisure.FIRST .. pMisure.LAST LOOP
        IF vPrevMisIn <> pMisure(i).TIPO_MISURA THEN
            vPrevMisIn := pMisure(i).TIPO_MISURA;
            BEGIN
                SELECT FLG_SPLIT
                  INTO vFlagSplit
                  FROM TIPI_MISURA_CONV_ORIG
                 WHERE ORIGINE = vOrigineMis
                   AND COD_TIPO_MISURA_IN = pMisure(i).TIPO_MISURA;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    vFlagSplit := NULL;
            END;
        END IF;
        CASE
            WHEN vFlagSplit IS NULL THEN vNumKO2 := vNumKO2 + 1;
            WHEN vFlagSplit = 1     THEN vMisureSplit.EXTEND;
                                         vMisureSplit(vMisureSplit.LAST) := pMisure(i);
            WHEN vFlagSplit = 0     THEN vMisureNoSplit.EXTEND;
                                         vMisureNoSplit(vMisureNoSplit.LAST) := pMisure(i);
        END CASE;
    END LOOP;

    IF vMisureSplit.LAST IS NOT NULL THEN
        p_Elabora(vMisureSplit,pAggrega,TRUE);
    END IF;

    IF vMisureNoSplit.LAST IS NOT NULL THEN
        p_Elabora(vMisureNoSplit,pAggrega,FALSE);
    END IF;

    vNumKO := vNumKO + vNumKO2;

    PKG_Logs.StdLogAddTxt(f_TipEle, vIdElemento, vNomEle, vLog);
    PKG_Logs.StdLogInfo('Misure inserite/modificate '||TO_CHAR(vNumIns + vNumMod),vLog);
    PKG_Logs.StdLogAddTxt('Misure Ricevute     : ' ||pMisure.LAST,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Inserite     : ' ||vNumIns,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Modificate   : ' ||vNumMod,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure gia'' presenti: '||vNumPre,TRUE,NULL,vLog);
    PKG_Logs.StdLogAddTxt('Misure Scartate     : ' ||vNumKO,TRUE,NULL,vLog);

    PKG_Logs.StdLogPrint(vLog);

    COMMIT;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END AddMisureGme;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisure            (pMisure        IN T_MISURA_GME_ARRAY) AS
  BEGIN
        AddMisureGme(pMisure);
  END AddMisure;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE CalcMisureStatiche   (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE)AS
/*-----------------------------------------------------------------------------------------------------------
    Sottomette il calcolo delle misure statiche previste
-----------------------------------------------------------------------------------------------------------*/
  BEGIN
        AddMisurePI (pData,pOrganizzazione,pStato,pAggrega);
        AddMisureNRI(pData,pOrganizzazione,pStato,pAggrega);
  END CalcMisureStatiche;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisurePI          (pData             IN DATE DEFAULT SYSDATE,
                                 pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                 pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                 pAggrega          IN BOOLEAN DEFAULT TRUE,
                                 pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure di potenza installata
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    AddMisureAcqStatiche (pData,PKG_Mago.gcPotenzaInstallata,pOrganizzazione,pStato,pAggrega,pElabImmediata);
 END AddMisurePI;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureNRI          (pData             IN DATE DEFAULT SYSDATE,
                                  pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                                  pStato            IN INTEGER DEFAULT PKG_MAGO.gcStatoNormale,
                                  pAggrega          IN BOOLEAN DEFAULT TRUE,
                                  pElabImmediata    IN INTEGER DEFAULT PKG_Aggregazioni.gcElaborazioneStandard) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza il numero di generatori
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    AddMisureAcqStatiche (pData,PKG_Mago.gcNumeroImpianti,pOrganizzazione,pStato,pAggrega,pElabImmediata);
 END AddMisureNRI;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE AddMisureMeteo       (pMisureMeteo    IN T_MISMETEO_ARRAY,
                                 pAggrega        IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure Meteo ricevute.
-----------------------------------------------------------------------------------------------------------*/

  vLog          PKG_Logs.t_StandardLog;

  vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'METEO';
  vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;

  vCodTipoMis   TIPI_MISURA.COD_TIPO_MISURA%TYPE;
  vFattore      NUMBER;
  vTipMis       TIPI_MISURA.COD_TIPO_MISURA%TYPE := '_';
  vCodElem      ELEMENTI.COD_ELEMENTO%TYPE := 0;
  vCodEleNome   ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vCodTipoElem  ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
  vCodTipoRete  TIPI_RETE.COD_TIPO_RETE%TYPE;
  vCodTipoClie  ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vDataMin      DATE := TO_DATE('01013000','ddmmyyyy');
  vDataMax      DATE := TO_DATE('01011900','ddmmyyyy');

  vResult       INTEGER;
  vNumIns       INTEGER := 0;
  vNumMod       INTEGER := 0;

  vTot          NUMBER := 0;

 BEGIN

    IF pMisureMeteo.FIRST IS NULL THEN
        vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                    pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                    pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                    pStoreOnFile  => FALSE);
        PKG_Logs.StdLogAddTxt('Nessuna misura ricevuta',TRUE,NULL,vLog);
        PKG_Logs.StdLogPrint(vLog);
        RETURN;
    END IF;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pStoreOnFile  => FALSE);

    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP
        IF vDataMin > pMisureMeteo(i).DATA THEN
            vDataMin := pMisureMeteo(i).DATA;
        END IF;
        IF vDataMax < pMisureMeteo(i).DATA THEN
            vDataMax := pMisureMeteo(i).DATA;
        END IF;
    END LOOP;
    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_Misure.AddMisureMeteo',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pDataRif      => vDataMin,
                                pDataRif_fine => vDataMax,
                                pStoreOnFile  => FALSE);

    FOR i IN pMisureMeteo.FIRST .. pMisureMeteo.LAST LOOP

        IF pMisureMeteo(i).COD_TIPO_MISURA  <> vTipMis THEN
            IF NOT GetMisuraOut(vOrigineMis,NULL,pMisureMeteo(i).COD_TIPO_MISURA,vCodTipoMis,vFattore,vFlagSplit) THEN
                PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Tipo Misura IN  : '||NVL(pMisureMeteo(i).COD_TIPO_MISURA,'<null>'),FALSE,NULL,vLog);
                PKG_Logs.StdLogAddTxt('Tipo Misura OUT : '||vCodTipoMis,FALSE,NULL,vLog);
                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Associazione OrigineMisura/TipoMisura non definita!');
            END IF;
        END IF;

        IF pMisureMeteo(i).COD_ELEMENTO <> vCodElem THEN
            BEGIN
                SELECT NOME_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_CLIENTE, COD_TIPO_RETE
                  INTO vCodEleNome,   vCodTipoElem,      vCodTipoClie,        vCodTipoRete
                  FROM (SELECT COD_ELEMENTO, NOME_ELEMENTO, E.COD_TIPO_ELEMENTO,
                               NVL(COD_TIPO_CLIENTE,PKG_Mago.gcClientePrdNonDeterm) COD_TIPO_CLIENTE,
                               ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
                          FROM ELEMENTI
                         INNER JOIN ELEMENTI_DEF E USING(COD_ELEMENTO)
                         WHERE COD_ELEMENTO = pMisureMeteo(i).COD_ELEMENTO
                           AND pMisureMeteo(i).DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE)
                 INNER JOIN TIPI_ELEMENTO E USING(COD_TIPO_ELEMENTO)
                 WHERE ORD = 1;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                      PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Codice Elemento : '||NVL(TO_CHAR(pMisureMeteo(i).COD_ELEMENTO),'<null>'),FALSE,NULL,vLog);
                      RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,'Codice Elemento ' || pMisureMeteo(i).COD_ELEMENTO || ' non definito!');
                WHEN OTHERS THEN
                      PKG_Logs.StdLogAddTxt('Origine         : '||vOrigineMis,FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Data Misura     : '||PKG_Mago.StdOutDate(pMisureMeteo(i).DATA),FALSE,NULL,vLog);
                      PKG_Logs.StdLogAddTxt('Codice Elemento : '||NVL(TO_CHAR(pMisureMeteo(i).COD_ELEMENTO),'<null>'),FALSE,NULL,vLog);
                      RAISE;
            END;
            vCodElem := pMisureMeteo(i).COD_ELEMENTO;
        END IF;
        vCodTratElem := GetTrattamentoElemento(pMisureMeteo(i).COD_ELEMENTO,
                                               vCodTipoElem,
                                               vCodTipoMis,
                                               pMisureMeteo(i).COD_TIPO_FONTE,
                                               vCodTipoRete,
                                               vCodTipoClie,
                                               PKG_Mago.gcOrganizzazELE,
                                               PKG_Mago.gcStatoNullo);
        AddMisuraAcq('MET',
                     vCodTratElem,
                     pMisureMeteo(i).DATA,
                     pMisureMeteo(i).COD_TIPO_MISURA,
                     pMisureMeteo(i).VALORE * vFattore,
                     vCodTipoRete,
                     pAggrega,
                     vResult);

        vTot := vTot + 1;

        CASE vResult
            WHEN PKG_Mago.gcInserito   THEN vNumIns := vNumIns + 1;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - inserita');
            WHEN PKG_Mago.gcModificato THEN vNumMod := vNumMod + 1;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - modificata');
            ELSE NULL;
--                    PKG_Logs.TraceLog('trt: '||vCodTratElem||' - '||
--                                      'ele: '||pMisureMeteo(i).COD_ELEMENTO||' - '||
--                                      'mis: '||pMisureMeteo(i).COD_TIPO_MISURA||' - '||
--                                      'dat: '||TO_CHAR(pMisureMeteo(i).DATA,'dd/mm/yyyy hh24.mi')||' - '||
--                                      'val: '||pMisureMeteo(i).VALORE||' - '||
--                                      'fon: '||pMisureMeteo(i).COD_TIPO_FONTE||' - invariata');
        END CASE;

    END LOOP;

    PKG_Logs.StdLogAddTxt('Misure Meteo:   Tot='||vTot||'   Ins='||vNumIns||'   Mod='||vNumMod,TRUE,NULL,vLog);
    PKG_Logs.StdLogPrint(vLog);

    COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END AddMisureMeteo;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoFonti          (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi fonte seperati da | restituisce l'ID globale dei tipo fonti ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN

    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    IF INSTR(pLista,'!') <> 0 THEN
        vLista := PKG_UtlGlb.SplitString(SUBSTR(pLista,3),PKG_Mago.cSepCharLst);
    ELSE
        vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    END IF;
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_RAGGR_FONTE)
              INTO vNum
             FROM RAGGRUPPAMENTO_FONTI
            WHERE COD_RAGGR_FONTE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoFonti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoReti          (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi rete seperati da | restituisce l'ID globale dei tipi rete ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN
    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_RETE)
              INTO vNum
             FROM TIPI_RETE
            WHERE COD_TIPO_RETE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoReti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetIdTipoClienti    (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    da elenco tipi cliente seperati da | restituisce l'ID globale dei tipi cliente ricevuti
    se Lista NULL o = 'ALL'  ritorna 0 -> TUTTI
-----------------------------------------------------------------------------------------------------------*/
    vID     NUMBER := 0;
    vNum    NUMBER;
    vLista  PKG_UtlGlb.t_SplitTbl;
 BEGIN
    IF NVL(pLista,'ALL') = 'ALL' THEN
        RETURN 0;
    END IF;
    vLista := PKG_UtlGlb.SplitString(pLista,PKG_Mago.cSepCharLst);
    IF vLista.LAST IS NOT NULL THEN
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            SELECT MAX(ID_CLIENTE )
              INTO vNum
             FROM TIPI_CLIENTE
            WHERE COD_TIPO_CLIENTE = vLista(i);
            vID := vID + NVL(vNum,0);
        END LOOP;
    END IF;
    RETURN vID;
 END GetIdTipoClienti;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMisuraStatica       (pTipMisura        IN TIPI_MISURA.COD_TIPO_MISURA%TYPE) RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna 1 se il tipo misura riche la gestione della misura di tipo statico
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pTipMisura IN (PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcNumeroImpianti/*,PKG_Mago.gcPotenzaContrattuale*/) THEN
        RETURN PKG_UtlGlb.gkFlagOn;
    ELSE
        RETURN PKG_UtlGlb.gkFlagOff;
    END IF;
 END IsMisuraStatica;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE AddinterpolationData (pDataDa IN DATE
                                ,pDataA IN DATE
                                ,pAggr IN NUMBER
                                ,pFilterFlg IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Inserisce i valori interpolati ( lineare ) relativi ai buchi di misure
-----------------------------------------------------------------------------------------------------------*/
   BEGIN
      IF pAggr != 0 THEN
         INSERT INTO GTTD_MISURE
                (cod_tipo_misura, DATA, valore, cod_tipo_fonte,FILTER,interpolazione)
         SELECT M.cod_tipo_misura
               ,d.data_attivazione datainterpol
               ,M.valore + (((M.valorenext - M.valore) / ((M.datanext - M.DATA)*24*60/pAggr))*(((D.data_disattivazione - M.DATA)*24*60/pAggr)-1)) valinterpol
               ,M.cod_tipo_fonte
               ,pFilterFlg
               ,1 interpolazione
           FROM
               (SELECT pDataDa + ( (LEVEL-1) / (1*24*60/pAggr)  ) data_attivazione
                      ,pDataDa + ( (LEVEL) / (1*24*60/pAggr) ) data_disattivazione
                      ,LEVEL livcon
                  FROM DUAL
               CONNECT BY LEVEL <= ((pDataA - pDataDa) * (1*24*60/pAggr)) +1
               ) d
               ,
               (
                SELECT DATA --pkg_misure.getrounddate(pDataDa,T.DATA,pAggr) DATA
                      ,T.valore
                      ,T.cod_tipo_misura
                      ,T.cod_tipo_fonte
                      ,LEAD(T.valore) OVER ( PARTITION BY T.cod_tipo_misura, T.cod_tipo_fonte ORDER BY T.DATA) valorenext
                      ,LEAD(T.DATA) OVER ( PARTITION BY T.cod_tipo_misura, T.cod_tipo_fonte ORDER BY T.DATA) datanext
                  FROM GTTD_MISURE T
                      ,TIPI_MISURA tmis
                 WHERE tmis.cod_tipo_misura = T.cod_tipo_misura
                   AND tmis.tipo_interpolazione_buchi_camp = 1
       	           --Eseguo l'interpolazione delle misure con interpolazione LINEARE
                   AND NVL(T.FILTER,0) = pFilterFlg
               ) M
          WHERE data_attivazione > DATA
            AND data_attivazione < datanext;

         DELETE FROM GTTD_MISURE
          WHERE DATA NOT IN (SELECT pDataDa + ( (LEVEL-1) / (1*24*60/pAggr)  ) data_attivazione
                               FROM DUAL
                            CONNECT BY LEVEL <= ((pDataA - pDataDa) * (1*24*60/pAggr)) +1
                            )
            AND PKG_Misure.IsMisuraStatica(COD_TIPO_MISURA) <> PKG_UtlGlb.gkFlagON;   -- Modifica C.M. 1.9.a.13
      END IF;
END AddinterpolationData;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CalcolaFilterPMP(pDataDa IN DATE
                                ,pDataA IN DATE
                                ,pAggr IN NUMBER)
                                AS
/*-----------------------------------------------------------------------------------------------------------
    Esegue il calcolo dei filtri PMP impostando la formula
     PPAGmp(t)(Fonte) = ( PAS(t-DeltaT)(Fonte) - PPAGmp(t-DeltaT)(AlphaFonte)
     Dove Fonte � una delle fonti per cui appllicare il filtro:
     - Termica
     - Idraulica
     - Altra rinnovabile
     - Altra convenzionale
-----------------------------------------------------------------------------------------------------------*/
   BEGIN
   	DBMS_OUTPUT.PUT_LINE ('Eseguo calcolo filtro PMP' );
   	  INSERT INTO GTTD_MISURE
             (cod_tipo_misura, DATA, valore, cod_tipo_fonte,interpolazione)
      SELECT v.alf1 cod_tipo_misura
            ,T.DATA
          ,SUM(CASE WHEN cod_tipo_misura = Pkg_Mago.gcPotenzaAttScambiata THEN T.valore ELSE T.valore * ( -1 ) END )
           ,T.cod_tipo_fonte
          ,CASE WHEN SUM(CASE WHEN T.cod_tipo_misura = Pkg_Mago.gcPotenzaAttScambiata THEN interpolazione ELSE 0 END) > 0 THEN 1 ELSE 0 END
       FROM GTTD_MISURE T
           ,GTTD_VALORI_TEMP v
           , METEO_JOB_RUNTIME_CONFIG M
			WHERE (T.cod_tipo_misura = Pkg_Mago.gcPotenzaAttScambiata OR T.cod_tipo_misura LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%')
			  AND T.FILTER = 1
		    AND M.job_id = 'MDS-service'
			  AND T.DATA <= NVL(M.last_forec_date,PKG_UtlGlb.gkDataTappo)
	      AND v.tip = Pkg_Mago.gcTmpTipFilMisKey
        AND v.alf1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%'
        AND v.alf2 = T.cod_tipo_fonte
			GROUP BY v.alf1, T.DATA, T.cod_tipo_fonte;

END CalcolaFilterPMP;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CalcolaFilterPMC (pDataDa IN DATE
                                ,pDataA IN DATE
                                ,pAggr IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Esegue il calcolo dei filtri PMC impostando la formula
     PPAGCT(t)(Fonte) = ( PI(Fonte) / PI(Totale) )  * PAS(t-DeltaT)(Fonte)
     Dove Fonte e' una delle fonti per cui appllicare il filtro:
     - Termica
     - Idraulica
     - Altra rinnovabile
     - Altra convenzionale
-----------------------------------------------------------------------------------------------------------*/
   BEGIN

     DBMS_OUTPUT.PUT_LINE ('Eseguo calcolo filtro PMC' );
      INSERT INTO GTTD_MISURE
             (cod_tipo_misura, DATA, valore, cod_tipo_fonte,interpolazione)
      SELECT cod_tipo_misura
             ,DATA
             ,CASE WHEN PITot != 0 THEN (PIFonte / PITot) * PASFonte WHEN PIFonte + PASFonte = 0 THEN 0 ELSE NULL END valore
             ,cod_tipo_fonte
             ,interpolazione
        FROM (
              SELECT cod_tipo_misura
                    ,DATA DATA
                    ,CASE WHEN SUM(totPIFonte) > 0 THEN SUM(PIFonte) END PIFonte
                    ,totPI PITot
                    ,CASE WHEN SUM(totPAS) > 0 THEN SUM(PASFonte) END PASFonte
                    ,cod_tipo_fonte
                    ,CASE WHEN SUM(CASE WHEN cod_tipo_misura_orig = Pkg_Mago.gcPotenzaAttScambiata THEN interpolazione ELSE 0 END) > 0 THEN 1 ELSE 0 END interpolazione
                FROM (
                      SELECT alf1 cod_tipo_misura
                            ,T.DATA
                            ,SUM(CASE WHEN cod_tipo_misura = Pkg_Mago.gcPotenzaInstallata THEN T.valore ELSE 0 END) OVER ( PARTITION BY T.DATA ) totPI
                            ,alf2 cod_tipo_fonte
                            ,CASE WHEN cod_tipo_misura =Pkg_Mago.gcPotenzaInstallata THEN T.valore ELSE 0 END PIFonte
                            ,CASE WHEN cod_tipo_misura =Pkg_Mago.gcPotenzaAttScambiata THEN T.valore ELSE 0 END PASFonte
                            ,CASE WHEN cod_tipo_misura =Pkg_Mago.gcPotenzaAttScambiata THEN 1 ELSE 0 END totPAS
                            ,CASE WHEN cod_tipo_misura =Pkg_Mago.gcPotenzaInstallata THEN 1 ELSE 0 END totPIFonte
                            ,T.interpolazione
                            ,T.cod_tipo_misura cod_tipo_misura_orig
                        FROM GTTD_MISURE T
                            ,GTTD_VALORI_TEMP v
                            ,METEO_JOB_RUNTIME_CONFIG M
                       WHERE T.cod_tipo_misura IN (Pkg_Mago.gcPotenzaInstallata, Pkg_Mago.gcPotenzaAttScambiata)
                         AND T.FILTER = 1
                         AND M.job_id = 'MDS-service'
                         AND T.DATA <= NVL(M.last_forec_date,PKG_UtlGlb.gkDataTappo)
                         AND v.tip = Pkg_Mago.gcTmpTipFilMisKey
                         AND v.alf1 LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso || '%'
                     )
               GROUP BY cod_tipo_misura,DATA, cod_tipo_fonte, totPI
             );

END CalcolaFilterPMC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisure            (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 15,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pdeltaT           IN INTEGER DEFAULT 7) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce un cursore con le curve misura richieste partendo da codice elemento numerico
-----------------------------------------------------------------------------------------------------------*/
    vGerEcs     TIPI_ELEMENTO.GER_ECS%TYPE;
    vIdFonti    NUMBER;
    vIdReti     NUMBER;
    vIdClie     NUMBER;
    vFlgNull    NUMBER(1)     := -1;
    vTotTipFon  NUMBER(1)     := -1;
    vTipiMisura VARCHAR2(300) := PKG_Mago.cSepCharLst||pTipiMisura||PKG_Mago.cSepCharLst;
    vAgrTemp    NUMBER  := pAgrTemporale;

--    vDataDa     DATE := pDataDa + (PKG_Date_Java.getTimeOffset(pDataDa) / 24);
--    vDataA      DATE := pDataA  + (PKG_Date_Java.getTimeOffset(pDataA)  / 24);

    vData       DATE;
    vDataDa     DATE := pDataDa;
    vDataA      DATE := pDataA;
    vFilterFlg NUMBER(1);
    vDataDeltaDa DATE;
    vDataDeltaA DATE;
    vtotpmp     NUMBER;
    vtotpmc     NUMBER;
    vtotpm      NUMBER;
    PROCEDURE fLog(pTxt IN VARCHAR2,pSqlCode NUMBER) AS
            vLevel INTEGER;
            vTxt   VARCHAR2(1000);
        BEGIN
            IF pSqlCode IS NOT NULL THEN
               vLevel := PKG_UtlGlb.gcTrace_ERR;
               vTxt := 'Errore '||pTxt||CHR(10);
            ELSE
               vLevel := PKG_UtlGlb.gcTrace_VRB;
               vTxt := '';
            END IF;
            IF pTxt IS NOT NULL THEN
                vTxt := pTxt || CHR(10) || vTxt || CHR(10) ;
            END IF;
            vTxt := vTxt||
                    'Periodo/Aggreg.    '||Pkg_Mago.StdOutDate(pDataDa)||' - '||Pkg_Mago.StdOutDate(pDataA)||' / '||vAgrTemp||CHR(10)||
                    'Elem/Org/Stato     '||NVL(TO_CHAR(pCodElem),'<null>')||'/'||NVL(TO_CHAR(pOrganizzazione),'<null>')||'/'||NVL(TO_CHAR(pStatoRete),'<null>')||CHR(10)||
                    'TipRete/TipMisura  '||NVL(pTipologiaRete,'<null>')||' / '||NVL(pTipiMisura,'<null>')||CHR(10)||
                    'TipFonte/TipClie   '||NVL(pFonte,'<null>')||' / '||NVL(pTipoClie,'<null>');
            PKG_Logs.StdLogAddTxt(vTxt,FALSE,pSqlCode);
            PKG_Logs.StdLogPrint(vLevel);
        END;

 BEGIN

    IF vAgrTemp > 1440 THEN
        vAgrTemp := 1440;
    END IF;

    vIdFonti := GetIdTipoFonti(pFonte);
    IF INSTR(pFonte,'!') <> 0 THEN
        CASE SUBSTR(pFonte,1,1)
            WHEN '0' THEN vTotTipFon := PKG_Misure.cMisTot;
            WHEN '1' THEN vTotTipFon := PKG_Misure.cMisDettTot;
        END CASE;
    ELSE
        vTotTipFon := PKG_Misure.cMisDett;
    END IF;
    vIdReti  := GetIdTipoReti(pTipologiaRete);
    vIdClie  := GetIdTipoClienti(pTipoClie);

    -- pulisce tabelle temporanee
    DELETE GTTD_VALORI_TEMP;
    DELETE GTTD_MISURE;

    /* Inserisce le misure/fonti per cui applicare i filtri FE */
    INSERT INTO GTTD_VALORI_TEMP ( tip , alf1 , alf2 )
    SELECT DISTINCT Pkg_Mago.gcTmpTipFilMisKey, cod_tipo_misura, cod_raggr_fonte
      FROM TIPO_FONTI tfon
          ,TIPI_MISURA tmis
     WHERE tmis.flg_apply_filter = 1
       AND INSTR(vTipiMisura, PKG_Mago.cSepCharLst || cod_tipo_misura || PKG_Mago.cSepCharLst) > 0
       AND (INSTR (PKG_Mago.cSepCharLst || SUBSTR(pfonte,INSTR(pfonte,'!')+1) || PKG_Mago.cSepCharLst
                   , PKG_Mago.cSepCharLst || cod_raggr_fonte || PKG_Mago.cSepCharLst ) > 0 OR INSTR(NVL(pfonte,'ALL'),'ALL') > 0 )
       AND tfon.flg_apply_filter = 1;

    IF SQL%ROWCOUNT > 0 THEN
       vFilterFlg := 1;
       vDataDeltaDa := vDataDa - pdeltaT;
       vDataDeltaA := vDataA - pdeltaT;
    END IF;

    -- produce i filtri elenchi dei codici filtro richiesti
    PKG_Mago.TrattaListaCodici(vTipiMisura,    PKG_Mago.gcTmpTipMisKey, vFlgNull);    -- non gestito il flag

  SELECT COUNT(CASE WHEN tip = Pkg_Mago.gcTmpTipFilMisKey AND ALF1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto ||'%' THEN 1 END) vtotpmp
        ,COUNT(CASE WHEN tip = Pkg_Mago.gcTmpTipFilMisKey AND ALF1 LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso ||'%' THEN 1 END) vtotpmc
        ,COUNT(CASE WHEN ALF1 LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso ||'%' OR ALF1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto ||'%' THEN 1 END) vtotpm
    INTO vtotpmp, vtotpmc, vtotpm
    FROM GTTD_VALORI_TEMP
   WHERE tip IN (Pkg_Mago.gcTmpTipFilMisKey,PKG_Mago.gcTmpTipMisKey);

    -- Completa parametri Tipo_misura ----------------------------------
    DBMS_OUTPUT.PUT_LINE( 'Completa parametri Tipo_misura: ' || SYSDATE );
    FOR i IN (SELECT A.COD_TIPO_MISURA,A.RISOLUZIONE_FE,A.MISURA_STATICA,
                     CASE WHEN COUNT(acq.TIPO_AGGREGAZIONE) > 0 THEN PKG_UtlGlb.gkFlagOn
                                                                ELSE PKG_UtlGlb.gkFlagOff
                     END ACQ,
                     CASE WHEN COUNT(agr.TIPO_AGGREGAZIONE) > 0 THEN PKG_UtlGlb.gkFlagOn
                                                                ELSE PKG_UtlGlb.gkFlagOff
                     END AGR
                FROM (SELECT A.COD_TIPO_MISURA,
                             CASE WHEN MISURA_STATICA = 1 THEN RISOLUZIONE_FE
                               WHEN vAgrTemp < RISOLUZIONE_FE THEN RISOLUZIONE_FE
                               ELSE  vAgrTemp
                             END RISOLUZIONE_FE,
                             MISURA_STATICA
                        FROM (SELECT COD_TIPO_MISURA, RISOLUZIONE_FE,PKG_MIsure.IsMisuraStatica(COD_TIPO_MISURA) MISURA_STATICA
                                FROM GTTD_VALORI_TEMP
                               INNER JOIN TIPI_MISURA ON ALF1 = COD_TIPO_MISURA
                               WHERE TIP = PKG_Mago.gcTmpTipMisKey
                              ) A
                     ) A
                LEFT OUTER JOIN TRATTAMENTO_ELEMENTI acq ON acq.COD_ELEMENTO = pCodElem
                                                        AND acq.COD_TIPO_MISURA = A.COD_TIPO_MISURA
                                                        AND acq.TIPO_AGGREGAZIONE = PKG_Mago.gcStatoNullo
                LEFT OUTER JOIN TRATTAMENTO_ELEMENTI agr ON agr.COD_ELEMENTO = pCodElem
                                                        AND agr.COD_TIPO_MISURA = A.COD_TIPO_MISURA
                                                        AND agr.TIPO_AGGREGAZIONE IN (PKG_Mago.gcStatoNormale,PKG_Mago.gcStatoAttuale)
               GROUP BY A.COD_TIPO_MISURA,A.RISOLUZIONE_FE,A.MISURA_STATICA
             ) LOOP

        vData := pDataA;

        UPDATE GTTD_VALORI_TEMP SET NUM1 = i.RISOLUZIONE_FE,
                                    NUM2 = i.MISURA_STATICA,
                                    DAT1 = vData             -- data ultimo valore per il tipo misura
         WHERE TIP IN (PKG_Mago.gcTmpTipMisKey , PKG_Mago.gcTmpTipFilMisKey )
           AND ALF1 = i.COD_TIPO_MISURA;
    END LOOP;

    IF vtotpm > 0 THEN
        /*Se presente PMP o PMC richiedo la PI per tutte le Fonti per calcolo successivo di azzeramento PMP/PMC se PI = 0 */
        INSERT INTO GTTD_MISURE (COD_TIPO_MISURA, DATA, VALORE, COD_TIPO_FONTE,FILTER)
            SELECT M.COD_TIPO_MISURA, M.DATA, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE,1 FILTER
              FROM (SELECT /*+ use_nl(t s) index(s scheduled_jobs_uk1) */
                           --T.ALF1 COD_TIPO_MISURA,
                           S.DATA_AL_VOLO, TMIS.RISOLUZIONE_FE
                      FROM GTTD_VALORI_TEMP T
                      INNER JOIN TIPI_MISURA TMIS ON (TMIS.COD_TIPO_MISURA = Pkg_Mago.gcPotenzaInstallata)
                      LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                            MIN(CASE WHEN vDataDeltaDa > DATARIF THEN vDataDeltaDa ELSE DATARIF END) DATA_AL_VOLO
                                         FROM SCHEDULED_JOBS
                                        WHERE COD_TIPO_MISURA IS NOT NULL
                                          AND ORGANIZZAZIONE = pOrganizzazione
                                          AND STATO = pStatoRete
                                          AND (DATARIF BETWEEN vDataDeltaDa AND vDataDeltaA
                                               OR vDataDeltaDa > DATARIF)
                                        GROUP BY COD_TIPO_MISURA
                                      ) S ON S.COD_TIPO_MISURA = Pkg_Mago.gcPotenzaInstallata
                     WHERE TIP = PKG_Mago.gcTmpTipMisKey
                       AND (T.ALF1 LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso || '%' OR T.ALF1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%')
                     GROUP BY --T.ALF1,
                           S.DATA_AL_VOLO, TMIS.RISOLUZIONE_FE
                   ) A,
                   TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                     Pkg_Mago.gcPotenzaInstallata,
                                                     '0', --TUTTE LE FONTI
                                                     vIdReti,
                                                     vIdClie,
                                                     vDataDa,
                                                     vDataA,
                                                     pOrganizzazione,
                                                     pStatoRete,
                                                     PKG_Misure.cMisDett,
                                                     A.RISOLUZIONE_FE,
                                                     A.DATA_AL_VOLO,
                                                     PKG_UtlGlb.gkFlagOff,
                                                     pDisconnect,
                                                     0 -- OnlyDiff  -- seleziono TUTTI GLI ISTANTI DI PI e NON solo i GRADINI
                                                    )
                         ) M
             WHERE M.DATA IS NOT NULL
             GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
             ORDER BY M.COD_TIPO_MISURA, M.DATA;

        Pkg_misure.AddinterpolationData(vDataDa, vDataA, vAgrTemp,vFilterFlg);

    END IF;

    /*CALCOLI FILTRI FE*/
    IF vFilterFlg = 1 THEN
        /*Se presente PMP o PMC con tipo fonte diverso da Solare e Eolico richiedo la PAS per calcolo filtri */
        INSERT INTO GTTD_MISURE (COD_TIPO_MISURA, DATA, VALORE, COD_TIPO_FONTE,FILTER)
                SELECT M.COD_TIPO_MISURA, M.DATA + pdeltaT, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE,vFilterFlg
                  FROM (SELECT /*+ use_nl(t f s) index(s scheduled_jobs_uk1) */
                               TMIS.COD_TIPO_MISURA COD_TIPO_MISURA, S.DATA_AL_VOLO, GREATEST(TMIS.RISOLUZIONE_FE,vAgrTemp) RISOLUZIONE_FE, SUM(DISTINCT f.id_raggr_fonte) IDFONTI
                          FROM GTTD_VALORI_TEMP T
                          INNER JOIN TIPI_MISURA TMIS ON (TMIS.COD_TIPO_MISURA = Pkg_Mago.gcPotenzaAttScambiata)
                          INNER JOIN (SELECT r.id_raggr_fonte
                                            ,f.cod_raggr_fonte
                                            ,f.cod_raggr_fonte_alfa_rif
                                            ,f.cod_tipo_fonte
                                        FROM RAGGRUPPAMENTO_FONTI r
                                            ,TIPO_FONTI f
                                       WHERE r.cod_raggr_fonte = f.cod_raggr_fonte
                                     )  f ON T.alf2 = f.cod_raggr_fonte
                          LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                                MIN(CASE WHEN vDataDeltaDa > DATARIF THEN vDataDeltaDa ELSE DATARIF END) DATA_AL_VOLO
                                             FROM SCHEDULED_JOBS
                                            WHERE COD_TIPO_MISURA IS NOT NULL
                                              AND ORGANIZZAZIONE = pOrganizzazione
                                              AND STATO = pStatoRete
                                              AND (DATARIF BETWEEN vDataDeltaDa AND vDataDeltaA
                                                   OR vDataDeltaDa > DATARIF)
                                            GROUP BY COD_TIPO_MISURA
                                          ) S ON S.COD_TIPO_MISURA = Pkg_Mago.gcPotenzaAttScambiata
                         WHERE TIP = PKG_Mago.gcTmpTipFilMisKey
                           AND (T.ALF1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%' OR T.ALF1 LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso || '%' )
                         GROUP BY TMIS.COD_TIPO_MISURA, S.DATA_AL_VOLO, GREATEST(TMIS.RISOLUZIONE_FE,vAgrTemp)
                       ) A,
                       TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                         Pkg_Mago.gcPotenzaAttScambiata,
                                                         A.IDFONTI,
                                                         vIdReti,
                                                         vIdClie,
                                                         vDataDeltaDa,
                                                         vDataDeltaA,
                                                         pOrganizzazione,
                                                         pStatoRete,
                                                         PKG_Misure.cMisDett,
                                                         A.RISOLUZIONE_FE,
                                                         A.DATA_AL_VOLO,
                                                         PKG_UtlGlb.gkFlagOff,
                                                         pDisconnect
                                                        )
                             ) M
                 WHERE M.DATA IS NOT NULL
                 GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
                 ORDER BY M.COD_TIPO_MISURA, M.DATA;

        Pkg_misure.AddinterpolationData(vDataDa, vDataA, vAgrTemp,vFilterFlg);
        
        IF vtotpmp > 0 THEN
            INSERT INTO GTTD_MISURE (COD_TIPO_MISURA, DATA, VALORE, COD_TIPO_FONTE,FILTER)
                    SELECT M.COD_TIPO_MISURA, M.DATA + pdeltaT, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE,vFilterFlg
                      FROM (SELECT /*+ use_nl(t s) index(s scheduled_jobs_uk1) */
                                   T.ALF1 COD_TIPO_MISURA, S.DATA_AL_VOLO, T.NUM1 RISOLUZIONE_FE, SUM(DISTINCT f.id_raggr_fonte) IDFONTI
                              FROM GTTD_VALORI_TEMP T
                              INNER JOIN (SELECT r.id_raggr_fonte
                                                ,f.cod_raggr_fonte
                                                ,f.cod_raggr_fonte_alfa_rif
                                                ,f.cod_tipo_fonte
                                            FROM RAGGRUPPAMENTO_FONTI r
                                                ,TIPO_FONTI f
                                           WHERE r.cod_raggr_fonte = f.cod_raggr_fonte_alfa_rif
                                         )  f ON T.alf2 = f.cod_raggr_fonte
                              LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                                    MIN(CASE WHEN vDataDeltaDa > DATARIF THEN vDataDeltaDa ELSE DATARIF END) DATA_AL_VOLO
                                                 FROM SCHEDULED_JOBS
                                                WHERE COD_TIPO_MISURA IS NOT NULL
                                                  AND ORGANIZZAZIONE = pOrganizzazione
                                                  AND STATO = pStatoRete
                                                  AND (DATARIF BETWEEN vDataDeltaDa AND vDataDeltaA
                                                       OR vDataDeltaDa > DATARIF)
                                                GROUP BY COD_TIPO_MISURA
                                              ) S ON S.COD_TIPO_MISURA = T.ALF1
                             WHERE TIP = PKG_Mago.gcTmpTipFilMisKey
                               AND (T.ALF1 LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%')
                             GROUP BY T.ALF1, S.DATA_AL_VOLO, T.NUM1
                           ) A,
                           TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                             A.COD_TIPO_MISURA,
                                                             A.IDFONTI,
                                                             vIdReti,
                                                             vIdClie,
                                                             vDataDeltaDa,
                                                             vDataDeltaA,
                                                             pOrganizzazione,
                                                             pStatoRete,
                                                             PKG_Misure.cMisDett,
                                                             A.RISOLUZIONE_FE,
                                                             A.DATA_AL_VOLO,
                                                             PKG_UtlGlb.gkFlagOff,
                                                             pDisconnect
                                                            )
                                 ) M
                     WHERE M.DATA IS NOT NULL
                     GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
                     ORDER BY M.COD_TIPO_MISURA, M.DATA;

            Pkg_misure.AddinterpolationData(vDataDa, vDataA, vAgrTemp,vFilterFlg);
            Pkg_misure.CalcolaFilterPMP(vDataDa, vDataA, vAgrTemp);

        END IF;

        IF vtotpmc > 0 THEN
            Pkg_misure.CalcolaFilterPMC(vDataDa, vDataA, vAgrTemp);
        END IF;

    END IF;

   /*FINE CALCOLI FILTRI*/
 
    -- CURSORE ----------------------------------

    -- Dettaglio + Totale Fonti
    -- calcola prima il dettaglio e lo inserisce in GTTD e quindi calcola il totale dal dettaglio

    INSERT INTO GTTD_MISURE (COD_TIPO_MISURA, DATA, VALORE, COD_TIPO_FONTE)
            SELECT M.COD_TIPO_MISURA, M.DATA, MAX(M.VALORE) VALORE, M.COD_TIPO_FONTE
              FROM (SELECT /*+ use_nl(t s) index(s scheduled_jobs_uk1) */ T.ALF1 COD_TIPO_MISURA, S.DATA_AL_VOLO, T.NUM1 RISOLUZIONE_FE, DAT1 DATA_ULT_MISURA
                      FROM GTTD_VALORI_TEMP T
                      LEFT OUTER JOIN (SELECT COD_TIPO_MISURA,
                                            MIN(CASE WHEN vDataDa > DATARIF THEN vDataDa ELSE DATARIF END) DATA_AL_VOLO
                                         FROM SCHEDULED_JOBS
                                        WHERE COD_TIPO_MISURA IS NOT NULL
                                          AND ORGANIZZAZIONE = pOrganizzazione
                                          AND STATO = pStatoRete
                                          AND (   DATARIF BETWEEN vDataDa AND vDataA
                                               OR vDataDa > DATARIF)
                                        GROUP BY COD_TIPO_MISURA
                                      ) S ON S.COD_TIPO_MISURA = T.ALF1
                     WHERE TIP = PKG_Mago.gcTmpTipMisKey
                   ) A,
                   TABLE (PKG_Misure.GetMisureTabMis(pCodElem,
                                                     A.COD_TIPO_MISURA,
                                                     vIdFonti ,
                                                     vIdReti,
                                                     vIdClie,
                                                      vDataDa,
                                                     vDataA,
                                                     pOrganizzazione,
                                                     pStatoRete,
                                                     CASE WHEN vTotTipFon = PKG_Misure.cMisDettTot THEN PKG_Misure.cMisDett ELSE vTotTipFon END,
                                                     A.RISOLUZIONE_FE,
                                                     A.DATA_AL_VOLO,
                                                     PKG_UtlGlb.gkFlagOff,
                                                     pDisconnect
                                                    )
                         ) M
             WHERE M.DATA IS NOT NULL
             GROUP BY M.COD_TIPO_MISURA, M.DATA, M.COD_TIPO_FONTE
             ORDER BY M.COD_TIPO_MISURA, M.DATA;

    --Calcola Interpolazione lineare
    Pkg_misure.AddinterpolationData(vDataDa, vDataA, vAgrTemp,0);

    IF vtotpm > 0 THEN
       MERGE INTO GTTD_MISURE T USING
           (
          SELECT  tmp.cod_tipo_misura,tmp.cod_tipo_fonte,tmp.DATA,CASE WHEN pi.valore = 0 THEN 0 ELSE tmp.valore END valore
              FROM GTTD_MISURE tmp
                  ,GTTD_MISURE pi
             WHERE pi.cod_tipo_misura = Pkg_Mago.gcPotenzaInstallata
               AND (tmp.cod_tipo_misura LIKE Pkg_Mago.gcPotenzaMeteoPrevisto || '%' OR tmp.cod_tipo_misura LIKE Pkg_Mago.gcPotenzaMeteoCieloTerso || '%')
               AND pi.DATA = tmp.DATA
               AND tmp.cod_tipo_fonte = pi.cod_tipo_fonte
               AND pi.valore = 0
           ) s
         ON (T.cod_tipo_misura = s.cod_tipo_misura AND T.cod_tipo_fonte = s.cod_tipo_fonte AND T.DATA = s.DATA)
       WHEN MATCHED THEN
       UPDATE SET T.valore = s.valore;
    END IF;

    IF vTotTipFon = PKG_Misure.cMisDettTot THEN

      OPEN pRefCurs FOR SELECT tmp.COD_TIPO_MISURA,tmp.DATA,CASE WHEN tip.rilevazioni_nagative = 0 AND tmp.VALORE < 0 THEN 0 ELSE tmp.valore END valore,tmp.COD_TIPO_FONTE,tmp.INTERPOLAZIONE
                            FROM GTTD_MISURE tmp
                                ,TIPI_MISURA tip
                          WHERE NVL(tmp.FILTER,0) = 0
                            AND tmp.cod_tipo_misura = tip.cod_tipo_misura
                          UNION ALL
                         SELECT tmp.COD_TIPO_MISURA,tmp.DATA,SUM(CASE WHEN tip.rilevazioni_nagative = 0 AND tmp.VALORE < 0 THEN 0 ELSE tmp.valore END)
                               ,'0' COD_TIPO_FONTE,CASE WHEN SUM(tmp.INTERPOLAZIONE) > 0 THEN 1 ELSE 0 END
                           FROM GTTD_MISURE tmp
                                ,TIPI_MISURA tip
                          WHERE NVL(tmp.FILTER,0) = 0
                            AND tmp.cod_tipo_misura = tip.cod_tipo_misura
                          GROUP BY tmp.COD_TIPO_MISURA,tmp.DATA
                          ORDER BY COD_TIPO_MISURA, COD_TIPO_FONTE, DATA;

    ELSE
        -- Dettaglio o Totale Fonti
        OPEN pRefCurs FOR SELECT COD_TIPO_MISURA,DATA,VALORE,COD_TIPO_FONTE,INTERPOLAZIONE
                            FROM (
                                  SELECT tmp.COD_TIPO_MISURA,tmp.DATA,SUM(CASE WHEN tip.rilevazioni_nagative = 0 AND tmp.VALORE < 0 THEN 0 ELSE tmp.valore END) VALORE
                                        ,CASE WHEN vTotTipFon = PKG_Misure.cMisTot THEN '0' ELSE tmp.COD_TIPO_FONTE END COD_TIPO_FONTE
                                        ,CASE WHEN SUM(tmp.INTERPOLAZIONE) > 0 THEN 1 ELSE 0 END INTERPOLAZIONE
                                    FROM GTTD_MISURE tmp
                                        ,TIPI_MISURA tip
                                   WHERE NVL(tmp.FILTER,0) = 0
                                     AND tmp.cod_tipo_misura = tip.cod_tipo_misura
                                   GROUP BY tmp.COD_TIPO_MISURA,tmp.DATA,CASE WHEN vTotTipFon = PKG_Misure.cMisTot THEN '0' ELSE tmp.COD_TIPO_FONTE END
                                  )
                           ORDER BY COD_TIPO_MISURA, COD_TIPO_FONTE, DATA;
    END IF;

    PKG_Logs.TraceLog('GetMisure - Periodo: '||PKG_Mago.StdOutDate(vDataDa)||'-'||PKG_Mago.StdOutDate(vDataA)||
                               ' - Elemento='||pCodElem||' - Org/Sta='||pOrganizzazione||'/'||pStatoRete||
                                    ' - Mis='||pTipiMisura||' - Ret='||pTipologiaRete||' - Fon='||pFonte||' - Cli='||pTipoClie,
                      PKG_UtlGlb.gcTrace_VRB);
--    fLog(NULL,NULL);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         fLog(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,SQLCODE);
         --PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         --PKG_Logs.StdLogPrint(PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetMisure;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMisureCG          (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pGestElem         IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTipiMisura       IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTipologiaRete    IN VARCHAR2,
                                 pFonte            IN VARCHAR2,
                                 pTipoClie         IN VARCHAR2,
                                 pAgrTemporale     IN INTEGER DEFAULT 15,
                                 pDisconnect       IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce un cursore con le curve misura richieste partendo da codice gestionale
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    GetMisure(pRefCurs,
              PKG_Elementi.GetCodElemento(pGestElem),
              pDataDa,
              pDataA,
              pTipiMisura,
              pOrganizzazione,
              pStatoRete,
              pTipologiaRete,
              pFonte,
              pTipoClie,
              pAgrTemporale,
              pDisconnect);
 END GetMisureCG;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetMisureTabMisStat  (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAgrTemporale     IN INTEGER,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pTable            IN VARCHAR2,
                                 pOrganizzazione   IN INTEGER,
                                 pTipoAggreg1      IN INTEGER,
                                 pTipoAggreg2      IN INTEGER,
                                 pGerECS           IN NUMBER,
                                 pTotTipFon        IN NUMBER,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pOnlyDiff         IN NUMBER DEFAULT 1) RETURN t_Misure PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le misure richieste in formato pipeline per i tipi misura statici
-----------------------------------------------------------------------------------------------------------*/

    vMis     t_Misura;

    vDataDa  DATE;
    vDataA   DATE;

    c1       PKG_UtlGlb.t_query_cur;
    c2       PKG_UtlGlb.t_query_cur;
/*
    vSql1    VARCHAR2(2200) :=
              'SELECT PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                      || -- pDataDa,pAgrTemporale
                     'COD_RAGGR_FONTE, MIN(DATA) MIN_DATA, MAX(DATA) MAX_DATA '           ||
                'FROM (SELECT DISTINCT '                                                  ||
                             'CASE WHEN DATA_ATTIVAZIONE < :DtDa THEN :DtDa '             || -- pDataDa,pDataDa
                                  'ELSE DATA_ATTIVAZIONE '                                ||
                             'END DATA '                                                  ||
                        'FROM MISURE_#TAB#_STATICHE '                                     ||
                       'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '    ||
                       'INNER JOIN '                                                      ||
                             '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                    ||
                                'FROM TIPO_FONTI '                                        ||
                               'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '  ||
                                'WHERE 1=1 #FILTRO_TIPO_FONTI# '                          ||
                             ') USING(COD_TIPO_FONTE) '                                   ||
                       '#JOIN_TIPO_RETE# '                                                ||
                       '#JOIN_TIPO_CLIENTE# '                                             ||
                       'WHERE COD_ELEMENTO = :ce '                                        || -- pCodElem
                         'AND COD_TIPO_MISURA = :tm '                                     || -- pTipMis
                         'AND ORGANIZZAZIONE = :org '                                     || -- pOrganizzazione
                         'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                          || -- pTipoAggreg1,pTipoAggreg2
                         '#FILTRO_TIPO_RETE# '                                            ||
                         '#FILTRO_TIPO_CLIENTE# '                                         ||
                         'AND DATA_DISATTIVAZIONE >= :DtDa '                              || -- pDataDa
                         'AND DATA_ATTIVAZIONE    <= :Dta '                               || -- pDataA
                     '), '                                                                ||
                     '(SELECT DISTINCT COD_RAGGR_FONTE '                                  ||
                        'FROM ( SELECT m.*, #DATATRCALC# data_disattivazione_calc FROM MISURE_#TAB#_STATICHE m )  ' ||
                       'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '    ||
                       'INNER JOIN '                                                      ||
                             '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                    ||
                                'FROM TIPO_FONTI '                                        ||
                               'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '  ||
                                'WHERE 1=1 #FILTRO_TIPO_FONTI# '                          ||
                             ') USING(COD_TIPO_FONTE) '                                   ||
                       '#JOIN_TIPO_RETE# '                                                ||
                       '#JOIN_TIPO_CLIENTE# '                                             ||
                       'WHERE COD_ELEMENTO = :ce '                                        || -- pCodElem
                         'AND COD_TIPO_MISURA = :tm '                                     || -- pTipMis
                         'AND ORGANIZZAZIONE = :org '                                     || -- pOrganizzazione
                         'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                          || -- pTipoAggreg1,pTipoAggreg2
                         '#FILTRO_TIPO_RETE# '                                            ||
                         '#FILTRO_TIPO_CLIENTE# '                                         ||
                         'AND DATA_DISATTIVAZIONE_CALC >= :DtDa '                              || -- pDataDa
                         'AND DATA_ATTIVAZIONE    <= :Dta '                               || -- pDataA
                     ') '                                                                 ||
--               'GROUP BY DATA, COD_RAGGR_FONTE '                                          ||
               'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag), COD_RAGGR_FONTE '       || -- pDataDa,pAgrTemporale
               'ORDER BY 1,2';

    vSql2    VARCHAR2(900) :=
              'SELECT SUM(VALORE) '                                                       ||
                'FROM ( SELECT m.*, #DATATRCALC# data_disattivazione_calc FROM MISURE_#TAB#_STATICHE m )  ' ||
               'INNER JOIN TRATTAMENTO_ELEMENTI USING (COD_TRATTAMENTO_ELEM) '            ||
               'INNER JOIN '                                                              ||
                     '(SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE '                            ||
                        'FROM TIPO_FONTI '                                                ||
                       'INNER JOIN RAGGRUPPAMENTO_FONTI USING(COD_RAGGR_FONTE) '          ||
                        'WHERE 1=1 #FILTRO_TIPO_FONTI# '                                  ||
                     ') USING(COD_TIPO_FONTE) '                                           ||
               '#JOIN_TIPO_RETE# '                                                        ||
               '#JOIN_TIPO_CLIENTE# '                                                     ||
               'WHERE COD_ELEMENTO = :ce '                                                || -- pCodElem
                 'AND COD_TIPO_MISURA = :tm '                                             || -- pTipMis
                 'AND ORGANIZZAZIONE = :org '                                             || -- pOrganizzazione
                 'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                                  || -- pTipoAggreg1,pTipoAggreg2
                 '#FILTRO_TIPO_RETE# '                                                    ||
                 '#FILTRO_TIPO_CLIENTE# '                                                 ||
                 'AND :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '              || -- vMis.DATA
--                 'AND DATA_DISATTIVAZIONE >= :DtDa '                                      || -- vDataDa
--                 'AND DATA_ATTIVAZIONE    <= :Dta '                                       || -- vDataA
                 'AND COD_RAGGR_FONTE = :fo ';                                               -- vMis.COD_TIPO_FONTE;
*/

   vSql2 VARCHAR2(3000) :=
                            'WITH mis AS ' ||
					                     '( SELECT  m.*, #DATAELECALC# data_disattivazione_calc ' ||
					                                 ',CASE WHEN data_attivazione < :daDa THEN :daDa ' ||
					                                  'ELSE DATA_ATTIVAZIONE ' ||
					                             'END  data_attivazione_calc ' ||
					                         'FROM MISURE_#TAB#_STATICHE m ' ||
					                             ', trattamento_elementi tr ' ||
					                             'WHERE tr.cod_trattamento_elem = m.cod_trattamento_elem ' ||
					                             ') ' ||
					'SELECT valore, cod_tipo_misura, PKG_Misure.GetRoundDate(:daDa, data_attivazione,:ag ) data_attivazione, cod_raggr_fonte ' ||
					  'FROM ( ' ||
					        'SELECT valore ' ||
					              ',data_attivazione ' ||
					              ',LAG(valore) OVER ( PARTITION BY cod_raggr_fonte ORDER BY data_attivazione ) val_prev ' ||
					              ',cod_raggr_fonte ' ||
					              ',cod_tipo_misura ' ||
					          'FROM (  ' ||
					                'SELECT SUM(valore) valore, cod_tipo_misura, data_attivazione , cod_raggr_fonte ' ||
					                  'FROM ( ' ||
																	'SELECT /* use_nl(d f tr mis ) */ ' ||
																	      'cod_raggr_fonte ' ||
																	      ',tr.cod_tipo_misura ' ||
																	      ',m.cod_trattamento_elem ' ||
																	      ',ROW_NUMBER() OVER ( PARTITION BY d.data_attivazione ,tr.cod_trattamento_elem ORDER BY m.data_attivazione_calc DESC) num ' ||
																	      ',d.data_attivazione ' ||
																	      ',m.data_attivazione_calc ' ||
																	      ',m.data_disattivazione_calc ' ||
																	      ',valore ' ||
																    'FROM mis m ' ||
																	 'INNER JOIN trattamento_elementi tr ON (tr.cod_trattamento_elem = m.cod_trattamento_elem) ' ||
																	 'INNER JOIN ' ||
																	           '(SELECT cod_raggr_fonte,cod_tipo_fonte ' ||
																	              'FROM tipo_fonti ' ||
																	             'INNER JOIN raggruppamento_fonti USING(cod_raggr_fonte) ' ||
																	             'WHERE 1=1 #FILTRO_TIPO_FONTI# ' ||
												                      ') f USING(cod_tipo_fonte) ' ||
																	 '#JOIN_TIPO_RETE# ' ||
																	 '#JOIN_TIPO_CLIENTE# ' ||
																	 'INNER JOIN ( SELECT :daDa + ( (level-1) / (1*24*60/:ag)  ) data_attivazione ' ||
																	                    ',:daDa + ( (level) / (1*24*60/:ag) ) data_disattivazione ' ||
																	                'FROM DUAL ' ||
																	             'CONNECT BY LEVEL <= (:daA - (:daDa)) * (1*24*60/:ag) +1' ||
																	             ') d ON (d.data_disattivazione BETWEEN m.data_attivazione_calc AND m.data_disattivazione_calc ) ' ||
																	 'WHERE COD_ELEMENTO = :ce ' ||
												             'AND COD_TIPO_MISURA = :tm ' ||
												             'AND ORGANIZZAZIONE = :org ' ||
												             'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) ' ||
												             '#FILTRO_TIPO_RETE# ' ||
												             '#FILTRO_TIPO_CLIENTE# ' ||
												  	         'AND m.data_attivazione_calc BETWEEN :daDa AND  :daA ' ||
																	   --'AND COD_RAGGR_FONTE IN (:fo) ' ||
					                        ') WHERE num = 1 ' ||
					                 'GROUP BY cod_tipo_misura, cod_raggr_fonte ,data_attivazione ' ||
					                 ') ' ||
					       ') ' ||
					  'WHERE (valore != val_prev OR val_prev IS NULL OR :onlydiff != 1)' ||
					  'ORDER BY  data_attivazione, cod_raggr_fonte ' ;

 BEGIN

    IF pDisconnect = 1  AND pTable = 'ACQUISITE' THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql2 := REPLACE (vSql2, '#DATAELECALC#' , ' nvl(lead(data_attivazione) over ( partition by tr.cod_trattamento_elem order by data_attivazione ),to_date(''01013000'',''ddmmyyyy''))' );
    ELSE
       vSql2 := REPLACE (vSql2, '#DATAELECALC#' , ' data_disattivazione ' );
    END IF;

    --vSql1 := CompletaSqlMisure(vSql1,NULL,NULL,pTable,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,pGerECS,pTotTipFon,pDataDa,pDataA);
    vSql2 := CompletaSqlMisure(vSql2,NULL,NULL,pTable,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,pGerECS,pTotTipFon,pDataDa,pDataA);
     --dbms_output.put_line (vSql2);
    OPEN c2 FOR vSql2
            USING pDataDa, pDataDa, pDataDa, pAgrTemporale, pTipiFonte, pDataDa, pAgrTemporale, pDataDa, pAgrTemporale, pDataA, pDataDa, pAgrTemporale, pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie, pDataDa, pDataA, pOnlyDiff;
--              USING pTipiFonte,pCodElem,pTipMis,pOrganizzazione,pTipoAggreg1,pTipoAggreg2,pTipiRete,pTipiClie,vDataA,vMis.COD_TIPO_FONTE;
        LOOP
            FETCH c2 INTO vMis.valore, vMis.cod_tipo_misura, vMis.DATA, vMis.cod_tipo_fonte;
            EXIT WHEN c2%NOTFOUND;
            --PRINT(CHR(9)||CHR(9)||vMis.VALORE);
            PIPE ROW(vMis);
        END LOOP;
        IF c2%ROWCOUNT = 0 THEN
            vMis.VALORE := 0;
            --PRINT(CHR(9)||CHR(9)||vMis.VALORE||' non trovato - forzato 0');
            PIPE ROW(vMis);
        END IF;
        CLOSE c2;

 END GetMisureTabMisStat;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetMisureTabMis      (pCodElem          IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pTipMis           IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pTipiFonte        IN NUMBER,
                                 pTipiRete         IN NUMBER,
                                 pTipiClie         IN NUMBER,
                                 pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pTotTipFon        IN INTEGER,
                                 pAgrTemporale     IN INTEGER,
                                 pDataAlVolo       IN DATE,
                                 pForceEmpyRow     IN INTEGER DEFAULT PKG_UtlGlb.gkFlagOff,
                                 pDisconnect       IN INTEGER DEFAULT 0,
                                 pOnlyDiff         IN NUMBER DEFAULT 1) RETURN t_Misure PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le misure richieste in formato pipeline per il tipo misura / fonti ricevute
-----------------------------------------------------------------------------------------------------------*/
    vOrganizz        INTEGER := pOrganizzazione;
    vStatoRete       INTEGER := pStatoRete;
    vCount           NUMBER  := 0;
    vFoundPI         BOOLEAN := FALSE;

    TYPE             t_TipMisBase IS VARRAY(12) OF INTEGER;
    vTipMisBase      t_TipMisBase :=  t_TipMisBase();

    vRaggrMis        CHAR(3);
    vGerECS          TIPI_ELEMENTO.GER_ECS%TYPE;
    vRisoluzPI       TIPI_MISURA.RISOLUZIONE_FE%TYPE;

    --vDiffDate        NUMBER  := 0;
    --vIntervallo      NUMBER;

    vTipoAggreg1     NUMBER(1):= -1;
    vTipoAggreg2     NUMBER(1):= -1;

    vMisStatica      BOOLEAN  := FALSE;
    vMisNormale      BOOLEAN  := FALSE;
    vTabMisure       REL_ELEMENTO_TIPMIS.TAB_MISURE%TYPE;

    vMisAggregate    BOOLEAN  := FALSE;
    vMisAcquisite    BOOLEAN  := FALSE;

    vDatAcqDa        DATE     := NULL;
    vDatAcqA         DATE     := NULL;
    vDatAgrDa        DATE     := NULL;
    vDatAgrA         DATE     := NULL;

    vDatAcqStatDa    DATE     := NULL;
    vDatAcqStatA     DATE     := NULL;
    vDatAgrStatDa    DATE     := NULL;
    vDatAgrStatA     DATE     := NULL;

    vDatAcqTotDa     DATE     := NULL;
    vDatAcqTotA      DATE     := NULL;
    vDatAgrTotDa     DATE     := NULL;
    vDatAgrTotA      DATE     := NULL;

    vDatAcqStatTotDa DATE     := NULL;
    vDatAcqStatTotA  DATE     := NULL;
    vDatAgrStatTotDa DATE     := NULL;
    vDatAgrStatTotA  DATE     := NULL;

    vDatAlVoloDa     DATE     := NULL;
    vDatAlVoloA      DATE     := NULL;
    vDatAlVoloTotDa  DATE     := NULL;
    vDatAlVoloTotA   DATE     := NULL;

    vCur             PKG_UtlGlb.t_query_cur;

    vMis             t_Misura;

    vDataDa          DATE    := pDataDa;
    vDataA           DATE    := pDataA;
    vTabella         VARCHAR2(10);

    vSqlMisStat      VARCHAR2(350) :=
                    'SELECT COD_TIPO_MISURA,DATA,'                                                      ||
                           'SUM(VALORE), #FONTE1# '                                                     ||
                      'FROM (SELECT * FROM TABLE(PKG_MISURE.GetMisureTabMisStat(:ce,:tm,:ag,'           || -- pCodElem, pTipMis, pAgrTemporale
                                                                               ':fte,:rte,:cli,'        || -- pTipiFonte,pTipiRete,pTipiClie
                                                                               ':DtDa,:DtA,:tab,'       || -- vDat...Da, vDat...A,tabella
                                                                               ':org,:ta1,:ta2,'        || -- vOrganizz,vTipoAggreg1,vTipoAggreg2,
                                                                               ':ecs,:tot,:disconnect,:OnlyDiff))) '          || -- vGerEcs,pTotTipFon,pDisconnect
                     'GROUP BY COD_TIPO_MISURA,DATA #FONTE2# ';

    vSqlMisBase      VARCHAR2(1400) :=
                     'SELECT COD_TIPO_MISURA,PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                     || -- vDat...Da, pAgrTemporale
                            --'#AGR#(VALORE) VALORE, #FONTE1# '                                                   ||
                            '#AGR#(VALORE) VALORE, COD_RAGGR_FONTE '                                            ||
                      'FROM (SELECT COD_TIPO_MISURA,DATA, COD_RAGGR_FONTE,'                                      ||
                                   --'SUM(CASE WHEN B.COD_ELEMENTO IS NULL THEN 0 ELSE VALORE END) VALORE '       ||
                                   'SUM(VALORE) VALORE '                                                        ||
                              'FROM MISURE_#TAB# #PART# '                                                       ||
                              'INNER JOIN (SELECT COD_TRATTAMENTO_ELEM,COD_TIPO_MISURA,COD_ELEMENTO,'           ||
                                                 'NVL(cod_raggr_fonte_alfa_rif,COD_RAGGR_FONTE) COD_RAGGR_FONTE '                                             ||
                                            'FROM TRATTAMENTO_ELEMENTI '                                        ||
                                          '#JOIN_TIPO_FONTI# '                                                  ||
                                          '#JOIN_TIPO_RETE# '                                                   ||
                                          '#JOIN_TIPO_CLIENTE# '                                                ||
                                          'WHERE COD_ELEMENTO = :ce '                                           || -- pCodElem
                                            'AND ORGANIZZAZIONE = :org '                                        || -- vOrganizz
                                            'AND COD_TIPO_MISURA = :tm '                                        || -- pTipMis
                                            'AND TIPO_AGGREGAZIONE IN (:ta1,:ta2) '                             || -- vTipoAggreg1,vTipoAggreg2
                                            '#FILTRO_TIPO_FONTI# '                                              ||
                                            '#FILTRO_TIPO_RETE# '                                               ||
                                            '#FILTRO_TIPO_CLIENTE# '                                            ||
                                        ') A USING (COD_TRATTAMENTO_ELEM) '                                     ||
                             --'LEFT OUTER JOIN GERARCHIA_#GER##STA# B '                                          ||
                             --                 'ON (    A.COD_ELEMENTO = B.COD_ELEMENTO '                        ||
                             --                     'AND DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) ' ||
                             'WHERE DATA BETWEEN :DtDa AND :Dta '                                               || -- vDat...Da, vDat...A
                               --'#FILT_ELEM_NULL# '                                                              ||
                             'GROUP BY COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE '                                   ||
                           ') '                                                                                 ||
                     'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag),'                                        || -- vDat...Da, pAgrTemporale
                              --'COD_TIPO_MISURA #FONTE2# ';
                              'COD_TIPO_MISURA, COD_RAGGR_FONTE ';

    vSqlMisBase2     VARCHAR2(1400);

    vSqlMisAlVolo    VARCHAR2(1300) :=
                    'SELECT COD_TIPO_MISURA,PKG_Misure.GetRoundDate(:DtDa,DATA,:ag) DATA,'                      || -- vDat...Da, pAgrTemporale
                           --'#AGR#(VALORE) VALORE, #FONTE1# '                                                    ||
                           '#AGR#(VALORE) VALORE, COD_RAGGR_FONTE '                                             ||
                      'FROM (SELECT COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE,'                                      ||
                                   --'SUM(CASE WHEN B.COD_ELEMENTO IS NULL THEN 0 ELSE VALORE END) VALORE '       ||
                                   'SUM(VALORE) VALORE '                                                        ||
                              'FROM MISURE_ACQUISITE '                                                   ||
                             'INNER JOIN (SELECT DISTINCT COD_TRATTAMENTO_ELEM,NVL(cod_raggr_fonte_alfa_rif,COD_RAGGR_FONTE) COD_RAGGR_FONTE,'                ||
                                                'COD_TIPO_MISURA,COD_ELEMENTO '                                 ||
                                           'FROM TRATTAMENTO_ELEMENTI tr '                                         ||
                                          'INNER JOIN TABLE(PKG_Elementi.LeggiGerarchiaLineare(:DtDa,:DtA,'     || -- vDat...Da,vDat...A
                                                                                              ':org,:sr,:ce,:disconnect)) g ' || -- vOrganizz,vStatoRete,pCodElem
                                                                 'USING(COD_ELEMENTO) '                         ||
                                          '#JOIN_TIPO_FONTI# '                                                  ||
                                          '#JOIN_TIPO_RETE# '                                                   ||
                                          '#JOIN_TIPO_CLIENTE# '                                                ||
                                          'WHERE ORGANIZZAZIONE = :org '                                        || -- PKG_Mago.gcOrganizzazELE (acquisite appartengono solo a ELE)
                                            'AND COD_TIPO_MISURA = :tm '                                        || -- pTipMis
                                            'AND TIPO_AGGREGAZIONE = 0 '                                        || -- costante 0 perche' legge solo acquisite
                                            '#FILTRO_TIPO_FONTI# '                                              ||
                                            '#FILTRO_TIPO_RETE# '                                               ||
                                            '#FILTRO_TIPO_CLIENTE# '                                            ||
                                        ') A USING (COD_TRATTAMENTO_ELEM) '                                     ||
                             --'INNER JOIN GERARCHIA_#GER##STA# B '                                               ||
                             --                 'ON (    A.COD_ELEMENTO = B.COD_ELEMENTO '                        ||
                             --                     'AND DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) ' ||
                             'WHERE DATA BETWEEN :DtDa AND :Dta '                                               || -- vDat...Da, vDat...A
                             'GROUP BY COD_TIPO_MISURA,DATA,COD_RAGGR_FONTE '                                   ||
                           ')'                                                                                  ||
                     'GROUP BY PKG_Misure.GetRoundDate(:DtDa,DATA,:ag),'                                        || -- vDat...Da, pAgrTemporale
                              --'COD_TIPO_MISURA #FONTE2# ';
                              'COD_TIPO_MISURA, COD_RAGGR_FONTE ';

 BEGIN

   -- determina su quale/i tabelle misure selezionare le curve
    vTabMisure   := GetTabMisure(pCodElem,pTipMis);

    IF vTabMisure <> 0 THEN             -- vTabMisure = 0 --> Tipo Misura non utilizzato per l'elemento

        IF PKG_Misure.IsMisuraStatica(pTipMis) = PKG_UtlGlb.gkFlagON THEN
            vMisStatica := TRUE;
        ELSE
            vMisNormale := TRUE;
            SELECT RISOLUZIONE_FE
              INTO vRisoluzPI
              FROM TIPI_MISURA
             WHERE COD_TIPO_MISURA = Pkg_Mago.gcPotenzaInstallata;
        END IF;

        IF pAgrTemporale <= 60 THEN
            vRaggrMis := 'AVG';
        ELSE
            vRaggrMis := 'MAX';
        END IF;

        vTipoAggreg1 := vStatoRete;
        vTipoAggreg2 := 0;

        IF PKG_Mago.IsMagoDGF THEN
            SELECT CASE WHEN GER_ECP = 0 THEN GER_ECS ELSE 0 END INTO vGerECS
             FROM ELEMENTI
            INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
            WHERE COD_ELEMENTO = pCodElem;
        ELSE
           SELECT GER_ECS INTO vGerECS
             FROM ELEMENTI
            INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
            WHERE COD_ELEMENTO = pCodElem;
        END IF;

        IF vGerECS = PKG_UtlGlb.gkFlagOn THEN
            IF vOrganizz != PKG_Mago.gcOrganizzazELE THEN
                vOrganizz := PKG_Mago.gcOrganizzazELE;
            END IF;
        END IF;

        IF vDataDa < PKG_Misure.GetRoundDate(pDataDa,PKG_Mago.GetStartDate(vDataDa),pAgrTemporale) THEN
            vDataDa := PKG_Misure.GetRoundDate(pDataDa,PKG_Mago.GetStartDate(vDataDa),pAgrTemporale);
            IF vDataDa > vDataA THEN
                vDataA := vDataDa;
            END IF;
        END IF;

        IF BITAND(vTabMisure,PKG_Mago.gcTabAcquisite) = PKG_Mago.gcTabAcquisite THEN
            vMisAcquisite := TRUE;
            -- imposta Range Date per misure acquisite
            vDatAcqDa := vDataDa;
            vDatAcqA  := vDataA;
            IF vMisStatica THEN
                IF pTotTipFon IN (cMisDettTot,cMisDett) THEN
                    vDatAcqStatDa    := vDataDa;
                    vDatAcqStatA     := vDataA;
                END IF;
                IF pTotTipFon IN (cMisDettTot,cMisTot) THEN
                    vDatAcqStatTotDa := vDataDa;
                    vDatAcqStatTotA  := vDataA;
                END IF;
            ELSE
                vTipMisBase.EXTEND;
                vTipMisBase(vTipMisBase.LAST) := PKG_Mago.gcTabAcquisite;
            END IF;
        END IF;

        IF BITAND(vTabMisure,PKG_Mago.gcTabAggregate) = PKG_Mago.gcTabAggregate THEN
           vMisAggregate := TRUE;
            -- imposta Range Date per misure aggregate
            vDatAgrDa := vDataDa;
            vDatAgrA  := vDataA;
            IF vMisStatica THEN
                vDatAgrStatDa := vDataDa;
                vDatAgrStatA  := vDataA;
            ELSE
                vTipMisBase.EXTEND;
                vTipMisBase(vTipMisBase.LAST) := PKG_Mago.gcTabAggregate;
            END IF;
        END IF;

        CASE pTotTipFon
            WHEN cMisDettTot THEN         -- Sono richiesti sia il dettaglio fonti che il totale
                    IF vMisAcquisite THEN
                        vDatAcqTotDa     := vDatAcqDa;
                        vDatAcqTotA      := vDatAcqA;
                        vDatAcqStatTotDa := vDatAcqDa;
                        vDatAcqStatTotA  := vDatAcqA;
                    END IF;
                    IF vMisAggregate THEN
                        vDatAgrTotDa     := vDatAgrDa;
                        vDatAgrTotA      := vDatAgrA;
                        vDatAgrStatTotDa := vDatAgrDa;
                        vDatAgrStatTotA  := vDatAgrA;
                    END IF;
            WHEN cMisTot THEN         -- E' richiesto il solo totale
                    IF vMisAcquisite THEN
                        vDatAcqTotDa     := vDatAcqDa;
                        vDatAcqTotA      := vDatAcqA;
                        vDatAcqStatTotDa := vDatAcqDa;
                        vDatAcqStatTotA  := vDatAcqA;
                    END IF;
                    IF vMisAggregate THEN
                        vDatAgrTotDa     := vDatAgrDa;
                        vDatAgrTotA      := vDatAgrA;
                        vDatAgrStatTotDa := vDatAgrDa;
                        vDatAgrStatTotA  := vDatAgrA;
                    END IF;
                    vDatAcqDa            := NULL;
                    vDatAcqA             := NULL;
                    vDatAgrDa            := NULL;
                    vDatAgrA             := NULL;
                    vDatAgrStatDa        := NULL;
                    vDatAgrStatA         := NULL;
            ELSE NULL;       -- E' richiesto solo il Dettaglio fonti
        END CASE;

        IF NOT vMisNormale THEN
            -- solo misure statiche
            vDatAcqDa        := NULL;
            vDatAcqA         := NULL;
            vDatAgrDa        := NULL;
            vDatAgrA         := NULL;
            vDatAcqTotDa     := NULL;
            vDatAcqTotA      := NULL;
            vDatAgrTotDa     := NULL;
            vDatAgrTotA      := NULL;
        END IF;

        IF NOT vMisStatica THEN
            -- solo misure non statiche
            vDatAcqStatDa    := NULL;
            vDatAcqStatA     := NULL;
            vDatAgrStatDa    := NULL;
            vDatAgrStatA     := NULL;
            vDatAcqStatTotDa := NULL;
            vDatAcqStatTotA  := NULL;
            vDatAgrStatTotDa := NULL;
            vDatAgrStatTotA  := NULL;
        END IF;

        IF pDataAlVolo IS NOT NULL THEN
            IF vMisAggregate AND vMisNormale THEN
                IF pDataAlVolo BETWEEN vDatAgrDa AND vDatAgrA THEN
                    vDatAlVoloA     := vDatAgrA;
                    vDatAlVoloDa    := pDataAlVolo;
                    vDatAgrA        := pDataAlVolo - cUnSecondo;
                END IF;
                IF pDataAlVolo BETWEEN vDatAgrTotDa AND vDatAgrTotA THEN
                    vDatAlVoloTotA  := vDatAgrTotA;
                    vDatAlVoloTotDa := pDataAlVolo;
                    vDatAgrTotA     := pDataAlVolo - cUnSecondo;
                END IF;
            END IF;
        END IF;

        IF vDatAgrTotDa > vDatAgrTotA THEN
            vDatAgrTotDa := NULL;
            vDatAgrTotA  := NULL;
        END IF;
        IF vDatAgrDa > vDatAgrA THEN
            vDatAgrDa := NULL;
            vDatAgrA  := NULL;
        END IF;

    END IF;

    --PKG_Logs.TraceLog(rpad('=',100,'='),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vOrganizz     '||RPAD(vOrganizz,10)    ||'vStatoRete  '||RPAD(vStatoRete,10),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('pAgrTemporale '||RPAD(pAgrTemporale,10)||'vTipoAggreg '||RPAD(vTipoAggreg1||'/'||vTipoAggreg2,10)||'pTotTipFon  '||CASE pTotTipFon WHEN PKG_Misure.cMisTot THEN 'Totale' ELSE 'Dettaglio' END,PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('pCodElem      '||RPAD(pCodElem,10)     ||'pTipMis     '||RPAD(pTipMis,10)   ||'vTabMisure  '||RPAD(vTabMisure,10) ||'vGerECS     '||vGerECS,PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('pTipiFonte    '||RPAD(pTipiFonte,10)   ||'pTipiRete   '||RPAD(pTipiRete,10) ||'pTipiClie   '||RPAD(pTipiClie,10),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vMisNormale   '||RPAD(CASE WHEN vMisNormale THEN 'SI' ELSE 'NO' END,10)||'vMisStatica '||CASE WHEN vMisStatica THEN 'SI' ELSE 'NO' END,PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('pData (input)  da '||PKG_Mago.StdOutDate(pDataDa         )||'  a '||PKG_Mago.StdOutDate(pDataA         )||' - pDataAlVolo '||PKG_Mago.StdOutDate(pDataAlVolo),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAcq        da '||PKG_Mago.StdOutDate(vDatAcqDa       )||'  a '||PKG_Mago.StdOutDate(vDatAcqA       ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAgr        da '||PKG_Mago.StdOutDate(vDatAgrDa       )||'  a '||PKG_Mago.StdOutDate(vDatAgrA       ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAcqStat    da '||PKG_Mago.StdOutDate(vDatAcqStatDa   )||'  a '||PKG_Mago.StdOutDate(vDatAcqStatA   ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAgrStat    da '||PKG_Mago.StdOutDate(vDatAgrStatDa   )||'  a '||PKG_Mago.StdOutDate(vDatAgrStatA   ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAcqTot     da '||PKG_Mago.StdOutDate(vDatAcqTotDa    )||'  a '||PKG_Mago.StdOutDate(vDatAcqTotA    ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAgrTot     da '||PKG_Mago.StdOutDate(vDatAgrTotDa    )||'  a '||PKG_Mago.StdOutDate(vDatAgrTotA    ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAcqStatTot da '||PKG_Mago.StdOutDate(vDatAcqStatTotDa)||'  a '||PKG_Mago.StdOutDate(vDatAcqStatTotA),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAgrStatTot da '||PKG_Mago.StdOutDate(vDatAgrStatTotDa)||'  a '||PKG_Mago.StdOutDate(vDatAgrStatTotA),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAlVolo     da '||PKG_Mago.StdOutDate(vDatAlVoloDa    )||'  a '||PKG_Mago.StdOutDate(vDatAlVoloA    ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('vDatAlVoloTot  da '||PKG_Mago.StdOutDate(vDatAlVoloTotDa )||'  a '||PKG_Mago.StdOutDate(vDatAlVoloTotA ),PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog(rpad('-',100,'-'));


    IF vTabMisure <> 0 THEN             -- vTabMisure = 0 --> Tipo Misura non utilizzato per l'elemento

        IF vMisStatica THEN
            -- Misure Statiche
            IF vDatAcqStatDa IS NOT NULL THEN
                vDataDa := vDatAcqStatDa;
                vDataA  := vDatAcqStatA;
                vTabella:= 'ACQUISITE';
            END IF;
            IF vDatAcqStatTotDa IS NOT NULL THEN
                vDataDa := vDatAcqStatTotDa;
                vDataA  := vDatAcqStatTotA;
                vTabella:= 'ACQUISITE';
            END IF;
            IF vDatAgrStatDa IS NOT NULL THEN
                vDataDa := vDatAgrStatDa;
                vDataA  := vDatAgrStatA;
                vTabella:= 'AGGREGATE';
            END IF;
            IF vDatAgrStatTotDa IS NOT NULL THEN
                vDataDa := vDatAgrStatTotDa;
                vDataA  := vDatAgrStatTotA;
                vTabella:= 'AGGREGATE';
            END IF;
            IF pTotTipFon = PKG_Misure.cMisTot THEN
                vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE1#','''0'' COD_TIPO_FONTE');
                vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE2#',' ');
            ELSE
                vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE1#','COD_TIPO_FONTE');
                vSqlMisStat := REPLACE(vSqlMisStat,'#FONTE2#',',COD_TIPO_FONTE');
            END IF;
            OPEN vCur FOR vSqlMisStat
                    USING pCodElem,pTipMis,pAgrTemporale,pTipiFonte,pTipiRete,pTipiClie,vDataDa,vDataA,
                          vTabella,vOrganizz,vTipoAggreg1,vTipoAggreg2,vGerEcs,pTotTipFon,pDisconnect,pOnlyDiff;
            LOOP
                FETCH vCur INTO vMis;
                EXIT WHEN vCur%NOTFOUND;
                vCount := vCount + 1;
                PIPE ROW(vMis);
            END LOOP;
            CLOSE vCur;
        ELSE
            IF vDatAcqDa    IS NOT NULL OR
               vDatAcqTotDa IS NOT NULL OR
               vDatAgrDa    IS NOT NULL OR
               vDatAgrTotDa IS NOT NULL THEN
                FOR j IN vTipMisBase.FIRST .. vTipMisBase.LAST LOOP
                    CASE vTipMisBase(j)
                        WHEN PKG_Mago.gcTabAggregate THEN
                            -- Misure Aggregate Standard
                            IF vDatAgrDa IS NOT NULL THEN
                                vDataDa := vDatAgrDa;
                                vDataA  := vDatAgrA;
                            END IF;
                            IF vDatAgrTotDa IS NOT NULL THEN
                                vDataDa := vDatAgrTotDa;
                                vDataA  := vDatAgrTotA;
                            END IF;
                            vTabella := 'AGGREGATE';
                        WHEN PKG_Mago.gcTabAcquisite THEN
                            -- Misure Acquisite Standard
                            IF vDatAcqDa IS NOT NULL THEN
                                vDataDa := vDatAcqDa;
                                vDataA  := vDatAcqA;
                            END IF;
                            IF vDatAcqTotDa IS NOT NULL THEN
                                vDataDa := vDatAcqTotDa;
                                vDataA  := vDatAcqTotA;
                            END IF;
                            vTabella := 'ACQUISITE';
                    END CASE;
                    vSqlMisBase2 := CompletaSqlMisure(vSqlMisBase,pOrganizzazione,pStatoRete,vTabella,
                                                      vRaggrMis,pTipiFonte,pTipiRete,pTipiClie,vGerECS,pTotTipFon,vDataDa,vDataA);
                    IF pTotTipFon = PKG_Misure.cMisTot THEN
                        vSqlMisBase2 := 'SELECT COD_TIPO_MISURA,DATA,SUM(VALORE) VALORE,''0'' COD_TIPO_FONTE FROM('||vSqlMisBase2||') GROUP BY COD_TIPO_MISURA,DATA';
                    END IF;
                    OPEN vCur FOR vSqlMisBase2
                             USING vDataDa,pAgrTemporale,
                                   pCodElem,vOrganizz,pTipMis,vTipoAggreg1,vTipoAggreg2,
                                   pTipiFonte,pTipiRete,pTipiClie,
                                   vDataDa,vDataA,
                                   vDataDa,pAgrTemporale;
                    LOOP
                        FETCH vCur INTO vMis;
                        EXIT WHEN vCur%NOTFOUND;
                        vCount := vCount + 1;
                        PIPE ROW(vMis);
                    END LOOP;
                    CLOSE vCur;
                END LOOP;
            END IF;
            IF vDatAlVoloDa    IS NOT NULL OR
               vDatAlVoloTotDa IS NOT NULL THEN
                -- Aggregate AlVolo
                IF vDatAlVoloDa IS NOT NULL THEN
                    vDataDa := vDatAlVoloDa;
                    vDataA  := vDatAlVoloA;
                END IF;
                IF vDatAlVoloTotDa IS NOT NULL THEN
                    vDataDa := vDatAlVoloTotDa;
                    vDataA  := vDatAlVoloTotA;
                END IF;
                vTabella:= 'AGGREGATE';
                vSqlMisAlVolo := CompletaSqlMisure(vSqlMisAlVolo,pOrganizzazione,pStatoRete,'null',
                                                   vRaggrMis,pTipiFonte,pTipiRete,pTipiClie,vGerEcs,pTotTipFon,vDataDa,vDataA);
                IF pTotTipFon = PKG_Misure.cMisTot THEN
                    vSqlMisAlVolo := 'SELECT COD_TIPO_MISURA,DATA,SUM(VALORE) VALORE,''0'' COD_TIPO_FONTE FROM('||vSqlMisAlVolo||') GROUP BY COD_TIPO_MISURA,DATA';
                END IF;
                OPEN vCur FOR vSqlMisAlVolo
                        USING vDataDa,pAgrTemporale,
                              vDataDa,vDataA,vOrganizz,vStatoRete,pCodElem,pDisconnect,PKG_Mago.gcOrganizzazELE,pTipMis,
                              pTipiFonte,pTipiRete,pTipiClie,
                              vDataDa,vDataA,
                              vDataDa,pAgrTemporale;
                LOOP
                    FETCH vCur INTO vMis;
                    EXIT WHEN vCur%NOTFOUND;
                    vCount := vCount + 1;
                    PIPE ROW(vMis);
                END LOOP;
                CLOSE vCur;
            END IF;
        END IF;

    END IF;

    IF pForceEmpyRow = PKG_UtlGlb.gkFlagON THEN
        IF vCount = 0 THEN
            --PRINT('non trovato');
            vMis := NULL;
            vMis.DATA := pDataDa;
            vMis.COD_TIPO_MISURA := pTipMis;
            PIPE ROW(vMis);
        END IF;
    END IF;
    RETURN;

 END GetMisureTabMis;

-- ----------------------------------------------------------------------------------------------------------
-- Implementazioni versione 1.6a
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetMeasureOfflineSessionID  (pSessionID     OUT NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce un nuovo identificativo di sessione Forecast
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
        SELECT OFFLINE_MEASURE_REQUEST_IDSEQ.NEXTVAL INTO pSessionID FROM DUAL;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Misure.GetMeasureOfflineSessionID'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetMeasureOfflineSessionID;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */
BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Misure',
                          pStoreOnFile => FALSE);

END PKG_Misure;
/
SHOW ERRORS;


