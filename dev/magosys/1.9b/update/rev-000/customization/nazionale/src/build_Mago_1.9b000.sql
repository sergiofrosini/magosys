PROMPT =======================================================================================
PROMPT rel_19b rev 0 NAZ
PROMPT =======================================================================================

conn magonaz/magonaz

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT =======================================================================================
PROMPT DETERMINA TIPO INSTALLAZIONE ORACLE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT
WHENEVER SQLERROR EXIT SQLCODE

var ORA CHAR(3);

DECLARE
  V_BNR VARCHAR2(128) := 'XX';
BEGIN
  BEGIN
     SELECT BANNER INTO V_BNR
       FROM V$VERSION
     WHERE INSTR(UPPER(BANNER),'ORACLE DATABASE')>0;
    EXCEPTION WHEN OTHERS THEN
    :ORA := 'xxx';
    RAISE_APPLICATION_ERROR (-20200, 'VERSION NOT FOUND '||CHR(10)||' STOP INSTALLAZIONE!');
  END;
  DBMS_OUTPUT.PUT_LINE('INSTALLAZIONE SU:'||CHR(10)||'    '||V_BNR);
  IF INSTR (UPPER(V_BNR), 'ENTERPRISE') > 0 THEN
    :ORA := 'ENT';
  ELSE
    :ORA := 'STD';
  END IF;
END;
/
COL DUMMY NEW_VALUE ORA
SELECT :ORA DUMMY FROM DUAL;


PROMPT =======================================================================================
PROMPT DEFINIZIONE OGGETTI MAGO   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE

PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT

@./Sequences/MANUTENZIONE_SEQ.sql;

PROMPT _______________________________________________________________________________________
PROMPT Directories
PROMPT

@./Directories/MAGO_EXTDATADIR.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

@./Tables/PRODUTTORI_TMP_EXT.sql;
@./Tables/ELEM_DEF_EOLICO_TMP_EXT.sql;
@./Tables/ELEM_DEF_SOLARE_TMP_EXT.sql;
@./Tables/MISURE_TMP_EXT.sql;
@./Tables/PRODUTTORI_TMP.sql;
@./Tables/ELEM_DEF_EOLICO_TMP.sql;
@./Tables/ELEM_DEF_SOLARE_TMP.sql;
@./Tables/MISURE_TMP.sql;
@./Tables/MISURE_ERR_&ORA.sql;
@./Tables/MISURE_ACQUISITE_MANUTENZIONE.sql;


@./Tables/ELEMENTI_GDF_EOLICO.sql;
@./Tables/ELEMENTI_GDF_SOLARE.sql;

@./Tables/CFG_APPLICATION_PARAMETER.sql;
@./Tables/RUN_STEP.sql;
@./Tables/OFFSET.sql;

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT

@./Types/T_ELEMAN_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT

@./Packages/PKG_GERARCHIA_DGF.sql;
@./Packages/PKG_MAGO_DGF.sql;
@./Packages/PKG_MAGO_UTL.sql;

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT

@./Views/V_ELEMENTI_DGF.sql;
@./Views/V_MANUTENZIONE_GENERATORI.sql;

PROMPT _______________________________________________________________________________________
PROMPT PackageBody 
PROMPT

@./PackageBodies/PKG_GERARCHIA_DGF.sql;
@./PackageBodies/PKG_MAGO_DGF.sql;
@./PackageBodies/PKG_MAGO_UTL.sql;
@./PackageBodies/PKG_GENERA_FILE_GEO.sql;
PROMPT _______________________________________________________________________________________
PROMPT Triggers
PROMPT

@./Triggers/AFTD_TMP_MANUTENZIONE.sql;
@./Triggers/AFTIUR_TMP_MANUTENZIONE.sql;
@./Triggers/AFTIU_TMP_MANUTENZIONE.sql;

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT

@./InitTables/RUN_STEP.sql;
@./InitTables/CFG_APPLICATION_PARAMETER.sql;
@./InitTables/APPLICATION_RUN.sql;

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE 1.9.b rev 0
PROMPT

SPOOL OFF

