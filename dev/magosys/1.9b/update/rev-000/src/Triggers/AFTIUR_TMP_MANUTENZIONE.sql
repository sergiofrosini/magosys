   CREATE OR REPLACE TRIGGER AFTIUR_TMP_MANUTENZIONE
   AFTER INSERT OR UPDATE
   ON TMP_MANUTENZIONE
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
   DECLARE
   PRAGMA AUTONOMOUS_TRANSACTION;
   flg_ecs NUMBER(1);
   v_new_startdate DATE;
   v_new_enddate DATE;
   v_id_man NUMBER;
   v_first NUMBER := 0;
   v_skip NUMBER(1);
   tot NUMBER;
   BEGIN
      SELECT CASE WHEN ger_ecp = 1 THEN 0 ELSE ger_ecs END flg_ecs, :new.id_manutenzione
        INTO flg_ecs, v_id_man
        FROM tipi_elemento
       WHERE cod_tipo_elemento = :new.tipo_elemento;

      IF flg_ecs = 0 THEN
         IF INSERTING THEN
            INSERT INTO tmp_manutenzione
                   ( id_manutenzione, cod_elemento, tipo_elemento, data_inizio, data_fine, percentuale)
            SELECT v_id_man, ger.cod_elemento_figlio, el.cod_tipo_elemento, :new.data_inizio, :new.data_fine, :new.percentuale
              FROM (
                    SELECT cod_elemento_padre,cod_elemento_figlio
                      FROM (
                            SELECT cod_elemento_padre, cod_elemento_figlio 
                              FROM rel_elementi_amm
                             WHERE NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) >= data_attivazione AND NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) < data_disattivazione 
                             UNION ALL 
                            SELECT cod_elemento_padre, cod_elemento_figlio 
                              FROM rel_elementi_ecs_sn 
                             WHERE NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) >= data_attivazione AND NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) < data_disattivazione 
                               AND flag_geo = 0
                            ) 
                      WHERE cod_elemento_padre = :NEW.cod_elemento 
                    ) ger
                    , elementi el
                    ,tipi_elemento tp
             WHERE ger.cod_elemento_figlio = el.cod_elemento
               AND el.cod_tipo_elemento = tp.cod_tipo_elemento;
             COMMIT;
         ELSE
            UPDATE TMP_MANUTENZIONE 
               SET data_fine = :NEW.data_fine
                  ,percentuale = :NEW.percentuale
             WHERE data_fine = :OLD.data_fine
               AND percentuale = :OLD.percentuale
               AND data_inizio = :NEW.data_inizio
               AND id_manutenzione = :NEW.id_manutenzione
               AND cod_elemento IN 
                   (
                    SELECT cod_elemento_figlio
                      FROM (
                            SELECT cod_elemento_padre, cod_elemento_figlio 
                              FROM rel_elementi_amm
                             WHERE NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) >= data_attivazione AND NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) < data_disattivazione 
                             UNION ALL 
                            SELECT cod_elemento_padre, cod_elemento_figlio 
                              FROM rel_elementi_ecs_sn 
                             WHERE NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) >= data_attivazione AND NVL(:NEW.data_fine,:NEW.data_inizio - 1/64000) < data_disattivazione 
                            ) 
                      WHERE cod_elemento_padre = :NEW.cod_elemento 
                    );
            COMMIT;
         END IF;
      END IF;              
         IF INSERTING THEN
            SELECT COUNT(*)
              INTO tot
              FROM v_manutenzione_generatori
             WHERE cod_elemento = :NEW.cod_elemento;
            IF tot > 0 THEN
               INSERT INTO MANUTENZIONE 
                           (id_manutenzione
                            ,cod_elemento
                            ,tipo_elemento
                            ,data_inizio
                            ,data_fine
                            ,percentuale
                            ,elaborato) 
                    SELECT :NEW.id_manutenzione id_manutenzione
                          ,cod_elemento
                          ,tipo_elemento
                          ,data_fine
                          ,data_inizio_next
                          ,:NEW.percentuale percentuale
                          ,0 
                      FROM 
                          ( 
                           SELECT NVL(LEAD(data_inizio) OVER ( PARTITION BY cod_elemento ORDER BY data_inizio ),:new.data_fine) data_inizio_next
                                  ,cod_elemento
                                  ,tipo_elemento
                                  ,percentuale
                                  ,data_fine                                
                             FROM v_manutenzione_generatori man 
                          )
                     WHERE :NEW.data_fine > data_fine 
                       AND data_inizio_next != data_fine 
                       AND cod_elemento = :NEW.cod_elemento;
            ELSE
               INSERT INTO MANUTENZIONE 
                      (id_manutenzione
                       ,cod_elemento
                       ,tipo_elemento
                       ,data_inizio
                       ,data_fine
                       ,percentuale
                       ,elaborato)
               VALUES (:NEW.id_manutenzione
                       ,:NEW.cod_elemento
                       ,:NEW.tipo_elemento
                       ,:NEW.data_inizio
                       ,:NEW.data_fine
                       ,:NEW.percentuale
                       ,0
                      );
               COMMIT;
            END IF;
            COMMIT;
         ELSIF :new.data_inizio != :old.data_inizio THEN
              raise_application_error(-20101, 'Data_inizio is not updatable');
         ELSE
            IF NVL(:old.data_fine,to_date('01013000','ddmmyyyy')) < NVL(:new.data_fine,to_date('01013000','ddmmyyyy')) THEN
               IF :new.percentuale != :old.percentuale THEN
                  UPDATE MANUTENZIONE 
                     SET percentuale = :new.percentuale
                        ,percentuale_old = :old.percentuale
                        ,elaborato = 0
                   WHERE cod_elemento = :new.cod_elemento
                     AND id_manutenzione = :new.id_manutenzione;
                  COMMIT;
               END IF;
            INSERT INTO MANUTENZIONE 
                        (id_manutenzione, cod_elemento, tipo_elemento, data_inizio, data_fine, percentuale,elaborato)
            SELECT :new.id_manutenzione
                  ,cod_elemento
                  ,tipo_elemento
                  ,data_fine
                  ,data_inizio_next
                  ,:NEW.percentuale
                  ,0 
              FROM 
                  (
                   SELECT NVL(LEAD(data_inizio) OVER ( PARTITION BY cod_elemento ORDER BY data_inizio ),:NEW.data_fine) data_inizio_next
                         ,cod_elemento
                         ,tipo_elemento
                         ,data_fine
                    FROM V_MANUTENZIONE_GENERATORI man 
                  ) 
             WHERE :NEW.data_fine > data_fine
               AND data_inizio_next != data_fine
               AND cod_elemento = :NEW.cod_elemento;
            COMMIT;
            ELSIF NVL(:old.data_fine,to_date('01013000','ddmmyyyy')) > NVL(:new.data_fine,to_date('01013000','ddmmyyyy')) THEN
               
               MERGE INTO manutenzione t USING 
                     (
                      SELECT cod_elemento
                            ,id_manutenzione
                            ,data_fine_check
                            ,flg_deleted
                            ,data_inizio
                            ,percentuale_new
                            ,percentuale_old
                            ,CASE WHEN flg_deleted = 0 AND data_inizio = data_fine_check AND data_fine != data_fine_new THEN data_fine_new ELSE data_fine END data_fine_new
                            ,CASE WHEN flg_deleted = 0 AND data_inizio = data_fine_check AND data_fine != data_fine_new THEN data_fine END data_fine_old
                        FROM (
                              SELECT cod_elemento
                                    ,id_manutenzione
                                    ,percentuale_new
                                    ,data_fine_new
                                    ,CASE WHEN data_inizio >= NVL(data_fine_new,to_date('01013000','ddmmyyyy')) THEN 1 ELSE 0 END flg_deleted
                                    ,data_inizio
                                    ,data_fine 
                                    ,NVL(data_fine_check,data_inizio) data_fine_check
                                    ,percentuale_old
                                FROM (
                                      SELECT cod_elemento
                                            ,id_manutenzione
                                            ,data_inizio,data_fine
                                            ,:NEW.data_fine data_fine_new
                                            ,CASE WHEN :NEW.percentuale != percentuale THEN percentuale END percentuale_old
                                            ,:NEW.percentuale percentuale_new
                                            ,MAX(CASE WHEN NVL(data_fine,to_date('01013000','ddmmyyyy')) < NVL(:new.data_fine,to_date('01013000','ddmmyyyy')) THEN data_fine END) OVER (PARTITION BY cod_elemento) data_fine_check 
                                        FROM manutenzione
                                      ) 
                               WHERE id_manutenzione = :NEW.id_manutenzione
                                 AND cod_elemento = :NEW.cod_elemento
                             ) 
                       WHERE (data_inizio = data_fine_check OR flg_deleted = 1 OR percentuale_old IS NOT NULL)
                     ) s
                  ON (s.cod_elemento = t.cod_elemento AND t.id_manutenzione = s.id_manutenzione AND t.data_inizio = s.data_inizio )
                WHEN MATCHED THEN
              UPDATE SET
                     t.flg_deleted = s.flg_deleted
                    ,t.data_fine = s.data_fine_new
                    ,t.data_fine_old = s.data_fine_old
                    ,t.percentuale = s.percentuale_new
                    ,t.percentuale_old = s.percentuale_old
                    ,t.elaborato = 0
                WHEN NOT MATCHED THEN
              INSERT
                 (cod_elemento)
              VALUES
                 (NULL);
                 
             COMMIT;
             ELSE
               IF :new.percentuale != :old.percentuale THEN
                  UPDATE MANUTENZIONE 
                     SET percentuale = :new.percentuale
                        ,percentuale_old = :old.percentuale
                        ,elaborato = 0
                   WHERE cod_elemento = :new.cod_elemento
                     AND id_manutenzione = :new.id_manutenzione;
                  COMMIT;
               END IF;
            END IF;
        END IF;
     --END IF;
END AFTIUR_TMP_MANUTENZIONE;
/
