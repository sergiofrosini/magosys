Prompt Insert Table CFG_APPLICATION_PARAMETER;

INSERT INTO cfg_application_parameter(parameter_name,par_value_char) VALUES ('MIS.DUPL','SUBSTITUTE');
INSERT INTO cfg_application_parameter(parameter_name,par_value_char) VALUES ('MIS.NOMIS','SKIP');
INSERT INTO cfg_application_parameter(parameter_name,par_value_char) VALUES ('MIS.NOELEM','SKIP');
INSERT INTO cfg_application_parameter(parameter_name,par_value_date) VALUES ('RUN.DATA',NULL);
INSERT INTO cfg_application_parameter(parameter_name,par_value_num) VALUES ('RUN.MIS_INTERVAL',10);
INSERT INTO cfg_application_parameter(parameter_name,par_value_num) VALUES ('RUN.EXEC_MODE',1);
COMMIT;

