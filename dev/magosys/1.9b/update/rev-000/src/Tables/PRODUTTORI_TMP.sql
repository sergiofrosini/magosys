Prompt Table produttori_tmp;

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE PRODUTTORI_TMP';
    DBMS_OUTPUT.PUT_LINE('Tabella PRODUTTORI_TMP droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE produttori_tmp
(
  cod_gruppo        VARCHAR2(15),
  descr_gruppo      VARCHAR2(30),
  cod_contatore_fis VARCHAR2(14),
  cg_produttore     VARCHAR2(14),
  descr_produttore  VARCHAR2(60),
  cod_contatore_sc  VARCHAR2(14),
  cg_generatore     VARCHAR2(14),
  comune            VARCHAR2(50),
  cod_comune        VARCHAR2(6),
  tipo_coord_p      VARCHAR2(5),
  coord_nord_p      VARCHAR2(30),
  coord_est_p       VARCHAR2(30),
  coord_up_p        NUMBER,
  tipo_coord_a      VARCHAR2(5),
  coord_nord_a      VARCHAR2(30),
  coord_est_a       VARCHAR2(30),
  coord_up_a        NUMBER,
  h_anem            NUMBER,
  tipo_rete         VARCHAR2(2),
  fonte             VARCHAR2(1)
);

ALTER TABLE produttori_tmp
       ADD  ( CONSTRAINT produttori_tmp_PK PRIMARY KEY (
              cg_generatore) ) ;
 