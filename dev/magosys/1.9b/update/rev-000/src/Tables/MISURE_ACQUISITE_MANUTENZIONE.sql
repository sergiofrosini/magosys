CREATE TABLE misure_acquisite_manutenzione
(
  cod_trattamento_elem  NUMBER,
  data                  DATE,
  valore                NUMBER(20,4), 
  CONSTRAINT misure_acquisite_man_pk
 PRIMARY KEY
 (cod_trattamento_elem, data)
)
TABLESPACE &TBS&DAT
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

