Prompt Table ELEM_DEF_SOLARE_TMP;

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE ELEM_DEF_SOLARE_TMP';
    DBMS_OUTPUT.PUT_LINE('Tabella ELEM_DEF_SOLARE_TMP droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/


CREATE TABLE ELEM_DEF_SOLARE_TMP
(
  cg_generatore  VARCHAR2(14)
  ,potenza_installata NUMBER
  ,id_tecnologia  VARCHAR2(1)
  ,flag_tracking  VARCHAR2(2)
  ,orientazione   NUMBER
  ,inclinazione   NUMBER
  ,superficie     NUMBER
);

ALTER TABLE elem_def_solare_tmp
       ADD  ( CONSTRAINT elem_def_solare_tmp_PK PRIMARY KEY (
              cg_generatore) ) ;


 