CREATE TABLE CFG_APPLICATION_PARAMETER
(
parameter_name VARCHAR2(50)
,par_value_char VARCHAR2(50)
,par_value_num  NUMBER
,par_value_date DATE
,CONSTRAINT CFG_APPL_PARAM_IDX_PK
 PRIMARY KEY
 (parameter_name)
);
