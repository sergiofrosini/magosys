Prompt Table elementi_gdf_eolico
 
Prompt Table ELEMENTI_GDF_EOLICO;
--
-- ELEMENTI_GDF_EOLICO (Table) 
--
CREATE TABLE elementi_gdf_eolico
(
cod_elemento NUMBER NOT NULL
,marca_turbina VARCHAR2(50)
,modello_turbina VARCHAR2(50)
,vel_cutin NUMBER(5,2)
,vel_cutoff NUMBER(5,2)
,vel_max NUMBER(5,2)
,data_attivazione DATE NOT NULL
,data_disattivazione DATE NOT NULL
,vel_recutin NUMBER(5,2)
,delta_cutoff        NUMBER(4)
,delta_recutin       NUMBER(4)
,wind_sect_manager   NUMBER(1)
,start_wind_sect     NUMBER(5,2)
,stop_wind_sect      NUMBER(5,2)
,vel_cutoff_shdown   NUMBER(5,2)
,vel_recutin_shdown  NUMBER(5,2)
,h_mozzo             NUMBER(6,2)
)
TABLESPACE &TBS&DAT
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE elementi_gdf_eolico IS 'Dettaglio di attributi di un Impianto Eolico';

COMMENT ON COLUMN elementi_gdf_eolico.cod_elemento IS 'Codice Elemento interno';

COMMENT ON COLUMN elementi_gdf_eolico.marca_turbina IS 'Marca della turbina';

COMMENT ON COLUMN elementi_gdf_eolico.modello_turbina IS 'Modello della turbina';

COMMENT ON COLUMN elementi_gdf_eolico.vel_cutin IS 'Velocita'' di cutin delle pale eoliche';

COMMENT ON COLUMN elementi_gdf_eolico.vel_cutoff IS 'Velocita'' di cutoff delle pale eoliche';

COMMENT ON COLUMN elementi_gdf_eolico.vel_max IS 'Velocita'' massima delle pale eoliche';

--COMMENT ON COLUMN elementi_gdf_eolico.flag_tracking IS 'Flag che segnala se viene registrata la velocita'' del vento';

COMMENT ON COLUMN elementi_gdf_eolico.data_attivazione IS 'Data inizio validita''';

COMMENT ON COLUMN elementi_gdf_eolico.data_disattivazione IS 'Data fine validita''';

Prompt Index ELEMENTI_GDF_EOLICO_PK;
--
-- ELEMENTI_GDF_EOLICO_PK  (Index) 
--
CREATE UNIQUE INDEX elementi_gdf_eolico_PK ON elementi_gdf_eolico
(cod_elemento, data_attivazione)
LOGGING
TABLESPACE &TBS&IDX
NOPARALLEL;

-- 
-- Primary Key Constraints for Table ELEMENTI_GDF_EOLICO 
-- 
Prompt Primary Key Constraints on Table ELEMENTI_GDF_EOLICO;
ALTER TABLE elementi_gdf_eolico ADD (
  CONSTRAINT elementi_gdf_eolico_PK
 PRIMARY KEY
 (cod_elemento, data_attivazione)
    USING INDEX 
    TABLESPACE &TBS&IDX);

-- 
-- Foreign Key Constraints for Table ELEMENTI_GDF_EOLICO 
-- 
Prompt Foreign Key Constraints on Table ELEMENTI_GDF_EOLICO;
ALTER TABLE elementi_gdf_eolico ADD (
  CONSTRAINT ELE_ELEEOL 
 FOREIGN KEY (cod_elemento) 
 REFERENCES ELEMENTI (cod_elemento));
 
