Prompt Table MISURE_TMP_EXT;

CREATE TABLE misure_tmp_ext
(
  cg_impianto      VARCHAR2(14)
  ,data            VARCHAR2(10)
  ,ora             VARCHAR2(10)
  ,valore          VARCHAR2(30)
  ,velocita_vento  VARCHAR2(30)
  ,direzione_vento VARCHAR2(30)
  ,irraggiamento   VARCHAR2(30)
  ,temperatura     VARCHAR2(10)
)
ORGANIZATION EXTERNAL
   (TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EXTDATA_DIR
    ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
        BADFILE 'misure.bad'
        SKIP 1
        FIELDS TERMINATED BY ";" OPTIONALLY ENCLOSED BY '"' 
        MISSING FIELD VALUES ARE NULL
        )
      LOCATION ('misure.csv')
   );
 