Prompt Table ELEM_DEF_EOLICO_TMP_EXT;

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE ELEM_DEF_EOLICO_TMP_EXT';
    DBMS_OUTPUT.PUT_LINE('Tabella ELEM_DEF_EOLICO_TMP_EXT droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE elem_def_eolico_tmp_ext
(
cg_generatore VARCHAR2(14)
,potenza_installata VARCHAR2(10)
,marca_turbina VARCHAR2(50)
,modello_turbina VARCHAR2(50)
,altezza_mozzo VARCHAR2(10)
,vel_cutin VARCHAR2(10)
,vel_max VARCHAR2(10)
,vel_cutoff VARCHAR2(10)
,vel_recutin VARCHAR2(10)
,delta_cutoff VARCHAR2(4)
,delta_recutin VARCHAR2(4)
,wind_sect_manager VARCHAR2(2)
,start_wind_sect VARCHAR2(10)
,stop_wind_sect  VARCHAR2(10)
,vel_cutoff_shdown VARCHAR2(10)
,vel_recutin_shdown  VARCHAR2(10)
)
ORGANIZATION EXTERNAL
   (TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EXTDATA_DIR
    ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
        BADFILE 'eolico.bad'
        SKIP 12
        FIELDS  TERMINATED BY ";" OPTIONALLY ENCLOSED BY '"' LRTRIM 
        MISSING FIELD VALUES ARE NULL
        (cg_generatore,potenza_installata,marca_turbina,modello_turbina,altezza_mozzo,vel_cutin,vel_max,vel_cutoff,vel_recutin,delta_cutoff,delta_recutin,wind_sect_manager,start_wind_sect,stop_wind_sect,vel_cutoff_shdown,vel_recutin_shdown)
       )
      LOCATION ('eolico.csv')
   );
