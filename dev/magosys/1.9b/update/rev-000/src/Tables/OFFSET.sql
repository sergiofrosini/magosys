Prompt Table OFFSET;
--
-- TIPO_FONTI  (Table) 
--
--  Dependencies: 
--   OFFSET (Table)
--
CREATE TABLE OFFSET
(
  OFFSET_START  NUMBER,
  OFFSET_STOP   NUMBER,
  NUM           NUMBER
)
TABLESPACE &TBS&DAT
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE OFFSET IS 'Tabella temporanea per calcolo range temporale misure';

COMMENT ON COLUMN OFFSET.OFFSET_START IS 'Range di minuti iniziale';

COMMENT ON COLUMN OFFSET.OFFSET_STOP IS 'Range di minuti finale';

COMMENT ON COLUMN OFFSET.NUM IS 'Identificativo del range temporale';

