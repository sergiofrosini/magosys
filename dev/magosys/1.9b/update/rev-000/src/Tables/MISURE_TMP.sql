Prompt Table MISURE_TMP;

CREATE TABLE misure_tmp
(
  cg_impianto      VARCHAR2(14)
  ,data            DATE
  ,ora             VARCHAR2(10)
  ,valore          NUMBER
  ,velocita_vento  NUMBER(5,2)
  ,direzione_vento NUMBER
  ,irraggiamento   NUMBER
  ,temperatura     NUMBER(5,2)
);

 