
BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE PRODUTTORI_TMP_EXT';
    DBMS_OUTPUT.PUT_LINE('Tabella PRODUTTORI_TMP_EXT droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/


CREATE TABLE PRODUTTORI_TMP_EXT
(
  cod_gruppo        VARCHAR2(15),
  descr_gruppo      VARCHAR2(30),
  cod_contatore_fis VARCHAR2(14),
  descr_produttore  VARCHAR2(60),
  cod_contatore_sc  VARCHAR2(14),
  cg_generatore     VARCHAR2(14),
  comune            VARCHAR2(50),
  cod_comune        VARCHAR2(6),
  tipo_coord_p      VARCHAR2(5),
  coord_nord_p      VARCHAR2(30),
  coord_est_p       VARCHAR2(30),
  coord_up_p        VARCHAR2(10),
  tipo_coord_a      VARCHAR2(5),
  coord_nord_a      VARCHAR2(30),
  coord_est_a       VARCHAR2(30),
  coord_up_a        VARCHAR2(10),
  h_anem            VARCHAR2(10),
  tipo_rete         VARCHAR2(2),
  fonte             VARCHAR2(1)
)
ORGANIZATION EXTERNAL
  (  TYPE ORACLE_LOADER
     DEFAULT DIRECTORY EXTDATA_DIR
     ACCESS PARAMETERS 
       ( RECORDS DELIMITED BY NEWLINE 
        BADFILE 'produttori.bad' 
        SKIP 14 
        FIELDS TERMINATED BY ";" OPTIONALLY ENCLOSED BY '"' 
        MISSING FIELD VALUES ARE NULL 
          )
     LOCATION (EXTDATA_DIR:'produttori.csv')
  )
REJECT LIMIT 0
NOPARALLEL
NOMONITORING;
