Prompt Table ELEM_DEF_EOLICO_TMP;

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE ELEM_DEF_EOLICO_TMP';
    DBMS_OUTPUT.PUT_LINE('Tabella ELEM_DEF_EOLICO_TMP droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE elem_def_eolico_tmp
(
  cg_generatore       VARCHAR2(14)
  ,potenza_installata  NUMBER
  ,marca_turbina       VARCHAR2(50)
  ,modello_turbina     VARCHAR2(50)
  ,h_mozzo             NUMBER(6,2)
  ,vel_cutin           NUMBER(5,2)
  ,vel_max             NUMBER(5,2)
  ,vel_cutoff          NUMBER(5,2)
  ,vel_recutin         NUMBER(5,2)
  ,delta_cutoff        NUMBER(4)
  ,delta_recutin       NUMBER(4)
  ,wind_sect_manager   VARCHAR2(2)
  ,start_wind_sect     NUMBER(5,2)
  ,stop_wind_sect      NUMBER(5,2)
  ,vel_cutoff_shdown   NUMBER(5,2)
  ,vel_recutin_shdown  NUMBER(5,2)
); 

ALTER TABLE elem_def_eolico_tmp
       ADD  ( CONSTRAINT elem_def_eolico_tmp_PK PRIMARY KEY (
              cg_generatore) ) ;
              