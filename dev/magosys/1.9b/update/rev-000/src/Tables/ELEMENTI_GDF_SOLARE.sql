Prompt Table ELEMENTI_GDF_SOLARE;
--
-- ELEMENTI_GDF_SOLARE (Table) 
--
CREATE TABLE elementi_gdf_solare
(
cod_elemento NUMBER NOT NULL
,orientazione NUMBER
,id_tecnologia VARCHAR2(1)
,flag_tracking NUMBER(1)
,inclinazione NUMBER
,superficie NUMBER
,data_attivazione DATE NOT NULL
,data_disattivazione DATE NOT NULL
)
TABLESPACE &TBS&DAT
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
MONITORING;

COMMENT ON TABLE elementi_gdf_solare IS 'Dettaglio di attributi di un Impianto Solare';

COMMENT ON COLUMN elementi_gdf_solare.cod_elemento IS 'Codice Elemento interno';

COMMENT ON COLUMN elementi_gdf_solare.orientazione IS 'Orientazione del pannello in gradi';

COMMENT ON COLUMN elementi_gdf_solare.id_tecnologia IS 'Link verso la tipologia di pannelli solari';

COMMENT ON COLUMN elementi_gdf_solare.flag_tracking IS 'Flag che segnala se viene registrata l''irradiamento solare';

COMMENT ON COLUMN elementi_gdf_solare.inclinazione IS 'Inclinazione dei pannelli';

COMMENT ON COLUMN elementi_gdf_solare.superficie IS 'Superficie dei pannelli';

COMMENT ON COLUMN elementi_gdf_solare.data_attivazione IS 'Data inizio validita''';

COMMENT ON COLUMN elementi_gdf_solare.data_disattivazione IS 'Data fine validita''';

Prompt Index ELEMENTI_GDF_SOLARE_PK;
--
-- ELEMENTI_GDF_SOLARE_PK  (Index) 
--
CREATE UNIQUE INDEX elementi_gdf_solare_PK ON elementi_gdf_solare
(cod_elemento, data_attivazione)
LOGGING
TABLESPACE &TBS&IDX
NOPARALLEL;

-- 
-- Primary Key Constraints for Table ELEMENTI_GDF_SOLARE 
-- 
Prompt Primary Key Constraints on Table ELEMENTI_GDF_SOLARE;
ALTER TABLE elementi_gdf_solare ADD (
  CONSTRAINT elementi_gdf_solare_PK
 PRIMARY KEY
 (cod_elemento, data_attivazione)
    USING INDEX 
    TABLESPACE &TBS&IDX);

-- 
-- Foreign Key Constraints for Table ELEMENTI_GDF_SOLARE 
-- 
Prompt Foreign Key Constraints on Table ELEMENTI_GDF_SOLARE;
ALTER TABLE elementi_gdf_solare ADD (
  CONSTRAINT ELE_ELESOL 
 FOREIGN KEY (cod_elemento) 
 REFERENCES ELEMENTI (cod_elemento));
 
-- 
-- Foreign Key Constraints for Table ELEMENTI_GDF_SOLARE 
-- 
Prompt Foreign Key Constraints on Table ELEMENTI_GDF_SOLARE;
ALTER TABLE elementi_gdf_solare ADD (
  CONSTRAINT TIPOSOL_ELESOL 
 FOREIGN KEY (id_tecnologia) 
 REFERENCES TIPO_TECNOLOGIA_SOLARE (id_tecnologia));
 
 
