Prompt Table ELEM_DEF_SOLARE_TMP_EXT;

BEGIN
    EXECUTE IMMEDIATE 'DROP TABLE ELEM_DEF_SOLARE_TMP_EXT';
    DBMS_OUTPUT.PUT_LINE('Tabella ELEM_DEF_SOLARE_TMP_EXT droppata');
EXCEPTION 
    WHEN OTHERS THEN
        IF SQLCODE = -00942 THEN NULL;
                            ELSE RAISE;
        END IF;
END;
/

CREATE TABLE elem_def_solare_tmp_ext
(
cg_generatore VARCHAR2(14)
,potenza_installata VARCHAR2(10)
,id_tecnologia VARCHAR2(1)
,flag_tracking VARCHAR2(2)
,orientazione VARCHAR2(10)
,inclinazione VARCHAR2(10)
,superficie VARCHAR2(10)
)
ORGANIZATION EXTERNAL
   (TYPE ORACLE_LOADER
    DEFAULT DIRECTORY EXTDATA_DIR
    ACCESS PARAMETERS
       (RECORDS DELIMITED BY NEWLINE
        BADFILE 'solare.bad'
        SKIP 12
        FIELDS TERMINATED BY ";" OPTIONALLY ENCLOSED BY '"' LRTRIM
        MISSING FIELD VALUES ARE NULL 
        (cg_generatore,potenza_installata,id_tecnologia,flag_tracking,orientazione,inclinazione,superficie)
       )
      LOCATION ('solare.csv')
   );
 