Prompt Package PKG_MAGO_DGF;
--
-- PKG_MAGO_DGF  (Package) 
--
--  Dependencies: 
--   DUAL (Synonym)
--   STANDARD (Package)
--   STANDARD (Package)
--   MISURE_ACQUISITE (Table)
--   PKG_UTLGLB (Synonym)
--   PKG_UTLGLB (Synonym)
--   PROVINCE (Table)
--   COMUNI (Table)
--   PRODUTTORI_TMP_EXT (Table)
--   ELEM_DEF_SOLARE_TMP_EXT (Table)
--   MISURE_TMP_EXT (Table)
--   PRODUTTORI_TMP (Table)
--   ELEM_DEF_EOLICO_TMP (Table)
--   ELEM_DEF_SOLARE_TMP (Table)
--   MISURE_TMP (Table)
--   RAGGRUPPAMENTO_FONTI (Table)
--   TIPI_ELEMENTO (Table)
--   TIPI_MISURA (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   ELEMENTI_GDF_EOLICO (Table)
--   ELEMENTI_GDF_SOLARE (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   OFFSET (Table)
--   REL_ELEMENTO_TIPMIS (Table)
--   ELEM_DEF_EOLICO_TMP_EXT (Table)
--   PKG_GERARCHIA_DGF (Package)
--   PKG_MAGO (Package)
--   PKG_MISURE (Package)
--   PKG_SCHEDULER (Package)
--   V_ELEMENTI_DGF (View)
--   DUAL ()
--   PKG_UTLGLB ()
--   PKG_UTLGLB ()
--
CREATE OR REPLACE PACKAGE pkg_mago_dgf AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_Dgf
   PURPOSE:    Servizi per la gestione del caricamento dai Eolico e Solare
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      20/04/2012  Paolo Campi      Created this package.
   NOTES:
*********************************************************************************************************** */
   PROCEDURE initMagoDGF;
   FUNCTION  checkMis( checktype IN VARCHAR2 ) RETURN NUMBER;
   PROCEDURE load_produttori;
   PROCEDURE load_def_eolico;
   PROCEDURE load_def_solare;
   PROCEDURE load_misure;
   PROCEDURE insert_element;
   PROCEDURE insert_rel_misure;
   PROCEDURE insert_def (pData IN DATE);
   PROCEDURE insert_def_eolico (pData IN DATE);
   PROCEDURE insert_def_solare (pData IN DATE);
   PROCEDURE insert_trattamento_elementi (pData IN DATE);
   PROCEDURE insert_misure (pMin IN NUMBER);
   PROCEDURE insert_manutenzione(pEle IN T_ELEMAN_OBJ); 
   PROCEDURE elabora_manutenzione;
END;
/

SHOW ERRORS;


