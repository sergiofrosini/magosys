Prompt Package PKG_MAGO_UTL;
CREATE OR REPLACE PACKAGE pkg_mago_utl AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_utl
   PURPOSE:    Servizi di Utility
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      17/09/2012  Paolo Campi      Created this package.
   NOTES:
*********************************************************************************************************** */
 gcOrganizzazELE    CONSTANT NUMBER(1)    := 1;        -- organizzazione elettrica
 gcOrganizzazGEO    CONSTANT NUMBER(1)    := 2;        -- organizzazione geografica (Istat)
 gcOrganizzazAMM    CONSTANT NUMBER(1)    := 3;        -- organizzazione amministrativa
 gcLogDir           CONSTANT VARCHAR2(30) := PKG_UtlGlb.gv_DirLog;
 gcLogFile          CONSTANT VARCHAR2(30) := PKG_UtlGlb.GetLogFileName('Mag_PkgLogDef__');
 -- ----------------------------------------------------------------------------------------------------------
-- Tipi Produttore
-- ----------------------------------------------------------------------------------------------------------
 gcProduttorePuro       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'A';   -- Produttore puro
 gcProduttoreCliente    CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'B';   -- Produttore/Cliente
 gcProduttoreNonDeterm  CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'X';   -- Produttore non determinato
-- ----------------------------------------------------------------------------------------------------------
-- Tipi Misura
-- ----------------------------------------------------------------------------------------------------------
 gcPotenzaAttGenerata   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAG'; -- Potenza Attiva Generata
 gcVelVento             CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'VVE';  -- Velocita' Vento
 gcDirVento             CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'DVE';  -- Direzione Vento
 gcIrradiamentoSol      CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'IRR'; -- Irradiamento Solare
 gcTemperatura          CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'TMP'; -- Temperatura
 -- ---------------------------------------------------------------------------------------------------------
  gcSeparatoreElem   CONSTANT VARCHAR2(1) := '#' ; -- '#' per separare i valori nel codice gestionale
  gcSostitutSeparatoreElem      CONSTANT VARCHAR2(1) := '_' ; -- '#' per separare i valori nel codice gestionale
 -- ---------------------------------------------------------------------------------------------------------
   FUNCTION getproduttorepuro RETURN VARCHAR2;
   FUNCTION getproduttorecliente RETURN VARCHAR2;
   FUNCTION getproduttorenondeterm RETURN VARCHAR2;
   FUNCTION getSeparatore RETURN VARCHAR2;
   FUNCTION  leggiparam(pparam_name IN VARCHAR2) RETURN VARCHAR2;
   FUNCTION  spacchettaparam(pproc_name IN VARCHAR2,param_num IN NUMBER, last_param OUT NUMBER) RETURN VARCHAR2;
   FUNCTION  convertiparam(pproc_name IN VARCHAR2,param_num IN NUMBER, pparvalue IN VARCHAR2) RETURN VARCHAR2;
   PROCEDURE run_application_group(gruppo IN VARCHAR2);
   PROCEDURE startRun;
   FUNCTION checkRun ( pproc_name IN VARCHAR2 ) RETURN NUMBER ;
   PROCEDURE setRun ( pproc_name IN VARCHAR2, flag_run IN NUMBER, flag_err IN NUMBER) ;
   PROCEDURE insLog( pproc_name IN VARCHAR2, pmex IN VARCHAR2, errnum IN NUMBER);
END;
/

SHOW ERRORS;


