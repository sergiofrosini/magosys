CREATE OR REPLACE VIEW V_ELEMENTI_DGF
AS 
SELECT 
       NUM, COD_ELEMENTO, COD_TIPO_ELEMENTO, DESCRIZIONE, IMPIANTO
      ,FONTE, COD_TIPO_CLIENTE, POTENZA_INSTALLATA, H_ANEM, FATTORE
      ,COD_COMUNE, COD_GEO, COORDINATA_X, COORDINATA_Y, ALTITUDINE
      ,COD_GEO_A, COORDINATA_X_A, COORDINATA_Y_A, ALTITUDINE_A
 FROM
(
    --GENERATORE
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num
          ,el.cod_elemento
          ,el.cod_tipo_elemento
          ,prod.descr_gruppo descrizione
          ,NVL(prod.cg_produttore,prod.cod_gruppo) impianto
          ,prod.fonte
          ,'A' cod_tipo_cliente
          ,prod.potenza_installata potenza_installata
          ,prod.h_anem
          ,1 fattore
          ,cod_comune
          ,pgeo.cod_geo
          ,round(pkg_localizza_geo.coord_conversion(prod.coord_nord_p, prod.tipo_coord_p),7) coordinata_x
          ,round(pkg_localizza_geo.coord_conversion(prod.coord_est_p, prod.tipo_coord_p),7) coordinata_y
          ,prod.coord_up_p altitudine
          ,pgeo_a.cod_geo cod_geo_a
          ,coord_nord_a_calc coordinata_x_a
          ,coord_est_a_calc coordinata_y_a
          ,coord_up_a_calc altitudine_a
      FROM 
           ( SELECT prod.*
                    ,gen.potenza_installata
                    ,round(pkg_localizza_geo.coord_conversion(prod.coord_nord_p, prod.tipo_coord_p),7) coord_nord_p_conv
                    ,round(pkg_localizza_geo.coord_conversion(prod.coord_est_p, prod.tipo_coord_p),7) coord_est_p_conv
                    ,round(NVL(pkg_localizza_geo.coord_conversion(prod.coord_nord_a, prod.tipo_coord_a),AVG(pkg_localizza_geo.coord_conversion(prod.coord_nord_p,prod.tipo_coord_p)) OVER ( PARTITION BY NVL(prod.cg_produttore,prod.cod_gruppo))),7) coord_nord_a_calc
                    ,round(NVL(pkg_localizza_geo.coord_conversion(prod.coord_est_a, prod.tipo_coord_a),AVG(pkg_localizza_geo.coord_conversion(prod.coord_est_p,prod.tipo_coord_p)) OVER ( PARTITION BY NVL(prod.cg_produttore,prod.cod_gruppo))),7) coord_est_a_calc
                    ,NVL(prod.coord_up_a,ROUND(AVG(prod.coord_up_p) OVER ( PARTITION BY NVL(prod.cg_produttore,prod.cod_gruppo)))) coord_up_a_calc
               FROM produttori_tmp prod
                   ,(SELECT potenza_installata
                           ,cg_generatore
                       FROM ELEM_DEF_EOLICO_TMP
                      UNION ALL
                     SELECT potenza_installata
                           ,cg_generatore
                       FROM ELEM_DEF_SOLARE_TMP
                     ) gen
               WHERE gen.cg_generatore = prod.cg_generatore
           ) prod
          ,elementi el
          ,anagrafica_punti pgeo
          ,anagrafica_punti pgeo_a
     WHERE el.cod_gest_elemento = prod.cg_generatore
       AND pgeo.coordinata_x (+) = coord_nord_p_conv
       AND pgeo.coordinata_y (+) = coord_est_p_conv
       AND pgeo.coordinata_h (+) = prod.coord_up_p
       AND pgeo.tipo_coord (+) = 'P'
       AND pgeo_a.coordinata_x (+) = coord_nord_a_calc
       AND pgeo_a.coordinata_y (+) = coord_est_a_calc
       AND pgeo_a.coordinata_h (+) = coord_up_a_calc
       AND pgeo_a.tipo_coord (+) = 'A'
     UNION ALL --PRODUTTORE
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num
          ,el.cod_elemento
          ,el.cod_tipo_elemento
          ,prod.descr_produttore
          ,NVL(prod.cg_produttore,prod.cod_gruppo) impianto
          ,prod.fonte
          ,'A' cod_tipo_produttore
          ,NULL h_anem
          ,NULL potenza_installata
          ,NULL fattore
          ,NULL cod_comune
          ,NULL cod_geo
          ,NULL coordinata_x
          ,NULL coordinata_y
          ,NULL altitudine
          ,NULL cod_geo_a
          ,NULL coordinata_x_a
          ,NULL coordinata_y_a
          ,NULL altitudine_a
      FROM 
           produttori_tmp prod
          ,elementi el
     WHERE prod.cg_produttore = el.cod_gest_elemento
     UNION ALL --PRODUTTORE @ COMUNE
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num
          ,el.cod_elemento
          ,el.cod_tipo_elemento
          ,prod.descr_produttore
          ,NVL(prod.cg_produttore,prod.cod_gruppo) impianto
          ,prod.fonte
          ,'A' cod_tipo_produttore
          ,NULL potenza_installata
          ,NULL h_anem
          ,NULL fattore
          ,NULL cod_comune
          ,NULL cod_geo
          ,NULL coordinata_x
          ,NULL coordinata_y
          ,NULL altitudine
          ,NULL cod_geo_a
          ,NULL coordinata_x_a
          ,NULL coordinata_y_a
          ,NULL altitudine_a
      FROM 
          (SELECT prod.*
                 ,count(*) over ( partition by cg_produttore) tot
                 ,count(*) over (partition by cg_produttore, cod_comune) totcom
                 ,prod.cod_comune || pkg_mago_utl.getSeparatore || prod.cg_produttore cod_gest_elemento
             FROM produttori_tmp prod
            WHERE cg_produttore IS NOT NULL
          ) prod
          ,elementi el
     WHERE prod.cod_gest_elemento = el.cod_gest_elemento
       AND prod.tot != prod.totcom
     UNION ALL --ZONA
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num
          ,el.cod_elemento
          ,el.cod_tipo_elemento
          ,prod.descr_gruppo
          ,NULL impianto
          ,NULL fonte
          ,NULL cod_tipo_produttore
          ,NULL potenza_installata
          ,NULL h_anem
          ,NULL fattore
          ,NULL cod_comune
          ,NULL cod_geo
          ,NULL coordinata_x
          ,NULL coordinata_y
          ,NULL altitudine
          ,NULL cod_geo_a
          ,NULL coordinata_x_a
          ,NULL coordinata_y_a
          ,NULL altitudine_a
      FROM 
           produttori_tmp prod
          ,elementi el
     WHERE prod.cod_gruppo = el.cod_gest_elemento
      UNION ALL --PROVINCIA
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num 
           ,el.cod_elemento 
           ,el.cod_tipo_elemento 
           ,prv.nome 
           ,NULL impianto
           ,NULL fonte 
           ,NULL cod_tipo_produttore
           ,NULL h_anem
           ,NULL potenza_installata 
           ,NULL fattore 
           ,NULL cod_comune 
           ,NULL cod_geo
           ,prv.latitudine_gps coordinata_x 
           ,prv.longitudine_gps coordinata_y 
           ,NULL altitudine 
           ,NULL cod_geo_a
           ,NULL coordinata_x_a
           ,NULL coordinata_y_a
           ,NULL altitudine_a
     FROM produttori_tmp prod 
         ,elementi el 
         ,sar_Admin.comuni com 
         ,sar_admin.province prv 
    WHERE /*(el.cod_tipo_elemento || pkg_mago_utl.getSeparatore || */ (prv.sigla) = el.cod_gest_elemento 
      AND com.cod_istat_prov = prv.cod_istat_prov 
      AND com.cod_istat_comune = prod.cod_comune
      UNION ALL --COMUNE
    SELECT ROW_NUMBER() OVER (PARTITION BY el.cod_elemento ORDER BY sysdate) num 
           ,el.cod_elemento 
           ,el.cod_tipo_elemento 
           ,com.nome 
           ,NULL impianto
           ,NULL fonte 
           ,NULL cod_tipo_produttore
           ,NULL h_anem
           ,NULL potenza_installata 
           ,NULL fattore 
           ,NULL cod_comune 
           ,NULL cod_geo
           ,com.latitudine_gps coordinata_x 
           ,com.longitudine_gps coordinata_y 
           ,NULL altitudine 
           ,NULL cod_geo_a
           ,NULL coordinata_x_a
           ,NULL coordinata_y_a
           ,NULL altitudine_a
     FROM produttori_tmp prod 
         ,elementi el 
         ,sar_Admin.comuni com 
    WHERE (prod.cod_comune) = el.cod_gest_elemento 
      AND com.cod_istat_comune = prod.cod_comune
) WHERE NUM = 1
/


