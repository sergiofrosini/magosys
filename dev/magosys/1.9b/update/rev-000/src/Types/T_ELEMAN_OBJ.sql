Prompt Type T_ELEMAN_OBJ;
--
-- T_ELEMAN_OBJ  (Type) 
--
--  Dependencies: 
--   STANDARD (Package)
--
CREATE OR REPLACE TYPE T_ELEMAN_OBJ AS OBJECT  
                      (COD_ELEMENTO     NUMBER,
                       PERCENTUALE      NUMBER,
                       DATAFROM         DATE,
                       DATATO           DATE,
                       DATATOOLD        DATE,
                       OPER             CHAR(1)
                      );
/

SHOW ERRORS;


