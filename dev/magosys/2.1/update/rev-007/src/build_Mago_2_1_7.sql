SPOOL &spool_all append

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.1.7 rev-007 SRC
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

conn mago/mago&tns_arcdb2


PROMPT _______________________________________________________________________________________
PROMPT Packags
PROMPT
@./Packages/PKG_METEO.sql

PROMPT _______________________________________________________________________________________
PROMPT Package bodies
PROMPT
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_METEO.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ELEMENTI_DGF.sql


PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.1.7 SRC
PROMPT

spool off
