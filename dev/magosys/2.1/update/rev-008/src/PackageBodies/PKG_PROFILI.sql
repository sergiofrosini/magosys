PROMPT file   PackagesBodies/PKG_PROFILI.sql

CREATE OR REPLACE PACKAGE BODY PKG_PROFILI AS

/* ***********************************************************************************************************
   NAME:       PKG_PROFILI
   PURPOSE:    Gestione dei Profili Misura

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.11.2     21/06/2016  Moretti C.       Definizione Package
   2.0.4     11/11/2016  Forno S.         Aggiunta funzione AddMisureSTLPE per il Forecast con ST-LPE
   *** Spec 2.1.6 ***
   2.1.6    11/11/2016  Forno S.         MAGO-1189 - modificato tipo  T_MISMETEO_ARRAY   - 

   NOTES:

*********************************************************************************************************** */




/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION AddProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER,
                                 pProfili          IN T_PROFILI,
                                 pDataInizio       IN DATE,
                                 pDataFine         IN DATE
                                ) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
   Insersce i profili ricevuti
-----------------------------------------------------------------------------------------------------------*/
    vCount     NUMBER := 0;
    vCodElem   ELEMENTI.COD_ELEMENTO%TYPE;

 BEGIN
    vCodElem := PKG_Elementi.GetCodElemento(pCodGestElem);
    IF vCodElem IS NULL THEN
        RETURN 0;
    END IF;

--    MERGE INTO PROFILI B
--         USING (
--                SELECT vCodElem COD_ELEMENTO, gcProfiloFeriale      TIPO,  pTipoMisura COD_TIPO_MISURA FROM DUAL
--                UNION ALL
--                SELECT vCodElem COD_ELEMENTO,  gcProfiloSemisestivo TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
--                UNION ALL
--                SELECT vCodElem COD_ELEMENTO, gcProfiloFestivo     TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
--               ) A
--               ON (    B.COD_ELEMENTO    = A.COD_ELEMENTO
--                       AND B.TIPO            = A.TIPO
--                       AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
--         WHEN NOT MATCHED THEN
--                INSERT (  COD_ELEMENTO,  COD_TIPO_MISURA,  TIPO)
--                VALUES (A.COD_ELEMENTO,A.COD_TIPO_MISURA,A.TIPO);

    MERGE INTO PROFILI B
         USING (
                SELECT vCodElem COD_ELEMENTO, gcProfiloFeriale      TIPO,  pTipoMisura COD_TIPO_MISURA FROM DUAL
               ) A
               ON (    B.COD_ELEMENTO    = A.COD_ELEMENTO
                       AND B.TIPO            = A.TIPO
                       AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
         WHEN NOT MATCHED THEN
                INSERT (  COD_ELEMENTO,  COD_TIPO_MISURA,  TIPO)
                VALUES (A.COD_ELEMENTO,A.COD_TIPO_MISURA,A.TIPO);

    MERGE INTO PROFILI B
         USING (
                SELECT vCodElem COD_ELEMENTO,  gcProfiloSemisestivo TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
               ) A
               ON (    B.COD_ELEMENTO    = A.COD_ELEMENTO
                       AND B.TIPO            = A.TIPO
                       AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
         WHEN NOT MATCHED THEN
                INSERT (  COD_ELEMENTO,  COD_TIPO_MISURA,  TIPO)
                VALUES (A.COD_ELEMENTO,A.COD_TIPO_MISURA,A.TIPO);

    MERGE INTO PROFILI B
         USING (
                SELECT vCodElem COD_ELEMENTO, gcProfiloFestivo     TIPO, pTipoMisura COD_TIPO_MISURA FROM DUAL
               ) A
               ON (    B.COD_ELEMENTO    = A.COD_ELEMENTO
                       AND B.TIPO            = A.TIPO
                       AND B.COD_TIPO_MISURA = A.COD_TIPO_MISURA)
         WHEN NOT MATCHED THEN
                INSERT (  COD_ELEMENTO,  COD_TIPO_MISURA,  TIPO)
                VALUES (A.COD_ELEMENTO,A.COD_TIPO_MISURA,A.TIPO);




    MERGE INTO PROFILI_DEF B
         USING (SELECT B.COD_PROFILO,ANNO,MESE,
                       pDataInizio DT_MISURE_INIZIO,pDataFine DT_MISURE_FINE
                  FROM (SELECT DISTINCT pAnnoRif ANNO, MESE
                          FROM TABLE (CAST(pProfili AS T_PROFILI))
                       ) A
                  LEFT OUTER JOIN PROFILI B ON (    B.COD_ELEMENTO = vCodElem
                                                AND B.COD_TIPO_MISURA = pTipoMisura)
               ) A ON (    B.COD_PROFILO = A.COD_PROFILO
                       AND B.ANNO        = A.ANNO
                       AND B.MESE        = A.MESE)
         WHEN NOT MATCHED THEN
                INSERT (  COD_PROFILO,   ANNO,  MESE,   DT_MISURE_INIZIO,   DT_MISURE_FINE)
                VALUES (A.COD_PROFILO, A.ANNO,A.MESE, A.DT_MISURE_INIZIO, A.DT_MISURE_FINE)
         WHEN MATCHED THEN
                UPDATE SET B.DT_MISURE_INIZIO = A.DT_MISURE_INIZIO,
                           B.DT_MISURE_FINE   = A.DT_MISURE_FINE;

    MERGE INTO PROFILI_VAL B
         USING (SELECT B.COD_PROFILO, pAnnoRif ANNO, A.MESE, REPLACE(A.DAYTIME,':','') DAYTIME, A.VALORE
                  FROM (SELECT MESE, TIPO, DAYTIME, MAX(VALORE) VALORE
                          FROM TABLE (CAST(pProfili AS T_PROFILI))
                         GROUP BY MESE,TIPO,DAYTIME
                       ) A
                  JOIN PROFILI B ON B.COD_ELEMENTO = vCodElem
                                AND B.COD_TIPO_MISURA = pTipoMisura
                                AND B.TIPO = A.TIPO
               ) A
           ON (    B.COD_PROFILO = A.COD_PROFILO
               AND B.ANNO        = A.ANNO
               AND B.MESE        = A.MESE
               AND B.DAYTIME     = A.DAYTIME)
         WHEN NOT MATCHED THEN
                INSERT (  COD_PROFILO,  ANNO,  MESE,  DAYTIME,  VALORE)
                VALUES (A.COD_PROFILO,A.ANNO,A.MESE,A.DAYTIME,A.VALORE)
         WHEN MATCHED THEN
                UPDATE SET B.VALORE = A.VALORE
                 WHERE B.VALORE <> A.VALORE;
    vCount := SQL%ROWCOUNT;
    COMMIT;
    RETURN vCount;
 END AddProfili;

 -- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProfili           (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                ) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il cursore con i profili richiesti.
   La chiusura del cursore e' a cura del chiamente
-----------------------------------------------------------------------------------------------------------*/
    vNum     INTEGER;
    vAnnoRif INTEGER := NVL(pAnnoRif,TO_NUMBER(TO_CHAR(SYSDATE,'YYYY')));
 BEGIN
    SELECT COUNT(*)
      INTO vNum
      FROM V_PROFILI
     WHERE COD_GEST_ELEMENTO = pCodGestElem
       AND COD_TIPO_MISURA = pTipoMisura
       AND ANNO = vAnnoRif;
    IF vNum = 0 THEN
        SELECT MAX(ANNO)
          INTO vAnnoRif
          FROM V_PROFILI
         WHERE COD_GEST_ELEMENTO = pCodGestElem
           AND COD_TIPO_MISURA = pTipoMisura
           AND ANNO < pAnnoRif;
    END IF;
    OPEN pRefCurs FOR SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO,
                             DAYTIME,VALORE,DT_MISURE_INIZIO,DT_MISURE_FINE
                        FROM V_PROFILI
                       WHERE ANNO = vAnnoRif
                         AND COD_TIPO_MISURA = pTipoMisura
                         AND COD_GEST_ELEMENTO = pCodGestElem
                         AND CASE WHEN pMeseRif
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pMeseRif = MESE
                                                        THEN 1
                                                        ELSE 0
                                            END
                             END = 1
                         AND CASE WHEN pTipoGiorno
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pTipoGiorno = TIPO
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1
                         AND CASE WHEN pDayTime
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pDayTime = DAYTIME
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1;
                       --ORDER BY COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO,DAYTIME;
 END GetProfili;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetProfili            (pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 pAnnoRif          IN INTEGER  DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL,
                                 pDayTime          IN VARCHAR2 DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce la Pipeline con i profili richiesti.
-----------------------------------------------------------------------------------------------------------*/
    vCursore    PKG_UtlGlb.t_query_cur;
    vProfilo    V_PROFILI%ROWTYPE;
 BEGIN
    GetProfili(vCursore,pCodGestElem,pTipoMisura,pAnnoRif,pMeseRif,pTipoGiorno,pDayTime);
    LOOP
        FETCH vCursore INTO vProfilo;
        EXIT WHEN vCursore%NOTFOUND;
        PIPE ROW(vProfilo);
    END LOOP;
    CLOSE vCursore;
    RETURN;
 END GetProfili;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElencoProfili     (pRefCurs         OUT PKG_UtlGlb.t_query_cur,
                                 pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                ) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il cursore con l'elenco dei profili richiesti.
   La chiusura del cursore e' a cura del chiamente
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    OPEN pRefCurs FOR SELECT COD_ELEMENTO,E.COD_GEST_ELEMENTO,P.COD_TIPO_MISURA,D.ANNO,D.MESE,P.TIPO,
                             MIN(D.DT_MISURE_INIZIO) DT_MISURE_INIZIO, MAX(D.DT_MISURE_FINE) DT_MISURE_FINE
                        FROM PROFILI P
                        JOIN PROFILI_DEF D USING(COD_PROFILO)
                        JOIN ELEMENTI E USING(COD_ELEMENTO)
                       WHERE D.ANNO = pAnnoRif
                         AND E.COD_GEST_ELEMENTO LIKE NVL(pCodGestElem,'%')
                         AND P.COD_TIPO_MISURA LIKE NVL(pTipoMisura,'%')
                         AND CASE WHEN pMeseRif
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pMeseRif = D.MESE
                                                        THEN 1
                                                        ELSE 0
                                            END
                             END = 1
                         AND CASE WHEN pTipoGiorno
                                    IS NULL THEN 1
                                            ELSE CASE WHEN pTipoGiorno = P.TIPO
                                                        THEN 1
                                                        ELSE 0
                                                 END
                             END = 1
                       GROUP BY COD_ELEMENTO,E.COD_GEST_ELEMENTO,P.COD_TIPO_MISURA,D.ANNO,D.MESE,P.TIPO;
                       --ORDER BY COD_GEST_ELEMENTO,COD_TIPO_MISURA,ANNO,MESE,TIPO;
 END GetElencoProfili;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElencoProfili      (pAnnoRif          IN INTEGER,
                                 pCodGestElem      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE DEFAULT NULL,
                                 pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                 pMeseRif          IN INTEGER  DEFAULT NULL,
                                 pTipoGiorno       IN INTEGER  DEFAULT NULL
                                ) RETURN t_TabProfili PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce la Pipeline con l'elenco dei profili richiesti.
-----------------------------------------------------------------------------------------------------------*/
    vCursore    PKG_UtlGlb.t_query_cur;
    vProfilo    V_PROFILI%ROWTYPE;
 BEGIN
    GetElencoProfili(vCursore,pAnnoRif,pCodGestElem,pTipoMisura,pMeseRif,pTipoGiorno);
    LOOP
        FETCH vCursore INTO vProfilo.COD_ELEMENTO, vProfilo.COD_GEST_ELEMENTO, vProfilo.COD_TIPO_MISURA,
                            vProfilo.ANNO, vProfilo.MESE, vProfilo.TIPO,
                            vProfilo.DT_MISURE_INIZIO, vProfilo.DT_MISURE_FINE;
        EXIT WHEN vCursore%NOTFOUND;
        PIPE ROW(vProfilo);
    END LOOP;
    CLOSE vCursore;
    RETURN;
 END GetElencoProfili;







-- ----------------------------------------------------------------------------------------------------------



PROCEDURE AddMisureSTLPE       (  pDataDA IN DATE ,
                                                    pDataA IN DATE ,
                                                    pTipoMisura       IN TIPI_MISURA.COD_TIPO_MISURA%TYPE DEFAULT NULL,
                                                    pCodElem      IN ELEMENTI.COD_ELEMENTO%TYPE DEFAULT -1,
                                                    pAggrega        IN NUMBER DEFAULT 1
                                                      ) AS
/*-----------------------------------------------------------------------------------------------------------
    Memorizza le misure da ST-LPE  ricevute.
-----------------------------------------------------------------------------------------------------------*/

  vLog          PKG_Logs.t_StandardLog;

  vOrigineMis   TIPI_MISURA_CONV_ORIG.ORIGINE%TYPE := 'METEO';
  vFlagSplit    TIPI_MISURA_CONV_ORIG.FLG_SPLIT%TYPE;



  vFattore      NUMBER;
  vTipMis       TIPI_MISURA.COD_TIPO_MISURA%TYPE := '_';

  vCodEleNome   ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
  vCodTipoElem  ELEMENTI_DEF.COD_TIPO_ELEMENTO%TYPE;
  vCodTipoRete  TIPI_RETE.COD_TIPO_RETE%TYPE;
  vCodTipoClie  ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE;
  vCodTratElem  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE := 0;

  vDataMin      DATE := TO_DATE('01013000','ddmmyyyy');
  vDataMax      DATE := TO_DATE('01011900','ddmmyyyy');

  vResult       INTEGER;
  vNumIns       INTEGER := 0;
  vNumMod       INTEGER := 0;

  vTot          NUMBER := 0;



vSql  VARCHAR2(1200) := 'select COD_ELEMENTO, :pTipoMisura COD_TIPO_MISURA , to_date(to_char(:p_data, ''dd-mm-yyyy'') || DAYTIME ,''dd-mm-yyyy hh24:mi:ss'')  DATA ,  VALORE , NVL(F.COD_RAGGR_FONTE,''S'') COD_TIPO_FONTE   from PROFILI   '||
                                        'JOIN PROFILI_VAL  USING(COD_PROFILO) '||
                                        'JOIN ELEMENTI_DEF D USING(COD_ELEMENTO) '||
                                        'LEFT OUTER JOIN TIPO_FONTI F USING(COD_TIPO_FONTE) '||
                                        'where COD_ELEMENTO in ( '||
                                        '      SELECT DISTINCT COD_ELEMENTO   FROM ELEMENTI    WHERE COD_TIPO_ELEMENTO IN (''CMT'',''TRM'') ) '||
                                        '          and mese = to_char (:p_data , ''mm'')    '||
                                        '          and tipo =sar_admin.PKG_CALENDARIO.GetTipoData(:p_data)  '||
                                        '          and cod_tipo_misura = :pTipoMisura '||
                                        '           AND :p_data BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE';


vSqlPeriodo VARCHAR2(1000) :=
' SELECT DATA GIORNO  ' ||
' FROM TABLE(sar_admin.PKG_CALENDARIO.GetCalendario(TO_DATE(''01/01/''||to_char(:pdatada , ''YYYY'') ,''DD/MM/YYYY''),TO_DATE(''01/01''||TO_CHAR(TO_NUMBER(to_char(:pdataa , ''YYYY''))+1),''DD/MM/YYYY''),1)) '||
'where DATA between :pdatada and :pdataa';



    vCodElem       ELEMENTI.COD_ELEMENTO%TYPE := 0;
    vCodTipoMis   TIPI_MISURA.COD_TIPO_MISURA%TYPE;
    vDataMisura     DATE;
    vValore             NUMBER;
    vCodTipoFonte TIPO_FONTI.COD_TIPO_FONTE%TYPE;

    vGiornoElaborato DATE;

   cvIns SYS_REFCURSOR;
   cvPeriodo SYS_REFCURSOR;

   pMisureMeteo         T_MISMETEO_ARRAY := T_MISMETEO_ARRAY();
   --pMisureMeteoObj T_MISMETEO_OBJ := T_MISMETEO_OBJ(0,'0',sysdate,0,'S');   
   pMisureMeteoObj T_MISMETEO_OBJ := T_MISMETEO_OBJ(0,'0',sysdate,0,'S','');   -- mod MAGO-1180 


 BEGIN




    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassBE||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_Misure.AddMisureSTLPE',
                                pDescrizione  => 'Origine Misura: '||vOrigineMis,
                                pStoreOnFile  => FALSE);

    DBMS_OUTPUT.PUT_LINE (vSqlPeriodo);
    OPEN cvPeriodo FOR vSqlPeriodo
    using  pDataDA, pDataA,pDataDA, pDataA;

     LOOP
        FETCH cvPeriodo INTO
        vGiornoElaborato;
        EXIT WHEN cvPeriodo%NOTFOUND;
        DBMS_OUTPUT.PUT_LINE ('****************************************************'||vGiornoElaborato);

        if  pCodElem <> -1 then
            vSql := Vsql || ' AND COD_ELEMENTO = ' ||  pCodElem;
        end if;
        DBMS_OUTPUT.PUT_LINE (vSql);

        OPEN cvIns FOR vSql
        using  pTipoMisura , vGiornoElaborato, vGiornoElaborato,vGiornoElaborato,pTipoMisura,vGiornoElaborato ;

        LOOP
            FETCH cvIns INTO
            pMisureMeteoObj.COD_ELEMENTO, pMisureMeteoObj.COD_TIPO_MISURA,   pMisureMeteoObj.DATA, pMisureMeteoObj.VALORE, pMisureMeteoObj.COD_TIPO_FONTE;
            EXIT WHEN cvIns%NOTFOUND;
                pMisureMeteo.extend;
                IF pMisureMeteo.LAST IS NOT NULL THEN
                        DBMS_OUTPUT.PUT_LINE ('COD_ELEMENTO:' || pMisureMeteoObj.COD_ELEMENTO   || ' COD_TIPO_MISURA:' || pMisureMeteoObj.COD_TIPO_MISURA || ' DATA:' || to_char(pMisureMeteoObj.DATA, 'dd-mm-yyyy hh24:mi:ss')
                        || ' VALORE:' || pMisureMeteoObj.VALORE   || ' COD_TIPO_FONTE:' || pMisureMeteoObj.COD_TIPO_FONTE);
                    pMisureMeteo(pMisureMeteo.LAST)  := pMisureMeteoObj;
                end if ;
        END LOOP;

        DBMS_OUTPUT.PUT_LINE ('----------------- CREATO ARRAY CON MISURE ---------------' || pMisureMeteo.count );

        DBMS_OUTPUT.PUT_LINE ('----------------- PRIMA AddMisureMeteo ---------------' || pMisureMeteo.count );
        if  (pAggrega = 1 ) then
            PKG_MISURE.AddMisureMeteo( pMisureMeteo , true );
        else
            PKG_MISURE.AddMisureMeteo( pMisureMeteo , false );
        end if ;
        DBMS_OUTPUT.PUT_LINE ('----------------- DOPO AddMisureMeteo ---------------' || pMisureMeteo.count );

    END LOOP;



 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END AddMisureSTLPE;




/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_PROFILI;
/