SPOOL &spool_all append

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.1.8 rev-008 SRC
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

PROMPT _______________________________________________________________________________________
PROMPT GRANT  <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT
conn sys/sys_dba&tns_arcdb2 as sysdba
@./adRES/Grants/MANAGE_SCHEDULER.sql
conn sar_admin/sar_admin_dba&tns_arcdb2
@./adRES/Grants/ADMIN_PARAMETRI_GENERALI.sql
@./adRES/Grants/ADMIN_PKG_UTL.sql

PROMPT _______________________________________________________________________________________
PROMPT SAR_ADMIN PARAMETRI GENERALI   <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT
conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/ADM_PARAMETRI_GENERALI.sql
@./adRES/InitTables/SAR_ADMIN_PARAMETRI_GENERALI.sql


conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Ferma job attivi
PROMPT
@./adRES/SchedulerJobs/STOP_SINCRO_TRANSFER_RES.sql
@./adRES/SchedulerJobs/STOP_SINCRO_TRANSFER_RES_R.sql
@./adRES/SchedulerJobs/STOP_STORICO_REGISTRO_INVIO.sql

PROMPT _______________________________________________________________________________________
PROMPT DB Links
PROMPT
@./adRES/Dblinks/RES_DB_NAZ.sql
@./adRES/Dblinks/PKG1_RIGEDI.sql

PROMPT _______________________________________________________________________________________
PROMPT TYPES
PROMPT
@./Types/T_MISMETEO_OBJ.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./adRES/Tables/GTTD_GET_MISURE.sql
@./adRES/Tables/GTTD_MIS_SCHEMA_FRONT.sql
@./adRES/Tables/REGISTRO_RES_INVIO.sql
@./adRES/Tables/SCAGLIONI_DI_POTENZA.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ELEMENTI_DGF.sql
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql


PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./adRES/Packages/PKG_TRANSFER_RES.sql
@./Packages/PKG_MISURE.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_PROFILI.sql
@./Packages/PKG_REPORTS.sql
@./Packages/PKG_ANAGRAFICHE.sql
@./Packages/PKG_ELEMENTI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./adRES/PackageBodies/PKG_TRANSFER_RES.sql
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_PROFILI.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_ELEMENTI.sql

PROMPT _______________________________________________________________________________________
PROMPT Scheduler jobs
PROMPT
@./adRES/SchedulerJobs/SINCRO_TRANSFER_RES.sql
@./adRES/SchedulerJobs/SINCRO_TRANSFER_RES_R.sql
@./adRES/SchedulerJobs/STORICO_REGISTRO_INVIO.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
conn sar_admin/sar_admin_dba&tns_arcdb1
@./adRES/InitTables/SAR_ADMIN_PARAMETRI_GENERALI.sql
conn mago/mago&tns_arcdb2
@./adRES/InitTables/SCAGLIONI_DI_POTENZA.sql


PROMPT _______________________________________________________________________________________
PROMPT DB Links
PROMPT
@./adRES/Dblinks/PKG1_RIGEDI.sql

PROMPT _______________________________________________________________________________________
PROMPT Scheduler jobs
PROMPT
@./adRES/SchedulerJobs/SINCRO_TRANSFER_RES.sql
@./adRES/SchedulerJobs/SINCRO_TRANSFER_RES_R.sql
@./adRES/SchedulerJobs/STORICO_REGISTRO_INVIO.sql


PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.1.8 SRC
PROMPT

spool off
