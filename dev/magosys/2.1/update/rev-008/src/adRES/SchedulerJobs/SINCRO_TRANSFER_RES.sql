PROMPT SCHEDULER JOB SINCRO_TRANSFER_RES;

BEGIN
	
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'SINCRO_TRANSFER_RES',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_TRANSFER_RES.SP_TRANSFER_MIS',
                 start_date           => TRUNC(SYSDATE),
                 repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Esegue trasferimento dati sistema MAGO verso RES nazionale'
                );
                
   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
/

