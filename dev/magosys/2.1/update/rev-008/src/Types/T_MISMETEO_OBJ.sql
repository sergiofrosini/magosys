PROMPT TYPES MODIFICA T_MISMETEO_OBJ - T_MISMETEO_ARRAY


BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_MISMETEO_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_MISMETEO_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

CREATE OR REPLACE TYPE "T_MISMETEO_OBJ" AS OBJECT
      (COD_ELEMENTO NUMBER
       ,COD_TIPO_MISURA  VARCHAR2(6)
       ,DATA             DATE
       ,VALORE           NUMBER
       ,COD_TIPO_FONTE   VARCHAR2(2)
       ,QUALITY VARCHAR2(1) 
      )
/

CREATE OR REPLACE TYPE "T_MISMETEO_ARRAY"                                          IS TABLE OF T_MISMETEO_OBJ;
/