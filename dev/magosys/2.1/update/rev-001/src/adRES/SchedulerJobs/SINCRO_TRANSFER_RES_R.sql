PROMPT SCHEDULER JOB SINCRO_TRANSFER_RES_R;

BEGIN
	   
   SYS.DBMS_SCHEDULER.CREATE_JOB
                (job_name             => 'SINCRO_TRANSFER_RES_R',
                 job_type             => 'stored_procedure',
                 job_action           => 'PKG_TRANSFER_RES.SP_TRANSFER_MIS_R',
                 start_date           => TRUNC(SYSDATE),
                 repeat_interval      => 'FREQ=HOURLY; BYMINUTE=00,15,30,45;',
                 enabled              => TRUE,
                 auto_drop            => FALSE,
                 comments             => 'Esegue trasferimento dati sistema RIGEDI verso RES nazionale'
                );

   EXCEPTION
    WHEN OTHERS THEN NULL;
   END;
/