PROMPT Package Body PKG_TRANSFER_RES

create or replace PACKAGE BODY      PKG_TRANSFER_RES AS

-- =========================================================================================

g_Sistema_Rigedi    NUMBER := 1;
g_Sistema_Mago      NUMBER := 2;

g_ApplXParam        VARCHAR2(50) := 'MAGO';

g_ParamDelta        VARCHAR2(50) := 'RES_DELTA_MINUTI';
g_ParamDeltaR       VARCHAR2(50) := 'RES_DELTA_MINUTI_R';
g_ParamAttivo       VARCHAR2(50) := 'RES_ATTIVA_JOB';
g_ParamStorico      VARCHAR2(50) := 'RES_REGISTRO_INVIO';

g_defaultDelta      NUMBER := 60;
g_defaultDeltaR     NUMBER := 15;
g_defaultAttivo     NUMBER := 1;
g_defaultStorico    NUMBER := 60;

g_StatoRecord       GTTD_MIS_SCHEMA_FRONT.FLG_STATO_RECORD%TYPE := 'R';

g_VTContatori        GTTD_VALORI_TEMP.TIP%TYPE := 'CONTAxRES';

g_DumDum_s          VARCHAR2(100); -- variabile generica stringa
g_DumDum_n          NUMBER; -- variabile generica numerica

-- =========================================================================================
PROCEDURE PRINTA(pTxt IN VARCHAR2 DEFAULT NULL, pLev IN INTEGER DEFAULT PKG_UtlGlb.gcTrace_INF) AS
BEGIN
 PKG_Logs.TraceLog(pTxt,pLev);
  --INSERT INTO ZZZ_PRINTA (QUANDO, TESTO) VALUES (CURRENT_TIMESTAMP, pTxt);
END;
-- =========================================================================================

procedure SP_STORICO_REGISTRO_INVIO
 as

  RetVal NUMBER;
  v_num_par  number;

BEGIN

    SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamStorico, g_DumDum_s);
    v_num_par := NVL(g_DumDum_s,g_defaultStorico);


    delete from  REGISTRO_RES_INVIO
    where DATA_MISURA < trunc(sysdate - v_num_par);
     COMMIT;

Exception when others then
      rollback;

END;

-- =========================================================================================

PROCEDURE  sp_transfer_mis_R IS

 v_def_site elementi.cod_elemento%TYPE;
 v_def_ese elementi.cod_elemento%TYPE;
 v_def_sg_site elementi.cod_gest_elemento%TYPE;
 v_def_sg_ese elementi.cod_gest_elemento%TYPE;
 v_def_t_site elementi.cod_tipo_elemento%TYPE;
 v_def_t_ese elementi.cod_tipo_elemento%TYPE;
 v_data_input DATE;
 v_data_input0 DATE;
 v_data_invio DATE;
 v_risulta NUMBER;
 v_log_filename VARCHAR2(40) := 'RIGEDI_sp_transfer_sql';

 v_tipmis GTTD_MIS_SCHEMA_FRONT.COD_TIPO_MISURA%TYPE := 'PMP';

 v_delta NUMBER;

 err_msg VARCHAR2(1000);

 v_sql_insString VARCHAR2(2000):= PKG_UTLGLB.CompattaSelect(
         'INSERT INTO GTTD_MIS_SCHEMA_FRONT ' ||
         '(SG_COD, COD_TIPO_ELEMENTO, COD_TIPO_MISURA, COD_TIPO_FONTE, ' ||
         ' COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
         ' STATO_PRODUTTORE_ID,  ' ||
         ' ORGANIZZAZIONE, TIPO_AGGREGAZIONE, SISTEMA, ' ||
         ' DATA_MISURA, VALORE_MISURA,  SCAGLIONE_POT,  NUMERO_PROD, ' ||
         '  FLG_STATO_RECORD,  LOG_ERRORE_ELABORAZIONE, DATA_RICEZIONE) ' ||
         'select :v_site SG_COD , :v_t_site COD_TIPO_ELEMENTO, COD_TIPO_MISURA , ' ||
         'f.cod_raggr_fonte COD_TIPO_FONTE, ' ||
         ' ''0'' COD_TIPO_RETE , ''0''  COD_TIPO_CLIENTE, ' ||
         ' PR.CATEGORIA_PROD_ID, ' ||
         ' PR.stato_attuale_id STATO_PRODUTTORE_ID,  ' ||
         '1 ORGANIZZAZIONE,   1 TIPO_AGGREGAZIONE,  ' ||
         g_Sistema_Rigedi||' SISTEMA, ' ||
--         ' cast(trunc(m.DATA,''hh24'') as timestamp) at time zone ''UTC''  DATA_MISURA,  ' ||
         ' cast(m.DATA as timestamp) at time zone ''UTC''  DATA_MISURA,  ' ||
         '  sum(M.VALORE) VALORE_MISURA, ' ||
         ' :v_scagl SCAGLIONE_POT,   ' ||
         ' count(*) NUMERO_PROD, '''||g_statoRecord||''' FLG_STATO_RECORD,  ' ||
         ' null LOG_ERRORE_ELABORAZIONE,  ' ||
         ' CURRENT_TIMESTAMP DATA_RICEZIONE ' ||
         ' from V_PRODUTTORI_X_RES@PKG1_RIGEDI.IT PR ' ||
         ' INNER JOIN (SELECT f.cod_raggr_fonte,f.cod_tipo_fonte  FROM RAGGRUPPAMENTO_FONTI r'||
         '            inner join TIPO_FONTI f'||
                      ' ON (r.cod_raggr_fonte = f.cod_raggr_fonte'||
                         ' AND f.COD_RAGGR_FONTE_ALFA_RIF IS NULL) '||
                         '                      )  f ON PR.FONTE = f.cod_tipo_fonte  '||
         ' JOIN (SELECT * FROM MISURE_ACQUISITE M  ' ||
         '              JOIN (SELECT * FROM TRATTAMENTO_ELEMENTI WHERE COD_TIPO_MISURA IN (:v_tipmis) ' ||
         '                      AND COD_TIPO_CLIENTE IN ( ''A'',''B'',''X'' ) ) T ' ||
         '              ON (M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM)' ||
         '              JOIN ELEMENTI E ON (E.COD_ELEMENTO = T.COD_ELEMENTO) ) M' ||
         ' ON (M.COD_GEST_ELEMENTO = PR.GEST_PROD_ID ' ||
         ' AND M.DATA BETWEEN PR.DT_INI_P AND PR.DT_FIN_P '||
         ' AND M.DATA BETWEEN NVL(PR.DT_INI_P,M.DATA) AND NVL(PR.DT_FIN_P,M.DATA) ) '||
         ' AND M.DATA BETWEEN :v_da1 AND :v_da2'||
         ' and PR.POT_INST >= :v_POT_DA '||
         ' AND PR.POT_INST < :v_POT_A '||
         ' group by  COD_TIPO_MISURA, f.cod_raggr_fonte ,  ' ||
         ' PR.CATEGORIA_PROD_ID, ' ||
         ' PR.stato_attuale_id ,    ' ||
     --    ' cast(trunc(m.DATA,''hh24'') as timestamp) , CURRENT_TIMESTAMP '
         ' cast(m.DATA as timestamp) , CURRENT_TIMESTAMP '
         );






 v_sql_s VARCHAR2(2000):= PKG_UTLGLB.CompattaSelect(
        ' INSERT INTO MISURE_SCHEMA_FRONTIERA@RES_DB_NAZ ' ||
        ' (SG_COD, COD_TIPO_ELEMENTO, COD_TIPO_MISURA, COD_TIPO_FONTE, ' ||
        ' COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
        ' STATO_PRODUTTORE_ID, ' ||
        ' ORGANIZZAZIONE, TIPO_AGGREGAZIONE, SISTEMA, ' ||
        ' SCAGLIONE_POT, NUMERO_PROD, ' ||
        ' DATA_MISURA, VALORE_MISURA,  FLG_STATO_RECORD,  LOG_ERRORE_ELABORAZIONE, DATA_RICEZIONE) ' ||
        ' select '||
        '  :v_site, :v_t_site, COD_TIPO_MISURA, COD_TIPO_FONTE, ' ||
        ' COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
        ' STATO_PRODUTTORE_ID, ' ||
        ' 1, 1, ' ||
        g_Sistema_Rigedi||', ' ||
        ' SCAGLIONE_POT, '||
        ' count(*) NUMERO_PROD, ' ||
        ' DATA_MISURA, sum(VALORE_MISURA) VALORE_MISURA,   ' ||
        ' '''||g_statoRecord||''',   ' ||
        ' null, DATA_RICEZIONE ' ||
        ' FROM GTTD_MIS_SCHEMA_FRONT ' ||
        ' group by  COD_TIPO_MISURA, COD_TIPO_FONTE ,  ' ||
        ' COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
        ' stato_produttore_id, scaglione_pot, ' ||
        ' DATA_MISURA , DATA_RICEZIONE '
              );

          vLst            PKG_UtlGlb.t_query_cur;

        vNumMis number;
        pOrganizzazione INTEGER := 1;
        pStatoRete      INTEGER := 1;
        pGstElem        ELEMENTI.COD_GEST_ELEMENTO%TYPE :=null;
        pCodElem        VARCHAR2(1000);
        pDataDa         DATE := TO_DATE ('01/12/2016 14.00', 'dd/mm/yyyy hh24.mi');
        pDataA          DATE := TO_DATE ('01/12/2016 15.00', 'dd/mm/yyyy hh24.mi');
        pTipiMisura     VARCHAR2 (300) := 'PMP';
        pTipologiaRete  VARCHAR2 (300) := 'A|B|X';
        pFonte          VARCHAR2 (300); -- := 'S|E|I|T|R|C|1|2|3';
        pTipoProd       VARCHAR2 (300) := 'A|B|X|';
        vAggr           INTEGER := 30;

        vRigaGetMisure GTTD_GET_MISURE%ROWTYPE;

BEGIN

    SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamAttivo, g_DumDum_s);
    g_DumDum_n := NVL(g_DumDum_s,g_defaultAttivo);

    IF g_DumDum_n = 1 THEN


        PRINTA('Attivo sp_transfer_mis_R '||to_char(sysdate,'dd-mm-yyyy hh24:mi:ss'),PKG_UtlGlb.gcTrace_INF);


        SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamDeltaR, g_DumDum_s);
        v_delta := NVL(g_DumDum_s,g_defaultDeltaR) / 1440;


        pFonte := null;
        FOR i_fonti in (SELECT distinct cod_raggr_fonte FROM
                            RAGGRUPPAMENTO_FONTI R
                            JOIN TIPO_FONTI F
                            USING (cod_raggr_fonte)
                            WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                            AND NOT  COD_TIPO_FONTE IN ('2','3','H'))
        LOOP

           pfonte := pfonte || '|' || i_fonti.cod_raggr_fonte ;

        END LOOP;

        pfonte := SUBSTR(pfonte,2);



   /*   Occorre fare la select in maniera differente tra intervalllo di un'ora ripetto altre in quanto se si passa da 15 ad 60 minuti si richia di avere il registro avanzare 18:15; 19:15:.... */
    BEGIN
        select trunc(CASE v_delta
                WHEN 60 THEN
                 nvl(max(trunc(data_misura, 'HH24') ),  trunc(sysdate , 'HH24') - (v_delta/60)/24  )  + (v_delta/60)/24
                ELSE
                 nvl(max(trunc(data_misura, 'MI') ),  trunc(sysdate , 'HH24') - 1/24   )  + (v_delta/60) / 24
                END,
                'MI')
            into v_data_input0
            from REGISTRO_RES_INVIO
            where sistema = g_Sistema_Rigedi ;
    EXCEPTION WHEN OTHERS THEN
        v_data_input0 :=  trunc(sysdate , 'HH24') ;
    END ;



/*
        BEGIN
            SELECT nvl(MAX(data_misura) , SYSDATE  - v_delta  )  + v_delta
            INTO v_data_input0
            FROM registro_res_invio
            WHERE SISTEMA = g_sistema_Mago;
        EXCEPTION WHEN OTHERS THEN
            v_data_input0 :=  sysdate ;
        END ;
 */

        v_data_invio :=  sysdate;


        SELECT
            cod_elemento_co,cod_elemento_ese,
            PKG_ELEMENTI.GetGestElemento(cod_elemento_co),PKG_ELEMENTI.GetGestElemento(cod_elemento_ese),
            ts.cod_tipo_elemento, te.cod_tipo_elemento
        INTO
            v_def_site,v_def_ese,
            v_def_sg_site,v_def_sg_ese,
            v_def_t_site,v_def_t_ese
        FROM default_co
        JOIN elementi ts
        ON (ts.cod_elemento=cod_elemento_co)
        JOIN elementi te
        ON (te.cod_elemento=cod_elemento_ese)
        WHERE flag_primario = 1;


        v_data_input := v_Data_Input0;

        vNumMis := 0;


        WHILE v_data_input <= v_data_invio
        LOOP

            FOR i_TIPI in ( select * from
                                    (SELECT 'A' tipCli from dual
                                        union SELECT 'B' tipCli from dual
                                        union SELECT 'X' tipCli from dual) cli,
                                    (SELECT 'B' tipRet from dual
                                        union SELECT 'M' tipRet from dual) ret )
            LOOP

                FOR i_PROD in (SELECT PR.GEST_PROD_ID,
                                      PR.CATEGORIA_PROD_ID,
                                      PR.stato_attuale_id STATO_PRODUTTORE_ID,
                                        E.COD_TIPO_ELEMENTO,
                                        s.scaglione_pot
                                FROM V_PRODUTTORI_X_RES@PKG1_RIGEDI.IT PR
                                join ELEMENTI E
                                ON (pr.gest_prod_id=e.cod_gest_elemento)
                                join SCAGLIONI_DI_POTENZA s
                                ON ( PR.POT_INST >= s.POT_DA
                                    AND PR.POT_INST < s.POT_A)  )
                LOOP

                   pkg_Misure.GetMisure (vLst, PKG_ELEMENTI.GetCodElemento(i_PROD.GEST_PROD_ID),
                     --   (v_data_input-v_delta), v_data_input,
                        v_data_input, v_data_input,
                        pTipiMisura, pOrganizzazione, pStatoRete,
                        i_TIPI.tipRet, pfonte, i_TIPI.tipCli, vaggr);

                 vRigaGetMisure.COD_ELEMENTO := v_def_ese;

                    LOOP
                    FETCH vLst INTO
                        vRigaGetMisure.COD_MISURA,
                        vRigaGetMisure.DATA_MISURA,
                        vRigaGetMisure.VALORE_MISURA,
                        vRigaGetMisure.TIPO_FONTE,
                        vRigaGetMisure.INTERPOL;
                    EXIT WHEN vLst%NOTFOUND;

                        vNumMis := vNumMis+1;

                        INSERT INTO GTTD_MIS_SCHEMA_FRONT
                        (
                            SG_COD,
                            COD_TIPO_ELEMENTO,
                            CATEGORIA_PROD_ID,
                            STATO_PRODUTTORE_ID,
                            SISTEMA,
                            SCAGLIONE_POT,
                            NUMERO_PROD,
                            FLG_STATO_RECORD,
                            LOG_ERRORE_ELABORAZIONE,
                            DATA_RICEZIONE,
                            COD_TIPO_MISURA,
                            COD_TIPO_FONTE,
                            COD_TIPO_RETE,
                            COD_TIPO_CLIENTE,
                            ORGANIZZAZIONE,
                            TIPO_AGGREGAZIONE,
                            DATA_MISURA,
                            VALORE_MISURA
                        )
                        values
                        (
                            v_def_sg_ese,
                            v_def_t_site,
                            i_PROD.CATEGORIA_PROD_ID,
                            i_PROD.STATO_PRODUTTORE_ID,
                            g_Sistema_Rigedi,
                            i_PROD.SCAGLIONE_POT,
                            0,
                            g_statoRecord,
                            null ,
                            cast(trunc(v_data_input,'MI') as timestamp) at time zone 'UTC',
                            vRigaGetMisure.COD_MISURA,
                            vRigaGetMisure.TIPO_FONTE,
                            i_TIPI.TIPRET,
                            i_TIPI.TIPCLI,
                            1,
                            1,
                            cast(trunc(v_data_input,'MI') as timestamp) at time zone 'UTC',
                            vRigaGetMisure.VALORE_MISURA
                        );

                    END LOOP; -- curosor GeTMisure
                    CLOSE vLst;

                END LOOP;-- loop produttori

        PRINTA('   fine cli/ret - '||i_TIPI.tipCLI||' - '||i_TIPI.tipRet||' - '||v_data_input||' - '||current_timestamp||']',PKG_UtlGlb.gcTrace_VRB);


            END LOOP;  -- tipi ret(fon(cli

            INSERT INTO REGISTRO_RES_INVIO
            (DATA_MISURA, DATA_INVIO, SISTEMA)
            VALUES
            (v_data_input, v_data_invio, g_sistema_Rigedi);




            v_data_input := v_data_input + v_delta;

            IF v_data_input > v_data_invio THEN
                EXIT;
            END IF;

        --    dbms_output.put_line(' ');
    
        DBMS_OUTPUT.PUT_LINE(v_sql_s);
        EXECUTE IMMEDIATE v_sql_s 
        USING v_def_sg_ese, v_def_t_site;

        DELETE FROM GTTD_MIS_SCHEMA_FRONT;
        END LOOP; -- date

   --     dbms_output.put_line(' ');

 --   DELETE FROM zzz_misureResR;
 --  INSERT INTO zzz_misureresR SELECT * FROM GTTD_MIS_SCHEMA_FRONT;
        
        
    --    dbms_output.put_line(' arrivo - '||current_timestamp);

        COMMIT;

        PKG_LOGS.TraceLog('sp_transfer_sql [Terminato]');


    END IF;

EXCEPTION WHEN OTHERS THEN
      err_msg := '[ERROR] sp_transfer_sql '|| SUBSTR(SQLERRM, 1, 100) ;
      PKG_LOGS.TraceLog(err_msg,PKG_UtlGlb.gcTrace_ERR);
      raise;
END ;

--==============================================================================

PROCEDURE  sp_transfer_mis IS

 TYPE t_contatori IS TABLE OF NUMBER INDEX BY VARCHAR2(30);

 v_contatori t_contatori;
 v_contatoriBase t_contatori;

 v_indxCont VARCHAR2(30);



 TYPE t_fontiRaggr IS TABLE OF VARCHAR2(1) INDEX BY VARCHAR2(1);

 v_fontiRaggr t_fontiRaggr;

 v_indxFont VARCHAR2(1);



 v_def_site elementi.cod_elemento%TYPE;
 v_def_ese elementi.cod_elemento%TYPE;
 v_def_sg_site elementi.cod_gest_elemento%TYPE;
 v_def_sg_ese elementi.cod_gest_elemento%TYPE;
 v_def_t_site elementi.cod_tipo_elemento%TYPE;
 v_def_t_ese elementi.cod_tipo_elemento%TYPE;
 v_data_input DATE;
 v_data_input0 DATE;
 v_data_invio DATE;
 v_risulta NUMBER;
 v_log_filename VARCHAR2(40) := 'RIGEDI_sp_transfer_sql';

 v_delta NUMBER;

 err_msg VARCHAR2(1000);

 v_conta number;



 v_sql_upd1 VARCHAR2(1500):= PKG_UTLGLB.CompattaSelect(
      ' UPDATE GTTD_MIS_SCHEMA_FRONT g   ' ||
      '  SET g.NUMERO_PROD = (SELECT NVL(v.NUM1,0)  ' ||
      '              FROM GTTD_VALORI_TEMP v     ' ||
      '              WHERE v.TIP = '''||g_VTContatori||'''   ' ||
      '              AND g.COD_TIPO_RETE = v.ALF1    ' ||
      '              AND g.COD_TIPO_FONTE = v.ALF2  ' ||
      '              AND ( (v.ALF3 = g.COD_TIPO_CLIENTE) OR  (v.ALF3 IN (''X'',''C'') AND (g.COD_TIPO_CLIENTE=''X'') ) )  ' ||
      ' ) WHERE g.cod_tipo_misura = ''PI'' '
      );


   v_sql_upd2 VARCHAR2(1500):= PKG_UTLGLB.CompattaSelect(
  'UPDATE GTTD_MIS_SCHEMA_FRONT m ' ||
        ' SET m.NUMERO_PROD = (SELECT p.NUMERO_PROD FROM ' ||
        ' GTTD_MIS_SCHEMA_FRONT p '||
       ' where p.cod_tipo_misura IN ( ''PI'' ) ' ||
         'and m.COD_TIPO_RETE    =  p.COD_TIPO_RETE ' ||
        ' and m.COD_TIPO_CLIENTE = p.COD_TIPO_CLIENTE '||
        'and m.COD_TIPO_FONTE    =  p.COD_TIPO_FONTE ) ' ||
        ' where m.cod_tipo_misura = ''PMP'' '
        );

  v_sql_upd2_old VARCHAR2(1500):= PKG_UTLGLB.CompattaSelect(
        'UPDATE GTTD_MIS_SCHEMA_FRONT g ' ||
        'SET g.NUMERO_PROD = NVL(g.NUMERO_PROD,0)+ (SELECT COUNT(distinct COD_elemento) FROM ' ||
        ' MISURE_ACQUISITE_STATICHE m ' ||
        '    join TRATTAMENTO_ELEMENTI t ' ||
        '    using (cod_trattamento_elem) ' ||
         ' INNER JOIN (SELECT f.cod_raggr_fonte,f.cod_tipo_fonte  FROM RAGGRUPPAMENTO_FONTI r'||
         '            inner join TIPO_FONTI f'||
                      ' ON (r.cod_raggr_fonte = f.cod_raggr_fonte'||
                      ' AND f.COD_RAGGR_FONTE_ALFA_RIF IS NULL)'||
                         '                       )  f ON T.cod_tipo_fonte = f.cod_tipo_fonte  '||
        'where t.cod_tipo_misura IN ( ''PI'', ''PMP'' ) ' ||
        'and m.valore> 0  ' ||
        'and t.cod_tipo_elemento IN (''TRM'',''CMT'',''GMT'') '||
        'and t.ORGANIZZAZIONE = g.organizzazione  ' ||
        'and t.COD_TIPO_RETE =  g.COD_TIPO_RETE ' ||
    --    ' and ( (t.COD_TIPO_CLIENTE = g.COD_TIPO_CLIENTE) OR  (t.COD_TIPO_CLIENTE IN (''X'',''C'') AND (g.COD_TIPO_CLIENTE=''X'') ) ) '||
        ' and t.COD_TIPO_CLIENTE = g.COD_TIPO_CLIENTE '||
        'and f.cod_raggr_fonte =  g.COD_TIPO_FONTE ' ||
        'and m.DATA_DISATTIVAZIONE >=  :pdata1 '||
        'and m.DATA_ATTIVAZIONE <= :pdata2 '||
        ' ) where g.cod_tipo_misura IN (''PI'', ''PMP'')  '
        );


 v_sql_s VARCHAR2(1500):= PKG_UTLGLB.CompattaSelect(
        'INSERT INTO MISURE_SCHEMA_FRONTIERA@RES_DB_NAZ ' ||
        '(SG_COD, COD_TIPO_ELEMENTO, COD_TIPO_MISURA, COD_TIPO_FONTE, ' ||
        'COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
        'STATO_PRODUTTORE_ID, ' ||
        'ORGANIZZAZIONE, TIPO_AGGREGAZIONE, SISTEMA, ' ||
        'SCAGLIONE_POT, NUMERO_PROD, ' ||
        'DATA_MISURA, VALORE_MISURA,  FLG_STATO_RECORD,  LOG_ERRORE_ELABORAZIONE, DATA_RICEZIONE) ' ||
        'select '||
        'SG_COD, COD_TIPO_ELEMENTO, COD_TIPO_MISURA, COD_TIPO_FONTE, ' ||
        'COD_TIPO_RETE, COD_TIPO_CLIENTE, CATEGORIA_PROD_ID, ' ||
        'STATO_PRODUTTORE_ID, ' ||
        'ORGANIZZAZIONE, TIPO_AGGREGAZIONE, SISTEMA, ' ||
        'SCAGLIONE_POT, '||
        'NUMERO_PROD, ' ||
        'DATA_MISURA, VALORE_MISURA,  FLG_STATO_RECORD,  LOG_ERRORE_ELABORAZIONE, DATA_RICEZIONE ' ||
        ' FROM GTTD_MIS_SCHEMA_FRONT '
         );


        vLst            PKG_UtlGlb.t_query_cur;

        vNumMis number;
        vNumMisTRM number;
        pOrganizzazione INTEGER := 1;
        pStatoRete      INTEGER := 1;
        pGstElem        ELEMENTI.COD_GEST_ELEMENTO%TYPE :=null;
        pCodElem        VARCHAR2(1000);
        pDataDa         DATE := TO_DATE ('01/12/2016 14.00', 'dd/mm/yyyy hh24.mi');
        pDataA          DATE := TO_DATE ('01/12/2016 15.00', 'dd/mm/yyyy hh24.mi');
        pTipiMisura     VARCHAR2 (300) := 'PI|PMP';
        pTipologiaRete  VARCHAR2 (300) := 'A|B|X';
        pFonte          VARCHAR2 (300); -- := 'S|E|I|T|R|C|1|2|3';
        pFonteBase      VARCHAR2 (300); -- := 'S|E|I|T|R|C|1|2|3';
        pTipoProd       VARCHAR2 (300) := 'A|B|X';
        vAggr           INTEGER := 30;

        vRigaGetMisure GTTD_GET_MISURE%ROWTYPE;

        vTempo TIMESTAMP;

        vPrima  timestamp;
        vDopo   timestamp;
        vPrima0  timestamp;
        vDopo0   timestamp;

        vValMis NUMBER;


             --   vPrima  timestamp;
            --    vDopo   timestamp;
             --   vPrima0  timestamp;
             --   vDopo0   timestamp;

                tmdat TIMESTAMP ( 4 );
             --   vLst PKG_UtlGlb.t_query_cur;
                pTipologiaRete   VARCHAR2(300)  := 'M|B';
              --  pFonte           VARCHAR2(300)  := 'S|E|I|T|R|C|1';
                pTipoProd        VARCHAR2(300)  := 'A|B|X|C';
                pTipoElement     VARCHAR2(300)  := 'TRM|CMT|GMT';
                vElePadre        ELEMENTI.COD_ELEMENTO%TYPE;
                vGstPadre        ELEMENTI.COD_GEST_ELEMENTO%TYPE;
                vCodEle          ELEMENTI.COD_ELEMENTO%TYPE;
                vGstElem         ELEMENTI.COD_GEST_ELEMENTO%TYPE;
                vTipElem         TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
                vFonte           TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
                vPotenza         ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
                vCitta           METEO_REL_ISTAT.COD_CITTA%TYPE;
                vTipProd         TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
                vTipRet          TIPI_RETE.COD_TIPO_RETE%TYPE;
                vLat             NUMBER;
                vLon             NUMBER;
                vP01             FORECAST_PARAMETRI.PARAMETRO1%TYPE;
                vP02             FORECAST_PARAMETRI.PARAMETRO2%TYPE;
                vP03             FORECAST_PARAMETRI.PARAMETRO3%TYPE;
                vP04             FORECAST_PARAMETRI.PARAMETRO4%TYPE;
                vP05             FORECAST_PARAMETRI.PARAMETRO5%TYPE;
                vP06             FORECAST_PARAMETRI.PARAMETRO6%TYPE;
                vP07             FORECAST_PARAMETRI.PARAMETRO7%TYPE;
                vP08             FORECAST_PARAMETRI.PARAMETRO8%TYPE;
                vP09             FORECAST_PARAMETRI.PARAMETRO9%TYPE;
                vP10             FORECAST_PARAMETRI.PARAMETRO10%TYPE;
                vDT              FORECAST_PARAMETRI.DATA_ULTIMO_AGG%TYPE;
                vNum             NUMBER := 0;

vdumdum number:=0;
BEGIN -- SPC

    SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamAttivo, g_DumDum_s);
    g_DumDum_n := NVL(g_DumDum_s,g_defaultAttivo);

    IF g_DumDum_n = 1 THEN


        SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamDelta, g_DumDum_s);
        v_delta := NVL(g_DumDum_s,g_defaultDelta) / 1440;



        pFonte := null;
        FOR i_fonti in (SELECT distinct cod_raggr_fonte FROM
                            RAGGRUPPAMENTO_FONTI R
                            JOIN TIPO_FONTI F
                            USING (cod_raggr_fonte)
                            WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                            AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                            )
        LOOP

           pfonte := pfonte || '|' || i_fonti.cod_raggr_fonte ;

        END LOOP;

        pFonte := null;



        FOR i_fonti in (SELECT distinct cod_tipo_fonte, cod_raggr_fonte FROM
                            RAGGRUPPAMENTO_FONTI R
                            JOIN TIPO_FONTI F
                            USING (cod_raggr_fonte)
                            WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                            AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                            )
        LOOP

           v_fontiRaggr(i_fonti.cod_tipo_fonte) := i_fonti.cod_raggr_fonte;
           pfonteBase := pfonteBase || '|' || i_fonti.cod_tipo_fonte ;

        END LOOP;

        pfonte := SUBSTR(pfonte,2);


        /*   Occorre fare la select in maniera differente
        tra intervalllo di un'ora rispetto ad altre in quanto
        se si passa da 15 ad 60 minuti si richia di avere
        il registro avanzare 18:15; 19:15:.... */
        BEGIN
            select trunc(CASE v_delta
                    WHEN 60 THEN
                     nvl(max(trunc(data_misura, 'HH24') ),  trunc(sysdate , 'HH24') - (v_delta/60)/24  )  + (v_delta/60)/24
                    ELSE
                     nvl(max(trunc(data_misura, 'MI') ),  trunc(sysdate , 'HH24') - 1/24   )  + (v_delta/60) / 24
                    END,
                    'MI')
                into v_data_input0
                from REGISTRO_RES_INVIO
                where sistema = g_sistema_Mago ;
        EXCEPTION WHEN OTHERS THEN
            v_data_input0 :=  trunc(sysdate , 'HH24') ;
        END ;


        v_data_invio :=trunc(sysdate , 'HH24');

        SELECT
            cod_elemento_co,cod_elemento_ese,
            PKG_ELEMENTI.GetGestElemento(cod_elemento_co),PKG_ELEMENTI.GetGestElemento(cod_elemento_ese),
            ts.cod_tipo_elemento, te.cod_tipo_elemento
        INTO
            v_def_site,v_def_ese,
            v_def_sg_site,v_def_sg_ese,
            v_def_t_site,v_def_t_ese
        FROM default_co
        JOIN elementi ts
        ON (ts.cod_elemento=cod_elemento_co)
        JOIN elementi te
        ON (te.cod_elemento=cod_elemento_ese)
        WHERE flag_primario = 1;


        PRINTA(' partenza - '||current_timestamp);

        vPrima0 := current_timestamp;
        --    PRINTA('DATA_INIZIO:' ||to_char(v_data_input,'dd/mm/yyyy HH24:MI:SS'));
        v_data_input := v_Data_Input0;--v_data_invio - v_delta;


       ----****** VERIFICA *********
--       v_conta := 1;
--
--       Insert into verifica
--       (OPERAZIONE,DATA_OPE,VALORI )
--        values (1, sysdate, to_char(v_data_input, 'dd/mm/yyyy HH24:MI:SS')||'-'||v_data_invio );
--               Insert into verifica
--       (OPERAZIONE,DATA_OPE,VALORI )
--        values (2, sysdate, 'delta:' ||v_delta );
--              Insert into verifica
--       (OPERAZIONE,DATA_OPE,VALORI )
--        values (3, sysdate, 'fonti:' ||pfonte );
--     commit;
--

     ---************************************


        PRINTA(' v_data_input - '||to_char(v_data_input, 'dd/mm/yyyy HH24:MI:SS'));

        PRINTA(' v_data_invio - '||to_char(v_data_invio, 'dd/mm/yyyy HH24:MI:SS'));


        WHILE v_data_input <= v_data_invio
        LOOP

                vNumMis := 0;

                vNumMisTRM := 0;


                FOR i_TIPI in ( select * from
                                (SELECT 'A' tipCli from dual
                                    union SELECT 'B' tipCli from dual
                                     union SELECT 'X' tipCli from dual) cli,
                               (SELECT distinct cod_raggr_fonte as tipFon FROM
                                    RAGGRUPPAMENTO_FONTI R
                                    JOIN TIPO_FONTI F
                                    USING (cod_raggr_fonte)
                                    WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                                    AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                                   ) Fon,
                                (SELECT 'B' tipRet from dual
                                    union SELECT 'M' tipRet from dual) ret )
                loop

                    v_contatori(i_Tipi.tipRet || i_Tipi.tipfon || i_tipi.tipcli):=0;

                end loop;

                                FOR i_TIPI in ( select * from
                                (SELECT 'A' tipCli from dual
                                    union SELECT 'B' tipCli from dual
                                     union SELECT 'X' tipCli from dual) cli,
                               (SELECT distinct cod_tipo_fonte as tipFon FROM
                                    RAGGRUPPAMENTO_FONTI R
                                    JOIN TIPO_FONTI F
                                    USING (cod_raggr_fonte)
                                    WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                                    AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                                   ) Fon,
                                (SELECT 'B' tipRet from dual
                                    union SELECT 'M' tipRet from dual) ret )
                loop

                    v_contatoriBase(i_Tipi.tipRet || i_Tipi.tipfon || i_tipi.tipcli):=0;

                end loop;



                FOR i_TIPI in ( select * from
                                  --   (SELECT 'A|B|X|C|Z' tipCli from dual) cli,
                                  (SELECT 'A|B|X' tipCli from dual) cli,
                                   (SELECT pFonteBase tipFon from dual) Fon,
                                    (SELECT 'B|M' tipRet from dual) ret )

                loop

                tmdat := CURRENT_TIMESTAMP;
           --     zzz_numprod.GetElementForecast(vLst,
             --PKG_METEO.GetElementForecast(vLst,
             GetElementForRES(vLst,
                                                v_data_Input,
                                                i_Tipi.tipRet,
                                                i_Tipi.tipfon,
                                                i_tipi.tipcli,
                                                pTipoElement);
                   LOOP
                      FETCH vLst INTO vElePadre,vGstPadre,vCodEle,vGstElem,vTipElem,
                                      vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                      vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
                      EXIT WHEN vLst%NOTFOUND;

                    --  IF vTipElem IN ('TRM','CMT') THEN

                        v_indxCont := vTipRet
                                    ||v_fontiRaggr(vFonte)
                                    ||vTipProd;
                              -- ||(CASE WHEN vTipProd IN ('C','Z') THEN 'X' ELSE vTipProd END);

                        v_contatori(v_indxCont) := v_contatori(v_indxCont)+1;

                       v_indxCont := vTipRet
                                    ||vFonte
                                    ||vTipProd;
                              -- ||(CASE WHEN vTipProd IN ('C','Z') THEN 'X' ELSE vTipProd END);

                        v_contatoriBase(v_indxCont) := v_contatoriBase(v_indxCont)+1;

                        vNum := vNum + 1;

                 --     END IF;
                      END LOOP;
                 END LOOP;


        /*
                DELETE FROM GTTD_VALORI_TEMP WHERE TIP = g_VTContatori;
                DELETE FROM ZZZ_VT WHERE TIP = g_VTContatori;

                FOR i_TIPI in ( select * from
                                (SELECT 'A' tipCli from dual
                                    union SELECT 'B' tipCli from dual
                                     union SELECT 'X' tipCli from dual) cli,
                               (SELECT distinct cod_raggr_fonte as tipFon FROM
                                    RAGGRUPPAMENTO_FONTI R
                                    JOIN TIPO_FONTI F
                                    USING (cod_raggr_fonte)
                                    WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                                    AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                                   ) Fon,
                                (SELECT 'B' tipRet from dual
                                    union SELECT 'M' tipRet from dual) ret )
                loop

                 v_indxCont := i_TIPI.tipRet
                                ||i_TIPI.tipFon
                                ||(CASE i_TIPI.tipCli WHEN 'C' THEN 'X' ELSE i_TIPI.tipCli END);


                    INSERT INTO GTTD_VALORI_TEMP
                    (
                        TIP,
                        ALF1,ALF2,ALF3,
                        NUM1
                    )
                    VALUES
                    (
                        g_VTContatori,
                        i_TIPI.tipRet,
                        i_TIPI.tipFon,
                        (CASE i_TIPI.tipCli WHEN 'C' THEN 'X' ELSE i_TIPI.tipCli END),
                        v_contatori(v_indxCont)
                    );

                  INSERT INTO ZZZ_VT
                    (
                        TIP,
                        ALF1,ALF2,ALF3,
                        NUM1
                    )
                    VALUES
                    (
                        g_VTContatori,
                        i_TIPI.tipRet,
                        i_TIPI.tipFon,
                        (CASE i_TIPI.tipCli WHEN 'C' THEN 'X' ELSE i_TIPI.tipCli END),
                        v_contatori(v_indxCont)
                    );

                end loop;

             */



                vNumMis := vNumMis; -- questo e' solo un segnalibro per il debug

                -- inizio dello show:
                --      ciclo sui tipi cliente e reti +
                --      per fare per ciascuno una Getmisure differente
                --      altrimenti avrei i dati aggregeti

                FOR i_TIPI in ( select * from
                                    (SELECT 'A' tipCli from dual
                                        union SELECT 'B' tipCli from dual
                                        union SELECT 'X' tipCli from dual) cli,
                                    (SELECT 'B' tipRet from dual
                                        union SELECT 'M' tipRet from dual) ret )
                LOOP

                   vPrima := current_timestamp;

                    pkg_Misure.GetMisure (vLst, PKG_ELEMENTI.GetCodelemento(v_def_sg_ese),
                       -- (v_data_input-v_delta), v_data_input,
                        v_data_input, v_data_input,
                        pTipiMisura, pOrganizzazione, pStatoRete,
                        i_TIPI.tipRet, pfonte,
                   --     (case i_Tipi.TipCli WHEN 'X' THEN 'X|C|Z' ELSE i_Tipi.TipCli END),
                        i_Tipi.TipCli,
                        vAggr);

                       vDopo := current_timestamp;


                    vRigaGetMisure.COD_ELEMENTO := v_def_ese;

                    LOOP
                    FETCH vLst INTO
                        vRigaGetMisure.COD_MISURA,
                        vRigaGetMisure.DATA_MISURA,
                        vRigaGetMisure.VALORE_MISURA,
                        vRigaGetMisure.TIPO_FONTE,
                        vRigaGetMisure.INTERPOL;
                    EXIT WHEN vLst%NOTFOUND;

                        vNumMis := vNumMis+1;

                        INSERT INTO GTTD_MIS_SCHEMA_FRONT
                        (
                            SG_COD,
                            COD_TIPO_ELEMENTO,
                            CATEGORIA_PROD_ID,
                            STATO_PRODUTTORE_ID,
                            SISTEMA,
                            SCAGLIONE_POT,
                            NUMERO_PROD,
                            FLG_STATO_RECORD,
                            LOG_ERRORE_ELABORAZIONE,
                            DATA_RICEZIONE,
                            COD_TIPO_MISURA,
                            COD_TIPO_FONTE,
                            COD_TIPO_RETE,
                            COD_TIPO_CLIENTE,
                            ORGANIZZAZIONE,
                            TIPO_AGGREGAZIONE,
                            DATA_MISURA,
                            VALORE_MISURA
                        )
                        values
                        (
                            v_def_sg_ese,
                            v_def_t_site,
                            '999',
                            '999',
                            g_Sistema_Mago,
                            0,0,
                            g_statoRecord,
                            null ,
                            cast(trunc(v_data_invio,'hh24') as timestamp) at time zone 'UTC',
                            vRigaGetMisure.COD_MISURA,
                            vRigaGetMisure.TIPO_FONTE,
                            i_TIPI.TIPRET,
                            i_TIPI.TIPCLI,
                            1,
                            0,
                            cast(trunc(v_data_invio,'hh24') as timestamp) at time zone 'UTC',
                            vRigaGetMisure.VALORE_MISURA
                        );

                    END LOOP; -- cursor GeTMisure

                    CLOSE vLst;


                END LOOP; -- tipi ret(fon(cli

         --       vdumdum := v_contatori('B

                      vDopo0 := current_timestamp;

               vdumdum := v_contatori('BEX');

               /*
               DELETE FROM zzz_contatori;
               v_indxCont := v_contatori.FIRST;
               WHILE v_indxCont IS NOT NULL
               LOOP
                    IF v_contatori(v_indxCont)>0 then
                        INSERT INTO zzz_contatori
                        values (v_indxCont,v_contatori(v_indxCont));
                    end if;
                    v_indxCont := v_contatori.NEXT(v_indxCont);
               END LOOP;


               DELETE FROM zzz_contatoriBase;
               v_indxCont := v_contatoriBase.FIRST;
               WHILE v_indxCont IS NOT NULL
               LOOP
                    IF v_contatoriBase(v_indxCont)>0 then
                        INSERT INTO zzz_contatoriBase
                        values (v_indxCont,v_contatoriBase(v_indxCont));
                    end if;
                    v_indxCont := v_contatoriBase.NEXT(v_indxCont);
               END LOOP;



               DELETE FROM zzz_misureRes;
               INSERT INTO zzz_misureres SELECT * FROM GTTD_MIS_SCHEMA_FRONT;
                 */

              --   dbms_output.put_line(v_sql_upd1);
                -- aggionro i num. prod per rete B

             --


                IF (1=0) THEN -- con 1=0 esegue ciclo su v_contatori

                     EXECUTE IMMEDIATE v_sql_upd1;

                ELSE

                     FOR i_TIPI in ( select * from
                                (SELECT 'A' tipCli from dual
                                    union SELECT 'B' tipCli from dual
                                     union SELECT 'X' tipCli from dual) cli,
                               (SELECT distinct cod_raggr_fonte as tipFon FROM
                                    RAGGRUPPAMENTO_FONTI R
                                    JOIN TIPO_FONTI F
                                    USING (cod_raggr_fonte)
                                    WHERE f.COD_RAGGR_FONTE_ALFA_RIF IS NULL
                                    AND NOT  COD_TIPO_FONTE IN ('2','3','H')
                                   ) Fon,
                                 (SELECT 'B' tipRet from dual
                                    union SELECT 'M' tipRet from dual) ret,
                                   (SELECT 'PI' tipMis from dual
                                    union SELECT 'PMP' tipMis from dual) Mis  )
                    LOOP

                        MERGE INTO
                            GTTD_MIS_SCHEMA_FRONT g
                        USING
                            (select v_contatori(i_Tipi.tipRet || i_Tipi.tipfon || i_tipi.tipcli)
                            as quanti
                            from dual) c
                        ON
                               ( g.cod_tipo_misura = i_Tipi.tipMis
                            AND g.COD_TIPO_RETE = i_Tipi.tipRet
                            AND g.COD_TIPO_FONTE = i_Tipi.tipFon
                            AND g.cod_tipo_cliente = i_Tipi.tipCli)
                        WHEN MATCHED THEN
                        UPDATE SET
                           g.NUMERO_PROD = c.quanti
                        WHEN NOT MATCHED THEN
                        INSERT
                        (
                            SG_COD,
                            COD_TIPO_ELEMENTO,
                            CATEGORIA_PROD_ID,
                            STATO_PRODUTTORE_ID,
                            SISTEMA,
                            SCAGLIONE_POT,
                            NUMERO_PROD,
                            FLG_STATO_RECORD,
                            LOG_ERRORE_ELABORAZIONE,
                            DATA_RICEZIONE,
                            COD_TIPO_MISURA,
                            COD_TIPO_FONTE,
                            COD_TIPO_RETE,
                            COD_TIPO_CLIENTE,
                            ORGANIZZAZIONE,
                            TIPO_AGGREGAZIONE,
                            DATA_MISURA,
                            VALORE_MISURA
                        )
                        VALUES
                        (
                            v_def_sg_ese,
                            v_def_t_site,
                            '999',
                            '999',
                            g_Sistema_Mago,
                            0,
                            c.quanti,
                            g_statoRecord,
                            null ,
                            cast(trunc(v_data_invio,'hh24') as timestamp) at time zone 'UTC',
                            i_tipi.tipMis,
                            i_Tipi.tipFon,
                            i_TIPI.TIPRET,
                            i_TIPI.TIPCLI,
                            1,
                            0,
                            cast(trunc(v_data_invio,'hh24') as timestamp) at time zone 'UTC',
                            0
                        );


                    END LOOP;

             END IF;



             --                              INSERT INTO ZZZ_TEMPI (PROD,FON,CLI,RET,PRIMA,DOPO1,DOPO2) VALUES ( -666,
              --  -1,-1,-1,
              --              vPrima0,vDopo0,current_timestamp);
          --     DELETE FROM zzz_misureRes;
           --    INSERT INTO zzz_misureres SELECT * FROM GTTD_MIS_SCHEMA_FRONT;

                INSERT INTO REGISTRO_RES_INVIO
                (DATA_MISURA, DATA_INVIO, SISTEMA)
                VALUES
                (v_data_input, v_data_invio, g_sistema_Mago);


                PRINTA(' arrivo  - '||current_timestamp||' - num mis CO '||vnummis||' - num mis TRM '||vNumMisTRM );

                v_data_input := v_data_input + v_delta;

                IF v_data_input > v_data_invio THEN
                    EXIT;
                END IF;

        -- insert su tabella NAZ
        --   PRINTA(v_sql_s);
           EXECUTE IMMEDIATE v_sql_s;
           DELETE FROM  GTTD_MIS_SCHEMA_FRONT;                

        END LOOP; -- date
        
        
             
        PRINTA(' arrivo - '||current_timestamp);

        COMMIT;

        PKG_LOGS.TraceLog('sp_transfer_sql [Terminato]');


    END IF;

EXCEPTION WHEN OTHERS THEN
      err_msg := '[ERROR] sp_transfer_sql '|| SUBSTR(SQLERRM, 1, 100) ;
      PKG_LOGS.TraceLog(err_msg,PKG_UtlGlb.gcTrace_ERR);
      raise;
END ;

--===============================================================================

PROCEDURE GetElementForRES   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2,
                                pFonte          IN VARCHAR2,
                                pTipoProd       IN VARCHAR2,
                                pTipoElement    IN VARCHAR2,
                                pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                                pFlagPI         IN NUMBER DEFAULT 1) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante - Funzione clonata dalla PKG_METEO.GetElementForecast
-----------------------------------------------------------------------------------------------------------*/
    vNum        INTEGER;
    vFlgNull    NUMBER(1) := -1;

    vIdFonti    INTEGER := PKG_Mago_utl.GetIdTipoFonti(pFonte);
    vIdReti     INTEGER := PKG_Mago_utl.GetIdTipoReti(pTipologiaRete);
    vIdProd     INTEGER := PKG_Mago_utl.GetIdTipoClienti(pTipoProd);

    vElePadre   ELEMENTI.COD_ELEMENTO%TYPE;
    vGstPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vGstElem    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
    vFonte      TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
    vPotenza    ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
    vCitta      VARCHAR2(30);--METEO_REL_ISTAT.COD_CITTA%TYPE;
    vTipProd    TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
    vTipRet     TIPI_RETE.COD_TIPO_RETE%TYPE;
    vLat        NUMBER;
    vLon        NUMBER;
    vP01        FORECAST_PARAMETRI.PARAMETRO1%TYPE;
    vP02        FORECAST_PARAMETRI.PARAMETRO2%TYPE;
    vP03        FORECAST_PARAMETRI.PARAMETRO3%TYPE;
    vP04        FORECAST_PARAMETRI.PARAMETRO4%TYPE;
    vP05        FORECAST_PARAMETRI.PARAMETRO5%TYPE;
    vP06        FORECAST_PARAMETRI.PARAMETRO6%TYPE;
    vP07        FORECAST_PARAMETRI.PARAMETRO7%TYPE;
    vP08        FORECAST_PARAMETRI.PARAMETRO8%TYPE;
    vP09        FORECAST_PARAMETRI.PARAMETRO9%TYPE;
    vP10        FORECAST_PARAMETRI.PARAMETRO10%TYPE;
    vDT         FORECAST_PARAMETRI.DATA_ULTIMO_AGG%TYPE;

BEGIN

    DELETE FROM GTTD_FORECAST_ELEMENTS;

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey;
    PKG_Mago_utl.TrattaListaCodici(pTipoElement,PKG_Mago_utl.gcTmpTipEleKey, vFlgNull); -- non gestito il flag

    SELECT COUNT(*)
      INTO vNum
      FROM GTTD_VALORI_TEMP
     WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey
       AND ALF1 IN (PKG_Mago_utl.gcClienteAT,PKG_Mago_utl.gcClienteMT,PKG_Mago_utl.gcClienteBT);
     IF vNum > 0 THEN
        PKG_Meteo.GetProduttori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,0/*ElemenNonDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        IF PKG_Mago_utl.isMagoDgf THEN
            PKG_Meteo.GetProduttori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,1/*ElemenDisconnessi*/);
            LOOP
               FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;

    SELECT COUNT(*)
      INTO vNum
      FROM GTTD_VALORI_TEMP
     WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey
       AND ALF1 IN (PKG_Mago_utl.gcTrasformMtBt);
     IF vNum > 0 THEN
        GetTrasformatoriforRES(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,0 /*ElemenNonDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;

           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        IF PKG_Mago_utl.isMagoDgf THEN
            GetTrasformatoriForRES(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,1 /*ElemDisconnessi*/);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;

    SELECT COUNT(*)
      INTO vNum
      FROM GTTD_VALORI_TEMP
     WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey
       AND ALF1 IN (PKG_Mago_utl.gcGeneratoreAT,PKG_Mago_utl.gcGeneratoreMT,PKG_Mago_utl.gcGeneratoreBT);
     IF vNum > 0 THEN
        PKG_Meteo.GetGeneratori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,0/*ElemenDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                               vGstPadre,vGstElem;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                               COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                               vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        IF PKG_Mago_utl.isMagoDgf THEN
            PKG_Meteo.GetGeneratori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,1/*ElemenDisconnessi*/);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                                   vGstPadre,vGstElem;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO GTTD_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                                   COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                                   vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;


     OPEN pRefCurs FOR SELECT COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                              COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                              SUM(POTENZA_INSTALLATA) POTENZA_INSTALLATA,
                              COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                              PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                              PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                         FROM GTTD_FORECAST_ELEMENTS G
                        INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                        INNER JOIN RAGGRUPPAMENTO_FONTI F ON F.COD_RAGGR_FONTE = G.COD_TIPO_FONTE
                        INNER JOIN TIPI_RETE R USING(COD_TIPO_RETE)
                        INNER JOIN TIPI_CLIENTE P ON P.COD_TIPO_CLIENTE = G.COD_TIPO_PRODUTTORE
                        INNER JOIN (SELECT ALF1 COD_TIPO_ELEMENTO
                                      FROM GTTD_VALORI_TEMP
                                     WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey) X ON X.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                        WHERE
                        --COD_CITTA IS NOT NULL AND
                        BITAND(vIdFonti,F.ID_RAGGR_FONTE) = F.ID_RAGGR_FONTE
                          AND BITAND(vIdReti,R.ID_RETE) = R.ID_RETE
                          AND BITAND(vIdProd,P.ID_CLIENTE) = P.ID_CLIENTE
                        GROUP BY COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                                 COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                                 COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                                 PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                 PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                        ORDER BY COD_GEST_ELEMENTO_PADRE,E.COD_GEST_ELEMENTO;


   PKG_Logs.TraceLog('Eseguito GetElementForRES - '||PKG_Mago_utl.StdOutDate(pData)||
                                       '   TipiRete='||pTipologiaRete||
                                          '   Fonti='||pFonte||
                                       '   TipiProd='||pTipoProd||
                                       '   TipiElem='||pTipoElement,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.GetElementForRES'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetElementForRES;

--===============================================================================

PROCEDURE GetTrasformatoriforRES  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

    DELETE FROM GTTD_VALORI_TEMP
     WHERE TIP =  PKG_Mago_utl.gcTmpTipRetKey
        OR TIP =  PKG_Mago_utl.gcTmpTipFonKey
        OR TIP =  PKG_Mago_utl.gcTmpTipCliKey;

    PKG_Mago_utl.TrattaListaCodici(pTipologiaRete,  PKG_Mago_utl.gcTmpTipRetKey, vFlgNull);
    PKG_Mago_utl.TrattaListaCodici(pTipoProd,       PKG_Mago_utl.gcTmpTipCliKey, vFlgNull);
    PKG_Mago_utl.TrattaListaCodici(pFonte,          PKG_Mago_utl.gcTmpTipFonKey, vFlgNull);



    IF pDisconnect = 0 THEN
        OPEN pRefCurs FOR
            SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,
                   NVL(COD_CITTA,PKG_METEO.GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(COD_ELEMENTO_TRASFORMATORE),pData)) COD_CITTA,
                   COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                   PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                   PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
              FROM (SELECT PKG_Elementi.GetElementoPadre (A.COD_ELEMENTO,PKG_Mago_utl.gcTrasformMtBt,pData,
                                                          PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale) COD_ELEMENTO_TRASFORMATORE,
                           COD_TIPO_FONTE FONTE, POTENZA_INSTALLATA,
                           CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                WHEN pTipoGeo = 'P' THEN cod_geo
                                WHEN pTipoGeo = 'A' THEN cod_geo_a
                           END cod_citta,
                           COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                      FROM (SELECT ELE.COD_ELEMENTO,
                                    tfonte.COD_RAGGR_FONTE COD_TIPO_FONTE  --RES restituiesco il Raggruppamento
                                    ,NVL(TR.valore, 0) POTENZA_INSTALLATA,ELE.COD_TIPO_ELEMENTO,
                                   ELE.COD_TIPO_CLIENTE, ELE.COD_TIPO_RETE
                              FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                           NVL(COD_TIPO_FONTE, PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                           --NVL(COD_TIPO_CLIENTE, PKG_Mago_utl.gcClientePrdNonDeterm) COD_TIPO_CLIENTE   --MAGO-590  Definire i TR MT/BT come "autoproduttori"
                                           NVL(COD_TIPO_CLIENTE, PKG_Mago_utl.gcClienteAutoProd) COD_TIPO_CLIENTE
                                      FROM (SELECT ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                   PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                              FROM ELEMENTI_DEF DEF INNER JOIN ELEMENTI ELE ON (ELE.cod_elemento = DEF.cod_elemento)
                                             WHERE ELE.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                             GROUP BY ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente, 'B'
                                           ) ELE
                                    ) ELE
                              LEFT OUTER JOIN (SELECT TR.*, MIS.valore, MIS.data_attivazione, MIS.data_disattivazione
                                                 FROM TRATTAMENTO_ELEMENTI TR
                                                INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                              AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata)
                                              ) TR ON (    TR.cod_elemento = ELE.cod_elemento
                                                       AND TR.cod_tipo_fonte = ELE.cod_tipo_fonte
                                                       AND TR.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             INNER JOIN (SELECT ALF1 COD_TIPO_RETE
                                           FROM GTTD_VALORI_TEMP
                                          WHERE TIP = PKG_Mago_utl.gcTmpTipRetKey
                                        ) trete ON (trete.cod_tipo_rete = ELE.cod_tipo_rete)
                             INNER JOIN (SELECT COD_RAGGR_FONTE,COD_TIPO_FONTE
                                           FROM GTTD_VALORI_TEMP
                                          INNER JOIN TIPO_FONTI ON ALF1 = COD_RAGGR_FONTE
                                          WHERE TIP = PKG_Mago_utl.gcTmpTipFonKey
                                         -- GROUP BY COD_TIPO_FONTE
                                         ) tfonte ON (tfonte.cod_tipo_fonte = ELE.cod_tipo_fonte)
                             INNER JOIN (SELECT ALF1 COD_TIPO_CLIENTE
                                           FROM GTTD_VALORI_TEMP
                                          WHERE TIP = PKG_Mago_utl.gcTmpTipCliKey
                                        ) tcli ON (tcli.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             WHERE (  TR.cod_trattamento_elem IS NOT NULL
                                    OR pFlagPI = 0)
                               AND pData BETWEEN NVL (TR.DATA_ATTIVAZIONE, TO_DATE ('01011900', 'ddmmyyyy'))
                                             AND NVL (TR.DATA_DISATTIVAZIONE, TO_DATE ('01013000', 'ddmmyyyy'))
                           ) A
                      LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                           PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale)
                      LEFT OUTER JOIN METEO_REL_ISTAT I ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO, INSTR(B.COD_GEST_ELEMENTO,PKG_Mago_utl.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                     INNER JOIN (SELECT COD_ELEMENTO,ROUND(COORDINATA_Y,7) LATITUDINE,ROUND(COORDINATA_X,7) LONGITUDINE,cod_geo,cod_geo_a
                                   FROM ELEMENTI_DEF
                                 WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcSbarraCabSec,pData,PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale)
                   ) A
              LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                       AND B.COD_TIPO_FONTE = A.FONTE
                                                       AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE)
                        ;
   ELSE
        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazGEO organizzazione
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,COD_CITTA,COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                FROM (SELECT TR.cod_elemento_trasformatore,COD_TIPO_FONTE FONTE,POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN c.cod_geo
                                  WHEN pTipoGeo ='A' THEN c.cod_geo_a
                             END cod_citta,COD_TIPO_CLIENTE,COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                        FROM (SELECT ele.COD_ELEMENTO,ele.COD_TIPO_FONTE,NVL(tr.valore,0) POTENZA_INSTALLATA,ele.COD_TIPO_ELEMENTO,ele.COD_TIPO_CLIENTE,ele.COD_TIPO_RETE
                                FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                             NVL(COD_TIPO_FONTE,PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                             NVL(COD_TIPO_CLIENTE,PKG_Mago_utl.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                        FROM (SELECT ele.cod_elemento, DEF.cod_tipo_fonte,
                                                     ele.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                     PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                                FROM ELEMENTI_DEF DEF
                                              INNER JOIN ELEMENTI ELE ON (ele.cod_elemento = DEF.cod_elemento)
                                              WHERE ele.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                              GROUP BY ele.cod_elemento, DEF.cod_tipo_fonte, ele.cod_tipo_elemento, DEF.cod_tipo_cliente , 'B'
                                             ) ELE
                                     ) ELE
                                LEFT OUTER JOIN (SELECT tr.* , MIS.valore, MIS.data_attivazione, MIS.data_disattivazione,
                                                        NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                   FROM TRATTAMENTO_ELEMENTI TR
                                                  INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                               AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata
                                                                                               )
                                                  WHERE MIS.valore != 0
                                                ) TR ON (    TR.cod_elemento = ele.cod_elemento
                                                         AND tr.cod_tipo_fonte = ele.cod_tipo_fonte
                                                         AND tr.cod_tipo_cliente = ele.cod_tipo_cliente )
                               INNER JOIN (SELECT ALF1 COD_TIPO_RETE
                                             FROM GTTD_VALORI_TEMP WHERE TIP =  PKG_Mago_utl.gcTmpTipRetKey
                                          ) trete ON (trete.cod_tipo_rete = ele.cod_tipo_rete)
                               INNER JOIN (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE
                                             FROM GTTD_VALORI_TEMP
                                            INNER JOIN TIPO_FONTI ON ALF1 = COD_RAGGR_FONTE
                                            WHERE TIP =  PKG_Mago_utl.gcTmpTipFonKey
                                            GROUP BY COD_RAGGR_FONTE
                                          ) tfonte ON (tfonte.cod_tipo_fonte = ele.cod_tipo_fonte)
                               INNER JOIN (SELECT ALF1 COD_TIPO_CLIENTE
                                             FROM GTTD_VALORI_TEMP WHERE TIP =  PKG_Mago_utl.gcTmpTipCliKey
                                          ) tcli ON (tcli.cod_tipo_cliente = ele.cod_tipo_cliente)
                               WHERE (tr.cod_trattamento_elem IS NOT NULL OR pFlagPI = 0)
                                 AND pData BETWEEN NVL(tr.data_attivazione, TO_DATE('01011900','ddmmyyyy'))
                                               AND NVL(tr.data_disattivazione_calc,TO_DATE('01013000','ddmmyyyy'))
                             ) A
                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_geo rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazGEO
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcComune
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) B ON (b.cod_elemento = A.cod_elemento)
                        LEFT OUTER JOIN METEO_REL_ISTAT I ON (I.COD_ISTAT = NVL(SUBSTR(B.cod_gest_comune,INSTR(B.cod_gest_comune,PKG_Mago_utl.cSeparatore)+1),B.cod_gest_comune))
                       INNER JOIN (SELECT ele.cod_elemento cod_elemento_trasformatore, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcTrasformMtBt
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) TR ON (tr.cod_elemento = A.cod_elemento)
                       INNER JOIN (SELECT rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs,
                                          ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE,
                                          ele.cod_geo, ele.cod_geo_a
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND ele.cod_tipo_elemento = PKG_Mago_utl.gcSbarraCabSec
                                      AND ele.cod_elemento != rel.cod_elemento
                                      AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) C ON (C.COD_ELEMENTO = A.COD_ELEMENTO)
                        ) A
                   LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                            AND B.COD_TIPO_FONTE = A.FONTE
                                                            AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE )


                              ;
   END IF;

   PKG_Logs.TraceLog('Eseguito GetTrasformatoriforRES - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.GetTrasformatoriforRES'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformatoriforRES;

-- =========================================================================================

BEGIN
    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_TRANSFER_RES',
                          pStoreOnFile => FALSE);
END PKG_TRANSFER_RES;
/

SHOW ERRORS;