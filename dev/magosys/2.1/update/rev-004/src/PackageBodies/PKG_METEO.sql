PROMPT file   PackagesBodies/PKG_METEO.sql

create or replace PACKAGE BODY PKG_METEO AS

/* ***********************************************************************************************************
   NAME:       PKG_METEO

   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      14/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.c      21/03/2012  Risso M.         Integrazione SPC relative flusso meteo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni - Aggiornamento SPC relative flusso meteo
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.g      08/08/2012  Migliaccio G.    Gestione distrib.list diversivicata per tipi di installazione
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8a       07/10/2013  Moretti C.       Calcolo citt� di default per Clienti, Generatori, Trasformatori MT/BT
                                           vedi funzione GetCitta_Cliente_TrasfMT
   1.9a.3     02/10/2014  Moretti C.       Gestione Centri satellite
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   1.9c.4     23/11/2015  Campi P.         Modidica per uso generalizzato in proc GetMeteoDistribList e GetMeteoDistribListCO
   1.10.1     09/02/2016  Moretti C.       Utilizzo Sinonimi per dabelle esterne allo schema

   1.11         09/06/2016 Roberto Z.      Modificato per MAGONAZ

   1.11.3   01/07/2016  Forno S.            Supervisione + Storicizzazione della tabella FORECAST_PARAMETRI
   1.11.3   01/07/2016  Forno S.            Supervisione + Gestione periodi addestramento parametri modello
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione Reset Forecast Parameter
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione per dettaglio parametri
   1.11.3   01/08/2016  Forno S.            Supervisione + Update parametri globali dell'algoritmo
   1.11.3   01/08/2016  Forno S.            Supervisione + Get parametri globali dell'algoritmo
   1.11.3   12/08/2016  Forno S.            Supervisione + Get Periodi di addestramento parametri
   1.11.3   12/08/2016  Forno S.            Supervisione + Get_ListParameterChange
   1.11.3   12/09/2016  Forno S.            Supervisione + Get_InfoTableParameter (numero addestramento + numero cambiamenti)
   1.11.3   12/09/2016  Forno S.            Supervisione + Modificata SetForecastParameter per caso Eolico

   2.0.3    25/10/2016  Forno S.            MAGO-613 - GetAnagElementForecastParam - Fix campo Lat. Long.
   2.0.3    25/10/2016  Forno S.            MAGO-620 - GetAnagElementForecastParam- Fix campo Rete
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - storicizzo la FORECAST_PARAM_GLOBAL solo se almeno un valore � cambiato
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - Fix recupero dati tappo finale
   2.0.4    28/10/2016  Forno S.            MAGO-633    Get_InfoTableParameter
   2.0.4    28/10/2016  Forno S.            MAGO-636    Get_ListParameterChange
   2.0.5    12/12/2016  Forno S.            MAGO-696    GetDettaglioForecastParam
   2.1.0    03/03/2017  Forno S.            MAGO-590  Definire i TR MT/BT come "autoproduttori"
   2.1.2    14/03/2017  Forno S.            MAGO-590  Ulteriore modifica GetTrasformatori
   2.1.3    10/05/2017  Forno               MAGO-1068 - Reset parametri con 2 fornitori meteo presenti in tabella    -- Spec. Version 2.1.3
   2.1.3    25/05/2017  Frosini             MAGO-1068 - KPI - Reset parametri in ambiente con 2 fornitori meteo
   2.1.3    25/05/2017  Frosini             MAGO-1105 (parzialm.) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.3    25/05/2017  Frosini             MAGO-1112 - correzioni recupero righe per elementi con fonte "mista" in lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1105 (completo) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1132 - corretta gestione RestForecastParametri eolici
   -- Spec. Version 2.1.4
   2.1.4    21/06/2017  Frosini             MAGO-1046 - modificata GetforecastElements e GetProduttori/Generatori/Trasformatori per gestione corretta disconnssi
     NOTES:

*********************************************************************************************************** */

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.1.4
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Private

********************************************************************************************************** */

gv_RegExpSepara CONSTANT VARCHAR2(10) := '[^\|]+';

function gRegExpSepara RETURN VARCHAR2 IS BEGIN RETURN gv_RegExpSepara; END;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS


/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetCitta_Cliente_TrasfMT(pCodGest      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pData         IN DATE) RETURN METEO_REL_ISTAT.COD_CITTA%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce la citta' del cliente / generatore / trasformatore MT/BT
    Utilizza SAR_ADMIN e AUI per risalire al comune opportuno
    Non fa nulla per DGF (non c'e' AUI)
-----------------------------------------------------------------------------------------------------------*/
    vCodGest  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodCitta METEO_REL_ISTAT.COD_CITTA%TYPE := NULL;
    vTipElem  TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := NULL;
BEGIN

    IF NOT PKG_Mago.isMagoDgf THEN

        BEGIN

            SELECT COD_TIPO_ELEMENTO
                  INTO vTipElem
                  FROM ELEMENTI
                 WHERE COD_GEST_ELEMENTO = pCodGest;

            IF vTipElem NOT IN (PKG_Mago.gcTrasformMtBt,
                                PKG_Mago.gcTrasformMtBtDett,
                                PKG_Mago.gcClienteAT,
                                PKG_Mago.gcClienteMT,
                                PKG_Mago.gcClienteBT,
                                PKG_Mago.gcGeneratoreAT,
                                PKG_Mago.gcGeneratoreMT,
                                PKG_Mago.gcGeneratoreBT
                               ) THEN
                RAISE NO_DATA_FOUND;
            END IF;

            IF vTipElem IN (PKG_Mago.gcTrasformMtBtDett,
                            PKG_Mago.gcGeneratoreAT,
                            PKG_Mago.gcGeneratoreMT,
                            PKG_Mago.gcGeneratoreBT
                           ) THEN
                SELECT ID_ELEMENTO -- recupera il codice gestionale del padre del genaratore
                  INTO vCodGest
                  FROM ELEMENTI E
                 INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                          AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                        AND D.DATA_DISATTIVAZIONE
                 WHERE E.COD_GEST_ELEMENTO = vCodGest;
            ELSE
                vCodGest := pCodGest;
            END IF;

            EXECUTE IMMEDIATE 'SELECT MI.COD_CITTA '                                                                       ||
                                'FROM ELEMENTI CE '                                                                        ||
                               'INNER JOIN ELEMENTI_DEF CD ON CD.COD_ELEMENTO = CE.COD_ELEMENTO '                          ||
                                                         'AND :dt BETWEEN CD.DATA_ATTIVAZIONE AND CD.DATA_DISATTIVAZIONE ' ||
                               'INNER JOIN ELEMENTI SE     ON SE.COD_GEST_ELEMENTO = CD.ID_ELEMENTO '                      ||
                               'INNER JOIN ELEMENTI_DEF SD ON SD.COD_ELEMENTO = SE.COD_ELEMENTO '                          ||
                                                         'AND :dt BETWEEN SD.DATA_ATTIVAZIONE AND SD.DATA_DISATTIVAZIONE ' ||
                               'INNER JOIN AUI_NODI_TLC AU ON AU.COD_ORG_NODO = SUBSTR(SD.ID_ELEMENTO,1,4) '               ||
                                                         'AND AU.SER_NODO     = SUBSTR(SD.ID_ELEMENTO,5,1) '               ||
                                                         'AND AU.NUM_NODO     = SUBSTR(SD.ID_ELEMENTO,6,6) '               ||
                                                         'AND AU.TRATTAMENTO  = 0 '                                        ||
                                                         'AND AU.STATO        = ''E'' '                                    ||
                               'INNER JOIN SAR_REL_CFT_COMUNI CC ON CC.CODIFICA_COMPLETA_CFT = AU.COD_CFT '                ||
                               'INNER JOIN METEO_REL_ISTAT MI ON mi.COD_ISTAT = CC.COD_ISTAT_COMUNE '                      ||
                               'WHERE CE.COD_GEST_ELEMENTO = :gest '
                          INTO vCodCitta
                         USING pData, pData, vCodGest;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vCodCitta := NULL;
        END;
    END IF;
    RETURN vCodCitta;
END GetCitta_Cliente_TrasfMT;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetCodiciMeteo    (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce la lista dei codici meteo associati ai comuni presenti nel Centro operativo
-----------------------------------------------------------------------------------------------------------*/
BEGIN
   OPEN pRefCurs FOR   SELECT NOME, COD_CITTA, COD_ISTAT
                         FROM (SELECT DISTINCT COD_ELEMENTO_PADRE,
                                        SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1) COD_ISTAT
                                 FROM REL_ELEMENTI_GEO
                                INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_PADRE
                                WHERE SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                  AND COD_TIPO_ELEMENTO = PKG_Mago.gcComune
                              )
                        INNER JOIN METEO_REL_ISTAT M USING(COD_ISTAT)
                        ORDER BY COD_ISTAT;
   PKG_Logs.TraceLog('Eseguito GetCodiciMeteo',PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetCodiciMeteo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetCodiciMeteo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2,
                                pFonte          IN VARCHAR2,
                                pTipoProd       IN VARCHAR2,
                                pTipoElement    IN VARCHAR2,
                                pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pFlagDisconnect IN NUMBER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vNum        INTEGER;
    vFlgNull    NUMBER(1) := -1;

    vIdFonti    INTEGER := PKG_Mago_utl.GetIdTipoFonti(pFonte);
    vIdReti     INTEGER := PKG_Mago_utl.GetIdTipoReti(pTipologiaRete);
    vIdProd     INTEGER := PKG_Mago_utl.GetIdTipoClienti(pTipoProd);

    vElePadre   ELEMENTI.COD_ELEMENTO%TYPE;
    vGstPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vGstElem    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
    vFonte      TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
    vPotenza    ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
    vCitta      VARCHAR2(30);--METEO_REL_ISTAT.COD_CITTA%TYPE;
    vTipProd    TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
    vTipRet     TIPI_RETE.COD_TIPO_RETE%TYPE;
    vLat        NUMBER;
    vLon        NUMBER;
    vP01        FORECAST_PARAMETRI.PARAMETRO1%TYPE;
    vP02        FORECAST_PARAMETRI.PARAMETRO2%TYPE;
    vP03        FORECAST_PARAMETRI.PARAMETRO3%TYPE;
    vP04        FORECAST_PARAMETRI.PARAMETRO4%TYPE;
    vP05        FORECAST_PARAMETRI.PARAMETRO5%TYPE;
    vP06        FORECAST_PARAMETRI.PARAMETRO6%TYPE;
    vP07        FORECAST_PARAMETRI.PARAMETRO7%TYPE;
    vP08        FORECAST_PARAMETRI.PARAMETRO8%TYPE;
    vP09        FORECAST_PARAMETRI.PARAMETRO9%TYPE;
    vP10        FORECAST_PARAMETRI.PARAMETRO10%TYPE;
    vDT         FORECAST_PARAMETRI.DATA_ULTIMO_AGG%TYPE;

BEGIN

    DELETE FROM gttd_FORECAST_ELEMENTS;

    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcClienteAT,PKG_Mago_utl.gcClienteMT,PKG_Mago_utl.gcClienteBT);

     IF vNum > 0 THEN
        PKG_METEO.GetProduttori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pFlagDisconnect/*ElemenNonDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        
        
        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN
            PKG_METEO.GetProduttori2(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo);
            LOOP
               FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;



    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcTrasformMtBt);
       
     IF vNum > 0 THEN
        PKG_METEO.GetTrasformatori(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,pFlagDisconnect /*ElemenNonDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;

           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        
        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN
            PKG_METEO.GetTrasformatori2(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;

    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcGeneratoreAT,PKG_Mago_utl.gcGeneratoreMT,PKG_Mago_utl.gcGeneratoreBT);
     
     IF vNum > 0 THEN
        PKG_METEO.GetGeneratori(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pFlagDisconnect/*ElemenDisconnessi*/);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                               vGstPadre,vGstElem;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                               COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                               vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;
        
        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN

            PKG_METEO.GetGeneratori2(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                                   vGstPadre,vGstElem;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                                   COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                                   vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;


     OPEN pRefCurs FOR SELECT COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                              COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                              SUM(POTENZA_INSTALLATA) POTENZA_INSTALLATA,
                              COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                              PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                              PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                         FROM gttd_FORECAST_ELEMENTS G
                        INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                        INNER JOIN RAGGRUPPAMENTO_FONTI F ON F.COD_RAGGR_FONTE = G.COD_TIPO_FONTE
                        INNER JOIN TIPI_RETE R USING(COD_TIPO_RETE)
                        INNER JOIN TIPI_CLIENTE P ON P.COD_TIPO_CLIENTE = G.COD_TIPO_PRODUTTORE
                        INNER JOIN (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) COD_TIPO_ELEMENTO from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null ) X ON X.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                        WHERE COD_CITTA IS NOT NULL
                          AND BITAND(vIdFonti,F.ID_RAGGR_FONTE) = F.ID_RAGGR_FONTE
                          AND BITAND(vIdReti,R.ID_RETE) = R.ID_RETE
                          AND BITAND(vIdProd,P.ID_CLIENTE) = P.ID_CLIENTE
                        GROUP BY COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                                 COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                                 COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                                 PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                 PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                        ORDER BY COD_GEST_ELEMENTO_PADRE,E.COD_GEST_ELEMENTO;


   PKG_Logs.TraceLog('Eseguito GetElementForecast - '||PKG_Mago_utl.StdOutDate(pData)||
                                       '   TipiRete='||pTipologiaRete||
                                          '   Fonti='||pFonte||
                                       '   TipiProd='||pTipoProd||
                                       '   TipiElem='||pTipoElement,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetElementForecast'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetElementForecast;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProduttori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pDisconnect     IN NUMBER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

       OPEN pRefCurs FOR
              SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.FONTE,A.POTENZA_INSTALLATA,
                     NVL(A.COD_CITTA,PKG_METEO.GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(A.COD_ELEMENTO_CLIENTE),pData)) COD_CITTA,
                     A.COD_TIPO_CLIENTE,A.COD_TIPO_RETE,A.LATITUDINE,A.LONGITUDINE,
                     B.PARAMETRO1,B.PARAMETRO2,B.PARAMETRO3,B.PARAMETRO4,B.PARAMETRO5,
                     B.PARAMETRO6,B.PARAMETRO7,B.PARAMETRO8,B.PARAMETRO9,B.PARAMETRO10,B.DATA_ULTIMO_AGG
                FROM (SELECT PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,TIP_ELE_GEN,pData,
                                                           PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,
                                                           pDisconnect) COD_ELEMENTO_CLIENTE,
                             A.COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE FONTE,A.POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN cod_geo
                                  WHEN pTipoGeo = 'A' THEN cod_geo_a
                             END COD_CITTA,
                             A.COD_TIPO_CLIENTE, A.COD_TIPO_RETE,
                             NVL(C.LATITUDINE,A.LATITUDINE)   LATITUDINE,
                             NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE
                                FROM (SELECT ROUND(DEF.COORDINATA_Y,7) LATITUDINE, ROUND(DEF.COORDINATA_X,7) LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,TR.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             CASE TR.COD_TIPO_ELEMENTO
                                                  WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Mago.gcClienteMT
                                                  WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Mago.gcClienteBT
                                                  WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Mago.gcClienteAT
                                              END TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.COD_ELEMENTO = TR.COD_ELEMENTO)
                                       INNER JOIN  (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F ON (F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                        ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago.gcClienteMT,
																	  PKG_Mago.gcClienteBT,
																	  PKG_Mago.gcClienteAT,
																	  PKG_Mago.gcGeneratoreMT,
                                                                      PKG_Mago.gcGeneratoreBT,
                                                                      PKG_Mago.gcGeneratoreAT)
                                         AND sysdate BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE
                                     )
                               ) A
                          LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcComune,pData,
                                                                                                                   PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,
                                                                                                                   pDisconnect)
                          LEFT OUTER JOIN METEO_REL_ISTAT I
                               ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO,INSTR(B.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                          LEFT OUTER JOIN (SELECT COD_ELEMENTO, ROUND(COORDINATA_Y,7) LATITUDINE, ROUND(COORDINATA_X,7) LONGITUDINE , cod_geo , cod_geo_a
                                             FROM ELEMENTI_DEF
                                            WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                          ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcSbarraCabSec,pData,
                                                                                                PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,
                                                                                                pDisconnect)
                      ) A
                 INNER JOIN ELEMENTI_DEF E ON E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                                          AND pData BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                                          AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE
                  LEFT OUTER JOIN FORECAST_PARAMETRI B ON B.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE AND B.COD_TIPO_FONTE = A.FONTE  AND COD_TIPO_COORD = pTipoGeo
                 INNER JOIN TIPI_CLIENTE TCL ON (TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE)
                 WHERE TCL.FORNITORE = PKG_Mago.gcON;

   PKG_Logs.TraceLog('Eseguito GetProduttori - '||PKG_Mago.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetProduttori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetProduttori;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProduttori2      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C') AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

   OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazGEO organizzazione
                                       FROM GERARCHIA_GEO rel
                                     )
              SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.FONTE,A.POTENZA_INSTALLATA,A.COD_CITTA,
                     A.COD_TIPO_CLIENTE,A.COD_TIPO_RETE,A.LATITUDINE,A.LONGITUDINE,
                     B.PARAMETRO1,B.PARAMETRO2,B.PARAMETRO3,B.PARAMETRO4,B.PARAMETRO5,
                     B.PARAMETRO6,B.PARAMETRO7,B.PARAMETRO8,B.PARAMETRO9,B.PARAMETRO10,B.DATA_ULTIMO_AGG
                FROM (SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE FONTE,A.POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN cod_geo
                                  WHEN pTipoGeo = 'A' THEN cod_geo_a
                             END cod_citta,
	                         COD_TIPO_CLIENTE,COD_TIPO_RETE,
                             NVL(C.LATITUDINE,A.LATITUDINE)   LATITUDINE,
                             NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
	                    FROM (SELECT A.COD_ELEMENTO,A.COD_ELEMENTO COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE,A.COD_TIPO_FONTE,
	                                 A.POTENZA_INSTALLATA,A.COD_TIPO_CLIENTE,A.COD_ELEMENTO_CLIENTE,A.COD_TIPO_RETE,A.TIP_ELE_GEN,A.LATITUDINE,A.LONGITUDINE
	                            FROM (SELECT ROUND(DEF.COORDINATA_Y,7) LATITUDINE,
                                             ROUND(DEF.COORDINATA_X,7) LONGITUDINE,
	                                         TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,TR.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
	                                         MIS.VALORE POTENZA_INSTALLATA,
	                                         CASE TR.COD_TIPO_ELEMENTO
	                                            WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Mago.gcClienteMT
	                                            WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Mago.gcClienteBT
	                                            WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Mago.gcClienteAT
	                                         END TIP_ELE_GEN,COD_ELEMENTO_CLIENTE
	                                    FROM TRATTAMENTO_ELEMENTI TR
	                                   INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_cliente, ele.cod_elemento cod_elemento_cliente,ele.cod_tipo_elemento, rel.cod_elemento
                                                     FROM (SELECT *
                                                             FROM wgerarchia_ele rel
                                                            WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                                          ) rel
                                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                                   WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                     AND rel.data_disattivazione != rel.data_disattivazione_calc
                                                  ) CLI ON (    tr.cod_elemento = cli.cod_elemento
                                                            AND cli.cod_tipo_elemento = CASE tr.cod_tipo_elemento
                                                                                           WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Mago.gcClienteMT
                                                                                           WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Mago.gcClienteAT
                                                                                           WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Mago.gcClienteBT
                                                                                        END
                                                           )
	                                   INNER JOIN (SELECT MIS.*,
	                                                      NVL(lead(MIS.data_attivazione-1/(24*60*60))
                                                                   OVER (PARTITION BY MIS.cod_trattamento_elem
                                                                             ORDER BY MIS.data_attivazione), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
	                                                 FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
	                                   INNER JOIN ELEMENTI_DEF DEF ON (DEF.COD_ELEMENTO = TR.COD_ELEMENTO)
	                                   INNER JOIN (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F  ON (F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE)
	                                   INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
	                                   INNER JOIN (
                                             SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
	                                              ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
	                                   WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago.gcClienteMT,
																		PKG_Mago.gcClienteBT,
																		PKG_Mago.gcClienteAT,
																		PKG_Mago.gcGeneratoreMT,
	                                                                  PKG_Mago.gcGeneratoreBT,
	                                                                  PKG_Mago.gcGeneratoreAT)
	                                     AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
	                                     AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
	                                 ) A
                             ) A
                       INNER JOIN
                             (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                FROM (SELECT *
                                        FROM wgerarchia_geo rel
                                        WHERE organizzazione = PKG_Mago.gcOrganizzazGEO
                                     ) rel
                               INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                               WHERE 
                                  pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                 AND 
                                 cod_tipo_elemento = PKG_Mago.gcComune
                                 AND rel.data_disattivazione != rel.data_disattivazione_calc
                             ) B ON (b.cod_elemento = A.cod_elemento)
                        LEFT OUTER JOIN METEO_REL_ISTAT I
                             ON I.COD_ISTAT = NVL(SUBSTR(B.cod_gest_comune,INSTR(B.cod_gest_comune,PKG_Mago.cSeparatore)+1),B.cod_gest_comune)
                       INNER JOIN
                             (SELECT rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs,
                                     ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE,
                                     ele.cod_geo, ele.cod_geo_a
                                FROM (SELECT *
                                        FROM wgerarchia_ele rel
                                       WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                     ) rel
                               INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                               WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                 AND ele.cod_tipo_elemento = PKG_Mago.gcSbarraCabSec
                                 AND ele.cod_elemento != rel.cod_elemento
                                 AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                 AND rel.data_disattivazione != rel.data_disattivazione_calc
                             ) C ON (C.COD_ELEMENTO = A.COD_ELEMENTO)
                     ) A
               INNER JOIN ELEMENTI_DEF E ON E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                                        AND pData BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                                        AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE
                LEFT OUTER JOIN FORECAST_PARAMETRI B ON B.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE AND B.COD_TIPO_FONTE = A.FONTE  AND COD_TIPO_COORD = pTipoGeo
               INNER JOIN TIPI_CLIENTE TCL ON (TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE)
	           WHERE TCL.FORNITORE = PKG_Mago.gcON;
   

   PKG_Logs.TraceLog('Eseguito GetProduttori2 - '||PKG_Mago.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetProduttori2'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetProduttori2;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneratori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                              pDisconnect IN NUMBER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vMagoDGF   VARCHAR2(1);
BEGIN

    vMagoDGF := CASE WHEN PKG_Mago.IsMagoDGF THEN 'Y' ELSE 'N' END;

     OPEN pRefCurs FOR
              SELECT COD_ELEMENTO_GENERATORE,FONTE,POTENZA_INSTALLATA,
                     NVL(COD_CITTA,PKG_METEO.GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(COD_ELEMENTO_GENERATORE),pData)) COD_CITTA,
                     COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG,COD_GEST_CLIENTE,COD_GEST_GENERATORE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,I.COD_CITTA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE,NVL(C.LATITUDINE,A.LATITUDINE)LATITUDINE, NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,
                                     PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoPadre(COD_ELEMENTO,
                                                                                                CASE TIP_ELE_GEN
                                                                                                    WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Mago.gcClienteMT
                                                                                                    WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Mago.gcClienteAT
                                                                                                    WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Mago.gcClienteBT
                                                                                                END,pdata,1,1,
                                                                                                pdisconnect)) COD_GEST_CLIENTE
                                FROM (SELECT CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_Y,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_Y,7)
                                                  ELSE ROUND(DEF.COORDINATA_Y,7)
                                             END LATITUDINE,
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_X,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_X,7)
                                                  ELSE ROUND(DEF.COORDINATA_X,7)
                                             END LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*,
                                                         NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND tr.cod_tipo_misura = Pkg_MAGO.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI pgeo ON (DEF.cod_geo = pgeo.cod_geo)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI AGEO ON (DEF.cod_geo_a = ageo.cod_geo)
                                       INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) 
                                       ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (ret.cod_tipo_rete=tr.cod_tipo_rete)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreMT,
                                                                      PKG_Mago.gcGeneratoreBT,
                                                                      PKG_Mago.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                        LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcComune,pData,
                                                                                                                PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,
                                                                                                                pdisconnect)
                       LEFT OUTER JOIN METEO_REL_ISTAT I
                            ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO,INSTR(B.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                       LEFT OUTER JOIN (SELECT COD_ELEMENTO, ROUND(COORDINATA_Y,7) LATITUDINE, ROUND(COORDINATA_X,7) LONGITUDINE
                                          FROM ELEMENTI_DEF
                                         WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                       ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcSbarraCabSec,pData,
                                                                                             PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,
                                                                                             pdisconnect)
                     ) A
                LEFT OUTER JOIN FORECAST_PARAMETRI B ON COD_ELEMENTO = COD_ELEMENTO_GENERATORE  AND B.COD_TIPO_FONTE = A.FONTE AND COD_TIPO_COORD = pTipoGeo;

    PKG_Logs.TraceLog('Eseguito GetGeneratori - '||PKG_Mago.StdOutDate(pData)||
                                   '   TipiRete='||pTipologiaRete||
                                       '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetGeneratori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetGeneratori;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneratori2      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C') AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vMagoDGF   VARCHAR2(1);
BEGIN

    vMagoDGF := CASE WHEN PKG_Mago.IsMagoDGF THEN 'Y' ELSE 'N' END;

        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
										     rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazGEO organizzazione
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT /*+ ORDERED USE_HASH(A,B,C)*/COD_ELEMENTO_GENERATORE,FONTE,POTENZA_INSTALLATA,COD_CITTA,
                     COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG,COD_GEST_CLIENTE,COD_GEST_GENERATORE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,B.COD_CITTA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE,NVL(C.LATITUDINE,A.LATITUDINE)LATITUDINE, NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE,
                             PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                             PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,COD_GEST_CLIENTE,
                                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                                FROM (SELECT /*+ USE_HASH(TR,MIS,DEF,PGEO,AGEO,CLI,F,RET,PR,B ) */
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_Y,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_Y,7)
                                                  ELSE ROUND(DEF.COORDINATA_Y,7)
                                             END LATITUDINE,
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_X,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_X,7)
                                                  ELSE ROUND(DEF.COORDINATA_X,7)
                                             END  LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,cli.cod_gest_cliente,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN,
                                             PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                                             PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*
                                                         ,NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND TR.cod_tipo_misura = PKG_MAGO.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI pgeo ON (DEF.cod_geo = pgeo.cod_geo)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI AGEO ON (DEF.cod_geo_a = ageo.cod_geo)
                                        LEFT OUTER JOIN FORECAST_PARAMETRI B ON b.COD_ELEMENTO = tr.COD_ELEMENTO  AND tr.COD_TIPO_FONTE = b.cod_tipo_FONTE AND COD_TIPO_COORD = pTipoGeo
                                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_cliente, ele.cod_elemento cod_elemento_cliente,ele.cod_tipo_elemento, rel.cod_elemento
                                                     FROM (SELECT *
                                                             FROM wgerarchia_ele rel
                                                            WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                                          ) rel
                                                   INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                                   WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                     AND rel.data_disattivazione != rel.data_disattivazione_calc
                                                  ) CLI ON (    tr.cod_elemento = cli.cod_elemento
                                                            AND cli.cod_tipo_elemento = CASE tr.cod_tipo_elemento
                                                                                             WHEN PKG_Mago.gcGeneratoreMT THEN PKG_Mago.gcClienteMT
                                                                                             WHEN PKG_Mago.gcGeneratoreAT THEN PKG_Mago.gcClienteAT
                                                                                             WHEN PKG_Mago.gcGeneratoreBT THEN PKG_Mago.gcClienteBT
                                                                                        END
                                                           )
                                       INNER JOIN (SELECT /*+ USE_NL(tmp tp) */
                                                          cod_tipo_fonte, cod_raggr_fonte
                                                     FROM gttd_VALORI_TEMP tmp
                                                    INNER JOIN TIPO_FONTI tp ON alf1 = cod_raggr_fonte
                                                    WHERE TIP =  PKG_Mago.gcTmpTipFonKey
                                                  ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (ret.cod_tipo_rete=tr.cod_tipo_rete)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago.gcGeneratoreMT,
                                                                      PKG_Mago.gcGeneratoreBT,
                                                                      PKG_Mago.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                       INNER JOIN (SELECT /*+ USE_HASH(REL,ELE,I) */
                                         ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                         , i.cod_citta
                                    FROM
                                        (SELECT *
                                           FROM wgerarchia_geo rel
                                          WHERE organizzazione = PKG_Mago.gcOrganizzazGEO
                                        ) rel
                                        INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                         LEFT OUTER JOIN METEO_REL_ISTAT I
                                        ON I.COD_ISTAT = NVL(SUBSTR(ele.cod_gest_elemento,INSTR(ele.cod_gest_elemento,PKG_Mago.cSeparatore)+1),ele.cod_gest_elemento)
                                       WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                         AND cod_tipo_elemento = PKG_Mago.gcComune
                                         AND rel.data_disattivazione != rel.data_disattivazione_calc
                                         ) B ON B.COD_ELEMENTO = A.COD_ELEMENTO
                       INNER JOIN (SELECT /*+ USE_HASH(REL,ELE) */
                                         rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs
                                         ,ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE
                                         ,ele.cod_geo, ele.cod_geo_a
                                          FROM
                                              (SELECT *
                                                 FROM wgerarchia_ele rel
                                                WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                              ) rel
                                         INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                         WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                           AND ele.cod_tipo_elemento = PKG_Mago.gcSbarraCabSec
                                           AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                           AND rel.data_disattivazione != rel.data_disattivazione_calc
                                        ) C ON C.COD_ELEMENTO = A.COD_ELEMENTO
                     ) A;
 
    PKG_Logs.TraceLog('Eseguito GetGeneratori2 - '||PKG_Mago.StdOutDate(pData)||
                                   '   TipiRete='||pTipologiaRete||
                                       '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetGeneratori2'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetGeneratori2;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformatori  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

        OPEN pRefCurs FOR
            SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,
                   NVL(COD_CITTA,PKG_METEO.GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(COD_ELEMENTO_TRASFORMATORE),pData)) COD_CITTA,
                   COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                   PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                   PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
              FROM (SELECT PKG_Elementi.GetElementoPadre (A.COD_ELEMENTO,PKG_Mago.gcTrasformMtBt,pData,
                                                          PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,
                                                          pDisconnect) COD_ELEMENTO_TRASFORMATORE,
                           COD_TIPO_FONTE FONTE, POTENZA_INSTALLATA,
                           CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                WHEN pTipoGeo = 'P' THEN cod_geo
                                WHEN pTipoGeo = 'A' THEN cod_geo_a
                           END cod_citta,
                           COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                      FROM (SELECT ELE.COD_ELEMENTO,ELE.COD_TIPO_FONTE,NVL(TR.valore, 0) POTENZA_INSTALLATA,ELE.COD_TIPO_ELEMENTO,
                                   ELE.COD_TIPO_CLIENTE, ELE.COD_TIPO_RETE
                              FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                           NVL(COD_TIPO_FONTE, PKG_Mago.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                           NVL(COD_TIPO_CLIENTE, pkg_mago.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                      FROM (SELECT ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                   PKG_Mago.gcTipReteBT cod_tipo_rete
                                              FROM ELEMENTI_DEF DEF INNER JOIN ELEMENTI ELE ON (ELE.cod_elemento = DEF.cod_elemento)
                                             WHERE ELE.COD_TIPO_ELEMENTO IN (PKG_Mago.gcTrasformMtBtDett)
                                             GROUP BY ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente, 'B'
                                           ) ELE
                                    ) ELE
                              LEFT OUTER JOIN (SELECT TR.*, MIS.valore, MIS.data_attivazione, MIS.data_disattivazione
                                                 FROM TRATTAMENTO_ELEMENTI TR
                                                INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                              AND TR.COD_TIPO_MISURA = PKG_MAGO.gcPotenzaInstallata)
                                              ) TR ON (    TR.cod_elemento = ELE.cod_elemento
                                                       AND TR.cod_tipo_fonte = ELE.cod_tipo_fonte
                                                       AND TR.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        ) trete ON (trete.cod_tipo_rete = ELE.cod_tipo_rete)
                             INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        )) tfonte ON (tfonte.cod_tipo_fonte = ELE.cod_tipo_fonte)
                             INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                        ) tcli ON (tcli.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             WHERE (  TR.cod_trattamento_elem IS NOT NULL
                                    OR pFlagPI = 0)
                               AND pData BETWEEN NVL (TR.DATA_ATTIVAZIONE, TO_DATE ('01011900', 'ddmmyyyy'))
                                             AND NVL (TR.DATA_DISATTIVAZIONE, TO_DATE ('01013000', 'ddmmyyyy'))
                           ) A
                      LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcComune,pData,
                                                                                                           PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale,
                                                                                                           pDisconnect)
                      LEFT OUTER JOIN METEO_REL_ISTAT I ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO, INSTR(B.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                     INNER JOIN (SELECT COD_ELEMENTO,ROUND(COORDINATA_Y,7) LATITUDINE,ROUND(COORDINATA_X,7) LONGITUDINE,cod_geo,cod_geo_a
                                   FROM ELEMENTI_DEF
                                 WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago.gcSbarraCabSec,pData,PKG_Mago.gcOrganizzazELE,PKG_Mago.gcStatoNormale,
                                pDisconnect)
                   ) A
              LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                       AND B.COD_TIPO_FONTE = A.FONTE
                                                       AND COD_TIPO_COORD = pTipoGeo);
 

   PKG_Logs.TraceLog('Eseguito GetTrasformatori - '||PKG_Mago.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetTrasformatori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformatori;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformatori2  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago.cSeparatore || L02 || PKG_Mago.cSeparatore || L03 || PKG_Mago.cSeparatore ||
                                             L04 || PKG_Mago.cSeparatore || L05 || PKG_Mago.cSeparatore || L06 || PKG_Mago.cSeparatore ||
                                             L07 || PKG_Mago.cSeparatore || L08 || PKG_Mago.cSeparatore || L09 || PKG_Mago.cSeparatore ||
                                             L10 || PKG_Mago.cSeparatore || L11 || PKG_Mago.cSeparatore || L12 || PKG_Mago.cSeparatore ||
                                             L13 || PKG_Mago.cSeparatore || L14 || PKG_Mago.cSeparatore || L15 || PKG_Mago.cSeparatore ||
                                             L16 || PKG_Mago.cSeparatore || L17 || PKG_Mago.cSeparatore || L18 || PKG_Mago.cSeparatore ||
                                             L19 || PKG_Mago.cSeparatore || L20 || PKG_Mago.cSeparatore || L21 || PKG_Mago.cSeparatore ||
                                             L22 || PKG_Mago.cSeparatore || L23 || PKG_Mago.cSeparatore || L24 || PKG_Mago.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago.gcOrganizzazGEO organizzazione
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,COD_CITTA,COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                FROM (SELECT TR.cod_elemento_trasformatore,COD_TIPO_FONTE FONTE,POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN c.cod_geo
                                  WHEN pTipoGeo ='A' THEN c.cod_geo_a
                             END cod_citta,COD_TIPO_CLIENTE,COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                        FROM (SELECT ele.COD_ELEMENTO,ele.COD_TIPO_FONTE,NVL(tr.valore,0) POTENZA_INSTALLATA,ele.COD_TIPO_ELEMENTO,ele.COD_TIPO_CLIENTE,ele.COD_TIPO_RETE
                                FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                             NVL(COD_TIPO_FONTE,PKG_Mago.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                             NVL(COD_TIPO_CLIENTE,pkg_mago.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                        FROM (SELECT ele.cod_elemento, DEF.cod_tipo_fonte,
                                                     ele.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                     PKG_Mago.gcTipReteBT cod_tipo_rete
                                                FROM ELEMENTI_DEF DEF
                                              INNER JOIN ELEMENTI ELE ON (ele.cod_elemento = DEF.cod_elemento)
                                              WHERE ele.COD_TIPO_ELEMENTO IN (PKG_Mago.gcTrasformMtBtDett)
                                              GROUP BY ele.cod_elemento, DEF.cod_tipo_fonte, ele.cod_tipo_elemento, DEF.cod_tipo_cliente , 'B'
                                             ) ELE
                                     ) ELE
                                LEFT OUTER JOIN (SELECT tr.* , MIS.valore, MIS.data_attivazione, MIS.data_disattivazione,
                                                        NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                   FROM TRATTAMENTO_ELEMENTI TR
                                                  INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                               AND TR.COD_TIPO_MISURA = PKG_MAGO.gcPotenzaInstallata
                                                                                               )
                                                  WHERE MIS.valore != 0
                                                ) TR ON (    TR.cod_elemento = ele.cod_elemento
                                                         AND tr.cod_tipo_fonte = ele.cod_tipo_fonte
                                                         AND tr.cod_tipo_cliente = ele.cod_tipo_cliente )
                               INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        ) trete ON (trete.cod_tipo_rete = ele.cod_tipo_rete)
                               INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        )) tfonte ON (tfonte.cod_tipo_fonte = ele.cod_tipo_fonte)
                               INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                          ) tcli ON (tcli.cod_tipo_cliente = ele.cod_tipo_cliente)
                               WHERE (tr.cod_trattamento_elem IS NOT NULL OR pFlagPI = 0)
                                 AND pData BETWEEN NVL(tr.data_attivazione, TO_DATE('01011900','ddmmyyyy'))
                                               AND NVL(tr.data_disattivazione_calc,TO_DATE('01013000','ddmmyyyy'))
                             ) A
                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_geo rel
                                            WHERE organizzazione = PKG_Mago.gcOrganizzazGEO
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago.gcComune
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) B ON (b.cod_elemento = A.cod_elemento)
                        LEFT OUTER JOIN METEO_REL_ISTAT I ON (I.COD_ISTAT = NVL(SUBSTR(B.cod_gest_comune,INSTR(B.cod_gest_comune,PKG_Mago.cSeparatore)+1),B.cod_gest_comune))
                       INNER JOIN (SELECT ele.cod_elemento cod_elemento_trasformatore, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago.gcTrasformMtBt
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) TR ON (tr.cod_elemento = A.cod_elemento)
                       INNER JOIN (SELECT rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs,
                                          ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE,
                                          ele.cod_geo, ele.cod_geo_a
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago.cSeparatore || ele.cod_elemento || PKG_Mago.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND ele.cod_tipo_elemento = PKG_Mago.gcSbarraCabSec
                                      AND ele.cod_elemento != rel.cod_elemento
                                      AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) C ON (C.COD_ELEMENTO = A.COD_ELEMENTO)
                        ) A
                   LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    COD_ELEMENTO = COD_ELEMENTO_TRASFORMATORE
                                                            AND B.COD_TIPO_FONTE = A.FONTE
                                                            AND COD_TIPO_COORD = pTipoGeo);

   PKG_Logs.TraceLog('Eseguito GetTrasformatori2 - '||PKG_Mago.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetTrasformatori2'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformatori2;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pDataDa         IN DATE,
                                pDataA          IN DATE,
                                pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                pTipoMeteo      IN INTEGER,
                                pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                pCodPrev IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le previsioni meteo per le citta' ricevute
    Se ListaCitta = NULL si intende che si vogliono le previsioni di tutte le citta'
/*---------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vListaCitta PKG_UtlGlb.t_SplitTbl;
    cTipCitta   CONSTANT gttd_VALORI_TEMP.TIP%TYPE := 'CODCITTA';

BEGIN

    DELETE FROM gttd_VALORI_TEMP WHERE TIP = cTipCitta;

    IF pTipoMeteo = 0 THEN
        IF pListaCitta IS NULL THEN
            INSERT INTO gttd_VALORI_TEMP (TIP,ALF1) SELECT cTipCitta,TO_CHAR(COD_CITTA) FROM METEO_REL_ISTAT;
        ELSE
            PKG_MAgo_utl.TrattaListaCodici(pListaCitta, cTipCitta, vFlgNull);
        END IF;
        OPEN pRefCurs FOR SELECT METEO.COD_CITTA, DATA, TEMPERATURA, DESCRIZIONE, PRECIPITAZIONI, DIREZIONE_VENTO, VELOCITA_VENTO,
                                 INDICE_DI_RAFFREDDAMENTO, INDICE_DI_CALORE, UMIDITA_RELATIVA, VISIBILITA, PRESSIONE, HZERO,
                                 IRRAGGIAMENTO_SOLARE
                            FROM METEO_PREVISIONE METEO
                           INNER JOIN (SELECT TO_NUMBER(ALF1) COD_CITTA
                                         FROM gttd_VALORI_TEMP WHERE TIP = cTipCitta
                                      )TMP ON (TMP.COD_CITTA = METEO.COD_CITTA)
                           WHERE DATA BETWEEN pDataDa AND pDataA
                             AND COD_TIPO_COORD = pTipoGeo
                             AND NVL(COD_PREV_METEO,'0') = NVL(pCodPrev,'0');
    ELSE
        PKG_MAgo_utl.TrattaListaCodici(pListaCitta, cTipCitta, vFlgNull);
        OPEN pRefCurs FOR SELECT MIS.COD_ELEMENTO COD_CITTA, MIS.DATA, MIS.TEMPERATURA, NULL DESCRIZIONE, NULL PRECIPITAZIONI, MIS.DIREZIONE_VENTO, MIS.VELOCITA_VENTO,
                                 NULL INDICE_DI_RAFFREDDAMENTO, NULL INDICE_DI_CALORE, NULL UMIDITA_RELATIVA, NULL VISIBILITA, NULL PRESSIONE, NULL HZERO,
                                 MIS.IRRAGGIAMENTO_SOLARE
                            FROM
                                 (SELECT TR.COD_ELEMENTO
                                        ,MIS.DATA
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'TMP' THEN MIS.VALORE ELSE NULL END) TEMPERATURA
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'DVE' THEN MIS.VALORE ELSE NULL END) DIREZIONE_VENTO
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'VVE' THEN MIS.VALORE ELSE NULL END) VELOCITA_VENTO
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'IRR' THEN MIS.VALORE ELSE NULL END) IRRAGGIAMENTO_SOLARE
                                    FROM MISURE_ACQUISITE MIS
                                   INNER JOIN TRATTAMENTO_ELEMENTI TR ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
                                   WHERE TR.COD_TIPO_MISURA IN ('TMP','DVE','VVE', 'IRR')
                                     AND MIS.DATA BETWEEN pDataDa AND pDataA
                                   GROUP BY TR.COD_ELEMENTO ,MIS.DATA
                                 ) MIS
                           INNER JOIN ELEMENTI B ON (B.COD_ELEMENTO = MIS.COD_ELEMENTO)
                           INNER JOIN gttd_VALORI_TEMP TMP ON TMP.ALF1 = B.COD_ELEMENTO
                           WHERE  TIP = cTipCitta;
--                           INNER JOIN ELEMENTI B ON (B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(MIS.COD_ELEMENTO,PKG_Mago.gcComune,pDataA,
--                                                                                                    PKG_Mago.gcOrganizzazGEO,PKG_Mago.gcStatoNormale)
--                                                     )
--                           INNER JOIN (SELECT TO_NUMBER(ALF1) COD_CITTA, ISTAT.COD_ISTAT
--                                         FROM gttd_VALORI_TEMP TMP
--                                         INNER JOIN METEO_REL_ISTAT ISTAT
--                                           ON (  TO_NUMBER(TMP.ALF1) =  TO_NUMBER(ISTAT.COD_CITTA) )
--                                        WHERE TIP = cTipCitta
--                                      ) TMP ON (TMP.COD_ISTAT = B.COD_GEST_ELEMENTO);
    END IF;

   PKG_Logs.TraceLog('Eseguito GetMeteo - Periodo '||PKG_Mago.StdOutDate(pDataDa)||' - '||PKG_Mago.StdOutDate(pDataDa)||
                                  '   TipoMeteo='||pTipoMeteo||CHR(10)||
                                  '   ListaCitta='||pListaCitta,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetMeteo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Memorizza i parametri per il calcolo del Forecast
-----------------------------------------------------------------------------------------------------------*/
  vIns NUMBER := 0;
  vMod NUMBER := 0;
BEGIN

    IF pForecastParam.FIRST IS NOT NULL THEN
        FOR i IN pForecastParam.FIRST .. pForecastParam.LAST LOOP
            UPDATE FORECAST_PARAMETRI SET DATA_ULTIMO_AGG = pForecastParam(i).DATA_ULTIMO_AGG,
                                          PARAMETRO1      = pForecastParam(i).PARAMETRO1,
                                          PARAMETRO2      = pForecastParam(i).PARAMETRO2,
                                          PARAMETRO3      = pForecastParam(i).PARAMETRO3,
                                          PARAMETRO4      = pForecastParam(i).PARAMETRO4,
                                          PARAMETRO5      = pForecastParam(i).PARAMETRO5,
                                          PARAMETRO6      = pForecastParam(i).PARAMETRO6,
                                          PARAMETRO7      = pForecastParam(i).PARAMETRO7,
                                          PARAMETRO8      = pForecastParam(i).PARAMETRO8,
                                          PARAMETRO9      = pForecastParam(i).PARAMETRO9,
                                          PARAMETRO10     = pForecastParam(i).PARAMETRO10
             WHERE COD_ELEMENTO = pForecastParam(i).COD_ELEMENTO
               AND COD_TIPO_FONTE  = pForecastParam(i).COD_TIPO_FONTE
               AND COD_TIPO_COORD = pForecastParam(i).COD_TIPO_COORD
               AND COD_PREV_METEO = pForecastParam(i).COD_PREV_METEO;
             IF SQL%ROWCOUNT = 0 THEN
                INSERT INTO FORECAST_PARAMETRI (COD_ELEMENTO,
                                                COD_TIPO_FONTE,
                                                COD_TIPO_COORD,
                                                COD_PREV_METEO,
                                                DATA_ULTIMO_AGG,
                                                PARAMETRO1,
                                                PARAMETRO2,
                                                PARAMETRO3,
                                                PARAMETRO4,
                                                PARAMETRO5,
                                                PARAMETRO6,
                                                PARAMETRO7,
                                                PARAMETRO8,
                                                PARAMETRO9,
                                                PARAMETRO10)
                                        VALUES (pForecastParam(i).COD_ELEMENTO,
                                                pForecastParam(i).COD_TIPO_FONTE,
                                                pForecastParam(i).COD_TIPO_COORD,
                                                pForecastParam(i).COD_PREV_METEO,
                                                pForecastParam(i).DATA_ULTIMO_AGG,
                                                pForecastParam(i).PARAMETRO1,
                                                pForecastParam(i).PARAMETRO2,
                                                pForecastParam(i).PARAMETRO3,
                                                pForecastParam(i).PARAMETRO4,
                                                pForecastParam(i).PARAMETRO5,
                                                pForecastParam(i).PARAMETRO6,
                                                pForecastParam(i).PARAMETRO7,
                                                pForecastParam(i).PARAMETRO8,
                                                pForecastParam(i).PARAMETRO9,
                                                pForecastParam(i).PARAMETRO10);
                vIns := vIns + 1;
             ELSE
                vMod := vMod + 1;
             END IF;
        END LOOP;
    END IF;

    COMMIT;

    PKG_Logs.TraceLog('Eseguito SetForecastParameter - Inseriti='||vIns||'   Modificati='||vMod,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.SetForecastParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END SetForecastParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Memorizza i parametri per il calcolo del Forecast (dati eolici)
-----------------------------------------------------------------------------------------------------------*/
  vIns NUMBER := 0;
  vMod NUMBER := 0;
BEGIN

    IF pEolicParam.FIRST IS NOT NULL THEN
        FOR i IN pEolicParam.FIRST .. pEolicParam.LAST LOOP
            UPDATE FORECAST_PARAMETRI SET DATA_ULTIMO_AGG = pEolicParam(i).DATA_ULTIMO_AGG,
                                          V_CUT_IN        = pEolicParam(i).V_CUT_IN,
                                          V_CUT_OFF       = pEolicParam(i).V_CUT_OFF,
                                          V_MAX_POWER     = pEolicParam(i).V_MAX_POWER
             WHERE COD_ELEMENTO = pEolicParam(i).COD_ELEMENTO
               AND COD_TIPO_FONTE = PKG_Mago.gcRaggrFonteEolica
               AND COD_TIPO_COORD = pEolicParam(i).COD_TIPO_COORD;
             IF SQL%ROWCOUNT = 0 THEN
                INSERT INTO FORECAST_PARAMETRI (COD_ELEMENTO,
                                                COD_TIPO_FONTE,
                                                DATA_ULTIMO_AGG,
                                                V_CUT_IN,
                                                V_CUT_OFF,
                                                V_MAX_POWER,
                                                COD_TIPO_COORD,
                                                COD_PREV_METEO)
                                        VALUES (pEolicParam(i).COD_ELEMENTO,
                                                PKG_Mago.gcRaggrFonteEolica,
                                                pEolicParam(i).DATA_ULTIMO_AGG,
                                                pEolicParam(i).V_CUT_IN,
                                                pEolicParam(i).V_CUT_OFF,
                                                pEolicParam(i).V_MAX_POWER,
                                                pEolicParam(i).COD_TIPO_COORD,
                                                pEolicParam(i).COD_PREV_METEO);
                vIns := vIns + 1;
             ELSE
                vMod := vMod + 1;
             END IF;
        END LOOP;
    END IF;

    COMMIT;

    PKG_Logs.TraceLog('Eseguito SetEolicParameter - Inseriti='||vIns||'   Modificati='||vMod,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.SetEolicParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END SetEolicParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pTipiElemento   IN VARCHAR2,
                                pData           IN DATE DEFAULT SYSDATE,
                                pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                pCodPrevMeteo IN VARCHAR2 DEFAULT '0') AS
/*-----------------------------------------------------------------------------------------------------------
     Restituisce i parametri eolici
-----------------------------------------------------------------------------------------------------------*/
  vTipEle  PKG_UtlGlb.t_SplitTbl;
  vFlgNull NUMBER(1) := -1;

  vSqlSTM  VARCHAR2(0600) := 'SELECT E.COD_ELEMENTO,DATA_ULTIMO_AGG,'                                            ||
                                    '0 VALORI_REALI,V_CUT_IN,V_CUT_OFF,V_MAX_POWER '                             ||
                              'FROM ELEMENTI E '                                                                 ||
                             'INNER JOIN ELEMENTI_DEF G ON G.COD_ELEMENTO = E.COD_ELEMENTO '                     ||
                             'LEFT OUTER JOIN FORECAST_PARAMETRI F '                                                  ||
                                               'ON (    F.COD_ELEMENTO = E.COD_ELEMENTO '                        ||
                                               ' AND NVL(F.COD_PREV_METEO,''' || pCodPrevMeteo || ''') = ''' || pCodPrevMeteo || '''' ||
                                               ' AND NVL(F.COD_TIPO_COORD,'''|| pTipoGeo || ''') = ''' || pTipoGeo || '''' ||
                                                   'AND F.COD_TIPO_FONTE='''||PKG_Mago.gcRaggrFonteEolica||''') '||
                             'INNER JOIN (SELECT ALF1 COD_TIPO_ELEMENTO '                                        ||
                                           'FROM gttd_VALORI_TEMP '                                              ||
                                          'WHERE TIP = '''||PKG_Mago.gcTmpTipEleKey||''' '                       ||
                                        ') T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO '                      ||
                             'WHERE :dt BETWEEN G.DATA_ATTIVAZIONE AND G.DATA_DISATTIVAZIONE '                   || -- pData
                             'ORDER BY E.COD_ELEMENTO';

  vSqlGDF  VARCHAR2(1000) := 'SELECT COD_ELEMENTO, NVL(DATA_ULTIMO_AGG,DATA_ATTIVAZIONE) DATA_ULTIMO_AGG, '      ||
                                'FLAG VALORI_REALI,'                                                             ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_CUT_IN '                                               ||
                                         'ELSE        G_V_CUT_IN '                                               ||
                                    'END V_CUT_IN,'                                                              ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_CUT_OFF '                                              ||
                                         'ELSE        G_V_CUT_OFF '                                              ||
                                    'END V_CUT_OFF,'                                                             ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_MAX_POWER '                                            ||
                                         'ELSE        G_V_MAX_POWER '                                            ||
                                    'END V_MAX_POWER '                                                           ||
                               'FROM (SELECT E.COD_ELEMENTO,DATA_ULTIMO_AGG,G.DATA_ATTIVAZIONE, '                ||
                                            'CASE '                                                              ||
                                               'WHEN G.COD_ELEMENTO IS NOT NULL THEN 1 '                         ||
                                               'ELSE 0 '                                                         ||
                                            'END FLAG,'                                                          ||
                                            'G.VEL_CUTIN   G_V_CUT_IN,'                                          ||
                                            'G.VEL_CUTOFF  G_V_CUT_OFF,'                                         ||
                                            'G.VEL_MAX     G_V_MAX_POWER,'                                       ||
                                            'F.V_CUT_IN    F_V_CUT_IN,'                                          ||
                                            'F.V_CUT_OFF   F_V_CUT_OFF,'                                         ||
                                            'F.V_MAX_POWER F_V_MAX_POWER '                                       ||
                                       'FROM ELEMENTI E '                                                        ||
                                      'INNER JOIN ELEMENTI_GDF_EOLICO G ON G.COD_ELEMENTO = E.COD_ELEMENTO '     ||
                                       'LEFT OUTER JOIN FORECAST_PARAMETRI F ON F.COD_ELEMENTO = E.COD_ELEMENTO '||
                                      'INNER JOIN (SELECT ALF1 COD_TIPO_ELEMENTO '                               ||
                                                    'FROM gttd_VALORI_TEMP '                                     ||
                                                   'WHERE TIP = '''||PKG_Mago.gcTmpTipEleKey||''' '              ||
                                                 ') T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO '             ||
                                      'WHERE :dt BETWEEN G.DATA_ATTIVAZIONE AND G.DATA_DISATTIVAZIONE '          || -- pData
                                    ') '                                                                         ||
                              'ORDER BY COD_ELEMENTO ';

BEGIN

    DELETE FROM gttd_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipEleKey;
    PKG_MAgo_utl.TrattaListaCodici(pTipiElemento, PKG_Mago.gcTmpTipEleKey, vFlgNull);

    IF PKG_Mago.IsMagoDGF THEN
        OPEN pRefCurs FOR vSqlGDF USING pData;
    ELSE
        OPEN pRefCurs FOR vSqlSTM USING pData;
    END IF;

   PKG_Logs.TraceLog('Eseguito GetEolicParameter - '||PKG_Mago.StdOutDate(pData)||'   TipiElem='||pTipiElemento,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetEolicParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetEolicParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraMisure     (pMisureMeteo    IN T_MISMETEO_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Elabora le misure presenti in tabella MISURE_METEO per la memorizzazione definitiva
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    PKG_Misure.AddMisureMeteo(pMisureMeteo);
END ElaboraMisure;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                pFinishTimestamp IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
     Riceve l'indicazione di fine caricamento misure da MDS
     Lancia la richiesta di elabirazione aggregate
-----------------------------------------------------------------------------------------------------------*/
  vTxt  VARCHAR2(300);
BEGIN

   DBMS_SCHEDULER.RUN_JOB('MAGO_INS_REQ_AGG_METEO',FALSE);

   OPEN pRefCurs FOR  SELECT 'OK' MESSAGE FROM DUAL;

   PKG_Logs.TraceLog('Eseguito MDScompleted',PKG_UtlGlb.gcTrace_VRB);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vTxt := SQLERRM;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.MDScompleted'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR  SELECT vTxt MESSAGE FROM DUAL;
         RETURN;

END MDScompleted;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetMeteoDistribListCO (pRefCurs OUT PKG_UtlGlb.t_query_cur) AS
/* ---------------------
   Ritorna le credenziali ftp  host/usr/passwd/DestDir
    per distribuire i file meteo-zip ai vari C.O. per mezzo del meto-ftp-service.
    Sul locale non e' significativa (basta che sia definita e ritorni un refCur)
-- record del pRefCurs:
--  cod_gest_CO VARCHAR2
--  nome_CO     VARCHAR2
--  host_name   VARCHAR2
--  OSusr       VARCHAR2
--  Ospassw     VARCHAR2
--  fildir     VARCHAR2 (sempre la stessa)
------------------------- */
  v_osusr        VARCHAR2(16) := 'magosys';
  v_ospsw        VARCHAR2(16) := 'magosys';
  k_appl_mago    CONSTANT VARCHAR2(6) := 'MAGO';

  v_mago_fildir  VARCHAR2(60);
  v_s1           VARCHAR2(200);
  v_j1           VARCHAR2(200);
  v_j3           VARCHAR2(200);
  v_wh           VARCHAR2(50);
BEGIN

   IF PKG_Mago.IsMagoDGF  THEN
      -- DGF non usa ftp-service
      v_mago_fildir := '/home/medase/meteodatafile';
      v_osusr := 'medase';
        v_ospsw := 'no_Need';
   ELSE
      v_mago_fildir := '/usr/NEW/magosys/meteofile';
      v_ospsw := INITCAP(v_osusr);
   END IF;

  -- nota DGF: u.codifica_utr|| e.codifica_esercizio = elementi.cod_gest_elemento (tipo_elemento=ESE)
   v_s1 := 'SELECT distinct u.codifica_utr|| e.codifica_esercizio cod_gest_CO, e.nome nome_CO, rs.ip_pkg2 host_name, '||
                 ' :posusr OSusr, :pospw Ospassw, :pmago_fildir fildir ';

   v_j1 := ' FROM sar_unita_territoriali u '||
           ' INNER JOIN sar_esercizi e '||
           '   USING(cod_utr) '||
           ' INNER JOIN sar_retesar_esercizi rs '||
           '    USING(cod_utr,cod_esercizio) ';

   v_j3 := ' INNER JOIN ( SELECT cod_utr,cod_esercizio,cod_applicazione '||
                             '  FROM sar_esercizi_abilitati '||
                             ' WHERE cod_applicazione=:pappl_mago)  ab '||
           ' USING (cod_utr,cod_esercizio) ';

   OPEN pRefCurs FOR v_s1||v_j1||v_j3 USING  v_osusr,v_ospsw, v_mago_fildir, k_appl_mago;

   PKG_Logs.TraceLog('Eseguito GetMeteoDistribListCO',PKG_UtlGlb.gcTrace_VRB);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteoDistribListCO'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetMeteoDistribListCO;



PROCEDURE GetMeteoDistribList (pRefCurs OUT PKG_UtlGlb.t_query_cur,pTipoGeo IN VARCHAR2 DEFAULT 'C') AS
/* ---------------------
   Ritorna lla lista dei comuni appartenenti ai vari CO.
   Sul nazionale si ha una lista suddivisa per CO (in uso a meteo-ftp-service)
   Sui locali ritorna la lsita dei comuni del CO abilitato (in uso a meteo-data-service)
-- record del pRefCurs:
--  cod_gest_CO VARCHAR2
--  cod_citta   VARCHAR2
--  nome_citta  VARCHAR2
------------------------- */

  k_appl_mago   CONSTANT VARCHAR2(6) := 'MAGO';
  v_s2          VARCHAR2(200);
  v_j1          VARCHAR2(200);
  v_j2          VARCHAR2(200);
  v_j3          VARCHAR2(200);
  v_j4          VARCHAR2(400);
  v_wh          VARCHAR2(200);

BEGIN

  IF PKG_Mago.IsMagoSTM THEN

      v_s2 := 'SELECT distinct u.codifica_utr||e.codifica_esercizio cod_gest_CO,'||
                CASE WHEN pTipoGeo IN ('A','P') THEN 'geo.cod_geo '
                                                ELSE ' m.cod_citta '
                END ||' cod_citta,m.nome nome_citta ';

      v_j1 := ' FROM sar_unita_territoriali u '||
              ' INNER JOIN sar_esercizi e '||
              '   USING(cod_utr) '||
              ' INNER JOIN sar_retesar_esercizi rs '||
              '    USING(cod_utr,cod_esercizio) ';

      v_j2 := ' INNER JOIN sar_rel_cft_comuni rel '||
              '     USING(cod_utr,cod_esercizio) '||
              ' INNER JOIN meteo_rel_istat m '||
              '  ON rel.cod_istat_comune = m.cod_istat ';

      v_j3 := ' INNER JOIN ( SELECT cod_utr,cod_esercizio,cod_applicazione '||
              '  FROM sar_esercizi_abilitati '||
              ' WHERE cod_applicazione=:pappl_mago)  ab '||
              ' USING (cod_utr,cod_esercizio) ';

      v_j4 := ' INNER JOIN (SELECT distinct pkg_elementi.getgestelemento(pkg_elementi.GetElementoPadre(elegeo.cod_elemento,'''||pkg_mago.gcComune||''',TO_DATE(''01013000'',''ddmmyyyy''),'||pkg_mago.gcOrganizzazGEO||','||pkg_mago.gcStatoNormale||')) cod_istat,'||
                    CASE WHEN pTipoGeo = 'A' THEN ' elegeo.cod_geo_a '
                         WHEN pTipoGeo = 'P' THEN ' elegeo.cod_geo '
                                             ELSE ' NULL '
                    END||' cod_geo '||
              '  FROM elementi_def elegeo '||
              ' WHERE elegeo.data_disattivazione = TO_DATE(''01013000'',''ddmmyyyy'')'||
              '   AND '||CASE WHEN pTipoGeo = 'A' THEN ' cod_geo_a IS NOT NULL '
                                                    ELSE ' cod_geo IS NOT NULL '
                           END||')  geo '||
              ' ON m.cod_istat = geo.cod_istat ';

      PKG_Logs.StdLogAddTxt  ('Is STM',TRUE,NULL);
      DBMS_OUTPUT.PUT_LINE(v_s2||v_j1||v_j2||v_j3||CASE WHEN pTipoGeo IN ('A','P') THEN v_j4 END);
      OPEN pRefCurs FOR v_s2||v_j1||v_j2||v_j3||CASE WHEN pTipoGeo IN ('A','P') THEN v_j4 END USING k_appl_mago;

  ELSIF pkg_mago.IsMagoDGF THEN -- IsMagoDGF (attualmente il dgf e' sempre locale)

      PKG_Logs.StdLogAddTxt  ('IsDGF',TRUE,NULL);

      v_s2 := 'SELECT distinct elese.cod_gest_elemento cod_gest_CO,'||CASE WHEN pTipoGeo = 'A' THEN 'elegeo.cod_geo_a ' WHEN pTipoGeo = 'P' THEN 'elegeo.cod_geo ' ELSE 'com.cod_citta ' END||'cod_citta,com.nome nome_citta ';

      v_j1 := ' FROM elementi_def elegeo '||
              ' INNER JOIN meteo_rel_istat com '||
              ' ON elegeo.rif_elemento = com.cod_istat ';

      v_j2 := ' CROSS JOIN elementi elese ';

      v_j3 := '';

      v_wh := ' WHERE elegeo.cod_tipo_elemento like ''G_T'''||
                    ' AND elegeo.data_disattivazione = TO_DATE(''01013000'',''ddmmyyyy'')'||
                    ' AND elese.cod_tipo_elemento =''ESE''';

      OPEN pRefCurs FOR v_s2||v_j1||v_j2||v_wh;

      PKG_Logs.TraceLog('Eseguito GetMeteoDistribList',PKG_UtlGlb.gcTrace_VRB);

  ELSE
    -- parametro (solo nazionale) non gestito
    PKG_Logs.TraceLog('Eseguito GetMeteoDistribList - Nothing to do',PKG_UtlGlb.gcTrace_VRB);
    RAISE pkg_UtlGlb.geNothingToDo;
  END IF;


EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteoDistribList'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetMeteoDistribList;

PROCEDURE GetAlgorithmParameter(
                        pRefCurs        OUT PKG_UtlGlb.t_query_cur ,
                        pValidDate               IN DATE
                        ) AS

    pgg_misure     NUMBER;    -- Utilizza misure [giorni]
    pgg_ogni     NUMBER;    -- Addestramento ogni [giorni]
    pjmax         NUMBER;    -- parametro J MAX
    palfamax     NUMBER;   -- parametro ALFA MAX
    palfamin     NUMBER ;

    vSql      VARCHAR2(2000);

BEGIN


    BEGIN
        select VALUE into pgg_misure from METEO_JOB_STATIC_CONFIG where KEY = 'MPS.estimation.measure.days.number';
    EXCEPTION
            WHEN NO_DATA_FOUND THEN pgg_misure := -9999;
    END;

    BEGIN
        select VALUE into pgg_ogni from METEO_JOB_STATIC_CONFIG where KEY = 'MPS.estimation.measure.interval.days';
    EXCEPTION
            WHEN NO_DATA_FOUND THEN pgg_ogni := -9999;
    END;



    -- pgg_misure     Utilizza misure [giorni]
    -- pgg_ogni     Addestramento ogni [giorni]
    -- pjmax         parametro J MAX
    -- palfamax    parametro ALFA MAX
    -- palfamin    parametro ALFA MIN


     vSql := 'select 1 ID,  ' || pgg_misure  ||' GGMISURE ,' ||
     pgg_ogni ||' GGOGNI ,' ||
     '(select J_MAX   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  JMAX , ' ||
     '(select ALFA_MAX   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  ALFAMAX , ' ||
     '(select ALFA_MIN   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  ALFAMIN  ' ||
     ' from dual'  ;


    --DBMS_OUTPUT.PUT_LINE (vSql);

    OPEN pRefCurs FOR vSql
    using pValidDate,pValidDate,pValidDate ;

    PKG_Logs.TraceLog('Eseguito GetAlgorithmParameter  '   ,PKG_UtlGlb.gcTRACE_INF);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetAlgorithmParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetAlgorithmParameter ;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassMET,
                          pFunzione    => 'PKG_METEO',
                          pStoreOnFile => FALSE);

END PKG_Meteo;
/


