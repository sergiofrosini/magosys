PROMPT file Packages/PKG_METEO.sql

CREATE OR REPLACE PACKAGE PKG_METEO AS


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.1.4
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
   NAME:       PKG_METEO

   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      14/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.c      21/03/2012  Risso M.         Integrazione SPC relative flusso meteo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni - Aggiornamento SPC relative flusso meteo
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.g      08/08/2012  Migliaccio G.    Gestione distrib.list diversivicata per tipi di installazione
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8a       07/10/2013  Moretti C.       Calcolo citt¿ di default per Clienti, Generatori, Trasformatori MT/BT
                                           vedi funzione GetCitta_Cliente_TrasfMT
   1.9a.3     02/10/2014  Moretti C.       Gestione Centri satellite
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   1.9c.4     23/11/2015  Campi P.         Modidica per uso generalizzato in proc GetMeteoDistribList e GetMeteoDistribListCO
   1.10.1     09/02/2016  Moretti C.       Utilizzo Sinonimi per dabelle esterne allo schema

   1.11         09/06/2016 Roberto Z.      Modificato per MAGONAZ

   1.11.3   01/07/2016  Forno S.            Supervisione + Storicizzazione della tabella FORECAST_PARAMETRI
   1.11.3   01/07/2016  Forno S.            Supervisione + Gestione periodi addestramento parametri modello
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione Reset Forecast Parameter
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione per dettaglio parametri
   1.11.3   01/08/2016  Forno S.            Supervisione + Update parametri globali dell'algoritmo
   1.11.3   01/08/2016  Forno S.            Supervisione + Get parametri globali dell'algoritmo
   1.11.3   12/08/2016  Forno S.            Supervisione + Get Periodi di addestramento parametri
   1.11.3   12/08/2016  Forno S.            Supervisione + Get_ListParameterChange
   1.11.3   12/09/2016  Forno S.            Supervisione + Get_InfoTableParameter (numero addestramento + numero cambiamenti)
   1.11.3   12/09/2016  Forno S.            Supervisione + Modificata SetForecastParameter per caso Eolico

   2.0.3    25/10/2016  Forno S.            MAGO-613 - GetAnagElementForecastParam - Fix campo Lat. Long.
   2.0.3    25/10/2016  Forno S.            MAGO-620 - GetAnagElementForecastParam- Fix campo Rete
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - storicizzo la FORECAST_PARAM_GLOBAL solo se almeno un valore ¿ cambiato
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - Fix recupero dati tappo finale
   2.0.4    28/10/2016  Forno S.            MAGO-633    Get_InfoTableParameter
   2.0.4    28/10/2016  Forno S.            MAGO-636    Get_ListParameterChange
   2.0.5    12/12/2016  Forno S.            MAGO-696    GetDettaglioForecastParam
   2.1.0    03/03/2017  Forno S.            MAGO-590  Definire i TR MT/BT come "autoproduttori"
   2.1.2    14/03/2017  Forno S.            MAGO-590  Ulteriore modifica GetTrasformatori
   2.1.3    10/05/2017  Forno               MAGO-1068 - Reset parametri con 2 fornitori meteo presenti in tabella    -- Spec. Version 2.1.3
   2.1.3    25/05/2017  Frosini             MAGO-1068 - KPI - Reset parametri in ambiente con 2 fornitori meteo
   2.1.3    25/05/2017  Frosini             MAGO-1105 (parzialm.) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.3    25/05/2017  Frosini             MAGO-1112 - correzioni recupero righe per elementi con fonte "mista" in lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1105 (completo) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1132 - corretta gestione RestForecastParametri eolici
   2.1.4    21/06/2017  Frosini             MAGO-1046 - modificata GetforecastElements e GetProduttori/Generatori/Trasformatori per gestione corretta disconnssi
     NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

function gRegExpSepara RETURN VARCHAR2;

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------
 FUNCTION GetCitta_Cliente_TrasfMT(pCodGest      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                   pData         IN DATE) RETURN METEO_REL_ISTAT.COD_CITTA%TYPE;

 PROCEDURE GetCodiciMeteo       (pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pFlagDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetProduttori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetProduttori2        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C');

 PROCEDURE GetGeneratori        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pDisconnect IN NUMBER DEFAULT 0);

 PROCEDURE GetGeneratori2        (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C');

 PROCEDURE GetTrasformatori     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pDisconnect IN NUMBER DEFAULT 0);

  PROCEDURE GetTrasformatori2     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pFonte          IN VARCHAR2,
                                 pTipologiaRete  IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1);

 PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                  pTipoMeteo      IN INTEGER,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrev IN VARCHAR2 DEFAULT NULL);

 PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY);

 PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY);

 PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pTipiElemento   IN VARCHAR2,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pCodPrevMeteo IN VARCHAR2 DEFAULT '0');

 PROCEDURE ElaboraMisure        (pMisureMeteo    IN T_MISMETEO_ARRAY);

 PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                 pFinishTimestamp IN DATE);

 PROCEDURE GetMeteoDistribListCO(pRefCurs       OUT PKG_UtlGlb.t_query_cur);

 PROCEDURE GetMeteoDistribList  (pRefCurs       OUT PKG_UtlGlb.t_query_cur, pTipoGeo IN VARCHAR2 DEFAULT 'C');

PROCEDURE GetAlgorithmParameter( pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                                      pValidDate IN DATE
                        );
-- ----------------------------------------------------------------------------------------------------------

END PKG_Meteo;
/