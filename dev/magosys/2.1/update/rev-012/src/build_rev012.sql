SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.1 rev 12
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/TIPI_MISURA.sql
@./InitTables/TIPI_MISURA_CONV_ORIG.sql
@./InitTables/METEO_JOB_STATIC_CONFIG.sql
@./InitTables/MJ_STATIC_CONFIG_involvedMeasureType.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_METEO.sql


PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MISURE.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql



disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.1 rev 12
PROMPT =======================================================================================

SPOOL OFF
