## VERSION v2.1.12 rev 012 ###


  MAGO-1290 - Fix tolto group by sulla QUALITA nella getmisure
  MAGO-1379 - Valori della PAS ID_Specto sono moltiplicati per 2 e non per 4		
  
  MAGO-923	- Storicizzazione dati di rilevazione meteo nel DB - Inserita gestione misure TMP.N1 - IRR.N1 pee NOWCAST
  
  fix nella PKG_ANAGRAFICHE.InitApplMagoSTM in caso di tabella vuote, corretto errore FLAG null valorizzato in modo errato 