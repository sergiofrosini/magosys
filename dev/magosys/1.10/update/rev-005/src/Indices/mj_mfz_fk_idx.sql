Prompt INDEX  mj_mfz_fk_idx 

BEGIN 
    EXECUTE IMMEDIATE 'drop index mj_mfz_fk_idx';
EXCEPTION 
    WHEN OTHERS THEN 
            NULL;
 
END;
/
 

create index mj_mfz_fk_idx 
on METEO_FILE_XML (METEO_FILE_ZIP_KEY)
TABLESPACE MAGO_IDX;
