PROMPT VIEW V_GERARCHIA_AMMINISTRATIVA;
--
-- V_GERARCHIA_AMMINISTRATIVA  (View) 
--
--  Dependencies: 
--   PKG_MAGO (Package)
--   SAR_CFT (Synonym)
--   SAR_ESERCIZI (Synonym)
--   SAR_ESERCIZI_ABILITATI (Synonym)
--   SAR_UNITA_TERRITORIALI (Synonym)
--   SAR_ZONE (Synonym)
--   V_ESERCIZI (View)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   GERARCHIA_IMP_SA (Table)
--
CREATE OR REPLACE VIEW V_GERARCHIA_AMMINISTRATIVA
AS 
SELECT ESE_GST,
       SUBSTR (A.ESE_NOM,1,60) ESE_NOM,
       ZNA_GST,
       SUBSTR (A.ZNA_NOM,1,60) ZNA_NOM,
       ZNA_X,
       ZNA_Y,
       CFT_GST,
       SUBSTR (A.CFT_NOM,1,60) CFT_NOM,
       CFT_X,
       CFT_Y,
       COD_GEST_ELEMENTO SCS_GST,
       NOME_ELEMENTO SCS_NOM,
       COORDINATA_X SCS_X,
       COORDINATA_Y SCS_Y,
       COD_ELEMENTO SCS_COD
  FROM (SELECT COD_ESE ESE_COD,
               E.GST_ESE ESE_GST,
               E.NOM_ESE ESE_NOM,
               CASE WHEN PKG_Mago.IsRomaniaFL = 0
                        THEN CASE WHEN COD_UTR = 5 AND COD_ESE = 4 AND COD_ZONA = 4
                                        THEN 'LG' || Z.CODIFICA_ZONA
                                        ELSE E.CODIFICA_UTR || Z.CODIFICA_ZONA
                             END
                        ELSE E.CODIFICA_UTR || Z.CODIFICA_ZONA
               END ZNA_GST,
               Z.NOME ZNA_NOM,
               Z.LONGITUDINE_GPS ZNA_X,
               Z.LATITUDINE_GPS ZNA_Y,
               C.CODIFICA_COMPLETA_CFT CFT_GST,
               C.NOME CFT_NOM,
               C.LONGITUDINE_GPS CFT_X,
               C.LATITUDINE_GPS CFT_Y,
               ESE_PRIMARIO
          FROM (SELECT DISTINCT
                       CASE WHEN PKG_Mago.IsRomaniaFL = 0 THEN A.COD_UTR
                                                          ELSE B.COD_UTR
                       END COD_UTR,
                       CASE WHEN PKG_Mago.IsRomaniaFL = 0 THEN A.COD_ESERCIZIO
                                                          ELSE B.COD_ESERCIZIO
                       END COD_ESE,
                       CASE WHEN PKG_Mago.IsRomaniaFL = 0 THEN A.GST_ESE
                                                          ELSE B.ESE_GST
                       END GST_ESE,
                       CASE WHEN PKG_Mago.IsRomaniaFL = 0 THEN A.NOM_ESE
                                                          ELSE B.ESE_NOM
                       END NOM_ESE,
                       CASE WHEN PKG_Mago.IsRomaniaFL = 0 THEN A.ESE_PRIMARIO
                                                          ELSE 1
                       END ESE_PRIMARIO,
                       CODIFICA_UTR
                  FROM v_Esercizi A,
                       (SELECT U.COD_UTR,U.CODIFICA_UTR,E.COD_ESERCIZIO,U.CODIFICA_UTR||E.CODIFICA_ESERCIZIO ESE_GST,UPPER(E.NOME) ESE_NOM
                          FROM SAR_ESERCIZI_ABILITATI A
                         INNER JOIN SAR_UNITA_TERRITORIALI U ON U.COD_UTR = A.COD_UTR
                         INNER JOIN SAR_ESERCIZI E ON E.COD_UTR = A.COD_UTR
                                                        AND E.COD_ESERCIZIO = A.COD_ESERCIZIO
                         WHERE A.COD_APPLICAZIONE = 'MAGO'
                       ) B
               ) E
               INNER JOIN (SELECT COD_UTR,COD_ESERCIZIO COD_ESE,COD_ZONA,CODIFICA_ZONA,NOME,LATITUDINE_GPS,LONGITUDINE_GPS
                             FROM SAR_ZONE
                            WHERE COD_ZONA != 9999) Z
                   USING (COD_UTR,COD_ESE)
               INNER JOIN
               (SELECT COD_UTR,COD_ESERCIZIO COD_ESE,COD_ZONA,COD_CFT,CODIFICA_CFT,CODIFICA_COMPLETA_CFT,NOME,LATITUDINE_GPS
                     ,LONGITUDINE_GPS
                  FROM SAR_CFT) C
                   USING (COD_UTR,COD_ESE,COD_ZONA)
       ) A
  LEFT OUTER JOIN (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,RIF_ELEMENTO,NOME_ELEMENTO,COORDINATA_X,COORDINATA_Y
                     FROM ELEMENTI E
                    INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)
                    INNER JOIN GERARCHIA_IMP_SA R USING (COD_ELEMENTO)
                    WHERE E.COD_TIPO_ELEMENTO = 'SCS'
                      AND SYSDATE BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE
                      AND SYSDATE BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE
                  ) ON RIF_ELEMENTO = CFT_GST
 ORDER BY ESE_GST, ZNA_GST, SCS_GST
/
