PROMPT VIEW V_GERARCHIA_IMPIANTO_AT_MT;
--
-- V_GERARCHIA_IMPIANTO_AT_MT  (View) 
--
--  Dependencies: 
--   PKG_ELEMENTI (Package)
--   CORELE_AVVOLGIMENTI_SA (Synonym)
--   CORELE_ESERCIZI (Synonym)
--   CORELE_IMPIANTIAT_SA (Synonym)
--   CORELE_IMPIANTIMT_SA (Synonym)
--   CORELE_MONTANTIMT_SA (Synonym)
--   CORELE_SBARRE_SA (Synonym)
--   CORELE_SEZIONATORI_SA (Synonym)
--   CORELE_TRASFORMATORIAT_SA (Synonym)
--   ELEMENTI (Table)
--
CREATE OR REPLACE VIEW V_GERARCHIA_IMPIANTO_AT_MT
AS 
WITH /*
       ATTENZIONE. Per i commenti non usare il doppio trattino !!!!
                   NON eliminare prefissi di schema !!!!
     */
   MyEsercizi AS
       (SELECT COD_GEST cg_ese
             , COD_ENTE tp_ese
             , CODICE_ST ese_st
          FROM CORELE_ESERCIZI E
       )
 , MyCabPrim AS
       (SELECT COD_GEST cg_cpr, COD_ENTE tp_cpr, CODICE_ST cpr_st, COD_ESERCIZIO ese_st
          FROM CORELE_IMPIANTIAT_SA
         WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
       )
 , MyTrasfAT AS
       (SELECT cg_tra, tp_tra, tra_st, cpr_st
          FROM (SELECT CODICE_ST sat_st, CODICEST_IMPAT cpr_st
                  FROM CORELE_SBARRE_SA S
                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                   AND TIPO = 'AT'
                UNION ALL /* SELEZIONA EVENTUALI SBARRE MT CHE ALIMENTANO IL PRIMARIO DI UN TRASF.MT/MT */
                SELECT CODICE_ST sat_st, CODICEST_IMPAT cpr_st
                  FROM CORELE_SBARRE_SA S
                 WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                   AND S.TIPO = 'MT'
                   AND CODICE_ST IN (SELECT /*+ PUSH_SUBQ */ CODICEST_SBARRA
                                       FROM CORELE_AVVOLGIMENTI_SA AVVOLGIMENTI_SA1
                                      WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                                        AND COD_ENTE = 'PRIM'
                                      GROUP BY CODICEST_SBARRA
                                    )
               )
         INNER JOIN (SELECT CODICE_ST avv_prim_st, CODICEST_SBARRA sat_st
                       FROM CORELE_AVVOLGIMENTI_SA
                      WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                        AND COD_ENTE = 'PRIM' /*AND STATO = 'CH'*/
                    ) TRP USING (sat_st)
         INNER JOIN (SELECT COD_GEST cg_tra, CODICE_ST tra_st, CODICEST_AVV_PRIM avv_prim_st, COD_ENTE tp_tra
                       FROM CORELE_TRASFORMATORIAT_SA
                      WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
                    ) TRF USING (avv_prim_st)
       )
 , MySecTer AS
       (SELECT AV.COD_GEST cg_sec, AV.COD_ENTE tp_sec, TR.CODICEST_AVV_SECN avv_sec_st, TR.CODICE_ST tra_st
          FROM CORELE_TRASFORMATORIAT_SA TR
          INNER JOIN CORELE_AVVOLGIMENTI_SA AV ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                              AND AV.CODICE_ST = TR.CODICEST_AVV_SECN
                                              AND AV.STATO = 'CH'
         WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
        UNION
        SELECT AV.COD_GEST cg_ter, AV.COD_ENTE tp_ter, TR.CODICEST_AVV_TERZ avv_ter_st, TR.CODICE_ST tra_st
          FROM CORELE_TRASFORMATORIAT_SA TR
         INNER JOIN CORELE_AVVOLGIMENTI_SA AV  ON SYSDATE BETWEEN AV.DATA_INIZIO AND AV.DATA_FINE
                                              AND AV.CODICE_ST = TR.CODICEST_AVV_TERZ
                                              AND AV.STATO = 'CH'
         WHERE SYSDATE BETWEEN TR.DATA_INIZIO AND TR.DATA_FINE
       )
 , MySbarreMT AS
       (SELECT S.COD_GEST cg_smt, S.COD_ENTE tp_smt, S.CODICE_ST smt_st, S.CODICEST_AVV avv_st
             , S.CODICEST_MONT_ALIM mont_alim_st
          FROM CORELE_SBARRE_SA S
          LEFT OUTER JOIN (SELECT CODICEST_SBARRA1 CODICE_ST
                             FROM CORELE_SEZIONATORI_SA Z
                            INNER JOIN CORELE_SBARRE_SA S ON CODICEST_SBARRA1 = S.CODICE_ST
                            WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                              AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                           UNION
                           SELECT CODICEST_SBARRA2 CODICE_ST
                             FROM CORELE_SEZIONATORI_SA Z
                            INNER JOIN CORELE_SBARRE_SA S ON CODICEST_SBARRA2 = S.CODICE_ST
                            WHERE SYSDATE BETWEEN Z.DATA_INIZIO AND Z.DATA_FINE
                              AND SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
                          ) Z ON (Z.CODICE_ST = S.CODICE_ST)
         WHERE SYSDATE BETWEEN S.DATA_INIZIO AND S.DATA_FINE
           AND S.TIPO = 'MT' /* AND S.CODICEST_AVV <> 'ND' */
         )
   , MyLineeMT AS
         (SELECT COD_GEST cg_lmt, COD_ENTE tp_lmt, CODICE_ST lmt_st, CODICEST_SBARRA smt_st
            FROM CORELE_MONTANTIMT_SA
           WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
             AND STATO = 'CH'
         )
   , MySbCabSec AS
         (SELECT COD_GEST_SBARRA cg_scs, 'SBCS' tp_scs, CODICEST_MONTANTE lmt_st
            FROM CORELE_IMPIANTIMT_SA
           WHERE SYSDATE BETWEEN DATA_INIZIO AND DATA_FINE
         )
SELECT NVL(P.COD_TIPO_ELEMENTO,'COP')||'-'||F.COD_TIPO_ELEMENTO tipo,
       NVL(P.COD_ELEMENTO,PKG_ELEMENTI.GetElementoBase)         padre,
       F.COD_ELEMENTO                                           figlio
  FROM (SELECT NULL GEST_PADRE,
               NULL TIPO_PADRE,
               COD_GEST GEST_FIGLIO,
               'ESER' TIPO_FIGLIO
          FROM CORELE_ESERCIZI E
         WHERE SYSDATE BETWEEN E.DATA_INIZIO AND E.DATA_FINE
        UNION ALL
        SELECT V.GEST_PADRE,V.TIPO_PADRE,V.GEST_FIGLIO,V.TIPO_FIGLIO
          FROM (
                SELECT /* seleziono Esercizi */
                       NULL gest_padre, NULL tipo_padre, cg_ese gest_figlio, tp_ese tipo_figlio FROM MyEsercizi
                UNION ALL
                SELECT /* associo CABINE PRIMARIE ad ESERCIZIO */
                       cg_ese gest_padre, tp_ese tipo_padre, cg_cpr gest_figlio, tp_cpr tipo_figlio
                  FROM MyCabPrim A INNER JOIN MyEsercizi B ON B.ese_st = A.ese_st
                UNION ALL
                SELECT /* associo TRASFORMATORI a CABINA PRIMARIA */
                       cg_cpr gest_padre, tp_cpr tipo_padre, cg_tra gest_figlio, tp_tra tipo_figlio
                  FROM MyTrasfAT A INNER JOIN MyCabPrim B ON B.cpr_st = A.cpr_st
                UNION ALL
                SELECT /* associo SECONDARI/TERZIARI a TRASFORMATORE */
                       cg_tra gest_padre, tp_tra tipo_padre, cg_sec gest_figlio, tp_sec tipo_figlio
                  FROM MySecTer A INNER JOIN MyTrasfAT B ON B.tra_st = A.tra_st
                UNION ALL
                SELECT /* associo SBARRE MT a SECONDARIO/TERZIARIO */
                       cg_sec gest_padre, tp_sec tipo_padre, cg_smt gest_figlio, tp_smt tipo_figlio
                  FROM MySbarreMT A INNER JOIN MySecTer B ON B.avv_sec_st = A.avv_st
                 WHERE A.mont_alim_st IS NULL /* esclude le SBARRE MT associate a MONTANTI di altre CP */
                UNION ALL
                SELECT /* associo LINEE MT a SBARRA MT */
                       cg_smt gest_padre, tp_smt tipo_padre, cg_lmt gest_figlio, tp_lmt tipo_figlio
                  FROM MyLineeMT A INNER JOIN MySbarreMT B ON B.smt_st = A.smt_st
                UNION ALL
                SELECT /* associo SBARRE MT a LINEE MT - SbarreMT alimentate da Linee di altra CP (es: Centro Satellite) */
                       cg_lmt gest_padre, tp_lmt tipo_padre, cg_smt gest_figlio, tp_smt tipo_figlio
                  FROM MySbarreMT A INNER JOIN MyLineeMT B ON B.lmt_st = A.mont_alim_st
                 WHERE A.mont_alim_st IS NOT NULL /* seleziona solo le SBARRE MT associate a MONTANTI di altre CP */
                UNION ALL
                SELECT /* associo SBARRE DI CABINA SECONRARIA a LINEA MT */
                       cg_lmt gest_padre, tp_lmt tipo_padre, cg_scs gest_figlio, tp_scs tipo_figlio
                  FROM MySbCabSec A INNER JOIN MyLineeMT B ON B.lmt_st = A.lmt_st
               ) V
         WHERE V.TIPO_FIGLIO <> 'ESER'
       ) A
  LEFT OUTER JOIN ELEMENTI P ON P.COD_GEST_ELEMENTO = A.GEST_PADRE
 INNER JOIN       ELEMENTI F ON F.COD_GEST_ELEMENTO = A.GEST_FIGLIO
 WHERE CASE WHEN P.COD_ELEMENTO IS NOT NULL
                THEN 1
                ELSE CASE WHEN F.COD_TIPO_ELEMENTO = 'ESE'
                              THEN 1
                              ELSE 0
                     END
       END = 1
 ORDER BY PKG_ELEMENTI.GetOrderByElemType(F.COD_TIPO_ELEMENTO,F.COD_GEST_ELEMENTO),
          CASE SUBSTR(F.COD_GEST_ELEMENTO,1,1)
               WHEN 'D' THEN 1
               WHEN 'A' THEN 2
                        ELSE 3
          END,
          F.COD_GEST_ELEMENTO
/
