SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 1.10.1 SRC
PROMPT =======================================================================================

conn corele/corele&tns_arcdb2

Prompt GRANT ON VIEW V_FILE_ESERCIZI TO MAGO;
GRANT SELECT ON V_FILE_ESERCIZI TO MAGO;

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 
PROMPT _______________________________________________________________________________________
PROMPT InitTables

conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/SAR_PARAMETRI_GENERALI.sql

COMMIT;
					  
conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Synonym
PROMPT
@./Synonym/ANAGRAFICA_PUNTI.sql;
@./Synonym/AUI_CLIENTI_TLC.sql;
@./Synonym/AUI_GENERATORI_TLC.sql;
@./Synonym/AUI_NODI_TLC.sql;
@./Synonym/AUI_TRASF_PROD_BT_TLC.sql;
@./Synonym/AUI_UPDATE_AUI_TLC.sql;
@./Synonym/AUI_UTILIZZO_TLC.sql;
@./Synonym/CORELE_AVVOLGIMENTI_SA.sql;
@./Synonym/CORELE_AVVOLGIMENTI_SN.sql;
@./Synonym/CORELE_CAVI_SA.sql;
@./Synonym/CORELE_CAVI_SN.sql;
@./Synonym/CORELE_CLIENTIAT_SA.sql;
@./Synonym/CORELE_CLIENTIAT_SN.sql;
@./Synonym/CORELE_CLIENTIMT_SA.sql;
@./Synonym/CORELE_CLIENTIMT_SN.sql;
@./Synonym/CORELE_CONGIUNTORIMT_SN.sql;
@./Synonym/CORELE_CONGIUNTORI_SA.sql;
@./Synonym/CORELE_CONGIUNTORI_SN.sql;
@./Synonym/CORELE_CONSEGNE_SA.sql;
@./Synonym/CORELE_CONSEGNE_SN.sql;
@./Synonym/CORELE_DATE_IMPORT_CONSISTENZA.sql;
@./Synonym/CORELE_ESERCIZI.sql;
@./Synonym/CORELE_IMPIANTIAT_SA.sql;
@./Synonym/CORELE_IMPIANTIAT_SN.sql;
@./Synonym/CORELE_IMPIANTIMT_SA.sql;
@./Synonym/CORELE_IMPIANTIMT_SN.sql;
@./Synonym/CORELE_LINEEBT_SA.sql;
@./Synonym/CORELE_LINEEBT_SN.sql;
@./Synonym/CORELE_MONTANTIAT_SA.sql;
@./Synonym/CORELE_MONTANTIAT_SN.sql;
@./Synonym/CORELE_MONTANTIMT_SA.sql;
@./Synonym/CORELE_MONTANTIMT_SN.sql;
@./Synonym/CORELE_NEA_INTCLI.sql;
@./Synonym/CORELE_NODIMT_SN.sql;
@./Synonym/CORELE_PARALLELI_SA.sql;
@./Synonym/CORELE_PARALLELI_SN.sql;
@./Synonym/CORELE_RIFASATORI_SA.sql;
@./Synonym/CORELE_RIFASATORI_SN.sql;
@./Synonym/CORELE_SBARRE_SA.sql;
@./Synonym/CORELE_SBARRE_SN.sql;
--@./Synonym/CORELE_SEMAFORO.sql;
@./Synonym/CORELE_SEZIONATORIMT_SN.sql;
@./Synonym/CORELE_SEZIONATORI_SA.sql;
@./Synonym/CORELE_SEZIONATORI_SN.sql;
@./Synonym/CORELE_SEZIONIBT_SA.sql;
@./Synonym/CORELE_SEZIONIBT_SN.sql;
@./Synonym/CORELE_TRASFORMATORIAT_SA.sql;
@./Synonym/CORELE_TRASFORMATORIAT_SN.sql;
@./Synonym/CORELE_TRASFORMATORIBT_SA.sql;
@./Synonym/CORELE_TRASFORMATORIBT_SN.sql;
@./Synonym/CORELE_TRASLATORI_SA.sql;
@./Synonym/CORELE_TRASLATORI_SN.sql;
@./Synonym/CORELE_V_MOD_ASSETTO_RETE.sql;
@./Synonym/CORELE_FILE_ESERCIZI.sql;
@./Synonym/SAR_CFT.sql;
@./Synonym/SAR_COMUNI.sql;
@./Synonym/SAR_ESERCIZI.sql;
@./Synonym/SAR_ESERCIZI_ABILITATI.sql;
@./Synonym/SAR_MACRO_AREE.sql;
@./Synonym/SAR_PROVINCE.sql;
@./Synonym/SAR_REGIONI.sql;
@./Synonym/SAR_REL_CFT_COMUNI.sql;
@./Synonym/SAR_RETESAR_ESERCIZI.sql;
@./Synonym/SAR_UNITA_TERRITORIALI.sql;
@./Synonym/SAR_ZONE.sql;

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/ELEMENTI_DEF.sql

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_ESERCIZI.sql
@./Views/V_ANAGRAFICA_IMPIANTO.sql
@./Views/V_GERARCHIA_IMPIANTO_AT_MT.sql
@./Views/V_GERARCHIA_IMPIANTO_MT_BT.sql
@./Views/V_GERARCHIA_AMMINISTRATIVA.sql
@./Views/V_GERARCHIA_GEOGRAFICA.sql
@./Views/V_MAGO_ANAGRAFICA_BASE.sql
@./Views/V_MAGO_REL_IMP_AT_MT.sql
@./Views/V_MOD_ANAGRAFICHE_IN_SOSPESO.sql
@./Views/V_MOD_ASSETTO_RETE_SA.sql
@./Views/V_MAGO_REL_IMP_AT_MT.sql
@./Views/V_CURRENT_VERSION.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ANAGRAFICHE.sql;
@./Packages/PKG_METEO.sql;

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ANAGRAFICHE.sql;
@./PackageBodies/PKG_METEO.sql;

--PROMPT _______________________________________________________________________________________
--PROMPT InitTables
--@./InitTables/ELEMENTI.sql

COMMIT;

					  
conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';


PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 1.10.1 SRC
PROMPT

spool off
