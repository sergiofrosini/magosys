PROMPT ELEMENTI - Conversione codice Gestionale Esercizi

BEGIN
    FOR i IN (SELECT * FROM corele.ESERCIZI) LOOP
        CASE pkg_Mago.IsRomaniaFL 
            WHEN 1 THEN -- Caso Romania
                        FOR j IN (SELECT * FROM ELEMENTI 
                                   WHERE COD_GEST_ELEMENTO = i.UT||i.ESE
                                     AND COD_TIPO_ELEMENTO = 'ESE'
                                 ) LOOP
                            UPDATE ELEMENTI SET COD_GEST_ELEMENTO = i.COD_GEST
                             WHERE COD_ELEMENTO = j.COD_ELEMENTO;
                        END LOOP;
            WHEN 0 THEN -- Altri Casi
                        FOR j IN (SELECT * FROM ELEMENTI 
                                   WHERE COD_GEST_ELEMENTO IN (i.COD_GEST, i.COD_GEST||'_'||i.ESE)
                                     AND COD_TIPO_ELEMENTO = 'ESE'
                                 ) LOOP
                            UPDATE ELEMENTI SET COD_GEST_ELEMENTO = i.COD_GEST
                             WHERE COD_ELEMENTO = j.COD_ELEMENTO;
                        END LOOP;
        END CASE;
        FOR j IN (SELECT * FROM ELEMENTI WHERE COD_TIPO_ELEMENTO = 'ESE'
                 ) LOOP
            UPDATE ELEMENTI_DEF SET ID_ELEMENTO = i.ESE,
                                    RIF_ELEMENTO = i.UT
             WHERE COD_ELEMENTO IN (SELECT COD_ELEMENTO FROM ELEMENTI WHERE COD_GEST_ELEMENTO = I.COD_GEST);
        END LOOP;
    END LOOP;
END;
/
