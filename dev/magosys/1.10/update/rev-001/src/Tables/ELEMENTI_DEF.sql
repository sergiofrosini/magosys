PROMPT TABLE ELEMENTI_DEF;


COMMENT ON COLUMN ELEMENTI_DEF.ID_ELEMENTO IS 'Codice gestionale dell''elemento da cui dipende - per Generatori, Clienti e Sbarre di CS  - Codifica STM per gli Esercizi'
/

COMMENT ON COLUMN ELEMENTI_DEF.RIF_ELEMENTO IS 'Riferimento esterno: CFT per Sbarre di CS - POD Eneltel dell''elemento (se applicabile) - CPR fisica per Sbarre MT - Codifica STM dell''UTR per gli Esercizi'
/

COMMENT ON COLUMN ELEMENTI_DEF.FLAG IS 'ESE; 1=Principale, 0=Esercizio dell''UTR - CPR; 1=Centro Satellite'
/

