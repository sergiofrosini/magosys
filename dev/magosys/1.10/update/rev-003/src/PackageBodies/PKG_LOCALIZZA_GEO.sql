Prompt Package Body PKG_LOCALIZZA_GEO;
--
-- PKG_LOCALIZZA_GEO  (Package Body) 
--
--  Dependencies: 
--   PKG_LOCALIZZA_GEO (Package)
--
CREATE OR REPLACE PACKAGE BODY PKG_LOCALIZZA_GEO AS
/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.9c.0
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/
proc_name VARCHAR2(50);
--vLog         PKG_Mago.t_StandardLog;
err_code NUMBER;
err_msg VARCHAR2(250);
FUNCTION coord_conversion ( coord_value IN VARCHAR2, coord_type IN VARCHAR2, decimal_point IN VARCHAR2 DEFAULT '.' ) RETURN NUMBER AS
val NUMBER;
BEGIN
   CASE coord_type
      WHEN 'GPS' THEN
      /* Per convertire il formato GPS in WGS84 prendere i minuti e moltiplicarli per 60 */
         SELECT  SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                 +
                 TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = ''' ||decimal_point || '|''')  / 60 * SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ', 1)-1))
           INTO val
           FROM dual;
      WHEN 'DMS' THEN
         SELECT SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                +
                (( TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ') +1, INSTR(TRIM(coord_value),' ', -1) - INSTR(TRIM(coord_value),' ') -1),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 60
                  + TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 3600
                 )
                 *SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1))
                )
           INTO val
           FROM dual;
      WHEN 'WGS84' THEN
         val := TO_NUMBER(TRIM(coord_value),'999D999999999999999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''');
      ELSE
        val := TO_NUMBER(TRIM(coord_value));
      END CASE;
RETURN (val);
END;
PROCEDURE load_punti AS
BEGIN
   proc_name := 'PKG_LOCALIZZA_GEO.LOAD_PUNTI';
   INSERT INTO ANAGRAFICA_PUNTI
   (cod_geo , tipo_coord, coordinata_x, coordinata_y, coordinata_h )
   SELECT NVL(cod_geo_p,cod_geo_a),tipo_coord, coordinata_x, coordinata_y, altitudine
   FROM (
         SELECT new_coord.*
               ,CASE WHEN new_coord.tipo_coord ='P' THEN cod_geo_p + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_p
               ,CASE WHEN new_coord.tipo_coord ='A' THEN cod_geo_a + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_a
           FROM
               (
                SELECT DISTINCT 'P' tipo_coord
                       ,ROUND(ele.coordinata_y,7) coordinata_x --le coordinate in AUI sono invertite
                       ,ROUND(ele.coordinata_x,7) coordinata_y --le coordinate in AUI sono invertite
                       ,NVL(ele.altitudine,0) altitudine
                  FROM ELEMENTI_DEF ele
                  WHERE NVL(ele.coordinata_x,ele.coordinata_y) IS NOT NULL
                    AND ele.cod_tipo_elemento = pkg_mago.gcSbarraCabSec
                 /* UNION ALL
                SELECT DISTINCT 'A' tipo_coord
                       ,ROUND(coord_conversion(prod.coord_nord_a, prod.tipo_coord_a),7) coordinata_x
                       ,ROUND(coord_conversion(prod.coord_est_a, prod.tipo_coord_a),7) coordinata_y
                       ,prod.coord_up_a altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NOT NULL
                 UNION
                SELECT 'A' tipo_coord
                       ,ROUND(AVG(coord_conversion(prod.coord_nord_p, prod.tipo_coord_p)),7) coordinata_x
                       ,ROUND(AVG(coord_conversion(prod.coord_est_p, prod.tipo_coord_p)),7) coordinata_y
                       ,ROUND(AVG(prod.coord_up_p)) altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NULL
                  GROUP BY NVL(prod.cg_produttore,prod.cod_gruppo)
                */
            ) new_coord
               , ANAGRAFICA_PUNTI old_coord
               ,( SELECT NVL(MAX(cod_geo_p),'020000') cod_geo_p
                        ,NVL(MAX(cod_geo_a),'300000') cod_geo_a
                    FROM
                        (
                         SELECT TO_NUMBER(CASE WHEN anag.TIPO_COORD = 'P' THEN anag.COD_GEO END) cod_geo_p
                               ,TO_NUMBER(CASE WHEN anag.TIPO_COORD = 'A' THEN anag.COD_GEO END) cod_geo_a
                           FROM ANAGRAFICA_PUNTI anag
                          UNION ALL
                         SELECT TO_NUMBER(ese.COD_UTR || TRIM(TO_CHAR(ese.COD_ESERCIZIO, '00')) || '020000') cod_geo_p
                               ,TO_NUMBER(ese.COD_UTR || TRIM(TO_CHAR(ese.COD_ESERCIZIO, '00')) || '300000') cod_geo_a
                           FROM V_ESERCIZI ese
                          WHERE ese.ESE_PRIMARIO = 1
                        )
                ) maxval
          WHERE new_coord.coordinata_x = old_coord.coordinata_x(+)
            AND new_coord.coordinata_y = old_coord.coordinata_y(+)
            AND new_coord.altitudine = old_coord.coordinata_h(+)
            AND new_coord.tipo_coord = old_coord.tipo_coord(+)
            AND old_coord.cod_geo IS NULL
        );
   COMMIT;
END LOAD_PUNTI;
PROCEDURE associa_punti(coord_type IN VARCHAR2) AS
BEGIN
   IF coord_type ='P' THEN
   MERGE INTO ELEMENTI_DEF T
        USING (
                SELECT /*+ use_hash(ele anag)*/ anag.cod_geo
                       ,ele.cod_elemento
                       ,ROUND(ele.coordinata_y,7) coordinata_x --le coordinate in AUI sono invertite
                       ,ROUND(ele.coordinata_x,7) coordinata_y --le coordinate in AUI sono invertite
                       ,NVL(ele.altitudine,0) altitudine
                       ,ele.data_disattivazione
                  FROM ELEMENTI_DEF ele
                      ,ANAGRAFICA_PUNTI anag
                  WHERE NVL(ele.coordinata_x,ele.coordinata_y) IS NOT NULL
                    AND ele.cod_tipo_elemento = pkg_mago.gcSbarraCabSec
                    AND ROUND(ele.coordinata_y,7) = anag.coordinata_x
                    AND ROUND(ele.coordinata_x,7) = anag.coordinata_y
                    AND NVL(ele.altitudine,0) = NVL(anag.coordinata_h,0)
                    AND ele.data_disattivazione >= TRUNC(SYSDATE)
              ) s
           ON (T.cod_elemento = s.cod_elemento AND T.data_disattivazione = s.data_disattivazione )
      WHEN MATCHED THEN
    UPDATE SET
              T.cod_geo = s.cod_geo
      WHEN NOT MATCHED THEN
    INSERT
          (cod_elemento)
    VALUES (NULL);

   COMMIT;
 END IF;
END;
END;
/
SHOW ERRORS;


