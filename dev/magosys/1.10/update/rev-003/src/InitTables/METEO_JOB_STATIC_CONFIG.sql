prompt INSERT INTO meteo_job_static_config

MERGE INTO meteo_job_static_config C
   	USING (
	SELECT 'MPS.load.forecast.customer.save.groupsize' key, 50 value FROM DUAL
	UNION
	SELECT 'MPS.load.forecast.customer.save.thread' key, 1 value FROM DUAL
	UNION
	SELECT 'MPS.load.forecast.transformer.save.groupsize' key, 50 value FROM DUAL
	UNION
	SELECT 'MPS.load.forecast.transformer.save.thread' key, 2 value FROM DUAL
	UNION
	SELECT 'MPS.estimation.measure.week.number' key, 1 value FROM DUAL
	) S
   	ON (C.key = S.key)
   	WHEN MATCHED THEN
    	UPDATE SET C.value = S.value
   	WHEN NOT MATCHED THEN 
   		INSERT (C.key, C.value)
   		VALUES (S.key, S.value);