PROMPT INIT TABLE MEASURE_OFFLINE_PARAMETERS;
 
MERGE INTO MEASURE_OFFLINE_PARAMETERS A USING
 (SELECT 'STWebRegistry-WS.measure.offline.forecast_info_involved_measures' as key,
         'PMP' AS value
    FROM DUAL
 ) B ON (A.key = B.key)
WHEN NOT MATCHED THEN INSERT (key,value)
                      VALUES (B.key, b.value)
WHEN MATCHED THEN  UPDATE SET A.value=b.value;

COMMIT;