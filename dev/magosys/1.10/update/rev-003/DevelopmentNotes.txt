﻿## VERSION 1.10.3 rev 003 ###

Compatibilita'
——————————————————————
CORELE    ver 1.5.1 
STMAUI    ver 3.1
MAGO      ver 1.10.1
SAR_ADMIN ver 1.2 rev 10 o superiore

ATTENZIONE:
CORELE 1.5.1 e MAGO 1.10.1 DEVONO essere installati contestualmente con sequenza:
  - CORELE 1.5.1
  - MAGO   1.10.1


 Attivita'
——————————————————————

21/07/2016  - MAGO-247 - problema ACEA file Geo per sito non primario
							

30/06/2016  - MAGO-209: Non vinene visualizzare la versione di MAGO nell'Amministrazione
					Aggiunto entry MAGO nella SAR_ADMIN.CONTESTO_WEB

