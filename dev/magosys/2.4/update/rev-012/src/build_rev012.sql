 
PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 12
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2 

PROMPT _______________________________________________________________________________________
PROMPT Triggers
PROMPT
@./Triggers/AFT_IUR_ELEMENTI_DEF.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_MAGO_LINK.sql
@./PackageBodies/PKG_ANAGRAFICHE.sql



disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 12
PROMPT =======================================================================================
 
