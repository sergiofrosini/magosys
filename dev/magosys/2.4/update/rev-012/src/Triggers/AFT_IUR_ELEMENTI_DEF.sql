PROMPT TRIGGER AFT_IUR_ELEMENTI_DEF 

create or replace TRIGGER AFT_IUR_ELEMENTI_DEF 
AFTER INSERT OR UPDATE OF FLAG ON ELEMENTI_DEF
   REFERENCING NEW AS New OLD AS Old
   FOR EACH ROW
DECLARE
   v_CO NUMBER;
   v_INST number;
   v_Ese number;
   v_quanti number;
BEGIN

    IF :new.COD_TIPO_ELEMENTO = 'ESE' THEN

       IF :new.FLAG = 1 THEN

           SELECT COD_ELEMENTO_CO, COD_ELEMENTO_ESE, TIPO_INST
           INTO v_CO, v_ESE, V_INST
           FROM DEFAULT_CO
           WHERE flag_primario=1;

           IF v_ESE <> :new.COD_ELEMENTO then

               -- insert into Log_2580 values (current_timestamp,'TRIG_ELE_DEF','reset def co 0 '||:new.COD_ELEMENTO);
                UPDATE DEFAULT_CO
                SET FLAG_PRIMARIO=0;

                SELECT count(*)
                INTO v_quanti
                FROM DEFAULT_CO
                WHERE COD_ELEMENTO_ESE=:new.COD_ELEMENTO;

                IF v_quanti=0 then
                --    insert into Log_2580 values (current_timestamp,'TRIG_ELE_DEF','insert def ese '||:new.COD_ELEMENTO);
                    INSERT INTO DEFAULT_CO
                    (COD_ELEMENTO_CO, COD_ELEMENTO_ESE, TIPO_INST, FLAG_PRIMARIO, START_DATE)
                    VALUES
                    (V_CO, :new.COD_ELEMENTO, V_INST, 1, sysdate);
                ELSE
                 --   insert into Log_2580 values (current_timestamp,'TRIG_ELE_DEF','update def ese '||:new.COD_ELEMENTO);
                    UPDATE DEFAULT_CO
                    SET flag_primario=1
                    WHERE COD_ELEMENTO_ESE=:new.COD_ELEMENTO
                    AND START_DATE = (SELECT MAX(START_DATE) fROM DEFAULT_CO WHERE COD_ELEMENTO_ESE=:new.COD_ELEMENTO);
                 END IF;

            end if;

        END IF;

    END IF;

END;
/