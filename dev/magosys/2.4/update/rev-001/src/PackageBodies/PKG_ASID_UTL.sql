PROMPT PACKAGE BODY PKG_ASID_UTL 

create or replace PACKAGE BODY PKG_ASID_UTL AS

/* ========================================================================== */

  gc_ASID2018 CONSTANT VARCHAR2(8) := 'ASID2018';
  
  gd_ASID2018 DATE := NULL;

/* ========================================================================== */

  function f_ASID2018 return varchar2 AS begin return gc_ASID2018; END;

/* ========================================================================== */

  function f_GetData(pTipo IN VARCHAR2) return date AS

    vData date;

  BEGIN

        SELECT data_validita 
        into vData
        from ricodifica_elementi@PKG1_STMAUI.IT
        where tipo_ricodifica = pTipo
        and rownum = 1;
        
    --   vData := to_DATE('20112018','ddmmyyyy'); --fake pe rle prove
        
        return vData;
  END f_GetData;

/* ========================================================================== */

  procedure sp_GetData(pDataOut IN OUT date, pTipo IN VARCHAR2) AS
  BEGIN
    pDataOut := f_GetData(ptipo);
  END sp_GetData;

/* ========================================================================== */

  function f_GetDataASID2018 return date AS
  BEGIN
  
      IF gd_ASID2018 IS NULL THEN
        gd_ASID2018 := f_GetData(f_ASID2018);
      END IF;
  
      return gd_ASID2018;
      
  END f_GetDataASID2018;

/* ========================================================================== */

  procedure sp_GetDataASID2018(pDataOut IN OUT date) AS
  BEGIN
    pDataOut := f_GetDataASID2018;
  END sp_GetDataASID2018;

/* ========================================================================== */

END PKG_ASID_UTL;
/
show errors;