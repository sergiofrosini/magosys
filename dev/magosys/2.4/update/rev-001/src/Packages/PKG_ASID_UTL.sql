PROMPT PACKAGE PKG_ASID_UTL

create or replace PACKAGE PKG_ASID_UTL AS 

 function f_ASID2018 return varchar2;

 function f_GetData( pTipo IN VARCHAR2) return date;
 procedure sp_GetData(pDataOut IN OUT date, pTipo IN VARCHAR2);
 
 function f_GetDataASID2018 return date;
 procedure sp_GetDataASID2018(pDataOut IN OUT date);

END PKG_ASID_UTL;
/