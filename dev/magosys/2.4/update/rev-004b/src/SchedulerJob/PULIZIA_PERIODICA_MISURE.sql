PROMPT SCHEDULERJOB PULIZIA_PERIODICA_MISURE


BEGIN
  SYS.DBMS_SCHEDULER.DROP_JOB
    (job_name  => 'PULIZIA_PERIODICA_MISURE');
 EXCEPTION
    WHEN OTHERS THEN NULL;       
END;
/

BEGIN
  SYS.DBMS_SCHEDULER.CREATE_JOB
    (
       job_name        => 'PULIZIA_PERIODICA_MISURE'
      ,start_date      => TO_TIMESTAMP_TZ('2019/07/04 00:00:00.061731 Europe/Vienna','yyyy/mm/dd hh24:mi:ss.ff tzr')
      ,repeat_interval => 'FREQ=DAILY;BYMONTHDAY=1'
      ,end_date        => NULL
      ,job_class       => 'DEFAULT_JOB_CLASS'
      ,job_type        => 'STORED_PROCEDURE'
      ,job_action      => 'MAGO.SP_PULIZIA_PERIODICA_MISURE'
      ,comments        => 'Job che lancia il giorno 1 del mese, la pulizia periodica delle misure (MAGO.SP_PULIZIA_PERIODICA_MISURE)  mantendo a sistema un numero di mesi configurato nel parametro MESI_MISURE_STORICIZZATI'
    );
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'RESTARTABLE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'LOGGING_LEVEL'
     ,value     => SYS.DBMS_SCHEDULER.LOGGING_RUNS);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'MAX_FAILURES');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'MAX_RUNS');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'STOP_ON_WINDOW_CLOSE'
     ,value     => FALSE);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'JOB_PRIORITY'
     ,value     => 3);
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE_NULL
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'SCHEDULE_LIMIT');
  SYS.DBMS_SCHEDULER.SET_ATTRIBUTE
    ( name      => 'MAGO.PULIZIA_PERIODICA_MISURE'
     ,attribute => 'AUTO_DROP'
     ,value     => FALSE);

  SYS.DBMS_SCHEDULER.ENABLE
    (name                  => 'MAGO.PULIZIA_PERIODICA_MISURE');
END;
/


