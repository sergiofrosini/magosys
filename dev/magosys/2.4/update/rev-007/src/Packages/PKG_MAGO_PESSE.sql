PROMPT PACKAGE: PKG_MAGO_PESSE

create or replace PACKAGE  PKG_MAGO_PESSE AS 


/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.4 rev 007
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

    PROCEDURE GetMisureLink( --pMisTab OUT PKG_MAGO_PESSE.t_Misure,
                        pCodElem IN ELEMENTI.COD_ELEMENTO%TYPE,
                        pDataDa           IN DATE,
                        pDataA            IN DATE,
                        pTipiMisura       IN VARCHAR2,
                        pOrganizzazione   IN INTEGER,
                        pStatoRete        IN INTEGER,
                        pTipologiaRete    IN VARCHAR2,
                        pFonte            IN VARCHAR2,
                        pTipoClie         IN VARCHAR2,                    
                        pAgrTemporale     IN INTEGER DEFAULT 15,
                        pDisconnect       IN INTEGER DEFAULT 0,
                        pdeltaT           IN INTEGER DEFAULT 7);
                        
    PROCEDURE GetMisureCGLink(   --pMisTab OUT PKG_MAGO_PESSE.t_Misure,
                            pGestElem         IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pDataDa           IN DATE,
                             pDataA            IN DATE,
                             pTipiMisura       IN VARCHAR2,
                             pOrganizzazione   IN INTEGER,
                             pStatoRete        IN INTEGER,
                             pTipologiaRete    IN VARCHAR2,
                             pFonte            IN VARCHAR2,
                             pTipoClie         IN VARCHAR2,
                             pAgrTemporale     IN INTEGER DEFAULT 15,
                             pDisconnect       IN INTEGER DEFAULT 0);

                                      
END PKG_MAGO_PESSE;
/