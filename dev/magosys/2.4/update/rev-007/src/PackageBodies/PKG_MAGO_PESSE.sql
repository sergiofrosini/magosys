PROMPT PACKAGE BODY: PKG_MAGO_PESSE

create or replace PACKAGE BODY PKG_MAGO_PESSE AS
/* *********************************************************************************************************** 
   NAME:       PKG_MAGO_PESSE
   PURPOSE:    Implementazione funzione fornitura misure tramite DBlink

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.4.7      25/03/2020  MALANDRINO       Created this package.

*********************************************************************************************************** */

-- ===========================================================================================================================

PROCEDURE GetMisureLink( --pMisTab OUT PKG_MAGO_PESSE.t_Misure,
                        pCodElem IN ELEMENTI.COD_ELEMENTO%TYPE,
                        pDataDa           IN DATE,
                        pDataA            IN DATE,
                        pTipiMisura       IN VARCHAR2,
                        pOrganizzazione   IN INTEGER,
                        pStatoRete        IN INTEGER,
                        pTipologiaRete    IN VARCHAR2,
                        pFonte            IN VARCHAR2,
                        pTipoClie         IN VARCHAR2,                    
                        pAgrTemporale     IN INTEGER DEFAULT 15,
                        pDisconnect       IN INTEGER DEFAULT 0,
                        pdeltaT           IN INTEGER DEFAULT 7) IS

    vLst            PKG_UtlGlb.t_query_cur;
    vCodMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE;
    vData           DATE;
    vValore         NUMBER;
    vTipoFonte      TIPO_FONTI.COD_TIPO_FONTE%TYPE;
    vinter NUMBER;
    vqualita varchar2(20);
    vLog  PKG_Logs.t_StandardLog;
    --vRec t_Misura;    
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_MAGO_PESSE.GetMisureLink',
                                pForceLogFile => TRUE);

    --delete from gttd_misure_link;
    execute immediate 'truncate table misure_link';
    PKG_Logs.TraceLog('Truncate Table MISURE_LINK - '||TO_CHAR(systimestamp,'dd/mm/yyyy hh24:mi:ss'),PKG_UtlGlb.gcTrace_VRB);
    
    pkg_misure.GetMisure(vLst,pCodElem,pDataDa,pDataA, pTipiMisura, pOrganizzazione, pStatoRete, pTipologiaRete, pFonte,pTipoClie,pAgrTemporale, pDisconnect,pdeltaT);
    loop
        fetch vLst into vCodMis, vData, vValore,  vQualita, vTipoFonte, vinter;
        exit when vLst%notfound;
--        pMisTab.extend;
--        vRec.COD_TIPO_MISURA := vCodMis;
--        vRec.DATA := vData;
--        vRec.VALORE := vValore;
--        vRec.QUALITY := vQualita;
--        vRec.COD_TIPO_FONTE := vTipoFonte;
--        vRec.INTERPOLAZIONE := vinter;
--        pMisTab(pMisTab.last) := vRec;
        insert into misure_link values (vCodMis, vData, vValore,  vQualita, vTipoFonte, vinter);
        PKG_Logs.TraceLog('Insert row - '||'cod mis: '||vCodMis||' Data:'||to_char(vData,'ddmmyyyy hh24:mi:ss')||' valore:'||vValore,PKG_UtlGlb.gcTrace_VRB);

    end loop;
    close vLst;

    commit;

END;

PROCEDURE GetMisureCGLink(   --pMisTab OUT PKG_MAGO_PESSE.t_Misure,
                            pGestElem         IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pDataDa           IN DATE,
                             pDataA            IN DATE,
                             pTipiMisura       IN VARCHAR2,
                             pOrganizzazione   IN INTEGER,
                             pStatoRete        IN INTEGER,
                             pTipologiaRete    IN VARCHAR2,
                             pFonte            IN VARCHAR2,
                             pTipoClie         IN VARCHAR2,
                             pAgrTemporale     IN INTEGER DEFAULT 15,
                             pDisconnect       IN INTEGER DEFAULT 0) IS

    vLst            PKG_UtlGlb.t_query_cur;
    vCodMis         TIPI_MISURA.COD_TIPO_MISURA%TYPE;
    vData           DATE;
    vValore         NUMBER;
    vTipoFonte      TIPO_FONTI.COD_TIPO_FONTE%TYPE;
    vinter NUMBER;
    vqualita varchar2(20);
    
    vLog  PKG_Logs.t_StandardLog;
    --vRec t_Misura;    
BEGIN

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassUTL||PKG_Mago.gcJobSubClassMIS,
                                pFunzione     => 'PKG_MAGO_PESSE.GetMisureCGLink',
                                pForceLogFile => TRUE);
    --pMisTab := t_Misure();
    --delete from gttd_misure_link;
    execute immediate 'truncate table misure_link';
    PKG_Logs.TraceLog('Truncate Table MISURE_LINK - '||TO_CHAR(systimestamp,'dd/mm/yyyy hh24:mi:ss'),PKG_UtlGlb.gcTrace_VRB);
    
    pkg_misure.GetMisureCG(vLst,pGestElem,pDataDa,pDataA, pTipiMisura, pOrganizzazione, pStatoRete, pTipologiaRete, pFonte,pTipoClie,pAgrTemporale, pDisconnect);
    loop
        fetch vLst into vCodMis, vData, vValore,  vQualita, vTipoFonte, vinter;
        exit when vLst%notfound;
        insert into misure_link values (vCodMis, vData, vValore,  vQualita, vTipoFonte, vinter);          
        PKG_Logs.TraceLog('Insert row - '||'cod mis: '||vCodMis||' Data:'||to_char(vData,'ddmmyyyy hh24:mi:ss')||' valore:'||vValore,PKG_UtlGlb.gcTrace_VRB);

    end loop;
    
    close vLst;
    commit;
END;

END PKG_MAGO_PESSE;

/