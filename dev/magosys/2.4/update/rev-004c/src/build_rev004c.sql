SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 4c
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Procedures
PROMPT
@./Procedures/SP_PULIZIA_PERIODICA_MISURE.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 4c
PROMPT =======================================================================================

SPOOL OFF
