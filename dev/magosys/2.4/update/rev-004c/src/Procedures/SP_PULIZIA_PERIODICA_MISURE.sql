PROMPT procedure SP_PULIZIA_PERIODICA_MISURE


create or replace procedure SP_PULIZIA_PERIODICA_MISURE
 as

  --v_yhigh   user_tab_partitions.high_value%TYPE;
  v_log     VARCHAR2(32) := 'MAGOpartitionDrop.log';
  v_maxPeriod DATE;
  p_mesi_storicizzati NUMBER := 0;

BEGIN

   pkg_logs.tracelog(ptrace=>'************* SP_PULIZIA_PERIODICA_MISURE  - START - *************');

   /*se  parametro assente o valorizzato uguale a 0, nessuna operazione eseguita*/
   BEGIN
    select nvl(trim(VALORE_PARAMETRO),'0')
    into p_mesi_storicizzati
    from SAR_ADMIN.PARAMETRI_GENERALI
    where COD_APPLICAZIONE = 'MAGO'
    and nome_parametro = 'MESI_MISURE_STORICIZZATI';

    Exception when no_data_found then
       p_mesi_storicizzati := 0  ;
    when others then
       Return;
   END;


   pkg_logs.tracelog(ptrace=>'MESI_MISURE_STORICIZZATI ' ||p_mesi_storicizzati );


   IF p_mesi_storicizzati <> 0 THEN


       select to_date('01' || to_char (add_months ((sysdate),-(p_mesi_storicizzati+1)) ,'mmyyyy'),'dd-mm-yyyy',  'nls_date_language=italian')
         into v_maxPeriod
         from dual;

      pkg_logs.tracelog(ptrace=>'Mese soglia :'||v_maxPeriod);

      pkg_logs.tracelog(ptrace=>'***************************************** user_tab_partitions');

      FOR crec IN (
                  select  table_name, partition_name ,
                          case WHEN  instr(partition_name,'X') =0 THEN
                                            to_date('01' ||to_char(to_date(substr(partition_name,-7,3), 'MON', 'nls_date_language=italian'),'mm', 'nls_date_language=italian') || substr(partition_name,-4,4),'dd-mm-yyyy')
                                            ELSE to_date('31/12/3000','dd-mm-yyyy', 'nls_date_language=italian')       END AS  meseAnno   
                     from user_tab_partitions
                    where tablespace_name = 'MAGO_IOT'
                    order by 1,3
                     )
      LOOP


        IF instr(crec.partition_name,'X') > 0 THEN

          null; --dbms_output.put_line('___ NESSUNA OPERAZIONE  '||crec.partition_name||'...'||TO_CHAR(sysdate,'dd hh24:mi:ss'));


        ELSIF  crec.meseAnno <= v_maxPeriod THEN --v_maxPeriod
          pkg_logs.tracelog(ptrace=>'Inizio....');
          dbms_output.put_line( crec.table_name ||'.'|| crec.partition_name  || ' anno = '||crec.meseAnno);
          dbms_output.put_line('___ DROPPING partition '||crec.partition_name||' ...'||TO_CHAR(sysdate,'dd hh24:mi:ss'));

          pkg_logs.tracelog(ptrace=>crec.table_name ||'.'|| crec.partition_name  ||' anno = '||crec.meseAnno);
          pkg_logs.tracelog(ptrace=>'   DROPPING partition '||crec.partition_name||' ...');

          EXECUTE IMMEDIATE 'ALTER TABLE '||crec.table_name ||'  DROP PARTITION '||crec.partition_name;

          dbms_output.put_line('___ partition '||crec.partition_name||' dropped '||TO_CHAR(sysdate,'dd hh24:mi:ss'));
          pkg_logs.tracelog(ptrace=>'       ... partition '||crec.partition_name||' dropped');

       ELSE
            null; --dbms_output.put_line('___ NESSUNA OPERAZIONE '||crec.partition_name|| ' ' || TO_CHAR(sysdate,'dd hh24:mi:ss'));

       END IF;

      END LOOP;
      
      -- parte IOT
      pkg_logs.tracelog(ptrace=>'***************************************** user_segments');

      for  crec in (
    SELECT segment_name,partition_name, segment_type,anno,sum(bytes)/power(2,20)  mb_size,table_name,meseAnno
                       FROM (
                       SELECT  segment_name,partition_name, segment_type, i.table_name,
                                    CASE WHEN instr(partition_name,'X') = 0 THEN
                                         to_number(substr(partition_name,-4,4),'fm0000')
                                    END AS  anno,
                                    bytes, partition_name sss
                                    ,
                                    CASE WHEN instr(partition_name,'X') = 0 and instr(partition_name,'Y') = 0 THEN
                                         to_date('01' ||to_char(to_date(substr(partition_name,-7,3), 'MON', 'nls_date_language=italian'),'mm', 'nls_date_language=italian') || substr(partition_name,-4,4),'dd-mm-yyyy', 'nls_date_language=italian')
                                          ELSE
                                          to_date('31/12/3000','dd-mm-yyyy', 'nls_date_language=italian')       END AS meseAnno
                               FROM user_segments s
                         INNER JOIN user_indexes i on s.segment_name= i.index_name
                             WHERE s.tablespace_name = 'MAGO_IOT'
                             AND partition_name IS NOT null
                             )
                     GROUP BY  segment_name,partition_name, segment_type, anno,table_name
                     ORDER BY 1,meseAnno
                     )
      LOOP


        --dbms_output.put_line( crec.table_name ||'.'|| crec.partition_name  ||' anno = '||crec.anno);
        IF instr(crec.partition_name,'X') > 0 and instr(crec.partition_name,'Y') = 0 THEN

          NULL; --dbms_output.put_line('___ NESSUNA OPERAZIONE '||crec.partition_name||'...'||TO_CHAR(sysdate,'dd hh24:mi:ss'));


        ELSIF crec.meseAnno <= v_maxPeriod THEN
          dbms_output.put_line( crec.table_name ||'.'|| crec.partition_name  ||' anno = '||crec.meseAnno);
          dbms_output.put_line('___ DROPPING partition '||crec.partition_name||' ...'||TO_CHAR(sysdate,'dd hh24:mi:ss'));

          pkg_logs.tracelog(ptrace=>crec.table_name ||'.'|| crec.partition_name  ||' anno = '||crec.meseAnno);
          pkg_logs.tracelog(ptrace=>'   DROPPING partition '||crec.partition_name||' ...');

          EXECUTE IMMEDIATE 'ALTER TABLE '||crec.table_name ||'  DROP PARTITION '||crec.partition_name;

          pkg_logs.tracelog(ptrace=>'       ... partition '||crec.partition_name||' dropped '||TO_CHAR(sysdate,'dd hh24:mi:ss'));
          dbms_output.put_line('        ... partition '||crec.partition_name||' dropped '||TO_CHAR(sysdate,'dd hh24:mi:ss'));

       ELSE
          null; --dbms_output.put_line('___ NESSUNA OPERAZIONE '||crec.partition_name||TO_CHAR(sysdate,'dd hh24:mi:ss'));

       END IF;

      END LOOP;

    ELSE

        pkg_logs.tracelog(ptrace=>'Paramento MESI_MISURE_STORICIZZATI = 0 - Nessuna operazione eseguita - ');

    END IF;


   pkg_logs.tracelog(ptrace=>'************* SP_PULIZIA_PERIODICA_MISURE  - END   - *************');

    EXCEPTION WHEN others THEN rollback; raise;

END;

/