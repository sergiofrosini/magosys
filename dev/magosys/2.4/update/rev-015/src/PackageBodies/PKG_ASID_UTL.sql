PROMPT PACKAGE BODY PKG_ASID_UTL 

create or replace PACKAGE BODY PKG_ASID_UTL AS

/* ========================================================================== */

  gc_ASID2018 CONSTANT VARCHAR2(8) := 'ASID2018';

  gd_ASID2018 DATE := NULL;

/* ========================================================================== */

  function f_ASID2018 return varchar2 AS begin return gc_ASID2018; END;

/* ========================================================================== */

  function f_GetData(pTipo IN VARCHAR2) return date AS

    vData date;

  BEGIN

        SELECT data_validita
        into vData
        from ricodifica_elementi@PKG1_STMAUI.IT
        where tipo_ricodifica = pTipo
        and rownum = 1;

    --   vData := to_DATE('20112018','ddmmyyyy'); --fake pe rle prove

        return vData;
  END f_GetData;

/* ========================================================================== */

  procedure sp_GetData(pDataOut IN OUT date, pTipo IN VARCHAR2) AS
  BEGIN
    pDataOut := f_GetData(ptipo);
  END sp_GetData;

/* ========================================================================== */

  function f_GetDataASID2018 return date AS
  BEGIN

      IF gd_ASID2018 IS NULL THEN
        gd_ASID2018 := f_GetData(f_ASID2018);
      END IF;

      return gd_ASID2018;

  END f_GetDataASID2018;

/* ========================================================================== */

  procedure sp_GetDataASID2018(pDataOut IN OUT date) AS
  BEGIN
    pDataOut := f_GetDataASID2018;
  END sp_GetDataASID2018;

/* ========================================================================== */


/* MODIFICHE ASID22 */


function f_GetDataAsidDA( pDataIn IN date) return date as 
 vData date;
 begin
 
  select DATA_VALIDITA INTO vData from V_ASID_ELEMENTS 
  where DATA_VALIDITA = (SELECT MAX(DATA_VALIDITA) FROM V_ASID_ELEMENTS where DATA_VALIDITA <= pDataIn );
  
  return vData;
 end;
 
function f_GetLastAsidData return date as 
  vData date;
 begin
  SELECT  DATA_VALIDITA 
        into vData
         from V_ASID_ELEMENTS WHERE ROWNUM=1 order by DATA_VALIDITA DESC;
        return vData;
 end;
 
function f_GetLastAsidCode return varchar2 as 
  vCode varchar2(50);
begin
  SELECT TIPO_RICODIFICA
    into vCode
   from V_ASID_ELEMENTS WHERE ROWNUM=1 order by DATA_VALIDITA DESC;
    
    return vCode;
end;

  
function f_GetExistAsidTable return int as 
 vCntTable int;
 
 begin
   select Count(*) into vCntTable from ALL_OBJECTS
	where OBJECT_TYPE = 'TABLE' and OBJECT_NAME = 'ANAGRAFICA_ASID';
    
   if vCntTable = 0 then
      return 0;
   else
      return 1;
   end if;
 end;

function f_GetDenominazioneAsid(code varchar2) return varchar2 as 
 vDenominazione VARCHAR2 (50);
 vsql_qry       VARCHAR2(150);
 begin
  
   vsql_qry := 'SELECT DENOMINAZIONE_ASID FROM SAR_ADMIN.ANAGRAFICA_ASID WHERE ASID_CODE = ''' || code || '''';
     
   BEGIN   
   
      if f_GetExistAsidTable()=1 then
         EXECUTE IMMEDIATE vsql_qry INTO vDenominazione;  
	 if vDenominazione is null THEN
		vDenominazione := '';
	 end if;
      else
         vDenominazione := '';
      end if;
       
    EXCEPTION  WHEN OTHERS THEN
       vDenominazione := '';
   END;
   
   return vDenominazione;
   
 end;
 

 procedure sp_GetLastASIDData(pDataOut IN OUT date) AS
  BEGIN
    pDataOut := f_GetLastAsidData;
  END sp_GetLastASIDData;

 procedure sp_GetASID2022Data(pDataOut IN OUT date) AS
  vDenominazione VARCHAR2 (50);
  vDateOut date;
    BEGIN
	select to_date('01/01/3000','dd/mm/yyyy') into vDateOut from dual;
        
	if f_GetExistAsidTable() = 0 then
		pDataOut := vDateOut;
        else
		vDenominazione := f_GetDenominazioneAsid('02');
		if nvl(length(vDenominazione),0) > 1 then
			pDataOut := f_GetData(vDenominazione);
		ELSE
			pDataOut := vDateOut;
		end if;
        end if;       
    END sp_GetASID2022Data;

END PKG_ASID_UTL;
/







show errors;