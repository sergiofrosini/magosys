PROMPT PACKAGE PKG_ASID_UTL

create or replace PACKAGE PKG_ASID_UTL AS 

 function f_ASID2018 return varchar2;

 function f_GetData( pTipo IN VARCHAR2) return date;
 procedure sp_GetData(pDataOut IN OUT date, pTipo IN VARCHAR2);
 
 function f_GetDataASID2018 return date;
 procedure sp_GetDataASID2018(pDataOut IN OUT date);

 function f_GetDataAsidDA( pDataIn IN date) return date;
 function f_GetLastAsidData return date;
 function f_GetLastAsidCode return varchar2;
 procedure sp_GetLastASIDData(pDataOut IN OUT date);
 procedure sp_GetASID2022Data(pDataOut IN OUT date);

 function f_GetDenominazioneAsid(code IN varchar2) return varchar2; 

END PKG_ASID_UTL;
/