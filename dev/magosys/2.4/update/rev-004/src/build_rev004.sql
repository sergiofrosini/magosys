SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 4
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2


disconn

PROMPT =======================================================================================
PROMPT FINE   mago 2.4 rev 4
PROMPT =======================================================================================

SPOOL OFF
