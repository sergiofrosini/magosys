CREATE OR REPLACE PACKAGE PKG_MISURE_NOWCAST AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.4 rev 011
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/




/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */
 TYPE t_CodTable IS TABLE OF NUMBER; 

 cUnSecondo         CONSTANT NUMBER  := (1 / 1440) / 60;

-- ----------------------------------------------------------------------------------------------------------
-- codici Log
-- ----------------------------------------------------------------------------------------------------------

 gkInfo           CONSTANT NUMBER := 001; -- informazione
 gkWarning        CONSTANT NUMBER := 002; -- informazione
 gkAnagrNonPres   CONSTANT NUMBER := 101; -- non definito in anagrafica
 gkAnagrIncompl   CONSTANT NUMBER := 102; -- anagrafica incompleta
 gkAnagrIncong    CONSTANT NUMBER := 103; -- anagrafica incongruente

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */
function gRegExpSepara RETURN VARCHAR2 ;

-- ----------------------------------------------------------------------------------------------------------
PROCEDURE sp_ClonaMisureNowcast(pRefCurs      	 OUT PKG_UtlGlb.t_query_cur,
                               pCodElemTable     IN t_CodTable,
                               pDataInit      	 IN DATE,
                               pDataTras       	 IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipiMisura       IN VARCHAR2 DEFAULT 'PPAGNC|PPRGNC',
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                               );

PROCEDURE sp_ClonaMisureNowcastCG(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pCodGestList      IN T_COD_GEST_ARRAY,
                               pDataInit      	 IN DATE,
                               pDataTras       	 IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipiMisura       IN VARCHAR2 DEFAULT 'PPAGNC|PPRGNC',
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                               );
                               
PROCEDURE sp_ClonaPMPNowcast(pRefCurs      		 OUT PKG_UtlGlb.t_query_cur,
                               pCodElemTable     IN t_CodTable,
                               pDataTras      	 IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                         );


PROCEDURE sp_ClonaPMPNowcastCG(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                               pCodGestList   IN T_COD_GEST_ARRAY,
                               pDataTras      IN DATE);


PROCEDURE sp_GetMisureNowcast(pRefCurs        OUT PKG_UtlGlb.t_query_cur,
							pCodElemTable     IN PKG_MISURE_NOWCAST.t_CodTable,
							pData             IN DATE,
							pTipiMisura       IN VARCHAR2,
							pOrganizzazione   IN INTEGER,
							pStatoRete        IN INTEGER,
							pTipologiaRete    IN VARCHAR2,
							pFonte            IN VARCHAR2,
							pTipoClie         IN VARCHAR2);

PROCEDURE sp_GetMisureNowcastPerimeter(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
									pGestElem         IN VARCHAR2,
									pElemTypeReq      IN VARCHAR2,
									pData             IN DATE,
									pTipiMisura       IN VARCHAR2,
									pOrganizzazione   IN INTEGER,
									pStatoRete        IN INTEGER,
									pTipologiaRete    IN VARCHAR2,
									pFonte            IN VARCHAR2,
									pTipoClie         IN VARCHAR2,
									pDisconnect       IN INTEGER DEFAULT 0);

END PKG_MISURE_NOWCAST;
/