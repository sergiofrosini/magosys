CREATE OR REPLACE PACKAGE BODY PKG_MISURE_NOWCAST AS


/* ***********************************************************************************************************
   NAME:       PKG_MISURE
   PURPOSE:    Servizi per la gestione delle Misure

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
      NOTES:
*********************************************************************************************************** */
gv_RegExpSepara CONSTANT VARCHAR2(10) := '[^\|]+';



    gTmpTipMisExt GTTD_VALORI_TEMP.TIP%TYPE := 'TIPMISEXT';
    gTmpElementi  GTTD_VALORI_TEMP.TIP%TYPE := 'ELEMENTI';
    gTmpElemErr  GTTD_VALORI_TEMP.TIP%TYPE := 'ELEMERR';
    gTmpCloneNC  GTTD_VALORI_TEMP.TIP%TYPE := 'CLONENC';
    gTmpElemPMP  GTTD_VALORI_TEMP.TIP%TYPE := 'ELEMPMP';
    gTmpElemGeTMis  GTTD_VALORI_TEMP.TIP%TYPE := 'ELEMGMIS';

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gMisura                   PKG_GestAnagr.t_DefAnagr;
 --gUltimoTipoMisura         TIPI_MISURA.COD_TIPO_MISURA%TYPE := '*';
 gPrevData                 DATE := PKG_UtlGlb.gkDataTappo;

 --cTotalizza       CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagON);   -- Totalizza misure per tipo codice
 --cNonTotalizzare  CONSTANT CHAR(1) := TO_CHAR(PKG_UtlGlb.gkFlagOFF);  -- Per il tipo codice non totalizzare le misure

 gGestElem                   ELEMENTI.COD_GEST_ELEMENTO%TYPE;

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */
function gRegExpSepara RETURN VARCHAR2 IS BEGIN RETURN gv_RegExpSepara; END;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
       DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
       DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

PROCEDURE sp_ClonaMisureNowcast(pRefCurs      	 OUT PKG_UtlGlb.t_query_cur,
							   pCodElemTable     IN t_CodTable,
                               pDataInit      	 IN DATE,
                               pDataTras         IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipiMisura       IN VARCHAR2 DEFAULT 'PPAGNC|PPRGNC',
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                               ) AS


    VeRRMES VARCHaR2(2000);

BEGIN

--
-- campi nella GTTD_VALORI_TEMP 
--
-- gTmpElementi
-- ALF1  cod gest elemento
-- NUM1  codice_elemento
-- NUM2  trovata misura 0/1
--
--
-- gTmpElemErr
-- ALF1  cod gest elemento
-- ALF3  desc errore (lung campo 2000)
-- 


    -- pulisce tabelle temporanee
    DELETE GTTD_VALORI_TEMP WHERE tip = gTmpElementi;
    DELETE GTTD_VALORI_TEMP WHERE tip = gTmpElemErr;

 FOR i in pCodElemTable.FIRST .. pCodElemTable.LAST
 LOOP

    INSERT INTO GTTD_VALORI_TEMP
    (TIP,ALF1,num1,num2)
    SELECT
    gTmpElementi,COD_GEST_ELEMENTO,COD_ELEMENTO,0
    FROM ELEMENTI
    WHERE COD_ELEMENTO=pCodElemTable(i);

 END LOOP;



 for rigaMisure in (
   SELECT TRele.COD_ELEMENTO,cod_Trattamento_elem, pDataTras data, mis.valore,mis.quality
   FROM (select * from MISURE_ACQUISITE WHERE DATA = pDatAInit) MIS
   JOIN TRATTAMENTO_ELEMENTI trele USING (COD_TRATTAMENTO_ELEM)
   JOIN (SELECT NUM1 CODELE FROM GTTD_VALORI_TEMP WHERE TIP = gTmpElementi) CDE 
   ON (CDE.CODELE=TRele.COD_ELEMENTO)
 WHERE trele.COD_TIPO_FONTE in  ( SELECT T.COD_TIPO_FONTE--, T.COD_RAGGR_FONTE
            FROM (SELECT regexp_substr(pFonte,gRegExpSepara, 1, level) as ALF1 FROM dual
                  CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL) PF
                 INNER JOIN TIPO_FONTI T ON PF.ALF1 = T.COD_RAGGR_FONTE)          
  and trele.cod_tipo_rete IN ( SELECT regexp_substr(pTipologiaRete,gRegExpSepara, 1, level) as COD_TIPO_RETE FROM dual
          CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL )
  and trele.cod_tipo_misura IN ( SELECT regexp_substr(pTipiMisura,gRegExpSepara, 1, level) as cod_tipo_misura FROM dual
          CONNECT BY regexp_substr(pTipiMisura, gRegExpSepara, 1, level) IS NOT NULL )
  and trele.cod_tipo_cliente IN (SELECT regexp_substr(pTipoClie,gRegExpSepara, 1, level) as COD_TIPO_CLIENTE FROM dual
          CONNECT BY regexp_substr(pTipoClie,gRegExpSepara, 1, level) IS NOT NULL )
 ) 
 LOOP

    UPDATE GTTD_VALORI_TEMP
    SET NUM2 = 1 
    WHERE TIP = gTmpElementi
    AND NUM1 = rigaMisure.Cod_elemento;

    BEGIN

        MERGE INTO MISURE_ACQUISITE T USING
         (SELECT
           rigaMisure.cod_Trattamento_elem as cod_Trattamento_elem, 
           rigaMisure.data as data, 
           rigaMisure.valore as valore,
           2 as quality--rigaMisure.quality as quality
          FROM DUAL) S
        ON (T.cod_Trattamento_elem=S.cod_Trattamento_elem
            and t.data = s.data)
        WHEN NOT MATCHED THEN 
        INSERT ( cod_Trattamento_elem, data, valore, quality )
        VALUES ( S.cod_Trattamento_elem, S.data, S.valore, S.quality )
        WHEN MATCHED THEN
        UPDATE SET 
          T.valore = S.valore,
          T.quality = S.quality;

    EXCEPTION
        WHEN others  THEN
            VeRRMES := 'ERR '||SQLCODE ||' - '||SQLERRM||' - '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
            INSERT INTO GTTD_VALORI_TEMP
            (TIP,ALF1,ALF3)
            VALUES
            (gTmpElemErr,rigaMisure.Cod_elemento,VeRRMES);
    END;


  END LOOP;


  INSERT INTO GTTD_VALORI_TEMP
    (TIP,ALF1,ALF3)
    SELECT
    gTmpElemErr,s_err.alf1,'Non trovate misure'
    FROM GTTD_VALORI_TEMP s_err
    WHERE s_err.TIP = GTmpelementi
    AND s_err.NUM2 = 0;


  OPEN pRefCurs FOR SELECT ALF1 COD_GEST_ELEMENTO, ALF3 CAUSALE
  FROM GTTD_VALORI_TEMP
  WHERE TIP=gTmpElemErr;


END sp_ClonaMisureNowcast;


-- =============================================================================


PROCEDURE sp_ClonaMisureNowcastCG(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestList      IN T_COD_GEST_ARRAY,
                               pDataInit      IN DATE,
                               pDataTras       IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipiMisura       IN VARCHAR2 DEFAULT 'PPAGNC|PPRGNC',
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                               ) AS


 vCodList t_CodTable;

BEGIN

 vCodList := new t_CodTable();

 FOR i in pCodGestList.FIRST .. pCodGestList.LAST
 LOOP

    vCodList.EXTEND(1);
    SELECT COD_ELEMENTO 
    INTO vcodList(vCodlist.LAST)
    FROM ELEMENTI
    WHERE COD_GEST_ELEMENTO = pCodGestList(i).COD_GEST;

 END LOOP;

 sp_ClonaMisureNowcast(pRefCurs ,
                               vCodList,    -- CODICI ELEMENTO
                               pDataInit,
                               pDataTras,
                               pOrganizzazione,
                               pStatoRete,
                               pTipiMisura,
                               pTipologiaRete,
                               pFonte,
                               pTipoClie,
                               pAgrTemporale ,
                               pDisconnect,
                               pdeltaT );

END sp_ClonaMisureNowcastCG;

-- =============================================================================

-- =============================================================================

PROCEDURE sp_ClonaPMPNowcast (pRefCurs      	 OUT PKG_UtlGlb.t_query_cur,
                               pCodElemTable     IN t_CodTable,
                               pDataTras      	 IN DATE,
                               pOrganizzazione   IN INTEGER DEFAULT PKG_MAGO.gcOrganizzazELE,
                               pStatoRete        IN INTEGER DEFAULT PKG_MAGO.gcStatoAttuale,
                               pTipologiaRete    IN VARCHAR2 DEFAULT 'M|B',
                               pFonte            IN VARCHAR2 DEFAULT 'S',
                               pTipoClie         IN VARCHAR2 DEFAULT 'A|B|X|Z',
                               pAgrTemporale     IN INTEGER DEFAULT 15,
                               pDisconnect       IN INTEGER DEFAULT 0,
                               pdeltaT           IN INTEGER DEFAULT 7
                         ) as
                            
--PDATATRAS � il parametro in ingresso di riferimento per la generazione delle misure nowcast a partire dalla potenza meteo prevista.
--
--Esempio
--dato il parametro di ingresso PDATATRAS valorizzato come 30/11/2020 20:34
--l'algoritmo preleva la misura della PMP delle 20.34 interpolata nel range tra 20:00 e le 21:00
--
--In particolare ricava
--PPAGNC = PMP*(20:34)
--PPRGNC = PMP*(20:34) * NOWCAST_K_CONSTANT_VALUE
--
--dove PMP*(20:34) sta per misura della PMP interpolata all'instante 20:34
--

    VeRRMES VARCHaR2(2000);
    
    vTipoMisIn TIPI_MISURa.COD_TIPO_MISURa%TYPE := 'PMP';

    vTipoMisOut1 TIPI_MISURa.COD_TIPO_MISURa%TYPE := 'PPAGNC';
    vTipoMisOut2 TIPI_MISURa.COD_TIPO_MISURa%TYPE := 'PPRGNC';

    vTreleOut1 NUMBER;
    vTreleOut2 NUMBER;
    
    vOra0 DATE;
    vOra1 DATE;
    
    vValInterp NUMBER;
    
    v_param_nowcast number;
BEGIN

--   
-- campi nella GTTD_VALORI_TEMP 
--
-- gTmpElemPmp
-- ALF1  cod gest elemento
-- ALF2 cod_tipo_elemento
-- NUM1  codice_elemento
-- NUM2  trovata misura 0/1
--
--
-- gTmpElemErr
-- ALF1  cod gest elemento
-- ALF3  desc errore (lung campo 2000)
-- 
    --DBMS_OUTPUT.PUT_LINE('Start nowcast');
      BEGIN
        select TO_NUMBER(nvl(trim(VALORE_PARAMETRO),'1'),
                        '99999999999999999999D99999999999999999999',
                        'NLS_NUMERIC_CHARACTERS=''.,''')
        into v_param_nowcast
        from SAR_ADMIN.PARAMETRI_GENERALI
        where COD_APPLICAZIONE = 'MAGO'
        and nome_parametro = 'NOWCAST_K_CONSTANT';

        Exception when no_data_found then
           v_param_nowcast := 1  ;
        when others then
           return  ;
       END;

    -- pulisce tabelle temporanee
    DELETE GTTD_VALORI_TEMP WHERE tip = gTmpElemPMP;
    DELETE GTTD_VALORI_TEMP WHERE tip = gTmpElemErr;


    vOra0 := TRUNC(pDataTras, 'HH'); -- TODO
    vOra1 := vOra0 + 1/24;


 FOR i in pCodElemTable.FIRST .. pCodElemTable.LAST
 LOOP
    INSERT INTO GTTD_VALORI_TEMP
    (TIP,ALF1,num1,ALF2,num2)
    SELECT
    gTmpElemPMP,COD_GEST_ELEMENTO,COD_ELEMENTO,cod_tipo_elemento,0
    FROM ELEMENTI
    WHERE COD_ELEMENTO=pCodElemTable(i);

 END LOOP;



 for rigaMisure in (
        SELECT 
            TRele.COD_ELEMENTO,
            TREle.COd_TIPO_FONTE,
            TREle.cod_tipo_rete,
            TREle.cod_tipo_cliente,
            TREle.cod_tipo_misura,
            CDE.cod_tipo_elemento,
            mis0.valore val0,
            mis0.quality qual0, 
            mis1.valore val1,
            mis1.quality qual1 FROM 
        (select * from MISURE_ACQUISITE WHERE DATA = vOra0) MIS0
        JOIN (select * from MISURE_ACQUISITE WHERE DATA = vOra1) MIS1 USING (COD_TRATTAMENTO_ELEM)
       JOIN TRATTAMENTO_ELEMENTI trEle USING (COD_TRATTAMENTO_ELEM)
       JOIN (SELECT NUM1 CODELE, ALF2 COD_TIPO_ELEMENTO FROM GTTD_VALORI_TEMP WHERE TIP = gTmpElemPMP) CDE    
       ON (CDE.CODELE=trEle.COD_ELEMENTO)
      WHERE trele.COD_TIPO_FONTE in  ( SELECT T.COD_TIPO_FONTE--, T.COD_RAGGR_FONTE
                FROM (SELECT regexp_substr(pFonte,gRegExpSepara, 1, level) as ALF1 FROM dual
                      CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL) PF
                     INNER JOIN TIPO_FONTI T ON PF.ALF1 = T.COD_RAGGR_FONTE)          
      and trele.cod_tipo_rete IN ( SELECT regexp_substr(pTipologiaRete,gRegExpSepara, 1, level) as COD_TIPO_RETE FROM dual
              CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL )
      and trele.cod_tipo_cliente IN (SELECT regexp_substr(pTipoClie,gRegExpSepara, 1, level) as COD_TIPO_CLIENTE FROM dual
              CONNECT BY regexp_substr(pTipoClie,gRegExpSepara, 1, level) IS NOT NULL )
      and trele.cod_tipo_misura IN ( vTipoMisIn )  
  ) 
 LOOP
--    DBMS_OUTPUT.PUT_LINE('TROVATA MISURA '||rigaMisure.COD_ELEMENTO||
--                         'TIPO FONTE '||rigaMisure.COd_TIPO_FONTE||
--                         'TIPO MISURA '||rigaMisure.cod_tipo_misura||
--                        'TIPO ELEMENTO '||rigaMisure.cod_tipo_elemento||
--                        'VAL0 '|| rigaMisure.val0||
--                        'QUAL0 '|| rigaMisure.qual0|| 
--                        'VAL1 '|| rigaMisure.val1||
--                        'QUAL1 '|| rigaMisure.qual1);
            
    UPDATE GTTD_VALORI_TEMP
    SET NUM2 = 1 
    WHERE TIP = gTmpElementi
    AND NUM1 = rigaMisure.Cod_elemento;
    
    -- calcolo valore interpolato
    vValInterp := rigaMisure.val0 + ((rigaMisure.val1 - rigaMisure.val0)*(to_number(to_char(pdatatras,'mi'))))/60;

    -- clona PPAGNC

    vTreleOut1 := PKG_MISURE.GetTrattamentoElemento(rigaMisure.COD_ELEMENTO,
                                                rigaMisure.COd_TIPO_ELEMENTO,
                                                vTipoMisOut1,
                                                rigaMisure.COd_TIPO_FONTE,
                                                rigaMisure.cod_tipo_rete,
                                                rigaMisure.cod_tipo_cliente,
                                                 pOrganizzazione,
                                                 0);

    BEGIN

        MERGE INTO MISURE_ACQUISITE T USING
         (SELECT
           vTreleOut1 as cod_Trattamento_elem, 
           pDataTras as data, 
           vValInterp as valore,
           3 as quality--rigaMisure.quality as quality
          FROM DUAL) S
        ON (T.cod_Trattamento_elem=S.cod_Trattamento_elem
            and t.data = s.data)
        WHEN NOT MATCHED THEN 
        INSERT ( cod_Trattamento_elem, data, valore, quality )
        VALUES ( S.cod_Trattamento_elem, S.data, S.valore, S.quality )
        WHEN MATCHED THEN
        UPDATE SET 
          T.valore = S.valore,
          T.quality = S.quality;

    EXCEPTION
        WHEN others  THEN
            VeRRMES := 'ERR '||SQLCODE ||' - '||SQLERRM||' - '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
            INSERT INTO GTTD_VALORI_TEMP
            (TIP,ALF1,ALF3)
            VALUES
            (gTmpElemErr,rigaMisure.Cod_elemento,VeRRMES);
    END;


    -- clona PPRGNC

    vTreleOut2 := PKG_MISURE.GetTrattamentoElemento(rigaMisure.COD_ELEMENTO,
                                                rigaMisure.COd_TIPO_ELEMENTO,
                                                vTipoMisOut2,
                                                rigaMisure.COd_TIPO_FONTE,
                                                rigaMisure.cod_tipo_rete,
                                                rigaMisure.cod_tipo_cliente,
                                                 pOrganizzazione,
                                                 0);



    BEGIN

        MERGE INTO MISURE_ACQUISITE T USING
         (SELECT
           vTreleOut2 as cod_Trattamento_elem, 
           pDataTras as data, 
           vValInterp*v_param_nowcast as valore,
           3 as quality--rigaMisure.quality as quality
          FROM DUAL) S
        ON (T.cod_Trattamento_elem=S.cod_Trattamento_elem
            and t.data = s.data)
        WHEN NOT MATCHED THEN 
        INSERT ( cod_Trattamento_elem, data, valore, quality )
        VALUES ( S.cod_Trattamento_elem, S.data, S.valore, S.quality )
        WHEN MATCHED THEN
        UPDATE SET 
          T.valore = S.valore,
          T.quality = S.quality;

    EXCEPTION
        WHEN others  THEN
            VeRRMES := 'ERR '||SQLCODE ||' - '||SQLERRM||' - '||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE;
            INSERT INTO GTTD_VALORI_TEMP
            (TIP,ALF1,ALF3)
            VALUES
            (gTmpElemErr,rigaMisure.Cod_elemento,VeRRMES);
    END;


  END LOOP;


  INSERT INTO GTTD_VALORI_TEMP
    (TIP,ALF1,ALF3)
    SELECT
    gTmpElemErr,s_err.alf1,'Non trovate misure'
    FROM GTTD_VALORI_TEMP s_err
    WHERE s_err.TIP = GTmpelementi
    AND s_err.NUM2 = 0;


  OPEN pRefCurs FOR SELECT ALF1 COD_GEST_ELEMENTO, ALF3 CAUSALE
  FROM GTTD_VALORI_TEMP
  WHERE TIP=gTmpElemErr;


end sp_ClonaPMPNowcast;

-- =============================================================================

PROCEDURE sp_ClonaPMPNowcastCG (pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestList   IN T_COD_GEST_ARRAY,
                               pDataTras      IN DATE) as

 vCodList t_CodTable;

BEGIN

 vCodList := new t_CodTable();

 FOR i in pCodGestList.FIRST .. pCodGestList.LAST
 LOOP

    vCodList.EXTEND(1);
    SELECT COD_ELEMENTO 
    INTO vcodList(vCodlist.LAST)
    FROM ELEMENTI
    WHERE COD_GEST_ELEMENTO = pCodGestList(i).COD_GEST;

 END LOOP;

 sp_ClonaPMPNowcast(pRefCurs ,
                    vCodList,
                    pDataTras);

end sp_ClonaPMPNowcastCG;


-- =============================================================================


PROCEDURE sp_GetMisureNowcast(pRefCurs        OUT PKG_UtlGlb.t_query_cur,
							pCodElemTable     IN PKG_MISURE_NOWCAST.t_CodTable,
							pData             IN DATE,
							pTipiMisura       IN VARCHAR2,
							pOrganizzazione   IN INTEGER,
							pStatoRete        IN INTEGER,
							pTipologiaRete    IN VARCHAR2,
							pFonte            IN VARCHAR2,
							pTipoClie         IN VARCHAR2)
AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce un cursore con le curve misura richieste partendo da codice elemento numerico
-----------------------------------------------------------------------------------------------------------*/
 
 v_cod_elem VARCHAR2(32767);
 vTipoMisuraList   	PKG_UtlGlb.t_SplitTbl;
 vTipoFonteList   	PKG_UtlGlb.t_SplitTbl;
 
 /*
	campi nella GTTD_VALORI_TEMP 

	TIP:	gTmpElemGetMis
	NUM1: 	CODICE_ELEMENTO
	ALF1: 	CODICE_MISURA
	ALF2: 	TIPO_FONTE
 */
 
 BEGIN

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP=gTmpElemGetMis;
    
	vTipoMisuraList := PKG_UtlGlb.SplitString(pTipiMisura, Pkg_Mago_utl.cSepCharLst);
	vTipoFonteList := PKG_UtlGlb.SplitString(pFonte, Pkg_Mago_utl.cSepCharLst);
	
	IF pCodElemTable IS NOT NULL AND pCodElemTable.COUNT > 0 THEN
		FOR i in pCodElemTable.FIRST .. pCodElemTable.last 
		LOOP
			FOR misIdx in vTipoMisuraList.FIRST .. vTipoMisuraList.LAST 
			LOOP
				FOR fonteIdx in vTipoFonteList.FIRST .. vTipoFonteList.LAST 
				LOOP
					INSERT INTO GTTD_VALORI_TEMP (tip, NUM1, ALF1, ALF2)
					VALUES (gTmpElemGetMis, pCodElemTable(i), vTipoMisuraList(misIdx), vTipoFonteList(fonteIdx));
				END LOOP;
			END LOOP;
			

		END LOOP;
	END IF;
	
	OPEN pRefCurs FOR
	SELECT ROWNUM as ROW_ID, ELE.COD_GEST_ELEMENTO as ROOT_GEST_CODE, 
		NVL(TRele.COD_TIPO_MISURA, GTTV.ALF1) as COD_TIPO_MISURA,
		NVL(mis.DATA, pData) as DATA,
		mis.VALORE,
		NVL(TRele.COD_TIPO_FONTE, GTTV.ALF2) as COD_TIPO_FONTE,
		mis.quality,
		0 INTERPOLAZIONE
	FROM GTTD_VALORI_TEMP GTTV
	JOIN ELEMENTI ELE ON GTTV.NUM1 = ELE.COD_ELEMENTO
	JOIN ELEMENTI_DEF ELEDEF ON ELEDEF.COD_ELEMENTO = ELE.COD_ELEMENTO
	LEFT JOIN TRATTAMENTO_ELEMENTI trele 
		ON ELE.COD_ELEMENTO = trele.COD_ELEMENTO 
		AND trele.COD_TIPO_MISURA = GTTV.ALF1
		AND trele.COD_TIPO_FONTE IN (
			SELECT COD_TIPO_FONTE 
			FROM TIPO_FONTI
			WHERE COD_RAGGR_FONTE = GTTV.ALF2
		)
		AND trele.cod_tipo_rete IN ( SELECT regexp_substr(pTipologiaRete,gRegExpSepara, 1, level) as COD_TIPO_RETE FROM dual
		  CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL )
		AND trele.tipo_aggregazione = 0 -- per le solo misure acquisite
	LEFT JOIN MISURE_ACQUISITE MIS 
		ON MIS.COD_TRATTAMENTO_ELEM = trele.COD_TRATTAMENTO_ELEM
			AND MIS.data = pData
	WHERE 
		(
			pTipoClie IS NULL 
			OR ELEDEF.cod_tipo_cliente IN (
			SELECT regexp_substr(pTipoClie,gRegExpSepara, 1, level) as COD_TIPO_CLIENTE FROM dual
			 CONNECT BY regexp_substr(pTipoClie,gRegExpSepara, 1, level) IS NOT NULL)
		) 
		AND 
		(	
			pTipoClie IS NULL 
			OR ( 
				SELECT MAX(NVL(M.VALORE,0)) 
				FROM TRATTAMENTO_ELEMENTI T 
				JOIN MISURE_AGGREGATE_STATICHE M ON M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM
				WHERE T.COD_ELEMENTO = ELE.COD_ELEMENTO 
				AND T.TIPO_AGGREGAZIONE IN (0, pStatoRete)
				AND T.COD_TIPO_MISURA = Pkg_Mago_utl.gcPotenzaInstallata
				AND (pData BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE)
			) > 0
		)
		AND 
		(
			pTipoClie IS NULL 
			OR trele.cod_tipo_cliente IS NULL 
			OR trele.cod_tipo_cliente IN (
				SELECT regexp_substr(pTipoClie,gRegExpSepara, 1, level) as COD_TIPO_CLIENTE FROM dual
					CONNECT BY regexp_substr(pTipoClie,gRegExpSepara, 1, level) IS NOT NULL)
		)
		AND pData BETWEEN ELEDEF.DATA_ATTIVAZIONE and ELEDEF.DATA_DISATTIVAZIONE;
		
END sp_GetMisureNowcast;


-- =============================================================================


PROCEDURE sp_GetMisureNowcastPerimeter(
	pRefCurs         OUT PKG_UtlGlb.t_query_cur,
	pGestElem         IN VARCHAR2,
	pElemTypeReq      IN VARCHAR2,
	pData             IN DATE,
	pTipiMisura       IN VARCHAR2,
	pOrganizzazione   IN INTEGER,
	pStatoRete        IN INTEGER,
	pTipologiaRete    IN VARCHAR2,
	pFonte            IN VARCHAR2,
	pTipoClie         IN VARCHAR2,
	pDisconnect       IN INTEGER DEFAULT 0) 
AS
	vCodList 		t_CodTable;
	vPerimeterCurs	PKG_UtlGlb.t_query_cur;
	
	vCodElem		NUMBER;
	vCodGest		VARCHAR2(20);
	vCodTipoElem	VARCHAR2(3);
	vLivello		INTEGER;
	vSequenza		INTEGER;
	
BEGIN

	vCodList := new t_CodTable();

	OPEN vPerimeterCurs FOR 
		SELECT * FROM TABLE(
		  PKG_ELEMENTI.LeggiGerarchiaInf(
			PKG_ELEMENTI.GetCodElemento(pGestElem), pOrganizzazione, 
			pStatoRete, pData, pElemTypeReq, pDisconnect)
		);
 
	LOOP FETCH vPerimeterCurs INTO vCodElem, vCodGest, vCodTipoElem, vLivello, vSequenza;
		EXIT WHEN vPerimeterCurs%NOTFOUND;
		vCodList.EXTEND(1);
		vcodList(vCodlist.LAST) := vCodElem;
	END LOOP;      
	
	sp_GetMisureNowcast(
		pRefCurs, vCodList, pData,
		pTipiMisura, pOrganizzazione, pStatoRete, 
		pTipologiaRete, pFonte, pTipoClie
	);

END sp_GetMisureNowcastPerimeter;


/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_MISURE',
                          pStoreOnFile => FALSE);

END PKG_MISURE_NOWCAST;
/