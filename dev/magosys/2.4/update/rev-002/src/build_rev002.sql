SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 2
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn mago/mago&tns_arcdb2

PROMPT ---------------------------------------------------------------------------------------
PROMPT Stop/Drop old jobs 
PROMPT ---------------------------------------------------------------------------------------
@./SchedulerJob/STOP_SINCRO_TRANSFER_RES.sql
@./SchedulerJob/STOP_SINCRO_TRANSFER_RES_R.sql


PROMPT ---------------------------------------------------------------------------------------
PROMPT Packages
PROMPT ---------------------------------------------------------------------------------------
@./Packages/PKG_METEO.sql

PROMPT ---------------------------------------------------------------------------------------
PROMPT PackageBodies
PROMPT ---------------------------------------------------------------------------------------
@./PackageBodies/PKG_ANAGRAFICHE.sql
@./PackageBodies/PKG_METEO.sql




PROMPT ---------------------------------------------------------------------------------------
PROMPT Triggers 
PROMPT ---------------------------------------------------------------------------------------
@./Triggers/AFT_IUR_ELEMENTI_DEF.sql
     


PROMPT ---------------------------------------------------------------------------------------
PROMPT Restart jobs 
PROMPT ---------------------------------------------------------------------------------------
@./SchedulerJob/SINCRO_TRANSFER_RES.sql
@./SchedulerJob/SINCRO_TRANSFER_RES_R.sql

disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 2
PROMPT =======================================================================================

SPOOL OFF
