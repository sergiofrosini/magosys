PROMPT VIEW V_ESERCIZI_ANTEASID

  CREATE OR REPLACE FORCE VIEW V_ESERCIZI_ANTEASID AS 
  SELECT M.COD_MACRO_AREA COD_MA,
            M.NOME NOM_MA,
            CASE M.COD_MACRO_AREA /* Acronimo Macro Area provvisoriamente valorizzato fittiziamente */
               WHEN 1 THEN 'DANO'
               WHEN 2 THEN 'DANE'
               WHEN 3 THEN 'DAC'
               WHEN 4 THEN 'DAS'
            END
               ACR_MA,
            U.COD_UTR COD_UTR,
            U.CODIFICA_UTR GST_UTR,
            U.NOME NOM_UTR,
            U.ACRONIMO ACR_UTR,
            X.COD_ESERCIZIO COD_ESERCIZIO,
            E.COD_GEST_ELEMENTO GST_ESE,
            D.ID_ELEMENTO ESE,
            D.NOME_ELEMENTO NOM_ESE,
            NVL (L.ACRONIMO, X.ACRONIMO) ACR_ESE,
            D.FLAG ESE_PRIMARIO,
            E.COD_ELEMENTO COD_ESE
       FROM ELEMENTI E
            JOIN ELEMENTI_DEF D
               ON     D.COD_ELEMENTO = E.COD_ELEMENTO
                  AND --SYSDATE BETWEEN D.DATA_ATTIVAZIONE
                                  --AND 
                                  D.DATA_DISATTIVAZIONE<  PKG_ASID_UTL.f_GetDataASID2018
            LEFT OUTER JOIN /* non posso usare CODIFICA_STM perche' spesso mal valorizzata */
                           SAR_UNITA_TERRITORIALI U
               ON U.CODIFICA_UTR = SUBSTR (E.COD_GEST_ELEMENTO, 1, 2)
            LEFT OUTER JOIN SAR_MACRO_AREE M
               ON M.COD_MACRO_AREA = U.COD_MACRO_AREA
            LEFT OUTER JOIN
                SAR_ESERCIZI L
               ON     L.COD_UTR = U.COD_UTR
                  AND L.CODIFICA_ESERCIZIO = D.ID_ELEMENTO
            LEFT OUTER JOIN SAR_ESERCIZI X
               ON     X.COD_UTR = U.COD_UTR
                  AND X.CODIFICA_ESERCIZIO = SUBSTR (E.COD_GEST_ELEMENTO, 3, 2)
      WHERE E.COD_TIPO_ELEMENTO = 'ESE'      /*AND D.ID_ELEMENTO IS NOT NULL*/
   ORDER BY D.FLAG DESC NULLS LAST, D.NOME_ELEMENTO;
 
