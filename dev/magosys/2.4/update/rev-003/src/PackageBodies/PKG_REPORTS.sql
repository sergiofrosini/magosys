PROMPT PACKAGE PKG_REPORTS

create or replace PACKAGE BODY PKG_REPORTS AS
/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.3.2
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

-- ----------------
-- gia' presenti su PKG_REPORTS
--
--

     gcProduttoreAT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CAT'; -- Produttore alta tensione
     gcProduttoreMT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CMT'; -- Produttore media tensione
     gcProduttoreBT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CBT'; -- Produttore bassa tensione


     gcConsi             CONSTANT NUMBER(1) := 1;
     gcStima             CONSTANT NUMBER(1) := 2;

     gcFatt_kW          CONSTANT NUMBER := 1 / 1000;
     --gcFatt_kVA         CONSTANT NUMBER := 1 / 0.9 / 1000;
     gcFatt_MVh         NUMBER;

     gcRepBody          CONSTANT GTTD_VALORI_REP.TIP%TYPE := 'RepBody';

     gcRepValori        CONSTANT GTTD_VALORI_REP.TIP%TYPE := 'TEMPDATREP';



     gOraDesc           VARCHAR2(10);
     gDeltaTime         NUMBER;
     gDataDa            DATE;
     gDataA             DATE;


-- ----------------
-- da aggiungere a PKG_REPORTS
--
--
 gTipRetA           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetM           TIPI_RETE.COD_TIPO_RETE%TYPE;
 gTipRetB           TIPI_RETE.COD_TIPO_RETE%TYPE;

 gTipReteScelti    VARCHAR2(20);



 gTipProdA         TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
 gTipProdB         TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;

 gTipProdScelti    VARCHAR2(20);



 gTipeleFiglio      ELEMENTI.COD_TIPO_ELEMENTO%TYPE;

 gOrganizzazione    NUMBER(1);
 gStatoRete         NUMBER(1);

 SCEGLIELEMENTO      INTEGER := 1;
 SCEGLIFONTE         INTEGER := 2;


 gTipEleMacroarea  CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'MAR';
 gTipEleDirez      CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'DTR';

 gCodPerRigaTotali CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'x';

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */
-- ----------------
-- gia' presenti su PKG_REPORTS
--
--
-- ----------------------------------------------------------------------------------------------------------

     PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
    /*-----------------------------------------------------------------------------------------------------------
        UTILITA' - dbms_output
    -----------------------------------------------------------------------------------------------------------*/
     BEGIN
        IF pLine THEN
            DBMS_OUTPUT.PUT_LINE(pTxt);
        ELSE
            DBMS_OUTPUT.PUT(pTxt);
        END IF;
     END PRINT;

    -- ----------------------------------------------------------------------------------------------------------

     FUNCTION DateDiff(pMask  IN VARCHAR2,
                       pD1    IN DATE,
                       pD2    IN DATE ) RETURN NUMBER AS
    /*-----------------------------------------------------------------------------------------------------------
        restituisce la differenza tra due date in ore o minuti
    -----------------------------------------------------------------------------------------------------------*/
         vRes    NUMBER;
     BEGIN
        SELECT (pD2 - pD1) * CASE UPPER(pMask)
                               WHEN 'MI' THEN 24*60
                               WHEN 'HH' THEN 24
                             END
          INTO vRes
          FROM dual;
        RETURN vRes;
     END;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */
-- ----------------
-- da aggiungere a PKG_REPORTS
--
--
-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisStimaFnte          (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pCodRaggrFonte  IN RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipPrd2        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per l'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
    dumdum      TIPO_FONTI.COD_TIPO_FONTE%TYPE;
 BEGIN

   SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT = pFlagTab
       AND COD_ELEMENTO        = NVL(pCodElemento,COD_ELEMENTO)
       AND COD_RAGGR_FONTE     = NVL(pCodRaggrFonte,COD_RAGGR_FONTE)
       AND COD_TIPO_MISURA     = pTipMisura
       AND COD_TIPO_RETE       = NVL(pTipRete,COD_TIPO_RETE)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN 0;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisStimaFnte;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetMisStima              (pFlagTab        IN INTEGER,
                                    pCodElemento    IN ELEMENTI.COD_ELEMENTO%TYPE,
                                    pTipMisura      IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                    pTipPrd1        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipPrd2        IN TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                    pTipRete        IN TIPI_RETE.COD_TIPO_RETE%TYPE,
                                    pSegno          IN CHAR DEFAULT NULL) RETURN NUMBER AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce il valore della misura per l'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vValore     NUMBER := 0;
    vCount      NUMBER := 0;
 BEGIN
    SELECT SUM(VALORE), COUNT(*)
      INTO vValore,     vCount
      FROM GTTD_REP_ENERGIA_POTENZA
     WHERE TIPO_REPORT          = pFlagTab
       AND COD_ELEMENTO         = NVL(pCodElemento,COD_ELEMENTO)
       AND COD_TIPO_MISURA      = pTipMisura
       AND COD_TIPO_RETE        = NVL(pTipRete,COD_TIPO_RETE)
       AND COD_TIPO_PRODUTTORE IN (pTipPrd1,pTipPrd2)
       AND CASE NVL(pSegno,PKG_Mago.cSeparatore)
             WHEN '+' THEN CASE SEGNO
                             WHEN '+'  THEN 1
                             ELSE           0
                          END
             WHEN '-' THEN CASE SEGNO
                             WHEN '-'  THEN 1
                             ELSE           0
                          END
             ELSE                           1
          END = 1;
    IF vCount = 0 THEN
        RETURN 0;
    ELSE
        RETURN vValore;
    END IF;
 END GetMisStima;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitVarGlob     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pDataDa         IN DATE,
                            pDataA          IN DATE,
                            pOrganizzazione IN NUMBER,
                            pStatoRete      IN NUMBER,
                            pTipologiaRete  IN VARCHAR2,
                            pTipologiaProd  IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vOre            NUMBER;
    vMin            NUMBER;

    vFlgNull        INTEGER;

    vTmp            PKG_UtlGlb.t_SplitTbl;

    TYPE t_Fonti    IS TABLE OF RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE;
    vFonti          t_Fonti;

 BEGIN


    gOrganizzazione := pOrganizzazione;
    gStatoRete      := pStatoRete;


    gTipReteScelti := pTipologiaRete;
    gTipRetA := NULL;
    gTipRetM := NULL;
    gTipRetB := NULL;
    vTmp := PKG_UtlGlb.SplitString(pTipologiaRete,PKG_Mago.cSepCharLst);

    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcTipReteAT THEN
            gTipRetA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteMT THEN
            gTipRetM := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcTipReteBT THEN
            gTipRetB := vTmp(i);
        END IF;
    END LOOP;


    gTipProdScelti := pTipologiaProd;
    gTipProdA := NULL;
    gTipProdB := NULL;
    vTmp := PKG_UtlGlb.SplitString(pTipologiaProd,PKG_Mago.cSepCharLst);

    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP
        IF vTmp(i) = PKG_Mago.gcClienteProdPuro THEN
            gTipProdA := vTmp(i);
        END IF;
        IF vTmp(i) = PKG_Mago.gcClienteAutoProd THEN
            gTipProdB := vTmp(i);
        END IF;
    END LOOP;


    -- bug 961 - 9-XII-2013 - SF4
    gcFatt_MVh := 1/1000;


--
-- solo per scaramanzia
-- non cancellatemi ancora
-- il seguente blocchetto commentato
--          Sergio, in un incasinato dicembre 2013
--
/*

   IF pDataDa <> pDataA THEN
        gDeltaTime := DateDiff('HH', pDataDa, pDataA);
        vOre := TRUNC(gDeltaTime);
        vMin := (gDeltaTime - vOre) * 60;
        gcFatt_MVh := 1 / gDeltaTime / 1000000;
    ELSE
        gDeltaTime := 0;
        vOre := 0;
        vMin := 0;
        gcFatt_MVh := 1;
    END IF;

*/
--
-- grazie :)
--


    gOraDesc   := TO_CHAR(vOre)||':'||TO_CHAR(vMin,'fm09');


    -- sf4 Jira 930 - se date uguali, sono per "istante" consistenza produttori;
    --                altrimenti sono per report stima e forzo giornata completa
   IF pDataDa = pDataA THEN

        gDataDa    := pDataDa;
        gDataA     := pDataA;

    ELSE

        gDataDa := TO_DATE(TO_CHAR(pDataDa, 'DD-MM-YYYY') || ' 00:00:00','DD-MM-YYYY HH24:MI:SS');
        gDataA := TO_DATE(TO_CHAR(pDataA, 'DD-MM-YYYY') || ' 23:59:59','DD-MM-YYYY HH24:MI:SS');

    END IF;


    OPEN pRefCurs FOR SELECT COUNT(*) fake FROM DUAL; -- cursore fasullo ma necessario al FE

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.InitVarGlob',
                                     pDataRif      => pDataDa,
                                     pDataRif_fine => pDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Org:'||NVL(pOrganizzazione,'<null>')||'-'||
                                                      'Sta:'||NVL(pStatoRete,'<null>')||'-'||
                                                      'Ret:'||NVL(pTipologiaRete,'<null>')--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END InitVarGlob;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ScendiNellAlbero( pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE) AS

-- ----------------------------------------------------------------------------------------------------------
    vLog            PKG_Logs.t_StandardLog;
    vCod            ELEMENTI.COD_ELEMENTO%TYPE;
    vGst            ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNom            ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vTip            TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vGstCP          ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNomCP          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

BEGIN
   DBMS_OUTPUT.PUT_LINE(' ScendiNellAlbero '||pElementoPadre);

      vGstCP := '';
      vNomCP := '';

      FOR vRiga IN
        -- bug 809 - modif. COD_GEST
        (SELECT cod_elemento, t.cod_tipo_elemento, T.cod_gest_elemento, d.nome_elemento, T.sequenza
           FROM TABLE(PKG_ELEMENTI.LEGGIGERARCHIAInf(pElementoPadre,gOrganizzazione, gStatoRete,gDataDa)) T
           JOIN (SELECT * FROM ELEMENTI_DEF WHERE gDataDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) d USING (COD_ELEMENTO)
          ORDER BY SEQUENZA
        )

      LOOP

        vCod := vRiga.COD_ELEMENTO;
        vGst := vRiga.COD_GEST_ELEMENTO;
        vNom := vRiga.NOME_ELEMENTO;

--          DBMS_OUTPUT.PUT_LINE(' preinsert GTTD_VALORI_REP '||vCod||' - '||vgst||' - '||vNom);
--          DBMS_OUTPUT.PUT_LINE('   tipo'||vRiga.COD_TIPO_ELEMENTO);

        IF vRiga.COD_TIPO_ELEMENTO = PKG_MAGO.gcCabinaPrimaria THEN
          vGstCP := vRiga.COD_GEST_ELEMENTO;
          vNomCP := vRiga.NOME_ELEMENTO;
        END IF;

        IF vRiga.COD_TIPO_ELEMENTO = gTipEleFiglio THEN

        -- SF4 modifica rep 974 - 28-II-2014

           IF    (NOT vRiga.COD_TIPO_ELEMENTO IN ( gcProduttoreAT,
                                                   gcProduttoreMT,
                                                   gcProduttoreBT))
              OR
                 (    (vRiga.COD_TIPO_ELEMENTO = gcProduttoreAT)
                  AND (PKG_Mago.gcTipReteAT IN ( gTipRetA, gTipRetM, gTipRetB )))
              OR
                 (    (vRiga.COD_TIPO_ELEMENTO = gcProduttoreMT)
                  AND (PKG_Mago.gcTipReteMT IN ( gTipRetA, gTipRetM, gTipRetB )))
              OR
                 (    (vRiga.COD_TIPO_ELEMENTO = gcProduttoreBT)
                  AND (PKG_Mago.gcTipReteBT IN ( gTipRetA, gTipRetM, gTipRetB )))
           THEN

               INSERT INTO GTTD_VALORI_REP(TIP,NUM1,ALF1,ALF2,ALF3,ALF4,ALF5)
                                      VALUES(gcRepBody,
                                             vCod,
                                             vGst, --SUBSTR(vGst,INSTR(vGst,PKG_Mago.cSeparatore)+1),
                                             gTipEleFiglio,
                                             vNom,
                                             vGstCP,
                                             vnomCP);

                  DBMS_OUTPUT.PUT_LINE(' insert GTTD_VALORI_REP '||vCod||' - '||vgst||' - '||vRiga.COD_TIPO_ELEMENTO);

          END IF;

        END IF;

    END LOOP;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.ScendiNellAlbero',
                                     pDataRif      => gDataDa,
                                     pDataRif_fine => gDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Padre:'||pElementoPadre||'-'||
                                                      'Figlio:'||vCod--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END ScendiNellAlbero;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitGerarchia     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipEleFiglio   IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;
    vCod            ELEMENTI.COD_ELEMENTO%TYPE;
    vGst            ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vFlg            INTEGER;
    vFlg2           INTEGER;
    vNom            ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vTip            TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTSt            TIPI_ELEMENTO.CODIFICA_ST%TYPE;
    vOre            NUMBER;
    vMin            NUMBER;

    vFlgNull        INTEGER;

    vElemento       ELEMENTI.COD_ELEMENTO%TYPE := pElementoPadre;
    vGestionale     ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome           ELEMENTI_DEF.NOME_ELEMENTO%TYPE;

    vTmp            PKG_UtlGlb.t_SplitTbl;

    TYPE t_Fonti    IS TABLE OF RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE;
    vFonti          t_Fonti;

 BEGIN


        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                    -- pLogArea    => vLog
                                     );
        PKG_Logs.StdLogAddTxt('log init '||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
        PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);


    OPEN pRefCurs FOR SELECT COUNT(*) fake FROM DUAL; -- cursore fasullo ma necessario al FE

    EXECUTE IMMEDIATE 'TRUNCATE TABLE GTTD_VALORI_REP';


    IF NVL(pElementoFiglio,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
        IF NVL(vElemento,PKG_Mago.gcNullNumCode) = PKG_Mago.gcNullNumCode THEN
            PKG_Elementi.GetDefaultCO (vElemento, vGestionale, vNome);
        END IF;
    END IF;

   DBMS_OUTPUT.PUT_LINE(' x elencoxxx '||pTipEleFiglio||' - '||vElemento);


    gTipeleFiglio := pTipEleFiglio;

    ScendiNellAlbero(vElemento);


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione     => 'PKG_Reports.InitGerarchia',
                                     pDataRif      => gDataDa,
                                     pDataRif_fine => gDataA,
                                     pTipo   => 'Codici',
                                     pCodice       => 'Padre:'||pElementoPadre||'-'||
                                                      'Figlio:'||pElementoFiglio--,
                                     --pLogArea      => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END InitGerarchia;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE InitRepStimaConsi     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                  pDataDa         IN DATE,
                                  pDataA          IN DATE,
                                  pOrganizzazione IN NUMBER,
                                  pStatoRete      IN NUMBER,
                                  pTipologiaRete  IN VARCHAR2,
                                  pTipologiaProd  IN VARCHAR2,
                                  pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pElementoFiglio IN ELEMENTI.COD_ELEMENTO%TYPE,
                                  pTipEleFiglio   IN VARCHAR2) AS
/*-----------------------------------------------------------------------------------------------------------
    Inizializza le aree comuni per la valorizzazione dei report di consistenza di energia e potenza
-----------------------------------------------------------------------------------------------------------*/

 BEGIN

   InitVarGlob     (pRefCurs,pDataDa, pDataA, pOrganizzazione, pStatoRete, pTipologiaRete, pTipologiaProd);
   InitGerarchia   (pRefCurs, pElementoPadre, pElementoFiglio, pTipEleFiglio);

 END InitRepStimaConsi;

-- ------------------------------------------------------------------------------------------------------------

 PROCEDURE ReadRepMisureBody(pTipoReport IN INTEGER)    AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;

    vALF1 varchar2(100);
    vDAT1 date;
    vNUM1 NUMBER;
    vALF2 varchar2(100);
    vALFQ varchar2(100);
    vNum2 NUMBER;

 BEGIN

    -- SF4 23-07-2014 fix Bug 1214 - cancellava solo i valori di gcStima
    DELETE FROM GTTD_REP_ENERGIA_POTENZA WHERE TIPO_REPORT IN (pTipoReport);

--    DELETE FROM GTTD_VALORI_REP WHERE TIP = gcRepValori;



    FOR vRiga IN (SELECT DISTINCT NUM1 FROM GTTD_VALORI_REP WHERE TIP = gcRepBody)
    LOOP

    --    dbms_output.put_line('vRiga '||vRiga.NUM1);

        FOR vRete IN (SELECT gTipRetA aS RETE from DUAL
                      UNION SELECT gTipRetM AS RETE FROM DUAL
                      UNION SELECT gTipRetB AS RETE FROM DUAL )
        LOOP

            IF NOT vRete.RETE IS NULL THEN

      --        dbms_output.put_line('   vRete '||vRete.RETE);


              FOR vCli IN (SELECT gTipProdA aS CLI from DUAL
                           UNION SELECT gTipProdB AS CLI FROM DUAL)
              LOOP


                  IF NOT vCli.Cli IS NULL THEN


             --         dbms_output.put_line('        vCli '|| vCli.Cli);

                      PKG_MISURE.GetMisure  (vLst,
                                             vRiga.NUM1,
                                             gDataDa,
                                             gDataA,
                                             PKG_Mago.gcPotenzaMeteoPrevisto||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcNumeroImpianti||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcPotenzaInstallata||PKG_Mago.cSepCharLst,
                                             gOrganizzazione,
                                             gStatoRete,
                                             vRete.RETE,
                                             PKG_Mago.gcRaggrFonteSolare||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcRaggrFonteEolica||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcRaggrFonteIdraulica||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcRaggrFonteTermica||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcRaggrFonteRinnovab||PKG_Mago.cSepCharLst
                                             ||PKG_Mago.gcRaggrFonteConvenz,
                                             vCli.Cli,
                                             60);


                       LOOP
                       FETCH vLst INTO vAlf1,vDat1,vNum1,vAlfQ,vAlf2,vNum2;
                       EXIT WHEN vLst%NOTFOUND;

                           INSERT INTO GTTD_REP_ENERGIA_POTENZA (TIPO_REPORT,COD_ELEMENTO,COD_RAGGR_FONTE,COD_TIPO_RETE,COD_TIPO_PRODUTTORE,COD_TIPO_MISURA,SEGNO,VALORE)
                            SELECT pTipoReport TIPO_REPORT, vRiga.NUM1 COD_ELEMENTO,tpf.COD_RAGGR_FONTE,vRete.RETE COD_TIPO_RETE,vCli.Cli COD_TIPO_CLIENTE,vAlf1 as COD_TIPO_MISURA,
                                               CASE
                                                  WHEN vNum1 < 0 THEN '-'
                                                  ELSE                '+'
                                               END SEGNO,
                                               ABS(vNum1) VALORE
                                           from TIPO_FONTI tpf
                                           where tpf.COD_TIPO_FONTE=vAlf2;

                       END LOOP;

                       CLOSE vLst;

                   END IF;

             END LOOP;

          END IF;

       END LOOP;

   END LOOP;


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                       pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END ReadRepMisureBody;

-- ------------------------------------------------------------------------------------------------------------

 PROCEDURE RigaRepStimaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;

    vALF1 varchar2(100);
    vDAT1 date;
    vNUM1 NUMBER;
    vALF2 varchar2(100);
 BEGIN

    OPEN pRefCurs FOR
        SELECT  COD_ELEMENTO,
                COD_GEST_ELEMENTO,
                COD_TIPO_ELEMENTO,
                NOME_ELEMENTO,
                COD_GEST_CP,
                NOME_CP,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT
           NUM1 COD_ELEMENTO,
           ALF1 COD_GEST_ELEMENTO,
           ALF2 COD_TIPO_ELEMENTO,
           ALF3 NOME_ELEMENTO,
           ALF4 COD_GEST_CP,
           ALF5 NOME_CP,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT1,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
           GetMisStimaFnte(gcStima, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_1,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_MVh E_gen_1,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_2,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaMeteoPrevisto, PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_MVh E_gen_2,
        -- SF4 modifica rep 974 - 28-II-2014
        -- SF4 modifica bug 1215 - 23-VII-2014 - patch 1.7.a7
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteIdraulica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_3,
           0 as E_Gen_3,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteTermica,     PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_4,
           0 as E_Gen_4,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteRinnovab,    PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_5,
           0 as E_Gen_5,
           GetMisStimaFnte(gcStima, NUM1, PKG_Mago.gcRaggrFonteConvenz,     PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_6,
           0 as E_Gen_6,
           0  P_contr0
                  FROM GTTD_VALORI_REP
                 WHERE TIP = gcRepBody
               )
        -- bug 797 ordinamento numeri/lettere
        ORDER BY NLSSORT(NOME_CP,'NLS_SORT=BINARY'), NLSSORT(NOME_ELEMENTO,'NLS_SORT=BINARY');

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                       pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END RigaRepStimaBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaBody   (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;

    vALF1 varchar2(100);
    vDAT1 date;
    vNUM1 NUMBER;
    vALF2 varchar2(100);
 BEGIN

    ReadRepMisureBody(gcStima);
    RigaRepStimaBody(pRefcurs);


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                       pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
                       ID_RAGGR_FONTE,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_1,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_1,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_2,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_3,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_3,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_4,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_4,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_5,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_5,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN
                            GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_6,
                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN
                             GetMisStimaFnte(gcStima,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         ELSE 0 /*NULL*/ END  E_gen_6,

                       COD_RAGGR_FONTE  P_contr0
                  FROM RAGGRUPPAMENTO_FONTI
                )
    ORDER BY ID_RAGGR_FONTE;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaFooter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepStimaTot (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT  gCodPerRigaTotali COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1, E_gen_1,
                P_prev_2, E_gen_2,
                P_prev_3, E_gen_3,
                P_prev_4, E_gen_4,
                P_prev_5, E_gen_5,
                P_prev_6, E_gen_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                NVL(E_gen_1, 0) + NVL(E_gen_2, 0) + NVL(E_gen_3, 0) + NVL(E_gen_4, 0) + NVL(E_gen_5, 0) + NVL(E_gen_6, 0) E_gen_tot,
                P_contr0
          FROM (SELECT
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcStima,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteSolare, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_1,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteSolare,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         E_gen_1,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteEolica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_2,
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteEolica,PKG_Mago.gcPotenzaMeteoPrevisto,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)      * gcFatt_MVh
                         E_gen_2,

        -- SF4 modifica bug 1215 - 23-VII-2014 - patch 1.7.a7
                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_3,
                         0 as E_Gen_3,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteTermica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_4,
                         0 as E_Gen_4,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_5,
                         0 as E_Gen_5,

                         GetMisStimaFnte(gcStima,NULL,PKG_Mago.gcRaggrFonteConvenz, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_6,
                         0 as E_Gen_6,

                       gCodPerRigaTotali P_contr0
                  FROM dual)
      ;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepStimaTot;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE RigaRepConsiBody  (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;
 BEGIN


    OPEN pRefCurs FOR
        SELECT  COD_ELEMENTO,
                COD_GEST_ELEMENTO,
                COD_TIPO_ELEMENTO,
                NOME_ELEMENTO,
                COD_GEST_CP,
                NOME_CP,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1,
                P_prev_2,
                P_prev_3,
                P_prev_4,
                P_prev_5,
                P_prev_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT
           NUM1 COD_ELEMENTO,
           ALF1 COD_GEST_ELEMENTO,
           ALF2 COD_TIPO_ELEMENTO,
           ALF3 NOME_ELEMENTO,
           ALF4 COD_GEST_CP,
           ALF5 NOME_CP,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT1,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcNumeroImpianti,       PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
           GetMisStimaFnte(gcConsi, NUM1, NULL,                          PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteAutoProd, NULL,                         PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteSolare,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_1,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteEolica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_2,
        -- SF4 modifica bug 1215 - 23-VII-2014 - patch 1.7.a7
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteIdraulica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_3,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteTermica,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_4,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteRinnovab,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_5,
           GetMisStimaFnte(gcConsi, NUM1, PKG_Mago.gcRaggrFonteConvenz,   PKG_Mago.gcPotenzaInstallata,    PKG_Mago.gcClienteProdPuro,     PKG_Mago.gcClienteAutoProd, NULL)                * gcFatt_kW  P_prev_6,
           0  P_contr0
                  FROM GTTD_VALORI_REP
                 WHERE TIP = gcRepBody
               )
        -- bug 797 ordinamento numeri/lettere
        ORDER BY NLSSORT(NOME_CP,'NLS_SORT=BINARY'), NLSSORT(NOME_ELEMENTO,'NLS_SORT=BINARY');
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END RigaRepConsiBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsiBody  (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Body sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
    vLst            PKG_UtlGlb.t_query_cur;
 BEGIN

    ReadRepMisureBody(gcConsi);
    RigaRepConsiBody(pRefcurs);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepStimaPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiBody;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsiFooter (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1,
                P_prev_2,
                P_prev_3,
                P_prev_4,
                P_prev_5,
                P_prev_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT COD_RAGGR_FONTE COD_TIPO_FONTE,
                       ID_RAGGR_FONTE,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteSolare THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_1,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteEolica THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_2,
        -- SF4 modifica bug 1215 - 23-VII-2014 - patch 1.7.a7

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteIdraulica THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_3,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteTermica THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_4,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteRinnovab THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_5,

                         CASE COD_RAGGR_FONTE WHEN PKG_Mago.gcRaggrFonteConvenz THEN
                            GetMisStimaFnte(gcConsi,NULL,COD_RAGGR_FONTE, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         ELSE 0 /*NULL*/ END  P_prev_6,


                       COD_RAGGR_FONTE  P_contr0
                  FROM RAGGRUPPAMENTO_FONTI
                )
                ORDER BY ID_RAGGR_FONTE;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiFooter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetRepConsitot (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona le righe relative al Footer sezione Potenza del report di consistenza
-----------------------------------------------------------------------------------------------------------*/
    vLog            PKG_Logs.t_StandardLog;
BEGIN
    OPEN pRefCurs FOR
        SELECT  gCodPerRigaTotali COD_TIPO_FONTE,
                N_impBT1,
                P_instBT1,
                N_impBT2,
                P_instBT2,
                N_impBT1 + N_impBT2    N_impBT_tot,
                P_instBT1 + P_instBT2  P_instBT_tot,
                N_impMT1,
                P_instMT1,
                N_impMT2,
                P_instMT2,
                N_impMT1 + N_impMT2    N_impMT_tot,
                P_instMT1 + P_instMT2  P_instMT_tot,
                P_prev_1,
                P_prev_2,
                P_prev_3,
                P_prev_4,
                P_prev_5,
                P_prev_6,
                NVL(P_prev_1, 0) + NVL(P_prev_2, 0) + NVL(P_prev_3, 0) + NVL(P_prev_4, 0) + NVL(P_prev_5, 0) + NVL(P_prev_6, 0) P_prev_tot,
                P_contr0
          FROM (SELECT
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)                 N_impBT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteBT, NULL)    * gcFatt_kW  P_instBT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)             N_impBT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteBT, NULL)* gcFatt_kW  P_instBT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)                 N_impMT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteProdPuro,NULL, PKG_Mago.gcTipReteMT, NULL)    * gcFatt_kW  P_instMT1,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcNumeroImpianti,         PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)             N_impMT2,
                       GetMisStimaFnte(gcConsi,NULL,NULL,PKG_Mago.gcPotenzaInstallata,      PKG_Mago.gcClienteAutoProd,NULL, PKG_Mago.gcTipReteMT, NULL)* gcFatt_kW  P_instMT2,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteSolare, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_1,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteEolica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_2,
        -- SF4 modifica bug 1215 - 23-VII-2014 - patch 1.7.a7

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteIdraulica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_3,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteTermica, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_4,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteRinnovab, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_5,

                         GetMisStimaFnte(gcConsi,NULL,PKG_Mago.gcRaggrFonteConvenz, PKG_Mago.gcPotenzaInstallata,PKG_Mago.gcClienteProdPuro,PKG_Mago.gcClienteAutoProd, NULL)    * gcFatt_kW
                         P_prev_6,

                       gCodPerRigaTotali P_contr0 FROM DUAL)
                  ;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
        vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetRepConsPotenzaBody'--,
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END GetRepConsiTot;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetAggregDaPerim(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                            pElementoPadre  IN ELEMENTI.COD_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    seleziona i tipi elemento presenti in gerarchia come discendenti di un elemento specificato
-----------------------------------------------------------------------------------------------------------*/
   vLog            PKG_Logs.t_StandardLog;

BEGIN

 OPEN pRefCurs FOR
          WITH tipiGer AS (SELECT DISTINCT cod_tipo_elemento FROM
                          TABLE(PKG_ELEMENTI.LEGGIGERARCHIAInf(pElementoPadre,gOrganizzazione, gStatoRete,gDataDa))
                          --- SF4 correzione 7-5-13 escludo i generatori
                          WHERE cod_tipo_elemento NOT IN (
                            PKG_MAGO.gcGeneratoreAT,
                            PKG_MAGO.gcGeneratoreMT,
                            PKG_MAGO.gcGeneratoreBT
                          ))
          SELECT cod_tipo_elemento, descrizione FROM
          (SELECT cod_tipo_elemento, E.descrizione FROM
          tipiGer T JOIN (select * from TIPI_ELEMENTO
                          --- SF4 correzione 16-9-13 escludo i tipi nascosti
                         WHERE NASCOSTO = PKG_UtlGlb.gkFlagOFF) E
          USING (cod_tipo_elemento))
          ORDER BY descrizione;

 RETURN;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetAggregDaPerim',
                                     pCodice       => 'Padre:'||pElementoPadre
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetAggregDaPerim;


-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDatiPerimHeader(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pElePerim IN ELEMENTI.COD_ELEMENTO%TYPE) AS

/*-----------------------------------------------------------------------------------------------------------
    seleziona i tipi elemento presenti in gerarchia come discendenti di un elemento specificato
-----------------------------------------------------------------------------------------------------------*/
   vLog       PKG_Logs.t_StandardLog;
   vEleEse    ELEMENTI.COD_ELEMENTO%TYPE;
  vEleGEse    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
  
  
  
BEGIN

  SELECT G.COD_ELEMENTO, E.COD_GEST_ELEMENTO
    INTO vEleEse,vEleGEse
    FROM TABLE(PKG_ELEMENTI.LeggiGerarchiaSup(pElePerim,gOrganizzazione,gStatoRete,gDataDa)) G
   INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = G.COD_ELEMENTO
                        AND E.COD_TIPO_ELEMENTO = PKG_Mago.gcEsercizio;


 /* --- caso per prove */

--  OPEN pRefCurs FOR
--    SELECT 'DANO' pEleMa, 'DF00 ' pEleDTR,   /*'DF70'*/vEleGEse pEleCO    FROM DUAL;

/* --- caso corretto da elaborare quando ci sarÂ¿ la vista */

  OPEN pRefCurs FOR
 
 SELECT DISTINCT
    v2.ACR_MA pEleMa,
    v2.GST_UTR
    || FIRST_VALUE(v1.ACR_ESE) OVER (ORDER BY v1.ese) pEleDTR,
    v2.ACR_ESE pEleCO
FROM
    (
        SELECT
            *
        FROM
            V_ESERCIZI
        WHERE
            GST_ESE      = vEleGEse
            AND gdataA >= PKG_ASID_UTL.f_GetDataASID2018
    )
    V2
JOIN V_ESERCIZI V1
ON
    (
        V2.GST_UTR=V2.GST_UTR
    )
UNION ALL
SELECT
    ACR_MA pEleMa,
    GST_UTR pEleDTR,
    ACR_ESE pEleCO
FROM
    V_ESERCIZI_ANTEASID
WHERE
    GST_ESE     = vEleGEse
    AND gdataA < PKG_ASID_UTL.f_GetDataASID2018;


 RETURN;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassREP||PKG_Mago.gcJobSubClassFE,
                                     pFunzione   => 'PKG_Reports.GetDatiPerimHeader',
                                     pCodice       => 'Elemento:'||pElePerim
                                     --pLogArea    => vLog
                                     );
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetDatiPerimHeader;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_REPORTS;
/
show errors;