PROMPT V_PRODUTTORI_X_RES_ASID  

CREATE OR REPLACE FORCE VIEW V_PRODUTTORI_X_RES_ASID   AS 
  SELECT                                    -- dati da tabella PRODUTTORI_DEF
         --P.GEST_PROD_ID,
         s.cod_gest_old as gest_prod_id,
          v.DT_INI_P,
          v.DT_FIN_P,
          v.FONTE,
          v.pot_inst,
          -- dati da tabella PRODUTTORI_RIGEDI
          v.STATO_ATTUALE_ID,
          v.CATEGORIA_PROD_ID,
          v.DT_INI_R,
          v.DT_FIN_R
from V_PRODUTTORI_X_RES@PKG1_RIGEDI.IT v
join ricodifica_elementi@pkg1_stmaui.it s
on (s.cod_gest_new=v.gest_prod_id)
join (select PKG_ASID_UTL.f_GetLastAsidCode() code from dual) asid
on (asid.code=s.tipo_ricodifica);
/