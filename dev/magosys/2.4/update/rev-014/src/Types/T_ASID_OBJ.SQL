
PROMPT TYPES MODIFICA T_ASID_TBL - T_ASID_OBJ

BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_ASID_TBL';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_ASID_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/


 CREATE OR REPLACE TYPE "T_ASID_OBJ" AS OBJECT 
    (DATA_VALIDITA DATE
    ,TIPO_RICODIFICA  VARCHAR2(50)
    ,DATA_VALIDITA_PREC  DATE
    )
/
 
 CREATE OR REPLACE TYPE T_ASID_TBL IS TABLE OF T_ASID_OBJ
/
 