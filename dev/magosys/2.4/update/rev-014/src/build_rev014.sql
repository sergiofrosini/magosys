SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 14
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2


PROMPT ---------------------------------------------------------------------------------------
PROMPT Types
PROMPT ---------------------------------------------------------------------------------------
@./Types/T_ASID_OBJ.SQL


PROMPT ---------------------------------------------------------------------------------------
PROMPT Views
PROMPT ---------------------------------------------------------------------------------------
@./views/V_ASID_ELEMENTS.sql
@./views/V_PRODUTTORI_X_RES_ASID.sql
@./views/V_ESERCIZI_ANTEASID.sql

PROMPT ---------------------------------------------------------------------------------------
PROMPT Packages
PROMPT ---------------------------------------------------------------------------------------
@./Packages/PKG_ASID_UTL.sql

PROMPT ---------------------------------------------------------------------------------------
PROMPT PackageBodies
PROMPT ---------------------------------------------------------------------------------------
@./PackageBodies/PKG_ASID_UTL.sql
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_REPORTS.sql
@./PackageBodies/PKG_TRANSFER_RES.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 14
PROMPT =======================================================================================

SPOOL OFF
