SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 9
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

conn mago/mago&tns_arcdb2



PROMPT ---------------------------------------------------------------------------------------
PROMPT Packages
PROMPT ---------------------------------------------------------------------------------------
@./Packages/PKG_MAGO_LINK.sql


PROMPT ---------------------------------------------------------------------------------------
PROMPT Package Bodies
PROMPT ---------------------------------------------------------------------------------------
@./PackageBodies/PKG_MAGO_LINK.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 9
PROMPT =======================================================================================

SPOOL OFF
