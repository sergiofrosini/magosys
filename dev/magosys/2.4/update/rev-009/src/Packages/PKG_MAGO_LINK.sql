PROMPT PACKAGE: PKG_MAGO_LINK

create or replace package PKG_MAGO_LINK as

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.0.0
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */
type t_profilo_rec is record (mese    number(2),
                              tipo    number(1),
                              daytime varchar2(5),
                              valore  number);
                              
type t_profili_tbl is table of t_profilo_rec;

type t_mis_rec is record (nome tipi_misura.cod_tipo_misura%type,
                          datmis date,
                          valore number,
                          qualita varchar2(20),
                          tipofonte tipo_fonti.cod_tipo_fonte%type,
                          inter number);

type t_mis_tbl is table of t_mis_rec;

  function AddProfili            ( pCodGestElem      in elementi.cod_gest_elemento%type,
                                   pTipoMisura       in tipi_misura.cod_tipo_misura%type,
                                   pAnnoRif          in integer,
                                   pProfili          in t_profili_tbl,
                                   pDataInizio       in date,
                                   pDataFine         in date
                                  ) return number;

  procedure GetMisureCG          ( pMisTbl           out t_mis_tbl,
                                   pGestElem         in elementi.cod_gest_elemento%TYPE,
                                   pDataDa           in date,
                                   pDataA            in date,
                                   pTipiMisura       in varchar2,
                                   pOrganizzazione   in integer,
                                   pStatoRete        in integer,
                                   pTipologiaRete    in varchar2,
                                   pFonte            in varchar2,
                                   pTipoClie         in varchar2,
                                   pAgrTemporale     in integer default 15,
                                   pDisconnect       in integer default 0);

end pkg_mago_link;
/