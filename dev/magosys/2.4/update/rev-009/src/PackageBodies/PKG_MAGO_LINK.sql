PROMPT PACKAGE BODY: PKG_MAGO_LINK


create or replace package body PKG_MAGO_LINK as

/* ***********************************************************************************************************
   NAME:       PKG_MAGO_LINK
   PURPOSE:    Servizi per accesso ai servizi MAGO tramite dblink

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.1      03/06/2020  Malandrino       Created this package.



/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */
  function AddProfili            ( pCodGestElem      IN elementi.cod_gest_elemento%type,
                                   pTipoMisura       IN tipi_misura.cod_tipo_misura%type,
                                   pAnnoRif          IN integer,
                                   pProfili          IN t_profili_tbl,
                                   pDataInizio       IN date,
                                   pDataFine         IN date
                                  ) return number
                                  
  is PRAGMA AUTONOMOUS_TRANSACTION;
    vProfilo t_profilo;
    vProfili t_profili := t_profili();
    i number;
  begin
   
    if (pProfili.count>0) then      
      for i in pProfili.first..pProfili.last loop
        vProfilo := t_profilo(pProfili(i).mese,pProfili(i).tipo,pProfili(i).daytime,pProfili(i).valore);
        vProfili.extend();
        vProfili(i) := vProfilo;
        
      end loop;
            
    end if;
    
    return pkg_profili.AddProfili(pCodGestElem,pTipoMisura,pAnnoRif,vProfili,pDataInizio,pDataFine);

  exception
    when others then
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_MAGO_LINK.AddProfili '||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         return 0;
  end;
  
  procedure GetMisureCG          (   pMisTbl           out t_mis_tbl,
                                     pGestElem         in elementi.cod_gest_elemento%TYPE,
                                     pDataDa           in date,
                                     pDataA            in date,
                                     pTipiMisura       in varchar2,
                                     pOrganizzazione   in integer,
                                     pStatoRete        in integer,
                                     pTipologiaRete    in varchar2,
                                     pFonte            in varchar2,
                                     pTipoClie         in varchar2,
                                     pAgrTemporale     in integer default 15,
                                     pDisconnect       in integer default 0)
  is PRAGMA AUTONOMOUS_TRANSACTION;
    vRef          pkg_utlglb.t_query_cur;
    vCodMis       tipi_misura.cod_tipo_misura%TYPE;
    vData         date;
    vValore       number;
    vTipoFonte    tipo_fonti.cod_tipo_fonte%TYPE;
    vinter        number;
    vqualita      varchar2(20);    

    vmis_rec t_mis_rec;
    --vmis_tbl t_mis_tbl;
    
  begin
    pkg_misure.GetMisureCG(vRef, pGestElem, pDataDa,pDataA,pTipiMisura,pOrganizzazione,pStatoRete,pTipologiaRete,pFonte,pTipoClie,pAgrTemporale,pDisconnect);
    --dbms_output.put_line('entro loop ');
    
    pMisTbl := t_mis_tbl();
    loop 
      fetch vRef into vCodMis,vData,vValore,vTipoFonte,vinter,vqualita;
      exit when vRef%notfound;
      --dbms_output.put_line('leggo ');
      vmis_rec.nome := vCodMis;
      vmis_rec.datmis := vData;
      vmis_rec.valore := vValore;
      vmis_rec.qualita := vqualita;
      vmis_rec.tipofonte := vTipoFonte;
      vmis_rec.inter := vinter;
      
      pMisTbl.extend();
      pMisTbl(pMisTbl.count) := vmis_rec;
    end loop;
    close vRef;
    
    commit;
    
  exception
    when others then
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_MAGO_LINK.GetMeasureCG '||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

  end;
                                
end pkg_mago_link;

/