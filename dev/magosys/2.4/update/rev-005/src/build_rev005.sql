SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 5
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT SQL.SQLCODE

conn crel/crel&tns_arcdb2
PROMPT ---------------------------------------------------------------------------------------
PROMPT GRANT da Crel (eventuale copia su ARCDB2)
PROMPT ---------------------------------------------------------------------------------------
@./Grants/GRANT_CREL.sql

conn mago/mago&tns_arcdb2
PROMPT ---------------------------------------------------------------------------------------
PROMPT Packages
PROMPT ---------------------------------------------------------------------------------------
@./Packages/PKG_MAGO_CREL.sql
PROMPT ---------------------------------------------------------------------------------------
PROMPT Package Bodies
PROMPT ---------------------------------------------------------------------------------------
@./PackageBodies/PKG_MAGO.sql
@./PackageBodies/PKG_MAGO_CREL.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_SPV_ANAG_AUI.sql

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 5
PROMPT =======================================================================================

SPOOL OFF
