PROMPT PACKAGE PKG_MAGO_CREL

create or replace PACKAGE PKG_MAGO_CREL AS 

-- COPIATI DA CREL.PKG_CREL

 TYPE t_GerElem      IS RECORD (LIV          NUMBER,
                                ID_PADRE     NUMBER,
                                ID_FIGLIO    NUMBER,
                                SG_COD       VARCHAR(2000),
                                TIPO_ELEM    VARCHAR(2000),
                                NOME_ELEM    VARCHAR(2000),
                                DT_DISCONN   DATE,
                                SEQ          NUMBER,
                                FLAGS        VARCHAR(16),
                                DT_INI       DATE,
                                DT_FIN       DATE
                               );
 TYPE t_GerTable     IS TABLE OF t_GerElem;

-----------------------------------------------------------------------------------------------

 gcCodeStaparator     CONSTANT CHAR(1)      := '_';
 gcListSeparator      CONSTANT CHAR(1)      := ',';

 gcStatoAttuale       CONSTANT NUMBER(1)    := 1;            -- Stato Attuale
 gcStatoReteAttuale   CONSTANT VARCHAR2(2)  := 'SA';         -- Stato Attuale

 gcStatoNormale       CONSTANT NUMBER(1)    := 2;            -- Stato Normale
 gcStatoReteNormale   CONSTANT VARCHAR2(2)  := 'SN';         -- Stato Normale

 gcFattConvAmp        CONSTANT NUMBER       := 1;            -- fattore di conversione di default da VA a V

 gcStatoElemAperto    CONSTANT CHAR(2)      := 'AP';         -- stato aperto
 gcStatoElemChiuso    CONSTANT CHAR(2)      := 'CH';         -- stato chiuso

 gcOriAUI             CONSTANT CHAR(6)      := 'STMAUI';
 gcOriSAR             CONSTANT CHAR(9)      := 'SAR_ADMIN';
 gcOriST              CONSTANT CHAR(2)      := 'ST';
 gcOriSIGRAF          CONSTANT CHAR(6)      := 'SIGRAF';
 gcOriGIS             CONSTANT CHAR(3)      := 'GIS';
 gcOriDMS             CONSTANT CHAR(3)      := 'DMS';
 gcOriNazionale       CONSTANT CHAR(3)      := 'NAZ';
 gcOriSTAMS           CONSTANT CHAR(5)      := 'STAMS';
 gcOriPOD             CONSTANT CHAR(3)      := 'POD';
 gcOriCREL            CONSTANT CHAR(4)      := 'CREL';

 gcGerAUI             CONSTANT NUMBER(1)    := 0;                            -- Gerarchia AUI (da Schema STMAUI)
 gcGerCPSA            CONSTANT NUMBER(1)    := 1;                            -- Cabina Primaria di stato Attuale;
 gcGerCPSN            CONSTANT NUMBER(1)    := 2;                            -- Cabina Primaria di stato Normale;
 gcGerCSSA            CONSTANT NUMBER(1)    := 4;                            -- Cabina Secondaria di stato Attuale;
 gcGerCSSN            CONSTANT NUMBER(1)    := 8;                            -- Cabina Secondaria di stato Normale;
 gcGerAMM             CONSTANT NUMBER(2)    := 16;                           -- Amministrativa;
 gcGerGEO             CONSTANT NUMBER(2)    := 32;                           -- Geografica (Istat);
 gcGerEleSA           CONSTANT NUMBER(2)    := gcGerCPSA + gcGerCSSA;        -- Elettrica di stato Attuale;
 gcGerEleSN           CONSTANT NUMBER(2)    := gcGerCPSN + gcGerCSSN;        -- Elettrica di stato Normale;
 gcGerAmmSA           CONSTANT NUMBER(2)    := gcGerAMM  + gcGerCSSA;        -- Amministrativa di stato Attuale;
 gcGerAmmSN           CONSTANT NUMBER(2)    := gcGerAMM  + gcGerCSSN;        -- Amministrativa di stato Normale;
 gcGerGeoSA           CONSTANT NUMBER(2)    := gcGerGEO  + gcGerCSSA;        -- Geografica (Istat) di stato Attuale;
 gcGerGeoSN           CONSTANT NUMBER(2)    := gcGerGEO  + gcGerCSSN;        -- Geografica (Istat) di stato Normale;
 gcGerTronchiRami     CONSTANT NUMBER(2)    := 64;                           -- Tronchi/Rami
 gcGerDMS             CONSTANT NUMBER(3)    := 128;                          -- Cabine P/S DMS
 gcGerCPExtSA         CONSTANT NUMBER(3)    := 256;                          -- Cabina Primaria Estesa di stato Attuale;
 gcGerCPExtSN         CONSTANT NUMBER(3)    := 512;                          -- Cabina Primaria Estesa di stato Normale;

 -------------------------------------------------------------------------------------------------------------

 gcTipoReteNonApplic  CONSTANT VARCHAR2(2) := 'NA';  -- Tipo Rete    Non Applicabile

 gcTipoFnteNonOmog    CONSTANT VARCHAR2(1) := '1';   -- Raggr. Fonte Non Omogenea
 gcTipoFnteNonApplic  CONSTANT VARCHAR2(1) := '2';   -- Raggr. Fonte Non Applicabile
 gcTipoFnteNonDispon  CONSTANT VARCHAR2(1) := '3';   -- Raggr. Fonte Non Disponibile

 gcTipoClieCliente    CONSTANT VARCHAR2(1) := 'C';   -- Tipo Cliente CLIENTE
 gcTipoClieNonApplic  CONSTANT VARCHAR2(1) := 'Z';   -- Tipo Cliente Non Applicabile
 gcTipoClieNonDeterm  CONSTANT VARCHAR2(1) := 'X';   -- Tipo Cliente Non Dererminato

 -------------------------------------------------------------------------------------------------------------

 gcCentroStatellite   CONSTANT VARCHAR2(2) := 'SA';   -- CentroSatellite

 -------------------------------------------------------------------------------------------------------------

 -- costanti Attributi da visualizzare (combinabili)

 gcAttribOutOff       CONSTANT INTEGER    := 0; -- nessun attributo
 gcAttribOutNome      CONSTANT INTEGER    := 1; -- Visualizza il nome
 gcAttribOutFlags     CONSTANT INTEGER    := 2; -- Visualizza i Flags
 gcAttribOutDates     CONSTANT INTEGER    := 4; -- Visualizza Date Validità della relazione

 -------------------------------------------------------------------------------------------------------------

 -- costanti Tipi Selezione Contratti sui POD

 gcContrattiTutti     CONSTANT NUMBER(1)  := 0; -- tutti i contratti
 gcContrattiValidi    CONSTANT NUMBER(1)  := 1; -- Solo i contratti validi
 gcContrattiNonValidi CONSTANT NUMBER(1)  := 2; -- Solo i contratti non validi

 -------------------------------------------------------------------------------------------------------------

 FUNCTION GetParentsCGTApp       (pAppDefaults    IN VARCHAR2,
                                   pElemento       IN VARCHAR2,
                                   pTipoElemento   IN VARCHAR2,
                                   pData           IN DATE,
                                   pGerarchia      IN NUMBER,
                                   pAttributiOut   IN INTEGER ,
                                   pUsaGerEstesa   IN INTEGER DEFAULT NULL,
                                   pTipEleHide     IN VARCHAR2 DEFAULT NULL,
                                   pTipEleExcl     IN VARCHAR2 DEFAULT NULL)  RETURN t_GerTable PIPELINED;


FUNCTION  GetGerarchiaCGTApp     (pAppDefaults    IN VARCHAR2,
                                   pElementoStart  IN VARCHAR2,
                                   pTipoElemento   IN VARCHAR2,
                                   pData           IN DATE,
                                   pGerarchia      IN number,
                                   pAttributiOut   IN INTEGER DEFAULT gcAttribOutOff,
                                   pTipiScelti     IN VARCHAR DEFAULT gcListSeparator,
                                   pUsaGerEstesa   IN INTEGER DEFAULT NULL,
                                   pTipEleHide     IN VARCHAR2 DEFAULT NULL,
                                   pTipEleExcl     IN VARCHAR2 DEFAULT NULL) RETURN t_GerTable PIPELINED;

FUNCTION  GetGerarchia           (pElementoStart  IN NUMBER,
                                   pData           IN DATE,
                                   pGerarchia      IN NUMBER,
                                   pAttributiOut   IN INTEGER DEFAULT gcAttribOutOff,
                                   pTipiScelti     IN VARCHAR DEFAULT gcListSeparator,
                                   pUsaGerEstesa   IN INTEGER DEFAULT NULL,
                                   pTipEleHide     IN VARCHAR2 DEFAULT NULL,
                                   pTipEleExcl     IN VARCHAR2 DEFAULT NULL,
   pAppDefaults  IN VARCHAR2 DEFAULT 'MAGO')
        RETURN t_GerTable PIPELINED ;
END PKG_MAGO_CREL;
/