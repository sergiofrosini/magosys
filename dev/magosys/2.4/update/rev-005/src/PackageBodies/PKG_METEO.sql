PROMPT PACKAGE BODY PKG_METEO AS

							 

create or replace PACKAGE BODY      PKG_METEO AS


/* ***********************************************************************************************************
   NAME:       PKG_METEO

   PURPOSE:    Servizi per la gestione del Meteo

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      14/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.c      21/03/2012  Risso M.         Integrazione SPC relative flusso meteo
   1.0.d      20/04/2012  Moretti C.       Avanvamento
   1.0.e      24/04/2012  Moretti C.       Avanzamento e Correzioni - Aggiornamento SPC relative flusso meteo
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.g      08/08/2012  Migliaccio G.    Gestione distrib.list diversivicata per tipi di installazione
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8a       07/10/2013  Moretti C.       Calcolo citt¿ di default per Clienti, Generatori, Trasformatori MT/BT
                                           vedi funzione GetCitta_Cliente_TrasfMT
   1.9a.3     02/10/2014  Moretti C.       Gestione Centri satellite
   1.9c.0     01/07/2015  Moretti C.       Definizione ambiente ROMANIA
   1.9c.4     23/11/2015  Campi P.         Modidica per uso generalizzato in proc GetMeteoDistribList e GetMeteoDistribListCO
   1.10.1     09/02/2016  Moretti C.       Utilizzo Sinonimi per dabelle esterne allo schema

   1.11         09/06/2016 Roberto Z.      Modificato per MAGONAZ

   1.11.3   01/07/2016  Forno S.            Supervisione + Storicizzazione della tabella FORECAST_PARAMETRI
   1.11.3   01/07/2016  Forno S.            Supervisione + Gestione periodi addestramento parametri modello
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione Reset Forecast Parameter
   1.11.3   01/07/2016  Forno S.            Supervisione + Funzione per dettaglio parametri
   1.11.3   01/08/2016  Forno S.            Supervisione + Update parametri globali dell'algoritmo
   1.11.3   01/08/2016  Forno S.            Supervisione + Get parametri globali dell'algoritmo
   1.11.3   12/08/2016  Forno S.            Supervisione + Get Periodi di addestramento parametri
   1.11.3   12/08/2016  Forno S.            Supervisione + Get_ListParameterChange
   1.11.3   12/09/2016  Forno S.            Supervisione + Get_InfoTableParameter (numero addestramento + numero cambiamenti)
   1.11.3   12/09/2016  Forno S.            Supervisione + Modificata SetForecastParameter per caso Eolico

   2.0.3    25/10/2016  Forno S.            MAGO-613 - GetAnagElementForecastParam - Fix campo Lat. Long.
   2.0.3    25/10/2016  Forno S.            MAGO-620 - GetAnagElementForecastParam- Fix campo Rete
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - storicizzo la FORECAST_PARAM_GLOBAL solo se almeno un valore ¿ cambiato
   2.0.3    25/10/2016  Forno S.            MAGO-621/619 - Fix recupero dati tappo finale
   2.0.4    28/10/2016  Forno S.            MAGO-633    Get_InfoTableParameter
   2.0.4    28/10/2016  Forno S.            MAGO-636    Get_ListParameterChange
   2.0.5    12/12/2016  Forno S.            MAGO-696    GetDettaglioForecastParam
   2.1.0    03/03/2017  Forno S.            MAGO-590  Definire i TR MT/BT come "autoproduttori"
   2.1.2    14/03/2017  Forno S.            MAGO-590  Ulteriore modifica GetTrasformatori
   2.1.3    10/05/2017  Forno               MAGO-1068 - Reset parametri con 2 fornitori meteo presenti in tabella    -- Spec. Version 2.1.3
   2.1.3    25/05/2017  Frosini             MAGO-1068 - KPI - Reset parametri in ambiente con 2 fornitori meteo
   2.1.3    25/05/2017  Frosini             MAGO-1105 (parzialm.) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.3    25/05/2017  Frosini             MAGO-1112 - correzioni recupero righe per elementi con fonte "mista" in lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1105 (completo) - KPI - Reset delta -> 0 per parametri Solari ed eliminazione righe duplicate dalla lista variazioni
   2.1.4    08/06/2017  Frosini             MAGO-1132 - corretta gestione RestForecastParametri eolici

   2.2.1   27/04/2017  Forno/Frosini        SUPERVISIONE            -- Spec. Version 2.2.1
   2.2.2   10/05/2017  Forno                MAGO-1068 - Reset parametri con 2 fornitori meteo presenti in tabella    -- Spec. Version 2.2.1
   2.2.4   23/06/2017  forno/frosini        MAGO-1046 - modificata GetForecastElements e GetProduttori/Trasformatori/Generatori per gestione disconnect -- Spec. Version 2.2.4
   2.2.7  07/08/2017  Forno				    MAGO-1249 - Problema duplicazione righe GetElementForecast a seguito di N parametri salvati per un produttore  -- Spec. Version 2.2.7
   2.3.4  18/10/2018  Forno                 FIX GetProduttori - Potenza Duplicata in caso di storicizzazione GMT con CMT sottesi
   2.3.5  23/10/2018  Forno					MAGO-1832 - Regressione MAGO-590
   2.4.2    03/12/2018  Forno S.            MAGO-1922 Parametrizzare PKG_METEO.GetAnagElementForecastParam con data   
   NOTES:

*********************************************************************************************************** */



/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

gv_RegExpSepara CONSTANT VARCHAR2(10) := '[^\|]+';

function gRegExpSepara RETURN VARCHAR2 IS BEGIN RETURN gv_RegExpSepara; END;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS

/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetCitta_Cliente_TrasfMT(pCodGest      IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                  pData         IN DATE) RETURN METEO_REL_ISTAT.COD_CITTA%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce la citta' del cliente / generatore / trasformatore MT/BT
    Utilizza SAR_ADMIN e AUI per risalire al comune opportuno
    Non fa nulla per DGF (non c'e' AUI)
-----------------------------------------------------------------------------------------------------------*/
    vCodGest  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodCitta METEO_REL_ISTAT.COD_CITTA%TYPE := NULL;
    vTipElem  TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := NULL;
BEGIN

    IF NOT PKG_Mago_utl.isMagoDgf THEN

        BEGIN

            SELECT COD_TIPO_ELEMENTO
              INTO vTipElem
              FROM ELEMENTI
             WHERE COD_GEST_ELEMENTO = pCodGest;

            IF vTipElem NOT IN (PKG_Mago_utl.gcTrasformMtBt,
                                PKG_Mago_utl.gcTrasformMtBtDett,
                                PKG_Mago_utl.gcClienteAT,
                                PKG_Mago_utl.gcClienteMT,
                                PKG_Mago_utl.gcClienteBT,
                                PKG_Mago_utl.gcGeneratoreAT,
                                PKG_Mago_utl.gcGeneratoreMT,
                                PKG_Mago_utl.gcGeneratoreBT
                               ) THEN
                RAISE NO_DATA_FOUND;
            END IF;

            IF vTipElem IN (PKG_Mago_utl.gcTrasformMtBtDett,
                            PKG_Mago_utl.gcGeneratoreAT,
                            PKG_Mago_utl.gcGeneratoreMT,
                            PKG_Mago_utl.gcGeneratoreBT
                           ) THEN
                SELECT ID_ELEMENTO -- recupera il codice gestionale del padre del genaratore
                  INTO vCodGest
                  FROM ELEMENTI E
                 INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                          AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                        AND D.DATA_DISATTIVAZIONE
                 WHERE E.COD_GEST_ELEMENTO = vCodGest;
            ELSE
                vCodGest := pCodGest;
            END IF;

            EXECUTE IMMEDIATE  'SELECT MI.COD_CITTA '                                                                       ||
                               'FROM ELEMENTI CE '                                                                         ||
                               'INNER JOIN ELEMENTI_DEF CD ON CD.COD_ELEMENTO = CE.COD_ELEMENTO '                          ||
                                                         'AND :dt BETWEEN CD.DATA_ATTIVAZIONE AND CD.DATA_DISATTIVAZIONE ' ||
                               'INNER JOIN ELEMENTI SE     ON SE.COD_GEST_ELEMENTO = CD.ID_ELEMENTO '                      ||
                               'INNER JOIN ELEMENTI_DEF SD ON SD.COD_ELEMENTO = SE.COD_ELEMENTO '                          ||
                                                         'AND :dt BETWEEN SD.DATA_ATTIVAZIONE AND SD.DATA_DISATTIVAZIONE ' ||
                               'INNER JOIN AUI_NODI_TLC AU ON AU.COD_ORG_NODO = SUBSTR(SD.ID_ELEMENTO,1,4) '               ||
                                                         'AND AU.SER_NODO     = SUBSTR(SD.ID_ELEMENTO,5,1) '               ||
                                                         'AND AU.NUM_NODO     = SUBSTR(SD.ID_ELEMENTO,6,6) '               ||
                                                         'AND AU.TRATTAMENTO  = 0 '                                        ||
                                                         'AND AU.STATO        = ''E'' '                                    ||
                               'INNER JOIN SAR_REL_CFT_COMUNI CC ON CC.CODIFICA_COMPLETA_CFT = AU.COD_CFT '                ||
                               'INNER JOIN METEO_REL_ISTAT MI ON mi.COD_ISTAT = CC.COD_ISTAT_COMUNE '                      ||
                               'WHERE CE.COD_GEST_ELEMENTO = :gest '
                          INTO vCodCitta
                         USING pData, pData, vCodGest;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN vCodCitta := NULL;
        END;
    END IF;
    RETURN vCodCitta;
END GetCitta_Cliente_TrasfMT;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetCodiciMeteo    (pRefCurs       OUT PKG_UtlGlb.t_query_cur) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce la lista dei codici meteo associati ai comuni presenti nel Centro operativo
-----------------------------------------------------------------------------------------------------------*/
BEGIN
   OPEN pRefCurs FOR   SELECT NOME, COD_CITTA, COD_ISTAT
                         FROM (SELECT DISTINCT COD_ELEMENTO_PADRE,
                                        SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,PKG_Mago_utl.cSeparatore)+1) COD_ISTAT
                                 FROM REL_ELEMENTI_GEO
                                INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_PADRE
                                WHERE SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                  AND COD_TIPO_ELEMENTO = PKG_Mago_utl.gcComune
                              )
                        INNER JOIN METEO_REL_ISTAT M USING(COD_ISTAT)
                        ORDER BY COD_ISTAT;
   PKG_Logs.TraceLog('Eseguito GetCodiciMeteo',PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetCodiciMeteo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetCodiciMeteo;

-- =============================================================================

 PROCEDURE GetElementForecast   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                 pData           IN DATE,
                                 pTipologiaRete  IN VARCHAR2,
                                 pFonte          IN VARCHAR2,
                                 pTipoProd       IN VARCHAR2,
                                 pTipoElement    IN VARCHAR2,
                                 pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pFlagPI IN NUMBER DEFAULT 1,
                                 pFlagDisconnect IN NUMBER DEFAULT 0) IS
 BEGIN
        GetElementForecastRoot   (pRefCurs,
                                 pData,
                                 pTipologiaRete,
                                 pFonte,
                                 pTipoProd,
                                 pTipoElement,
                                 pTipoGeo,
                                 pFlagPI,
                                 gDefaultElem);

 END GetElementForecast;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementForecastCG   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2,
                                pFonte          IN VARCHAR2,
                                pTipoProd       IN VARCHAR2,
                                pTipoElement    IN VARCHAR2,
                                pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pFlagDisconnect IN NUMBER DEFAULT 0,
                                pGestRoot IN VARCHAR2 DEFAULT gDefaultGest) AS
BEGIN
        GetElementForecastRoot   (pRefCurs,
                                 pData,
                                 pTipologiaRete,
                                 pFonte,
                                 pTipoProd,
                                 pTipoElement,
                                 pTipoGeo,
                                 pFlagPI,pFlagDisconnect,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));

END GetElementForecastCG;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementForecastRoot   (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pData           IN DATE,
                                pTipologiaRete  IN VARCHAR2,
                                pFonte          IN VARCHAR2,
                                pTipoProd       IN VARCHAR2,
                                pTipoElement    IN VARCHAR2,
                                pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                                pFlagPI         IN NUMBER DEFAULT 1,
                                pFlagDisconnect IN NUMBER DEFAULT 0,
                                pEleRoot IN NUMBER DEFAULT gDefaultElem) AS

/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vNum        INTEGER;
    vFlgNull    NUMBER(1) := -1;

    vIdFonti    INTEGER := PKG_Mago_utl.GetIdTipoFonti(pFonte);
    vIdReti     INTEGER := PKG_Mago_utl.GetIdTipoReti(pTipologiaRete);
    vIdProd     INTEGER := PKG_Mago_utl.GetIdTipoClienti(pTipoProd);

    vElePadre   ELEMENTI.COD_ELEMENTO%TYPE;
    vGstPadre   ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vGstElem    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
    vFonte      TIPO_FONTI.COD_RAGGR_FONTE%TYPE;
    vPotenza    ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE;
    vCitta      VARCHAR2(30);--METEO_REL_ISTAT.COD_CITTA%TYPE;
    vTipProd    TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE;
    vTipRet     TIPI_RETE.COD_TIPO_RETE%TYPE;
    vLat        NUMBER;
    vLon        NUMBER;
    vP01        FORECAST_PARAMETRI.PARAMETRO1%TYPE;
    vP02        FORECAST_PARAMETRI.PARAMETRO2%TYPE;
    vP03        FORECAST_PARAMETRI.PARAMETRO3%TYPE;
    vP04        FORECAST_PARAMETRI.PARAMETRO4%TYPE;
    vP05        FORECAST_PARAMETRI.PARAMETRO5%TYPE;
    vP06        FORECAST_PARAMETRI.PARAMETRO6%TYPE;
    vP07        FORECAST_PARAMETRI.PARAMETRO7%TYPE;
    vP08        FORECAST_PARAMETRI.PARAMETRO8%TYPE;
    vP09        FORECAST_PARAMETRI.PARAMETRO9%TYPE;
    vP10        FORECAST_PARAMETRI.PARAMETRO10%TYPE;
    vDT         FORECAST_PARAMETRI.DATA_ULTIMO_AGG%TYPE;
    
    
    vcodElemento varchar2(100);
    vCodtipoElem varchar2(100);
    vPadre varchar2(100);
    vSGPadre varchar2(100);
    vTipoProd varchar2(100);
    vEsePrincipale  elementi.cod_elemento%TYPE;

    /* -----------------------------------------------------------------------*/

    PROCEDURE sp_GetPTCZ is
    BEGIN
           
      SELECT COD_ELEMENTO_ESE into vEsePrincipale FROM DEFAULT_CO WHERE FLAG_PRIMARIO = 1;
      DBMS_OUTPUT.PUT_LINE (TO_CHAR (pData,'dd/mm/yyyy hh24:mi:ss' ) || ' --- Ese Principale: ' ||  vEsePrincipale );
      
      -- loop sui punti di confine da CREL
      FOR vRigaPTCZ IN (SELECT id_Figlio id_ele,SG_COD FROM TABLE (pkg_mago_crel.GetGerarchia( vEsePrincipale , pData, 5,0,'PTCZ')))
      LOOP         
             DBMS_OUTPUT.PUT_LINE ('-----');
             DBMS_OUTPUT.PUT_LINE ('PCTZ - '||vRigaPTCZ.SG_COD  );   
             
             -- loop sui possibili produttori (con id e tipo_elem di CREL!!!)
             FOR vRigaCli IN (SELECT * FROM TABLE (pkg_mago_crel.GetGerarchiaCGTApp( 'MAGO', vRigaPTCZ.SG_COD, 'ESE', pData, 5, 0 ,'GENMT,GENAT,GENBT,CLMT,CLAT,CLBT,TRAT,TRMT,TRBT'))
                              ELE_PTCZ , CREL.ELEM_DETT_ANAGR_AUI E_AUI 
                             where E_AUI.ID_ELE = ELE_PTCZ.ID_FIGLIO 
                               AND pData BETWEEN DT_INIZIO AND DT_FINE  
                               AND (TIPO_ELEM != 'CLMT' OR TIPO_FORN IN ('PP', 'PA')))
             LOOP
                 
                   -- recupero ID, TIPO del produttore su MAGO
                   BEGIN
                     select COD_ELEMENTO, COD_TIPO_ELEMENTO
                     into vcodElemento, vCodtipoElem 
                     FROM ELEMENTI 
                     where COD_GEST_ELEMENTO =  vRigaCli.SG_COD;
                   EXCEPTION
                        WHEN NO_DATA_FOUND THEN vcodElemento := NULL;
                   END;
        
                   
                   -- dati produttore: sbarra "padre"
                   BEGIN
                     select ID_FIGLIO, SG_COD 
                     into vPadre, vSGPadre 
                     from TABLE(pkg_mago_crel.GetParentsCGTApp( 'MAGO', vRigaCli.SG_COD,vRigaCli.TIPO_ELEM, SYSDATE, 36, 'SBCS' )) ;
                   EXCEPTION
                        WHEN NO_DATA_FOUND THEN vPadre := NULL;
                   END;
        
        
                   -- dati produttore: fonte, potenza
                   BEGIN
                        SELECT TR.cod_tipo_fonte, MIS.valore--, MIS.data_attivazione, MIS.data_disattivazione
                        into vFonte,vPotenza
                        FROM TRATTAMENTO_ELEMENTI TR
                        INNER JOIN MISURE_ACQUISITE_STATICHE MIS
                        ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                            AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata
                            and TR.cod_elemento = vcodElemento
                            and pData between MIS.data_attivazione and MIS.data_disattivazione);
                   EXCEPTION
                        WHEN NO_DATA_FOUND THEN 
                            vFonte := NULL;
                            vPotenza := null;
                   END;


                   -- dati produttore: citta'  
                   BEGIN
                         vCitta := GetCitta_Cliente_TrasfMT(vRigaCli.SG_COD ,pData);
                   EXCEPTION
                       WHEN NO_DATA_FOUND THEN vCitta := NULL;
                   END;
                
                
                   -- dati produttore: tipo produttore'  
                   BEGIN
                     select cod_tipo_cliente 
                     into vTipoProd  
                     from ELEMENTI_DEF 
                     where cod_elemento =  vRigaCli.id_ele
                       and pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                   EXCEPTION
                        WHEN NO_DATA_FOUND THEN vTipoProd := NULL;
                   END;
                   
                   
                   -- lat/long da sbarra        
                   BEGIN
                     select COORDINATA_X, coordinata_y
                     into vLat,vLon
                     from ELEMENTI_DEF 
                     where cod_elemento in( select COD_ELEMENTO 
                                            from ELEMENTI 
                                            where cod_gest_elemento =  vSGPadre) 
                       and pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                   EXCEPTION
                       WHEN NO_DATA_FOUND THEN 
                            vLat := NULL;
                            vLon := NULL;
                   END;
        
        
                   -- NVL recuperati delle select dei GETxxxRoot
                   vFonte := nvl(vFonte,PKG_Mago_utl.gcRaggrFonteSolare);
                   vTipoProd := nvl(vTipoProd,PKG_Mago_utl.gcClientePrdNonDeterm);
                   vPotenza := nvl(vPotenza,0);


                   IF vPotenza = 0 THEN
                        -- TRM, TRB, TRA
                        IF vCodtipoElem LIKE 'T%' THEN
                            BEGIN
                                SELECT POT_NOM_1 
                                INTO vPotenza
                                FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT
                                WHERE vRigaCli.SG_COD = COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_TRASF;
                            EXCEPTION
                                WHEN NO_DATA_FOUND THEN 
                                    vPotenza := 0;
                            END;
                        -- CMT, CBT, CAT
                        ELSIF vCodtipoElem LIKE 'C%' THEN
                            BEGIN
                                SELECT POT_GRUPPI
                                INTO vPotenza
                                FROM CLIENTI_TLC@PKG1_STMAUI.IT
                                WHERE vRigaCli.SG_COD = COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_CLIENTE;
                            EXCEPTION
                                WHEN NO_DATA_FOUND THEN 
                                    vPotenza := 0;
                            END;
                        -- GMT, GBT, GAT
                        ELSE
                            BEGIN
                                SELECT NVL(P_APP_NOM*N_GEN_PAR*N_GEN_PAR,0)
                                INTO vPotenza
                                FROM GENERATORI_TLC@PKG1_STMAUI.IT
                                WHERE vRigaCli.SG_COD = COD_ORG_NODO||SER_NODO||NUM_NODO||TIPO_ELEMENTO||ID_GENERATORE;
                            EXCEPTION
                                WHEN NO_DATA_FOUND THEN 
                                    vPotenza := 0;
                            END;
                        END IF;
                   END IF;



                   -- parametri forecast
                   BEGIN
                        SELECT
                            NVL(PARAMETRO1, 0),
                            NVL(PARAMETRO2, 0),
                            NVL(PARAMETRO3, 0),
                            NVL(PARAMETRO4, 0),
                            NVL(PARAMETRO5, 0),
                            NVL(PARAMETRO6, 0),
                            NVL(PARAMETRO7, 0),
                            NVL(PARAMETRO8, 0),
                            NVL(PARAMETRO9, 0),
                            NVL(PARAMETRO10,0)
                        INTO 
                            vP01,vP02,vP03,vP04,vP05,
                            vP06,vP07,vP08,vP09,vP10
                        FROM
                            FORECAST_PARAMETRI
                        WHERE COD_ELEMENTO = vcodElemento  
                        AND vfonte = cod_tipo_FONTE 
                        AND pData BETWEEN DT_INIZIO AND DT_FINE;
                   EXCEPTION
                        WHEN NO_DATA_FOUND THEN 
                            vP01 := 0;
                            vP02 := 0;
                            vP03 := 0;
                            vP04 := 0;
                            vP05 := 0;
                            vP06 := 0;
                            vP07 := 0;
                            vP08 := 0;
                            vP09 := 0;
                            vP10 := 0;
                   END;

        
                 DBMS_OUTPUT.PUT_LINE ('    '||vRigaCli.SG_COD
                 ||' -tipo ['||vCodtipoElem||'] '
                 ||' -fonte ['||vfonte||'] '
                 ||' -prod ['||vtipoprod||'] '      
                 ||' -potenza ['||vPotenza||'] '      
                 ||' -citta ['||vCitta||'] ');         
                                                                                     
                    
                  INSERT INTO gttd_FORECAST_ELEMENTS 
                        (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                       COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                       COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                       PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                       PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,
                       DATA_ULTIMO_AGG,FLG_DISCONNECT)
                   VALUES ( vPadre, vSGPadre,
                            vcodElemento,                                                         
                            vFonte,
                            vPotenza, 
                            vCitta,
                            vTipoProd,
                            PKG_Mago_utl.gcTipReteBT, --vTipRet,
                            vLat,vLon,
                            vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,
                            pData,1);
                     
                END LOOP;
                
        END LOOP;
        
    END;

    /* -----------------------------------------------------------------------*/
    /* main */

BEGIN
--dbms_output.put_line(vidfonti);
--dbms_output.put_line(vidreti);
--dbms_output.put_line(vidprod);
    DELETE FROM gttd_FORECAST_ELEMENTS;

    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcClienteAT,PKG_Mago_utl.gcClienteMT,PKG_Mago_utl.gcClienteBT);

     IF vNum > 0 THEN
        GetProduttoriRoot(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pFlagDisconnect/*ElemenNonDisconnessi*/,pEleRoot);
        LOOP
           FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;


        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN
            GetProdutGerRoot(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pEleRoot);
            LOOP
               FETCH pRefCurs INTO vElePadre,vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vElePadre,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;



    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcTrasformMtBt);

     IF vNum > 0 THEN
        GetTrasformatoriRoot(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,pFlagDisconnect /*ElemenNonDisconnessi*/,pEleRoot);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;

           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;

        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN
            GetTrasformaGerRoot(pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,pEleRoot);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;

    SELECT COUNT(*)
      INTO vNum
      FROM (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) ALF1 from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null )
     WHERE ALF1 IN (PKG_Mago_utl.gcGeneratoreAT,PKG_Mago_utl.gcGeneratoreMT,PKG_Mago_utl.gcGeneratoreBT);

     IF vNum > 0 THEN
        GetGeneratoriRoot(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pFlagDisconnect/*ElemenDisconnessi*/,pEleRoot);
        LOOP
           FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                               vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                               vGstPadre,vGstElem;
           EXIT WHEN pRefCurs%NOTFOUND;
           INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                               COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                               COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                               PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                               PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                       VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                               vCodEle,vFonte,vPotenza,vCitta,
                                               vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,0);
        END LOOP;
        CLOSE pRefCurs;

        IF (PKG_Mago_utl.isMagoDgf OR pFlagDisconnect=1) THEN

            GetGeneraGerRoot(pRefCurs,pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,pEleRoot);
            LOOP
               FETCH pRefCurs INTO vCodEle,vFonte,vPotenza,vCitta,vTipProd,vTipRet,vLat,vLon,
                                   vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,
                                   vGstPadre,vGstElem;
               EXIT WHEN pRefCurs%NOTFOUND;
               INSERT INTO gttd_FORECAST_ELEMENTS (COD_ELEMENTO_PADRE,COD_GEST_ELEMENTO_PADRE,
                                                   COD_ELEMENTO,COD_TIPO_FONTE,POTENZA_INSTALLATA,COD_CITTA,
                                                   COD_TIPO_PRODUTTORE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                                                   PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                                   PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10,DATA_ULTIMO_AGG,FLG_DISCONNECT)
                                           VALUES (PKG_Elementi.GetCodElemento(vGstPadre), vGstPadre,
                                                   vCodEle,vFonte,vPotenza,vCitta,
                                                   vTipProd,vTipRet,vLat,vLon,vP01,vP02,vP03,vP04,vP05,vP06,vP07,vP08,vP09,vP10,vDT,1);
            END LOOP;
            CLOSE pRefCurs;
        END IF;
     END IF;


  /***********************************************************/
  /************************** PTCZ ***************************/
  /* recupera i clienti/trasf/gen sottesi a punti di confine */

    sp_GetPTCZ;
 
  /***********************************************************/
  /***********************************************************/
---delete from zzz_forecast;
---insert into zzz_forecast select * from gttd_forecast_elements;

/* versione senza "doppioni" con parameter ultima data modifica  */
     OPEN pRefCurs FOR SELECT COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                              COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                              POTENZA_INSTALLATA,
                              COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                              PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                              PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG from
     (SELECT COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                              COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                              SUM(POTENZA_INSTALLATA) POTENZA_INSTALLATA,
                              COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                              PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                              PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG,
            RANK() OVER (PARTITION BY COD_ELEMENTO,COD_TIPO_FONTE
            ORDER BY DATA_ULTIMO_AGG DESC) rankData
                         FROM gttd_FORECAST_ELEMENTS G
                        INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                        INNER JOIN RAGGRUPPAMENTO_FONTI F ON F.COD_RAGGR_FONTE = G.COD_TIPO_FONTE
                        INNER JOIN TIPI_RETE R USING(COD_TIPO_RETE)
                        INNER JOIN TIPI_CLIENTE P ON P.COD_TIPO_CLIENTE = G.COD_TIPO_PRODUTTORE
                        INNER JOIN (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) COD_TIPO_ELEMENTO from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null ) X ON X.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                        WHERE COD_CITTA IS NOT NULL
                          AND BITAND(vIdFonti,F.ID_RAGGR_FONTE) = F.ID_RAGGR_FONTE
                          AND BITAND(vIdReti,R.ID_RETE) = R.ID_RETE
                          AND BITAND(vIdProd,P.ID_CLIENTE) = P.ID_CLIENTE
                        GROUP BY COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                                 COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                                 COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                                 PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                 PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG)
            where rankData=1
                        ORDER BY COD_GEST_ELEMENTO_PADRE,COD_GEST_ELEMENTO;

/* versione originale con tutti i parameter di qualunque data

     OPEN pRefCurs FOR SELECT COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                              COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                              SUM(POTENZA_INSTALLATA) POTENZA_INSTALLATA,
                              COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                              PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                              PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                         FROM gttd_FORECAST_ELEMENTS G
                        INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                        INNER JOIN RAGGRUPPAMENTO_FONTI F ON F.COD_RAGGR_FONTE = G.COD_TIPO_FONTE
                        INNER JOIN TIPI_RETE R USING(COD_TIPO_RETE)
                        INNER JOIN TIPI_CLIENTE P ON P.COD_TIPO_CLIENTE = G.COD_TIPO_PRODUTTORE
                        INNER JOIN (select regexp_substr(pTipoElement,gRegExpSepara, 1, level) COD_TIPO_ELEMENTO from dual
            connect by regexp_substr(pTipoElement, gRegExpSepara, 1, level) is not null ) X ON X.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO
                        WHERE COD_CITTA IS NOT NULL
                          AND BITAND(vIdFonti,F.ID_RAGGR_FONTE) = F.ID_RAGGR_FONTE
                          AND BITAND(vIdReti,R.ID_RETE) = R.ID_RETE
                          AND BITAND(vIdProd,P.ID_CLIENTE) = P.ID_CLIENTE
                        GROUP BY COD_ELEMENTO_PADRE, COD_GEST_ELEMENTO_PADRE,
                                 COD_ELEMENTO, E.COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO, COD_TIPO_FONTE,
                                 COD_CITTA, COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE,
                                 PARAMETRO1, PARAMETRO2, PARAMETRO3, PARAMETRO4, PARAMETRO5,
                                 PARAMETRO6, PARAMETRO7, PARAMETRO8, PARAMETRO9, PARAMETRO10, DATA_ULTIMO_AGG
                        ORDER BY COD_GEST_ELEMENTO_PADRE,COD_GEST_ELEMENTO;
 */

   PKG_Logs.TraceLog('Eseguito GetElementForecast - '||PKG_Mago_utl.StdOutDate(pData)||
                                       '   TipiRete='||pTipologiaRete||
                                          '   Fonti='||pFonte||
                                       '   TipiProd='||pTipoProd||
                                       '   TipiElem='||pTipoElement,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetElementForecast'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetElementForecastRoot;

-- =============================================================================

PROCEDURE GetProduttori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pDisconnect     IN NUMBER DEFAULT 0) AS
 BEGIN
   GetProduttoriRoot(pRefCurs, pData,pTipologiaRete,pFonte, pTipoProd,pTipoGeo, pDisconnect,gDefaultElem);
 END GetProduttori;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProduttoriCG      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pDisconnect     IN NUMBER DEFAULT 0,
                              pGestRoot IN VARCHAR2 DEFAULT gDefaultGest) AS
                              BEGIN
   GetProduttoriRoot(pRefCurs, pData,pTipologiaRete,pFonte, pTipoProd,pTipoGeo, pDisconnect,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));

END GetProduttoriCG;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProduttoriRoot      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pDisconnect     IN NUMBER DEFAULT 0,
                              pEleRoot IN NUMBER DEFAULT gDefaultElem) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

       OPEN pRefCurs FOR
              SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.FONTE,A.POTENZA_INSTALLATA,
                     NVL(A.COD_CITTA,GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(A.COD_ELEMENTO_CLIENTE),pData)) COD_CITTA,
                     A.COD_TIPO_CLIENTE,A.COD_TIPO_RETE,A.LATITUDINE,A.LONGITUDINE,
                     B.PARAMETRO1,B.PARAMETRO2,B.PARAMETRO3,B.PARAMETRO4,B.PARAMETRO5,
                     B.PARAMETRO6,B.PARAMETRO7,B.PARAMETRO8,B.PARAMETRO9,B.PARAMETRO10,B.DATA_ULTIMO_AGG
                FROM (SELECT PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,TIP_ELE_GEN,pData,
                                                           PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                                           pDisconnect) COD_ELEMENTO_CLIENTE,
                             A.COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE FONTE,A.POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN cod_geo
                                  WHEN pTipoGeo = 'A' THEN cod_geo_a
                             END COD_CITTA,
                             A.COD_TIPO_CLIENTE, A.COD_TIPO_RETE,
                             NVL(C.LATITUDINE,A.LATITUDINE)   LATITUDINE,
                             NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE
                                FROM (SELECT ROUND(DEF.COORDINATA_Y,7) LATITUDINE, ROUND(DEF.COORDINATA_X,7) LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,TR.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             CASE TR.COD_TIPO_ELEMENTO
                                                  WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                  WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                  WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                              END TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.COD_ELEMENTO = TR.COD_ELEMENTO)
                                       INNER JOIN  (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F ON (F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                        ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcClienteMT,
																	  PKG_Mago_utl.gcClienteBT,
																	  PKG_Mago_utl.gcClienteAT,
																	  PKG_Mago_utl.gcGeneratoreMT,
                                                                      PKG_Mago_utl.gcGeneratoreBT,
                                                                      PKG_Mago_utl.gcGeneratoreAT)
                                        --AND sysdate BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                        --AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE
                                          AND
                                          (
                                            pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                          AND
                                          (
                                            NOT pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE OR
                                            pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE
                                           )
                                           )


                                     )
                               ) A
                          LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                                   PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale,
                                                                                                                   pDisconnect)
                          LEFT OUTER JOIN METEO_REL_ISTAT I
                               ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO,INSTR(B.COD_GEST_ELEMENTO,PKG_Mago_utl.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                          LEFT OUTER JOIN (SELECT COD_ELEMENTO, ROUND(COORDINATA_Y,7) LATITUDINE, ROUND(COORDINATA_X,7) LONGITUDINE , cod_geo , cod_geo_a
                                             FROM ELEMENTI_DEF
                                            WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                          ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcSbarraCabSec,pData,
                                                                                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                                                                                pDisconnect)
                      ) A
                 INNER JOIN ELEMENTI_DEF E ON E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                                          AND pData BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                                          AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE
                 INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                pDisconnect))) G ON E.COD_ELEMENTO = G.COD_ELEMENTO
                 LEFT OUTER JOIN FORECAST_PARAMETRI B ON B.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE AND B.COD_TIPO_FONTE = A.FONTE  AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE
                 INNER JOIN TIPI_CLIENTE TCL ON (TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE)
                 WHERE TCL.FORNITORE = PKG_Mago_utl.gcON;

   PKG_Logs.TraceLog('Eseguito GetProduttori - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetProduttori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetProduttoriRoot;

-- =============================================================================

PROCEDURE GetProdutGer      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C') AS
 BEGIN
   GetProdutGerRoot(pRefCurs, pData,pTipologiaRete,pFonte, pTipoProd,pTipoGeo);
 END GetProdutGer;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProdutGerCG      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                              pGestRoot IN VARCHAR2 DEFAULT gDefaultGest) AS
BEGIN
   GetProdutGerRoot(pRefCurs, pData,pTipologiaRete,pFonte, pTipoProd,pTipoGeo,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));

END GetProdutGerCG;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetProdutGerRoot      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo        IN VARCHAR2 DEFAULT 'C',
                                 pEleRoot IN NUMBER DEFAULT gDefaultElem) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei produttori e relativi generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

   OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazGEO organizzazione
                                       FROM GERARCHIA_GEO rel
                                     )
              SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.FONTE,A.POTENZA_INSTALLATA,A.COD_CITTA,
                     A.COD_TIPO_CLIENTE,A.COD_TIPO_RETE,A.LATITUDINE,A.LONGITUDINE,
                     B.PARAMETRO1,B.PARAMETRO2,B.PARAMETRO3,B.PARAMETRO4,B.PARAMETRO5,
                     B.PARAMETRO6,B.PARAMETRO7,B.PARAMETRO8,B.PARAMETRO9,B.PARAMETRO10,B.DATA_ULTIMO_AGG
                FROM (SELECT A.COD_ELEMENTO_CLIENTE,A.COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE FONTE,A.POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN cod_geo
                                  WHEN pTipoGeo = 'A' THEN cod_geo_a
                             END cod_citta,
	                         COD_TIPO_CLIENTE,COD_TIPO_RETE,
                             NVL(C.LATITUDINE,A.LATITUDINE)   LATITUDINE,
                             NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
	                    FROM (SELECT A.COD_ELEMENTO,A.COD_ELEMENTO COD_ELEMENTO_GENERATORE,A.COD_RAGGR_FONTE,A.COD_TIPO_FONTE,
	                                 A.POTENZA_INSTALLATA,A.COD_TIPO_CLIENTE,A.COD_ELEMENTO_CLIENTE,A.COD_TIPO_RETE,A.TIP_ELE_GEN,A.LATITUDINE,A.LONGITUDINE
	                            FROM (SELECT ROUND(DEF.COORDINATA_Y,7) LATITUDINE,
                                             ROUND(DEF.COORDINATA_X,7) LONGITUDINE,
	                                         TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,TR.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
	                                         MIS.VALORE POTENZA_INSTALLATA,
	                                         CASE TR.COD_TIPO_ELEMENTO
	                                            WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
	                                            WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
	                                            WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
	                                         END TIP_ELE_GEN,COD_ELEMENTO_CLIENTE
	                                    FROM TRATTAMENTO_ELEMENTI TR
	                                   INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_cliente, ele.cod_elemento cod_elemento_cliente,ele.cod_tipo_elemento, rel.cod_elemento
                                                     FROM (SELECT *
                                                             FROM wgerarchia_ele rel
                                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                                          ) rel
                                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                                   WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                     AND rel.data_disattivazione != rel.data_disattivazione_calc
                                                  ) CLI ON (    tr.cod_elemento = cli.cod_elemento
                                                            AND cli.cod_tipo_elemento = CASE tr.cod_tipo_elemento
                                                                                           WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                                           WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                                           WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                                                        END
                                                           )
	                                   INNER JOIN (SELECT MIS.*,
	                                                      NVL(lead(MIS.data_attivazione-1/(24*60*60))
                                                                   OVER (PARTITION BY MIS.cod_trattamento_elem
                                                                             ORDER BY MIS.data_attivazione), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
	                                                 FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
	                                   INNER JOIN ELEMENTI_DEF DEF ON (DEF.COD_ELEMENTO = TR.COD_ELEMENTO)
	                                   INNER JOIN (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        ) F  ON (F.COD_TIPO_FONTE = TR.COD_TIPO_FONTE)
	                                   INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (RET.COD_TIPO_RETE=TR.COD_TIPO_RETE)
	                                   INNER JOIN (
                                             SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
	                                              ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
	                                   WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcClienteMT,
																		PKG_Mago_utl.gcClienteBT,
																		PKG_Mago_utl.gcClienteAT,
																		PKG_Mago_utl.gcGeneratoreMT,
	                                                                  PKG_Mago_utl.gcGeneratoreBT,
	                                                                  PKG_Mago_utl.gcGeneratoreAT)
	                                     AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
	                                     AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
	                                 ) A
                             ) A
                       INNER JOIN
                             (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                FROM (SELECT *
                                        FROM wgerarchia_geo rel
                                        WHERE organizzazione = PKG_Mago_utl.gcOrganizzazGEO
                                     ) rel
                               INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                               WHERE
                                  pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                 AND
                                 cod_tipo_elemento = PKG_Mago_utl.gcComune
                                 AND rel.data_disattivazione != rel.data_disattivazione_calc
                             ) B ON (b.cod_elemento = A.cod_elemento)
                        LEFT OUTER JOIN METEO_REL_ISTAT I
                             ON I.COD_ISTAT = NVL(SUBSTR(B.cod_gest_comune,INSTR(B.cod_gest_comune,PKG_Mago_utl.cSeparatore)+1),B.cod_gest_comune)
                       INNER JOIN
                             (SELECT rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs,
                                     ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE,
                                     ele.cod_geo, ele.cod_geo_a
                                FROM (SELECT *
                                        FROM wgerarchia_ele rel
                                       WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                     ) rel
                               INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                               WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                 AND ele.cod_tipo_elemento = PKG_Mago_utl.gcSbarraCabSec
                                 AND ele.cod_elemento != rel.cod_elemento
                                 AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                 AND rel.data_disattivazione != rel.data_disattivazione_calc
                             ) C ON (C.COD_ELEMENTO = A.COD_ELEMENTO)
                     ) A
               INNER JOIN ELEMENTI_DEF E ON E.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE
                                        AND pData BETWEEN E.DATA_ATTIVAZIONE AND E.DATA_DISATTIVAZIONE
                                        AND A.COD_TIPO_CLIENTE = E.COD_TIPO_CLIENTE
                 INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                1))) G ON E.COD_ELEMENTO = G.COD_ELEMENTO
                LEFT OUTER JOIN FORECAST_PARAMETRI B ON B.COD_ELEMENTO = A.COD_ELEMENTO_CLIENTE AND B.COD_TIPO_FONTE = A.FONTE  AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE
               INNER JOIN TIPI_CLIENTE TCL ON (TCL.COD_TIPO_CLIENTE = A.COD_TIPO_CLIENTE)
	           WHERE TCL.FORNITORE = PKG_Mago_utl.gcON;


   PKG_Logs.TraceLog('Eseguito GetProdutGer - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetProdutGer'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetProdutGerRoot;

-- =============================================================================

PROCEDURE GetGeneratori      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                              pDisconnect IN NUMBER DEFAULT 0) AS
 BEGIN
    GetGeneratoriRoot(pRefCurs, pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo);
 END GetGeneratori;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneratoriCG      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                              pDisconnect IN NUMBER DEFAULT 0,
                              pGestRoot IN VARCHAR2 DEFAULT gDefaultGest) AS
BEGIN
   GetGeneratoriRoot(pRefCurs, pData,pTipologiaRete,pFonte,pTipoProd,pTipoGeo,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));

END GetGeneratoriCG;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneratoriRoot      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                              pDisconnect IN NUMBER DEFAULT 0,
                                 pEleRoot IN NUMBER DEFAULT gDefaultElem) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vMagoDGF   VARCHAR2(1);
BEGIN

    vMagoDGF := CASE WHEN PKG_Mago_utl.IsMagoDGF THEN 'Y' ELSE 'N' END;

     OPEN pRefCurs FOR
              SELECT COD_ELEMENTO_GENERATORE,FONTE,POTENZA_INSTALLATA,
                     NVL(COD_CITTA,GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(COD_ELEMENTO_GENERATORE),pData)) COD_CITTA,
                     COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG,COD_GEST_CLIENTE,COD_GEST_GENERATORE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,I.COD_CITTA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE,NVL(C.LATITUDINE,A.LATITUDINE)LATITUDINE, NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,
                                     PKG_Elementi.GetGestElemento(PKG_Elementi.GetElementoPadre(COD_ELEMENTO,
                                                                                                CASE TIP_ELE_GEN
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                                                    WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                                                                END,pdata,1,1,
                                                                                                pdisconnect)) COD_GEST_CLIENTE
                                FROM (SELECT CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_Y,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_Y,7)
                                                  ELSE ROUND(DEF.COORDINATA_Y,7)
                                             END LATITUDINE,
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_X,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_X,7)
                                                  ELSE ROUND(DEF.COORDINATA_X,7)
                                             END LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*,
                                                         NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND tr.cod_tipo_misura = PKG_Mago_utl.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI pgeo ON (DEF.cod_geo = pgeo.cod_geo)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI AGEO ON (DEF.cod_geo_a = ageo.cod_geo)
                                       INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        )
                                       ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (ret.cod_tipo_rete=tr.cod_tipo_rete)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                      PKG_Mago_utl.gcGeneratoreBT,
                                                                      PKG_Mago_utl.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                        LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                                PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale,
                                                                                                                pdisconnect)
                       LEFT OUTER JOIN METEO_REL_ISTAT I
                            ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO,INSTR(B.COD_GEST_ELEMENTO,PKG_Mago_utl.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                       LEFT OUTER JOIN (SELECT COD_ELEMENTO, ROUND(COORDINATA_Y,7) LATITUDINE, ROUND(COORDINATA_X,7) LONGITUDINE
                                          FROM ELEMENTI_DEF
                                         WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                       ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcSbarraCabSec,pData,
                                                                                             PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                                                                             pdisconnect)
                     ) A
                 INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                1))) G ON A.COD_ELEMENTO_GENERATORE = G.COD_ELEMENTO
                LEFT OUTER JOIN FORECAST_PARAMETRI B ON B.COD_ELEMENTO = A.COD_ELEMENTO_GENERATORE  AND B.COD_TIPO_FONTE = A.FONTE AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE;

    PKG_Logs.TraceLog('Eseguito GetGeneratori - '||PKG_Mago_utl.StdOutDate(pData)||
                                   '   TipiRete='||pTipologiaRete||
                                       '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetGeneratori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetGeneratoriRoot;

-- =============================================================================

PROCEDURE GetGeneraGer(pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C') AS
BEGIN
   GetGeneraGerRoot(pRefCurs,pData,pTipologiaRete, pFonte,pTipoProd,pTipoGeo);
END GetGeneraGer;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneraGerCG      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                              pGestRoot IN VARCHAR2 DEFAULT gDefaultGest) AS
                              BEGIN
   GetGeneraGerRoot(pRefCurs,pData,pTipologiaRete, pFonte,pTipoProd,pTipoGeo,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));

END GetGeneraGerCG;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetGeneraGerRoot      (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pFonte          IN VARCHAR2,
                              pTipoProd       IN VARCHAR2,
                              pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                 pEleRoot IN NUMBER DEFAULT gDefaultElem) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei generatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vMagoDGF   VARCHAR2(1);
BEGIN

    vMagoDGF := CASE WHEN PKG_Mago_utl.IsMagoDGF THEN 'Y' ELSE 'N' END;

        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
										     rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazGEO organizzazione
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT /*+ ORDERED USE_HASH(A,B,C)*/COD_ELEMENTO_GENERATORE,FONTE,POTENZA_INSTALLATA,COD_CITTA,
                     COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE,LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG,COD_GEST_CLIENTE,COD_GEST_GENERATORE
                FROM (SELECT COD_GEST_CLIENTE,COD_GEST_GENERATORE,COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE FONTE,POTENZA_INSTALLATA,B.COD_CITTA,
                             COD_TIPO_CLIENTE,COD_TIPO_RETE,NVL(C.LATITUDINE,A.LATITUDINE)LATITUDINE, NVL(C.LONGITUDINE,A.LONGITUDINE) LONGITUDINE,
                             PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                             PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                        FROM (SELECT COD_ELEMENTO,COD_ELEMENTO COD_ELEMENTO_GENERATORE,COD_RAGGR_FONTE,COD_TIPO_FONTE,
                                     POTENZA_INSTALLATA,COD_TIPO_CLIENTE,COD_TIPO_RETE,TIP_ELE_GEN,LATITUDINE,LONGITUDINE,
                                     PKG_Elementi.GetGestElemento(COD_ELEMENTO) COD_GEST_GENERATORE,COD_GEST_CLIENTE,
                                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                                     PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                                FROM (SELECT /*+ USE_HASH(TR,MIS,DEF,PGEO,AGEO,CLI,F,RET,PR,B ) */
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_Y,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_Y,7)
                                                  ELSE ROUND(DEF.COORDINATA_Y,7)
                                             END LATITUDINE,
                                             CASE WHEN pTipoGeo = 'A' THEN ROUND(AGEO.COORDINATA_X,7)
                                                  WHEN pTipoGeo = 'P' THEN ROUND(PGEO.COORDINATA_X,7)
                                                  ELSE ROUND(DEF.COORDINATA_X,7)
                                             END  LONGITUDINE,
                                             TR.COD_ELEMENTO,TR.COD_TIPO_FONTE,F.COD_RAGGR_FONTE,DEF.COD_TIPO_CLIENTE,cli.cod_gest_cliente,TR.COD_TIPO_RETE,
                                             MIS.VALORE POTENZA_INSTALLATA,
                                             TR.COD_TIPO_ELEMENTO TIP_ELE_GEN,
                                             PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                                             PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                                        FROM TRATTAMENTO_ELEMENTI TR
                                       INNER JOIN (SELECT MIS.*
                                                         ,NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                     FROM MISURE_ACQUISITE_STATICHE MIS
                                                  ) MIS ON (MIS.cod_trattamento_elem = tr.cod_trattamento_elem AND TR.cod_tipo_misura = PKG_Mago_utl.gcPotenzaInstallata)
                                       INNER JOIN ELEMENTI_DEF DEF ON (DEF.cod_elemento = tr.cod_elemento)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI pgeo ON (DEF.cod_geo = pgeo.cod_geo)
                                        LEFT OUTER JOIN ANAGRAFICA_PUNTI AGEO ON (DEF.cod_geo_a = ageo.cod_geo)
                                        LEFT OUTER JOIN FORECAST_PARAMETRI B ON b.COD_ELEMENTO = tr.COD_ELEMENTO  AND tr.COD_TIPO_FONTE = b.cod_tipo_FONTE AND COD_TIPO_COORD = pTipoGeo AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE
                                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_cliente, ele.cod_elemento cod_elemento_cliente,ele.cod_tipo_elemento, rel.cod_elemento
                                                     FROM (SELECT *
                                                             FROM wgerarchia_ele rel
                                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                                          ) rel
                                                   INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                                   WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                                     AND rel.data_disattivazione != rel.data_disattivazione_calc
                                                  ) CLI ON (    tr.cod_elemento = cli.cod_elemento
                                                            AND cli.cod_tipo_elemento = CASE tr.cod_tipo_elemento
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreMT THEN PKG_Mago_utl.gcClienteMT
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreAT THEN PKG_Mago_utl.gcClienteAT
                                                                                             WHEN PKG_Mago_utl.gcGeneratoreBT THEN PKG_Mago_utl.gcClienteBT
                                                                                        END
                                                           )
                                       INNER JOIN (SELECT /*+ USE_NL(tmp tp) */
                                                          cod_tipo_fonte, cod_raggr_fonte
                                                     FROM gttd_VALORI_TEMP tmp
                                                    INNER JOIN TIPO_FONTI tp ON alf1 = cod_raggr_fonte
                                                    WHERE TIP =  PKG_Mago_utl.gcTmpTipFonKey
                                                  ) F  ON (f.cod_tipo_fonte = tr.cod_tipo_fonte)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        )RET ON (ret.cod_tipo_rete=tr.cod_tipo_rete)
                                       INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                                  ) PR ON (PR.COD_TIPO_CLIENTE=TR.COD_TIPO_CLIENTE)
                                       WHERE TR.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcGeneratoreMT,
                                                                      PKG_Mago_utl.gcGeneratoreBT,
                                                                      PKG_Mago_utl.gcGeneratoreAT)
                                         AND pData BETWEEN DEF.DATA_ATTIVAZIONE AND DEF.DATA_DISATTIVAZIONE
                                         AND pData BETWEEN MIS.DATA_ATTIVAZIONE AND MIS.DATA_DISATTIVAZIONE_CALC
                                     )
                             ) A
                       INNER JOIN (SELECT /*+ USE_HASH(REL,ELE,I) */
                                         ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                         , i.cod_citta
                                    FROM
                                        (SELECT *
                                           FROM wgerarchia_geo rel
                                          WHERE organizzazione = PKG_Mago_utl.gcOrganizzazGEO
                                        ) rel
                                        INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                         LEFT OUTER JOIN METEO_REL_ISTAT I
                                        ON I.COD_ISTAT = NVL(SUBSTR(ele.cod_gest_elemento,INSTR(ele.cod_gest_elemento,PKG_Mago_utl.cSeparatore)+1),ele.cod_gest_elemento)
                                       WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                         AND cod_tipo_elemento = PKG_Mago_utl.gcComune
                                         AND rel.data_disattivazione != rel.data_disattivazione_calc
                                         ) B ON B.COD_ELEMENTO = A.COD_ELEMENTO
                       INNER JOIN (SELECT /*+ USE_HASH(REL,ELE) */
                                         rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs
                                         ,ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE
                                         ,ele.cod_geo, ele.cod_geo_a
                                          FROM
                                              (SELECT *
                                                 FROM wgerarchia_ele rel
                                                WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                              ) rel
                                         INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                         WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                           AND ele.cod_tipo_elemento = PKG_Mago_utl.gcSbarraCabSec
                                           AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                           AND rel.data_disattivazione != rel.data_disattivazione_calc
                                        ) C ON C.COD_ELEMENTO = A.COD_ELEMENTO
                     ) A
                                      INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                1))) G ON A.COD_ELEMENTO_GENERATORE = G.COD_ELEMENTO
;

    PKG_Logs.TraceLog('Eseguito GetGeneraGer - '||PKG_Mago_utl.StdOutDate(pData)||
                                   '   TipiRete='||pTipologiaRete||
                                       '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetGeneraGer'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetGeneraGerRoot;

-- =============================================================================

PROCEDURE GetTrasformatori  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0)  AS
BEGIN
GetTrasformatoriRoot  (pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,pDisconnect);
END GetTrasformatori;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformatoriCG  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0,
                                 pGestRoot IN VARCHAR2 DEFAULT gDefaultGest)  AS
BEGIN
GetTrasformatoriRoot  (pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,pDisconnect,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));
END GetTrasformatoriCG;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformatoriRoot  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                             pDisconnect IN NUMBER DEFAULT 0,
                                 pEleRoot IN NUMBER DEFAULT gDefaultElem)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

        OPEN pRefCurs FOR
            SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,
                   NVL(COD_CITTA,GetCitta_Cliente_TrasfMT(PKG_Elementi.GetGestElemento(COD_ELEMENTO_TRASFORMATORE),pData)) COD_CITTA,
                   COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                   PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,
                   PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
              FROM (SELECT PKG_Elementi.GetElementoPadre (A.COD_ELEMENTO,PKG_Mago_utl.gcTrasformMtBt,pData,
                                                          PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                                          pDisconnect) COD_ELEMENTO_TRASFORMATORE,
                           COD_TIPO_FONTE FONTE, POTENZA_INSTALLATA,
                           CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                WHEN pTipoGeo = 'P' THEN cod_geo
                                WHEN pTipoGeo = 'A' THEN cod_geo_a
                           END cod_citta,
                           COD_TIPO_CLIENTE, COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                      FROM (SELECT ELE.COD_ELEMENTO,ELE.COD_TIPO_FONTE,NVL(TR.valore, 0) POTENZA_INSTALLATA,ELE.COD_TIPO_ELEMENTO,
                                   ELE.COD_TIPO_CLIENTE, ELE.COD_TIPO_RETE
                              FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                           NVL(COD_TIPO_FONTE, PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                          --NVL(COD_TIPO_CLIENTE, PKG_Mago_utl.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                           NVL(COD_TIPO_CLIENTE, PKG_Mago_utl.gcClienteAutoProd) COD_TIPO_CLIENTE         --MAGO-590  Definire i TR MT/BT come "autoproduttori"
                                      FROM (SELECT ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                   PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                              FROM ELEMENTI_DEF DEF INNER JOIN ELEMENTI ELE ON (ELE.cod_elemento = DEF.cod_elemento)
                 INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                1))) G ON ele.COD_ELEMENTO = G.COD_ELEMENTO
                                             WHERE ELE.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                             GROUP BY ELE.cod_elemento, DEF.cod_tipo_fonte, ELE.cod_tipo_elemento, DEF.cod_tipo_cliente, 'B'
                                           ) ELE
                                    ) ELE
                              LEFT OUTER JOIN (SELECT TR.*, MIS.valore, MIS.data_attivazione, MIS.data_disattivazione
                                                 FROM TRATTAMENTO_ELEMENTI TR
                                                INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                              AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata)
                                              ) TR ON (    TR.cod_elemento = ELE.cod_elemento
                                                       AND TR.cod_tipo_fonte = ELE.cod_tipo_fonte
                                                       AND TR.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        ) trete ON (trete.cod_tipo_rete = ELE.cod_tipo_rete)
                             INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        )) tfonte ON (tfonte.cod_tipo_fonte = ELE.cod_tipo_fonte)
                             INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                        ) tcli ON (tcli.cod_tipo_cliente = ELE.cod_tipo_cliente)
                             WHERE (  TR.cod_trattamento_elem IS NOT NULL
                                    OR pFlagPI = 0)
                               AND pData BETWEEN NVL (TR.DATA_ATTIVAZIONE, TO_DATE ('01011900', 'ddmmyyyy'))
                                             AND NVL (TR.DATA_DISATTIVAZIONE, TO_DATE ('01013000', 'ddmmyyyy'))
                           ) A
                      LEFT OUTER JOIN ELEMENTI B ON B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcComune,pData,
                                                                                                           PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale,
                                                                                                           pDisconnect)
                      LEFT OUTER JOIN METEO_REL_ISTAT I ON I.COD_ISTAT = NVL(SUBSTR(B.COD_GEST_ELEMENTO, INSTR(B.COD_GEST_ELEMENTO,PKG_Mago_utl.cSeparatore)+1),B.COD_GEST_ELEMENTO)
                     INNER JOIN (SELECT COD_ELEMENTO,ROUND(COORDINATA_Y,7) LATITUDINE,ROUND(COORDINATA_X,7) LONGITUDINE,cod_geo,cod_geo_a
                                   FROM ELEMENTI_DEF
                                 WHERE pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                                ) C ON C.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(A.COD_ELEMENTO,PKG_Mago_utl.gcSbarraCabSec,pData,PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pDisconnect)
                   ) A
              LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    B.COD_ELEMENTO = A.COD_ELEMENTO_TRASFORMATORE
                                                       AND B.COD_TIPO_FONTE = A.FONTE
                                                       AND COD_TIPO_COORD = pTipoGeo
													   AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE)
                                                       ;


   PKG_Logs.TraceLog('Eseguito GetTrasformatori - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetTrasformatori'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformatoriRoot;

-- =============================================================================

PROCEDURE GetTrasformaGer  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1)  AS
 BEGIN
 GetTrasformaGerRoot  (pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI);
 END GetTrasformaGer;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformaGerCG  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                                 pGestRoot IN VARCHAR2 DEFAULT gDefaultGest)  AS
BEGIN
 GetTrasformaGerRoot  (pRefCurs,pData,pFonte,pTipologiaRete,pTipoProd,pTipoGeo,pFlagPI,
                                 PKG_ELEMENTI.GETCodElemento(pGestRoot));
END GetTrasformaGerCG;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetTrasformaGerRoot  (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pData           IN DATE,
                             pFonte          IN VARCHAR2,
                             pTipologiaRete  IN VARCHAR2,
                             pTipoProd       IN VARCHAR2,
                             pTipoGeo IN VARCHAR2 DEFAULT 'C',
                             pFlagPI IN NUMBER DEFAULT 1,
                                 pEleRoot IN NUMBER DEFAULT gDefaultElem)  AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elenco dei trasformatori in un determinato istante
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
BEGIN

        OPEN pRefCurs FOR
              WITH wgerarchia_ele AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazELE organizzazione
                                        FROM GERARCHIA_IMP_SN rel
                                     )
                  ,wgerarchia_geo AS (SELECT /*+ materialize */
                                             rel.cod_elemento
                                            ,L01 || PKG_Mago_utl.cSeparatore || L02 || PKG_Mago_utl.cSeparatore || L03 || PKG_Mago_utl.cSeparatore ||
                                             L04 || PKG_Mago_utl.cSeparatore || L05 || PKG_Mago_utl.cSeparatore || L06 || PKG_Mago_utl.cSeparatore ||
                                             L07 || PKG_Mago_utl.cSeparatore || L08 || PKG_Mago_utl.cSeparatore || L09 || PKG_Mago_utl.cSeparatore ||
                                             L10 || PKG_Mago_utl.cSeparatore || L11 || PKG_Mago_utl.cSeparatore || L12 || PKG_Mago_utl.cSeparatore ||
                                             L13 || PKG_Mago_utl.cSeparatore || L14 || PKG_Mago_utl.cSeparatore || L15 || PKG_Mago_utl.cSeparatore ||
                                             L16 || PKG_Mago_utl.cSeparatore || L17 || PKG_Mago_utl.cSeparatore || L18 || PKG_Mago_utl.cSeparatore ||
                                             L19 || PKG_Mago_utl.cSeparatore || L20 || PKG_Mago_utl.cSeparatore || L21 || PKG_Mago_utl.cSeparatore ||
                                             L22 || PKG_Mago_utl.cSeparatore || L23 || PKG_Mago_utl.cSeparatore || L24 || PKG_Mago_utl.cSeparatore ||
                                             L25 list_elem
                                            ,data_attivazione, data_disattivazione
                                            ,NVL(lead(rel.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY rel.cod_elemento ORDER BY rel.data_attivazione )
                                            ,TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                            ,PKG_Mago_utl.gcOrganizzazGEO organizzazione
                                        FROM GERARCHIA_GEO rel
                                     )
              SELECT COD_ELEMENTO_TRASFORMATORE,FONTE,POTENZA_INSTALLATA,COD_CITTA,COD_TIPO_CLIENTE,COD_TIPO_RETE,LATITUDINE, LONGITUDINE,
                     PARAMETRO1,PARAMETRO2,PARAMETRO3,PARAMETRO4,PARAMETRO5,PARAMETRO6,PARAMETRO7,PARAMETRO8,PARAMETRO9,PARAMETRO10,DATA_ULTIMO_AGG
                FROM (SELECT TR.cod_elemento_trasformatore,COD_TIPO_FONTE FONTE,POTENZA_INSTALLATA,
                             CASE WHEN pTipoGeo = 'C' THEN I.COD_CITTA
                                  WHEN pTipoGeo = 'P' THEN c.cod_geo
                                  WHEN pTipoGeo ='A' THEN c.cod_geo_a
                             END cod_citta,COD_TIPO_CLIENTE,COD_TIPO_RETE, LATITUDINE, LONGITUDINE
                        FROM (SELECT ele.COD_ELEMENTO,ele.COD_TIPO_FONTE,NVL(tr.valore,0) POTENZA_INSTALLATA,ele.COD_TIPO_ELEMENTO,ele.COD_TIPO_CLIENTE,ele.COD_TIPO_RETE
                                FROM (SELECT COD_ELEMENTO, COD_TIPO_RETE, COD_TIPO_ELEMENTO,
                                             NVL(COD_TIPO_FONTE,PKG_Mago_utl.gcRaggrFonteSolare) COD_TIPO_FONTE,
                                             NVL(COD_TIPO_CLIENTE,PKG_Mago_utl.gcClientePrdNonDeterm) COD_TIPO_CLIENTE
                                        FROM (SELECT ele.cod_elemento, DEF.cod_tipo_fonte,
                                                     ele.cod_tipo_elemento, DEF.cod_tipo_cliente,
                                                     PKG_Mago_utl.gcTipReteBT cod_tipo_rete
                                                FROM ELEMENTI_DEF DEF
                                              INNER JOIN ELEMENTI ELE ON (ele.cod_elemento = DEF.cod_elemento)
                   INNER JOIN (SELECT COD_ELEMENTO FROM TABLE (PKG_ELEMENTI.LeggiGerarchiaInf(pEleRoot,
                                PKG_Mago_utl.gcOrganizzazELE,PKG_Mago_utl.gcStatoNormale,
                                pData,
                                NULL,
                                1))) G ON ELE.COD_ELEMENTO = G.COD_ELEMENTO
                                              WHERE ele.COD_TIPO_ELEMENTO IN (PKG_Mago_utl.gcTrasformMtBtDett)
                                              GROUP BY ele.cod_elemento, DEF.cod_tipo_fonte, ele.cod_tipo_elemento, DEF.cod_tipo_cliente , 'B'
                                             ) ELE
                                     ) ELE
                                LEFT OUTER JOIN (SELECT tr.* , MIS.valore, MIS.data_attivazione, MIS.data_disattivazione,
                                                        NVL(lead(MIS.data_attivazione-1/(24*60*60)) OVER ( PARTITION BY MIS.cod_trattamento_elem ORDER BY MIS.data_attivazione ), TO_DATE('01013000','ddmmyyyy')) data_disattivazione_calc
                                                   FROM TRATTAMENTO_ELEMENTI TR
                                                  INNER JOIN MISURE_ACQUISITE_STATICHE MIS ON (    MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM
                                                                                               AND TR.COD_TIPO_MISURA = PKG_Mago_utl.gcPotenzaInstallata
                                                                                               )
                                                  WHERE MIS.valore != 0
                                                ) TR ON (    TR.cod_elemento = ele.cod_elemento
                                                         AND tr.cod_tipo_fonte = ele.cod_tipo_fonte
                                                         AND tr.cod_tipo_cliente = ele.cod_tipo_cliente )
                               INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipologiaRete,gRegExpSepara, 1, level)
                                                as COD_TIPO_RETE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipologiaRete, gRegExpSepara, 1, level) IS NOT NULL
                                        ) trete ON (trete.cod_tipo_rete = ele.cod_tipo_rete)
                               INNER JOIN ( (
                                            SELECT
                                                T.COD_TIPO_FONTE,
                                                T.COD_RAGGR_FONTE
                                            FROM
                                                (
                                                    SELECT
                                                        regexp_substr(pFonte,gRegExpSepara, 1, level)
                                                        as ALF1
                                                    FROM dual
                                                    CONNECT BY regexp_substr(pFonte,gRegExpSepara, 1, level) IS NOT NULL
                                                ) PF
                                            INNER JOIN TIPO_FONTI T
                                            ON PF.ALF1 = T.COD_RAGGR_FONTE
                                        )) tfonte ON (tfonte.cod_tipo_fonte = ele.cod_tipo_fonte)
                               INNER JOIN (
                                            SELECT
                                                regexp_substr(pTipoProd,gRegExpSepara, 1, level)
                                                as COD_TIPO_CLIENTE
                                            FROM dual
                                            CONNECT BY regexp_substr(pTipoProd,gRegExpSepara, 1, level) IS NOT NULL
                                          ) tcli ON (tcli.cod_tipo_cliente = ele.cod_tipo_cliente)
                               WHERE (tr.cod_trattamento_elem IS NOT NULL OR pFlagPI = 0)
                                 AND pData BETWEEN NVL(tr.data_attivazione, TO_DATE('01011900','ddmmyyyy'))
                                               AND NVL(tr.data_disattivazione_calc,TO_DATE('01013000','ddmmyyyy'))
                             ) A
                       INNER JOIN (SELECT ele.cod_gest_elemento cod_gest_comune, ele.cod_elemento cod_elemento_comune, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_geo rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazGEO
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcComune
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) B ON (b.cod_elemento = A.cod_elemento)
                        LEFT OUTER JOIN METEO_REL_ISTAT I ON (I.COD_ISTAT = NVL(SUBSTR(B.cod_gest_comune,INSTR(B.cod_gest_comune,PKG_Mago_utl.cSeparatore)+1),B.cod_gest_comune))
                       INNER JOIN (SELECT ele.cod_elemento cod_elemento_trasformatore, rel.cod_elemento
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND cod_tipo_elemento = PKG_Mago_utl.gcTrasformMtBt
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) TR ON (tr.cod_elemento = A.cod_elemento)
                       INNER JOIN (SELECT rel.cod_elemento,ele.cod_elemento cod_elemento_sbcs,
                                          ROUND(ele.COORDINATA_Y,7) LATITUDINE, ROUND(ele.COORDINATA_X,7) LONGITUDINE,
                                          ele.cod_geo, ele.cod_geo_a
                                     FROM (SELECT *
                                             FROM wgerarchia_ele rel
                                            WHERE organizzazione = PKG_Mago_utl.gcOrganizzazELE
                                          ) rel
                                    INNER JOIN ELEMENTI_DEF ele ON (list_elem LIKE '%' || PKG_Mago_utl.cSeparatore || ele.cod_elemento || PKG_Mago_utl.cSeparatore || '%')
                                    WHERE pData BETWEEN rel.data_attivazione AND rel.data_disattivazione_calc
                                      AND ele.cod_tipo_elemento = PKG_Mago_utl.gcSbarraCabSec
                                      AND ele.cod_elemento != rel.cod_elemento
                                      AND pData BETWEEN ele.DATA_ATTIVAZIONE AND ele.DATA_DISATTIVAZIONE
                                      AND rel.data_disattivazione != rel.data_disattivazione_calc
                                  ) C ON (C.COD_ELEMENTO = A.COD_ELEMENTO)
                        ) A
                   LEFT OUTER JOIN FORECAST_PARAMETRI B ON (    B.COD_ELEMENTO = A.COD_ELEMENTO_TRASFORMATORE
                                                            AND B.COD_TIPO_FONTE = A.FONTE
                                                            AND COD_TIPO_COORD = pTipoGeo
															AND pData BETWEEN B.DT_INIZIO AND B.DT_FINE);

   PKG_Logs.TraceLog('Eseguito GetTrasformaGer - '||PKG_Mago_utl.StdOutDate(pData)||
                                  '   TipiRete='||pTipologiaRete||
                                     '   Fonti='||pFonte||
                                  '   TipiProd='||pTipoProd,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetTrasformaGer'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetTrasformaGerRoot;

-- =============================================================================

PROCEDURE GetMeteo             (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pDataDa         IN DATE,
                                pDataA          IN DATE,
                                pListaCitta     IN VARCHAR2 DEFAULT NULL,
                                pTipoMeteo      IN INTEGER,
                                pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                pCodPrev IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le previsioni meteo per le citta' ricevute
    Se ListaCitta = NULL si intende che si vogliono le previsioni di tutte le citta'
------------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vListaCitta PKG_UtlGlb.t_SplitTbl;
    cTipCitta   CONSTANT gttd_VALORI_TEMP.TIP%TYPE := 'CODCITTA';

BEGIN

    DELETE FROM gttd_VALORI_TEMP WHERE TIP = cTipCitta;

    IF pTipoMeteo = 0 THEN
        IF pListaCitta IS NULL THEN
            INSERT INTO gttd_VALORI_TEMP (TIP,ALF1) SELECT cTipCitta,TO_CHAR(COD_CITTA) FROM METEO_REL_ISTAT;
        ELSE
            PKG_MAgo_utl.TrattaListaCodici(pListaCitta, cTipCitta, vFlgNull);
        END IF;
        OPEN pRefCurs FOR SELECT METEO.COD_CITTA, DATA, TEMPERATURA, DESCRIZIONE, PRECIPITAZIONI, DIREZIONE_VENTO, VELOCITA_VENTO,
                                 INDICE_DI_RAFFREDDAMENTO, INDICE_DI_CALORE, UMIDITA_RELATIVA, VISIBILITA, PRESSIONE, HZERO,
                                 IRRAGGIAMENTO_SOLARE, null IRRAGGIAMENTO_SOLARE_NOWCAST, null TEMPERATURA_NOWCAST
                            FROM METEO_PREVISIONE METEO
                           INNER JOIN (SELECT TO_NUMBER(ALF1) COD_CITTA
                                         FROM gttd_VALORI_TEMP WHERE TIP = cTipCitta
                                      )TMP ON (TMP.COD_CITTA = METEO.COD_CITTA)
                           WHERE DATA BETWEEN pDataDa AND pDataA
                             AND COD_TIPO_COORD = pTipoGeo
                             AND NVL(COD_PREV_METEO,'0') = NVL(pCodPrev,'0');
    ELSE
        PKG_MAgo_utl.TrattaListaCodici(pListaCitta, cTipCitta, vFlgNull);
        OPEN pRefCurs FOR SELECT MIS.COD_ELEMENTO COD_CITTA, MIS.DATA, MIS.TEMPERATURA, NULL DESCRIZIONE, NULL PRECIPITAZIONI, MIS.DIREZIONE_VENTO, MIS.VELOCITA_VENTO,
                                 NULL INDICE_DI_RAFFREDDAMENTO, NULL INDICE_DI_CALORE, NULL UMIDITA_RELATIVA, NULL VISIBILITA, NULL PRESSIONE, NULL HZERO,
                                 MIS.IRRAGGIAMENTO_SOLARE, MIS.IRRAGGIAMENTO_SOLARE_NOWCAST, MIS.TEMPERATURA_NOWCAST
                            FROM
                                 (SELECT TR.COD_ELEMENTO
                                        ,MIS.DATA
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'TMP' THEN MIS.VALORE ELSE NULL END) TEMPERATURA
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'DVE' THEN MIS.VALORE ELSE NULL END) DIREZIONE_VENTO
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'VVE' THEN MIS.VALORE ELSE NULL END) VELOCITA_VENTO
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'IRR' THEN MIS.VALORE ELSE NULL END) IRRAGGIAMENTO_SOLARE
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'IRR.N1' THEN MIS.VALORE ELSE NULL END) IRRAGGIAMENTO_SOLARE_NOWCAST
                                        ,MAX(CASE WHEN TR.COD_TIPO_MISURA = 'TMP.N1' THEN MIS.VALORE ELSE NULL END) TEMPERATURA_NOWCAST
                                    FROM MISURE_ACQUISITE MIS
                                   INNER JOIN TRATTAMENTO_ELEMENTI TR ON (MIS.COD_TRATTAMENTO_ELEM = TR.COD_TRATTAMENTO_ELEM)
                                   WHERE TR.COD_TIPO_MISURA IN ('TMP','DVE','VVE', 'IRR', 'IRR.N1', 'TMP.N1')
                                     AND MIS.DATA BETWEEN pDataDa AND pDataA
                                   GROUP BY TR.COD_ELEMENTO ,MIS.DATA
                                 ) MIS
                           INNER JOIN ELEMENTI B ON (B.COD_ELEMENTO = MIS.COD_ELEMENTO)
                           INNER JOIN gttd_VALORI_TEMP TMP ON TMP.ALF1 = B.COD_ELEMENTO
                           WHERE  TIP = cTipCitta;
--                           INNER JOIN ELEMENTI B ON (B.COD_ELEMENTO = PKG_Elementi.GetElementoPadre(MIS.COD_ELEMENTO,PKG_Mago_utl.gcComune,pDataA,
--                                                                                                    PKG_Mago_utl.gcOrganizzazGEO,PKG_Mago_utl.gcStatoNormale)
--                                                     )
--                           INNER JOIN (SELECT TO_NUMBER(ALF1) COD_CITTA, ISTAT.COD_ISTAT
--                                         FROM gttd_VALORI_TEMP TMP
--                                         INNER JOIN METEO_REL_ISTAT ISTAT
--                                           ON (  TO_NUMBER(TMP.ALF1) =  TO_NUMBER(ISTAT.COD_CITTA) )
--                                        WHERE TIP = cTipCitta
--                                      ) TMP ON (TMP.COD_ISTAT = B.COD_GEST_ELEMENTO);
    END IF;

   PKG_Logs.TraceLog('Eseguito GetMeteo - Periodo '||PKG_Mago_utl.StdOutDate(pDataDa)||' - '||PKG_Mago_utl.StdOutDate(pDataA)||
                                  '   TipoMeteo='||pTipoMeteo||CHR(10)||
                                  '   ListaCitta='||pListaCitta,PKG_UtlGlb.gcTrace_VRB);
EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteo'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
END GetMeteo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetForecastParameter (pForecastParam  IN T_PARAM_PREV_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Memorizza i parametri per il calcolo del Forecast
-----------------------------------------------------------------------------------------------------------*/
    vTab                PKG_GestAnagr.t_DefAnagr;
   -- vRec                FORECAST_PARAMETRI%ROWTYPE;
  p_data DATE;

  vIns NUMBER := 0;
  vMod NUMBER := 0;
BEGIN

    IF pForecastParam.FIRST IS NOT NULL THEN
        FOR i IN pForecastParam.FIRST .. pForecastParam.LAST LOOP

            p_data := pForecastParam(i).DATA_ULTIMO_AGG;

            -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
            PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
            PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
            PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
            PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
            PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'V_CUT_IN', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'V_CUT_OFF', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'V_MAX_POWER', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
            PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);

            PKG_GestAnagr.InitRow  (vTab);
            PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pForecastParam(i).COD_ELEMENTO);
            PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pForecastParam(i).COD_TIPO_FONTE);
            PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pForecastParam(i).COD_TIPO_COORD);
            PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pForecastParam(i).COD_PREV_METEO);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', pForecastParam(i).PARAMETRO1);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', pForecastParam(i).PARAMETRO2);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', pForecastParam(i).PARAMETRO3);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4', pForecastParam(i).PARAMETRO4);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', pForecastParam(i).PARAMETRO5);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pForecastParam(i).PARAMETRO6);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pForecastParam(i).PARAMETRO7);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pForecastParam(i).PARAMETRO8);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pForecastParam(i).PARAMETRO9);
            PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pForecastParam(i).PARAMETRO10);
            PKG_GestAnagr.AddVal   (vTab,'V_CUT_IN', pForecastParam(i).V_CUT_IN);
            PKG_GestAnagr.AddVal   (vTab,'V_CUT_OFF', pForecastParam(i).V_CUT_OFF);
            PKG_GestAnagr.AddVal   (vTab,'V_MAX_POWER', pForecastParam(i).V_MAX_POWER);
            PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', pForecastParam(i).DATA_ULTIMO_AGG);
            PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pForecastParam(i).DT_INIZIO_CALCOLO);
            PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pForecastParam(i).DT_FINE_CALCOLO);
            PKG_GestAnagr.Elabora  (vTab);
            -- FINE ELABORAZIONE per FORECAST_PARAMETRI


--            UPDATE FORECAST_PARAMETRI SET DATA_ULTIMO_AGG = pForecastParam(i).DATA_ULTIMO_AGG,
--                                          PARAMETRO1      = pForecastParam(i).PARAMETRO1,
--                                          PARAMETRO2      = pForecastParam(i).PARAMETRO2,
--                                          PARAMETRO3      = pForecastParam(i).PARAMETRO3,
--                                          PARAMETRO4      = pForecastParam(i).PARAMETRO4,
--                                          PARAMETRO5      = pForecastParam(i).PARAMETRO5,
--                                          PARAMETRO6      = pForecastParam(i).PARAMETRO6,
--                                          PARAMETRO7      = pForecastParam(i).PARAMETRO7,
--                                          PARAMETRO8      = pForecastParam(i).PARAMETRO8,
--                                          PARAMETRO9      = pForecastParam(i).PARAMETRO9,
--                                          PARAMETRO10     = pForecastParam(i).PARAMETRO10
--             WHERE COD_ELEMENTO = pForecastParam(i).COD_ELEMENTO
--               AND COD_TIPO_FONTE  = pForecastParam(i).COD_TIPO_FONTE
--               AND COD_TIPO_COORD = pForecastParam(i).COD_TIPO_COORD
--               AND COD_PREV_METEO = pForecastParam(i).COD_PREV_METEO;
--             IF SQL%ROWCOUNT = 0 THEN
--                INSERT INTO FORECAST_PARAMETRI (COD_ELEMENTO,
--                                                COD_TIPO_FONTE,
--                                                COD_TIPO_COORD,
--                                                COD_PREV_METEO,
--                                                DATA_ULTIMO_AGG,
--                                                PARAMETRO1,
--                                                PARAMETRO2,
--                                                PARAMETRO3,
--                                                PARAMETRO4,
--                                                PARAMETRO5,
--                                                PARAMETRO6,
--                                                PARAMETRO7,
--                                                PARAMETRO8,
--                                                PARAMETRO9,
--                                                PARAMETRO10)
--                                        VALUES (pForecastParam(i).COD_ELEMENTO,
--                                                pForecastParam(i).COD_TIPO_FONTE,
--                                                pForecastParam(i).COD_TIPO_COORD,
--                                                pForecastParam(i).COD_PREV_METEO,
--                                                pForecastParam(i).DATA_ULTIMO_AGG,
--                                                pForecastParam(i).PARAMETRO1,
--                                                pForecastParam(i).PARAMETRO2,
--                                                pForecastParam(i).PARAMETRO3,
--                                                pForecastParam(i).PARAMETRO4,
--                                                pForecastParam(i).PARAMETRO5,
--                                                pForecastParam(i).PARAMETRO6,
--                                                pForecastParam(i).PARAMETRO7,
--                                                pForecastParam(i).PARAMETRO8,
--                                                pForecastParam(i).PARAMETRO9,
--                                                pForecastParam(i).PARAMETRO10);
                vIns := vIns + 1;
--             ELSE
--                vMod := vMod + 1;
--             END IF;
        END LOOP;
    END IF;

    COMMIT;

    PKG_Logs.TraceLog('Eseguito SetForecastParameter - Elaborati ='||vIns ,PKG_UtlGlb.gcTrace_VRB);
    --PKG_Logs.TraceLog('Eseguito SetForecastParameter - Inseriti='||vIns||'   Modificati='||vMod,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.SetForecastParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END SetForecastParameter;
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetEolicParameter    (pEolicParam     IN T_PARAM_EOLIC_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Memorizza i parametri per il calcolo del Forecast (dati eolici)
-----------------------------------------------------------------------------------------------------------*/
  vIns NUMBER := 0;
  vMod NUMBER := 0;
BEGIN

    IF pEolicParam.FIRST IS NOT NULL THEN
        FOR i IN pEolicParam.FIRST .. pEolicParam.LAST LOOP
            UPDATE FORECAST_PARAMETRI SET DATA_ULTIMO_AGG = pEolicParam(i).DATA_ULTIMO_AGG,
                                          V_CUT_IN        = pEolicParam(i).V_CUT_IN,
                                          V_CUT_OFF       = pEolicParam(i).V_CUT_OFF,
                                          V_MAX_POWER     = pEolicParam(i).V_MAX_POWER
             WHERE COD_ELEMENTO = pEolicParam(i).COD_ELEMENTO
               AND COD_TIPO_FONTE = PKG_Mago_utl.gcRaggrFonteEolica
               AND COD_TIPO_COORD = pEolicParam(i).COD_TIPO_COORD;
             IF SQL%ROWCOUNT = 0 THEN
                INSERT INTO FORECAST_PARAMETRI (COD_ELEMENTO,
                                                COD_TIPO_FONTE,
                                                DATA_ULTIMO_AGG,
                                                V_CUT_IN,
                                                V_CUT_OFF,
                                                V_MAX_POWER,
                                                COD_TIPO_COORD,
                                                COD_PREV_METEO)
                                        VALUES (pEolicParam(i).COD_ELEMENTO,
                                                PKG_Mago_utl.gcRaggrFonteEolica,
                                                pEolicParam(i).DATA_ULTIMO_AGG,
                                                pEolicParam(i).V_CUT_IN,
                                                pEolicParam(i).V_CUT_OFF,
                                                pEolicParam(i).V_MAX_POWER,
                                                pEolicParam(i).COD_TIPO_COORD,
                                                pEolicParam(i).COD_PREV_METEO);
                vIns := vIns + 1;
             ELSE
                vMod := vMod + 1;
             END IF;
        END LOOP;
    END IF;

    COMMIT;

    PKG_Logs.TraceLog('Eseguito SetEolicParameter - Inseriti='||vIns||'   Modificati='||vMod,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.SetEolicParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END SetEolicParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetEolicParameter    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                                pTipiElemento   IN VARCHAR2,
                                pData           IN DATE DEFAULT SYSDATE,
                                pTipoGeo IN VARCHAR2 DEFAULT 'C',
                                pCodPrevMeteo IN VARCHAR2 DEFAULT '0') AS
/*-----------------------------------------------------------------------------------------------------------
     Restituisce i parametri eolici
-----------------------------------------------------------------------------------------------------------*/
  vTipEle  PKG_UtlGlb.t_SplitTbl;
  vFlgNull NUMBER(1) := -1;

  vSqlSTM  VARCHAR2(0600) := 'SELECT E.COD_ELEMENTO,DATA_ULTIMO_AGG,'                                            ||
                                    '0 VALORI_REALI,V_CUT_IN,V_CUT_OFF,V_MAX_POWER '                             ||
                              'FROM ELEMENTI E '                                                                 ||
                             'INNER JOIN ELEMENTI_DEF G ON G.COD_ELEMENTO = E.COD_ELEMENTO '                     ||
                             'LEFT OUTER JOIN FORECAST_PARAMETRI F '                                                  ||
                                               'ON (    F.COD_ELEMENTO = E.COD_ELEMENTO '                        ||
                                               ' AND NVL(F.COD_PREV_METEO,''' || pCodPrevMeteo || ''') = ''' || pCodPrevMeteo || '''' ||
                                               ' AND NVL(F.COD_TIPO_COORD,'''|| pTipoGeo || ''') = ''' || pTipoGeo || '''' ||
                                                   'AND F.COD_TIPO_FONTE='''||PKG_Mago_utl.gcRaggrFonteEolica||''') '||
                             'INNER JOIN (SELECT ALF1 COD_TIPO_ELEMENTO '                                        ||
                                           'FROM gttd_VALORI_TEMP '                                              ||
                                          'WHERE TIP = '''||PKG_Mago_utl.gcTmpTipEleKey||''' '                       ||
                                        ') T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO '                      ||
                             'WHERE :dt BETWEEN G.DATA_ATTIVAZIONE AND G.DATA_DISATTIVAZIONE '                   || -- pData
                             'ORDER BY E.COD_ELEMENTO';

  vSqlGDF  VARCHAR2(1000) := 'SELECT COD_ELEMENTO, NVL(DATA_ULTIMO_AGG,DATA_ATTIVAZIONE) DATA_ULTIMO_AGG, '      ||
                                'FLAG VALORI_REALI,'                                                             ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_CUT_IN '                                               ||
                                         'ELSE        G_V_CUT_IN '                                               ||
                                    'END V_CUT_IN,'                                                              ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_CUT_OFF '                                              ||
                                         'ELSE        G_V_CUT_OFF '                                              ||
                                    'END V_CUT_OFF,'                                                             ||
                                    'CASE FLAG '                                                                 ||
                                         'WHEN 0 THEN F_V_MAX_POWER '                                            ||
                                         'ELSE        G_V_MAX_POWER '                                            ||
                                    'END V_MAX_POWER '                                                           ||
                               'FROM (SELECT E.COD_ELEMENTO,DATA_ULTIMO_AGG,G.DATA_ATTIVAZIONE, '                ||
                                            'CASE '                                                              ||
                                               'WHEN G.COD_ELEMENTO IS NOT NULL THEN 1 '                         ||
                                               'ELSE 0 '                                                         ||
                                            'END FLAG,'                                                          ||
                                            'G.VEL_CUTIN   G_V_CUT_IN,'                                          ||
                                            'G.VEL_CUTOFF  G_V_CUT_OFF,'                                         ||
                                            'G.VEL_MAX     G_V_MAX_POWER,'                                       ||
                                            'F.V_CUT_IN    F_V_CUT_IN,'                                          ||
                                            'F.V_CUT_OFF   F_V_CUT_OFF,'                                         ||
                                            'F.V_MAX_POWER F_V_MAX_POWER '                                       ||
                                       'FROM ELEMENTI E '                                                        ||
                                      'INNER JOIN ELEMENTI_GDF_EOLICO G ON G.COD_ELEMENTO = E.COD_ELEMENTO '     ||
                                       'LEFT OUTER JOIN FORECAST_PARAMETRI F ON F.COD_ELEMENTO = E.COD_ELEMENTO '||
                                      'INNER JOIN (SELECT ALF1 COD_TIPO_ELEMENTO '                               ||
                                                    'FROM gttd_VALORI_TEMP '                                     ||
                                                   'WHERE TIP = '''||PKG_Mago_utl.gcTmpTipEleKey||''' '              ||
                                                 ') T ON E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO '             ||
                                      'WHERE :dt BETWEEN G.DATA_ATTIVAZIONE AND G.DATA_DISATTIVAZIONE '          || -- pData
                                    ') '                                                                         ||
                              'ORDER BY COD_ELEMENTO ';

BEGIN

    DELETE FROM gttd_VALORI_TEMP WHERE TIP = PKG_Mago_utl.gcTmpTipEleKey;
    PKG_MAgo_utl.TrattaListaCodici(pTipiElemento, PKG_Mago_utl.gcTmpTipEleKey, vFlgNull);

    IF PKG_Mago_utl.IsMagoDGF THEN
        OPEN pRefCurs FOR vSqlGDF USING pData;
    ELSE
        OPEN pRefCurs FOR vSqlSTM USING pData;
    END IF;

   PKG_Logs.TraceLog('Eseguito GetEolicParameter - '||PKG_Mago_utl.StdOutDate(pData)||'   TipiElem='||pTipiElemento,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetEolicParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetEolicParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE ElaboraMisure     (pMisureMeteo    IN T_MISMETEO_ARRAY) AS
/*-----------------------------------------------------------------------------------------------------------
     Elabora le misure presenti in tabella MISURE_METEO per la memorizzazione definitiva
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    PKG_Misure.AddMisureMeteo(pMisureMeteo);
END ElaboraMisure;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE MDScompleted         (pRefCurs        OUT PKG_UtlGlb.t_query_cur,
                                pFinishTimestamp IN DATE) AS
/*-----------------------------------------------------------------------------------------------------------
     Riceve l'indicazione di fine caricamento misure da MDS
     Lancia la richiesta di elabirazione aggregate
-----------------------------------------------------------------------------------------------------------*/
  vTxt  VARCHAR2(300);
BEGIN

   DBMS_SCHEDULER.RUN_JOB('MAGO_INS_REQ_AGG_METEO',FALSE);

   OPEN pRefCurs FOR  SELECT 'OK' MESSAGE FROM DUAL;

   PKG_Logs.TraceLog('Eseguito MDScompleted',PKG_UtlGlb.gcTrace_VRB);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         vTxt := SQLERRM;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.MDScompleted'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR  SELECT vTxt MESSAGE FROM DUAL;
         RETURN;

END MDScompleted;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetMeteoDistribListCO (pRefCurs OUT PKG_UtlGlb.t_query_cur) AS
/* ---------------------
   Ritorna le credenziali ftp  host/usr/passwd/DestDir
    per distribuire i file meteo-zip ai vari C.O. per mezzo del meto-ftp-service.
    Sul locale non e' significativa (basta che sia definita e ritorni un refCur)
-- record del pRefCurs:
--  cod_gest_CO VARCHAR2
--  nome_CO     VARCHAR2
--  host_name   VARCHAR2
--  OSusr       VARCHAR2
--  Ospassw     VARCHAR2
--  fildir     VARCHAR2 (sempre la stessa)
------------------------- */
  v_osusr        VARCHAR2(16) := 'magosys';
  v_ospsw        VARCHAR2(16) := 'magosys';
  k_appl_mago    CONSTANT VARCHAR2(6) := 'MAGO';

  v_mago_fildir  VARCHAR2(60);
  v_s1           VARCHAR2(200);
  v_j1           VARCHAR2(200);
  v_j3           VARCHAR2(200);
  v_wh           VARCHAR2(50);
BEGIN

   IF PKG_Mago_utl.IsMagoDGF  THEN
      -- DGF non usa ftp-service
      v_mago_fildir := '/home/medase/meteodatafile';
      v_osusr := 'medase';
        v_ospsw := 'no_Need';
   ELSE
      v_mago_fildir := '/usr/NEW/magosys/meteofile';
      v_ospsw := INITCAP(v_osusr);
   END IF;

  -- nota DGF: u.codifica_utr|| e.codifica_esercizio = elementi.cod_gest_elemento (tipo_elemento=ESE)
   v_s1 := 'SELECT distinct u.codifica_utr|| e.codifica_esercizio cod_gest_CO, e.nome nome_CO, rs.ip_pkg2 host_name, '||
                 ' :posusr OSusr, :pospw Ospassw, :pmago_fildir fildir ';

   v_j1 := ' FROM sar_unita_territoriali u '||
           ' INNER JOIN sar_esercizi e '||
           '   USING(cod_utr) '||
           ' INNER JOIN sar_retesar_esercizi rs '||
           '    USING(cod_utr,cod_esercizio) ';

   v_j3 := ' INNER JOIN ( SELECT cod_utr,cod_esercizio,cod_applicazione '||
                             '  FROM sar_esercizi_abilitati '||
                             ' WHERE cod_applicazione=:pappl_mago)  ab '||
           ' USING (cod_utr,cod_esercizio) ';

   OPEN pRefCurs FOR v_s1||v_j1||v_j3 USING  v_osusr,v_ospsw, v_mago_fildir, k_appl_mago;

   PKG_Logs.TraceLog('Eseguito GetMeteoDistribListCO',PKG_UtlGlb.gcTrace_VRB);

   EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteoDistribListCO'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetMeteoDistribListCO;



PROCEDURE GetMeteoDistribList (pRefCurs OUT PKG_UtlGlb.t_query_cur,pTipoGeo IN VARCHAR2 DEFAULT 'C') AS
/* ---------------------
   Ritorna lla lista dei comuni appartenenti ai vari CO.
   Sul nazionale si ha una lista suddivisa per CO (in uso a meteo-ftp-service)
   Sui locali ritorna la lsita dei comuni del CO abilitato (in uso a meteo-data-service)
-- record del pRefCurs:
--  cod_gest_CO VARCHAR2
--  cod_citta   VARCHAR2
--  nome_citta  VARCHAR2
------------------------- */

  k_appl_mago   CONSTANT VARCHAR2(6) := 'MAGO';
  v_s2          VARCHAR2(200);
  v_j1          VARCHAR2(200);
  v_j2          VARCHAR2(200);
  v_j3          VARCHAR2(200);
  v_j4          VARCHAR2(400);
  v_wh          VARCHAR2(200);

BEGIN

  IF PKG_Mago_utl.IsMagoSTM THEN

      v_s2 := 'SELECT distinct u.codifica_utr||e.codifica_esercizio cod_gest_CO,'||
                CASE WHEN pTipoGeo IN ('A','P') THEN 'geo.cod_geo '
                                                ELSE ' m.cod_citta '
                END ||' cod_citta,m.nome nome_citta ';

      v_j1 := ' FROM sar_unita_territoriali u '||
              ' INNER JOIN sar_esercizi e '||
              '   USING(cod_utr) '||
              ' INNER JOIN sar_retesar_esercizi rs '||
              '    USING(cod_utr,cod_esercizio) ';

      v_j2 := ' INNER JOIN sar_rel_cft_comuni rel '||
              '     USING(cod_utr,cod_esercizio) '||
              ' INNER JOIN meteo_rel_istat m '||
              '  ON rel.cod_istat_comune = m.cod_istat ';

      v_j3 := ' INNER JOIN ( SELECT cod_utr,cod_esercizio,cod_applicazione '||
              '  FROM sar_esercizi_abilitati '||
              ' WHERE cod_applicazione=:pappl_mago)  ab '||
              ' USING (cod_utr,cod_esercizio) ';

      v_j4 := ' INNER JOIN (SELECT distinct pkg_elementi.getgestelemento(pkg_elementi.GetElementoPadre(elegeo.cod_elemento,'''||PKG_Mago_utl.gcComune||''',TO_DATE(''01013000'',''ddmmyyyy''),'||PKG_Mago_utl.gcOrganizzazGEO||','||PKG_Mago_utl.gcStatoNormale||')) cod_istat,'||
                    CASE WHEN pTipoGeo = 'A' THEN ' elegeo.cod_geo_a '
                         WHEN pTipoGeo = 'P' THEN ' elegeo.cod_geo '
                                             ELSE ' NULL '
                    END||' cod_geo '||
              '  FROM elementi_def elegeo '||
              ' WHERE elegeo.data_disattivazione = TO_DATE(''01013000'',''ddmmyyyy'')'||
              '   AND '||CASE WHEN pTipoGeo = 'A' THEN ' cod_geo_a IS NOT NULL '
                                                    ELSE ' cod_geo IS NOT NULL '
                           END||')  geo '||
              ' ON m.cod_istat = geo.cod_istat ';

      PKG_Logs.StdLogAddTxt  ('Is STM',TRUE,NULL);
      DBMS_OUTPUT.PUT_LINE(v_s2||v_j1||v_j2||v_j3||CASE WHEN pTipoGeo IN ('A','P') THEN v_j4 END);
      OPEN pRefCurs FOR v_s2||v_j1||v_j2||v_j3||CASE WHEN pTipoGeo IN ('A','P') THEN v_j4 END USING k_appl_mago;

  ELSIF PKG_Mago_utl.IsMagoDGF THEN -- IsMagoDGF (attualmente il dgf e' sempre locale)

      PKG_Logs.StdLogAddTxt  ('IsDGF',TRUE,NULL);

      v_s2 := 'SELECT distinct elese.cod_gest_elemento cod_gest_CO,'||CASE WHEN pTipoGeo = 'A' THEN 'elegeo.cod_geo_a ' WHEN pTipoGeo = 'P' THEN 'elegeo.cod_geo ' ELSE 'com.cod_citta ' END||'cod_citta,com.nome nome_citta ';

      v_j1 := ' FROM elementi_def elegeo '||
              ' INNER JOIN meteo_rel_istat com '||
              ' ON elegeo.rif_elemento = com.cod_istat ';

      v_j2 := ' CROSS JOIN elementi elese ';

      v_j3 := '';

      v_wh := ' WHERE elegeo.cod_tipo_elemento like ''G_T'''||
                    ' AND elegeo.data_disattivazione = TO_DATE(''01013000'',''ddmmyyyy'')'||
                    ' AND elese.cod_tipo_elemento =''ESE''';

      OPEN pRefCurs FOR v_s2||v_j1||v_j2||v_wh;

      PKG_Logs.TraceLog('Eseguito GetMeteoDistribList',PKG_UtlGlb.gcTrace_VRB);

  ELSE
    -- parametro (solo nazionale) non gestito
    PKG_Logs.TraceLog('Eseguito GetMeteoDistribList - Nothing to do',PKG_UtlGlb.gcTrace_VRB);
    RAISE pkg_UtlGlb.geNothingToDo;
  END IF;


EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetMeteoDistribList'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetMeteoDistribList;
/***    SUPERVIONE FORECAST PARAMETER   ***/


 PROCEDURE SetForecastParamIntervTraining (pForecastParam  IN T_PARAM_INTERV_TRAINING_ARRAY) AS
 BEGIN

    IF pForecastParam.FIRST IS NOT NULL THEN
        FOR i IN pForecastParam.FIRST .. pForecastParam.LAST LOOP
            -- mantenere solo l'ultimo addestramento quindi se presente cancellare il precedente memorizzato
            DELETE FORECAST_PARAM_INTERV_TRAINING where
            COD_ELEMENTO =  pForecastParam(i).COD_ELEMENTO AND
            COD_TIPO_FONTE = pForecastParam(i).COD_TIPO_FONTE AND
            COD_TIPO_COORD = pForecastParam(i).COD_TIPO_COORD AND
            COD_PREV_METEO = pForecastParam(i).COD_PREV_METEO ;
        END LOOP;

        FOR i IN pForecastParam.FIRST .. pForecastParam.LAST LOOP
            -- memorizzare l'addestramento
            INSERT INTO FORECAST_PARAM_INTERV_TRAINING (COD_ELEMENTO,
                                                COD_TIPO_FONTE,
                                                COD_TIPO_COORD,
                                                COD_PREV_METEO,
                                                DT_INIZIO,
                                                DT_FINE
                                        )
                                        VALUES (pForecastParam(i).COD_ELEMENTO,
                                                pForecastParam(i).COD_TIPO_FONTE,
                                                pForecastParam(i).COD_TIPO_COORD,
                                                pForecastParam(i).COD_PREV_METEO,
                                                pForecastParam(i).DT_INIZIO,
                                                pForecastParam(i).DT_FINE);
        END LOOP;
    END IF;
    PKG_Logs.TraceLog('Eseguito SetForecastParamIntervTraining' ,PKG_UtlGlb.gcTrace_VRB);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.SetForecastParamIntervTraining'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;
 END SetForecastParamIntervTraining;

 PROCEDURE ResetForecastParameter (  pRefCurs        OUT PKG_UtlGlb.t_query_cur ,
                                                         pListaCodElemento  IN VARCHAR2,
                                                         pCodPrevMeteo IN INTEGER , -- 1 o 2
                                                         pTipoReset IN INTEGER --1 = Delta 2 = Model 0=Entrambi
                                                          )  AS

vcodTipoCoord VARCHAR2(1);
vTmp            PKG_UtlGlb.t_SplitTbl;
vTab                PKG_GestAnagr.t_DefAnagr;
p_data DATE;

vfound INTEGER;
pParam FORECAST_PARAMETRI%ROWTYPE;




BEGIN

    /*----- Reset dei parametri  -----*/
    if (pCodPrevMeteo = 1) then
        vcodTipoCoord := cTipoCoord_Comuni;
    elsif (pCodPrevMeteo = 2) then
        vcodTipoCoord := cTipoCoord_Punti;
    end if;

    vTmp := PKG_UtlGlb.SplitString(pListaCodElemento,PKG_Mago_utl.cSepCharLst);

    p_data :=sysdate;

    FOR i IN NVL(vTmp.FIRST,0) .. NVL(vTmp.LAST,0) LOOP

        if pTipoReset = cTipoResetAll then   -- TIPO RESET DELTA e MODEL

            vfound := 0;
            begin
           SELECT *     INTO    pParam   FROM FORECAST_PARAMETRI
                                    where COD_ELEMENTO=  vTmp(i) and
                                    sysdate between nvl(DT_INIZIO, to_date('31/12/1000','dd/mm/yyyy'))and  nvl(DT_FINE, to_date('31/12/3000','dd/mm/yyyy')   ) and
                                    COD_TIPO_FONTE = 'S'
                                    AND COD_PREV_METEO = pCodPrevMeteo; -- MAGO-1068
           EXCEPTION
                WHEN NO_DATA_FOUND THEN vfound  :=  -1;
                PKG_Logs.TraceLog(' ***** NON Eseguito ResetForecastParameter RESET ALL MODEL SOLARE ***** - Codice Elemento' || vTmp(i)    ,PKG_UtlGlb.gcTRACE_INF);
            END;

            if vfound <> -1 then
                -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
                PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
                PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.InitRow  (vTab);
                PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pParam.COD_ELEMENTO);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pParam.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pParam.COD_TIPO_COORD);
                PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pParam.COD_PREV_METEO);
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', 0); -- PARAMETRO DA ANNULLARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4',0);-- PARAMETRO DA AZZERARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', pParam.PARAMETRO5);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pParam.PARAMETRO6);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pParam.PARAMETRO7);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pParam.PARAMETRO8);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pParam.PARAMETRO9);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pParam.PARAMETRO10);
                PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', p_data);
                PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pParam.DT_INIZIO_CALCOLO);
                PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pParam.DT_FINE_CALCOLO);
                PKG_GestAnagr.Elabora  (vTab);

                PKG_Logs.TraceLog('Eseguito ResetForecastParameter - RESET ALL SOLARE - Codice Elemento' || pParam.COD_ELEMENTO   ,PKG_UtlGlb.gcTrace_VRB);
            end if;


            print ('TIPO RESET MODEL EOLICO');
            vfound := 0;
            begin
           SELECT *     INTO    pParam   FROM FORECAST_PARAMETRI
                                    where COD_ELEMENTO=  vTmp(i) and
                                    sysdate between nvl(DT_INIZIO, to_date('31/12/1000','dd/mm/yyyy'))and  nvl(DT_FINE, to_date('31/12/3000','dd/mm/yyyy')   ) and
                                    COD_TIPO_FONTE = 'E'
                                    AND COD_PREV_METEO = pCodPrevMeteo; -- MAGO-1068
           EXCEPTION
                WHEN NO_DATA_FOUND THEN vfound  :=  -1;
                PKG_Logs.TraceLog(' ***** NON Eseguito ResetForecastParameter RESET ALL MODEL EOLICO ***** - Codice Elemento' || vTmp(i)   ,PKG_UtlGlb.gcTRACE_INF);
            END;
            if vfound <> -1 then
                -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
                PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
                PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_CUT_IN', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_CUT_OFF', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_MAX_POWER',  PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.InitRow  (vTab);
                PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pParam.COD_ELEMENTO);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pParam.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pParam.COD_TIPO_COORD);
                PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pParam.COD_PREV_METEO);
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', 0); -- PARAMETRO DA ANNULLARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pParam.PARAMETRO6);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pParam.PARAMETRO7);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pParam.PARAMETRO8);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pParam.PARAMETRO9);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pParam.PARAMETRO10);
                PKG_GestAnagr.AddVal   (vTab,'V_CUT_IN', pParam.V_CUT_IN);
                PKG_GestAnagr.AddVal   (vTab,'V_CUT_OFF', pParam.V_CUT_OFF);
                PKG_GestAnagr.AddVal   (vTab,'V_MAX_POWER', pParam.V_MAX_POWER);
                PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', p_data);
                PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pParam.DT_INIZIO_CALCOLO);
                PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pParam.DT_FINE_CALCOLO);
                PKG_GestAnagr.Elabora  (vTab);

                PKG_Logs.TraceLog('Eseguito ResetForecastParameter RESET ALL EOLICO - Codice Elemento' || pParam.COD_ELEMENTO   ,PKG_UtlGlb.gcTrace_VRB);
            end if;




        end if;

        if pTipoReset = cTipoResetDelta then   -- TIPO RESET DELTA

            vfound := 0;
            BEGIN
               SELECT *     INTO    pParam   FROM FORECAST_PARAMETRI
                                                where COD_ELEMENTO=  vTmp(i) and
                                                sysdate between nvl(DT_INIZIO, to_date('31/12/1000','dd/mm/yyyy'))and  nvl(DT_FINE, to_date('31/12/3000','dd/mm/yyyy')   ) and
                                                COD_TIPO_FONTE = 'S'
                                                AND COD_PREV_METEO = pCodPrevMeteo; -- MAGO-1068
            EXCEPTION
                WHEN NO_DATA_FOUND THEN vfound  :=  -1;
                PKG_Logs.TraceLog(' ***** NON Eseguito ResetForecastParameter RESET DELTA SOLARE ***** - Codice Elemento' || vTmp(i)   ,PKG_UtlGlb.gcTRACE_INF);
            END;

             if (vfound <> -1) and (NVL(pParam.PARAMETRO4,-1) != 0) then
                p_data :=sysdate;
                -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
                PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
                PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);

                PKG_GestAnagr.InitRow  (vTab);
                PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pParam.COD_ELEMENTO);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pParam.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pParam.COD_TIPO_COORD);
                PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pParam.COD_PREV_METEO);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', pParam.PARAMETRO1);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', pParam.PARAMETRO2);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', pParam.PARAMETRO3);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4', 0 );            -- PARAMETRO DA AZZERARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', pParam.PARAMETRO5);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pParam.PARAMETRO6);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pParam.PARAMETRO7);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pParam.PARAMETRO8);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pParam.PARAMETRO9);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pParam.PARAMETRO10);
                PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', p_data);
                PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pParam.DT_INIZIO_CALCOLO);
                PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pParam.DT_FINE_CALCOLO);
                PKG_GestAnagr.Elabora  (vTab);
                PKG_Logs.TraceLog('Eseguito ResetForecastParameter RESET DELTA SOLARE - Codice Elemento' || pParam.COD_ELEMENTO   ,PKG_UtlGlb.gcTrace_VRB);
            end if;
        end if ;

        if pTipoReset = cTipoResetModel then   -- TIPO RESET MODEL


            vfound := 0;
            begin
           SELECT *     INTO    pParam   FROM FORECAST_PARAMETRI
                                    where COD_ELEMENTO=  vTmp(i) and
                                    sysdate between nvl(DT_INIZIO, to_date('31/12/1000','dd/mm/yyyy'))and  nvl(DT_FINE, to_date('31/12/3000','dd/mm/yyyy')   ) and
                                    COD_TIPO_FONTE = 'S'
                                    AND COD_PREV_METEO = pCodPrevMeteo; -- MAGO-1068
           EXCEPTION
                WHEN NO_DATA_FOUND THEN vfound  :=  -1;
                PKG_Logs.TraceLog(' ***** NON Eseguito ResetForecastParameter RESET MODEL SOLARE ***** - Codice Elemento' || vTmp(i)   ,PKG_UtlGlb.gcTRACE_INF);
            END;

            if vfound <> -1 then
                -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
                PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
                PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.InitRow  (vTab);
                PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pParam.COD_ELEMENTO);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pParam.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pParam.COD_TIPO_COORD);
                PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pParam.COD_PREV_METEO);
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', 0); -- PARAMETRO DA ANNULLARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4',pParam.PARAMETRO4);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', pParam.PARAMETRO5);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pParam.PARAMETRO6);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pParam.PARAMETRO7);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pParam.PARAMETRO8);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pParam.PARAMETRO9);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pParam.PARAMETRO10);
                PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', p_data);
                PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pParam.DT_INIZIO_CALCOLO);
                PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pParam.DT_FINE_CALCOLO);
                PKG_GestAnagr.Elabora  (vTab);

                PKG_Logs.TraceLog('Eseguito ResetForecastParameter - RESET MODEL SOLARE - Codice Elemento' || pParam.COD_ELEMENTO   ,PKG_UtlGlb.gcTrace_VRB);
            end if;


            print ('TIPO RESET MODEL EOLICO');
            vfound := 0;
            begin
           SELECT *     INTO    pParam   FROM FORECAST_PARAMETRI
                                    where COD_ELEMENTO=  vTmp(i) and
                                    sysdate between nvl(DT_INIZIO, to_date('31/12/1000','dd/mm/yyyy'))and  nvl(DT_FINE, to_date('31/12/3000','dd/mm/yyyy')   ) and
                                    COD_TIPO_FONTE = 'E'
                                    AND COD_PREV_METEO = pCodPrevMeteo; -- MAGO-1068
           EXCEPTION
                WHEN NO_DATA_FOUND THEN vfound  :=  -1;
                PKG_Logs.TraceLog(' ***** NON Eseguito ResetForecastParameter RESET MODEL EOLICO ***** - Codice Elemento' || vTmp(i)   ,PKG_UtlGlb.gcTRACE_INF);
            END;
            if vfound <> -1 then
                -- INIZIO ELABORAZIONE PER FORECAST_PARAMETRI
                PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAMETRI','DT_INIZIO','DT_FINE');
                PKG_GestAnagr.AddCol   (vTab,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_FONTE', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_TIPO_COORD', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'COD_PREV_METEO', PKG_GestAnagr.cColChiave);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO1', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO2', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO3', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO4', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO5', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO6', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO7', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO8', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO9', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'PARAMETRO10', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_CUT_IN', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_CUT_OFF', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'V_MAX_POWER',  PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DATA_ULTIMO_AGG', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_INIZIO_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.AddCol   (vTab,'DT_FINE_CALCOLO', PKG_GestAnagr.cColAttributo);
                PKG_GestAnagr.InitRow  (vTab);
                PKG_GestAnagr.AddVal   (vTab,'COD_ELEMENTO', pParam.COD_ELEMENTO);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_FONTE', pParam.COD_TIPO_FONTE);
                PKG_GestAnagr.AddVal   (vTab,'COD_TIPO_COORD', pParam.COD_TIPO_COORD);
                PKG_GestAnagr.AddVal   (vTab,'COD_PREV_METEO', pParam.COD_PREV_METEO);
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO1', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO2', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO3', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO4', 0); -- PARAMETRO DA ANNULLARE
                --PKG_GestAnagr.AddVal   (vTab,'PARAMETRO5', 0); -- PARAMETRO DA ANNULLARE
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO6', pParam.PARAMETRO6);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO7', pParam.PARAMETRO7);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO8', pParam.PARAMETRO8);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO9', pParam.PARAMETRO9);
                PKG_GestAnagr.AddVal   (vTab,'PARAMETRO10', pParam.PARAMETRO10);
                PKG_GestAnagr.AddVal   (vTab,'V_CUT_IN', pParam.V_CUT_IN);
                PKG_GestAnagr.AddVal   (vTab,'V_CUT_OFF', pParam.V_CUT_OFF);
                PKG_GestAnagr.AddVal   (vTab,'V_MAX_POWER', pParam.V_MAX_POWER);
                PKG_GestAnagr.AddVal   (vTab,'DATA_ULTIMO_AGG', p_data);
                PKG_GestAnagr.AddVal   (vTab,'DT_INIZIO_CALCOLO', pParam.DT_INIZIO_CALCOLO);
                PKG_GestAnagr.AddVal   (vTab,'DT_FINE_CALCOLO', pParam.DT_FINE_CALCOLO);
                PKG_GestAnagr.Elabora  (vTab);

                PKG_Logs.TraceLog('Eseguito ResetForecastParameter RESET MODEL EOLICO - Codice Elemento' || pParam.COD_ELEMENTO   ,PKG_UtlGlb.gcTrace_VRB);
            end if;

        end if;


    END LOOP;

   OPEN pRefCurs FOR 'select 0 RESULT from dual'  ;
    PKG_Logs.TraceLog('Eseguito ResetForecastParameter  '   ,PKG_UtlGlb.gcTrace_VRB);

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Meteo.ResetForecastParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR 'select -1 RESULT from dual' ;       -- -1 ERRORE
         RAISE;
END ResetForecastParameter;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetAnagElementForecastParam(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestElem      IN VARCHAR2, pDateElab DATE  ) AS

/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con il dettaglio dei codici gestionali ricevuti
-----------------------------------------------------------------------------------------------------------*/


    vSql      VARCHAR2(9000) :=
                    'SELECT COD_GEST_ELEMENTO,            ' ||
                    '      RIF_ELEMENTO POD ,    ' ||
                    '        NOME_ELEMENTO DESCRIZIONE,'||
                    '        DESCRIZIONE TIPO_PRODUTTORE,'||
                    '        DATA_ATTIVAZIONE DATA_AGGIORNAMENTO,          '||
                    '        ( select  nvl(tipi_rete.DESCRIZIONE, ''-'')  from tipi_elemento inner join tipi_rete  using (cod_tipo_rete) where cod_tipo_ELEMENTO  = LOTTO.COD_TIPO_ELEMENTO ) RETE , ' ||
                   '(select round(ED.COORDINATA_Y,4)  from ELEMENTI_DEF ED where ED.cod_elemento =  (PKG_ELEMENTI.GetElementoPadre ( LOTTO.COD_ELEMENTO,''SCS'' ,  LOTTO.DATA_ATTIVAZIONE, 1,2, 0 ) ) and  LOTTO.DATA_ATTIVAZIONE between  ED.DATA_ATTIVAZIONE and ED.DATA_DISATTIVAZIONE) LAT ,     '||
                   '(select round(ED.COORDINATA_X,4)  from ELEMENTI_DEF ED where ED.cod_elemento =  (PKG_ELEMENTI.GetElementoPadre ( LOTTO.COD_ELEMENTO,''SCS'' ,  LOTTO.DATA_ATTIVAZIONE, 1,2, 0 ) ) and  LOTTO.DATA_ATTIVAZIONE between  ED.DATA_ATTIVAZIONE and ED.DATA_DISATTIVAZIONE) LON ,     '||
                    '        ALTITUDINE        ,          '||
                    '       0 ALT_TERRENO  ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''S''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_SOLARE ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''E''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_EOLICA ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''I|O|M''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_IDRO ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''F|G|B|T|D|R''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_TERMICA ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''R''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_RINNO ,'||
                    '       (SELECT nvl(VALORE,0) FROM TABLE(PKG_MISURE.GetMisureTabMis(COD_ELEMENTO,''PI'',PKG_MISURE.GetIdTipoFonti(''C''),PKG_MISURE.GetIdTipoReti(''B|M''),PKG_MISURE.GETIDTIPOCLIENTI(''A|B|X|Z'')  ,  :pdate -1 ,:pdate,1, 2,0,1, :pdate,1,0,1))) PI_CONV'||
                    '    FROM (SELECT COD_ELEMENTO,     '||
                    '                 COD_GEST_ELEMENTO,'||
                    '                 COD_TIPO_CLIENTE, '||
                    '                 RIF_ELEMENTO,     '||
                    '                 NOME_ELEMENTO,    '||
                    '                 COD_TIPO_ELEMENTO,'||
                    '                 CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,'||
                    '                 COORDINATA_X,                '||
                    '                 COORDINATA_Y,                '||
                    '                 ALTITUDINE,                  '||
                    '                 DATA_ATTIVAZIONE             '||
                    '            FROM (SELECT COD_ELEMENTO,        '||
                    '                         COD_GEST_ELEMENTO,   '||
                    '                         RIF_ELEMENTO,        '||
                    '                         COD_TIPO_CLIENTE,    '||
                    '                         NOME_ELEMENTO,       '||
                    '                         DATA_ATTIVAZIONE ,   '||
                    '                         E.COD_TIPO_ELEMENTO, '||
                    '                         D.COORDINATA_X,      '||
                    '                         D.COORDINATA_Y,      '||
                    '                         D.ALTITUDINE,        '||
                    '                         ROW_NUMBER ()        '||
                    '                         OVER (PARTITION BY COD_ELEMENTO '||
                    '                               ORDER BY COD_ELEMENTO, DATA_ATTIVAZIONE DESC)'||
                    '                            ORD   '||
                    '                    FROM ELEMENTI E                                            '||
                    '                         INNER JOIN ELEMENTI_DEF D USING (COD_ELEMENTO)        '||
                    '                   WHERE COD_GEST_ELEMENTO =  '''||pCodGestElem||''')                 '||
                    '                 INNER JOIN TIPI_ELEMENTO USING (COD_TIPO_ELEMENTO)            '||
                    '           WHERE ORD = 1)  LOTTO                                                    '||
                    '         INNER JOIN TIPI_CLIENTE TC USING (COD_TIPO_CLIENTE)                   '||
                    'ORDER BY COD_GEST_ELEMENTO                                                     ';

BEGIN


--
--    IF pStatoRete = Pkg_Mago_utl.gcStatoNormale THEN
--        vSql := REPLACE(vSql,'#STATO#','SN');
--    ELSE
--        vSql := REPLACE(vSql,'#STATO#','SA');
--    END IF;
--
--    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
--       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
--    ELSE
--       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );
--    END IF;

    OPEN pRefCurs FOR vSql USING pDateElab,pDateElab,pDateElab,
    pDateElab,pDateElab,pDateElab,
    pDateElab,pDateElab,pDateElab,
    pDateElab,pDateElab,pDateElab,
    pDateElab,pDateElab,pDateElab,     
    pDateElab,pDateElab,pDateElab;


    PKG_Logs.TraceLog('Eseguito GetAnagElementForecastParam  '   ,PKG_UtlGlb.gcTrace_VRB);
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetAnagElementForecastParam'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetAnagElementForecastParam;


PROCEDURE GetDettaglioForecastParam(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pCodGestElem      IN VARCHAR2  ,    -- CODICE GESTIONALE
                               ptipoFonte           IN VARCHAR2  ,       -- TIPO_FONTE
                               pcod_prev_meteo IN VARCHAR2,
                               pcod_tipo_coord IN VARCHAR2
                               ) AS

/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con il dettaglio parametri
-----------------------------------------------------------------------------------------------------------*/
    pDataUltimaEse DATE ;
    pForecastDal    DATE;
    pForecastAl    DATE;


    vSql      VARCHAR2(3000) :=
                    'select         COD_GEST_ELEMENTO,        '||
                    ':pDataUltimaEse ULTIMA_ESEC,                 '||
                    '(select VALUE from METEO_JOB_STATIC_CONFIG where KEY = ''MPS.load.forecast.install.forward.hour'')  ORE_PROFONDITA,                             '||
                    ':pForecastDal PREVISIONE_DAL, '||
                    ':pForecastAl PREVISIONE_AL,'||
                    '(select VALUE from METEO_JOB_STATIC_CONFIG where KEy = ''MPS.estimation.measure.days.number'')  INTERVALLO_MISURE,                          '||
                    '(select VALUE from METEO_JOB_STATIC_CONFIG where KEy = ''MPS.estimation.measure.interval.days'')   GIORNI_ADDR,                               '||
                     'CASE  WHEN  ( (F.DATA_ULTIMO_AGG+(select VALUE from METEO_JOB_STATIC_CONFIG where KEy = ''MPS.estimation.measure.interval.days'')) <= sysdate ) '||
                     '   THEN sysdate  ELSE F.DATA_ULTIMO_AGG+(select VALUE from METEO_JOB_STATIC_CONFIG where KEy = ''MPS.estimation.measure.interval.days'') '||
                    'END PROSSIMO_ADDR,       '||
                    'F.DATA_ULTIMO_AGG  ULTIMO_ADDR,  '||
                    'PARAMETRO1  ,                                  '||
                    'PARAMETRO2  ,                                  '||
                    'PARAMETRO3  ,                                  '||
                    'PARAMETRO4  ,                                  '||
                    'PARAMETRO5  ,                                  '||
                    'PARAMETRO6  ,                                  '||
                    'PARAMETRO7  ,                                  '||
                    'PARAMETRO8  ,                                  '||
                    'PARAMETRO9  ,                                  '||
                    'PARAMETRO10 ,                                  '||
                    'V_CUT_IN    ,                                  '||
                    'V_CUT_OFF   ,                                  '||
                    'V_MAX_POWER,                                    '||
                    '(select J_MAX     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) J_MAX , '||
                    '(select ALFA_MAX     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) ALFA_MAX , '||
                    '(select ALFA_MIN     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) ALFA_MIN     '||
                    'from FORECAST_PARAMETRI F                      '||
                    'inner join ELEMENTI E using ( COD_ELEMENTO)    '||
                    'where COD_TIPO_FONTE = :ptipoFonte '||
                    'AND COD_PREV_METEO = :pcod_prev_meteo '||
                    'AND COD_TIPO_COORD = :pcod_tipo_coord '||
                    'AND Sysdate BETWEEN F.DT_INIZIO AND F.DT_FINE  '||
                    'AND E.COD_GEST_ELEMENTO = :pCodGestElem  ';


       vSql2      VARCHAR2(3000) :=   'select        0 COD_GEST_ELEMENTO,        '||
                    'NULL ULTIMA_ESEC,                 '||
                    'NULL   ORE_PROFONDITA,                             '||
                    'NULL PREVISIONE_DAL, '||
                    'NULL PREVISIONE_AL,'||
                    'NULL   INTERVALLO_MISURE,                          '||
                    'NULL   GIORNI_ADDR,                               '||
                    'NULL  PROSSIMO_ADDR,       '||
                    'NULL  ULTIMO_ADDR,  '||
                    'NULL PARAMETRO1  ,                                  '||
                    'NULL PARAMETRO2  ,                                  '||
                    'NULL PARAMETRO3  ,                                  '||
                    'NULL PARAMETRO4  ,                                  '||
                    'NULL PARAMETRO5  ,                                  '||
                    'NULL PARAMETRO6  ,                                  '||
                    'NULL PARAMETRO7  ,                                  '||
                    'NULL PARAMETRO8  ,                                  '||
                    'NULL PARAMETRO9  ,                                  '||
                    'NULL PARAMETRO10 ,                                  '||
                    'NULL V_CUT_IN    ,                                  '||
                    'NULL V_CUT_OFF   ,                                  '||
                    'NULL V_MAX_POWER,                                    '||
                    '(select J_MAX     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) J_MAX , '||
                    '(select ALFA_MAX     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) ALFA_MAX , '||
                    '(select ALFA_MIN     from FORECAST_PARAM_GLOBAL where sysdate BETWEEN DT_INIZIO AND DT_FINE) ALFA_MIN     '||
                    'from dual ' ;

vCOD_GEST_ELEMENTO VARCHAR2(200);
vULTIMA_ESEC          DATE ;
vORE_PROFONDITA     INTEGER;
vPREVISIONE_DAL     DATE;
vPREVISIONE_AL      DATE;
vINTERVALLO_MISURE     INTEGER;
vGIORNI_ADDR        INTEGER;
vPROSSIMO_ADDR         DATE;
vULTIMO_ADDR        DATE;
vPARAMETRO1  NUMBER;
vPARAMETRO2  NUMBER;
vPARAMETRO3  NUMBER;
vPARAMETRO4  NUMBER;
vPARAMETRO5  NUMBER;
vPARAMETRO6  NUMBER;
vPARAMETRO7  NUMBER;
vPARAMETRO8  NUMBER;
vPARAMETRO9  NUMBER;
vPARAMETRO10 NUMBER;
vV_CUT_IN    NUMBER;
vV_CUT_OFF   NUMBER;
vV_MAX_POWER NUMBER;
vJ_MAX NUMBER;
vALFA_MAX     NUMBER;
vALFA_MIN   NUMBER;

vNum INTEGER := 0;

BEGIN

    BEGIN
    select  S.DATE_START , S.DATE_FROM, S.DATE_TO
    into  pDataUltimaEse,    pForecastDal ,    pForecastAl
    from SESSION_STATISTICS  S
    where S.COD_TIPO_MISURA= 'PMP' and S.SERVICE_ID = 'MDS-service'   and rownum= 1  order by cod_sessione desc ;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
            pDataUltimaEse := sysdate ;
            pForecastDal   := sysdate;
            pForecastAl    := sysdate;
    END;

    --DBMS_OUTPUT.PUT_LINE (vSql);

    OPEN pRefCurs FOR vSql
    USING pDataUltimaEse, pForecastDal, pForecastAl, ptipoFonte,pcod_prev_meteo, pcod_tipo_coord, pCodGestElem;


    LOOP
   FETCH pRefCurs   INTO vCOD_GEST_ELEMENTO, vULTIMA_ESEC
                                    ,vORE_PROFONDITA
                                    ,vPREVISIONE_DAL
                                    ,vPREVISIONE_AL
                                    ,vINTERVALLO_MISURE
                                    ,vGIORNI_ADDR
                                    ,vPROSSIMO_ADDR
                                    ,vULTIMO_ADDR
                                    ,vPARAMETRO1
                                    ,vPARAMETRO2
                                    ,vPARAMETRO3
                                    ,vPARAMETRO4
                                    ,vPARAMETRO5
                                    ,vPARAMETRO6
                                    ,vPARAMETRO7
                                    ,vPARAMETRO8
                                    ,vPARAMETRO9
                                    ,vPARAMETRO10
                                    ,vV_CUT_IN
                                    ,vV_CUT_OFF
                                    ,vV_MAX_POWER
                                    ,vJ_MAX
                                    ,vALFA_MAX
                                    ,vALFA_MIN   ;
    EXIT WHEN pRefCurs%NOTFOUND;
     vNum := vNum + 1;
  END LOOP;

     if vNum = 1 then

        PKG_Logs.TraceLog (' ---------------------------PARAMETER FOUND ----------------------------'  ,PKG_UtlGlb.gcTRACE_INF);
        close pRefCurs;
        OPEN pRefCurs FOR vSql
        USING pDataUltimaEse, pForecastDal, pForecastAl, ptipoFonte,pcod_prev_meteo, pcod_tipo_coord, pCodGestElem;
    else

        PKG_Logs.TraceLog (' ---------------------------SOLO GLOBAL --------------------------------------'  ,PKG_UtlGlb.gcTRACE_INF);
        CLOSE   pRefCurs;
        --DBMS_OUTPUT.PUT_LINE (vSql2);
        OPEN pRefCurs FOR vSql2;
     end if;


    PKG_Logs.TraceLog('Eseguito GetDettaglioForecastParam  '   ,PKG_UtlGlb.gcTRACE_INF);
 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetDettaglioForecastParam'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetDettaglioForecastParam;


PROCEDURE UpdateAlgorithmParameter(
                        pRefCurs        OUT PKG_UtlGlb.t_query_cur ,
                        pgg_misure    IN NUMBER,    -- Utilizza misure [giorni]
                        pgg_ogni    IN NUMBER,    -- Addestramento ogni [giorni]
                        pjmax        IN NUMBER,    -- parametro J MAX
                        palfamax    IN NUMBER,    -- parametro ALFA MAX
                        palfamin    IN NUMBER     -- parametro ALFA MIN
                       )  AS


vTab                PKG_GestAnagr.t_DefAnagr;
p_data DATE;

pJ_maxNow           NUMBER;
palfa_maxNow       NUMBER;
palfa_minNow        NUMBER;



BEGIN

    p_data := sysdate;
    --preturn := 0;   -- 0 nessun errore


    --   pgg_misure e pgg_ogni IN METEO_JOB_STATIC_CONFIG
    UPDATE METEO_JOB_STATIC_CONFIG  SET VALUE =  pgg_misure  WHERE KEY =  'MPS.estimation.measure.days.number';       --  Utilizza misure [giorni]
    UPDATE METEO_JOB_STATIC_CONFIG  SET VALUE =  pgg_ogni  WHERE KEY =  'MPS.estimation.measure.interval.days';           -- Addestramento ogni [giorni]

    -- recupero l'ultima valori per vedere se i valori sono cambiati

       select
      max(J_max) KEEP (DENSE_RANK LAST ORDER BY ID_PROGRESS) J_max,
      max(alfa_max) KEEP (DENSE_RANK LAST ORDER BY ID_PROGRESS) alfa_max,
      max(alfa_min) KEEP (DENSE_RANK LAST ORDER BY ID_PROGRESS) alfa_min
      into
      pJ_maxNow,
      palfa_maxNow,
      palfa_minNow
       from FORECAST_PARAM_GLOBAL;


    -- se almeno un valore ¿ cambiato storicizzo

    IF (pJ_maxNow<>pjmax ) or (palfa_maxNow<>palfamax) or (palfa_minNow<>palfamin )  THEN

        -- INIZIO ELABORAZIONE PER FORECAST_PARAM_GLOBAL per pjmax - palfamax - palfamin
        PKG_GestAnagr.InitTab  (vTab,p_data,USER,'FORECAST_PARAM_GLOBAL','DT_INIZIO','DT_FINE',TRUE);
        PKG_GestAnagr.AddCol   (vTab,'ID_PROGRESS', PKG_GestAnagr.cColChiave);
        PKG_GestAnagr.AddCol   (vTab,'J_MAX', PKG_GestAnagr.cColAttributo);
        PKG_GestAnagr.AddCol   (vTab,'ALFA_MAX', PKG_GestAnagr.cColAttributo);
        PKG_GestAnagr.AddCol   (vTab,'ALFA_MIN', PKG_GestAnagr.cColAttributo);
        PKG_GestAnagr.InitRow  (vTab);
        PKG_GestAnagr.AddVal   (vTab,'J_MAX', pjmax);
        PKG_GestAnagr.AddVal   (vTab,'ALFA_MAX', palfamax);
        PKG_GestAnagr.AddVal   (vTab,'ALFA_MIN', palfamin);
        PKG_GestAnagr.Elabora  (vTab);

    END IF;


     OPEN pRefCurs FOR 'select 0 RESULT from dual'  ;
    PKG_Logs.TraceLog('Eseguito UpdateAlgorithmParameter  '   ,PKG_UtlGlb.gcTRACE_INF);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.UpdateAlgorithmParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OPEN pRefCurs FOR 'select -1 RESULT from dual' ;       -- -1 ERRORE
         RAISE;
END UpdateAlgorithmParameter;

PROCEDURE GetAlgorithmParameter(
                        pRefCurs        OUT PKG_UtlGlb.t_query_cur ,
                        pValidDate               IN DATE
                        ) AS

    pgg_misure     NUMBER;    -- Utilizza misure [giorni]
    pgg_ogni     NUMBER;    -- Addestramento ogni [giorni]
    pjmax         NUMBER;    -- parametro J MAX
    palfamax     NUMBER;   -- parametro ALFA MAX
    palfamin     NUMBER ;

    vSql      VARCHAR2(2000);

BEGIN


    BEGIN
        select VALUE into pgg_misure from METEO_JOB_STATIC_CONFIG where KEY = 'MPS.estimation.measure.days.number';
    EXCEPTION
            WHEN NO_DATA_FOUND THEN pgg_misure := -9999;
    END;

    BEGIN
        select VALUE into pgg_ogni from METEO_JOB_STATIC_CONFIG where KEY = 'MPS.estimation.measure.interval.days';
    EXCEPTION
            WHEN NO_DATA_FOUND THEN pgg_ogni := -9999;
    END;



    -- pgg_misure     Utilizza misure [giorni]
    -- pgg_ogni     Addestramento ogni [giorni]
    -- pjmax         parametro J MAX
    -- palfamax    parametro ALFA MAX
    -- palfamin    parametro ALFA MIN


     vSql := 'select 1 ID,  ' || pgg_misure  ||' GGMISURE ,' ||
     pgg_ogni ||' GGOGNI ,' ||
     '(select J_MAX   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  JMAX , ' ||
     '(select ALFA_MAX   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  ALFAMAX , ' ||
     '(select ALFA_MIN   from FORECAST_PARAM_GLOBAL where :pValidDate BETWEEN DT_INIZIO AND DT_FINE)  ALFAMIN  ' ||
     ' from dual'  ;


    --DBMS_OUTPUT.PUT_LINE (vSql);

    OPEN pRefCurs FOR vSql
    using pValidDate,pValidDate,pValidDate ;

    PKG_Logs.TraceLog('Eseguito GetAlgorithmParameter  '   ,PKG_UtlGlb.gcTRACE_INF);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.GetAlgorithmParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetAlgorithmParameter ;




PROCEDURE Get_ListParameterChange (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE,
                        pcod_tipo_fonte IN VARCHAR2,
                        pcod_prec_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2
) AS

 vSql               VARCHAR2(2000) ;
 vSqlGlobal      VARCHAR2(10000) ;
 vSqlResult      VARCHAR2(2000) ;




BEGIN


--      *******************
--      Mancano le entry per  inizio e fine periodo DA FARE
--      *******************


  DELETE FROM GTTD_PARAMETER_CHANGE where COD_GEST_ELEMENTO = pcodGestElem;


  vSql := ' INSERT INTO GTTD_PARAMETER_CHANGE ( ' ||
 ' COD_GEST_ELEMENTO      ,   COD_TIPO_FONTE    ,  DATA_AGG    , '  ||
 '  PARAMETRO1         ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
 ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
 ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER   ,  J_MAX,  ALFA_MIN ,  ALFA_MAX  ) ' ||
'   select COD_GEST_ELEMENTO , cod_tipo_fonte , DATA_ULTIMO_AGG,  parametro1 , parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9, parametro10, V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER , ' ||
     ' (select F.J_MAX from FORECAST_PARAM_GLOBAL F  where DATA_ULTIMO_AGG between F.DT_INIZIO and F.DT_FINE) JMAX , ' ||
 ' (select F.ALFA_MIN  from FORECAST_PARAM_GLOBAL F  where DATA_ULTIMO_AGG between F.DT_INIZIO and F.DT_FINE) ALFA_MIN, ' ||
 ' (select F.ALFA_MAX from FORECAST_PARAM_GLOBAL F  where DATA_ULTIMO_AGG between F.DT_INIZIO and F.DT_FINE) ALFA_MAX   ' ||
'    from' ||
'    (' ||
'           SELECT count(cod_elemento) conta,  max(E.COD_GEST_ELEMENTO) COD_GEST_ELEMENTO ,  max(data_ultimo_agg) data_ultimo_agg ,  cod_elemento , cod_tipo_fonte , parametro1, parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9, parametro10,  V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER   from' ||
'            FORECAST_PARAMETRI ' ||
'            inner join ELEMENTI E using ( COD_ELEMENTO) ' ||
'            where ' ||
'            E.COD_GEST_ELEMENTO = :pcodGestElem ' ||
'                AND DATA_ULTIMO_AGG between :pdata_da  and :pdata_a '   ||
'            group by cod_elemento , cod_tipo_fonte , parametro1, parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9, parametro10, V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER ' ||
'    ) ' ;



    --DBMS_OUTPUT.PUT_LINE (vSql);
    EXECUTE IMMEDIATE vSql
    USING pcodGestElem,pData_da, pData_a ;


--      *******************
--      entry per inizio periodo
--      *******************

      vSql := ' INSERT INTO GTTD_PARAMETER_CHANGE ( ' ||
 ' COD_GEST_ELEMENTO      ,   COD_TIPO_FONTE    ,  DATA_AGG    , '  ||
 ' PARAMETRO1         ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
 ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
 ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER   ,  J_MAX,  ALFA_MIN ,  ALFA_MAX  ) ' ||
 ' select   E.COD_GEST_ELEMENTO       , ' ||
 ' COD_TIPO_FONTE     , ' ||
 ' :pdata_da    , '||
 ' PARAMETRO1         ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
 ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
 ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER        , ' ||
 ' (select F.J_MAX from FORECAST_PARAM_GLOBAL F  where :pdata_da between F.DT_INIZIO and F.DT_FINE) JMAX , ' ||
 ' (select F.ALFA_MIN  from FORECAST_PARAM_GLOBAL F  where :pdata_da between F.DT_INIZIO and F.DT_FINE) ALFA_MIN, ' ||
 ' (select F.ALFA_MAX from FORECAST_PARAM_GLOBAL F  where :pdata_da between F.DT_INIZIO and F.DT_FINE) ALFA_MAX   ' ||
 ' from FORECAST_PARAMETRI FP ' ||
 ' inner join ELEMENTI E using ( COD_ELEMENTO) ' ||
 ' where  ' ||
 ' (:pdata_da between DT_INIZIO and DT_FINE or :pdata_a between DT_INIZIO and DT_FINE) ' ||
  ' AND E.COD_GEST_ELEMENTO = :pcodGestElem ' ||
 '  and rownum = 1 order by data_ultimo_agg desc' ;

    --DBMS_OUTPUT.PUT_LINE (vSql);
    EXECUTE IMMEDIATE vSql
    USING pData_da,pData_da,pData_da, pData_da, pData_da , pData_a  ,pcodGestElem ;


--      *******************
--      entry per fine periodo
--      *******************

      vSql := ' INSERT INTO GTTD_PARAMETER_CHANGE ( ' ||
 ' COD_GEST_ELEMENTO      ,   COD_TIPO_FONTE    ,  DATA_AGG    , '  ||
 ' PARAMETRO1         ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
 ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
 ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER   ,  J_MAX,  ALFA_MIN ,  ALFA_MAX  ) ' ||
 ' select   E.COD_GEST_ELEMENTO       , ' ||
 ' COD_TIPO_FONTE     , ' ||
 ' :pdata_a    , '||
 ' PARAMETRO1         ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
 ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
 ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER        , ' ||
 ' (select F.J_MAX from FORECAST_PARAM_GLOBAL F  where :pdata_a between F.DT_INIZIO and F.DT_FINE) JMAX , ' ||
 ' (select F.ALFA_MIN  from FORECAST_PARAM_GLOBAL F  where :pdata_a between F.DT_INIZIO and F.DT_FINE) ALFA_MIN, ' ||
 ' (select F.ALFA_MAX from FORECAST_PARAM_GLOBAL F  where :pdata_a between F.DT_INIZIO and F.DT_FINE) ALFA_MAX   ' ||
 ' from FORECAST_PARAMETRI FP ' ||
 ' inner join ELEMENTI E using ( COD_ELEMENTO) ' ||
 ' where  ' ||
 --' (:pdata_da between DT_INIZIO and DT_FINE or :pdata_a between DT_INIZIO and DT_FINE) ' ||
 ' ( :pdata_a between DT_INIZIO and DT_FINE) ' ||
  ' AND E.COD_GEST_ELEMENTO = :pcodGestElem ' ||
  '  and rownum = 1 order by data_ultimo_agg desc' ;

    --DBMS_OUTPUT.PUT_LINE (vSql);
    EXECUTE IMMEDIATE vSql
    --USING pData_a,pData_a,pData_a,pData_a, pData_da, pData_a,pcodGestElem ;
    USING pData_a,pData_a,pData_a,pData_a, pData_a,pcodGestElem ;

    -- CAMBIAMENTI DI PARAMETRI GLOBALI
      vSqlGlobal := ' INSERT INTO GTTD_PARAMETER_CHANGE ( ' ||
     ' COD_GEST_ELEMENTO      ,   COD_TIPO_FONTE    ,  DATA_AGG    , '  ||
     ' PARAMETRO1        ,  PARAMETRO2         ,  PARAMETRO3         ,  PARAMETRO4         ,  PARAMETRO5         , ' ||
     ' PARAMETRO6         ,  PARAMETRO7         ,  PARAMETRO8         ,  PARAMETRO9         ,  PARAMETRO10        , ' ||
     ' V_CUT_IN           ,  V_CUT_OFF          ,  V_MAX_POWER   ,  J_MAX,  ALFA_MIN ,  ALFA_MAX  ) ' ||
     ' select  '''  || pcodGestElem || '''' ||
     ' , ''' || pcod_tipo_fonte || '''' ||
     ' , DT_INIZIO    , '||
     --'  null PARAMETRO1     , ' ||
--     '    null PARAMETRO2         , null PARAMETRO3         , null  PARAMETRO4         ,null  PARAMETRO5         , ' ||
--     ' null PARAMETRO6         ,  null PARAMETRO7         , null  PARAMETRO8         , null PARAMETRO9         , null  PARAMETRO10        , ' ||
    ' (select  PARAMETRO1  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO1 ,' ||
    ' (select  PARAMETRO2  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO2 ,' ||
    ' (select  PARAMETRO3  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO3 ,' ||
    ' (select  PARAMETRO4  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO4 ,' ||
    ' (select  PARAMETRO5  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO5 ,' ||
    ' (select  PARAMETRO6  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO6 ,' ||
    ' (select  PARAMETRO7  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO7 ,' ||
    ' (select  PARAMETRO8  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO8 ,' ||
    ' (select  PARAMETRO9  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO9 ,' ||
    ' (select  PARAMETRO10  from FORECAST_PARAMETRI FP  inner join ELEMENTI E using ( COD_ELEMENTO)  where  ( FPG.DT_INIZIO between FP.DT_INIZIO and FP.DT_FINE)  AND  E.COD_GEST_ELEMENTO =  '''  || pcodGestElem || ''') PARAMETRO10 ,'||
     ' null V_CUT_IN           , null V_CUT_OFF          ,  null V_MAX_POWER        , ' ||
     '  J_MAX , ' ||
     '  ALFA_MIN, ' ||
     '  ALFA_MAX   ' ||
     ' from FORECAST_PARAM_GLOBAL FPG  ' ||
     ' where  ' ||
     ' DT_INIZIO between :pdata_da and :pdata_a or DT_FINE between :pdata_da and :pdata_a  '  ||
     ' AND to_char(DT_INIZIO ,''dd/mm/yyyy'')   <> ''01/01/2000'' ' ;   -- data tappo iniziale per parametri globali


        --DBMS_OUTPUT.PUT_LINE (vSqlGlobal);
        EXECUTE IMMEDIATE vSqlGlobal
    USING pData_da, pData_a , pData_da, pData_a;


    vSqlResult := 'SELECT COD_GEST_ELEMENTO ,  DATA_AGG           ,
                                                PARAMETRO1           ,  PARAMETRO2           ,  PARAMETRO3           ,  PARAMETRO4           ,  PARAMETRO5           ,
                                                PARAMETRO6           ,  PARAMETRO7           ,  PARAMETRO8           ,  PARAMETRO9           ,  PARAMETRO10          ,
                                                V_CUT_IN             ,  V_CUT_OFF            ,  V_MAX_POWER          ,  J_MAX                ,  ALFA_MAX             ,  ALFA_MIN
                                                FROM GTTD_PARAMETER_CHANGE where COD_GEST_ELEMENTO =   :pcodGestElem  order by DATA_AGG asc';


   -- DBMS_OUTPUT.PUT_LINE (vSqlResult);
    OPEN pRefCurs FOR vSqlResult USING  pcodGestElem ;




    PKG_Logs.TraceLog('Eseguito Get_ListParameterChange  '   ,PKG_UtlGlb.gcTRACE_INF);


 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.Get_ListParameterChange'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;


END Get_ListParameterChange;

PROCEDURE Get_IntervalloAddestraParam (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE,
                        pcod_tipo_fonte IN VARCHAR2  ,
                        pcod_prev_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2

) AS

  vSql      VARCHAR2(4000) :=
            ' SELECT ' ||
            '  DT_INIZIO DATA_ADDE_DA,  ' ||
            '  DT_FINE DATA_ADDE_A ' ||
            '  from  ' ||
            '  FORECAST_PARAM_INTERV_TRAINING ' ||
            '  inner join ELEMENTI E using ( COD_ELEMENTO) ' ||
            '  where  ' ||
            '  DT_FINE >=:pdata_da ' ||
            '  and DT_inizio <=:pdata_a ' ||
            '  and E.COD_GEST_ELEMENTO = :pcodGestElem ' ||
            '  and  cod_tipo_fonte = :pcod_tipo_fonte ' ||
            '  and  COD_TIPO_COORD = :pcod_tipo_coord ' ||
            '  and  COD_PREV_METEO = :pcod_prev_meteo ' ||
            '  order by DT_INIZIO ' ;



BEGIN


    --DBMS_OUTPUT.PUT_LINE (vSql);

    OPEN pRefCurs FOR vSql USING NVL(pdata_da,SYSDATE) , NVL(pdata_a,SYSDATE), pcodGestElem , pcod_tipo_fonte,pcod_tipo_coord , pcod_prev_meteo  ;

    PKG_Logs.TraceLog('Eseguito Get_IntervalloAddestraParam  '   ,PKG_UtlGlb.gcTRACE_INF);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.Get_IntervalloAddestraParam'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END Get_IntervalloAddestraParam;



PROCEDURE Get_InfoTableParameter (
                        pRefCurs OUT PKG_UtlGlb.t_query_cur,
                        pcodGestElem IN VARCHAR2,
                        pcod_tipo_fonte IN VARCHAR2,
                        pcod_prec_meteo IN VARCHAR2,
                        pcod_tipo_coord IN VARCHAR2,
                        pdata_da IN DATE,
                        pdata_a IN DATE
) AS

pNUM_RUN_ADDR NUMBER;
pNUM_MOD_PARAM NUMBER;
pNUM_MOD_GLOBAL NUMBER;

vSql      VARCHAR2(4000);

BEGIN

    BEGIN
        SELECT   count(*) into pNUM_RUN_ADDR  from
        FORECAST_PARAMETRI
        inner join ELEMENTI E using ( COD_ELEMENTO)
        where
        E.COD_GEST_ELEMENTO =  pcodGestElem;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pNUM_RUN_ADDR := 0;

    END;

    begin
        select  decode( count(*) , 0, 0, count(*)) INTO pNUM_MOD_PARAM
        from
        (
            select  cod_elemento , cod_tipo_fonte , parametro1, parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9,   rownum num_riga
            from
            (
                   SELECT count(cod_elemento) conta,  max(data_ultimo_agg) data_ultimo_agg ,  cod_elemento , cod_tipo_fonte , parametro1, parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9   from
                    FORECAST_PARAMETRI
                    inner join ELEMENTI E using ( COD_ELEMENTO)
                    where
                    E.COD_GEST_ELEMENTO =  pcodGestElem
                    AND DATA_ULTIMO_AGG between pdata_da  and pdata_a
                    group by cod_elemento , cod_tipo_fonte , parametro1, parametro2, parametro3 , parametro4, parametro5, parametro6, parametro7, parametro8, parametro9
            ) tabella
        );
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pNUM_MOD_PARAM := 0;

    END;

    begin
        select count(*) into pNUM_MOD_GLOBAL from FORECAST_PARAM_GLOBAL where DT_INIZIO between pdata_da  and pdata_a ;
    EXCEPTION
            WHEN NO_DATA_FOUND THEN
                pNUM_MOD_GLOBAL := 0;

    END;

    pNUM_MOD_PARAM := pNUM_MOD_PARAM + pNUM_MOD_GLOBAL ;

    vSql :=  'select 1 ID ,' || pNUM_RUN_ADDR || ' NUM_RUN_ADDR,'|| pNUM_MOD_PARAM ||' NUM_MOD_PARAM from dual';

    OPEN pRefCurs FOR vSql;

    PKG_Logs.TraceLog('Eseguito Get_InfoTableParameter  '   ,PKG_UtlGlb.gcTRACE_INF);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_METEO.Get_InfoTableParameter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END Get_InfoTableParameter;





-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago_utl.gcJobClassMET,
                          pFunzione    => 'PKG_METEO',
                          pStoreOnFile => FALSE);


    PKG_ELEMENTI.GetDefaultCO(gDefaultElem, gDefaultGest, gDefaultNome);


END PKG_METEO;
/
show errors;