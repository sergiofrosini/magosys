SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 13
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2
PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_PROFILI.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 13
PROMPT =======================================================================================

SPOOL OFF
