SPOOL &spool_all APPEND 

PROMPT =======================================================================================
PROMPT INIZIO MAGO 2.4 rev 6a
PROMPT =======================================================================================

WHENEVER SQLERROR EXIT SQL.SQLCODE
WHENEVER OSERROR  EXIT 3

conn mago/mago&tns_arcdb2



PROMPT ---------------------------------------------------------------------------------------
PROMPT Package Bodies
PROMPT ---------------------------------------------------------------------------------------
@./PackageBodies/PKG_MISURE.sql


disconn

PROMPT =======================================================================================
PROMPT FINE   MAGO 2.4 rev 6a
PROMPT =======================================================================================

SPOOL OFF
