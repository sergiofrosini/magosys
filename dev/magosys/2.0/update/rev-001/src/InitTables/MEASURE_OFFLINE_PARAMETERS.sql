PROMPT INIT TABLE MEASURE_OFFLINE_PARAMETERS;
 
MERGE INTO MEASURE_OFFLINE_PARAMETERS A USING
 (SELECT 'STWebRegistry-WS.measure.extraction.max_gest_code_items' as key,
         1000 AS value
    FROM DUAL
   UNION ALL
   SELECT 'STWebRegistry-WS.measure.extraction.execution_timeout_seconds' as key,
         600 AS value
    FROM DUAL
 ) B ON (A.key = B.key)
WHEN NOT MATCHED THEN INSERT (key,value)
                      VALUES (B.key, b.value)
WHEN MATCHED THEN  UPDATE SET A.value=b.value;

COMMIT;