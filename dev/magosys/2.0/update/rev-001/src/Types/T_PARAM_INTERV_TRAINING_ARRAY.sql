BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PARAM_INTERV_TRAINING_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PARAM_INTERV_TRAINING_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

CREATE OR REPLACE TYPE "T_PARAM_INTERV_TRAINING_OBJ"                                          AS OBJECT
      (COD_ELEMENTO     NUMBER,
       COD_TIPO_FONTE   VARCHAR2(2 BYTE),
       COD_TIPO_COORD   VARCHAR2(1),
       COD_PREV_METEO   VARCHAR2(1),      
       DT_INIZIO DATE,
       DT_FINE DATE       
      );
/

CREATE OR REPLACE TYPE "T_PARAM_INTERV_TRAINING_ARRAY"                                          IS TABLE OF T_PARAM_INTERV_TRAINING_OBJ;
/