BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PARAM_PREV_ARRAY';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
        BEGIN 
        EXECUTE IMMEDIATE 'DROP TYPE T_PARAM_PREV_OBJ';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -04043 
                            THEN NULL; -- ORA-04043: l'oggetto T_PROFILI non esiste
                            ELSE RAISE;
                         END IF;
    END;
END;
/

CREATE OR REPLACE TYPE "T_PARAM_PREV_OBJ"                                          AS OBJECT
      (COD_ELEMENTO     NUMBER,
       COD_TIPO_FONTE   VARCHAR2(2 BYTE),
       COD_TIPO_COORD   VARCHAR2(1),
       COD_PREV_METEO   VARCHAR2(1),
       DATA_ULTIMO_AGG  DATE,
       PARAMETRO1       NUMBER,
       PARAMETRO2       NUMBER,
       PARAMETRO3       NUMBER,
       PARAMETRO4       NUMBER,
       PARAMETRO5       NUMBER,
       PARAMETRO6       NUMBER,
       PARAMETRO7       NUMBER,
       PARAMETRO8       NUMBER,
       PARAMETRO9       NUMBER,
       PARAMETRO10      NUMBER,
       V_CUT_IN         NUMBER,
       V_CUT_OFF        NUMBER,
       V_MAX_POWER      NUMBER,       
       DT_INIZIO_CALCOLO DATE,
       DT_FINE_CALCOLO DATE       
      );
/

CREATE OR REPLACE TYPE "T_PARAM_PREV_ARRAY"                                          IS TABLE OF T_PARAM_PREV_OBJ;
/