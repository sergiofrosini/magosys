CREATE OR REPLACE PACKAGE "PKG_ELEMENTI" AS

/* ***********************************************************************************************************
   NAME:       PKG_Elementi
   PURPOSE:    Servizi per la gestione degli elementi

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.i      11/12/2012  Moretti C.       Created this package.
   1.1.c      13/05/2013  Moretti C.       Visualizzazione codici Direttrici per Montanti di linea MT
   1.4.a      22/01/2014  Moretti C.       Implementazioni per versione 1.4a.p11
   1.6.a      13/01/2014  Moretti C.       Implementazioni per versione 1.6a
                                           - GetGestCodePerimeter
   1.1.h      14/03/2014  Moretti C.       In funzione GetElemInfo esclude da selezione la PI = 0
   1.7.a.2    04/04/2014  Moretti C.       parametro aggiuntivo per stato Rete in GetElementsRegistry
   1.8.a.1    22/04/2014  Moretti C.       Utilizzo Carattere jolly in procedura GetElementList
   1.8.a.5    26/08/2014  Moretti C.       Modifica gestione GetElementList
   1.9.a.0    05/08/2014  Moretti C.       Gestione Centri satellite primo step
   1.9.a.2    08/08/2014  Moretti C.       Implementazione GestCodePerimeter
   1.9.a.3    24/09/2014  Moretti C.       Gestione Centri satellite implementazione finale
   1.9.a.4    16/10/2014  Moretti C.       modificato PKG_ELEMENTI.GetElementList per soppressione dei
                                           Centri Satellite dalla selezione da Ricerca Testuale
   1.9.a.6    21/10/2014  Moretti C.       modificato PKG_ELEMENTI.GestCodePerimeter per nuovo filtro
                                           elementi con PI
   1.9.a.9    16/02/2015  Moretti C.       corretta funzione GetElemInfo.CheckDate per DATA_DISATTIVAZIONE_UNDISCONN
   1.9.a.14   27/04/2015  Moretti C.       corretta funzione GetElemInfo per attribuzione StatoRete in elementi di CS
   1.9.c.7    16/02/2016  Frosini S.       modificata GestCodePerimeter - pi� tipi elementi, aggiunto parametro fonti
   1.11        09706/2016  Roberto Z.    Modiicata per MAGONAZ 

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_RowElemento         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       COD_TIPO_FONTE       ELEMENTI_DEF.COD_TIPO_FONTE%TYPE,
                                       COD_TIPO_CLIENTE     ELEMENTI_DEF.COD_TIPO_CLIENTE%TYPE,
                                       ID_ELEMENTO          ELEMENTI_DEF.ID_ELEMENTO%TYPE,
                                       RIF_ELEMENTO         ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                                       COORDINATA_X         ELEMENTI_DEF.COORDINATA_X%TYPE,
                                       COORDINATA_Y         ELEMENTI_DEF.COORDINATA_Y%TYPE,
                                       POTENZA_INSTALLATA   ELEMENTI_DEF.POTENZA_INSTALLATA%TYPE,
                                       POTENZA_CONTRATTUALE ELEMENTI_DEF.POTENZA_CONTRATTUALE%TYPE);
 TYPE t_DefElemento         IS TABLE OF t_RowElemento;

 TYPE t_RowElemList         IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       NOME_ELEMENTO        ELEMENTI_DEF.NOME_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       DES_TIPO_ELEMENTO    TIPI_ELEMENTO.DESCRIZIONE%TYPE,
                                       FLAG                 NUMBER,
                                       RIFERIMENTO          ELEMENTI_DEF.RIF_ELEMENTO%TYPE,
                                       COUNT                NUMBER);
 TYPE t_TabElemList         IS TABLE OF t_RowElemList ;


 TYPE t_RowGerarchiaLineare IS RECORD (COD_ELEMENTO  NUMBER,
                                       L01           NUMBER,
                                       L02           NUMBER,
                                       L03           NUMBER,
                                       L04           NUMBER,
                                       L05           NUMBER,
                                       L06           NUMBER,
                                       L07           NUMBER,
                                       L08           NUMBER,
                                       L09           NUMBER,
                                       L10           NUMBER,
                                       L11           NUMBER,
                                       L12           NUMBER,
                                       L13           NUMBER,
                                       L14           NUMBER,
                                       L15           NUMBER,
                                       L16           NUMBER,
                                       L17           NUMBER,
                                       L18           NUMBER,
                                       L19           NUMBER,
                                       L20           NUMBER,
                                       L21           NUMBER,
                                       L22           NUMBER,
                                       L23           NUMBER,
                                       L24           NUMBER,
                                       L25           NUMBER);
 TYPE t_TabGerarchiaLineare IS TABLE OF t_RowGerarchiaLineare;

 TYPE t_RowGerarchia        IS RECORD (COD_ELEMENTO         ELEMENTI.COD_ELEMENTO%TYPE,
                                       COD_GEST_ELEMENTO    ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                                       COD_TIPO_ELEMENTO    ELEMENTI.COD_TIPO_ELEMENTO%TYPE,
                                       LIVELLO              INTEGER,
                                       SEQUENZA             INTEGER);
 TYPE t_TabGerarchia        IS TABLE OF t_RowGerarchia;


/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetOrderByElemType (pTipoElem       IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pCodGestElem    IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE SetElemBaseEseDef (pElementoBase   IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pElementoEseDef IN ELEMENTI.COD_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoBase    RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoEseDef  RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION DecodeTipElem      (pTipEle         IN VARCHAR2,
                              pCodeOnNotFound IN BOOLEAN DEFAULT TRUE)
                       RETURN TIPI_ELEMENTO.DESCRIZIONE%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultCO      (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetDefaultESE     (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                              pDataA          IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- FUNCTION  GetElemDef        (pData           IN DATE,
--                              pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetCodElemento    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetGestElemento   (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pNoNascosti     IN INTEGER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  CalcOutCodGest    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pData           IN DATE DEFAULT SYSDATE,
                              pTipoElemento   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                              pRifElemento    IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE DEFAULT NULL)
                                                 RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementoPadre   (pElemFiglio     IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pTipoPadre      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pData           IN DATE,
                              pOrganizzazione IN NUMBER,
                              pStato          IN NUMBER,
                              pDisconnect     IN NUMBER DEFAULT 0)
                                                 RETURN ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  InsertElement     (pCodGest        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) RETURN  ELEMENTI.COD_ELEMENTO%TYPE;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetElemInfo       (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataRif        IN DATE,
                              pTipologiaRete  IN VARCHAR2,
                              pTipiCliente    IN VARCHAR2,
                              pOrganizzaz     IN NUMBER,
                              pStatoRete      IN INTEGER) RETURN INTEGER; -- DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION  GetElemTooltip    (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataRif        IN DATE,
                              pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pElemInfo       IN INTEGER,
                              pTipologiaRete  IN VARCHAR2,
                              pTipiCliente    IN VARCHAR2,
                              pOrganizzaz     IN NUMBER,
                              pStatoRete      IN INTEGER) RETURN VARCHAR2; -- DETERMINISTIC;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE,
                               pStatoRete     IN INTEGER DEFAULT PKG_Mago_utl.gcStatoAttuale,
                               pDisconnect    IN INTEGER DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pElemTypes      IN VARCHAR2 DEFAULT NULL,
                              pCompilaInfo    IN INTEGER  DEFAULT PKG_Mago_utl.gcON,
                              pCalcOutCodGest IN INTEGER  DEFAULT 1,
                              pDisconnect     IN INTEGER  DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN VARCHAR2,
                              pDataDa         IN DATE,
                              pDataA          IN DATE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementList    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pNumRows        IN INTEGER  DEFAULT 100);

 FUNCTION GetElementList     (pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL)
                       RETURN t_TabElemList PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaLineare (pDataDa         IN DATE,
                                 pDataA          IN DATE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchiaLineare PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION LeggiGerarchiaSup     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchia PIPELINED;

 FUNCTION LeggiGerarchiaInf     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pOrganizzazione IN INTEGER,
                                 pStatoRete      IN INTEGER,
                                 pData           IN DATE DEFAULT SYSDATE,
                                 pFiltroTipEle   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                                 pDisconnect     IN INTEGER DEFAULT 0)
                          RETURN t_TabGerarchia PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetClientType     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pPOD            IN INTEGER  DEFAULT 1,    -- Default 1 = POD altrimenti COD_GEST
                              pElem           IN VARCHAR2 DEFAULT NULL, -- se null ritorna tutti i clienti definiti
                              pData           IN DATE     DEFAULT SYSDATE);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE oldGestCodePerimeter (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pData           IN DATE,
                              pElemTypeReq    IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pDisconnect     IN INTEGER  DEFAULT 0,
                              pTipiCliente    IN VARCHAR2 DEFAULT NULL,
                              pElementiConPI  IN INTEGER  DEFAULT 0);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GestCodePerimeter(
    pRefCurs OUT PKG_UtlGlb.t_query_cur,
    pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
    pOrganizzazione IN INTEGER,
    pStatoRete      IN INTEGER,
    pData           IN DATE,
    pElemTypeReq    IN VARCHAR2,
    pDisconnect     IN INTEGER DEFAULT 0,
    pTipiCliente    IN VARCHAR2 DEFAULT NULL,
    pElementiConPI  IN INTEGER DEFAULT 0,
    pEleFonti       IN VARCHAR2 DEFAULT NULL);

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetCOprevalente   (pCodElem       OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGestElem      OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNomeElem      OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE);

-- ----------------------------------------------------------------------------------------------------------

END PKG_Elementi;
/