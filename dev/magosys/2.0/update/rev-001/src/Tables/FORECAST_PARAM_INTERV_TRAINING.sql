PROMPT Aggiornameto FORECAST_PARAM_INTERV_TRAINING


BEGIN
	EXECUTE IMMEDIATE 'ALTER TABLE FORECAST_PARAM_INTERV_TRAINING DROP PRIMARY KEY CASCADE';  
EXCEPTION 
 WHEN OTHERS THEN 
  NULL;
END;
/



 
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE FORECAST_PARAM_INTERV_TRAINING CASCADE CONSTRAINTS';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/
 



CREATE TABLE FORECAST_PARAM_INTERV_TRAINING
(
  ID_INTERV_TRAINING  NUMBER,
  COD_ELEMENTO        NUMBER,
  COD_TIPO_FONTE      VARCHAR2(2),
  COD_TIPO_COORD      VARCHAR2(1)               DEFAULT 'C',
  COD_PREV_METEO      VARCHAR2(1)               DEFAULT 0,
  DT_INIZIO           DATE,
  DT_FINE             DATE
)
STORAGE    (
            BUFFER_POOL      DEFAULT
           )
LOGGING 
NOCOMPRESS 
NOCACHE
NOPARALLEL
NOMONITORING
/


COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.ID_INTERV_TRAINING IS 'ID INTERVALLO ADDESTRAMENTO';
COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.DT_INIZIO IS 'DATA INIZIO INTERVALLO ADDESTRAMENTO';
COMMENT ON COLUMN FORECAST_PARAM_INTERV_TRAINING.DT_FINE IS 'DATA FINE INTERVALLO ADDESTRAMENTO';




ALTER TABLE FORECAST_PARAM_INTERV_TRAINING ADD (
  CONSTRAINT FORECAST_PARAMETER_INTERV_T_PK
  PRIMARY KEY
  (ID_INTERV_TRAINING, COD_ELEMENTO, COD_TIPO_FONTE, COD_TIPO_COORD, COD_PREV_METEO)
  ENABLE VALIDATE)
/
  
  