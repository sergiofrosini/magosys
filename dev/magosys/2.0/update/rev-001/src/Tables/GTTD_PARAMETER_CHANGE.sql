
PROMPT Aggiornameto GTTD_PARAMETER_CHANGE

 
BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE GTTD_PARAMETER_CHANGE CASCADE CONSTRAINTS';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/
 

CREATE GLOBAL TEMPORARY TABLE GTTD_PARAMETER_CHANGE
(
  COD_GEST_ELEMENTO        VARCHAR2(20 BYTE),
  DATA_AGG                DATE,
  COD_TIPO_FONTE        VARCHAR2(2 BYTE),
  COD_TIPO_PRODUTTORE    CHAR(1 BYTE),
  COD_TIPO_RETE            CHAR(1 BYTE),
  PARAMETRO1            NUMBER,
  PARAMETRO2            NUMBER,
  PARAMETRO3            NUMBER,
  PARAMETRO4            NUMBER,
  PARAMETRO5            NUMBER,
  PARAMETRO6            NUMBER,
  PARAMETRO7            NUMBER,
  PARAMETRO8            NUMBER,
  PARAMETRO9            NUMBER,
  PARAMETRO10            NUMBER,
  V_CUT_IN                NUMBER,
  V_CUT_OFF                NUMBER,
  v_MAX_POWER            NUMBER,
  J_MAX                    NUMBER,
  ALFA_MAX                NUMBER,
  ALFA_MIN                NUMBER
)
ON COMMIT DELETE ROWS
NOCACHE
/
