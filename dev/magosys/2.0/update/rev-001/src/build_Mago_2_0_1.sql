SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.0.1 SRC
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI SAR_ADMIN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/SAR_PARAMETRI_GENERALI.sql

COMMIT;
disconnect 


PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Synonym
PROMPT
@./Synonyms/SAR_ADMIN_OPERATORI.sql


PROMPT _______________________________________________________________________________________
PROMPT Sequences
PROMPT
@./Sequences/INTERV_TRAINING_PKSEQ.sql
@./Sequences/FORECAST_PARAM_GLOBAL_PKSEQ.sql

PROMPT _______________________________________________________________________________________
PROMPT Types
PROMPT
@./Types/T_PARAM_INTERV_TRAINING_ARRAY.sql
@./Types/T_PARAM_PREV.sql

PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/aggiornaFORECAST_PARAMETER.sql
@./Tables/FORECAST_PARAM_INTERV_TRAINING.sql
@./Tables/FORECAST_PARAM_GLOBAL.sql
@./Tables/GTTD_PARAMETER_CHANGE.sql

PROMPT _______________________________________________________________________________________
PROMPT INDICES
PROMPT
@./Indices/mj_mfz_fk_idx.sql

PROMPT _______________________________________________________________________________________
PROMPT Triggers
PROMPT
@./Triggers/BEF_IUR_INTERV_TRAINING.sql
@./Triggers/BEF_IUR_FORE_PARAM_GLOBAL.sql

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_ELEMENTI.sql
@./Packages/PKG_LOGS.sql
@./Packages/PKG_MAGO_UTL.sql
@./Packages/PKG_METEO.sql
@./Packages/PKG_MISURE.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_ELEMENTI.sql
@./PackageBodies/PKG_LOGS.sql
@./PackageBodies/PKG_MAGO_UTL.sql
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_MISURE.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/MEASURE_OFFLINE_PARAMETERS.sql
@./InitTables/FORECAST_PARAM_GLOBAL.sql
@./InitTables/FORECAST_PARAMETRI.sql

COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.0.1 SRC
PROMPT

spool off
