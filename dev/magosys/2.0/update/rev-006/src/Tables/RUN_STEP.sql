PROMPT table run_step.sql 

TRUNCATE TABLE run_step;

BEGIN
EXECUTE IMMEDIATE 'CREATE UNIQUE INDEX run_step_pk ON run_step ( proc_group, stepid ) TABLESPACE &TBS&IDX';
EXCEPTION 
   WHEN OTHERS THEN
      IF SQLCODE = -00955 /* obj already exists */ THEN
         NULL;
      ELSE
        RAISE;
      END IF;
END;
/


BEGIN
EXECUTE IMMEDIATE 'ALTER TABLE run_step ADD CONSTRAINT run_step_pk PRIMARY KEY ( proc_group, stepid ) ENABLE';
EXCEPTION 
   WHEN OTHERS THEN
      IF SQLCODE = -02260 /* PK already exists */ THEN
         NULL;
      ELSE
        RAISE;
      END IF;
END;
/
