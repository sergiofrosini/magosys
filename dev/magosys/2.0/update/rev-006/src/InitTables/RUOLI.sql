PROMPT TABELLA SAR_ADMIN.RUOLI  - RUOLI.sql

MERGE INTO RUOLI T 
     USING (
            SELECT 146 COD_RUOLO,'MAGO' COD_APPLICAZIONE,0 OPERATIVO,1 FUNZIONALE,'ConfiguratoreSupervisione' DESCRIZIONE,'CFG' ACRONIMO FROM DUAL             
            ) s        
        ON (T.COD_RUOLO = s.COD_RUOLO)
      WHEN MATCHED THEN UPDATE SET T.COD_APPLICAZIONE = s.COD_APPLICAZIONE,
                                   T.OPERATIVO = s.OPERATIVO,
                                   T.FUNZIONALE = s.FUNZIONALE,
                                   T.DESCRIZIONE = s.DESCRIZIONE,
                                   T.ACRONIMO = s.ACRONIMO
      WHEN NOT MATCHED THEN INSERT (COD_RUOLO,COD_APPLICAZIONE,OPERATIVO,FUNZIONALE,DESCRIZIONE,ACRONIMO)
                            VALUES (s.COD_RUOLO,s.COD_APPLICAZIONE,s.OPERATIVO,s.FUNZIONALE,s.DESCRIZIONE,s.ACRONIMO) 
;