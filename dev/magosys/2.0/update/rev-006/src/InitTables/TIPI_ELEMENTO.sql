PROMPT  TIPI_ELEMENTO.sql


BEGIN
    BEGIN 
        EXECUTE IMMEDIATE 'ALTER TABLE TIPI_ELEMENTO DROP COLUMN SEQ_GERARCHICA';
    EXCEPTION
        WHEN others THEN IF SQLCODE = -00904 
                            THEN NULL; -- ORA-00904: "SEQ_GERARCHICA": identificativo non valido
                            ELSE RAISE;
                         END IF;
    END;
END;
/




