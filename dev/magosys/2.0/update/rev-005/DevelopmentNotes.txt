﻿## VERSION 2.0.5 rev 005 ###

Compatibilita'
——————————————————————
CORELE    ver 1.5.1 
STMAUI    ver 3.1
SAR_ADMIN ver 1.2 rev 13 o superiore


 Attivita'
——————————————————————

14/12/2016 rel_2.0.5
- MAGO 743 - TORINO, BOLOGNA,... - GetMeasureOffline() STM chiede troppi elementi (?) per singola richiesta, alzare i limite da 800 a 2000
26/10/2016	Forno S.
- MAGO 696 - Modifica SPC PKG_METEO.GetDettaglioForecastParam