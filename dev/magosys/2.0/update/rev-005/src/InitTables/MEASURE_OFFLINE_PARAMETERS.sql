PROMPT INIT TABLE MEASURE_OFFLINE_PARAMETERS;
 
MERGE INTO MEASURE_OFFLINE_PARAMETERS A USING
 (SELECT 'STWebRegistry-WS.measure.offline.max_gest_code_per_request' as key,
         2000 AS value
    FROM DUAL
 ) B ON (A.key = B.key)
WHEN NOT MATCHED THEN INSERT (key,value)
                      VALUES (B.key, B.value)
WHEN MATCHED THEN  UPDATE SET A.value=B.value;

COMMIT;