SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.0.5 SRC
PROMPT =======================================================================================


SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 



PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2

PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_METEO.sql


PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_METEO.sql

PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/MEASURE_OFFLINE_PARAMETERS.sql



COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.0.5 SRC
PROMPT

spool off
