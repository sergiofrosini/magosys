Prompt Package PKG_GERARCHIA_DGF;
--
-- PKG_GERARCHIA_DGF  (Package) 
--
--  Dependencies: 
--   STANDARD (Package)
--   STANDARD (Package)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   PROVINCE (Table)
--   COMUNI (Table)
--   PRODUTTORI_TMP (Table)
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI_DEF (Table)
--   REL_ELEMENTI_AMM (Table)
--   REL_ELEMENTI_ECP_SN (Table)
--   REL_ELEMENTI_ECS_SN (Table)
--   REL_ELEMENTI_GEO (Table)
--   PKG_MAGO_DGF (Package)
--   DBMS_OUTPUT ()
--   PKG_UTLGLB ()
--   DEFAULT_CO (Table)
--
CREATE OR REPLACE PACKAGE            "PKG_GERARCHIA_DGF" AS
/* ***********************************************************************************************************
   NAME:       pkg_gerarchia_dgf
   PURPOSE:    Servizi per la gestione delle gerarchie di Eolico e Solare
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      15/05/2012  Paolo Campi      Created this package.
   1.9d.001		 			
   2.0.2	  14/10/2016  Forno.S			Fix per ACEA - Sostituzione in mago di CONNECT BY PRIOR <condizione> con CONNECT BY NOCYCLE PRIOR <condizione>
   NOTES:
*********************************************************************************************************** */
 gcLogDir           CONSTANT VARCHAR2(30) := PKG_UtlGlb.gv_DirLog;
 gcLogFile          CONSTANT VARCHAR2(30) := PKG_UtlGlb.GetLogFileName('Mag_PkgLogDef__');

   PROCEDURE load_gerarchia_ecs(pData IN DATE DEFAULT SYSDATE, pflag_geo IN NUMBER);
   PROCEDURE load_gerarchia_ecs_geo(pData IN DATE DEFAULT SYSDATE);
   PROCEDURE load_gerarchia_ecs_amm(pData IN DATE DEFAULT SYSDATE);
   PROCEDURE load_gerarchia_ecp(pData IN DATE DEFAULT SYSDATE);
   PROCEDURE load_gerarchia_amm(pData IN DATE DEFAULT SYSDATE);
   PROCEDURE load_gerarchia_geo(pData IN DATE DEFAULT SYSDATE);
   PROCEDURE linearizza_gerarchia(pTipoGer IN VARCHAR2);
   PROCEDURE linearizza_gerarchia_geo;
   PROCEDURE linearizza_gerarchia_amm;
   PROCEDURE linearizza_gerarchia_imp;

END;
/

SHOW ERRORS;


