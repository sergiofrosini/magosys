Prompt Package PKG_AGGREGAZIONI;
--
-- PKG_AGGREGAZIONI  (Package)
--
--  Dependencies:
--   STANDARD (Package)
--   STANDARD (Package)
--   PLITBLM (Synonym)
--   DBMS_OUTPUT (Synonym)
--   PKG_UTLGLB (Synonym)
--   PKG_GESTANAGR (Synonym)
--   TIPI_MISURA (Table)
--   TIPI_RETE (Table)
--   TIPO_FONTI (Table)
--   TIPI_ELEMENTO (Table)
--   ELEMENTI (Table)
--   ELEMENTI (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   TRATTAMENTO_ELEMENTI (Table)
--   GTTD_CALC_GERARCHIA (Table)
--   GTTD_VALORI_TEMP (Table)
--   GTTD_MISURE (Table)
--   TIPI_CLIENTE (Table)
--   PKG_UTLGLB ()
--   PKG_MAGO (Package)
--   PKG_ELEMENTI (Package)
--   PKG_LOGS (Package)
--   PKG_MISURE (Package)
--   DBMS_OUTPUT ()
--   PKG_GESTANAGR ()
--   PLITBLM ()
--   MISURE_AGGREGATE (Table)
--
CREATE OR REPLACE PACKAGE PKG_AGGREGAZIONI AS

/* ***********************************************************************************************************
   NAME:       PKG_Aggregazioni
   PURPOSE:    Servizi per la gestione delle Misure

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      07/10/2011  Moretti C.       Created this package.
   1.0.a      05/12/2011  Moretti C.       Varsione collaudo Iniziale
   1.0.b      06/01/2012  Moretti C.       Opzione Nazionale + Modifiche da collaudo
   1.0.d      20/04/2012  Moretti C.       Avanzamento
   1.0.e      11/06/2012  Moretti C.       Avanzamento e Correzioni
   1.0.f      13/07/2012  Moretti C.       Avanzamento STM, Implementazione GDF
   1.0.h      08/10/2012  Moretti C.       .....
   1.0.i      30/11/2012  Moretti C.       .....
   1.8.a.1    22/04/2014  Moretti C.       Aggregazione - Ottimizzazione query e Semplificazione processo
   1.9.a.3    24/08/2014  Moretti C.       Gestione Centri satellite
   1.9.a.9    13/02/2015  Moretti C.       Correzione per integrazione
   2.0.2	  14/10/2016  Forno.S			Fix per ACEA - Sostituzione in mago di CONNECT BY PRIOR <condizione> con CONNECT BY NOCYCLE PRIOR <condizione>

   NOTES:

*********************************************************************************************************** */


/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

    TYPE t_RowAggrMis IS RECORD (COD_TRATTAMENTO_ELEM  TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE,
                                 DATA                  DATE,
                                 COD_ELEMENTO          ELEMENTI.COD_ELEMENTO%TYPE,
                                 COD_TIPO_MISURA       TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                                 COD_TIPO_FONTE        TIPO_FONTI.COD_TIPO_FONTE%TYPE,
                                 COD_TIPO_RETE         TIPI_RETE.COD_TIPO_RETE%TYPE,
                                 COD_TIPO_CLIENTE      TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE,
                                 VALORE                NUMBER);
    TYPE t_TabAggrMis IS TABLE OF t_RowAggrMis;

    gcElaborazioneImmediata CONSTANT INTEGER := 1;
    gcElaborazioneStandard  CONSTANT INTEGER := 0;

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

 FUNCTION CalcAggregazioniAlVolo(pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pCodEle           IN ELEMENTI.COD_ELEMENTO%TYPE)
                          RETURN PKG_Aggregazioni.t_TabAggrMis PIPELINED;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LinearizzaGerarchia  (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pData           IN DATE,
                                 pOrganizzazione IN NUMBER,
                                 pStato          IN NUMBER);

 PROCEDURE EseguiAggregazione   (pOrganizzazione IN NUMBER,
                                 pStato          IN NUMBER,
                                 pData           IN DATE,
                                 pMisStat        IN INTEGER);

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CalcolaDisattivazioneUndisconn    (pTablename IN VARCHAR2);

END PKG_Aggregazioni;
/
SHOW ERRORS;


