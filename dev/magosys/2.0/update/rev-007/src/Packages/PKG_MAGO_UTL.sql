CREATE OR REPLACE PACKAGE "PKG_MAGO_UTL" AS
/* ***********************************************************************************************************
   NAME:       PKG_Mago_utl
   PURPOSE:    Servizi di Utility
   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   1.0.0      17/09/2012  Paolo Campi      Created this package.
   
      1.11      09/06/2016   Roberto Z.       Modifica per Magonaz
   2.0.7      24/03/2017   Frosini S.      JIra- 969 - spacchettaparam SELECT DISTINCT
   NOTES:
*********************************************************************************************************** */
 gcOrganizzazELE    CONSTANT NUMBER(1)    := 1;        -- organizzazione elettrica
 gcOrganizzazGEO    CONSTANT NUMBER(1)    := 2;        -- organizzazione geografica (Istat)
 gcOrganizzazAMM    CONSTANT NUMBER(1)    := 3;        -- organizzazione amministrativa
 gcLogDir           CONSTANT VARCHAR2(30) := PKG_UtlGlb.gv_DirLog;
 gcLogFile          CONSTANT VARCHAR2(30) := PKG_UtlGlb.GetLogFileName('Mag_PkgLogDef__');
 -- ----------------------------------------------------------------------------------------------------------
-- Tipi Produttore
-- ----------------------------------------------------------------------------------------------------------
 gcProduttorePuro       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'A';   -- Produttore puro
 gcProduttoreCliente    CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'B';   -- Produttore/Cliente
 gcProduttoreNonDeterm  CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE   := 'X';   -- Produttore non determinato
-- ----------------------------------------------------------------------------------------------------------
-- Tipi Misura
-- ----------------------------------------------------------------------------------------------------------
 gcPotenzaAttGenerata   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAG'; -- Potenza Attiva Generata
 gcVelVento             CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'VVE';  -- Velocita' Vento
 gcDirVento             CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'DVE';  -- Direzione Vento
 gcIrradiamentoSol      CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'IRR'; -- Irradiamento Solare
 gcTemperatura          CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'TMP'; -- Temperatura
 -- ---------------------------------------------------------------------------------------------------------
  gcSeparatoreElem   CONSTANT VARCHAR2(1) := '#' ; -- '#' per separare i valori nel codice gestionale
  gcSostitutSeparatoreElem      CONSTANT VARCHAR2(1) := '_' ; -- '#' per separare i valori nel codice gestionale
  
 /* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

 TYPE t_RetArea             IS RECORD (RC         INTEGER,
                                       SHORT_MSG  VARCHAR2(1000),
                                       LONG_MSG   LOG_HISTORY.MESSAGE%TYPE
                                      );
 TYPE t_Misura              IS RECORD (COD_TRATTAMENTO_ELEM NUMBER,
                                       DATA                 DATE,
                                       QUALITA              RAW(2),
                                       VALORE               NUMBER);
 TYPE t_TabMisure           IS TABLE OF t_Misura;

-- ----------------------------------------------------------------------------------------------------------

 cSepCharLst        CONSTANT CHAR(1)   := CHR(124);  -- |
 cSeparatore                 CHAR(1)   := CHR(167);  -- �

-- ----------------------------------------------------------------------------------------------------------

 gcON               CONSTANT NUMBER(1) := 1;
 gcOFF              CONSTANT NUMBER(1) := 0;

 gcNullNumCode      CONSTANT NUMBER(1) := -1;

 gcNoOper           CONSTANT NUMBER(1) := 0;
 gcInserito         CONSTANT NUMBER(1) := 1;
 gcModificato       CONSTANT NUMBER(1) := 2;


 gcStatoNullo       CONSTANT NUMBER(1) := 0;        -- stato non applicabile
 gcStatoNormale     CONSTANT NUMBER(1) := 1;        -- stato normale
 gcStatoAttuale     CONSTANT NUMBER(1) := 2;        -- stato attuale

 gcTabAcquisite     CONSTANT NUMBER(1) := 1;        -- Misure in tabella MISURE_ACQUISITE
 gcTabAggregate     CONSTANT NUMBER(1) := 2;        -- Misure in tabella MISURE_AGGREGATE

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Installazione
-- ----------------------------------------------------------------------------------------------------------

 gcMagoNazionale    CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 1;
 gcMagoSTM          CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 2;
 gcMagoDGF          CONSTANT DEFAULT_CO.TIPO_INST%TYPE := 4;

 gStartDate         DATE;
-- ----------------------------------------------------------------------------------------------------------
-- Tipi Elemento
-- ----------------------------------------------------------------------------------------------------------

 gcNazionale              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'NAZ'; -- nazionale
 gcRegione                CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'REG'; -- regione
 gcProvincia              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'PRV'; -- provincia
 gcComune                 CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'COM'; -- comune
 gcCFT                    CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CFT'; -- CFT
 gcZona                   CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ZNA'; -- zona
 gcCentroOperativo        CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'COP'; -- centro operativo
 gcEsercizio              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ESE'; -- esercizio

 gcCabinaPrimaria         CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CPR'; -- Cabina primaria
 gcSbarraAT               CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SAT'; -- Sbarra AT
 gcTrasfCabPrim           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRF'; -- Trasformatore di cabina primaria
 gcSecondarioDiTrasf      CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRS'; -- Secondario di trasformatore
 gcTerziarioDiTrasf       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRT'; -- Terziario di trasformatore
 gcSbarraMT               CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SMT'; -- Sbarra MT
 gcDiretticeMT            CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'DIR'; -- Direttice MT
 gcLineaMT                CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'LMT'; -- Linea MT
 gcCabinaSecondaria       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CSE'; -- cabina secondaria
 gcSbarraCabSec           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'SCS'; -- sbarra di cabina secondaria
 gcTrasformMtBt           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRM'; -- Trasformatore BT/BT
 gcTrasformMtBtDett       CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'TRX'; -- Dettaglio di Trasformatore BT/BT

 gcClienteAT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CAT'; -- Cliente alta tensione
 gcClienteMT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CMT'; -- Cliente media tensione
 gcClienteBT              CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'CBT'; -- Cliente bassa tensione

 gcGeneratoreAT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GAT'; -- Generatore alta tensione
 gcGeneratoreMT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GMT'; -- Generatore media tensione
 gcGeneratoreBT           CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'GBT'; -- Generatore bassa tensione

 gcAreaSconnessa          CONSTANT TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := 'ZSC'; -- Area Sconnessa

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Misura
-- ----------------------------------------------------------------------------------------------------------

 gcPotenzaAttScambiata    CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAS'; -- Potenza Attiva Scambiata
 gcPotenzaAttGenerata     CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PAG'; -- Potenza Attiva Generata
 gcPotenzaInstallata      CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PI';  -- Potenza Installata
 --gcPotenzaContrattuale    CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PC';  -- Potenza Contrattuale
 gcPotenzaMeteoPrevisto   CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PMP'; -- Potenza Meteo Previsto
 gcPotenzaMeteoCieloTerso CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'PMC'; -- Potenza Meteo Previsto
 gcNumeroImpianti         CONSTANT TIPI_MISURA.COD_TIPO_MISURA%TYPE     := 'NRI'; -- Numero Impianti

-- ----------------------------------------------------------------------------------------------------------
-- Raggruppamento Fonti
-- ----------------------------------------------------------------------------------------------------------
 gcRaggrFonteSolare       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'S';  -- Solare
 gcRaggrFonteSolareID     CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 1;    -- Solare
 gcRaggrFonteEolica       CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'E';  -- Elolica
 gcRaggrFonteEolicaID     CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 2;    -- Elolica
 gcRaggrFonteIdraulica    CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'I';  -- Idraulica
 gcRaggrFonteIdraulicaID  CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 4;    -- Idraulica
 gcRaggrFonteTermica      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'T';  -- Termica
 gcRaggrFonteTermicaID    CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 8;    -- Termica
 gcRaggrFonteRinnovab     CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'R';  -- Altra Rinnovavbile
 gcRaggrFonteRinnovabID   CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 16;   -- Altra Rinnovavbile
 gcRaggrFonteConvenz      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := 'C';  -- Altra Convenzionale
 gcRaggrFonteConvenzID    CONSTANT RAGGRUPPAMENTO_FONTI.ID_RAGGR_FONTE%TYPE  := 32;   -- Altra Convenzionale

 gcRaggrFonteNonOmog      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '1';  -- Non Omogenea
 gcRaggrFonteNonAppl      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '2';  -- Non Applicabile
 gcRaggrFonteNonDisp      CONSTANT RAGGRUPPAMENTO_FONTI.COD_RAGGR_FONTE%TYPE := '3';  -- Non Disponibile

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Rete
-- ----------------------------------------------------------------------------------------------------------

 gcTipReteAT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'A';   -- Rete AT
 gcTipReteMT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'M';   -- Rete MT
 gcTipReteBT           CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'B';   -- Rete BT
 gcTipReteNonDisp      CONSTANT TIPI_RETE.COD_TIPO_RETE%TYPE                := 'X';   -- Tipo Rete non disponibile

-- ----------------------------------------------------------------------------------------------------------
-- Tipi Cliente/Fornitore
-- ----------------------------------------------------------------------------------------------------------

 gcClienteProdPuro     CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'A';   -- Produttore puro
 gcClienteAutoProd     CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'B';   -- Autoproduttore
 gcClientePrdNonDeterm CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'X';   -- Non determinato
 gcClientePrdNonApplic CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'Z';   -- Non applicabile
 gcCliente             CONSTANT TIPI_CLIENTE.COD_TIPO_CLIENTE%TYPE          := 'C';   -- Cliente

-- ----------------------------------------------------------------------------------------------------------
-- Tipi CLASSI Job (eleborazioni)
-- ----------------------------------------------------------------------------------------------------------

 gcJobClassUTL         CONSTANT VARCHAR2(20) := 'Utility';
 gcJobClassMIS         CONSTANT VARCHAR2(20) := 'Misure';
 gcJobClassANA         CONSTANT VARCHAR2(20) := 'Anagraf';
 gcJobClassAGR         CONSTANT VARCHAR2(20) := 'Aggregaz';
 gcJobClassMET         CONSTANT VARCHAR2(20) := 'Meteo';
 gcJobClassREP         CONSTANT VARCHAR2(20) := 'Report';
 gcJobClassFE          CONSTANT VARCHAR2(20) := 'FrontEnd';
 gcJobClassBE          CONSTANT VARCHAR2(20) := 'BackEnd';
 gcJobClassReplay      CONSTANT VARCHAR2(20) := 'Replay';

 gcJobSubClassFE       CONSTANT VARCHAR2(7) := '.FE' ; -- FrontEnd
 gcJobSubClassBE       CONSTANT VARCHAR2(7) := '.BE' ; -- BackEnd
 gcJobSubClassELE      CONSTANT VARCHAR2(7) := '.ELE'; -- Elementi
 gcJobSubClassMIS      CONSTANT VARCHAR2(7) := '.MIS'; -- Misure
 gcJobSubClassMET      CONSTANT VARCHAR2(7) := '.MET'; -- Misure
 gcJobSubClassGME      CONSTANT VARCHAR2(7) := '.GME'; -- Misure GME
 gcJobSubClassSRV      CONSTANT VARCHAR2(7) := '.SRV'; -- Servizio
 gcJobSubClassINI      CONSTANT VARCHAR2(7) := '.INI'; -- Inizializzazione
 gcJobSubClassAGG      CONSTANT VARCHAR2(7) := '.AGG'; -- Aggiornamento
 gcJobSubClassINS      CONSTANT VARCHAR2(7) := '.INS'; -- Inserimento
 gcJobSubClassCAL      CONSTANT VARCHAR2(7) := '.CAL'; -- Calcolo
 gcJobSubClassLIN      CONSTANT VARCHAR2(7) := '.LIN'; -- Linearizzazione
 gcJobSubClassSCH      CONSTANT VARCHAR2(7) := '.SCH'; -- Schedulatore
 gcJobSubClassINIT     CONSTANT VARCHAR2(7) := '.INI'; -- Inizializzazione

-- ----------------------------------------------------------------------------------------------------------
-- Tipi GTTD_VALORI_TEMP
-- ----------------------------------------------------------------------------------------------------------

 gcTmpTipEleKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'CODELE';
 gcTmpTipMisKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPMIS';
 gcTmpTipFilMisKey     CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPFILMIS';
 gcTmpTipRetKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPRET';
 gcTmpTipFonKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPFON';
 gcTmpTipCliKey        CONSTANT GTTD_VALORI_TEMP.TIP%TYPE := 'TIPCLI';

/* ***********************************************************************************************************
 Variabili Pubbliche
*********************************************************************************************************** */

 gInizializzazione_In_Corso  BOOLEAN := FALSE;
  
   gTipoInstallazione DEFAULT_CO.TIPO_INST%TYPE;
  
-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMagoStm          RETURN BOOLEAN;
 FUNCTION IsMagoStmFL        RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsMagoDgf          RETURN BOOLEAN;
 FUNCTION IsMagoDgfFL        RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsNazionale        RETURN BOOLEAN;
 FUNCTION IsNazionaleFL      RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsReplay           RETURN BOOLEAN;
 FUNCTION IsReplayFL         RETURN INTEGER;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION IsRomania          RETURN BOOLEAN;
 FUNCTION IsRomaniaFL        RETURN INTEGER;

-- ---------------------------------------------------------------------------------------------------------
   FUNCTION getproduttorepuro RETURN VARCHAR2;
   FUNCTION getproduttorecliente RETURN VARCHAR2;
   FUNCTION getproduttorenondeterm RETURN VARCHAR2;
   FUNCTION getSeparatore RETURN VARCHAR2;
   FUNCTION  leggiparam(pparam_name IN VARCHAR2) RETURN VARCHAR2;
   FUNCTION  spacchettaparam(pproc_name IN VARCHAR2,param_num IN NUMBER, last_param OUT NUMBER) RETURN VARCHAR2;
   FUNCTION  convertiparam(pproc_name IN VARCHAR2,param_num IN NUMBER, pparvalue IN VARCHAR2) RETURN VARCHAR2;
   PROCEDURE run_application_group(gruppo IN VARCHAR2);
   PROCEDURE startRun;
   FUNCTION checkRun ( pproc_name IN VARCHAR2 ) RETURN NUMBER ;
   PROCEDURE setRun ( pproc_name IN VARCHAR2, flag_run IN NUMBER, flag_err IN NUMBER) ;
   PROCEDURE insLog( pproc_name IN VARCHAR2, pmex IN VARCHAR2, errnum IN NUMBER);
   
      FUNCTION StdOutDate          (pData          IN DATE) RETURN VARCHAR2;
      
 FUNCTION GetIdTipoFonti        (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;

 FUNCTION GetIdTipoReti         (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;

 FUNCTION GetIdTipoClienti      (pLista VARCHAR2) RETURN NUMBER DETERMINISTIC;      
 
      
    PROCEDURE TrattaListaCodici  (pLista         IN VARCHAR2,
                               pTempTip       IN GTTD_VALORI_TEMP.TIP%TYPE,
                               pAggrega      OUT NUMBER);
END;
/