PROMPT  METEO_JOB_STATIC_CONFIG_____MPS.estimation.measure.days.number 


BEGIN 

INSERT INTO METEO_JOB_STATIC_CONFIG (KEY, VALUE) VALUES ( 'MPS.estimation.measure.days.number', 
														( SELECT VALUE * 7 FROM METEO_JOB_STATIC_CONFIG WHERE KEY ='MPS.estimation.measure.week.number')
														);
EXCEPTION 
    WHEN OTHERS THEN 
            NULL;
 
END;
/
 
														

DELETE FROM METEO_JOB_STATIC_CONFIG WHERE KEY = 'MPS.estimation.measure.week.number';

