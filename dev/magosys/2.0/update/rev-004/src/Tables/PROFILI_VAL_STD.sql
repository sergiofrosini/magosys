PROMPT TABLE PROFILI_VAL * Standard Edition *;
--
-- PROFILI_VAL  (Table) 
--
--  Dependencies: 
--   PROFILI (Table)
--

BEGIN 
    EXECUTE IMMEDIATE 'DROP TABLE PROFILI_VAL PURGE';
EXCEPTION 
    WHEN OTHERS THEN IF SQLCODE = -00942 THEN NULL; -- ORA-00942: tabella o vista inesistente
                                         ELSE RAISE;
                     END IF;
END;
/


CREATE TABLE PROFILI_VAL
(
  COD_PROFILO  NUMBER,
  ANNO         NUMBER(4),
  MESE         NUMBER(2),
  DAYTIME      VARCHAR2(4),
  VALORE       NUMBER, 
  CONSTRAINT PROFILI_VAL_STORICO_PK
  PRIMARY KEY
  (ANNO, MESE, COD_PROFILO, DAYTIME)
  ENABLE VALIDATE
)
ORGANIZATION INDEX
TABLESPACE &TBS&IOT
/

COMMENT ON TABLE PROFILI_VAL IS 'Definizione dei PROFILI_VAL Misura'
/

COMMENT ON COLUMN PROFILI_VAL.COD_PROFILO IS 'Identificativo DEL profilo'
/

COMMENT ON COLUMN PROFILI_VAL.ANNO IS 'Anno cui il profilo (COD_PROFILO) si riferisce'
/

COMMENT ON COLUMN PROFILI_VAL.MESE IS 'Mese a cui il profilo (COD_PROFILO) si riferisce'
/

COMMENT ON COLUMN PROFILI_VAL.DAYTIME IS 'Formato HHMI - Ora e Minuto (intervallo) del Giorno a cui il valore di riferisce'
/

COMMENT ON COLUMN PROFILI_VAL.VALORE IS 'Valore attribuito al DAYTIME del profilo'
/



PROMPT TRIGGER BEF_IUR_PROFILI_VAL;
--
-- BEF_IUR_PROFILI_VAL  (Trigger) 
--
--  Dependencies: 
--   PROFILI_VAL (Table)
--
CREATE OR REPLACE TRIGGER BEF_IUR_PROFILI_VAL
BEFORE INSERT OR UPDATE
ON PROFILI_VAL
REFERENCING NEW AS NEW OLD AS OLD
FOR EACH ROW
DECLARE

/* *****************************************************************************
   NAME:       BEF_IUR_PROFILI_VAL
   PURPOSE:

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  ------------------------------------
   1.0.0      07/06/2016   Moretti C.      Created this trigger.
   
   NOTES:

***************************************************************************** */
    vDAYTIME  PROFILI_VAL.DAYTIME%TYPE;   
BEGIN
    BEGIN
        vDAYTIME := TO_CHAR(TO_DATE(:NEW.DAYTIME,'hh24mi'),'hh24mi');
    EXCEPTION
        WHEN OTHERS THEN RAISE_APPLICATION_ERROR(-20201,'DAYTIME '||:NEW.DAYTIME||' non valido per il profilo');
    END;
   :NEW.DAYTIME := vDAYTIME;
END BEF_IUR_PROFILI_VAL;
/


-- 
-- Non Foreign Key Constraints for Table PROFILI_VAL 
-- 
PROMPT Non-FOREIGN KEY CONSTRAINTS ON TABLE PROFILI_VAL;
ALTER TABLE PROFILI_VAL ADD (
  CHECK (MESE BETWEEN 1 AND 12)
  ENABLE VALIDATE)
/


-- 
-- Foreign Key Constraints for Table PROFILI_VAL 
-- 
PROMPT FOREIGN KEY CONSTRAINTS ON TABLE PROFILI_VAL;
ALTER TABLE PROFILI_VAL ADD (
  CONSTRAINT PRF_VAL_DEF 
  FOREIGN KEY (COD_PROFILO) 
  REFERENCES PROFILI (COD_PROFILO)
  ENABLE VALIDATE)
/
