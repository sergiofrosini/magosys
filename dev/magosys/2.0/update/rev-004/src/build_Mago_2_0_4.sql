SPOOL &spool_all append 

SET VER OFF 
SET SERVEROUTPUT ON SIZE UNLIMITED

WHENEVER SQLERROR EXIT SQL.SQLCODE

PROMPT =======================================================================================
PROMPT MAGO rel 2.0.4 SRC
PROMPT =======================================================================================

UNDEF TBS;
DEFINE TBS='MAGO';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 


PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI SAR_ADMIN <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn sar_admin/sar_admin_dba&tns_arcdb1
@./InitTables/SAR_PARAMETRI_GENERALI.sql



COLUMN ORATYPE NEW_VALUE ORATYPE;
COLUMN ORADESC NEW_VALUE ORADESC;

SELECT CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'STD'
        ELSE 'ENT'
       END ORATYPE,
	   CASE WHEN INSTR(LOWER(BANNER),'enterprise') = 0 
        THEN 'Standard Edition'
        ELSE 'Enterprise Edition'
       END ORADESC 
  FROM V$VERSION
 WHERE INSTR(LOWER(BANNER),'oracle database')>0;
 
 

COMMIT;
disconnect 



PROMPT _______________________________________________________________________________________
PROMPT DEFINIZIONE OGGETTI MAGO <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
PROMPT

conn mago/mago&tns_arcdb2




PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT

PROMPT Oracle Version &ORADESC

@./Tables/PROFILI_VAL_&ORATYPE.sql


PROMPT _______________________________________________________________________________________
PROMPT InitTables
PROMPT
@./InitTables/METEO_JOB_STATIC_CONFIG.sql




PROMPT _______________________________________________________________________________________
PROMPT Packages
PROMPT
@./Packages/PKG_METEO.sql
@./Packages/PKG_PROFILI.sql

PROMPT _______________________________________________________________________________________
PROMPT PackageBodies
PROMPT
@./PackageBodies/PKG_METEO.sql
@./PackageBodies/PKG_PROFILI.sql


COMMIT;

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE MAGO 2.0.4 SRC
PROMPT

spool off
