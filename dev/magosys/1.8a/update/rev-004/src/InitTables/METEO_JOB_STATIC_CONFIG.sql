PROMPT METEO_JOB_STATIC_CONFIG

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MPS.load.forecast.elaboration.chunk.size' key , '500' value
              FROM dual
             UNION ALL
            SELECT 'MPS.load.forecast.elaboration.default.forward.days' key , '2' value
              FROM dual
             UNION ALL
            SELECT 'MPS.load.forecast.recovery.enable' key , 'true' value
              FROM dual
             UNION ALL
            SELECT 'MPS.load.forecast.recovery.day.amount' key , '3' value
              FROM dual
            ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;
