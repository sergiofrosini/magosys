﻿## 24/10/2014 VERSION 1.8a.3 rev-003 

24/10/2014 rel_18a3
- Bug 1308 - WS-getMeasureOffline() troppo lenta - 
   21 sec per un campione PI; 11 sec un campione PPAGmp-S
 
14/11/2014  rel_18a3
  - gestione calendario sul PKg2 utilizzato da LFS
  - Aggiornamento Amministrazione Richiesto da MAGO 1.8a.3 per Amministrazione 1.2.1
  - Questo non influenza altre patch.
 