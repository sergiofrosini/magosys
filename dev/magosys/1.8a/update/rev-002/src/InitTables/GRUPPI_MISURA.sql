set serveroutput on;

declare

procedure insUna(pCOD_GRUPPO        in gruppi_misura.cod_gruppo%type,
                pCOD_TIPO_ELEMENTO  in gruppi_misura.COD_TIPO_ELEMENTO%type,
                pCOD_TIPO_MISURA    in gruppi_misura.COD_TIPO_MISURA%type,
                pseq_ord            in gruppi_misura.seq_ord%type,
                pflag_attivo IN NUMBER) is    
 
begin
  if (pCOD_GRUPPO != 1) THEN
     MERGE INTO gruppi_misura t
     USING ( 
            SELECT pCOD_GRUPPO cod_gruppo , pCOD_TIPO_ELEMENTO cod_tipo_elemento , pCOD_TIPO_MISURA cod_tipo_misura
                   ,pflag_attivo flag_attivo , pseq_ord seq_ord, 0 disattivato
              FROM DUAL 
            ) s
        ON (t.cod_gruppo =s.cod_gruppo AND t.cod_tipo_elemento = s.cod_tipo_elemento AND s.cod_tipo_misura = t.cod_tipo_misura)
      WHEN MATCHED THEN
    UPDATE SET 
               t.flag_attivo = s.flag_Attivo
              ,t.seq_ord = s.seq_ord
              ,t.disattivato = s.disattivato
      WHEN NOT MATCHED THEN 
    INSERT   
          (cod_gruppo,cod_tipo_elemento,cod_tipo_misura,flag_attivo,seq_ord,disattivato) 
    VALUES (s.cod_gruppo, s.cod_tipo_elemento, s.cod_tipo_misura, s.flag_attivo, s.seq_ord, s.disattivato);
   end if;
end;

begin

  for vRiga in 
    (select cod_gruppo, cod_tipo_elemento, max(seq_ord) m from gruppi_misura
    group by cod_gruppo, cod_tipo_elemento)
  loop
  
  dbms_output.put_line(vRiga.cod_gruppo||' '||vRiga.cod_tipo_elemento||' '||vRiga.m);

IF vRiga.cod_tipo_elemento in ('TRM','CMT','CBT') THEN  
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PAAS',vRiga.m+5,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
    insUna(vRiga.cod_gruppo,vRiga.cod_tipo_elemento,'PAGS',vRiga.m+6,case vRiga.cod_gruppo WHEN 1 THEN 1 ELSE 0 END);
END IF;

  end loop;

end;
/

commit;
