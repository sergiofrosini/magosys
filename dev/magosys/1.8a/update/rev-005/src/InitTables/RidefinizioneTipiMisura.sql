
SET SERVEROUTPUT ON SIZE UNLIMITED


DECLARE
    vNum INTEGER;
BEGIN

    SELECT COUNT(*) INTO vNum 
      FROM TIPI_MISURA
     WHERE COD_TIPO_MISURA IN ('PPATCT','PPATMP');
    
    IF vNum = 2 THEN
        DBMS_OUTPUT.PUT_LINE(RPAD('=',50,'=')||CHR(10)||'Tipi Misura PPATCT e PPATMP gia'' definiti !'||CHR(10)||RPAD('=',50,'='));
        RETURN;
    END IF;

    FOR I IN (SELECT A.TABLE_NAME, A.CONSTRAINT_NAME, b.STATUS, A.COLUMN_NAME
                FROM USER_CONS_COLUMNS A 
               INNER JOIN USER_CONSTRAINTS B ON B.CONSTRAINT_NAME = A.CONSTRAINT_NAME
                                            AND B.OWNER = A.OWNER
               WHERE COLUMN_NAME LIKE '%COD_TIPO_MISURA%'
                 AND CONSTRAINT_TYPE = 'R'
--                 AND A.OWNER = 'MAGO'
               ORDER BY 1,2
             ) LOOP
        EXECUTE IMMEDIATE 'ALTER TABLE '||i.TABLE_NAME||' DISABLE CONSTRAINT '||i.CONSTRAINT_NAME;
		DBMS_OUTPUT.PUT_LINE('Constraint '||i.CONSTRAINT_NAME||' di tabella '||i.TABLE_NAME||'  disabilitato.');
    END LOOP;

    UPDATE TIPI_MISURA SET COD_TIPO_MISURA = 'PPATCT',CODIFICA_ST = 'PPATct' WHERE UPPER(COD_TIPO_MISURA) = 'PPATCT';
    DBMS_OUTPUT.PUT_LINE('Tabella COD_TIPO_MISURA - '||SQL%ROWCOUNT||' records modificati (PPATCT)');
    UPDATE TIPI_MISURA SET COD_TIPO_MISURA = 'PPATMP',CODIFICA_ST = 'PPATmp' WHERE UPPER(COD_TIPO_MISURA) = 'PPT';
    DBMS_OUTPUT.PUT_LINE('Tabella COD_TIPO_MISURA - '||SQL%ROWCOUNT||' records modificati (PPATCT)');

    FOR I IN (SELECT A.TABLE_NAME, A.CONSTRAINT_NAME, b.STATUS, A.COLUMN_NAME
                FROM USER_CONS_COLUMNS A 
               INNER JOIN USER_CONSTRAINTS B ON B.CONSTRAINT_NAME = A.CONSTRAINT_NAME
                                            AND B.OWNER = A.OWNER
               WHERE COLUMN_NAME LIKE '%COD_TIPO_MISURA%'
                 AND CONSTRAINT_TYPE = 'R'
--                 AND A.OWNER = 'MAGO'
               ORDER BY 1,2
             ) LOOP
        IF i.TABLE_NAME = 'GRUPPI_MISURA' THEN
            EXECUTE IMMEDIATE 'UPDATE '||i.TABLE_NAME||' SET '||i.COLUMN_NAME||' = ''PPATMP'' WHERE UPPER('||i.COLUMN_NAME||') = ''PPT''';
            DBMS_OUTPUT.PUT_LINE(i.TABLE_NAME||'.'||i.COLUMN_NAME||' - '||SQL%ROWCOUNT||' records modificati (da tipmis PPT A tipmis PPATMP)');
            EXECUTE IMMEDIATE 'DELETE '||i.TABLE_NAME||' WHERE UPPER('||i.COLUMN_NAME||') = ''PPATCT'' ';
            DBMS_OUTPUT.PUT_LINE(i.TABLE_NAME||'.'||i.COLUMN_NAME||' - '||SQL%ROWCOUNT||' records eliminati (tipmis PPATCT)');
            EXECUTE IMMEDIATE 'INSERT INTO '||i.TABLE_NAME||' (COD_GRUPPO, COD_TIPO_ELEMENTO, '||i.COLUMN_NAME||', FLAG_ATTIVO, SEQ_ORD, DISATTIVATO) '||
                       'SELECT COD_GRUPPO, COD_TIPO_ELEMENTO, ''PPATCT'', FLAG_ATTIVO, SEQ_ORD, DISATTIVATO ' ||
                         'FROM GRUPPI_MISURA ' ||
                        'WHERE UPPER('||i.COLUMN_NAME||') = ''PPATMP'' ';
            DBMS_OUTPUT.PUT_LINE(i.TABLE_NAME||'.'||i.COLUMN_NAME||' - '||SQL%ROWCOUNT||' records inseriti (tipmis PPATCT) copiati da PPATMP)');
        ELSE
            EXECUTE IMMEDIATE 'UPDATE '||i.TABLE_NAME||' SET '||i.COLUMN_NAME||' = ''PPATCT'' WHERE UPPER('||i.COLUMN_NAME||') = ''PPATCT'' ';
            DBMS_OUTPUT.PUT_LINE(i.TABLE_NAME||'.'||i.COLUMN_NAME||' - '||SQL%ROWCOUNT||' records modificati (tipmis PPATCT)');
            EXECUTE IMMEDIATE 'UPDATE '||i.TABLE_NAME||' SET '||i.COLUMN_NAME||' = ''PPATMP'' WHERE UPPER('||i.COLUMN_NAME||') = ''PPT'' ';
            DBMS_OUTPUT.PUT_LINE(i.TABLE_NAME||'.'||i.COLUMN_NAME||' - '||SQL%ROWCOUNT||' records modificati (tipmis PPATMP)');
        END IF;
    END LOOP;
    
    COMMIT;

    FOR I IN (SELECT A.TABLE_NAME, A.CONSTRAINT_NAME, b.STATUS, A.COLUMN_NAME
                FROM USER_CONS_COLUMNS A 
               INNER JOIN USER_CONSTRAINTS B ON B.CONSTRAINT_NAME = A.CONSTRAINT_NAME
                                            AND B.OWNER = A.OWNER
               WHERE COLUMN_NAME LIKE '%COD_TIPO_MISURA%'
                 AND CONSTRAINT_TYPE = 'R'
--                 AND A.OWNER = 'MAGO'
               ORDER BY 1,2
             ) LOOP
        EXECUTE IMMEDIATE 'ALTER TABLE '|| i.TABLE_NAME || ' ENABLE CONSTRAINT ' || i.CONSTRAINT_NAME;
		DBMS_OUTPUT.PUT_LINE('CONSTRAINT '||i.CONSTRAINT_NAME||' di tabella '||i.TABLE_NAME||'  abilitato.');
    END LOOP;

END;     
/

  
