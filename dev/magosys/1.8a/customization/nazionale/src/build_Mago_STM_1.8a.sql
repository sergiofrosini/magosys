spool rel_18a_NAZ.log

conn magonaz/magonaz@arcdb2

UNDEF TBS;
DEFINE TBS='MAGONAZ';
UNDEF DAT;
DEFINE DAT='_DATA';
UNDEF IDX
DEFINE IDX='_IDX';
UNDEF IOT
DEFINE IOT='_IOT';

SET CONCAT |

SET ECHO OFF
SET TERMOUT ON

SET LINES 300
SET PAGES 2500

SET SERVEROUTPUT ON SIZE UNLIMITED FORMAT WORD_WRAPPED 

WHENEVER SQLERROR CONTINUE


PROMPT _______________________________________________________________________________________
PROMPT Tables
PROMPT
@./Tables/GTTD_MISURE.sql;
@./Tables/GTTD_FORECAST_ELEMENTS.sql;
@./Tables/ELEMENTI_CFG.sql;
@./Tables/TIPI_MISURA.sql;
@./Tables/DROP_INDEX_GERARCHIA.sql;
@./Tables/REL_ELEMENTI_ECP_SN.sql;
@./Tables/REL_ELEMENTI_ECP_SA.sql;
@./Tables/REL_ELEMENTI_ECS_SA.sql;
@./Tables/REL_ELEMENTI_ECS_SN.sql;
@./Tables/REL_ELEMENTI_AMM.sql;
@./Tables/REL_ELEMENTI_GEO.sql;
@./Tables/GERARCHIA_GEO.sql;
@./Tables/GERARCHIA_AMM.sql;
@./Tables/GERARCHIA_IMP_SN.sql;
@./Tables/GERARCHIA_IMP_SA.sql;

PROMPT _______________________________________________________________________________________
PROMPT Views
PROMPT
@./Views/V_SEARCH_ELEMENTS.sql;


PROMPT _______________________________________________________________________________________
PROMPT Packages 
PROMPT
@./Packages/PKG_GENERA_FILE_GEO.sql;
@./Packages/PKG_MAGO.sql;
@./Packages/PKG_ELEMENTI.sql;
@./Packages/PKG_MISURE.sql;
@./Packages/PKG_AGGREGAZIONI.sql;
@./Packages/PKG_SCHEDULER.sql;
@./Packages/PKG_INTEGRST.sql;
@./Packages/PKG_STATS.sql;
@./Packages/PKG_METEO.sql;

PROMPT _______________________________________________________________________________________
PROMPT Package Bodies 
PROMPT
@./PackageBodies/PKG_LOCALIZZA_GEO.sql;
@./PackageBodies/PKG_GENERA_FILE_GEO.sql;
@./PackageBodies/PKG_ELEMENTI.sql;
@./PackageBodies/PKG_MISURE.sql;
@./PackageBodies/PKG_AGGREGAZIONI.sql;
@./PackageBodies/PKG_SCHEDULER.sql;
@./PackageBodies/PKG_INTEGRST.sql;
@./PackageBodies/PKG_STATS.sql;
@./PackageBodies/PKG_METEO.sql;

PROMPT _______________________________________________________________________________________
PROMPT InitTables 
PROMPT

@./InitTables/METEO_JOB_STATIC_CONFIG.sql;
@./InitTables/TIPI_ELEMENTO.sql;
@./InitTables/TIPI_MISURA_CONV_ORIG.sql;
@./InitTables/TIPI_MISURA.sql;

@./InitTables/CorreggiDataDisattivazione.sql
@./InitTables/CalcolaDisattivazioneUndisconn.sql

@./InitTables/MEASURE_OFFLINE_PARAMETERS.sql
@./InitTables/VERSION.sql

PROMPT _______________________________________________________________________________________
PROMPT Grants MAGO to MAGONAZ
PROMPT
conn mago/mago
GRANT SELECT ON V_PUNTI_GEO TO MAGONAZ;

conn magonaz/magonaz

--PROMPT _______________________________________________________________________________________
--PROMPT Compilazione SCHEMA > DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER)
EXEC DBMS_UTILITY.COMPILE_SCHEMA(SCHEMA => USER);

PROMPT _______________________________________________________________________________________
PROMPT OGGETTI INVALIDI
COL OBJECT_NAME FORMAT A35
SELECT OBJECT_NAME, OBJECT_TYPE, STATUS FROM USER_OBJECTS WHERE STATUS <> 'VALID' AND OBJECT_TYPE <> 'MATERIALIZED VIEW';

PROMPT
PROMPT
PROMPT FINE UPGRADE 1.8.a
PROMPT

spool off

