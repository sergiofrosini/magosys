PROMPT Package Body PKG_LOCALIZZA_GEO;

CREATE OR REPLACE PACKAGE BODY PKG_LOCALIZZA_GEO AS
proc_name VARCHAR2(50);
--vLog         PKG_Mago.t_StandardLog;
err_code NUMBER;
err_msg VARCHAR2(250);
FUNCTION COORD_CONVERSION ( coord_value IN VARCHAR2, coord_type IN VARCHAR2, decimal_point IN VARCHAR2 DEFAULT '.' ) RETURN NUMBER AS
val NUMBER;
BEGIN
   CASE coord_type
      WHEN 'GPS' THEN
      /* Per convertire il formato GPS in WGS84 prendere i minuti e moltiplicarli per 60 */
         SELECT  SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                 +
                 TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = ''' ||decimal_point || '|''')  / 60 * SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ', 1)-1))
           INTO val
           FROM dual;
      WHEN 'DMS' THEN
         SELECT SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1)
                +
                (( TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ') +1, INSTR(TRIM(coord_value),' ', -1) - INSTR(TRIM(coord_value),' ') -1),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 60
                  + TO_NUMBER(SUBSTR(TRIM(coord_value), INSTR(TRIM(coord_value),' ', -1) +1, LENGTH(TRIM(coord_value))),'999D99999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''')  / 3600
                 )
                 *SIGN(SUBSTR(TRIM(coord_value),1, INSTR(TRIM(coord_value),' ')-1))
                )
           INTO val
           FROM dual;
      WHEN 'WGS84' THEN
         val := to_number(TRIM(coord_value),'999D999999999999999','NLS_NUMERIC_CHARACTERS = '''||decimal_point ||'|''');
      ELSE 
        val := to_number(TRIM(coord_value));
      END CASE;
RETURN (val);
END;
PROCEDURE LOAD_PUNTI AS
BEGIN
proc_name := 'PKG_LOCALIZZA_GEO.LOAD_PUNTI';
IF pkg_mago_utl.checkrun(proc_name) = 0 THEN 
   pkg_mago_utl.setrun(proc_name,1,0);
   COMMIT;
   INSERT INTO anagrafica_punti
   (cod_geo , tipo_coord, coordinata_x, coordinata_y, coordinata_h )
   SELECT NVL(cod_geo_p,cod_geo_a),tipo_coord, coordinata_x, coordinata_y, altitudine
   FROM (
         SELECT new_coord.* 
               ,CASE WHEN new_coord.tipo_coord ='P' THEN cod_geo_p + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_p
               ,CASE WHEN new_coord.tipo_coord ='A' THEN cod_geo_a + ROW_NUMBER() OVER ( PARTITION BY new_coord.tipo_coord ORDER BY new_coord.tipo_coord ) END cod_geo_a
           FROM
               (
                SELECT DISTINCT 'P' tipo_coord 
                       ,ROUND(coord_conversion(prod.coord_nord_p, prod.tipo_coord_p),7) coordinata_x
                       ,ROUND(coord_conversion(prod.coord_est_p, prod.tipo_coord_p),7) coordinata_y
                       ,prod.coord_up_p altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_p IS NOT NULL
                 UNION ALL
                SELECT DISTINCT 'A' tipo_coord
                       ,ROUND(coord_conversion(prod.coord_nord_a, prod.tipo_coord_a),7) coordinata_x
                       ,ROUND(coord_conversion(prod.coord_est_a, prod.tipo_coord_a),7) coordinata_y
                       ,prod.coord_up_a altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NOT NULL
                 UNION
                SELECT 'A' tipo_coord
                       ,ROUND(AVG(coord_conversion(prod.coord_nord_p, prod.tipo_coord_p)),7) coordinata_x
                       ,ROUND(AVG(coord_conversion(prod.coord_est_p, prod.tipo_coord_p)),7) coordinata_y
                       ,ROUND(AVG(prod.coord_up_p)) altitudine
                  FROM produttori_tmp prod
                  WHERE prod.tipo_coord_a IS NULL
                  GROUP BY NVL(prod.cg_produttore,prod.cod_gruppo)
            ) new_coord
               , anagrafica_punti old_coord
               ,( SELECT 
                      NVL(MAX(CASE WHEN TIPO_COORD = 'P' THEN COD_GEO END),20000) cod_geo_p 
                    ,NVL(MAX(CASE WHEN TIPO_COORD = 'A' THEN COD_GEO END),300000) cod_geo_a
                    FROM anagrafica_punti
               ) maxval
          WHERE new_coord.coordinata_x = old_coord.coordinata_x(+)
            AND new_coord.coordinata_y = old_coord.coordinata_y(+)
            AND new_coord.altitudine = old_coord.coordinata_h(+) 
            AND new_coord.tipo_coord = old_coord.tipo_coord(+)
            AND old_coord.cod_geo IS NULL
        );
   pkg_mago_utl.inslog(proc_name,'Procedure terminated',NULL);     
   pkg_mago_utl.setrun(proc_name,0,0);
   COMMIT;
ELSE
   pkg_mago_utl.inslog(proc_name,'Procedure running',99999);
   commit;
END IF;
exception when others then
     err_code := SQLCODE;
     err_msg := SQLERRM;
     ROLLBACK;
     pkg_mago_utl.inslog(proc_name,substr(err_msg,1,250),err_code);
     pkg_mago_utl.setrun(proc_name,0,1);
     COMMIT;
END LOAD_PUNTI;
END;
/

SHO ERRORS;
