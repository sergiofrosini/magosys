PROMPT PACKAGE BODY PKG_AGGREGAZIONI;
--
-- PKG_AGGREGAZIONI  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_AGGREGAZIONI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.8.a.4
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gcElabLinearizzazione CONSTANT INTEGER := 0;
 gcElebAggregazione    CONSTANT INTEGER := 1;

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Private

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE LinearizzaGerarchia  (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                 pData           IN DATE,
                                 pOrganizzazione IN NUMBER,
                                 pStato          IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Linearizza la gerarchia richiesta (Organizzazione e Stato ) per la data richiesta
-----------------------------------------------------------------------------------------------------------*/
    vLog    PKG_Logs.t_StandardLog;

    vRelazione  VARCHAR2(3);
    vGerarachia VARCHAR2(3);
    vStato1     VARCHAR2(3);
    vStato2     VARCHAR2(3);

    vTxt     VARCHAR2(100);

    vNumRec  NUMBER := 0;
    vTable   VARCHAR2(30);
    vCodEle  ELEMENTI.COD_ELEMENTO%TYPE;
    vCur     PKG_UtlGlb.t_query_cur;
    vLiv     INTEGER;
    vPrevLiv INTEGER := NULL;
    vEle     ELEMENTI.COD_ELEMENTO%TYPE;
    TYPE     t_tab IS VARRAY(20) OF NUMBER;
    vTab     t_tab;
    cMax     CONSTANT INTEGER := 13;

    vSql1    VARCHAR2(1500) :=
                      'SELECT eG.LIV + 1 liv,eG.COD_ELEMENTO_FIGLIO CODICE '                              ||
                        'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,LEVEL LIV,ROWNUM SEQ '       ||
                                'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                   ||
                                        'FROM REL_ELEMENTI_#TAB##STA1# '                                  ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '      || --pData
                                       'UNION ALL '                                                       ||
                                      'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                   ||
                                        'FROM REL_ELEMENTI_ECS#STA2# '                                    ||
                                       'WHERE :pDt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '     || --pData
                                         '#FILTRO_ELEM# '                                                 ||
                                     ') START WITH COD_ELEMENTO_PADRE = :Ele '                            || --pCodEle
                                     'CONNECT BY PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE '         ||
                              'UNION '                                                                    ||
                              'SELECT NULL,:Ele,0,0 FROM DUAL '                                           || --pCodEle
                             ') eG '                                                                      ||
                       'ORDER BY SEQ' ;

    vSqlFiltro VARCHAR2(1000) :=
                         'AND COD_ELEMENTO_PADRE '                                                        ||
                             'NOT IN (SELECT COD_ELEMENTO_PADRE FROM REL_ELEMENTI_ECS#STA2# '             ||
                                      'INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_PADRE '         ||
                                      'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '       ||
                                        'AND COD_TIPO_ELEMENTO='''||PKG_Mago.gcCabinaSecondaria||''' '    ||
                                     'MINUS '                                                             ||
                                     'SELECT COD_ELEMENTO FROM GERARCHIA_IMP#STA2# '                      ||
                                      'INNER JOIN ELEMENTI USING (COD_ELEMENTO) '                         ||
                                      'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '       ||
                                        'AND COD_TIPO_ELEMENTO='''||PKG_Mago.gcCabinaSecondaria||''' '    ||
                                    ') ';


    vSql2    VARCHAR2(1000) :=
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GTTD_CALC_GERARCHIA '                                                         ||
                    'MINUS '                                                                              ||
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GERARCHIA_#TAB##STA# '                                                        ||
                     'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE ';                          --pData
    vSql3    VARCHAR2(1000) :=
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GERARCHIA_#TAB##STA# '                                                        ||
                     'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '                        || --pData
                    'MINUS '                                                                              ||
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GTTD_CALC_GERARCHIA ';

    vRec     GTTD_CALC_GERARCHIA%ROWTYPE;
    vGL01    PKG_GestAnagr.t_DefAnagr;
    vGL02    PKG_GestAnagr.t_DefAnagr;
    vGL03    PKG_GestAnagr.t_DefAnagr;
    vGL04    PKG_GestAnagr.t_DefAnagr;
    vGL05    PKG_GestAnagr.t_DefAnagr;
    vGL06    PKG_GestAnagr.t_DefAnagr;
    vGL07    PKG_GestAnagr.t_DefAnagr;
    vGL08    PKG_GestAnagr.t_DefAnagr;
    vGL09    PKG_GestAnagr.t_DefAnagr;
    vGL10    PKG_GestAnagr.t_DefAnagr;
    vGL11    PKG_GestAnagr.t_DefAnagr;
    vGL12    PKG_GestAnagr.t_DefAnagr;
    vGL13    PKG_GestAnagr.t_DefAnagr;

 BEGIN

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE   THEN vTxt := 'Elettrica';
        WHEN PKG_Mago.gcOrganizzazGEO   THEN vTxt := 'Geografica';
        WHEN PKG_Mago.gcOrganizzazAMM   THEN vTxt := 'Amministrativa';
    END CASE;
    vTxt := vTxt || ' / ';
    CASE pStato
        WHEN PKG_Mago.gcStatoNormale    THEN vTxt := vTxt || 'Normale';
        WHEN PKG_Mago.gcStatoAttuale    THEN vTxt := vTxt || 'Attuale';
    END CASE;
    vLog := PKG_Logs.StdLogInit  (pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassLIN,
                                  pFunzione     => 'PKG_Aggregazioni.LinearizzaGerarchia',
                                  pDataRif      => pData,
                                  pTipo         => 'Organizzazione/Stato',
                                  pStoreOnFile  => FALSE,
                                  pForceLogFile => TRUE,
                                  pCodice       => vTxt);

    DELETE GTTD_CALC_GERARCHIA;

    IF pCodEle IS NULL THEN
        vCodEle := PKG_Elementi.GetElementoBase;
    ELSE
         vCodEle := pCodEle;
    END IF;
    CASE pStato
        WHEN PKG_Mago.gcStatoNormale  THEN vStato1 := '_SN';
        WHEN PKG_Mago.gcStatoAttuale  THEN vStato1 := '_SA';
    END CASE;
    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN vRelazione := 'ECP';
                                           vGerarachia := 'IMP';
                                           vSql1  := REPLACE(vSql1,'#TAB#', vRelazione);
                                           vTable := 'GERARCHIA_IMP#STA#';
                                           CASE pStato
                                               WHEN PKG_Mago.gcStatoNormale  THEN vStato2 := '_SN';
                                               WHEN PKG_Mago.gcStatoAttuale  THEN vStato2 := '_SA';
                                           END CASE;
                                           vSql1  := REPLACE(vSql1,'#FILTRO_ELEM#', '');
        WHEN PKG_Mago.gcOrganizzazGEO THEN vRelazione := 'GEO';
                                           vGerarachia := 'GEO';
                                           vSql1   := REPLACE(vSql1,'#FILTRO_ELEM#', vSqlFiltro);
                                           vSql1  := REPLACE(vSql1,'#TAB#', vRelazione);
                                           CASE pStato
                                               WHEN PKG_Mago.gcStatoNormale THEN vSql1 := REPLACE(vSql1,'#STA2#','_SN');
                                               WHEN PKG_Mago.gcStatoAttuale THEN vSql1 := REPLACE(vSql1,'#STA2#','_SA');
                                           END CASE;
                                           vSql1   := REPLACE(vSql1,'#STA1#','');
                                           vTable  := 'GERARCHIA_GEO';
                                           vStato2 := '';
        WHEN PKG_Mago.gcOrganizzazAMM THEN vRelazione := 'AMM';
                                           vGerarachia := 'AMM';
                                           vSql1  := REPLACE(vSql1,'#TAB#', vRelazione);
                                           vSql1   := REPLACE(vSql1,'#FILTRO_ELEM#', vSqlFiltro);
                                           CASE pStato
                                               WHEN PKG_Mago.gcStatoNormale THEN vSql1 := REPLACE(vSql1,'#STA2#','_SN');
                                               WHEN PKG_Mago.gcStatoAttuale THEN vSql1 := REPLACE(vSql1,'#STA2#','_SA');
                                           END CASE;
                                           vSql1   := REPLACE(vSql1,'#STA1#','');
                                           vTable  := 'GERARCHIA_AMM';
                                           vStato2 := '';
    END CASE;
    vSql1  := REPLACE(vSql1, '#STA1#',vStato1);
    vSql1  := REPLACE(vSql1, '#STA2#',vStato1);
    vTable := REPLACE(vTable,'#STA#' ,vStato1);

    vSql2  := REPLACE(vSql2, '#TAB#', vGerarachia);
    vSql2  := REPLACE(vSql2, '#STA#', vStato2);

    vSql3  := REPLACE(vSql3, '#TAB#', vGerarachia);
    vSql3  := REPLACE(vSql3, '#STA#', vStato2);

    PKG_GestAnagr.InitTab  (vGL01,pData,USER,vTable,'DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vGL01,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vGL01,'L01',          PKG_GestAnagr.cColAttributo);
    vGL02 := vGL01;
    PKG_GestAnagr.AddCol (vGL02,'L02', PKG_GestAnagr.cColAttributo);
    vGL03 := vGL02;
    PKG_GestAnagr.AddCol (vGL03,'L03', PKG_GestAnagr.cColAttributo);
    vGL04 := vGL03;
    PKG_GestAnagr.AddCol (vGL04,'L04', PKG_GestAnagr.cColAttributo);
    vGL05 := vGL04;
    PKG_GestAnagr.AddCol (vGL05,'L05', PKG_GestAnagr.cColAttributo);
    vGL06 := vGL05;
    PKG_GestAnagr.AddCol (vGL06,'L06', PKG_GestAnagr.cColAttributo);
    vGL07 := vGL06;
    PKG_GestAnagr.AddCol (vGL07,'L07', PKG_GestAnagr.cColAttributo);
    vGL08 := vGL07;
    PKG_GestAnagr.AddCol (vGL08,'L08', PKG_GestAnagr.cColAttributo);
    vGL09 := vGL08;
    PKG_GestAnagr.AddCol (vGL09,'L09', PKG_GestAnagr.cColAttributo);
    vGL10 := vGL09;
    PKG_GestAnagr.AddCol (vGL10,'L10', PKG_GestAnagr.cColAttributo);
    vGL11 := vGL10;
    PKG_GestAnagr.AddCol (vGL11,'L11', PKG_GestAnagr.cColAttributo);
    vGL12 := vGL11;
    PKG_GestAnagr.AddCol (vGL12,'L12', PKG_GestAnagr.cColAttributo);
    vGL13 := vGL12;
    PKG_GestAnagr.AddCol (vGL13,'L13', PKG_GestAnagr.cColAttributo);

    vNumRec := 0;
    vTab := t_tab();
    FOR i IN 1 .. cMax LOOP
        vTab.EXTEND;
        vTab(i) := -1;
    END LOOP;

    IF pOrganizzazione = PKG_Mago.gcOrganizzazELE THEN
        OPEN vCur FOR vSql1 USING pData, pData, vCodEle, vCodEle;
    ELSE
        OPEN vCur FOR vSql1 USING pData, pData, pData, pData, vCodEle, vCodEle;
    END IF;
    LOOP
        FETCH vCur INTO vLiv, vEle;
        EXIT WHEN vCur%NOTFOUND;
        FOR i IN vLiv + 1 .. cMax LOOP
            vTab(i) := -1;
        END LOOP;
        vTab(vLiv) := vEle;
        BEGIN
            INSERT INTO GTTD_CALC_GERARCHIA
                 VALUES (vEle
                        ,CASE vTab(01) WHEN -1 THEN NULL ELSE vTab(01) END
                        ,CASE vTab(02) WHEN -1 THEN NULL ELSE vTab(02) END
                        ,CASE vTab(03) WHEN -1 THEN NULL ELSE vTab(03) END
                        ,CASE vTab(04) WHEN -1 THEN NULL ELSE vTab(04) END
                        ,CASE vTab(05) WHEN -1 THEN NULL ELSE vTab(05) END
                        ,CASE vTab(06) WHEN -1 THEN NULL ELSE vTab(06) END
                        ,CASE vTab(07) WHEN -1 THEN NULL ELSE vTab(07) END
                        ,CASE vTab(08) WHEN -1 THEN NULL ELSE vTab(08) END
                        ,CASE vTab(09) WHEN -1 THEN NULL ELSE vTab(09) END
                        ,CASE vTab(10) WHEN -1 THEN NULL ELSE vTab(10) END
                        ,CASE vTab(11) WHEN -1 THEN NULL ELSE vTab(11) END
                        ,CASE vTab(12) WHEN -1 THEN NULL ELSE vTab(12) END
                        ,CASE vTab(13) WHEN -1 THEN NULL ELSE vTab(13) END
                        );
                vNumRec := vNumRec + 1;
        EXCEPTION
            WHEN DUP_VAL_ON_INDEX THEN NULL;
            WHEN OTHERS THEN RAISE;
        END;
    END LOOP;
    CLOSE vCur;
    PKG_Logs.StdLogAddTxt('Relazioni Elementi in Gerarchia        '||vNumRec,TRUE,NULL,vLog);
    vNumRec := 0;
    OPEN vCur FOR vSql2 USING pData;
    LOOP
        FETCH vCur INTO vRec;
        EXIT WHEN vCur%NOTFOUND;
        vNumRec := vNumRec + 1;
        IF vRec.L13 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL13);
            PKG_GestAnagr.AddVal (vGL13, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL13, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL13, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL13, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL13, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL13, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL13, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL13, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL13, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL13, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL13, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL13, 'L11', vRec.L11);
            PKG_GestAnagr.AddVal (vGL13, 'L12', vRec.L12);
            PKG_GestAnagr.AddVal (vGL13, 'L13', vRec.L13);
            PKG_GestAnagr.Elabora(vGL13);
        ELSIF vRec.L12 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL12);
            PKG_GestAnagr.AddVal (vGL12, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL12, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL12, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL12, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL12, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL12, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL12, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL12, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL12, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL12, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL12, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL12, 'L11', vRec.L11);
            PKG_GestAnagr.AddVal (vGL12, 'L12', vRec.L12);
            PKG_GestAnagr.Elabora(vGL12);
        ELSIF vRec.L11 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL11);
            PKG_GestAnagr.AddVal (vGL11, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL11, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL11, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL11, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL11, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL11, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL11, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL11, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL11, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL11, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL11, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL11, 'L11', vRec.L11);
            PKG_GestAnagr.Elabora(vGL11);
        ELSIF vRec.L10 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL10);
            PKG_GestAnagr.AddVal (vGL10, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL10, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL10, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL10, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL10, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL10, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL10, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL10, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL10, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL10, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL10, 'L10', vRec.L10);
            PKG_GestAnagr.Elabora(vGL10);
        ELSIF vRec.L09 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL09);
            PKG_GestAnagr.AddVal (vGL09, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL09, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL09, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL09, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL09, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL09, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL09, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL09, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL09, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL09, 'L09', vRec.L09);
            PKG_GestAnagr.Elabora(vGL09);
        ELSIF vRec.L08 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL08);
            PKG_GestAnagr.AddVal (vGL08, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL08, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL08, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL08, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL08, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL08, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL08, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL08, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL08, 'L08', vRec.L08);
            PKG_GestAnagr.Elabora(vGL08);
        ELSIF vRec.L07 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL07);
            PKG_GestAnagr.AddVal (vGL07, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL07, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL07, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL07, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL07, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL07, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL07, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL07, 'L07', vRec.L07);
            PKG_GestAnagr.Elabora(vGL07);
        ELSIF vRec.L06 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL06);
            PKG_GestAnagr.AddVal (vGL06, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL06, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL06, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL06, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL06, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL06, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL06, 'L06', vRec.L06);
            PKG_GestAnagr.Elabora(vGL06);
        ELSIF vRec.L05 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL05);
            PKG_GestAnagr.AddVal (vGL05, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL05, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL05, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL05, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL05, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL05, 'L05', vRec.L05);
            PKG_GestAnagr.Elabora(vGL05);
        ELSIF vRec.L04 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL04);
            PKG_GestAnagr.AddVal (vGL04, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL04, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL04, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL04, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL04, 'L04', vRec.L04);
            PKG_GestAnagr.Elabora(vGL04);
        ELSIF vRec.L03 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL03);
            PKG_GestAnagr.AddVal (vGL03, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL03, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL03, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL03, 'L03', vRec.L03);
            PKG_GestAnagr.Elabora(vGL03);
        ELSIF vRec.L02 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL02);
            PKG_GestAnagr.AddVal (vGL02, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL02, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL02, 'L02', vRec.L02);
            PKG_GestAnagr.Elabora(vGL02);
        ELSE
            PKG_GestAnagr.InitRow(vGL01);
            PKG_GestAnagr.AddVal (vGL01, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL01, 'L01', vRec.L01);
            PKG_GestAnagr.Elabora(vGL01);
        END IF;
    END LOOP;
    CLOSE vCur;
    PKG_Logs.StdLogAddTxt('Relazioni Elementi modificate/inserite '||vNumRec,TRUE,NULL,vLog);

    -- disattiva gli elementi di gerarchia lineare non piu' validi
    vNumRec := 0;
    OPEN vCur FOR vSql3 USING pData;
    LOOP
        FETCH vCur INTO vRec;
        EXIT WHEN vCur%NOTFOUND;
        vNumRec := vNumRec + 1;
        EXECUTE IMMEDIATE 'UPDATE GERARCHIA_'||vGerarachia||vStato2||' '                 ||
                             'SET DATA_DISATTIVAZIONE = :d1 - (1/86400) '                ||
                           'WHERE COD_ELEMENTO  = :e1 '                                  ||
                             'AND :d1 BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '
                    USING pData, vRec.COD_ELEMENTO, pData;
    END LOOP;
    CLOSE vCur;

    IF pOrganizzazione = Pkg_Mago.gcOrganizzazGEO THEN
       Pkg_Aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_GEO');
    ELSIF pOrganizzazione = Pkg_Mago.gcOrganizzazAMM THEN
       Pkg_Aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_AMM');
    ELSIF pOrganizzazione = Pkg_Mago.gcOrganizzazELE THEN
       IF pStato = PKG_Mago.gcStatoNormale THEN
          Pkg_Aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_IMP_SN');
       ELSE
          Pkg_Aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_IMP_SA');
     END IF;
    END IF;
    PKG_Logs.StdLogAddTxt('Relazioni Elementi eliminate           '||vNumRec,TRUE,NULL,vLog);
    PKG_Logs.StdLogPrint(vLog);

    --DELETE GTTD_CALC_GERARCHIA;

    --COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE,vLog);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END LinearizzaGerarchia;

 PROCEDURE LinearizzaGerarchia2   (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                   pData           IN DATE,
                                   pOrganizzazione IN NUMBER,
                                   pStato          IN NUMBER) AS
/*-----------------------------------------------------------------------------------------------------------
    Linearizza la gerarchia richiesta (Organizzazione e Stato ) per la data richiesta
-----------------------------------------------------------------------------------------------------------*/
    vLog    PKG_Logs.t_StandardLog;

    vRelazione  VARCHAR2(3);
    vGerarachia VARCHAR2(3);
    vStato1     VARCHAR2(3);
    vStato2     VARCHAR2(3);

    vTxt     VARCHAR2(100);

    vNumRec  NUMBER := 0;
    vTable   VARCHAR2(30);
    vCodEle  ELEMENTI.COD_ELEMENTO%TYPE := PKG_Elementi.GetElementoBase;
    vCur     PKG_UtlGlb.t_query_cur;
    vLiv     INTEGER;
    vPrevLiv INTEGER := NULL;
    vEle     ELEMENTI.COD_ELEMENTO%TYPE;
    TYPE     t_tab IS VARRAY(20) OF NUMBER;
    vTab     t_tab;
    cMax     CONSTANT INTEGER := 13;

    vSql1    VARCHAR2(3500) :=
                'INSERT INTO GTTD_CALC_GERARCHIA '                                                                                                           ||
                  'SELECT COD_ELEMENTO_FIGLIO '                                                                                                              ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,1)!=0 THEN INSTR(cod_el,''!'',1,1)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,2),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,2)-1)-(INSTR(cod_el,''!'',1,1)))) liv1'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,2)!=0 THEN INSTR(cod_el,''!'',1,2)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,3),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,3)-1)-(INSTR(cod_el,''!'',1,2)))) liv2'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,3)!=0 THEN INSTR(cod_el,''!'',1,3)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,4),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,4)-1)-(INSTR(cod_el,''!'',1,3)))) liv3'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,4)!=0 THEN INSTR(cod_el,''!'',1,4)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,5),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,5)-1)-(INSTR(cod_el,''!'',1,4)))) liv4'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,5)!=0 THEN INSTR(cod_el,''!'',1,5)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,6),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,6)-1)-(INSTR(cod_el,''!'',1,5)))) liv5'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,6)!=0 THEN INSTR(cod_el,''!'',1,6)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,7),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,7)-1)-(INSTR(cod_el,''!'',1,6)))) liv6'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,7)!=0 THEN INSTR(cod_el,''!'',1,7)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,8),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,8)-1)-(INSTR(cod_el,''!'',1,7)))) liv7'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,8)!=0 THEN INSTR(cod_el,''!'',1,8)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,9),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,9)-1)-(INSTR(cod_el,''!'',1,8)))) liv8'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,9)!=0 THEN INSTR(cod_el,''!'',1,9)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,10),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,10)-1)-(INSTR(cod_el,''!'',1,9)))) liv9'    ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,10)!=0 THEN INSTR(cod_el,''!'',1,10)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,11),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,11)-1)-(INSTR(cod_el,''!'',1,10)))) liv10'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,11)!=0 THEN INSTR(cod_el,''!'',1,11)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,12),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,12)-1)-(INSTR(cod_el,''!'',1,11)))) liv11'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,12)!=0 THEN INSTR(cod_el,''!'',1,12)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,13),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,13)-1)-(INSTR(cod_el,''!'',1,12)))) liv12'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,13)!=0 THEN INSTR(cod_el,''!'',1,13)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,14),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,14)-1)-(INSTR(cod_el,''!'',1,13)))) liv13 ' ||
                    'FROM (    SELECT SYS_CONNECT_BY_PATH (COD_ELEMENTO_figlio, ''!'') cod_el, COD_ELEMENTO_FIGLIO, LEVEL LIV, ROWNUM SEQ '                  ||
                                'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM REL_ELEMENTI_#TAB##STA1# '                                        ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '                                                         ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM REL_ELEMENTI_#TAB##STA2# '                                        ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '                                                         ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT NULL, :el FROM DUAL '                                                                                          ||
                                     ')'                                                                                                                     ||
                          'START WITH COD_ELEMENTO_PADRE IS NULL '                                                                                           ||
                          'CONNECT BY PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE)';

    vSql2    VARCHAR2(1000) :=
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GTTD_CALC_GERARCHIA '                                                         ||
                    'MINUS '                                                                              ||
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GERARCHIA_#TAB##STA# '                                                        ||
                     'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE ';                          --pData
    vSql3    VARCHAR2(1000) :=
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GERARCHIA_#TAB##STA# '                                                        ||
                     'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '                        || --pData
                    'MINUS '                                                                              ||
                    'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '            ||
                      'FROM GTTD_CALC_GERARCHIA ';

    vRec     GTTD_CALC_GERARCHIA%ROWTYPE;
    vGL01    PKG_GestAnagr.t_DefAnagr;
    vGL02    PKG_GestAnagr.t_DefAnagr;
    vGL03    PKG_GestAnagr.t_DefAnagr;
    vGL04    PKG_GestAnagr.t_DefAnagr;
    vGL05    PKG_GestAnagr.t_DefAnagr;
    vGL06    PKG_GestAnagr.t_DefAnagr;
    vGL07    PKG_GestAnagr.t_DefAnagr;
    vGL08    PKG_GestAnagr.t_DefAnagr;
    vGL09    PKG_GestAnagr.t_DefAnagr;
    vGL10    PKG_GestAnagr.t_DefAnagr;
    vGL11    PKG_GestAnagr.t_DefAnagr;
    vGL12    PKG_GestAnagr.t_DefAnagr;
    vGL13    PKG_GestAnagr.t_DefAnagr;

 BEGIN

    IF pCodEle IS NULL THEN
        vCodEle := PKG_Elementi.GetElementoBase;
    ELSE
         vCodEle := pCodEle;
    END IF;

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE   THEN vTxt := 'Elettrica';
        WHEN PKG_Mago.gcOrganizzazGEO   THEN vTxt := 'Geografica';
        WHEN PKG_Mago.gcOrganizzazAMM   THEN vTxt := 'Amministrativa';
    END CASE;
    vTxt := vTxt || ' / ';
    CASE pStato
        WHEN PKG_Mago.gcStatoNormale    THEN vTxt := vTxt || 'Normale';
        WHEN PKG_Mago.gcStatoAttuale    THEN vTxt := vTxt || 'Attuale';
    END CASE;

    vLog := PKG_Logs.StdLogInit  (pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassLIN,
                                  pFunzione     => 'PKG_Aggregazioni.LinearizzaGerarchia',
                                  pDataRif      => pData,
                                  pTipo         => 'Organizzazione/Stato',
                                  pStoreOnFile  => FALSE,
                                  pForceLogFile => TRUE,
                                  pCodice       => vTxt);

    DELETE GTTD_CALC_GERARCHIA;

    CASE pStato
        WHEN PKG_Mago.gcStatoNormale  THEN vStato1 := '_SN';
        WHEN PKG_Mago.gcStatoAttuale  THEN vStato1 := '_SA';
    END CASE;
    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN vRelazione := 'ECP';
                                           vGerarachia := 'IMP';
                                           vSql1  := REPLACE(vSql1,'#TAB#', vRelazione);
                                           vTable := 'GERARCHIA_IMP#STA#';
                                           CASE pStato
                                               WHEN PKG_Mago.gcStatoNormale  THEN vStato2 := '_SN';
                                               WHEN PKG_Mago.gcStatoAttuale  THEN vStato2 := '_SA';
                                           END CASE;
        WHEN PKG_Mago.gcOrganizzazGEO THEN vRelazione := 'GEO';
                                           vGerarachia := 'GEO';
                                           vSql1   := REPLACE(vSql1,'#TAB#', vRelazione);
                                           vSql1   := REPLACE(vSql1,'#STA1#','');
                                           vTable  := 'GERARCHIA_GEO';
                                           vStato2 := '';
        WHEN PKG_Mago.gcOrganizzazAMM THEN vRelazione := 'AMM';
                                           vGerarachia := 'AMM';
                                           vSql1   := REPLACE(vSql1,'#TAB#', vRelazione);
                                           vSql1   := REPLACE(vSql1,'#STA1#','');
                                           vTable  := 'GERARCHIA_AMM';
                                           vStato2 := '';
    END CASE;
    vSql1  := REPLACE(vSql1, '#STA1#',vStato1);
    vSql1  := REPLACE(vSql1, '#STA2#',vStato1);
    vTable := REPLACE(vTable,'#STA#' ,vStato1);

    vSql2  := REPLACE(vSql2, '#TAB#', vGerarachia);
    vSql2  := REPLACE(vSql2, '#STA#', vStato2);

    vSql3  := REPLACE(vSql3, '#TAB#', vGerarachia);
    vSql3  := REPLACE(vSql3, '#STA#', vStato2);

    PKG_GestAnagr.InitTab  (vGL01,pData,USER,vTable,'DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE');
    PKG_GestAnagr.AddCol   (vGL01,'COD_ELEMENTO', PKG_GestAnagr.cColChiave);
    PKG_GestAnagr.AddCol   (vGL01,'L01',          PKG_GestAnagr.cColAttributo);
    vGL02 := vGL01;
    PKG_GestAnagr.AddCol (vGL02,'L02', PKG_GestAnagr.cColAttributo);
    vGL03 := vGL02;
    PKG_GestAnagr.AddCol (vGL03,'L03', PKG_GestAnagr.cColAttributo);
    vGL04 := vGL03;
    PKG_GestAnagr.AddCol (vGL04,'L04', PKG_GestAnagr.cColAttributo);
    vGL05 := vGL04;
    PKG_GestAnagr.AddCol (vGL05,'L05', PKG_GestAnagr.cColAttributo);
    vGL06 := vGL05;
    PKG_GestAnagr.AddCol (vGL06,'L06', PKG_GestAnagr.cColAttributo);
    vGL07 := vGL06;
    PKG_GestAnagr.AddCol (vGL07,'L07', PKG_GestAnagr.cColAttributo);
    vGL08 := vGL07;
    PKG_GestAnagr.AddCol (vGL08,'L08', PKG_GestAnagr.cColAttributo);
    vGL09 := vGL08;
    PKG_GestAnagr.AddCol (vGL09,'L09', PKG_GestAnagr.cColAttributo);
    vGL10 := vGL09;
    PKG_GestAnagr.AddCol (vGL10,'L10', PKG_GestAnagr.cColAttributo);
    vGL11 := vGL10;
    PKG_GestAnagr.AddCol (vGL11,'L11', PKG_GestAnagr.cColAttributo);
    vGL12 := vGL11;
    PKG_GestAnagr.AddCol (vGL12,'L12', PKG_GestAnagr.cColAttributo);
    vGL13 := vGL12;
    PKG_GestAnagr.AddCol (vGL13,'L13', PKG_GestAnagr.cColAttributo);

    EXECUTE IMMEDIATE vSql1 USING  pData, pData, vCodEle;
    PKG_Logs.StdLogAddTxt('Relazioni Elementi in Gerarchia        '||vNumRec,TRUE,NULL);

    vNumRec := 0;
    OPEN vCur FOR vSql2 USING pData;
    LOOP
        FETCH vCur INTO vRec;
        EXIT WHEN vCur%NOTFOUND;
        vNumRec := vNumRec + 1;
        IF vRec.L13 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL13);
            PKG_GestAnagr.AddVal (vGL13, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL13, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL13, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL13, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL13, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL13, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL13, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL13, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL13, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL13, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL13, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL13, 'L11', vRec.L11);
            PKG_GestAnagr.AddVal (vGL13, 'L12', vRec.L12);
            PKG_GestAnagr.AddVal (vGL13, 'L13', vRec.L13);
            PKG_GestAnagr.Elabora(vGL13);
        ELSIF vRec.L12 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL12);
            PKG_GestAnagr.AddVal (vGL12, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL12, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL12, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL12, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL12, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL12, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL12, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL12, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL12, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL12, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL12, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL12, 'L11', vRec.L11);
            PKG_GestAnagr.AddVal (vGL12, 'L12', vRec.L12);
            PKG_GestAnagr.Elabora(vGL12);
        ELSIF vRec.L11 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL11);
            PKG_GestAnagr.AddVal (vGL11, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL11, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL11, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL11, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL11, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL11, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL11, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL11, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL11, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL11, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL11, 'L10', vRec.L10);
            PKG_GestAnagr.AddVal (vGL11, 'L11', vRec.L11);
            PKG_GestAnagr.Elabora(vGL11);
        ELSIF vRec.L10 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL10);
            PKG_GestAnagr.AddVal (vGL10, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL10, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL10, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL10, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL10, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL10, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL10, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL10, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL10, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL10, 'L09', vRec.L09);
            PKG_GestAnagr.AddVal (vGL10, 'L10', vRec.L10);
            PKG_GestAnagr.Elabora(vGL10);
        ELSIF vRec.L09 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL09);
            PKG_GestAnagr.AddVal (vGL09, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL09, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL09, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL09, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL09, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL09, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL09, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL09, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL09, 'L08', vRec.L08);
            PKG_GestAnagr.AddVal (vGL09, 'L09', vRec.L09);
            PKG_GestAnagr.Elabora(vGL09);
        ELSIF vRec.L08 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL08);
            PKG_GestAnagr.AddVal (vGL08, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL08, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL08, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL08, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL08, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL08, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL08, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL08, 'L07', vRec.L07);
            PKG_GestAnagr.AddVal (vGL08, 'L08', vRec.L08);
            PKG_GestAnagr.Elabora(vGL08);
        ELSIF vRec.L07 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL07);
            PKG_GestAnagr.AddVal (vGL07, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL07, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL07, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL07, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL07, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL07, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL07, 'L06', vRec.L06);
            PKG_GestAnagr.AddVal (vGL07, 'L07', vRec.L07);
            PKG_GestAnagr.Elabora(vGL07);
        ELSIF vRec.L06 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL06);
            PKG_GestAnagr.AddVal (vGL06, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL06, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL06, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL06, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL06, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL06, 'L05', vRec.L05);
            PKG_GestAnagr.AddVal (vGL06, 'L06', vRec.L06);
            PKG_GestAnagr.Elabora(vGL06);
        ELSIF vRec.L05 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL05);
            PKG_GestAnagr.AddVal (vGL05, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL05, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL05, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL05, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL05, 'L04', vRec.L04);
            PKG_GestAnagr.AddVal (vGL05, 'L05', vRec.L05);
            PKG_GestAnagr.Elabora(vGL05);
        ELSIF vRec.L04 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL04);
            PKG_GestAnagr.AddVal (vGL04, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL04, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL04, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL04, 'L03', vRec.L03);
            PKG_GestAnagr.AddVal (vGL04, 'L04', vRec.L04);
            PKG_GestAnagr.Elabora(vGL04);
        ELSIF vRec.L03 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL03);
            PKG_GestAnagr.AddVal (vGL03, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL03, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL03, 'L02', vRec.L02);
            PKG_GestAnagr.AddVal (vGL03, 'L03', vRec.L03);
            PKG_GestAnagr.Elabora(vGL03);
        ELSIF vRec.L02 IS NOT NULL THEN
            PKG_GestAnagr.InitRow(vGL02);
            PKG_GestAnagr.AddVal (vGL02, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL02, 'L01', vRec.L01);
            PKG_GestAnagr.AddVal (vGL02, 'L02', vRec.L02);
            PKG_GestAnagr.Elabora(vGL02);
        ELSE
            PKG_GestAnagr.InitRow(vGL01);
            PKG_GestAnagr.AddVal (vGL01, 'COD_ELEMENTO', vRec.COD_ELEMENTO );
            PKG_GestAnagr.AddVal (vGL01, 'L01', vRec.L01);
            PKG_GestAnagr.Elabora(vGL01);
        END IF;
    END LOOP;
    CLOSE vCur;
    PKG_Logs.StdLogAddTxt('Relazioni Elementi modificate/inserite '||vNumRec,TRUE,NULL);

    -- disattiva gli elementi di gerarchia lineare non piu' validi
    vNumRec := 0;
    OPEN vCur FOR vSql3 USING pData;
    LOOP
        FETCH vCur INTO vRec;
        EXIT WHEN vCur%NOTFOUND;
        vNumRec := vNumRec + 1;
        EXECUTE IMMEDIATE 'UPDATE GERARCHIA_'||vGerarachia||vStato2||' '                   ||
                             'SET DATA_DISATTIVAZIONE = :d1 - (1/86400) '                  ||
                           'WHERE COD_ELEMENTO  = :e1 '                                    ||
                             'AND :d1 BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '
                    USING pData, vRec.COD_ELEMENTO, pData;
    END LOOP;
    CLOSE vCur;

    PKG_Logs.StdLogAddTxt('Relazioni Elementi eliminate           '||vNumRec,TRUE,NULL);
    PKG_Logs.StdLogPrint ;

--    DELETE GTTD_CALC_GERARCHIA;

    --COMMIT;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (vLog,PKG_UtlGlb.gcTrace_ERR);
         RAISE;

 END LinearizzaGerarchia2;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetSqlAggregazione    (pOrganizzazione IN INTEGER,
                                 pStato          IN INTEGER,
                                 pMisureStatiche IN BOOLEAN,
                                 pCalcGerarchia  IN BOOLEAN,
                                 pDisconnect IN INTEGER DEFAULT 0) RETURN VARCHAR2 AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna lo statement per il calcolo degli aggregati
-----------------------------------------------------------------------------------------------------------*/
    p1          NUMBER;
    p2          NUMBER;

    vSqlGer     VARCHAR2(500) := 'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '       ||
                                   'FROM (SELECT g.*, #DATAELECALC# data_disattivazione_calc FROM GERARCHIA_#GER##STA# G ) '  ||
                                   'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ';                     -- pData
    vSqlGer2    VARCHAR2(500) := 'SELECT COD_ELEMENTO,L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '       ||
                                  'FROM (SELECT g.*, #DATAELECALC# data_disattivazione_calc FROM GERARCHIA_#GER##STA# G ) ' ||
                                  ' INNER JOIN ELEMENTI USING(COD_ELEMENTO) '                                      ||
                                  ' INNER JOIN TIPI_ELEMENTO USING (COD_TIPO_ELEMENTO) '                           ||
                                  ' WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '              || -- pData
                                    'AND CASE '||pOrganizzazione||' '                                             ||
                                             'WHEN '||PKG_Mago.gcOrganizzazGEO||' THEN '                          ||
                                                 'CASE WHEN GER_GEO = 1 OR GER_ECS = 1 THEN 1 '                   ||
                                                      'ELSE 0 '                                                   ||
                                                 'END '                                                           ||
                                             'WHEN '||PKG_Mago.gcOrganizzazAMM||' THEN '                          ||
                                                 'CASE WHEN GER_AMM = 1 OR GER_ECS = 1 THEN 1 '                   ||
                                                      'ELSE 0 '                                                   ||
                                                 'END '                                                           ||
                                        'END = 1 ' ;

    vSql        VARCHAR2(4000) := 'WITH MyGer AS (#GERARCHIA#),'                                                            || 
                                       'MyMis AS (#MISURE#) '                                                               ||
                                  'SELECT DATA,'                                                                            ||
                                         'CASE WHEN L13 IS NOT NULL THEN L13 '                                              ||
                                              'WHEN L12 IS NOT NULL THEN L12 '                                              ||
                                              'WHEN L11 IS NOT NULL THEN L11 '                                              ||
                                              'WHEN L10 IS NOT NULL THEN L10 '                                              ||
                                              'WHEN L09 IS NOT NULL THEN L09 '                                              ||
                                              'WHEN L08 IS NOT NULL THEN L08 '                                              ||
                                              'WHEN L07 IS NOT NULL THEN L07 '                                              ||
                                              'WHEN L06 IS NOT NULL THEN L06 '                                              ||
                                              'WHEN L05 IS NOT NULL THEN L05 '                                              ||
                                              'WHEN L04 IS NOT NULL THEN L04 '                                              ||
                                              'WHEN L03 IS NOT NULL THEN L03 '                                              ||
                                              'WHEN L02 IS NOT NULL THEN L02 '                                              ||
                                              'WHEN L01 IS NOT NULL THEN L01 '                                              ||
                                         'END COD_ELEMENTO,'                                                                ||
                                         'COD_TIPO_MISURA,'                                                                 ||
                                         'COD_TIPO_FONTE,'                                                                  ||
                                         'COD_TIPO_RETE,'                                                                   ||
                                         'COD_TIPO_CLIENTE,'                                                                ||
                                         'SUM(VALORE) VALORE '                                                              ||
                                    'FROM MyGer A '                                                                         ||
                                   'INNER JOIN MyMis M USING (COD_ELEMENTO) '                                               ||
                                   'GROUP BY ROLLUP '                                                                       ||
                                         '((L01,DATA,COD_TIPO_MISURA,COD_TIPO_FONTE,COD_TIPO_RETE,COD_TIPO_CLIENTE),'       ||
                                           'L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13) '                              ||
                                   'HAVING CASE '                                                                           ||
                                               'WHEN GROUPING(L01)=1 THEN 1 '                                               ||
                                               'WHEN GROUPING(L02)=1 THEN 2 '                                               ||
                                               'WHEN GROUPING(L03)=1 THEN 3 '                                               ||
                                               'WHEN GROUPING(L04)=1 THEN 4 '                                               ||
                                               'WHEN GROUPING(L05)=1 THEN 5 '                                               ||
                                               'WHEN GROUPING(L06)=1 THEN 6 '                                               ||
                                               'WHEN GROUPING(L07)=1 THEN 7 '                                               ||
                                               'WHEN GROUPING(L08)=1 THEN 8 '                                               ||
                                               'WHEN GROUPING(L09)=1 THEN 9 '                                               ||
                                               'WHEN GROUPING(L10)=1 THEN 10 '                                              ||
                                               'WHEN GROUPING(L11)=1 THEN 11 '                                              ||
                                               'WHEN GROUPING(L12)=1 THEN 12 '                                              ||
                                               'WHEN GROUPING(L13)=1 THEN 13 '                                              ||
                                          'END - 1 = CASE WHEN L13 IS NOT NULL THEN 13 '                                    ||
                                                         'WHEN L12 IS NOT NULL THEN 12 '                                    ||
                                                         'WHEN L11 IS NOT NULL THEN 11 '                                    ||
                                                         'WHEN L10 IS NOT NULL THEN 10 '                                    ||
                                                         'WHEN L09 IS NOT NULL THEN 9 '                                     ||
                                                         'WHEN L08 IS NOT NULL THEN 8 '                                     ||
                                                         'WHEN L07 IS NOT NULL THEN 7 '                                     ||
                                                         'WHEN L06 IS NOT NULL THEN 6 '                                     ||
                                                         'WHEN L05 IS NOT NULL THEN 5 '                                     ||
                                                         'WHEN L04 IS NOT NULL THEN 4 '                                     ||
                                                         'WHEN L03 IS NOT NULL THEN 3 '                                     ||
                                                         'WHEN L02 IS NOT NULL THEN 2 '                                     ||
                                                         'WHEN L01 IS NOT NULL THEN 1 '                                     ||
                                                    'END';

    vSqlMisCalc VARCHAR2(3000) := 'WITH MyGer AS (SELECT * '                                                                ||
                                                 'FROM TABLE(PKG_Elementi.LeggiGerarchiaLineare(:dt,:dt,:org,:st,:ele/*,disconnect */))'  ||
                                                 '),'                                                                       || 
                                       'MyMis AS (#MISURE#) '                                                               ||
                                  'SELECT DATA,L01 COD_ELEMENTO,COD_TIPO_MISURA,COD_TIPO_FONTE,COD_TIPO_RETE,'              ||
                                         'COD_TIPO_CLIENTE, SUM (VALORE) VALORE '                                           ||
                                    'FROM MyGer A '                                                                         ||
                                   'INNER JOIN MyMis M USING (COD_ELEMENTO) '                                               ||
                                   'WHERE COD_ELEMENTO = :ele '                                                             ||
                                   'GROUP BY GROUPING SETS '                                                                ||
                                           '((DATA,COD_TIPO_MISURA,COD_TIPO_FONTE,COD_TIPO_RETE,COD_TIPO_CLIENTE,L01))';

    vMisBase   VARCHAR2(1000)  := 'SELECT COD_TRATTAMENTO_ELEM,tr.COD_ELEMENTO,COD_TIPO_MISURA,'              ||                            
                                        'COD_TIPO_FONTE,COD_TIPO_CLIENTE,COD_TIPO_RETE,DATA, ' ||
                                        'CASE WHEN (tmis.FLG_MIS_CARICO = 1 AND NVL(cfg.FLG_PG,0) = 1) ' ||
                                     '        OR (tmis.FLG_MIS_CARICO = 1 AND NVL(FLG_PI,0) = 1) THEN 0 ELSE VALORE END VALORE ' ||                     
                                   'FROM MISURE_ACQUISITE m '                                               ||
                                  'INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM) '          ||
                                  'INNER JOIN TIPI_MISURA tmis USING (COD_TIPO_MISURA)         '          ||
                                  'LEFT OUTER JOIN ELEMENTI_CFG cfg ON (tr.COD_ELEMENTO = cfg.COD_ELEMENTO ' ||
                                         'AND :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) ' ||
                                  'INNER JOIN (SELECT ALF1 COD_TIPO_MISURA '                              ||
                                                'FROM GTTD_VALORI_TEMP '                                  ||
                                               'WHERE TIP = '''||PKG_Mago.gcTmpTipMisKey||''' '           ||
                                             ') A USING (COD_TIPO_MISURA) '                               ||
                                  'WHERE DATA = :dt '                                                     || -- Data
                                    'AND TIPO_AGGREGAZIONE = '||PKG_Mago.gcStatoNullo||' ';                                                     

    vMisStat   VARCHAR2(1000)  := 'SELECT COD_TRATTAMENTO_ELEM,tr.COD_ELEMENTO,COD_TIPO_MISURA,'              ||
                                        'COD_TIPO_FONTE,COD_TIPO_CLIENTE,COD_TIPO_RETE,:dt DATA,'  || -- Data
                                     ' CASE WHEN (tmis.FLG_MIS_CARICO = 1 AND NVL(cfg.FLG_PG,0) = 1) ' ||
                                     '        OR (tmis.FLG_MIS_CARICO = 1 AND NVL(FLG_PI,0) = 1) THEN 0 ELSE VALORE END VALORE ' ||
                                   'FROM (SELECT m.*, #DATATRCALC# data_disattivazione_calc FROM MISURE_ACQUISITE_STATICHE M ) M ' ||
                                  'INNER JOIN TRATTAMENTO_ELEMENTI tr USING(COD_TRATTAMENTO_ELEM) '          ||
                                  'INNER JOIN TIPI_MISURA tmis USING (COD_TIPO_MISURA)         '          ||
                                  'LEFT OUTER JOIN ELEMENTI_CFG cfg ON (tr.COD_ELEMENTO = cfg.COD_ELEMENTO ' ||
                                         'AND :dt BETWEEN cfg.DATA_ATTIVAZIONE AND cfg.DATA_DISATTIVAZIONE) ' ||
                                  'INNER JOIN (SELECT ALF1 COD_TIPO_MISURA '                              ||
                                                'FROM GTTD_VALORI_TEMP '                                  ||
                                               'WHERE TIP = '''||PKG_Mago.gcTmpTipMisKey||''' '           ||
                                             ') A USING (COD_TIPO_MISURA) '                               ||
                                  'WHERE :dt BETWEEN m.DATA_ATTIVAZIONE AND m.DATA_DISATTIVAZIONE_CALC '           || -- Data
                                    'AND TIPO_AGGREGAZIONE = '||PKG_Mago.gcStatoNullo||' ';                                                     

 BEGIN

    IF pCalcGerarchia THEN
        vSql := vSqlMisCalc;
        vSql := 'SELECT * FROM ('||vSql||') WHERE COD_ELEMENTO IN (SELECT NUM1 FROM GTTD_VALORI_TEMP WHERE TIP = '''||PKG_Mago.gcTmpTipEleKey||''')';
    ELSE        
        IF NOT PKG_Mago.IsMagoDGF THEN
            IF pOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
                --in caso di organizzazione Istat (GEO) o Amministrativa (AMM) o Elettrica In Stato Normale 
                --si seleziona la gerarchia fino al livello di SCS e le misure Base saranno le aggregate di SCS 
                vSqlGer := vSqlGer2; 
            END IF;
        END IF;
        -- dispone l'utilizzo della gerarchia appropriata
        CASE pOrganizzazione
            WHEN PKG_Mago.gcOrganizzazELE THEN vSqlGer     := REPLACE(vSqlGer,'#GER#','IMP');
            WHEN PKG_Mago.gcOrganizzazGEO THEN vSqlGer     := REPLACE(vSqlGer,'#GER#','GEO');
                                               vSqlGer     := REPLACE(vSqlGer,'#STA#','');
            WHEN PKG_Mago.gcOrganizzazAMM THEN vSqlGer     := REPLACE(vSqlGer,'#GER#','AMM');
                                               vSqlGer     := REPLACE(vSqlGer,'#STA#','');
        END CASE;
        CASE pStato
            WHEN PKG_Mago.gcStatoNormale  THEN vSqlGer     := REPLACE(vSqlGer,'#STA#','_SN');
            WHEN PKG_Mago.gcStatoAttuale  THEN vSqlGer     := REPLACE(vSqlGer,'#STA#','_SA');
        END CASE;
        vSql := REPLACE(vSql,'#GERARCHIA#',vSqlGer);
    END IF;

    IF pMisureStatiche THEN 
        vSql := REPLACE (vSql,'#MISURE#',vMisStat);
    ELSE
        vSql := REPLACE (vSql,'#MISURE#',vMisBase);
    END IF;
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(lead(data_attivazione) over ( partition by cod_elemento order by data_attivazione ),to_date(''01013000'',''ddmmyyyy''))' );
       vSql := REPLACE (vSql, '#DATATRCALC#' , ' nvl(lead(data_attivazione) over ( partition by cod_trattamento_elem order by data_attivazione ),to_date(''01013000'',''ddmmyyyy''))' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );    
       vSql := REPLACE (vSql, '#DATATRCALC#' , ' data_disattivazione ' );
    END IF;
    RETURN PKG_UtlGlb.CompattaSelect(vSql);

 END GetSqlAggregazione;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

 Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION CalcAggregazioniAlVolo(pDataDa           IN DATE,
                                 pDataA            IN DATE,
                                 pOrganizzazione   IN INTEGER,
                                 pStatoRete        IN INTEGER,
                                 pCodEle           IN ELEMENTI.COD_ELEMENTO%TYPE)
                          RETURN PKG_Aggregazioni.t_TabAggrMis PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il formato tabella (PIPELINE) le aggregazioni calcolate al volo per un singolo elemento
    (non lavora su tipi musura 'statici')
-----------------------------------------------------------------------------------------------------------*/
   vCurAggr     PKG_UtlGlb.t_query_cur;
   vRowAggr     t_RowAggrMis;
   vSqlAggr     VARCHAR2(5000) := GetSqlAggregazione(pOrganizzazione,pStatoRete,FALSE,TRUE /*,DISCONNECT NON GESTITO PER LE AGGREGAZIONI ( AGGREGA SOLO ELEMENTI IN GERARCHIA )*/);
 BEGIN
    OPEN vCurAggr FOR vSqlAggr USING pDataDa,pDataA,pOrganizzazione,pStatoRete,pCodEle,
                                     pDataDa,pDataA,pCodEle;
    LOOP
        FETCH vCurAggr INTO vRowAggr;
        EXIT WHEN vCurAggr%NOTFOUND;
        PIPE ROW(vRowAggr);
    END LOOP;
    CLOSE vCurAggr;
 END CalcAggregazioniAlVolo;



 PROCEDURE EseguiAggregazione   (pOrganizzazione IN NUMBER,
                                 pStato          IN NUMBER,
                                 pData           IN DATE,
                                 pMisStat        IN INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Esegue l'aggregazione per le misure comprese nel perioodo ricevuto
-----------------------------------------------------------------------------------------------------------*/

    vLog         PKG_Logs.t_StandardLog;

    vSqlAggr     VARCHAR2(5000);

    vSqlMisStat  VARCHAR2(1500) := 'INSERT INTO GTTD_MISURE(COD_TRATTAMENTO_ELEM,COD_ELEMENTO,VALORE,VALORE2,DATA) ' ||
                                       'SELECT COD_TRATTAMENTO_ELEM,COD_ELEMENTO,VALORE,:v,DATA_ATTIVAZIONE '        || -- valore
                                         'FROM (SELECT COD_TRATTAMENTO_ELEM,VALORE,DATA_ATTIVAZIONE '                ||
                                                 'FROM MISURE_AGGREGATE_STATICHE '                                   ||
                                                'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE) '       || -- pData
                                        'INNER JOIN '                                                                ||
                                              '(SELECT COD_TRATTAMENTO_ELEM,COD_ELEMENTO '                           ||
                                                 'FROM TRATTAMENTO_ELEMENTI '                                        ||
                                                'INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO) '                 ||
                                                'WHERE COD_TIPO_MISURA IN '                                          ||
                                                         '(SELECT ALF1 COD_TIPO_MISURA FROM GTTD_VALORI_TEMP '       ||
                                                           'WHERE TIP = '''||PKG_Mago.gcTmpTipMisKey||''') '         ||
                                                  'AND GER_ECS = '||PKG_UtlGlb.gkFlagOFF||' '                        ||
                                                  'AND ORGANIZZAZIONE = :o '                                         || -- pOrganizzazione
                                                  'AND TIPO_AGGREGAZIONE = :s '                                      || -- pStato
                                               '#MIS_GER_ECS# '                                                      ||
                                              ') USING(COD_TRATTAMENTO_ELEM) ';

    vSqlMisEcs VARCHAR2(300)    := 'UNION ALL '                                                                      ||
                                   'SELECT COD_TRATTAMENTO_ELEM,COD_ELEMENTO '                                       ||
                                     'FROM TRATTAMENTO_ELEMENTI '                                                    ||
                                    'INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO) '                             ||
                                    'WHERE COD_TIPO_MISURA IN '                                                      ||
                                             '(SELECT ALF1 COD_TIPO_MISURA FROM GTTD_VALORI_TEMP '                   ||
                                               'WHERE TIP = '''||PKG_Mago.gcTmpTipMisKey||''') '                     ||
                                      'AND GER_ECS = '||PKG_UtlGlb.gkFlagON||' '                                     ||
                                      'AND ORGANIZZAZIONE = :o '                                                     ||
                                      'AND TIPO_AGGREGAZIONE = :s ';

    vCurAggr     PKG_UtlGlb.t_query_cur;
    vNumTot      INTEGER := 0;
    vNumIni      INTEGER := 0;
    vNumIns      INTEGER := 0;
    vNumMod      INTEGER := 0;
    vNumPrs      INTEGER := 0;
    vNumDel      INTEGER := 0;
    vTmpZeroMis  GTTD_VALORI_TEMP.TIP%TYPE := 'ZeroValMis';
    vTxt         VARCHAR2(200);
    vNum         NUMBER := 0;
    vGerEcs      TIPI_ELEMENTO.GER_ECS%TYPE;
    vTrattElem   TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;

    vRowAggr     t_RowAggrMis;

    vMisStat     PKG_GestAnagr.t_DefAnagr;
    vPrevData    DATE := PKG_UtlGlb.gkDataTappo;

    FUNCTION CompletaSqlAggreg (pSql VARCHAR2) RETURN VARCHAR2 AS
    
        vSql VARCHAR2(6200) := 
               'SELECT COD_TRATTAMENTO_ELEM,DATA,COD_ELEMENTO,COD_TIPO_MISURA,'                 ||
                      'COD_TIPO_FONTE,COD_TIPO_RETE,COD_TIPO_CLIENTE,VALORE '                   ||
                 'FROM ('||pSql||') A '                                                         || 
                'LEFT OUTER JOIN (SELECT * '                                                    ||
                                   'FROM TRATTAMENTO_ELEMENTI '                                 ||
                                  'INNER JOIN (SELECT ALF1 COD_TIPO_MISURA '                    ||
                                                'FROM GTTD_VALORI_TEMP '                        ||
                                               'WHERE TIP = '''||PKG_Mago.gcTmpTipMisKey||''' ' ||
                                             ') USING (COD_TIPO_MISURA) '                       ||      
                                  'WHERE ORGANIZZAZIONE = :org '                                ||
                                   ' AND TIPO_AGGREGAZIONE = :sta '                             ||
                               ') T  USING(COD_ELEMENTO,'                                       ||
                                          'COD_TIPO_MISURA,'                                    ||
                                          'COD_TIPO_FONTE,'                                     ||
                                          'COD_TIPO_RETE,'                                      ||
                                          'COD_TIPO_CLIENTE) ';
        BEGIN
            IF PKG_Mago.IsMagoDGF THEN
                -- per MagoDGF selezione aggregate standard 
                RETURN vSql;
            END IF;
            IF pOrganizzazione = PKG_Mago.gcOrganizzazELE THEN
                -- per organizzazione Elettrica aggregate standard 
                RETURN vSql;
            END IF;
            -- per Mago STM e per gerarchie Amministrativa o Istat si considerano 'foglie' le aggregate delle sbarre di CS
            -- se elemento di CS e organizzazione non elettrica esclude se non connesso a CP
            RETURN 'SELECT A.* FROM ('||vSql||' ) A '                               ||
                    'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = A.COD_ELEMENTO '     ||
                    'INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO) '            ||
                    'WHERE '                                                        ||
                           'CASE '||pOrganizzazione||' '                            ||
                                'WHEN '||PKG_Mago.gcOrganizzazGEO||' THEN '         ||
                                    'CASE WHEN GER_GEO = 1 AND GER_ECS = 0 THEN 1 ' ||
                                         'ELSE 0 '                                  ||
                                    'END '                                          ||
                                'WHEN '||PKG_Mago.gcOrganizzazAMM||' THEN '         ||
                                    'CASE WHEN GER_AMM = 1 AND GER_ECS = 0 THEN 1 ' ||
                                         'ELSE 0 '                                  ||
                                    'END '                                          ||
                           'END = 1 ' ;
        END;

    PROCEDURE ElaboraMisureStandard AS
        BEGIN
            DELETE GTTD_MISURE;
            vSqlAggr := CompletaSqlAggreg(GetSqlAggregazione(pOrganizzazione,pStato,FALSE,FALSE/*,DISCONNECT NON GESTITO PER LE AGGREGAZIONI ( AGGREGA SOLO ELEMENTI IN GERARCHIA )*/));
            EXECUTE IMMEDIATE  'INSERT INTO GTTD_MISURE(COD_TRATTAMENTO_ELEM,DATA,COD_ELEMENTO,COD_TIPO_MISURA,'    ||
                                                       'COD_TIPO_FONTE,COD_TIPO_RETE,COD_TIPO_CLIENTE,VALORE'       ||
                                                      ') '|| vSqlAggr
                       USING pData, pData, pData, pOrganizzazione, pStato;    
            vNumTot := SQL%ROWCOUNT;
            FOR i IN ( SELECT * 
                         FROM GTTD_MISURE 
                        WHERE COD_TRATTAMENTO_ELEM IS NULL
                     ) LOOP
                UPDATE GTTD_MISURE 
                   SET COD_TRATTAMENTO_ELEM = PKG_Misure.GetTrattamentoElemento(i.COD_ELEMENTO,
                                                                                NULL,
                                                                                i.COD_TIPO_MISURA,
                                                                                i.COD_TIPO_FONTE,
                                                                                i.COD_TIPO_RETE,
                                                                                i.COD_TIPO_CLIENTE,
                                                                                pOrganizzazione,
                                                                                pStato)
                 WHERE COD_ELEMENTO = i.COD_ELEMENTO
                   AND COD_TIPO_MISURA = i.COD_TIPO_MISURA
                   AND COD_TIPO_FONTE = i.COD_TIPO_FONTE
                   AND COD_TIPO_RETE = i.COD_TIPO_RETE
                   AND COD_TIPO_CLIENTE = i.COD_TIPO_CLIENTE;
            END LOOP;
            
            MERGE INTO MISURE_AGGREGATE B
                 USING (SELECT COD_TRATTAMENTO_ELEM, DATA, VALORE
                          FROM GTTD_MISURE
                       ) A
                   ON (    B.COD_TRATTAMENTO_ELEM = A.COD_TRATTAMENTO_ELEM
                       AND B.DATA  = A.DATA)
                 WHEN NOT MATCHED THEN
                        INSERT (COD_TRATTAMENTO_ELEM, DATA, VALORE)
                        VALUES (A.COD_TRATTAMENTO_ELEM, A.DATA, A.VALORE)
                 WHEN MATCHED THEN
                        UPDATE SET B.VALORE = A.VALORE
                         WHERE B.VALORE <> A.VALORE;
            vNumMod := SQL%ROWCOUNT;                         
        END;

    PROCEDURE ElaboraMisureStatiche AS
        BEGIN
            -- inizializzazione area misure statiche
            vMisStat := NULL;
            PKG_GestAnagr.InitTab(vMisStat,pData,USER,'MISURE_AGGREGATE_STATICHE','DATA_ATTIVAZIONE','DATA_DISATTIVAZIONE',FALSE);
            PKG_GestAnagr.AddCol (vMisStat,'COD_TRATTAMENTO_ELEM',PKG_GestAnagr.cColChiave);
            PKG_GestAnagr.AddCol (vMisStat,'VALORE',PKG_GestAnagr.cColAttributo);
            DELETE GTTD_MISURE;
            -- predispongo le misure inizializzate (GTTD_MISURE) con valore iniziale = 0
            IF (pOrganizzazione = PKG_Mago.gcOrganizzazELE) OR PKG_Mago.IsMagoDGF THEN
                vSqlMisStat := REPLACE(vSqlMisStat,'#MIS_GER_ECS#',vSqlMisEcs);
                EXECUTE IMMEDIATE vSqlMisStat USING 0,pData,pOrganizzazione,pStato,pOrganizzazione,pStato;
            ELSE
                vSqlMisStat := REPLACE(vSqlMisStat,'#MIS_GER_ECS#','');
                EXECUTE IMMEDIATE vSqlMisStat USING 0,pData,pOrganizzazione,pStato;
            END IF;   
            vNumIni := vNumIni + SQL%ROWCOUNT;
            vSqlAggr := CompletaSqlAggreg(GetSqlAggregazione(pOrganizzazione,pStato,TRUE,FALSE/*,DISCONNECT NON GESTITO PER LE AGGREGAZIONI ( AGGREGA SOLO ELEMENTI IN GERARCHIA )*/));

            OPEN vCurAggr FOR vSqlAggr USING pData,pData,pData,pData,pOrganizzazione,pStato;
            LOOP
                FETCH vCurAggr INTO vRowAggr;
                EXIT WHEN vCurAggr%NOTFOUND;
                IF vRowAggr.COD_TRATTAMENTO_ELEM IS NULL THEN
                    vRowAggr.COD_TRATTAMENTO_ELEM := PKG_Misure.GetTrattamentoElemento(vRowAggr.COD_ELEMENTO,
                                                                                       NULL,
                                                                                       vRowAggr.COD_TIPO_MISURA,
                                                                                       vRowAggr.COD_TIPO_FONTE,
                                                                                       vRowAggr.COD_TIPO_RETE,
                                                                                       vRowAggr.COD_TIPO_CLIENTE,
                                                                                       pOrganizzazione,
                                                                                       pStato);
                END IF;
                -- aggiorno valore iniziale (GTTD_MISURE.VALORE2) con il valore dell'aggregata
                UPDATE GTTD_MISURE SET VALORE2  = vRowAggr.VALORE, MODIFICA = 1
                 WHERE COD_TRATTAMENTO_ELEM = vRowAggr.COD_TRATTAMENTO_ELEM;
                IF SQL%ROWCOUNT = 0 THEN
                    -- se aggregata non presente la inserisco
                    INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,
                                             COD_ELEMENTO,         
                                             VALORE2,
                                             DATA_A,
                                             INSERIMENTO)
                                     VALUES (vRowAggr.COD_TRATTAMENTO_ELEM,
                                             vRowAggr.COD_ELEMENTO,
                                             vRowAggr.VALORE,
                                             vRowAggr.DATA,
                                             1);
                END IF;
            END LOOP;
            CLOSE vCurAggr;
            FOR i IN (SELECT COD_TRATTAMENTO_ELEM,NVL(VALORE2,0) VALORE,NVL(DATA,DATA_A) DATA,COD_ELEMENTO,INSERIMENTO,
                             CASE WHEN VALORE <> NVL(VALORE2,0) THEN 1 ELSE NULL END MODIFICA
                        FROM GTTD_MISURE
                       INNER JOIN ELEMENTI USING(COD_ELEMENTO)
                       INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
                       WHERE (   (VALORE <> NVL(VALORE2,0)) -- se valore originale diverso da valore attuale
                              OR (VALORE IS NULL))          -- se valore originale non presente
                     ) LOOP
                IF (i.INSERIMENTO IS NULL) AND (i.MODIFICA IS NULL) THEN
                     i.VALORE := 0;
                END IF;
                PKG_GestAnagr.InitRow(vMisStat);
                PKG_GestAnagr.AddVal (vMisStat, 'COD_TRATTAMENTO_ELEM', i.COD_TRATTAMENTO_ELEM);
                PKG_GestAnagr.AddVal (vMisStat, 'VALORE', i.VALORE);
                PKG_GestAnagr.Elabora(vMisStat);
                IF i.MODIFICA = 1 THEN
                    vNumMod := vNumMod + 1;
                END IF;
                IF i.INSERIMENTO = 1 THEN
                    vNumIns := vNumIns + 1;
                END IF;
            END LOOP ;
            -- azzera eventuali aggregate non piu' presenti
            FOR i IN (SELECT COD_TRATTAMENTO_ELEM,T.COD_ELEMENTO,T.COD_TIPO_MISURA,
                             T.COD_TIPO_FONTE,T.COD_TIPO_RETE,T.COD_TIPO_CLIENTE,T.ORGANIZZAZIONE,T.TIPO_AGGREGAZIONE
                        FROM GTTD_MISURE
                       INNER JOIN TRATTAMENTO_ELEMENTI T USING(COD_TRATTAMENTO_ELEM) 
                       WHERE NVL(INSERIMENTO,0) + NVL(MODIFICA,0) = 0 ORDER BY 2,1
                     ) LOOP
                PKG_GestAnagr.InitRow(vMisStat);
                PKG_GestAnagr.AddVal (vMisStat, 'COD_TRATTAMENTO_ELEM', i.COD_TRATTAMENTO_ELEM);
                PKG_GestAnagr.AddVal (vMisStat, 'VALORE', 0);
                PKG_GestAnagr.Elabora(vMisStat);
                vNumDel := vNumDel + 1;
            END LOOP;
        END;

 BEGIN

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE   THEN vTxt := 'Elettrica';
        WHEN PKG_Mago.gcOrganizzazGEO   THEN vTxt := 'Geografica';
        WHEN PKG_Mago.gcOrganizzazAMM   THEN vTxt := 'Amministrativa';
    END CASE;
    vTxt := vTxt || ' / ';
    CASE pStato
        WHEN PKG_Mago.gcStatoNormale    THEN vTxt := vTxt || 'Normale';
        WHEN PKG_Mago.gcStatoAttuale    THEN vTxt := vTxt || 'Attuale';
    END CASE;

    vLog := PKG_Logs.StdLogInit(pClasseFunz   => PKG_Mago.gcJobClassAGR||PKG_Mago.gcJobSubClassCAL,
                                pFunzione     => 'PKG_Aggregazioni.EseguiAggregazione',
                                pDataRif      => pData,
                                pTipo         => 'Organizzazione/Stato',
                                pCodice       => vTxt,
                                pStoreOnFile  => FALSE,
                                pForceLogFile => TRUE);

    vTxt := 'Tipi Misura         : ';
    FOR vTip IN (SELECT ALF1
                  FROM GTTD_VALORI_TEMP
                 WHERE TIP = PKG_Mago.gcTmpTipMisKey)
    LOOP
        vTxt := vTxt || vTip.ALF1 || ',';
    END LOOP;
    IF SUBSTR(vTxt,LENGTH(vTxt)) = ',' THEN
        vTxt := SUBSTR(vTxt,1,LENGTH(vTxt) -1);
    END IF;

    PKG_Logs.StdLogAddTxt(vTxt,TRUE,NULL,vLog);
    IF pMisStat = PKG_UtlGlb.gkFlagON THEN
        ElaboraMisureStatiche;
        PKG_Logs.StdLogAddTxt('Aggr. Iniziali      : ' ||TO_CHAR(vNumIni),TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Aggr. Inserite      : ' ||TO_CHAR(vNumIns),TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Aggr. Modificate    : ' ||TO_CHAR(vNumMod),TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Aggr. Azzerate      : ' ||TO_CHAR(vNumDel),TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Aggr. Finali        : ' ||TO_CHAR((vNumIni + vNumIns)),TRUE,NULL,vLog);
    ELSE
        ElaboraMisureStandard;
        PKG_Logs.StdLogAddTxt('Aggr. Attuali       : ' ||TO_CHAR(vNumTot),TRUE,NULL,vLog);
        PKG_Logs.StdLogAddTxt('Aggr. Inser./Modif. : ' ||TO_CHAR(vNumMod),TRUE,NULL,vLog);
    END IF;

    PKG_Logs.StdLogPrint(vLog);

--    OPEN vCurAggr FOR vSqlAggr USING pData,pData,pData,pOrganizzazione,pStato;
--
--    LOOP
--        FETCH vCurAggr INTO vRowAggr;
--        EXIT WHEN vCurAggr%NOTFOUND;
--        IF vRowAggr.COD_TRATTAMENTO_ELEM IS NULL THEN
--            vRowAggr.COD_TRATTAMENTO_ELEM := PKG_Misure.GetTrattamentoElemento(vRowAggr.COD_ELEMENTO,
--                                                                               NULL,
--                                                                               vRowAggr.COD_TIPO_MISURA,
--                                                                               vRowAggr.COD_TIPO_FONTE,
--                                                                               vRowAggr.COD_TIPO_RETE,
--                                                                               vRowAggr.COD_TIPO_CLIENTE,
--                                                                               pOrganizzazione,
--                                                                               pStato);
--        END IF;
--        -- aggiorno valore iniziale (GTTD_MISURE.VALORE2) con il valore dell'aggregata
--        UPDATE GTTD_MISURE SET VALORE2  = vRowAggr.VALORE, MODIFICA = 1
--         WHERE COD_TRATTAMENTO_ELEM = vRowAggr.COD_TRATTAMENTO_ELEM;
--        IF SQL%ROWCOUNT = 0 THEN
--            -- se aggregata non presente la inserisco
--            INSERT INTO GTTD_MISURE (COD_TRATTAMENTO_ELEM,
--                                     COD_ELEMENTO,         
--                                     VALORE2,
--                                     DATA_A,
--                                     INSERIMENTO)
--                             VALUES (vRowAggr.COD_TRATTAMENTO_ELEM,
--                                     vRowAggr.COD_ELEMENTO,
--                                     vRowAggr.VALORE,
--                                     vRowAggr.DATA,
--                                     1);
--        END IF;
--    END LOOP;
--    CLOSE vCurAggr;
--
--    FOR i IN (SELECT COD_TRATTAMENTO_ELEM,NVL(VALORE2,0) VALORE,NVL(DATA,DATA_A) DATA,COD_ELEMENTO,INSERIMENTO,
--                     CASE WHEN VALORE <> NVL(VALORE2,0) THEN 1 ELSE NULL END MODIFICA
--                FROM GTTD_MISURE
--               INNER JOIN ELEMENTI USING(COD_ELEMENTO)
--               INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
--               WHERE (   (VALORE <> NVL(VALORE2,0)) -- se valore originale diverso da valore attuale
--                      OR (VALORE IS NULL))          -- se valore originale non presente
--             ) LOOP
--        IF (i.INSERIMENTO IS NULL) AND (i.MODIFICA IS NULL) THEN
--             i.VALORE := 0;
--        END IF;
--        IF i.MODIFICA = 1 THEN
--            IF pMisStat = PKG_UtlGlb.gkFlagOFF THEN 
--                UPDATE MISURE_AGGREGATE SET VALORE = i.VALORE
--                 WHERE COD_TRATTAMENTO_ELEM = i.COD_TRATTAMENTO_ELEM AND DATA = i.DATA;
--                IF SQL%ROWCOUNT = 0 THEN
--                    i.INSERIMENTO := 1;
--                ELSE
--                    vNumMod := vNumMod + 1;
--                END IF;
--            ELSE
--                BEGIN 
--                    PKG_GestAnagr.InitRow(vMisStat);
--                    PKG_GestAnagr.AddVal (vMisStat, 'COD_TRATTAMENTO_ELEM', i.COD_TRATTAMENTO_ELEM);
--                    PKG_GestAnagr.AddVal (vMisStat, 'VALORE', i.VALORE);
--                    PKG_GestAnagr.Elabora(vMisStat);
--                    vNumMod := vNumMod + 1;
--                EXCEPTION
--                    --WHEN DUP_VAL_ON_INDEX THEN
--                    --    UPDATE MISURE_AGGREGATE_STATICHE
--                    --       SET VALORE = i.VALORE
--                    --     WHERE COD_TRATTAMENTO_ELEM = i.COD_TRATTAMENTO_ELEM
--                    --       AND I.DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
--                    --    vNumMod := vNumMod + 1;
--                    WHEN OTHERS THEN RAISE;
--                END;
--            END IF;
--        END IF;
--        IF i.INSERIMENTO = 1 THEN
--            IF pMisStat = PKG_UtlGlb.gkFlagOFF THEN 
--                BEGIN
--                    INSERT INTO MISURE_AGGREGATE (  COD_TRATTAMENTO_ELEM,   DATA,   VALORE)
--                                          VALUES (i.COD_TRATTAMENTO_ELEM, i.DATA, i.VALORE);
--                    vNumIns := vNumIns + 1;
--                EXCEPTION
--                    WHEN DUP_VAL_ON_INDEX THEN
--                        UPDATE MISURE_AGGREGATE
--                           SET VALORE = i.VALORE
--                         WHERE COD_TRATTAMENTO_ELEM = i.COD_TRATTAMENTO_ELEM
--                           AND DATA = i.DATA;
--                        vNumMod := vNumMod + 1;
--                    WHEN OTHERS THEN RAISE;
--                END;
--            ELSE
--                BEGIN 
--                   PKG_GestAnagr.InitRow(vMisStat);
--                   PKG_GestAnagr.AddVal (vMisStat, 'COD_TRATTAMENTO_ELEM', i.COD_TRATTAMENTO_ELEM);
--                   PKG_GestAnagr.AddVal (vMisStat, 'VALORE', i.VALORE);
--                   PKG_GestAnagr.Elabora(vMisStat);
--                   vNumIns := vNumIns + 1;
--                EXCEPTION
--                    --WHEN DUP_VAL_ON_INDEX THEN
--                    --    UPDATE MISURE_AGGREGATE_STATICHE
--                    --       SET VALORE = i.VALORE
--                    --     WHERE COD_TRATTAMENTO_ELEM = i.COD_TRATTAMENTO_ELEM
--                    --       AND I.DATA BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
--                    --    vNumMod := vNumMod + 1;
--                    WHEN OTHERS THEN RAISE;
--                END;
--            END IF;
--        END IF;
--    END LOOP ;
--
--    IF pMisStat = PKG_UtlGlb.gkFlagOFF THEN
--        /*     
--        -- cancella eventuali aggregate non piu' presenti
--        DELETE MISURE_AGGREGATE
--         WHERE DATA = pData
--           AND COD_TRATTAMENTO_ELEM IN (SELECT COD_TRATTAMENTO_ELEM FROM GTTD_MISURE WHERE NVL(INSERIMENTO,0) + NVL(MODIFICA,0) = 0);
--        */
--        --/*
--        -- Aggiorno con valore 0 (zero) eventuali aggregate non piu' presenti
--        UPDATE MISURE_AGGREGATE
--           SET VALORE = 0
--         WHERE DATA = pData
--           AND COD_TRATTAMENTO_ELEM IN (SELECT COD_TRATTAMENTO_ELEM 
--                                          FROM GTTD_MISURE 
--                                         WHERE NVL(INSERIMENTO,0) + NVL(MODIFICA,0) = 0
--                                       )
--           AND VALORE = 0;
--        --*/
--        vNumDel := SQL%ROWCOUNT;
--    ELSE
--        -- azzera eventuali aggregate non piu' presenti
--        FOR i IN (SELECT COD_TRATTAMENTO_ELEM,T.COD_ELEMENTO,COD_TIPO_ELEMENTO,T.COD_TIPO_MISURA,
--                         T.COD_TIPO_FONTE,COD_TIPO_RETE,COD_TIPO_CLIENTE,ORGANIZZAZIONE,TIPO_AGGREGAZIONE
--                    FROM GTTD_MISURE
--                   INNER JOIN TRATTAMENTO_ELEMENTI T USING(COD_TRATTAMENTO_ELEM) 
--                   WHERE NVL(INSERIMENTO,0) + NVL(MODIFICA,0) = 0 ORDER BY 2,1) LOOP
--            PKG_GestAnagr.InitRow(vMisStat);
--            PKG_GestAnagr.AddVal (vMisStat, 'COD_TRATTAMENTO_ELEM', i.COD_TRATTAMENTO_ELEM);
--            PKG_GestAnagr.AddVal (vMisStat, 'VALORE', 0);
--            PKG_GestAnagr.Elabora(vMisStat);
--            vNumDel := vNumDel + 1;
--        END LOOP;
--    END IF;
--
--    vTxt := 'Tipi Misura         : ';
--    FOR vTip IN (SELECT ALF1
--                  FROM GTTD_VALORI_TEMP
--                 WHERE TIP = PKG_Mago.gcTmpTipMisKey)
--    LOOP
--        vTxt := vTxt || vTip.ALF1 || ',';
--    END LOOP;
--    IF SUBSTR(vTxt,LENGTH(vTxt)) = ',' THEN
--        vTxt := SUBSTR(vTxt,1,LENGTH(vTxt) -1);
--    END IF;
--    PKG_Logs.StdLogAddTxt(vTxt,TRUE,NULL,vLog);
--
--    PKG_Logs.StdLogAddTxt('Aggr. Iniziali      : ' ||TO_CHAR(vNumIni),TRUE,NULL,vLog);
--    PKG_Logs.StdLogAddTxt('Aggr. Inserite      : ' ||TO_CHAR(vNumIns),TRUE,NULL,vLog);
--    PKG_Logs.StdLogAddTxt('Aggr. Modificate    : ' ||TO_CHAR(vNumMod),TRUE,NULL,vLog);
--    --IF pMisStat = PKG_UtlGlb.gkFlagON THEN
--        PKG_Logs.StdLogAddTxt('Aggr. Azzerate      : ' ||TO_CHAR(vNumDel),TRUE,NULL,vLog);
--        PKG_Logs.StdLogAddTxt('Aggr. Finali        : ' ||TO_CHAR((vNumIni + vNumIns)),TRUE,NULL,vLog);
--    --ELSE
--    --    PKG_Logs.StdLogAddTxt('Aggr. Eliminate     : ' ||TO_CHAR(vNumDel),TRUE,NULL,vLog);
--    --    PKG_Logs.StdLogAddTxt('Aggr. Finali        : ' ||TO_CHAR((vNumIni + vNumIns) - vNumDel),TRUE,NULL,vLog);
--    --END IF;
--
--    PKG_Logs.StdLogPrint(vLog);

 END EseguiAggregazione;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE CalcolaDisattivazioneUndisconn    (pTablename IN VARCHAR2) AS

vsql VARCHAR2(1000):= 'MERGE INTO #TAB# t USING ( ' ||
            'SELECT * FROM ( ' ||
                           'SELECT g.* ' ||
                           '      ,lead(data_attivazione) over ( partition by #COL# order by data_attivazione )-1/(24*60*60) data_disattivazione_calc ' ||
                           ' FROM #TAB# G ' ||
                           'WHERE data_attivazione < data_disattivazione) ' || 
            'WHERE data_disattivazione_undisconn IS NULL AND data_disattivazione_calc IS NOT NULL) s ' ||
              ' ON (s.#COL# = t.#COL# AND s.data_attivazione = t.data_attivazione AND s.data_disattivazione = t.data_disattivazione ) ' ||
             'WHEN MATCHED THEN ' ||
           'UPDATE SET ' ||
                      't.data_disattivazione_undisconn = s.data_disattivazione_calc ' ||
             'WHEN NOT MATCHED THEN ' || 
           'INSERT (#COL#) ' ||
           'VALUES (NULL)';

BEGIN
	
	vsql := REPLACE(vsql, '#TAB#' , pTablename);
	CASE WHEN pTablename IN ('GERARCHIA_AMM','GERARCHIA_IMP_SN','GERARCHIA_IMP_SA','GERARCHIA_GEO') THEN
	   vsql := REPLACE(vsql,'#COL#', 'cod_elemento');
	     WHEN pTablename IN ('REL_ELEMENTI_ECP_SN','REL_ELEMENTI_ECP_SA','REL_ELEMENTI_ECS_SA','REL_ELEMENTI_ECS_SN','REL_ELEMENTI_GEO','REL_ELEMENTI_AMM') THEN
	   vsql := REPLACE(vsql,'#COL#', 'cod_elemento_figlio');  
	END CASE;
	EXECUTE IMMEDIATE ( vsql );
	
	COMMIT;

END CalcolaDisattivazioneUndisconn;
-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

END PKG_Aggregazioni;
/
SHOW ERRORS;


