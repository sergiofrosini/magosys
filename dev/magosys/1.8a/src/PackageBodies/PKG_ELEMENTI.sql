PROMPT PACKAGE BODY PKG_ELEMENTI;
--
-- PKG_ELEMENTI  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_ELEMENTI AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 1.8.a.5
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
Tipi, Costanti e Variabili Globali Private
*********************************************************************************************************** */

 gElementoBase      ELEMENTI.COD_ELEMENTO%TYPE;
 gElementoEseDef    ELEMENTI.COD_ELEMENTO%TYPE;

 cGerarchiaELE      CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazELE;          -- gerarchia elettrica          generico
 cGerarchiaELE_CP   CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazELE * 10 + 1; -- gerarchia elettrica          di cabina primaria
 cGerarchiaELE_CS   CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazELE * 10 + 2; -- gerarchia elettrica          di cabina secondaria
 cGerarchiaGEO      CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazGEO;          -- gerarchia geografica (Istat) base
 cGerarchiaGEO_CS   CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazGEO * 10 + 2; -- gerarchia geografica (Istat) di cabina secondaria
 cGerarchiaAMM      CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazAMM;          -- gerarchia amministrativa     base
 cGerarchiaAMM_CS   CONSTANT INTEGER   := Pkg_Mago.gcOrganizzazAMM * 10 + 2; -- gerarchia amministrativa     di cabina secondaria

 gIDReteInit        BOOLEAN := FALSE;
 gIDReteAT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete AT
 gIDReteMT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete MT
 gIDReteBT          TIPI_RETE.ID_RETE%TYPE;                                        -- ID Rete BT

 gIDProdInit        BOOLEAN := FALSE;
 gIDProdPuro        TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID CLIENTE puro
 gIDProdCliente     TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID CLIENTE/Cliente
 gIDProdNonDeterm   TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID Non determinato
 gIDProdNonApplic   TIPI_CLIENTE.ID_CLIENTE%TYPE;                            -- ID Non applicabile

 vCodEseDefault     ELEMENTI.COD_GEST_ELEMENTO%TYPE;
 vComDate           DATE;

/* ***********************************************************************************************************
Funzioni e Procedure Private
*********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
END PRINT;

PROCEDURE GetGerarchiaID    (pElemento       IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pGerarchia      IN INTEGER,
                             pGerarchiaBase OUT INTEGER,
                             pNextLevel     OUT INTEGER) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna l'identificativo gerarchia in base all'organizzazione e all'elemento
-----------------------------------------------------------------------------------------------------------*/
    vLivEcp TIPI_ELEMENTO.GER_ECP%TYPE;
    vLivEcs TIPI_ELEMENTO.GER_ECS%TYPE;
    vLivAmm TIPI_ELEMENTO.GER_AMM%TYPE;
    vLivGeo TIPI_ELEMENTO.GER_GEO%TYPE;
    vtip TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
 BEGIN
    SELECT E.COD_TIPO_ELEMENTO ,NVL(GER_ECP,0), NVL(GER_ECS,0), NVL(GER_AMM,0), NVL(GER_GEO,0)
      INTO vTip, vLivEcp, vLivEcs, vLivAmm, vLivGeo
      FROM ELEMENTI E
     INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO)
     WHERE COD_ELEMENTO = pElemento;
    IF vLivEcs > 0 THEN -- assumo che la CS sia il punto di congiunzione ...
        CASE pGerarchia
            WHEN cGerarchiaELE THEN pGerarchiaBase := cGerarchiaELE_CS;
            WHEN cGerarchiaGEO THEN pGerarchiaBase := cGerarchiaGEO_CS;
            WHEN cGerarchiaAMM THEN pGerarchiaBase := cGerarchiaAMM_CS;
        END CASE;
    ELSE
        CASE pGerarchia
            WHEN cGerarchiaELE THEN pGerarchiaBase := cGerarchiaELE_CP;
            WHEN cGerarchiaGEO THEN pGerarchiaBase := pGerarchia;
            WHEN cGerarchiaAMM THEN pGerarchiaBase := pGerarchia;
        END CASE;
    END IF;
    CASE pGerarchiaBase
        WHEN cGerarchiaELE_CP  THEN pNextLevel := vLivEcp; -- gerarchia elettrica di cabina primaria
        WHEN cGerarchiaELE_CS  THEN pNextLevel := vLivEcs; -- gerarchia elettrica di cabina secondaria
        WHEN cGerarchiaGEO_CS  THEN pNextLevel := vLivEcs; -- gerarchia geografica (Istat) di cabina secondariacabina secondaria
        WHEN cGerarchiaAMM_CS  THEN pNextLevel := vLivEcs; -- gerarchia amministrativa di cabina secondaria
        WHEN cGerarchiaGEO     THEN pNextLevel := vLivGeo; -- gerarchia geografica (Istat)
        WHEN cGerarchiaAMM     THEN pNextLevel := vLivAmm; -- gerarchia amministrativa
    END CASE;
END GetGerarchiaID;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetFETreeRefDate (pDataDa      IN DATE,
                           pDataA       IN DATE) RETURN DATE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la data ti riferimento per la selezione dell'albero in FE
-----------------------------------------------------------------------------------------------------------*/
    vData DATE;
BEGIN
--    CASE
--        WHEN SYSDATE BETWEEN pDataDa AND pDataA THEN  vData := SYSDATE;
--        WHEN SYSDATE       > pDataA             THEN  vData := pDataA;
--        WHEN SYSDATE       < pDataDa            THEN  vData := pDataDa;
--    END CASE;
--    RETURN GetStartDate (vData);
   RETURN pDataA;
END GetFETreeRefDate;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitIdRete AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza codici numerici identificativi dell tipo rete
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF gIDReteInit = TRUE THEN
        RETURN;
    END IF;
    SELECT ID_RETE INTO gIDReteAT FROM TIPI_RETE WHERE COD_TIPO_RETE = PKG_Mago.gcTipReteAT;
    SELECT ID_RETE INTO gIDReteMT FROM TIPI_RETE WHERE COD_TIPO_RETE = PKG_Mago.gcTipReteMT;
    SELECT ID_RETE INTO gIDReteBT FROM TIPI_RETE WHERE COD_TIPO_RETE = PKG_Mago.gcTipReteBT;
    gIDReteInit := TRUE;
END InitIdRete;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE InitIdProd AS
/*-----------------------------------------------------------------------------------------------------------
   Inizializza codici numerici identificativi dell tipo CLIENTE
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    IF gIDProdInit = TRUE THEN
        RETURN;
    END IF;
    SELECT ID_CLIENTE INTO gIDProdPuro      FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = PKG_Mago.gcClienteProdPuro;
    SELECT ID_CLIENTE INTO gIDProdCliente   FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = PKG_Mago.gcClienteAutoProd;
    SELECT ID_CLIENTE INTO gIDProdNonDeterm FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = PKG_Mago.gcClientePrdNonDeterm;
    SELECT ID_CLIENTE INTO gIDProdNonApplic FROM TIPI_CLIENTE WHERE COD_TIPO_CLIENTE = PKG_Mago.gcClientePrdNonApplic;
    gIDProdInit := TRUE;
END InitIdProd;

-- ----------------------------------------------------------------------------------------------------------

/* **********************************************************************************************************
*************************************************************************************************************

Funzioni e Procedure Pubbliche

********************************************************************************************************** */

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetOrderByElemType (pTipoElem       IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pCodGestElem    IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN INTEGER AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la priorit� di ordimamento di default per il tipo elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    CASE pTipoElem
       WHEN PKG_Mago.gcCentroOperativo   THEN RETURN 0;
       WHEN PKG_Mago.gcEsercizio THEN
                  CASE INSTR(pCodGestElem,'_')
                      WHEN 0             THEN RETURN 1;
                                         ELSE RETURN 2;
                  END CASE;
       WHEN PKG_Mago.gcCabinaPrimaria    THEN RETURN 3;
       WHEN PKG_Mago.gcTrasfCabPrim      THEN RETURN 4;
       WHEN PKG_Mago.gcClienteAT         THEN RETURN 5;
       WHEN PKG_Mago.gcGeneratoreAT      THEN RETURN 6;
       WHEN PKG_Mago.gcSecondarioDiTrasf THEN RETURN 7;
       WHEN PKG_Mago.gcTerziarioDiTrasf  THEN RETURN 8;
       WHEN PKG_Mago.gcSbarraMT          THEN RETURN 9;
       WHEN PKG_Mago.gcLineaMT           THEN RETURN 10;
       WHEN PKG_Mago.gcSbarraCabSec      THEN RETURN 11;
       WHEN PKG_Mago.gcClienteMT         THEN RETURN 12;
       WHEN PKG_Mago.gcGeneratoreMT      THEN RETURN 13;
       WHEN PKG_Mago.gcTrasformMtBt      THEN RETURN 14;
       WHEN PKG_Mago.gcTrasformMtBtDett  THEN RETURN 15;
       WHEN PKG_Mago.gcClienteBT         THEN RETURN 16;
       WHEN PKG_Mago.gcGeneratoreBT      THEN RETURN 17;
       ELSE                                   RETURN 99;
    END CASE;
 END GetOrderByElemType;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE SetElemBaseEseDef (pElementoBase     ELEMENTI.COD_ELEMENTO%TYPE,
                             pElementoEseDef   ELEMENTI.COD_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Imposta le variabili
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    gElementoBase   := pElementoBase;
    gElementoEseDef := gElementoEseDef;
 END SetElemBaseEseDef;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoBase    RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'esercizio di default
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gElementoBase;
 END GetElementoBase;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoEseDef    RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce l'elemento base
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    RETURN gElementoEseDef;
 END GetElementoEseDef;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION DecodeTipElem      (pTipEle         IN VARCHAR2,
                             pCodeOnNotFound IN BOOLEAN DEFAULT TRUE)
                      RETURN TIPI_ELEMENTO.DESCRIZIONE%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    decodifica il tipo elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vDesc  TIPI_ELEMENTO.DESCRIZIONE%TYPE;
BEGIN

    SELECT DESCRIZIONE
      INTO vDesc
      FROM TIPI_ELEMENTO
     WHERE COD_TIPO_ELEMENTO = pTipEle;
    RETURN vDesc;

 EXCEPTION
    WHEN NO_DATA_FOUND THEN
        IF pCodeOnNotFound THEN
            RETURN pTipEle;
        ELSE
            RETURN NULL;
        END IF;
    WHEN OTHERS THEN RAISE;

END DecodeTipElem;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetDefaultCO      (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                             pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le informazioni relative al CO di default
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO
      INTO pElem, pGest, pNome
      FROM DEFAULT_CO
     INNER JOIN V_ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_CO
     WHERE DATA_DISATTIVAZIONE >= SYSDATE
       AND DATA_ATTIVAZIONE    <= SYSDATE;
END GetDefaultCO;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetDefaultESE     (pElem          OUT ELEMENTI.COD_ELEMENTO%TYPE,
                             pGest          OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNome          OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce le informazioni relative alla Esercizio di default
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, NOME_ELEMENTO
      INTO pElem, pGest, pNome
      FROM DEFAULT_CO
     INNER JOIN V_ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
     WHERE DATA_DISATTIVAZIONE >= SYSDATE
       AND DATA_ATTIVAZIONE   <= SYSDATE;
END GetDefaultESE;

-- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                             pDataA          IN DATE,
--                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elementoralativi ALL'ultima occorenza nel periodo richiesto
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT COD_ELEMENTO,
--                        COD_GEST_ELEMENTO,
--                        NOME_ELEMENTO,
--                        COD_TIPO_ELEMENTO,
--                        COD_TIPO_FONTE,
--                        COD_TIPO_CLIENTE,
--                        ID_ELEMENTO,
--                        RIF_ELEMENTO,
--                        COORDINATA_X,
--                        COORDINATA_Y,
--                        POTENZA_INSTALLATA,
--                        POTENZA_CONTRATTUALE
--                   FROM (SELECT E.COD_ELEMENTO,
--                                COD_GEST_ELEMENTO,
--                                NOME_ELEMENTO,
--                                E.COD_TIPO_ELEMENTO,
--                                COD_TIPO_FONTE,
--                                COD_TIPO_CLIENTE,
--                                ID_ELEMENTO,
--                                RIF_ELEMENTO,
--                                COORDINATA_X,
--                                COORDINATA_Y,
--                                POTENZA_INSTALLATA,
--                                POTENZA_CONTRATTUALE,
--                                ROW_NUMBER()OVER(PARTITION BY E.COD_ELEMENTO ORDER BY E.COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD
--                           FROM ELEMENTI E
--                          INNER JOIN ELEMENTI_DEF D ON (D.COD_ELEMENTO = E.COD_ELEMENTO)
--                          WHERE COD_ELEMENTO = pCodElem
--                            AND (DATA_DISATTIVAZIONE >= pDataDa AND DATA_ATTIVAZIONE <= pDataA)
--                        )
--                  WHERE ORD=1
--                )
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pData           IN DATE,
--                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla DATA richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (PKG_Mago.GetElemDef(pData,pData,pCodElem)))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pDataDa         IN DATE,
--                             pDataA          IN DATE,
--                             pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla data richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (PKG_Mago.GetElemDef(pDataDa,pDataA,GetCodElemento(pGstElem))))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

---- ----------------------------------------------------------------------------------------------------------

--FUNCTION  GetElemDef        (pData           IN DATE,
--                             pGstElem        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE) RETURN t_DefElemento PIPELINED AS
--/*-----------------------------------------------------------------------------------------------------------
--    Ritorna i dati anafrafici dell'elemento alla data richiesta
-------------------------------------------------------------------------------------------------------------*/
--BEGIN

--    FOR vRow IN (SELECT * FROM TABLE (PKG_Mago.GetElemDef(pData,pData,GetCodElemento(pGstElem))))
--    LOOP
--        PIPE ROW(vRow);
--    END LOOP;
--    RETURN;

--END GetElemDef;

-- ----------------------------------------------------------------------------------------------------------

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetCodElemento    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pNoNascosti     IN INTEGER DEFAULT 0)
                                                RETURN ELEMENTI.COD_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Converte il codice gestionale in codice elemento
-----------------------------------------------------------------------------------------------------------*/
    vElem     ELEMENTI.COD_ELEMENTO%TYPE := NULL;
    vNascosto TIPI_ELEMENTO.NASCOSTO%TYPE;
BEGIN
    BEGIN
        SELECT COD_ELEMENTO, NASCOSTO
          INTO vElem, vNascosto
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
         WHERE COD_GEST_ELEMENTO = pGestElem;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                SELECT COD_ELEMENTO
                  INTO vElem
                  FROM ELEMENTI_DEF
                 INNER JOIN ELEMENTI E USING(COD_ELEMENTO)
                 INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
                 WHERE RIF_ELEMENTO = pGestElem
                   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN vElem := NULL;
            END ;
    END;
    IF pNoNascosti <> 0 AND vNascosto = 1 THEN
        vElem := NULL;
    END IF;
    RETURN vElem;
END GetCodElemento;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetGestElemento   (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pNoNascosti     IN INTEGER DEFAULT 0)
                                                RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Converte il codice elemento in godice gestionale
-----------------------------------------------------------------------------------------------------------*/
    vGest     ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
    vGest2    ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
    vTipo     ELEMENTI.COD_TIPO_ELEMENTO%TYPE := NULL;
    vNascosto TIPI_ELEMENTO.NASCOSTO%TYPE;
BEGIN
    BEGIN
        SELECT COD_GEST_ELEMENTO, NASCOSTO, COD_TIPO_ELEMENTO
          INTO vGest, vNascosto, vTipo
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodElem;
    IF pNoNascosti <> 0 AND vNascosto = 1 THEN
        vGest := NULL;
    ELSE
        IF vTipo = PKG_MAGO.gcLineaMT THEN
            BEGIN
                SELECT RIF_ELEMENTO
                  INTO vGest2
                  FROM ELEMENTI_DEF E
                 WHERE COD_ELEMENTO = pCodElem
                   AND SYSDATE BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                IF vGest2 IS NOT NULL THEN
                    vGest := vGest2;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN NULL;
            END;
        END IF;
    END IF;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN vGest := NULL;
    END;
    RETURN vGest;
END GetGestElemento;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  CalcOutCodGest    (pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pData           IN DATE DEFAULT SYSDATE,
                             pTipoElemento   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                             pRifElemento    IN ELEMENTI_DEF.RIF_ELEMENTO%TYPE DEFAULT NULL)
                                                RETURN ELEMENTI.COD_GEST_ELEMENTO%TYPE DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il codice gestionale piu' opportuno da presentare scegliendo tra codice Gestionale e Rif_elemento
-----------------------------------------------------------------------------------------------------------*/
    vRetCode ELEMENTI.COD_GEST_ELEMENTO%TYPE := NULL;
BEGIN
    IF (pTipoElemento IS NOT NULL) THEN
        IF pTipoElemento <> PKG_Mago.gcLineaMT THEN
            RETURN pGestElem;
        ELSE
            IF pRifElemento IS NOT NULL THEN
                RETURN pRifElemento;
            END IF;
        END IF;
    END IF;
    SELECT CASE
              WHEN E.COD_TIPO_ELEMENTO = PKG_Mago.gcLineaMT
                  THEN CASE
                         WHEN D.RIF_ELEMENTO IS NULL
                             THEN E.COD_GEST_ELEMENTO
                             ELSE D.RIF_ELEMENTO
                       END
                  ELSE E.COD_GEST_ELEMENTO
           END
      INTO vRetCode
      FROM ELEMENTI E
     INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
     WHERE E.COD_GEST_ELEMENTO = pGestElem
       AND NVL(pData,SYSDATE) BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE;
    RETURN vRetCode;
EXCEPTION
    WHEN NO_DATA_FOUND THEN RETURN NULL;
    WHEN OTHERS THEN RAISE;
END CalcOutCodGest;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetElementoPadre   (pElemFiglio     IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pTipoPadre      IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                             pData           IN DATE,
                             pOrganizzazione IN NUMBER,
                             pStato          IN NUMBER,
                             pDisconnect     IN NUMBER DEFAULT 0)
                                                RETURN ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Restituisce il Codice Elemento padre dell'elemento ricevuto
-----------------------------------------------------------------------------------------------------------*/
    vSelEle VARCHAR2(1000) := 'SELECT e.COD_ELEMENTO, e.COD_TIPO_ELEMENTO '                     ||
                               'FROM ( SELECT r.*, #DATAELECALC# data_disattivazione_calc '     ||
                                        'FROM #TAB# r '                                         ||
                                    ') r '                                                      ||
                              'INNER JOIN ELEMENTI e '                                          ||
                                 'ON (e.COD_ELEMENTO IN (r.l01, r.l02, r.l03, r.l04, r.l05, r.l06, r.l07, r.l08, r.l09, r.l10, r.l11, r.l12, r.l13) )'                    ||
                              'WHERE r.COD_ELEMENTO = :el '                                 || -- vElem
                                ' AND e.COD_TIPO_ELEMENTO = :tipo '                                ||
                                ' AND :dt BETWEEN r.DATA_ATTIVAZIONE AND r.DATA_DISATTIVAZIONE_CALC ';       -- pData
    vElem       ELEMENTI.COD_ELEMENTO%TYPE;
    vTipo       TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE := pTipoPadre;
    vGerEcs     TIPI_ELEMENTO.GER_ECS%TYPE;
    vRelTab     VARCHAR2(500);
    vLetto      BOOLEAN := FALSE;
    vTrovato    BOOLEAN := FALSE;
    TYPE t_cur IS REF CURSOR;
    vCur t_cur;

BEGIN
    BEGIN
--        SELECT GER_ECS
--          INTO vGerEcs
--          FROM TABLE(PKG_Mago.GetElemDef(pData,pElemFiglio))
--         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO);
        SELECT GER_ECS
          INTO vGerEcs
          FROM ELEMENTI E
         INNER JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pElemFiglio;
    EXCEPTION
         WHEN OTHERS THEN RAISE;
    END;

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN vRelTab := 'GERARCHIA_IMP#STA#';
        WHEN PKG_Mago.gcOrganizzazGEO THEN vRelTab := 'GERARCHIA_GEO';
        WHEN PKG_Mago.gcOrganizzazAMM THEN vRelTab := 'GERARCHIA_AMM';
    END CASE;

    CASE pStato
        WHEN PKG_Mago.gcStatoNormale  THEN vRelTab := REPLACE(vRelTab,'#STA#','_SN');
        WHEN PKG_Mago.gcStatoAttuale  THEN vRelTab := REPLACE(vRelTab,'#STA#','_SA');
    END CASE;

    vSelEle := REPLACE(vSelEle,'#TAB#',vRelTab);

    vElem := pElemFiglio;
    
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSelEle := REPLACE (vSelEle, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSelEle := REPLACE (vSelEle, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    
    BEGIN
        OPEN vCur FOR vSelEle USING vElem, vTipo, pData;
        LOOP
            FETCH vCur INTO vElem, vTipo;
            EXIT WHEN vCur%NOTFOUND;
            vTrovato := TRUE;
        END LOOP;
        CLOSE vCur;
        IF NOT vTrovato THEN
            vElem := NULL;
        END IF;
        RETURN vElem;
    END;

END GetElementoPadre;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  InsertElement     (pCodGest        IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                             pTipElem        IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE) RETURN  ELEMENTI.COD_ELEMENTO%TYPE AS
/*-----------------------------------------------------------------------------------------------------------
    Definisce, se non gia' presente l'elemento, ritorna il codice elemento definito o trovato
-----------------------------------------------------------------------------------------------------------*/
    vCodEle  ELEMENTI.COD_ELEMENTO%TYPE;
    vTipEle  TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
BEGIN

    BEGIN
        INSERT INTO ELEMENTI (COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO)
                      VALUES (pCodGest,pTipElem) RETURNING COD_ELEMENTO INTO vCodEle;
        PKG_Logs.TraceLog('Inserito elemento - Cod='||vCodEle||'  Gest='||pCodGest||'  Tipo='||pTipElem);
    EXCEPTION
        WHEN DUP_VAL_ON_INDEX THEN
            SELECT COD_ELEMENTO, COD_TIPO_ELEMENTO
              INTO vCodEle, vTipEle
              FROM ELEMENTI
             WHERE COD_GEST_ELEMENTO = pCodGest;
            IF vTipEle <> pTipElem THEN
                UPDATE ELEMENTI             SET COD_TIPO_ELEMENTO = pTipElem WHERE COD_ELEMENTO = vCodEle;
                UPDATE TRATTAMENTO_ELEMENTI SET COD_TIPO_ELEMENTO = pTipElem WHERE COD_ELEMENTO = vCodEle;
                PKG_Logs.TraceLog('Per elemento - Cod='||vCodEle||'  Gest='||pCodGest||'  il Tipo � cambiato: da'||vTipEle||' a '||pTipElem);
--                RAISE_APPLICATION_ERROR(PKG_UtlGlb.gkVALUE_ERROR,
--                                        'L''elemento con codice gestionale '||pCodGest||
--                                        ' e'' gia'' presente con codice '||vCodEle||' e tipo elemento '||vTipEle||
--                                        '. L''inserimento con tipo '||pTipElem||' non e'' ammesso!.');
            END IF;
        WHEN OTHERS THEN RAISE;
    END;
    RETURN vCodEle;

END InsertElement;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION  GetElemInfo       (pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataRif        IN DATE,
                             pTipologiaRete  IN VARCHAR2,
                             pTipiCliente    IN VARCHAR2,
                             pOrganizzaz     IN NUMBER,
                             pStatoRete      IN INTEGER) RETURN INTEGER AS -- DETERMINISTIC AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna un flag che descrive una caratteristica dell'elemento
        0 = Generico
        1 = AT
        2 = MT
        3 = AT+MT
        4 = BT
        5 = AT+BT
        6 = MT+BT
        7 = AT+MT+BT
        9 = Generazione non specificata (Tipologia rete non specificata)
    alla caratteristica determinata dalla/e tipologia/e rete aggiunge il valore 10 se l'elemento e' FOGLIA
-----------------------------------------------------------------------------------------------------------*/
    vNum        INTEGER;
    vTab        INTEGER;
    vRet        INTEGER := 0;
    vSumRet     INTEGER := NULL;
    vSumPrd     INTEGER := NULL;
    vCS         TIPI_ELEMENTO.GER_ECS%TYPE;
    vReteAT     REL_ELEMENTO_TIPMIS.RETE_AT%TYPE := 0;
    vReteMT     REL_ELEMENTO_TIPMIS.RETE_MT%TYPE := 0;
    vReteBT     REL_ELEMENTO_TIPMIS.RETE_BT%TYPE := 0;
    vTipEle     TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vStatoRete  INTEGER := pStatoRete;
    vTabRel     VARCHAR2(35) := 'REL_ELEMENTI_#GER##STA#';

    vSqlFoglia  VARCHAR2(400) := 'SELECT COUNT(*) FROM DUAL '                                                               ||
                                  'WHERE EXISTS (SELECT 1 '                                                                 ||
                                                  'FROM #TABREL# R '                                                        ||
                                                 'WHERE COD_ELEMENTO_PADRE = :ce '                                          ||
                                                   'AND :dt BETWEEN R.DATA_ATTIVAZIONE '                                    ||
                                                               'AND R.DATA_DISATTIVAZIONE '                                 ||
                                                   '#COND_GEO_DGF# '                                                        ||
                                                ')';

    vSqlFoglia2 VARCHAR2(400) := 'SELECT COUNT(*) FROM DUAL '                                                               ||
                                  'WHERE EXISTS (SELECT 1 '                                                                 ||
                                                  'FROM #TABREL# R '                                                        ||
                                                 'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = R.COD_ELEMENTO_FIGLIO '         ||
                                                 'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO ' ||
                                                 'WHERE COD_ELEMENTO_PADRE = :ce '                                          ||
                                                   'AND :dt BETWEEN R.DATA_ATTIVAZIONE '                                    ||
                                                               'AND R.DATA_DISATTIVAZIONE '                                 ||
                                                   'AND T.NASCOSTO = '||PKG_UtlGlb.gkFlagOFF||' '                           ||
                                                   '#COND_GEO_DGF# '                                                        ||
                                                ')';

    FUNCTION CheckDate(pTab IN INTEGER) RETURN INTEGER AS
        vNum INTEGER;
        vSql VARCHAR2(1000) :=
                'SELECT COUNT(*) FROM DUAL '                                                                                ||
                 'WHERE EXISTS (SELECT 1 '                                                                                  ||
                                 'FROM MISURE_#TAB#_STATICHE M '                                                            ||
                                'INNER JOIN TRATTAMENTO_ELEMENTI T ON (M.COD_TRATTAMENTO_ELEM = T.COD_TRATTAMENTO_ELEM) '   ||
                                'INNER JOIN ' || vTabRel || ' R ON (R.COD_ELEMENTO_PADRE = T.COD_ELEMENTO) '                ||
                                '#JOIN_TIPO_RETE# '                                                                         ||
                                '#JOIN_TIPO_CLIENTE# '                                                                      ||
                                'WHERE :dt BETWEEN M.DATA_ATTIVAZIONE AND M.DATA_DISATTIVAZIONE '                           ||
                                  'AND :dt BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE '                           ||
                                  'AND T.COD_ELEMENTO = :ce '                                                               ||
                                  'AND VALORE <> 0 '                                                                        ||
                                  'AND TIPO_AGGREGAZIONE = :st '                                                            ||
                                  '#FILTRO_TIPO_RETE# '                                                                     ||
                                  '#FILTRO_TIPO_CLIENTE# '                                                                  ||
                                  'AND COD_TIPO_MISURA IN ('''||PKG_Mago.gcPotenzaInstallata||''') '                        ||
                              ') ';
      BEGIN
        IF pTab = 1 THEN
            vSql := REPLACE(vSql,'#TAB#','ACQUISITE');
        ELSE
            vSql := REPLACE(vSql,'#TAB#','AGGREGATE');
        END IF;

        IF vSumRet IS NOT NULL THEN
            vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#','INNER JOIN TIPI_RETE USING(COD_TIPO_RETE) ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#','AND BITAND(:rte,ID_RETE) = ID_RETE ');
        ELSE
            vSql := REPLACE(vSql,'#JOIN_TIPO_RETE#',' ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_RETE#',' ');
        END IF;
        IF vSumPrd IS NOT NULL THEN
            vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#','INNER JOIN TIPI_CLIENTE USING(COD_TIPO_CLIENTE) ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#','AND BITAND(:prd,ID_CLIENTE) = ID_CLIENTE ');
        ELSE
            vSql := REPLACE(vSql,'#JOIN_TIPO_CLIENTE#',' ');
            vSql := REPLACE(vSql,'#FILTRO_TIPO_CLIENTE#',' ');
        END IF;
        CASE
            WHEN (vSumRet IS NOT NULL) AND (vSumPrd IS NOT NULL) THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumRet, vSumPrd;
            WHEN vSumRet IS NOT NULL THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumRet;
            WHEN vSumPrd IS NOT NULL THEN
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete, vSumPrd;
            ELSE
                EXECUTE IMMEDIATE vSql INTO vNum USING pDataRif, pDataRif, pCodElem, vStatoRete;
        END CASE;
        RETURN vNum;
      END;

BEGIN

    InitIdRete;
    InitIdProd;

    --SELECT CASE
    --          WHEN R.COD_ELEMENTO IS NOT NULL THEN COUNT(*)
    --          ELSE 0
    --       END CASE,
    --       MAX(RETE_AT),MAX(RETE_MT),MAX(RETE_BT),
    --       MAX(TAB_MISURE),MAX(GER_ECS),MAX(E.COD_TIPO_ELEMENTO)
    --  INTO vNum, vReteAT, vReteMT, vReteBT, vTab, vCS, vTipEle
    --  FROM ELEMENTI E
    -- LEFT OUTER JOIN (SELECT *
    --                    FROM REL_ELEMENTO_TIPMIS
    --                   WHERE COD_TIPO_MISURA IN (PKG_Mago.gcPotenzaInstallata)
    --                     AND COD_ELEMENTO = pCodElem
    --                 ) R ON (E.COD_ELEMENTO = R.COD_ELEMENTO)
    -- LEFT OUTER JOIN TIPI_MISURA T ON (T.COD_TIPO_MISURA=R.COD_TIPO_MISURA)
    -- INNER JOIN TIPI_ELEMENTO TEL ON(E.COD_TIPO_ELEMENTO = TEL.COD_TIPO_ELEMENTO)
    -- WHERE E.COD_ELEMENTO = pCodElem
    -- GROUP BY R.COD_ELEMENTO;

    BEGIN
        SELECT CASE
                  WHEN R.COD_ELEMENTO IS NOT NULL THEN COUNT(*)
                  ELSE 0
               END CASE,
               MAX(RETE_AT),MAX(RETE_MT),MAX(RETE_BT),
               MAX(TAB_MISURE),MAX(GER_ECS),MAX(E.COD_TIPO_ELEMENTO)
          INTO vNum,vReteAT, vReteMT, vReteBT, vTab, vCS, vTipEle
          FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, E.COD_TIPO_ELEMENTO,GER_ECS
                  FROM ELEMENTI E
                 INNER JOIN TIPI_ELEMENTO T ON(E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO)
                 WHERE COD_ELEMENTO = pCodElem
               ) E
          LEFT OUTER JOIN
               (SELECT COD_ELEMENTO, COD_TIPO_MISURA,
                       CASE
                        WHEN MAX(ACQ) = 0 AND MAX(AGR) = 0 THEN 0
                        WHEN MAX(ACQ) = 1 AND MAX(AGR) = 0 THEN 1
                        WHEN MAX(ACQ) = 0 AND MAX(AGR) = 1 THEN 2
                        WHEN MAX(ACQ) = 1 AND MAX(AGR) = 1 THEN 3
                       END TAB_MISURE,
                       MAX(RETE_AT) RETE_AT,
                       MAX(RETE_MT) RETE_MT,
                       MAX(RETE_BT) RETE_BT
                  FROM (SELECT COD_ELEMENTO, COD_TIPO_MISURA,
                               CASE TIPO_AGGREGAZIONE WHEN 0 THEN 1 ELSE 0 END ACQ,
                               CASE TIPO_AGGREGAZIONE WHEN 0 THEN 0 ELSE 1 END AGR,
                               CASE COD_TIPO_RETE     WHEN PKG_Mago.gcTipReteAT THEN 1 ELSE 0 END RETE_AT,
                               CASE COD_TIPO_RETE     WHEN PKG_Mago.gcTipReteMT THEN 1 ELSE 0 END RETE_MT,
                               CASE COD_TIPO_RETE     WHEN PKG_Mago.gcTipReteBT THEN 1 ELSE 0 END RETE_BT,
                               TIPO_AGGREGAZIONE
                          FROM TRATTAMENTO_ELEMENTI
                         WHERE COD_ELEMENTO = pCodElem
                           AND COD_TIPO_MISURA = PKG_Mago.gcPotenzaInstallata
                        )
                 GROUP BY COD_ELEMENTO, COD_TIPO_MISURA
             ) R ON (E.COD_ELEMENTO = R.COD_ELEMENTO)
       GROUP BY R.COD_ELEMENTO;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN 0;
    END;

    IF pOrganizzaz = PKG_Mago.gcOrganizzazELE THEN
        vStatoRete := pStatoRete;
    ELSE
        vStatoRete := PKG_Mago.gcStatoAttuale;
    END IF;

    IF vCS = PKG_UtlGlb.gkFlagON THEN
        -- a livello di gerarchie di Stato normale e Attuale:
        -- per elementi in gerarchia di CS uso solo lo stato normale
        vStatoRete := PKG_Mago.gcStatoNormale;
    END IF;

    IF vCS = PKG_UtlGlb.gkFlagON THEN
        vTabRel := REPLACE(vTabRel,'#GER#','ECS');
        vTabRel := REPLACE(vTabRel,'#STA#','_SA');
    ELSE
        CASE pOrganizzaz
            WHEN PKG_Mago.gcOrganizzazELE THEN vTabRel := REPLACE(vTabRel,'#GER#','ECP');
            WHEN PKG_Mago.gcOrganizzazAMM THEN vTabRel := REPLACE(vTabRel,'#GER#','AMM');
                                               vTabRel := REPLACE(vTabRel,'#STA#','');
            WHEN PKG_Mago.gcOrganizzazGEO THEN vTabRel := REPLACE(vTabRel,'#GER#','GEO');
                                               vTabRel := REPLACE(vTabRel,'#STA#','');
        END CASE;
        CASE vStatoRete
            WHEN PKG_Mago.gcStatoNormale  THEN vTabRel := REPLACE(vTabRel,'#STA#','_SN');
            WHEN PKG_Mago.gcStatoAttuale  THEN vTabRel := REPLACE(vTabRel,'#STA#','_SA');
        END CASE;
    END IF;

    IF vNum > 0 THEN
        IF NVL(pTipologiaRete,TO_CHAR(PKG_Mago.gcNullNumCode)) = TO_CHAR(PKG_Mago.gcNullNumCode) THEN
            vRet := 9;
            vSumRet := gIDReteAT + gIDReteMT + gIDReteBT;
        ELSE
            vRet := 0;
            IF vReteAT <> 0 THEN
                IF INSTR(pTipologiaRete,PKG_Mago.gcTipReteAT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteAT)) THEN
                    vRet := vRet + gIDReteAT;
                END IF;
            END IF;
            IF vReteMT <> 0 THEN
                IF INSTR(pTipologiaRete,PKG_Mago.gcTipReteMT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteMT)) THEN
                    vRet := vRet + gIDReteMT;
                END IF;
            END IF;
            IF vReteBT <> 0 THEN
                IF INSTR(pTipologiaRete,PKG_Mago.gcTipReteBT) <> 0 OR (pTipologiaRete = TO_CHAR(gIDReteBT)) THEN
                    vRet := vRet +gIDReteBT;
                END IF;
            END IF;
            IF vRet > 0 THEN
                vSumRet := vRet;
            END IF;
        END IF;

        IF pTipiCliente IS NOT NULL THEN
            vSumPrd := 0;
            IF INSTR(pTipiCliente,PKG_Mago.gcClienteProdPuro) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdPuro;
            END IF;
            IF INSTR(pTipiCliente,PKG_Mago.gcClienteAutoProd) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdCliente;
            END IF;
            IF INSTR(pTipiCliente,PKG_Mago.gcClientePrdNonDeterm) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdNonDeterm;
            END IF;
            IF INSTR(pTipiCliente,PKG_Mago.gcClientePrdNonApplic) <> 0 THEN
                vSumPrd := vSumPrd + gIDProdNonApplic;
            END IF;
        END IF;
        IF vTab = 1 THEN
            IF CheckDate(1) = 0 THEN
                vRet := 0;
            END IF;
        ELSE
            IF CheckDate(2) = 0 THEN
                vRet := 0;
            END IF;
        END IF;

    END IF;

    -- verifica se foglia

    IF vTipEle = PKG_Mago.gcTrasformMtBt THEN
        -- Il dettaglio di trasformatore Mt/Bt e' nascosto;
        -- uso query differente per eliminare eventuali tipi elementi da nascondere
        -- e visualizzare eventuali altre dipendenze
        vSqlFoglia := vSqlFoglia2;
    END IF;

    IF vCS = PKG_UtlGlb.gkFlagON THEN
        vSqlFoglia := REPLACE(vSqlFoglia,'#COND_GEO_DGF#','AND FLAG_GEO IN (CASE '||pOrganizzaz||' WHEN '||PKG_Mago.gcOrganizzazGEO||' THEN 1 ELSE 0 END, 2)');
    ELSE
        vSqlFoglia := REPLACE(vSqlFoglia,'#COND_GEO_DGF#', '');
    END IF;

    vSqlFoglia := REPLACE(vSqlFoglia,'#TABREL#', vTabRel);
    EXECUTE IMMEDIATE vSqlFoglia INTO vNum USING pCodElem, pDataRif;

    IF (vNum = 0) OR (vTipEle = PKG_Mago.gcTrasformMtBt) THEN
--    IF vNum = 0 THEN
        vRet := vRet + 10;
    END IF;

    -- ritorna il flag
    RETURN vRet;

END GetElemInfo;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElementsRegistry(pRefCurs      OUT PKG_UtlGlb.t_query_cur,
                               pGestElem      IN VARCHAR2,
                               pData          IN DATE DEFAULT SYSDATE,
                               pStatoRete     IN INTEGER DEFAULT PKG_Mago.gcStatoAttuale,
                               pDisconnect    IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con il dettaglio dei codici gestionali ricevuti
-----------------------------------------------------------------------------------------------------------*/

    vCodList  GTTD_VALORI_TEMP.TIP%TYPE := 'CG_LIST';
    vFlgNull  NUMBER(1) := -1;
    
    vSql      VARCHAR2(1000) := 
                'SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,'                              ||
                       'COD_TIPO_ELEMENTO,CODIFICA_ST_TIPO_ELEMENTO '                               ||
                  'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,RIF_ELEMENTO,NOME_ELEMENTO,'         ||
                               'COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO '           ||
                          'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,RIF_ELEMENTO,NOME_ELEMENTO,' ||
                                       'E.COD_TIPO_ELEMENTO,'                                       ||
                                       'ROW_NUMBER() OVER (PARTITION BY COD_ELEMENTO '              ||
                                                'ORDER BY COD_ELEMENTO,DATA_ATTIVAZIONE DESC) ORD ' ||
                                  'FROM ELEMENTI E '                                                ||
                                 'INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO) '                   ||
                                 'WHERE COD_GEST_ELEMENTO IN (SELECT ALF1 COD_GEST_ELEMENTO '       ||
                                                               'FROM GTTD_VALORI_TEMP '             ||
                                                              'WHERE TIP = '''||vCodList||''')'     ||
                                ') '                                                                ||
                         'INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO) '                       ||
                          'WHERE ORD = 1 '                                                          ||
                       ') '                                                                         ||
                 'INNER JOIN (SELECT g.*, #DATAELECALC# data_disattivazione_calc FROM GERARCHIA_IMP_#STATO# G ) USING (COD_ELEMENTO) ' ||
                 'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                 ||
                   'ORDER BY COD_GEST_ELEMENTO' ;
    
BEGIN

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vCodList;

    PKG_Mago.TrattaListaCodici(pGestElem,vCodList, vFlgNull); -- non gestito il flag
    
    IF pStatoRete = PKG_Mago.gcStatoNormale THEN
        vSql := REPLACE(vSql,'#STATO#','SN');
    ELSE
        vSql := REPLACE(vSql,'#STATO#','SA');
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    
    OPEN pRefCurs FOR vSql USING NVL(pData,SYSDATE);

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElementsRegistry'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetElementsRegistry;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetElements       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pCodElem        IN ELEMENTI.COD_ELEMENTO%TYPE,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL,
                             pElemTypes      IN VARCHAR2 DEFAULT NULL,
                             pCompilaInfo    IN INTEGER  DEFAULT Pkg_Mago.gcON,
                             pCalcOutCodGest IN INTEGER  DEFAULT 1,
                             PDisconnect IN INTEGER DEFAULT 0) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna il cursore con la struttura degli elementi all'istante richiesto (da codice elemento )
-----------------------------------------------------------------------------------------------------------*/
    vTipTmp        GTTD_VALORI_TEMP.TIP%TYPE := 'TipEleLst0';
    vElemento      ELEMENTI.COD_ELEMENTO%TYPE;
    vGestionale    ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNome          ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vGerarchia     INTEGER;
    vLivello       INTEGER;
    vElemTypeLst   PKG_UtlGlb.t_SplitTbl := PKG_UtlGlb.t_SplitTbl();
    vTipologiaRete VARCHAR2(100) := pTipologiaRete;
    vDataRif       DATE := GetFETreeRefDate(pDataDa,pDataA);
    vSeparatore    CHAR(1);
    vStatoRete     INTEGER := pStatoRete;

    vSql1 VARCHAR2(2000) :=
          'SELECT COD_ELEMENTO,#FLAG_INFO# FLAG_CLIENTE,'                                                             ||
                 'NVL(SUBSTR(A.COD_GEST_ELEMENTO,INSTR(A.COD_GEST_ELEMENTO,'''||vSeparatore||''')+1),'                   ||
                                                                              'A.COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,'  ||
                 'NVL(NOME_ELEMENTO,'' '') NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG '||
            'FROM (SELECT E.COD_ELEMENTO,E.NOME_ELEMENTO,COD_GEST_ELEMENTO,'                                             ||
                         'E.COD_TIPO_ELEMENTO,E.CODIFICA_ST,LEVEL liv,E.FLAG '                                           ||
                    'FROM (SELECT R.COD_ELEMENTO_PADRE,R.COD_ELEMENTO,E.COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,'          ||
                                 'T.CODIFICA_ST,NOME_ELEMENTO,E.FLAG, '                                                  ||
                            'FROM (SELECT R.COD_ELEMENTO_PADRE,R.COD_ELEMENTO_FIGLIO COD_ELEMENTO '                      ||
                                    'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA# R) R '                                                    ||
                                   'WHERE :pDa BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE_CALC '               || --vDataRif
                                   '#COND_GEO_DGF# '                                                                     ||
                                  ') R'                                                                                  ||
                            'INNER JOIN '                                                                                ||
                                 '(SELECT E.COD_ELEMENTO,D.NOME_ELEMENTO,'                                               ||
                                         'CASE '||pCalcOutCodGest||' WHEN 1 '                                            ||
                                                    'THEN PKG_Elementi.CalcOutCodGest(E.COD_GEST_ELEMENTO,'              ||
                                                                                     ':dt,E.COD_TIPO_ELEMENTO) '         || --vDataRif
                                                    'ELSE E.COD_GEST_ELEMENTO '                                          ||
                                         'END COD_GEST_ELEMENTO '                                                        ||
                                         'D.COD_TIPO_ELEMENTO,FLAG  '                                                    ||
                                    'FROM ELEMENTI_DEF D '                                                               ||
                                   'INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO) '                         ||
                                   'WHERE :pDa BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE '                    || --vDataRif
                                 ') E ON (E.COD_ELEMENTO = R.COD_ELEMENTO) '                                             ||
                           'INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO) '                  ||
                           'WHERE GER_#GER# <> 0 '                                                                       ||
                             'AND NASCOSTO <> 1 '                                                                        ||
                         ') E START WITH COD_ELEMENTO_PADRE = :ele '                                                     || --vElemento
                           'CONNECT BY PRIOR E.COD_ELEMENTO = COD_ELEMENTO_PADRE '                                       ||
                 ') A '                                                                                                  ||
           'WHERE liv = :liv ';                                                                                             --vLivello
    vSql2 VARCHAR2(2000) :=
          'SELECT COD_ELEMENTO_FIGLIO COD_ELEMENTO,#FLAG_INFO# FLAG_CLIENTE,'                                         ||
                 'NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,'''||vSeparatore||''')+ 1),'                      ||
                                                                                'COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,'  ||
                 'NVL(NOME_ELEMENTO,'' '') NOME_ELEMENTO,'                                                               ||
                 'E.COD_TIPO_ELEMENTO,T.CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,E.FLAG '                                   ||
            'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA# R ) R '                                                                            ||
           'INNER JOIN (SELECT D.COD_ELEMENTO,NOME_ELEMENTO,'                                                            ||
                         'CASE '||pCalcOutCodGest||' WHEN 1 THEN PKG_Elementi.CalcOutCodGest(E.COD_GEST_ELEMENTO,'       ||
                                                                                            ':dt,E.COD_TIPO_ELEMENTO) '  || --vDataRif
                                                    'ELSE E.COD_GEST_ELEMENTO '                                          ||
                         'END COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,D.FLAG '                                             ||
                         'FROM ELEMENTI_DEF D '                                                                          ||
                        'INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO) '                                    ||
                        'WHERE :pDa BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE '                               || --vDataRif
                       ') E ON E.COD_ELEMENTO=R.COD_ELEMENTO_FIGLIO '                                                    ||
           'INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO=T.COD_TIPO_ELEMENTO '                                      ||
           'WHERE R.COD_ELEMENTO_PADRE=:Ele '                                                                            || --vElemento
             '#COND_GEO_DGF# '                                                                                           ||
             'AND NASCOSTO <> 1 '                                                                                        ||
             'AND :pDa BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE_CALC ';                                              --vDataRif

        PROCEDURE NullCursor AS
            BEGIN
                OPEN pRefCurs FOR SELECT NULL COD_ELEMENTO,
                                         NULL FLAG_CLIENTE,
                                         NULL COD_GEST_ELEMENTO,
                                         NULL NOME_ELEMENTO,
                                         NULL COD_TIPO_ELEMENTO,
                                         NULL CODIFICA_ST_TIPO_ELEMENTO,
                                         NULL FLAG
                                    FROM DUAL WHERE DUMMY = '<NO SELECT>';
            END;
        FUNCTION CompletaSql(pSql VARCHAR2, pGerarchia INTEGER, pStato INTEGER) RETURN VARCHAR2 AS
                vSql VARCHAR2(4000) := pSql;
                vGeoCond VARCHAR2(200) := '';
            BEGIN
                CASE pGerarchia
                    WHEN cGerarchiaELE_CP THEN
                         vSql := REPLACE(vSql,'#TAB#','ECP');
                    WHEN cGerarchiaELE_CS THEN
                         vSql := REPLACE(vSql,'#TAB#','ECS');
                         vGeoCond := 'AND FLAG_GEO IN ( CASE '||pOrganizzazione||' WHEN '||Pkg_Mago.gcOrganizzazGEO||' THEN 1 ELSE 0 END, 2)';
                    WHEN cGerarchiaGEO    THEN vSql := REPLACE(vSql,'#TAB#','GEO');
                                               vSql := REPLACE(vSql,'#STA#','');
                    WHEN cGerarchiaAMM    THEN vSql := REPLACE(vSql,'#TAB#','AMM');
                                               vSql := REPLACE(vSql,'#STA#','');
                END CASE;
                IF (pGerarchia = cGerarchiaELE_CP) OR (vGerarchia = cGerarchiaELE_CS) THEN
                    CASE vStatoRete
                        WHEN Pkg_Mago.gcStatoNormale    THEN vSql := REPLACE(vSql,'#STA#','_SN');
                        WHEN Pkg_Mago.gcStatoAttuale    THEN vSql := REPLACE(vSql,'#STA#','_SA');
                    END CASE;
                END IF;
                vSql := REPLACE(vSql,'#COND_GEO_DGF#',vGeoCond);
                RETURN vSql;
            END;
BEGIN

    --InitMagoSession;
    IF Pkg_Mago.IsMagoSTM AND pOrganizzazione <> Pkg_Mago.gcOrganizzazELE THEN
        vStatoRete  := Pkg_Mago.gcStatoAttuale;
    END IF;

    IF Pkg_Mago.IsMagoDGF THEN
        vSeparatore := CHR(11);
    ELSE
        vSeparatore := Pkg_Mago.cSeparatore;
    END IF;

    IF NVL(pCodElem,Pkg_Mago.gcNullNumCode) = Pkg_Mago.gcNullNumCode THEN
        vElemento := GetElementoBase;
        IF Pkg_Mago.IsNazionale THEN
            --vCodEleTmp('GetElements - Nazionale ');
            OPEN pRefCurs FOR
               SELECT COD_ELEMENTO,PKG_Elementi.GetElemInfo(COD_ELEMENTO,vDataRif,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete) FLAG_CLIENTE,
                      NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,vSeparatore) + 1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,NOME_ELEMENTO,
                      E.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG
                 FROM (SELECT D.COD_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO,FLAG,NOME_ELEMENTO,COD_TIPO_FONTE
                         FROM ELEMENTI_DEF D
                        INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO)
                       WHERE vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
                      ) E
                INNER JOIN TIPI_ELEMENTO T ON E.COD_TIPO_ELEMENTO=T.COD_TIPO_ELEMENTO
                WHERE COD_ELEMENTO = vElemento;
            RETURN;
        ELSE
            --PRINT('GetElements - Non Nazionale ');
            vSql1 := 'SELECT A.COD_ELEMENTO,PKG_Elementi.GetElemInfo(A.COD_ELEMENTO,:dt,:tr,:tp,:org,:st) FLAG_CLIENTE,' ||
                            'NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,'''||vSeparatore||''')+1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,' ||
                            'NOME_ELEMENTO,A.COD_TIPO_ELEMENTO,CODIFICA_ST CODIFICA_ST_TIPO_ELEMENTO,FLAG ' ||
                       'FROM (SELECT COD_ELEMENTO_FIGLIO COD_ELEMENTO, COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO ' ||
                               'FROM REL_ELEMENTI_#TAB##STA# R ' ||
                              'INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_FIGLIO ' ||
                              'WHERE :dt BETWEEN R.DATA_ATTIVAZIONE AND R.DATA_DISATTIVAZIONE ' ||
                                 'AND COD_ELEMENTO_PADRE = :el ' ||
                                 '#COND_GEO_DGF# ' ||
                            ') A ' ||
                      'INNER JOIN ELEMENTI_DEF D ON (A.COD_ELEMENTO = D.COD_ELEMENTO) ' ||
                      'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = A.COD_TIPO_ELEMENTO ' ||
                      'WHERE :dt BETWEEN D.DATA_ATTIVAZIONE AND D.DATA_DISATTIVAZIONE ' ||
                      'ORDER BY NVL(FLAG,-1) DESC, NLSSORT(NOME_ELEMENTO,''NLS_SORT=BINARY''), COD_GEST_ELEMENTO';
            GetGerarchiaID (vElemento, pOrganizzazione, vGerarchia, vLivello);
            vSql1 := CompletaSql(vSql1,vGerarchia,vStatoRete);
            OPEN pRefCurs FOR vSql1 USING vDataRif, pTipologiaRete, pTipoClie, pOrganizzazione, vStatoRete, vDataRif, vElemento, vDataRif;

            RETURN;
        END IF;
    ELSE
        vElemento := pCodElem;
    END IF;
    BEGIN
        GetGerarchiaID (vElemento, pOrganizzazione, vGerarchia, vLivello);
        --PRINT('GetElements - GetGerarchiaID = '||vGerarchia||'  -  Livello = '||vLivello||'  per elemento '||vElemento);
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            --PRINT('GetElements - GetGerarchiaID non deterrminabile - Elemento = '||vElemento);
            NullCursor;
            OPEN pRefCurs FOR SELECT NULL,NULL,NULL,NULL,NULL,NULL,NULL FROM DUAL WHERE DUMMY = '<NO SELECT>';
            RETURN;
    END;
    IF (vGerarchia = cGerarchiaGEO_CS) OR (vGerarchia = cGerarchiaAMM_CS) THEN
        vGerarchia := cGerarchiaELE_CS;
    END IF;
    IF pElemTypes IS NOT NULL THEN
        DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vTipTmp;
        vElemTypeLst := PKG_UtlGlb.SplitString(TRIM(pElemTypes),',');
        FOR i IN vElemTypeLst.FIRST .. vElemTypeLst.LAST LOOP
            INSERT INTO GTTD_VALORI_TEMP (TIP,ALF1) VALUES (vTipTmp,vElemTypeLst(1));
        END LOOP;
    END IF;
    IF NVL(vLivello,1) = 1 THEN
        vSql1 := vSql2;
    END IF;

    vSql1 := CompletaSql(vSql1,vGerarchia,vStatoRete);

    IF pElemTypes IS NOT NULL THEN
        vSql1 := 'SELECT A.* FROM (' || vSql1 ||
                                  ') A INNER JOIN GTTD_VALORI_TEMP ON ALF1 = COD_TIPO_ELEMENTO ' ||
                          'WHERE TIP = ''#TIP#'' ';
        vSql1 := REPLACE(vSql1,'#TIP#',vTipTmp);
    END IF;
    IF pCompilaInfo = PKG_Mago.gcON THEN
        vSql1 := REPLACE(vSql1,'#FLAG_INFO#','PKG_Elementi.GetElemInfo(COD_ELEMENTO,:dt,:tr,:tp,:org,:st)');
    ELSE
        vTipologiaRete := TO_CHAR(PKG_Mago.gcNullNumCode);
        vSql1 := REPLACE(vSql1,'#FLAG_INFO#',':TR');
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql1 := REPLACE (vSql1, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql1:= REPLACE (vSql1, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    --vSql1 := 'SELECT * FROM ('||vSql1||') ' ||
    --         ' WHERE CASE WHEN '||PKG_UtlGlb.BooleanToFlag(PKG_Mago.IsMagoSTM)||' = 0 THEN 1 '        ||
    --                     'WHEN COD_TIPO_ELEMENTO NOT IN ('''||PKG_Mago.gcTrasformMtBt||''','          ||
    --                                                    ''''||PKG_Mago.gcClienteAT||''','             ||
    --                                                    ''''||PKG_Mago.gcClienteMT||''','             ||
    --                                                    ''''||PKG_Mago.gcClienteBT||''')  THEN 1 '    ||
    --                     'ELSE CASE WHEN FLAG_CLIENTE >= 10 THEN FLAG_CLIENTE - 10 '                  ||
    --                               'ELSE FLAG_CLIENTE '                                               ||
    --                          'END '                                                                  ||
    --                'END <> 0 ';
    DBMS_OUTPUT.PUT_LINE (vsql1);
    IF NVL(vLivello,1) = 1 THEN
        vSql1 := vSql1 || ' ORDER BY CASE NVL(COD_ELEMENTO,0)WHEN 0 THEN 1 ELSE 0 END, NLSSORT(UPPER(NOME_ELEMENTO),''NLS_SORT=BINARY''),COD_GEST_ELEMENTO ';
        IF pCompilaInfo = PKG_Mago.gcON THEN
            OPEN pRefCurs FOR vSql1 USING vDataRif,vTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete,vDataRif,vDataRif,vElemento,vDataRif;
        ELSE
            OPEN pRefCurs FOR vSql1 USING TO_NUMBER(vTipologiaRete),vDataRif,vDataRif,vElemento,vDataRif;
        END IF;
    ELSE
        CASE vGerarchia
            WHEN cGerarchiaGEO    THEN vSql1 := REPLACE(vSql1,'#GER#','GEO');
            WHEN cGerarchiaAMM    THEN vSql1 := REPLACE(vSql1,'#GER#','AMM');
            WHEN cGerarchiaELE_CP THEN vSql1 := REPLACE(vSql1,'#GER#','ECP');
            WHEN cGerarchiaELE_CS THEN vSql1 := REPLACE(vSql1,'#GER#','ECS');
        END CASE;
        vSql1 := vSql1 || ' ORDER BY NLSSORT(NOME_ELEMENTO,''NLS_SORT=BINARY''),COD_GEST_ELEMENTO ';
        IF pCompilaInfo = PKG_Mago.gcON THEN
            OPEN pRefCurs FOR vSql1 USING vDataRif,vTipologiaRete,pTipoClie,vDataRif,vDataRif,vDataRif,vElemento,vLivello;
        ELSE
            OPEN pRefCurs FOR vSql1 USING vTipologiaRete,vDataRif,vDataRif,vDataRif,vElemento,vLivello;
        END IF;
    END IF;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt('Codice Elemento: '||NVL(TO_CHAR(pCodElem),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Pariodo Selez. : '||PKG_Mago.StdOutDate(pDataDa)||' - '||PKG_Mago.StdOutDate(pDataA),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Data Riferim.  : '||PKG_Mago.StdOutDate(vDataRif),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Organizzazione : '||NVL(TO_CHAR(pOrganizzazione),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('StatoRete      : '||NVL(TO_CHAR(pStatoRete),'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('TipologiaRete  : '||NVL(pTipologiaRete,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Tipi Produtt.  : '||NVL(pTipoClie,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Tipi Elemento  : '||NVL(pElemTypes,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElements'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         NullCursor;
         RETURN;
END GetElements;

-- ----------------------------------------------------------------------------------------------------------

PROCEDURE FindElement       (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pGestElem       IN VARCHAR2,
                             pDataDa         IN DATE,
                             pDataA          IN DATE,
                             pOrganizzazione IN INTEGER,
                             pStatoRete      IN INTEGER,
                             pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                             pTipoClie       IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
    Localizza un codice gestionale nella gerarchia/stato indicate e restituisce la catena di elementi
    necessaria a raggiungere l'elemento stesso
-----------------------------------------------------------------------------------------------------------*/

    vSearchStr  VARCHAR2(100) := pGestElem;
    vDataRif    DATE := GetFETreeRefDate(pDataDa,pDataA);
    vTrovato    BOOLEAN := FALSE;
    vGerarchia  INTEGER;
    vLivello    INTEGER;
    vTipTmp     GTTD_VALORI_TEMP.TIP%TYPE := 'FindElem';
    vLiv        INTEGER;
    vNum        NUMBER;
    vTipEleRoot TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vCodEleRoot ELEMENTI.COD_ELEMENTO%TYPE;
    vCodInizio  ELEMENTI.COD_ELEMENTO%TYPE;
    vCodEleTmp  ELEMENTI.COD_ELEMENTO%TYPE;
    vFlg        INTEGER;
    vGst        ELEMENTI.COD_GEST_ELEMENTO%TYPE;
    vNom        ELEMENTI_DEF.NOME_ELEMENTO%TYPE;
    vRif        ELEMENTI_DEF.RIF_ELEMENTO%TYPE;
    vTip        TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;
    vTst        TIPI_ELEMENTO.CODIFICA_ST%TYPE;
    vSeq        INTEGER := 0;
    vFlagEse    ELEMENTI_DEF.FLAG%TYPE;
    vOrd        GTTD_VALORI_TEMP.NUM5%TYPE;
    vStatoRete  INTEGER := pStatoRete;
    vSeparatore CHAR(1);
    vRel1       VARCHAR2(200) := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_#TAB##STA1# '                                        ||
                                  'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE';              --vDataRif
    vRel2       VARCHAR2(400) := 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_#TAB##STA1# '                                        ||
                                   'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '          || --vDataRif
                                  'UNION ALL '                                                             ||
                                 'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                         ||
                                   'FROM REL_ELEMENTI_ECS#STA2# '                                          ||
                                  'WHERE :pDa BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE '           || --vDataRif
                                    'AND FLAG_GEO IN ( CASE '||pOrganizzazione||' '                        ||
                                                        'WHEN '||PKG_Mago.gcOrganizzazGEO||' THEN 1 '      ||
                                                        'ELSE 0 '                                          ||
                                                       'END, 2)';
    vSqlSelElem VARCHAR2(800) := 'SELECT A.COD_ELEMENTO_PADRE, A.LIV, E.COD_TIPO_ELEMENTO '                ||
                                   'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO, LEVEL liv '       ||
                                           'FROM (#REL#) '                                                 ||
                                                'START WITH COD_ELEMENTO_FIGLIO=:ele '                     || --vCodInizio
                                              'CONNECT BY PRIOR COD_ELEMENTO_PADRE = COD_ELEMENTO_FIGLIO ' ||
                                        ') A '                                                             ||
                                  'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = COD_ELEMENTO_PADRE '          ||
                                  'INNER JOIN TIPI_ELEMENTO T ON(E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO) '; --||
                                  --' ORDER BY LIV ';
    vSqlInsTemp VARCHAR2(800) := 'INSERT INTO GTTD_VALORI_TEMP ( TIP, NUM1, NUM2) '                        ||
                                   'SELECT '''||vTipTmp||'2'','                                            ||
                                          'ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY NUM1 DESC) ORD,'     ||
                                          'NUM2 COD_ELEMENTO '                                             ||
                                     'FROM (SELECT TIP,NUM1,NUM2 FROM GTTD_VALORI_TEMP '                   ||
                                            'WHERE TIP = '''||vTipTmp||'1'')  A '                          ||
                                     'LEFT OUTER JOIN (SELECT * FROM REL_ELEMENTI_#GER##STA1# '            ||
                                                       'WHERE :pDa BETWEEN NVL(DATA_ATTIVAZIONE,:pDa) '    || --vDataRif,vDataRif
                                                                      'AND NVL(DATA_DISATTIVAZIONE,:pDa) ' || --vDataRif
                                                      ') ON COD_ELEMENTO_FIGLIO = NUM2 '                   ||
                                     'LEFT OUTER JOIN ELEMENTI E ON E.COD_ELEMENTO = COD_ELEMENTO_PADRE '  ||
                                 'ORDER BY TIP, NUM1 DESC, NUM2 ';

--    vSqlInsTemp VARCHAR2(800) := 'INSERT INTO GTTD_VALORI_TEMP ( TIP, NUM1, NUM2) '                        ||
--                                    'SELECT '''||vTipTmp||'2'','                                           ||
--                                           'ROW_NUMBER() OVER (PARTITION BY 1 ORDER BY NUM1 DESC) ORD,'    ||
--                                           'NUM2 COD_ELEMENTO '                                            ||
--                                      'FROM GTTD_VALORI_TEMP A '                                           ||
--                                      'LEFT OUTER JOIN REL_ELEMENTI_#GER##STA1# ON COD_ELEMENTO_FIGLIO = NUM2 ' ||
--                                      'LEFT JOIN ELEMENTI E ON E.COD_ELEMENTO = COD_ELEMENTO_PADRE '           ||
--                                      'LEFT JOIN TIPI_ELEMENTO T ON (E.COD_TIPO_ELEMENTO = T.COD_TIPO_ELEMENTO) ' ||
--                                     'WHERE TIP = '''||vTipTmp||'1'' '                                     ||
--                                       'AND NVL(GER_#GER#,0) <= 1 '                                        ||
--                                       'AND :pDa BETWEEN NVL(DATA_ATTIVAZIONE,:pDa) '                      || --vDataRif
--                                                    'AND NVL(DATA_DISATTIVAZIONE,:pDa) '                   || --vDataRif
--                                     'ORDER BY TIP, NUM1 DESC , NUM2' ;



    vGeoCond VARCHAR2(200);
    PROCEDURE OpenCursor (pCurs OUT PKG_UtlGlb.t_query_cur, pTipTmp IN VARCHAR2) AS
            vSql VARCHAR2(650) := 'SELECT NUM2 COD_ELEMENTO, NUM3 FLAG_CLIENTE, '                                   ||
                                         'CASE '                                                                       ||
                                            'WHEN ALF5 IS NOT NULL THEN ALF5 '                                         ||
                                            'ELSE SUBSTR(ALF1,INSTR(ALF1,'''||vSeparatore||''')+1) '                   ||
                                         'END COD_GEST_ELEMENTO, '                                                     ||
                                         'ALF2 NOME_ELEMENTO,'                                                         ||
                                         'ALF3 COD_TIPO_ELEMENTO,'                                                     ||
                                         'ALF4 CODIFICA_ST_TIPO_ELEMENTO,'                                             ||
                                         'NUM1 - :liv LIVELLO, :org ORGANIZZAZIONE '                                   || -- decr livello, pOrganizzazione
                                    'FROM (SELECT A.*,SUM(CASE ALF3 WHEN :te THEN 1 ELSE 0 END '                       || -- tipo elemento root
                                                        ') OVER (PARTITION BY TIP ) ROOT '                             ||
                                                'FROM (SELECT * FROM GTTD_VALORI_TEMP '                                ||
                                                      '#UNION_NAZ#) A '                                                || -- pTipTmp (per nazionale)
                                               'WHERE TIP = NVL(:tip,''<null>'')'                                      || -- pTipTmp
                                         ')'                                                                           ||
                                   'WHERE ROOT = 1 '                                                                   ||
                                   'ORDER BY CASE WHEN COD_TIPO_ELEMENTO = :te THEN 0 ELSE 1 END,NVL(NUM5,-1) DESC,NUM4' ;
        BEGIN
            IF PKG_Mago.isNazionale THEN
                -- aggiunge root fittizia che sara' tolta del FE
                vSql := REPLACE(vSql,'#UNION_NAZ#','UNION ALL SELECT :tip,0,0,''?'',''?'',''-'||vSeparatore||'-'',0,-1 FROM DUAL');
                --PRINT('Nazionale - org='|| pOrganizzazione||' - TipEleRoot='||vTipEleRoot||' - TipTmp='||pTipTmp||CHR(10)||vSql);
                OPEN pCurs FOR vSql USING 0, pOrganizzazione, vTipEleRoot, pTipTmp, pTipTmp, vTipEleRoot;
            ELSE
                vSql := REPLACE(vSql,'#UNION_NAZ#','');
                --PRINT('NON Nazionale - org='|| pOrganizzazione||' - TipEleRoot='||vTipEleRoot||' - TipTmp='||pTipTmp||CHR(10)||vSql);
                OPEN pCurs FOR vSql USING 1, pOrganizzazione, vTipEleRoot, pTipTmp, vTipEleRoot;
            END IF;
        END;
    PROCEDURE AddElem (pLiv NUMBER, pCod ELEMENTI.COD_ELEMENTO%TYPE) AS
        BEGIN
            INSERT INTO GTTD_VALORI_TEMP (TIP,NUM1,NUM2) VALUES (vTipTmp||'1',pLiv,pCod);
        END;

    PROCEDURE GetFigli (pLiv NUMBER, pCod ELEMENTI.COD_ELEMENTO%TYPE,pTmp GTTD_VALORI_TEMP.TIP%TYPE) AS
        vCur  PKG_UtlGlb.t_query_cur;
        vEle  ELEMENTI.COD_ELEMENTO%TYPE;
        vCod  ELEMENTI.COD_ELEMENTO%TYPE;
        vTmp  ELEMENTI.COD_GEST_ELEMENTO%TYPE;
        BEGIN
            BEGIN
                --PRINT('GetFigli - tento di recuperare il livello '||pLiv||' per il cod '|| pCod||' - tmp='||pTmp);
                SELECT NUM2 INTO vCod FROM GTTD_VALORI_TEMP WHERE NUM1 = pLiv AND TIP = pTmp;
                --PRINT('GetFigli - a livello '||pLiv||' recupero il cod '|| vCod);
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                    --PRINT('GetFigli - non ho recuperato il livello '||pLiv||' per il cod '|| pCod||' - tmp='||pTmp);
                    RETURN;
            END;
            --PRINT('GetFigli - GetElements - Cod='||pCod);
            GetElements(vCur,pCod,vDataRif,vDataRif,pOrganizzazione,vStatoRete,pTipologiaRete,pTipoClie,NULL,PKG_Mago.gcON,0);
            --PRINT('GetFigli - GetElements - fine');
            LOOP
                FETCH vCur INTO vEle,vFlg,vGst,vNom,vTip,vTst,vFlagEse;
                EXIT WHEN vCur%NOTFOUND;
                vSeq := vSeq + 1;
                IF vTip = PKG_Mago.gcEsercizio THEN
                    vOrd := vFlagEse;
                END IF;
                --PRINT(vSeq||' - ele='||vEle||' - tip='||vTip||' - tst='||vTst||' - gst='||vGst||' - pGestElem='||pGestElem);
                IF PKG_Mago.IsMagoDGF THEN
                    vTmp := NVL(SUBSTR(vSearchStr,INSTR(vSearchStr,vSeparatore)+1),vSearchStr);
                ELSE
                    vTmp := vSearchStr;
                END IF;
                IF vGst = vTmp THEN
                    vTrovato := TRUE;
                    --PRINT('>>>>> TROVATO <<<<<<<');
                END IF;
                IF vEle = vCod THEN
                   --PRINT('        cerco i figli di '||vEle);
                   UPDATE GTTD_VALORI_TEMP SET NUM3 = vFlg,
                                               ALF1 = vGst,
                                               ALF2 = vNom,
                                               ALF3 = vTip,
                                               ALF4 = vTst,
                                               ALF5 = PKG_Elementi.CalcOutCodGest(SUBSTR(vGst,INSTR(vGst,vSeparatore)+1),vDataRif,vTip),
                                               NUM4 = vSeq,
                                               NUM5 = vOrd
                    WHERE TIP  = pTmp
                      AND NUM2 = vEle
                      AND NUM1 = pLiv;
                    GetFigli(pLiv + 1, vEle, pTmp);
                ELSE
                   INSERT INTO GTTD_VALORI_TEMP (TIP ,NUM1,NUM2,NUM3,NUM4,NUM5,
                                                      ALF1,ALF2,ALF3,ALF4,ALF5)
                                         VALUES (pTmp,pLiv,vEle,vFlg,vSeq,vOrd,
                                                      vGst,vNom,vTip,vTst,PKG_Elementi.CalcOutCodGest(SUBSTR(vGst,INSTR(vGst,vSeparatore)+1),vDataRif,vTip));
                END IF;
            END LOOP;
            CLOSE vCur;
        END;

BEGIN

    --PKG_Logs.TraceLog('FindElement     >'||vSearchStr||'< - '||TO_CHAR(pDataA,'dd/mm/yyyy hh24:mi:ss')||
    --                  ' - Org='||pOrganizzazione||' - Sta='||pStatoRete||' - '||
    --                  NVL(pTipologiaRete,'<null>')||' - '||NVL(pTipoClie,'<null>'));  

    IF PKG_Mago.IsMagoSTM AND pOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
        vStatoRete  := PKG_Mago.gcStatoAttuale;
    END IF;

    IF PKG_Mago.IsMagoDGF THEN
        vSeparatore := CHR(11);
    ELSE
        vSeparatore := PKG_Mago.cSeparatore;
    END IF;

    IF PKG_Mago.isNazionale THEN
        vTipEleRoot := PKG_Mago.gcNazionale;
    ELSE
        vTipEleRoot := PKG_Mago.gcCentroOperativo;
    END IF;

    BEGIN
        BEGIN
            SELECT COD_ELEMENTO INTO vCodInizio
              FROM ELEMENTI
             WHERE COD_GEST_ELEMENTO = vSearchStr;
        EXCEPTION
            WHEN NO_DATA_FOUND THEN
                SELECT COD_ELEMENTO,COD_GEST_ELEMENTO
                  INTO vCodInizio, vSearchStr
                  FROM ELEMENTI
                 INNER JOIN ELEMENTI_DEF D USING(COD_ELEMENTO)
                 WHERE D.RIF_ELEMENTO = vSearchStr
                   AND vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
        END;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
            BEGIN
                IF PKG_Mago.IsMagoDGF AND pOrganizzazione = PKG_Mago.gcOrganizzazGEO THEN
                    BEGIN
                        -- inizio forzatura per DGF
                        -- cerca il primo elemento con codice gestionale con 2^ parte del codice ricevuto corrispondente
                        SELECT MIN(COD_GEST_ELEMENTO) INTO vSearchStr
                          FROM ELEMENTI E
                         INNER JOIN ELEMENTI_DEF D ON (D.COD_ELEMENTO = E.COD_ELEMENTO)
                         WHERE SUBSTR(E.COD_GEST_ELEMENTO,INSTR(E.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore)+1) = SUBSTR(vSearchStr,INSTR(vSearchStr,PKG_Mago.cSeparatore)+1)
                           AND INSTR(E.COD_GEST_ELEMENTO,PKG_Mago.cSeparatore) > 0
                           AND vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;
                        -- fine  forzatura per DGF
                        vCodInizio := GetCodElemento(vSearchStr);
                    EXCEPTION
                        WHEN NO_DATA_FOUND THEN vCodInizio := NULL;
                    END;
                END IF;
                IF vCodInizio IS NULL THEN
                    SELECT MIN(COD_ELEMENTO)
                      INTO vCodInizio
                      FROM (SELECT DISTINCT COD_ELEMENTO
                              FROM V_SEARCH_ELEMENTS
                             WHERE SEARCH_STRING LIKE LOWER(vSearchStr||'%')
                           )
                     HAVING COUNT(*) = 1;
                    SELECT COD_GEST_ELEMENTO
                      INTO vSearchStr
                     FROM ELEMENTI
                    WHERE COD_ELEMENTO = vCodInizio;
                END IF;
            EXCEPTION
                WHEN NO_DATA_FOUND THEN
                     OpenCursor(pRefCurs,NULL);
                     --PRINT('GetFigli - Chiudo per NO_DATA_FOUND!');
                     RETURN;
                WHEN OTHERS THEN
                    IF SQLCODE = -1422 THEN
                       OpenCursor(pRefCurs,NULL);
                       --PRINT('GetFigli - Chiudo per SQLCODE = -1422');
                       RETURN;
                    ELSE
                       RAISE;
                    END IF;
            END;
    END;
    GetGerarchiaID (vCodInizio, pOrganizzazione, vGerarchia, vLivello);
    IF (vGerarchia = cGerarchiaELE) OR (vGerarchia = cGerarchiaGEO) OR (vGerarchia = cGerarchiaAMM) THEN
        vSqlSelElem := REPLACE(vSqlSelElem,'#REL#',vRel1);
    ELSE
        vSqlSelElem := REPLACE(vSqlSelElem,'#REL#',vRel2);
    END IF;
    CASE vGerarchia
         WHEN cGerarchiaELE_CP  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'ECP');
         WHEN cGerarchiaELE_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'ECP');
         WHEN cGerarchiaGEO     THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'GEO');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','');
         WHEN cGerarchiaAMM     THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'AMM');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','');
         WHEN cGerarchiaGEO_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'GEO');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
         WHEN cGerarchiaAMM_CS  THEN vSqlSelElem := REPLACE(vSqlSelElem,'#TAB#' ,'AMM');
                                     vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','');
                                     vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','');
    END CASE;

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazGEO THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','GEO');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','GEO');
        WHEN PKG_Mago.gcOrganizzazAMM THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','AMM');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','AMM');
        WHEN PKG_Mago.gcOrganizzazELE THEN vSqlSelElem := REPLACE(vSqlSelElem, '#GER#','ECP');
                                           vSqlInsTemp := REPLACE(vSqlInsTemp, '#GER#','ECP');
    END CASE;

    IF vStatoRete = PKG_Mago.gcStatoAttuale THEN
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','_SA');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','_SA');
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','_SA');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','_SA');
    ELSE
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA1#','_SN');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA1#','_SN');
        vSqlSelElem := REPLACE(vSqlSelElem,'#STA2#','_SN');
        vSqlInsTemp := REPLACE(vSqlInsTemp,'#STA2#','_SN');
    END IF;

    DELETE FROM GTTD_VALORI_TEMP WHERE TIP LIKE vTipTmp||'%';

    IF (vGerarchia = cGerarchiaELE) OR (vGerarchia = cGerarchiaGEO) OR (vGerarchia = cGerarchiaAMM) THEN
        OPEN pRefCurs FOR vSqlSelElem USING vDataRif, vCodInizio;
    ELSE
       OPEN pRefCurs FOR vSqlSelElem USING vDataRif, vDataRif, vCodInizio;
    END IF;
    LOOP
       FETCH pRefCurs INTO vCodEleTmp, vLiv, vTip;
       EXIT WHEN pRefCurs%NOTFOUND;
       IF vTip = vTipEleRoot THEN
          vCodEleRoot := vCodEleTmp;
       END IF;
       AddElem(vLiv,vCodEleTmp);
    END LOOP;
    CLOSE pRefCurs;

    IF vCodInizio IS NOT NULL THEN
       AddElem(0,vCodInizio);
    END IF;

    EXECUTE IMMEDIATE vSqlInsTemp USING vDataRif, vDataRif, vDataRif;
    DELETE FROM GTTD_VALORI_TEMP WHERE TIP = vTipTmp||'1';

    BEGIN
        SELECT PKG_Elementi.GetElemInfo(COD_ELEMENTO,vDataRif,pTipologiaRete,pTipoClie,pOrganizzazione,vStatoRete) FLAG,
               NVL(SUBSTR(COD_GEST_ELEMENTO,INSTR(COD_GEST_ELEMENTO,vSeparatore)+1),COD_GEST_ELEMENTO) COD_GEST_ELEMENTO,
               NOME_ELEMENTO,T.COD_TIPO_ELEMENTO,CODIFICA_ST
          INTO vFlg,vGst,vNom,vTip,vTst
          FROM (SELECT D.COD_ELEMENTO,NOME_ELEMENTO,COD_GEST_ELEMENTO,E.COD_TIPO_ELEMENTO
                  FROM ELEMENTI_DEF D
                 INNER JOIN ELEMENTI E ON (E.COD_ELEMENTO = D.COD_ELEMENTO)
                 WHERE vDataRif BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE
               ) E
         INNER JOIN TIPI_ELEMENTO T ON (T.COD_TIPO_ELEMENTO = E.COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = (SELECT NUM2 FROM GTTD_VALORI_TEMP WHERE NUM1 = 1 AND TIP = vTipTmp||'2');
    EXCEPTION
        WHEN NO_DATA_FOUND THEN
                --PRINT('>> Chiudo per NO_DATA_FOUND!');
                OpenCursor(pRefCurs,NULL);
                RETURN;
    END;
    --PRINT('aggiorno livello 1 con - NUM3='||vFlg||'  ALF1='||vGst||'  ALF2='||vNom||'  ALF3='||vTip||'  ALF4='||vTst||'  NUM4=0');
    UPDATE GTTD_VALORI_TEMP SET NUM3=vFlg,
                                ALF1=vGst,
                                ALF2=vNom,
                                ALF3=vTip,
                                ALF4=vTst,
                                NUM4=0
     WHERE NUM1 = 1
       AND TIP = vTipTmp||'2';  -- livello 1

    IF PKG_Mago.IsNazionale THEN
        GetFigli(2, GetElementoBase, vTipTmp||'2');
    ELSE
        GetFigli(2, vCodEleRoot, vTipTmp||'2');
    END IF;

    IF vTrovato THEN
        --PRINT('apro il cursore');
        OpenCursor(pRefCurs,vTipTmp||'2');
    ELSE
        --PRINT('elemento non trovato');
        OpenCursor(pRefCurs,NULL);
    END IF;
    --PRINT('Fine');

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.FindElement'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         OpenCursor(pRefCurs,NULL);
         RETURN;

END FindElement;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetElementList    (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL,
                              pNumRows        IN INTEGER  DEFAULT 100) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna un cursore riportante gli elementi che soddisfano la richiesta in Stringa di ricerca
-----------------------------------------------------------------------------------------------------------*/

--    vE       pkg_elementi.t_RowElemList;
    vSearchString  VARCHAR2(200) := pSearchString;
    vTabGerarchia  VARCHAR2(35)  := 'GERARCHIA_#GER##STA#';
    
    vSql           VARCHAR2(5000) := 
                      'SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,COD_TIPO_ELEMENTO,DES_TIPO_ELEMENTO,'            ||
                             'PKG_Elementi.GetElemInfo (COD_ELEMENTO,:dt,:tr,:tc,:org,:sta) FLAG,'                          ||
                             'CASE '                                                                                        ||
                                'WHEN COD_TIPO_ELEMENTO IN ('''||PKG_Mago.gcClienteMT||''','''||PKG_Mago.gcLineaMT||''')'   ||
                                        'THEN RIFERIMENTO '                                                                 ||
                                        'ELSE NULL '                                                                        ||
                             'END RIFERIMENTO,'                                                                             ||
                             'TOTALI COUNT '                                                                                ||
                        'FROM (SELECT COD_ELEMENTO,COD_GEST_ELEMENTO,NOME_ELEMENTO,T.COD_TIPO_ELEMENTO,'                    ||
                                     'T.DESCRIZIONE DES_TIPO_ELEMENTO,A.RIFERIMENTO,TOTALI '                                ||
                                'FROM (SELECT E.COD_ELEMENTO,'                                                              ||
                                             'CASE '                                                                        ||
                                                 'WHEN E.COD_TIPO_ELEMENTO = '''||PKG_Mago.gcLineaMT||''' '                 ||
                                                  'AND D.RIF_ELEMENTO IS NOT NULL '                                         ||
                                                    'THEN D.RIF_ELEMENTO '                                                  ||
                                                 'ELSE E.COD_GEST_ELEMENTO '                                                ||
                                             'END COD_GEST_ELEMENTO,'                                                       ||
                                             'NOME_ELEMENTO,'                                                               ||
                                             'E.COD_TIPO_ELEMENTO,'                                                         ||
                                             'CASE '                                                                        ||
                                                'WHEN E.COD_TIPO_ELEMENTO = '''||PKG_Mago.gcLineaMT||''' '                  ||
                                                    'THEN E.COD_GEST_ELEMENTO '                                             ||
                                                    'ELSE D.RIF_ELEMENTO '                                                  ||
                                             'END RIFERIMENTO,'                                                             ||
                                             'TOTALI '                                                                      ||
                                        'FROM ELEMENTI E '                                                                  ||
                                       'INNER JOIN ELEMENTI_DEF D ON D.COD_ELEMENTO = E.COD_ELEMENTO '                      ||
                                                                'AND :dt BETWEEN D.DATA_ATTIVAZIONE '                       ||
                                                                            'AND D.DATA_DISATTIVAZIONE '                    ||
                                       'INNER JOIN (SELECT COD_ELEMENTO,TIPO,TOTALI '                                       ||
                                                     'FROM (SELECT A.*,'                                                    ||
                                                                  'COUNT (1) OVER (PARTITION BY 1 ORDER BY 1 DESC) TOTALI ' ||
                                                             'FROM V_SEARCH_ELEMENTS A '                                    ||
                                                            'INNER JOIN ELEMENTI E ON E.COD_ELEMENTO = A.COD_ELEMENTO '     ||
                                                            'WHERE SEARCH_STRING LIKE :src '                                ||
                                                              'AND CASE '                                                   ||
                                                                     ' WHEN EXISTS (SELECT 1 FROM #GERARCHIA# X '           ||
                                                                                    'WHERE X.COD_ELEMENTO = A.COD_ELEMENTO '||
                                                                                      'AND :dt BETWEEN DATA_ATTIVAZIONE '   ||
                                                                                                  'AND DATA_DISATTIVAZIONE '||
                                                                                   ') '                                     ||
                                                                          'THEN 1 '                                         ||
                                                                          'ELSE 0 '                                         ||
                                                                      'END = 1 '                                            ||
                                                            'ORDER BY PKG_Elementi.GetOrderByElemType(E.COD_TIPO_ELEMENTO,' ||
                                                                                                     'E.COD_GEST_ELEMENTO)' ||
                                                          ') '                                                              ||
                                                    'WHERE ROWNUM <= :num '                                                 ||
                                                  ') S ON (S.COD_ELEMENTO = E.COD_ELEMENTO) '                               ||
                                     ') A '                                                                                 ||
                               'INNER JOIN TIPI_ELEMENTO T ON T.COD_TIPO_ELEMENTO = A.COD_TIPO_ELEMENTO) '                  ||
                               'ORDER BY PKG_Elementi.GetOrderByElemType (COD_TIPO_ELEMENTO, COD_GEST_ELEMENTO)';

    PROCEDURE NullCursor(pCur OUT PKG_UtlGlb.t_query_cur) AS
        BEGIN
            OPEN pCur FOR
                SELECT NULL COD_ELEMENTO,
                       NULL COD_GEST_ELEMENTO,
                       NULL NOME_ELEMENTO,
                       NULL COD_TIPO_ELEMENTO,
                       NULL DES_TIPO_ELEMENTO,
                       NULL FLAG,
                       NULL RIFERIMENTO,
                       0 COUNT
                  FROM DUAL
                 WHERE 1=2;
        END;

 BEGIN
    --PKG_Logs.TraceLog('GetElementList  >'||vSearchString||'< - '||TO_CHAR(pData,'dd/mm/yyyy hh24:mi:ss')||
    --                  ' - Org='||pOrganizzazione||' - Sta='||pStatoRete||' - '||
    --                  NVL(pTipologiaRete,'<null>')||' - '||NVL(pTipoClie,'<null>'));  

    IF NVL(TRIM(pSearchString),'*') = '*' THEN 
        NullCursor(pRefCurs);
        RETURN;
    END IF;

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','IMP');
        WHEN PKG_Mago.gcOrganizzazAMM THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','AMM');
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','');
        WHEN PKG_Mago.gcOrganizzazGEO THEN
                vTabGerarchia  := REPLACE(vTabGerarchia,'#GER#','GEO');
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','');
    END CASE;

    CASE pStatoRete
        WHEN PKG_Mago.gcStatoAttuale THEN 
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','_SA');
        WHEN PKG_Mago.gcStatoNormale THEN 
                vTabGerarchia  := REPLACE(vTabGerarchia,'#STA#','_SN');
    END CASE;
    
    vSql := REPLACE(vSql,'#GERARCHIA#',vTabGerarchia);
    
PRINT(NVL(vSearchString,'<null>')||' - '||vSql);

    IF SUBSTR(vSearchString,-1,1) <> '*' THEN
        vSearchString := pSearchString || '*';
    END IF;
 
    vSearchString := LOWER(TRIM(SUBSTR(vSearchString,1,200)));
    vSearchString := REPLACE(vSearchString,'*','%');
    vSearchString := REPLACE(vSearchString,'?','_');
    vSearchString := REGEXP_REPLACE(vSearchString,'[^[:alnum:]:%_:]');

    OPEN pRefCurs FOR vSql USING pData,pTipologiaRete,pTipoClie,pOrganizzazione,pStatoRete,
                                 pData,vSearchString,pData,pNumRows;                                     

--   LOOP
--      FETCH pRefCurs INTO vE;
--      EXIT WHEN pRefCurs%NOTFOUND;
--      PKG_Logs.TraceLog(CHR(9)||vE.COD_GEST_ELEMENTO||' - '||vE.NOME_ELEMENTO);  
--   END LOOP;
--   CLOSE pRefCurs;
--        OPEN pRefCurs FOR vSql USING pData,pTipologiaRete,pTipoClie,pOrganizzazione,pStatoRete,
--                                     pData,vSearchString,pData,pNumRows;
--                                     

 EXCEPTION
    WHEN OTHERS THEN
         PKG_Logs.StdLogAddTxt('DATA           : '||PKG_Mago.StdOutDate(pData),TRUE,NULL);
         PKG_Logs.StdLogAddTxt('Search STRING  : '||NVL(pSearchString,'<NULL>'),TRUE,NULL);
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GetElementList'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         NullCursor(pRefCurs);
         RETURN;

 END GetElementList;

-- ----------------------------------------------------------------------------------------------------------

 FUNCTION GetElementList     (pData           IN DATE,
                              pSearchString   IN VARCHAR2,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pTipologiaRete  IN VARCHAR2 DEFAULT NULL,
                              pTipoClie       IN VARCHAR2 DEFAULT NULL)
                       RETURN t_TabElemList PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna, in formato tabellare, gli elementi che soddisfano la richiesta in Stringa do ricerca
-----------------------------------------------------------------------------------------------------------*/
   vRow t_RowElemList;
   vCur PKG_UtlGlb.t_query_cur;
 BEGIN
    GetElementList(vCur,pData,pSearchString,pOrganizzazione,pStatoRete,pTipologiaRete,pTipoClie);
    LOOP
        FETCH vCur INTO vRow;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vRow);
     END LOOP;
    CLOSE vCur;
 END GetElementList;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaLineare (pDataDa         IN DATE,
                                pDataA          IN DATE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE, 
                                pDisconnect     IN INTEGER DEFAULT 0)
                       RETURN t_TabGerarchiaLineare PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) la gerarchia lineare dell'elemento
-----------------------------------------------------------------------------------------------------------*/

    vCurGer      PKG_UtlGlb.t_query_cur;
    vStatoRete   INTEGER := pStatoRete;
    vSqlGer      VARCHAR2(1000) := 'SELECT CASE WHEN NVL(L13,-1) > 0  THEN L13 '                      ||
                                              'WHEN NVL(L12,-1) > 0  THEN L12 '                      ||
                                              'WHEN NVL(L11,-1) > 0  THEN L11 '                      ||
                                              'WHEN NVL(L10,-1) > 0  THEN L10 '                      ||
                                              'WHEN NVL(L09,-1) > 0  THEN L09 '                      ||
                                              'WHEN NVL(L08,-1) > 0  THEN L08 '                      ||
                                              'WHEN NVL(L07,-1) > 0  THEN L07 '                      ||
                                              'WHEN NVL(L06,-1) > 0  THEN L06 '                      ||
                                              'WHEN NVL(L05,-1) > 0  THEN L05 '                      ||
                                              'WHEN NVL(L04,-1) > 0  THEN L04 '                      ||
                                              'WHEN NVL(L03,-1) > 0  THEN L03 '                      ||
                                              'WHEN NVL(L02,-1) > 0  THEN L02 '                      ||
                                              'WHEN NVL(L01,-1) > 0  THEN L01 '                      ||
                                         'END COD_ELEMENTO,'                                         ||
                                         'L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '      ||
                                    'FROM (SELECT g.*, #DATAELECALC# data_disattivazione_calc FROM GERARCHIA_#GER##STA# G ) '                                     ||
                                   'WHERE :dA BETWEEN DATA_ATTIVAZIONE  AND DATA_DISATTIVAZIONE_CALC '     ||   -- pDataA
                                     'AND :ele IN (L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13)'; -- pCodEle
    vIn     GTTD_CALC_GERARCHIA%ROWTYPE;
    vOut    GTTD_CALC_GERARCHIA%ROWTYPE;
BEGIN
    IF PKG_Mago.IsMagoSTM AND pOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
        vStatoRete  := PKG_Mago.gcStatoAttuale;
    END IF;
    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','IMP');
                CASE vStatoRete
                    WHEN PKG_Mago.gcStatoAttuale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA#','_SA');
                    WHEN PKG_Mago.gcStatoNormale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA#','_SN');
                END CASE;
        WHEN PKG_Mago.gcOrganizzazGEO THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','GEO');
        WHEN PKG_Mago.gcOrganizzazAMM THEN
                vSqlGer := REPLACE(vSqlGer,'#GER#','AMM');
    END CASE;
    vSqlGer := REPLACE(vSqlGer,'#STA#','');
    
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    
    --OPEN vCurGer FOR vSqlGer USING pDataDa, pDataA, pCodEle;
    OPEN vCurGer FOR vSqlGer USING pDataA, pCodEle;
    LOOP
        FETCH vCurGer INTO vIn;
        EXIT WHEN vCurGer%NOTFOUND;
        vOut := NULL;
        vOut.COD_ELEMENTO := vIn.COD_ELEMENTO;
        CASE pCodEle
            WHEN vIn.L01 THEN vOut.L01 := vIn.L01; vOut.L02 := vIn.L02; vOut.L03 := vIn.L03;
                              vOut.L04 := vIn.L04; vOut.L05 := vIn.L05; vOut.L06 := vIn.L06;
                              vOut.L07 := vIn.L07; vOut.L08 := vIn.L08; vOut.L09 := vIn.L09;
                              vOut.L10 := vIn.L10; vOut.L11 := vIn.L11; vOut.L12 := vIn.L12;
                              vOut.L13 := vIn.L13;
            WHEN vIn.L02 THEN vOut.L01 := vIn.L02; vOut.L02 := vIn.L03; vOut.L03 := vIn.L04;
                              vOut.L04 := vIn.L05; vOut.L05 := vIn.L06; vOut.L06 := vIn.L07;
                              vOut.L07 := vIn.L08; vOut.L08 := vIn.L09; vOut.L09 := vIn.L10;
                              vOut.L10 := vIn.L11; vOut.L11 := vIn.L12; vOut.L12 := vIn.L13;
            WHEN vIn.L03 THEN vOut.L01 := vIn.L03; vOut.L02 := vIn.L04; vOut.L03 := vIn.L05;
                              vOut.L04 := vIn.L06; vOut.L05 := vIn.L07; vOut.L06 := vIn.L08;
                              vOut.L07 := vIn.L09; vOut.L08 := vIn.L10; vOut.L09 := vIn.L11;
                              vOut.L10 := vIn.L12; vOut.L11 := vIn.L13;
            WHEN vIn.L04 THEN vOut.L01 := vIn.L04; vOut.L02 := vIn.L05; vOut.L03 := vIn.L06;
                              vOut.L04 := vIn.L07; vOut.L05 := vIn.L08; vOut.L06 := vIn.L09;
                              vOut.L07 := vIn.L10; vOut.L08 := vIn.L11; vOut.L09 := vIn.L12;
                              vOut.L10 := vIn.L13;
            WHEN vIn.L05 THEN vOut.L01 := vIn.L05; vOut.L02 := vIn.L06; vOut.L03 := vIn.L07;
                              vOut.L04 := vIn.L08; vOut.L05 := vIn.L09; vOut.L06 := vIn.L10;
                              vOut.L07 := vIn.L11; vOut.L08 := vIn.L12; vOut.L09 := vIn.L13;
            WHEN vIn.L06 THEN vOut.L01 := vIn.L06; vOut.L02 := vIn.L07; vOut.L03 := vIn.L08;
                              vOut.L04 := vIn.L09; vOut.L05 := vIn.L10; vOut.L06 := vIn.L11;
                              vOut.L07 := vIn.L12; vOut.L08 := vIn.L13;
            WHEN vIn.L07 THEN vOut.L01 := vIn.L07; vOut.L02 := vIn.L08; vOut.L03 := vIn.L09;
                              vOut.L04 := vIn.L10; vOut.L05 := vIn.L11; vOut.L06 := vIn.L12;
                              vOut.L07 := vIn.L13;
            WHEN vIn.L08 THEN vOut.L01 := vIn.L08; vOut.L02 := vIn.L09; vOut.L03 := vIn.L10;
                              vOut.L04 := vIn.L11; vOut.L05 := vIn.L12; vOut.L06 := vIn.L13;
            WHEN vIn.L09 THEN vOut.L01 := vIn.L09; vOut.L02 := vIn.L10; vOut.L03 := vIn.L11;
                              vOut.L04 := vIn.L12; vOut.L05 := vIn.L13;
            WHEN vIn.L10 THEN vOut.L01 := vIn.L10; vOut.L02 := vIn.L11; vOut.L03 := vIn.L12;
                              vOut.L04 := vIn.L13;
            WHEN vIn.L11 THEN vOut.L01 := vIn.L11; vOut.L02 := vIn.L12; vOut.L03 := vIn.L13;
            WHEN vIn.L12 THEN vOut.L01 := vIn.L12; vOut.L02 := vIn.L13;
            WHEN vIn.L13 THEN vOut.L01 := vIn.L13;
        END CASE;
        PIPE ROW(vOut);
    END LOOP;
    CLOSE vCurGer;
    RETURN;

END LeggiGerarchiaLineare;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaLineare2(pDataDa         IN DATE,
                                pDataA          IN DATE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pDisconnect     IN INTEGER DEFAULT 0)
                       RETURN t_TabGerarchiaLineare PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) la gerarchia lineare dell'elemento
-----------------------------------------------------------------------------------------------------------*/

--    vSqlGer      VARCHAR2(4000) := 'SELECT CASE WHEN NVL(L13,-1) > 0  THEN L13 '                      ||
--                                               'WHEN NVL(L12,-1) > 0  THEN L12 '                      ||
--                                               'WHEN NVL(L11,-1) > 0  THEN L11 '                      ||
--                                               'WHEN NVL(L10,-1) > 0  THEN L10 '                      ||
--                                               'WHEN NVL(L09,-1) > 0  THEN L09 '                      ||
--                                               'WHEN NVL(L08,-1) > 0  THEN L08 '                      ||
--                                               'WHEN NVL(L07,-1) > 0  THEN L07 '                      ||
--                                               'WHEN NVL(L06,-1) > 0  THEN L06 '                      ||
--                                               'WHEN NVL(L05,-1) > 0  THEN L05 '                      ||
--                                               'WHEN NVL(L04,-1) > 0  THEN L04 '                      ||
--                                               'WHEN NVL(L03,-1) > 0  THEN L03 '                      ||
--                                               'WHEN NVL(L02,-1) > 0  THEN L02 '                      ||
--                                               'WHEN NVL(L01,-1) > 0  THEN L01 '                      ||
--                                          'END COD_ELEMENTO,'                                         ||
--                                          'L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13 '      ||
--                                     'FROM GERARCHIA_#GER##STA# '                                     ||
--                                    'WHERE DATA_DISATTIVAZIONE >= :dDa  AND DATA_ATTIVAZIONE <= :dA ' ||   -- pDataDa, pDataA
--                                      'AND :ele IN (L01,L02,L03,L04,L05,L06,L07,L08,L09,L10,L11,L12,L13)'; -- pCodEle
--    vIn     GTTD_CALC_GERARCHIA%ROWTYPE;
--    vOut    GTTD_CALC_GERARCHIA%ROWTYPE;

    vCurGer      PKG_UtlGlb.t_query_cur;
    vStatoRete   INTEGER := pStatoRete;
    vRow         GTTD_CALC_GERARCHIA%ROWTYPE;
    vSqlGer      VARCHAR2(4000) :=
                  'SELECT COD_ELEMENTO_FIGLIO '                                                                                                              ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,1)!=0 THEN INSTR(cod_el,''!'',1,1)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,2),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,2)-1)-(INSTR(cod_el,''!'',1,1)))) liv1'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,2)!=0 THEN INSTR(cod_el,''!'',1,2)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,3),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,3)-1)-(INSTR(cod_el,''!'',1,2)))) liv2'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,3)!=0 THEN INSTR(cod_el,''!'',1,3)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,4),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,4)-1)-(INSTR(cod_el,''!'',1,3)))) liv3'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,4)!=0 THEN INSTR(cod_el,''!'',1,4)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,5),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,5)-1)-(INSTR(cod_el,''!'',1,4)))) liv4'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,5)!=0 THEN INSTR(cod_el,''!'',1,5)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,6),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,6)-1)-(INSTR(cod_el,''!'',1,5)))) liv5'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,6)!=0 THEN INSTR(cod_el,''!'',1,6)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,7),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,7)-1)-(INSTR(cod_el,''!'',1,6)))) liv6'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,7)!=0 THEN INSTR(cod_el,''!'',1,7)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,8),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,8)-1)-(INSTR(cod_el,''!'',1,7)))) liv7'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,8)!=0 THEN INSTR(cod_el,''!'',1,8)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,9),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,9)-1)-(INSTR(cod_el,''!'',1,8)))) liv8'      ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,9)!=0 THEN INSTR(cod_el,''!'',1,9)+1 ELSE NULL END,'                        ||
                                          'DECODE(INSTR(cod_el,''!'',1,10),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,10)-1)-(INSTR(cod_el,''!'',1,9)))) liv9'    ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,10)!=0 THEN INSTR(cod_el,''!'',1,10)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,11),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,11)-1)-(INSTR(cod_el,''!'',1,10)))) liv10'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,11)!=0 THEN INSTR(cod_el,''!'',1,11)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,12),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,12)-1)-(INSTR(cod_el,''!'',1,11)))) liv11'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,12)!=0 THEN INSTR(cod_el,''!'',1,12)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,13),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,13)-1)-(INSTR(cod_el,''!'',1,12)))) liv12'  ||
                        ',TO_NUMBER(SUBSTR(cod_el,CASE WHEN INSTR(cod_el,''!'',1,13)!=0 THEN INSTR(cod_el,''!'',1,13)+1 ELSE NULL END,'                      ||
                                          'DECODE(INSTR(cod_el,''!'',1,14),0,LENGTH(cod_el),INSTR(cod_el,''!'',1,14)-1)-(INSTR(cod_el,''!'',1,13)))) liv13 ' ||
                    'FROM (    SELECT SYS_CONNECT_BY_PATH (COD_ELEMENTO_figlio, ''!'') cod_el, COD_ELEMENTO_FIGLIO, LEVEL LIV, ROWNUM SEQ '                  ||
                                'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM '                                                                 ||
                                       ' (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TAB##STA1# r ) '                             ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                                                         ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO FROM '                                                                 ||
                                      ' (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_ECS#STA2# r ) '                                ||
                                       'WHERE :dt BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC '                                                         ||
                                      'UNION ALL '                                                                                                           ||
                                      'SELECT NULL, :el FROM DUAL '                                                                                          ||
                                     ')'                                                                                                                     ||
                          'START WITH COD_ELEMENTO_PADRE IS NULL '                                                                                           ||
                          'CONNECT BY PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE)';

BEGIN
    IF PKG_Mago.IsMagoSTM AND pOrganizzazione <> PKG_Mago.gcOrganizzazELE THEN
        vStatoRete  := PKG_Mago.gcStatoAttuale;
    END IF;

    CASE pOrganizzazione
        WHEN PKG_Mago.gcOrganizzazELE THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','ECP');
                CASE vStatoRete
                    WHEN PKG_Mago.gcStatoAttuale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA1#','_SA');
                        vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
                    WHEN PKG_Mago.gcStatoNormale THEN
                        vSqlGer := REPLACE(vSqlGer,'#STA1#','_SN');
                        vSqlGer := REPLACE(vSqlGer,'#STA2#','_SN');
                END CASE;
        WHEN PKG_Mago.gcOrganizzazGEO THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','GEO');
                vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
        WHEN PKG_Mago.gcOrganizzazAMM THEN
                vSqlGer := REPLACE(vSqlGer,'#TAB#','AMM');
                vSqlGer := REPLACE(vSqlGer,'#STA2#','_SA');
    END CASE;
    
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSqlGer := REPLACE (vSqlGer, '#DATAELECALC#' , ' data_disattivazione ' );   
    END IF;
    
    OPEN vCurGer FOR vSqlGer USING pDataDa, pDataA, pCodEle;
    LOOP
        FETCH vCurGer INTO vRow;
        EXIT WHEN vCurGer%NOTFOUND;
        PIPE ROW(vRow);
    END LOOP;
    CLOSE vCurGer;
    RETURN;

END LeggiGerarchiaLineare2;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaSup     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pData           IN DATE DEFAULT SYSDATE,
                                pDisconnect     IN INTEGER DEFAULT 0)
                         RETURN t_TabGerarchia PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) gli elementi gerarchicamente superiori all'elemento ricevuto
    - L'elemento ricevuto � parte della gerarchia restituita (identificato con livello piu' alto)
-----------------------------------------------------------------------------------------------------------*/

    vECP    TIPI_ELEMENTO.GER_ECP%TYPE;
    vECS    TIPI_ELEMENTO.GER_ECS%TYPE;
    vGEO    TIPI_ELEMENTO.GER_GEO%TYPE;
    vAMM    TIPI_ELEMENTO.GER_AMM%TYPE;

    vEle    t_RowGerarchia;

    vFlag   INTEGER;

    vCur    PKG_UtlGlb.t_query_cur;

    vSql    VARCHAR2(1200) :=
--               'SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, LIV, ROWNUM '               ||
               'SELECT COD_ELEMENTO,'                                                                  ||
                       'CASE '                                                                         ||
                         'WHEN COD_TIPO_ELEMENTO = '''||PKG_Mago.gcLineaMT||''' '                      ||
                            'THEN PKG_Elementi.GetGestElemento(COD_ELEMENTO) '                         ||
                            'ELSE COD_GEST_ELEMENTO '                                                  ||
                      'END COD_GEST_ELEMENTO,'                                                         ||
                      'COD_TIPO_ELEMENTO, LIV, ROWNUM '                                                ||
                 'FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, '                   ||
                              'MAX(LIV) OVER (PARTITION BY 1) - liv + 1 LIV '                          ||
                         'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,LEVEL LIV,ROWNUM SEQ '   ||
                                 'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TB1# r ) '                                    ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ' ||
                                       '#ECS# '                                                        ||
                                      ') START WITH COD_ELEMENTO_FIGLIO = :ele '                       ||
                                      'CONNECT BY PRIOR COD_ELEMENTO_PADRE = COD_ELEMENTO_FIGLIO '     ||
                              ') '                                                                     ||
                         'INNER JOIN ELEMENTI ON COD_ELEMENTO_FIGLIO = COD_ELEMENTO '                  ||
                         'ORDER BY SEQ DESC '                                                          ||
                      ') ';
    vSqlEcs VARCHAR2(500) :=
                    'UNION ALL '                                                                       ||
                   'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '                                   ||
                     'FROM (SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TB2# r )'                                                        ||
                    'WHERE :dat BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ';
BEGIN

    BEGIN
        SELECT GER_ECP, GER_ECS, GER_GEO, GER_AMM
          INTO vECP,    vECS,    vGEO,    vAMM
          FROM ELEMENTI
         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodEle;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    IF vECS = 1 THEN
        IF vECP + vGEO + vAMM > 1 THEN
            vSql := REPLACE(vSql,'#ECS#','');
            vFlag := 1;
        ELSE
            vSql := REPLACE(vSql,'#ECS#',vSqlEcs);
            vFlag := 2;
        END IF;
    ELSE
        vSql := REPLACE(vSql,'#ECS#','');
        vFlag := 1;
    END IF;

    CASE pOrganizzazione
        WHEN 1 THEN CASE pStatoRete
                        WHEN 1 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SN');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SN');
                        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SA');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
                        ELSE RETURN;
                    END CASE;
        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','GEO');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        WHEN 3 THEN vSql := REPLACE(vSql,'#TB1#','AMM');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        ELSE RETURN;
    END CASE;
    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    
    IF vFlag = 1 THEN
        OPEN vCur FOR vSql USING pData,        pCodEle;
    ELSE
        OPEN vCur FOR vSql USING pData, pData, pCodEle;
    END IF;

    LOOP
        FETCH vCur INTO vEle;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vEle);
    END LOOP;
    CLOSE vCur;
    RETURN;

END LeggiGerarchiaSup;

-- ----------------------------------------------------------------------------------------------------------

FUNCTION LeggiGerarchiaInf     (pCodEle         IN ELEMENTI.COD_ELEMENTO%TYPE,
                                pOrganizzazione IN INTEGER,
                                pStatoRete      IN INTEGER,
                                pData           IN DATE DEFAULT SYSDATE,
                                pFiltroTipEle   IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE DEFAULT NULL,
                                pDisconnect     IN INTEGER DEFAULT 0)
                         RETURN t_TabGerarchia PIPELINED AS
/*-----------------------------------------------------------------------------------------------------------
    restituisce in formato tabella (PIPELINE) gli elementi gerarchicamente dipendenti dall'elemento ricevuto
    - L'elemento ricevuto � parte della gerarchia restituita (identificato con livello 0) se non richiesto
      filtro su tipo elemento (pFiltroTipEle)
-----------------------------------------------------------------------------------------------------------*/

    vECP    TIPI_ELEMENTO.GER_ECP%TYPE;
    vECS    TIPI_ELEMENTO.GER_ECS%TYPE;
    vGEO    TIPI_ELEMENTO.GER_GEO%TYPE;
    vAMM    TIPI_ELEMENTO.GER_AMM%TYPE;
    vTip    TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE;

    vIncSeq NUMBER(1);

    vEle    t_RowGerarchia;

    vCur    PKG_UtlGlb.t_query_cur;

    vSql    VARCHAR2(1200) :=
               'SELECT COD_ELEMENTO,'                                                                  ||
                       'CASE '                                                                         ||
                         'WHEN COD_TIPO_ELEMENTO = '''||PKG_Mago.gcLineaMT||''' '                      ||
                            'THEN PKG_Elementi.GetGestElemento(COD_ELEMENTO) '                         ||
                            'ELSE COD_GEST_ELEMENTO '                                                  ||
                      'END COD_GEST_ELEMENTO,'                                                         ||
                      'COD_TIPO_ELEMENTO, LIV, SEQ + :incr '                                           ||
                 'FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO, COD_TIPO_ELEMENTO, SEQ, LIV '          ||
                         'FROM (SELECT COD_ELEMENTO_PADRE,COD_ELEMENTO_FIGLIO,LEVEL LIV,ROWNUM SEQ '   ||
                                 'FROM (SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM ( SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TB1# r ) '                                    ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ' ||
                                        'UNION ALL '                                                   ||
                                       'SELECT COD_ELEMENTO_PADRE, COD_ELEMENTO_FIGLIO '               ||
                                         'FROM ( SELECT r.*, #DATAELECALC# data_disattivazione_calc FROM REL_ELEMENTI_#TB2# r ) '                                    ||
                                        'WHERE :dat BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE_CALC ' ||
                                      ') START WITH COD_ELEMENTO_PADRE = :ele '                        ||
                                      'CONNECT BY PRIOR COD_ELEMENTO_FIGLIO = COD_ELEMENTO_PADRE '     ||
                              ') '                                                                     ||
                         'INNER JOIN ELEMENTI ON COD_ELEMENTO_FIGLIO = COD_ELEMENTO '                  ||
                         'WHERE COD_TIPO_ELEMENTO LIKE :tip '                                          ||
                         'ORDER BY SEQ '                                                               ||
                      ') ';
BEGIN

    BEGIN
        SELECT GER_ECP, GER_ECS, GER_GEO, GER_AMM, COD_TIPO_ELEMENTO
          INTO vECP,    vECS,    vGEO,    vAMM,    vTip
          FROM ELEMENTI
         INNER JOIN TIPI_ELEMENTO USING(COD_TIPO_ELEMENTO)
         WHERE COD_ELEMENTO = pCodEle;
    EXCEPTION
        WHEN NO_DATA_FOUND THEN RETURN;
        WHEN OTHERS THEN RAISE;
    END;

    CASE pOrganizzazione
        WHEN 1 THEN CASE pStatoRete
                        WHEN 1 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SN');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SN');
                        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','ECP_SA');
                                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
                        ELSE RETURN;
                    END CASE;
        WHEN 2 THEN vSql := REPLACE(vSql,'#TB1#','GEO');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        WHEN 3 THEN vSql := REPLACE(vSql,'#TB1#','AMM');
                    vSql := REPLACE(vSql,'#TB2#','ECS_SA');
        ELSE RETURN;
    END CASE;

    IF pFiltroTipEle IS NULL THEN
        vEle.COD_ELEMENTO := pCodEle;
        vEle.COD_GEST_ELEMENTO := GetGestElemento(pCodEle);
        vEle.COD_TIPO_ELEMENTO := vTip;
        vEle.LIVELLO := 0;
        vEle.SEQUENZA := 1;
        PIPE ROW(vEle);
        vIncSeq := 1;
    ELSE
        vIncSeq := 0;
    END IF;

    IF pDisconnect = 1 THEN /* Parametro per selezionare gli elementi di rete disconnessi */
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' nvl(data_disattivazione_undisconn,to_date(''01013000'',''ddmmyyyy'')) ' );
    ELSE
       vSql := REPLACE (vSql, '#DATAELECALC#' , ' data_disattivazione ' );    
    END IF;
    
    OPEN vCur FOR vSql USING vIncSeq, pData, pData, pCodEle, NVL(pFiltroTipEle,'%');
    LOOP
        FETCH vCur INTO vEle;
        EXIT WHEN vCur%NOTFOUND;
        PIPE ROW(vEle);
    END LOOP;
    CLOSE vCur;
    RETURN;

END LeggiGerarchiaInf;

-- ----------------------------------------------------------------------------------------------------------
-- Funzioni/Procedure definite per Rel 1.4.a ----------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------

PROCEDURE GetClientType     (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                             pPOD            IN INTEGER  DEFAULT 1,    -- Default 1 = POD altrimenti COD_GEST
                             pElem           IN VARCHAR2 DEFAULT NULL, -- se null ritorna tutti i clienti definiti
                             pData           IN DATE     DEFAULT SYSDATE) AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna, un cursore che descrive, per ogni cliente MT richiesto
         (o per tutti i clienti MT se nessun codice indicato),
    il tipo di cliente definito in anafrafica.
    I codici possono essere indicati come
      - POD      : parametro pPOD  = 1
        COD GEST : parametro pPOD <> 1
    il cursore di ritorno � composto da
        . codice elemento (POD o COD GEST come da valorizzazione del param pPOD)
        . Tipo cliente (vedere tab TIPI_CLIENTE per significato del tipo cliente)
        . flag di validit�: impostato = 1 se elemento in anagrafica all'istante richiesto
                                      = 0 se elemento non in anagrafica all'istante richiesto
-----------------------------------------------------------------------------------------------------------*/
    vLista      PKG_UtlGlb.t_SplitTbl;
    vKeyTmp     GTTD_VALORI_TEMP.TIP%TYPE := 'TIPCLIE.';
BEGIN

    DELETE GTTD_VALORI_TEMP WHERE TIP = vKeyTmp;

    IF pElem IS NULL THEN
        OPEN pRefCurs FOR SELECT CODICE, COD_TIPO_CLIENTE, 1 VALIDO
                            FROM (SELECT CASE pPOD
                                            WHEN 1 THEN D.RIF_ELEMENTO
                                                   ELSE E.COD_GEST_ELEMENTO
                                            END CODICE,
                                         D.COD_TIPO_CLIENTE
                                    FROM ELEMENTI E
                                   INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                                            AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                                          AND D.DATA_DISATTIVAZIONE
                                   WHERE E.COD_TIPO_ELEMENTO = PKG_MAGO.gcClienteMT
                                 )
                           WHERE CODICE IS NOT NULL
                           ORDER BY 1;
    ELSE
        vLista   := PKG_UtlGlb.SplitString(pElem,PKG_Mago.cSepCharLst);
        FOR i IN vLista.FIRST .. vLista.LAST LOOP
            INSERT INTO GTTD_VALORI_TEMP (TIP, ALF1,NUM1) VALUES (vKeyTmp,vLista(i),i);
        END LOOP;
        OPEN pRefCurs FOR SELECT T.ALF1 CODICE, X.COD_TIPO_CLIENTE,
                                 CASE
                                    WHEN X.RIFER IS NOT NULL THEN 1
                                                             ELSE 0
                                 END valido
                            FROM GTTD_VALORI_TEMP T
                            LEFT OUTER JOIN (SELECT D.RIF_ELEMENTO, E.COD_GEST_ELEMENTO, D.COD_TIPO_CLIENTE,
                                                    CASE pPOD
                                                       WHEN 1 THEN D.RIF_ELEMENTO
                                                              ELSE E.COD_GEST_ELEMENTO
                                                    END RIFER
                                               FROM (SELECT COD_ELEMENTO, COD_GEST_ELEMENTO
                                                       FROM ELEMENTI
                                                      WHERE COD_TIPO_ELEMENTO = PKG_MAGO.gcClienteMT
                                                    ) E
                                              INNER JOIN ELEMENTI_DEF D ON E.COD_ELEMENTO = D.COD_ELEMENTO
                                                                       AND pData BETWEEN D.DATA_ATTIVAZIONE
                                                                                     AND D.DATA_DISATTIVAZIONE
                                            ) X ON T.ALF1 = X.RIFER
                           ORDER BY NUM1;
    END IF;

END GetClientType;

-- ----------------------------------------------------------------------------------------------------------
-- Funzioni/Procedure definite per Rel 1.6.a ----------------------------------------------------------------
-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GestCodePerimeter (pRefCurs       OUT PKG_UtlGlb.t_query_cur,
                              pGestElem       IN ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pOrganizzazione IN INTEGER,
                              pStatoRete      IN INTEGER,
                              pData           IN DATE,
                              pElemTypeReq    IN TIPI_ELEMENTO.COD_TIPO_ELEMENTO%TYPE,
                              pDisconnect     IN INTEGER DEFAULT 0,
                              pTipiCliente    IN VARCHAR2 DEFAULT NULL) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il perimetro degli elementi relativi al tipo richiesto per il Codice Gestionale ricevuto
   Se pElemTypeReq � cliente (AT/MT/BT) e pTipiCliente e' valorizzato estrae solo i clienti che
      soddisfano pTipiCliente
   Se pElemTypeReq � non cliente pTipiCliente viene ignorato
-----------------------------------------------------------------------------------------------------------*/
    vFlgNull    NUMBER(1) := -1;
    vFiltTipCli NUMBER(1) := 0;  
BEGIN
    IF pElemTypeReq IN (PKG_Mago.gcClienteAT,PKG_Mago.gcClienteMT,PKG_Mago.gcClienteBT) AND
       pTipiCliente IS NOT NULL THEN
        vFiltTipCli := 1;
        DELETE GTTD_VALORI_TEMP WHERE TIP = PKG_Mago.gcTmpTipCliKey;
        PKG_Mago.TrattaListaCodici(pTipiCliente, PKG_Mago.gcTmpTipCliKey, vFlgNull); -- non gestito il flag            
    END IF;    

    OPEN pRefCurs FOR SELECT A.COD_ELEMENTO,A.COD_GEST_ELEMENTO,B.NOME_ELEMENTO
                        FROM TABLE(PKG_Elementi.LeggiGerarchiaInf(PKG_Elementi.GetCodElemento(pGestElem),
                                                                  pOrganizzazione,pStatoRete,pData,pElemTypeReq
                                                                 ,pDisconnect)
                                  ) A
                       LEFT OUTER JOIN ELEMENTI_DEF B ON B.COD_ELEMENTO = A.COD_ELEMENTO
                                                     AND pData BETWEEN B.DATA_ATTIVAZIONE
                                                                   AND B.DATA_DISATTIVAZIONE
                       WHERE CASE WHEN vFiltTipCli = 0 
                                THEN 1
                                ELSE CASE WHEN B.COD_TIPO_CLIENTE IN (SELECT ALF1 
                                                                        FROM GTTD_VALORI_TEMP
                                                                       WHERE TIP = PKG_Mago.gcTmpTipCliKey
                                                                     ) 
                                        THEN 1
                                        ELSE 0
                                     END
                       END = 1
                      ORDER BY COD_GEST_ELEMENTO;

EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GestCodePerimeter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GestCodePerimeter;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE GetCOprevalente   (pCodElem       OUT ELEMENTI.COD_ELEMENTO%TYPE,
                              pGestElem      OUT ELEMENTI.COD_GEST_ELEMENTO%TYPE,
                              pNomeElem      OUT ELEMENTI_DEF.NOME_ELEMENTO%TYPE) AS
/*-----------------------------------------------------------------------------------------------------------
   Restituisce il perimetro degli elementi relativi al tipo richiesto per il Codice Gestionale ricevuto
-----------------------------------------------------------------------------------------------------------*/
BEGIN
    SELECT A.COD_ELEMENTO, B.COD_GEST_ELEMENTO, C.NOME_ELEMENTO
      INTO pCodElem, pGestElem, pNomeElem
      FROM (SELECT PKG_ELEMENTI.GETELEMENTOESEDEF COD_ELEMENTO FROM DUAL) A
      LEFT OUTER JOIN ELEMENTI B  ON B.COD_ELEMENTO = A.COD_ELEMENTO
      LEFT OUTER JOIN ELEMENTI_DEF C ON C.COD_ELEMENTO = A.COD_ELEMENTO
                                    AND SYSDATE BETWEEN C.DATA_ATTIVAZIONE AND C.DATA_DISATTIVAZIONE;

 EXCEPTION
    WHEN OTHERS THEN
         ROLLBACK;
         PKG_Logs.StdLogAddTxt(SQLERRM||CHR(10)||'Funzione PKG_Elementi.GestCodePerimeter'||CHR(10)||DBMS_UTILITY.FORMAT_ERROR_BACKTRACE,FALSE,SQLCODE);
         PKG_Logs.StdLogPrint (PKG_UtlGlb.gcTrace_ERR);
         RAISE;

END GetCOprevalente;

/* **********************************************************************************************************
                                                     F I N E
********************************************************************************************************** */

BEGIN

    PKG_Logs.StdLogInit  (pClasseFunz  => PKG_Mago.gcJobClassUTL,
                          pFunzione    => 'PKG_Elementi',
                          pStoreOnFile => FALSE);

    BEGIN
        SELECT COD_ELEMENTO_CO, COD_ELEMENTO_ESE
          INTO gElementoBase,   gElementoEseDef
          FROM DEFAULT_CO
         WHERE FLAG_PRIMARIO = 1;
    EXCEPTION
        WHEN OTHERS THEN IF Pkg_Mago.gInizializzazione_In_Corso THEN
                            NULL;
                         ELSE
                            RAISE;
                         END IF;
    END;

-- ----------------------------------------------------------------------------------------------------------

END PKG_Elementi;
/
SHOW ERRORS;


