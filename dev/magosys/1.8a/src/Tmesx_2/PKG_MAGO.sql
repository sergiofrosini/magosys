PROMPT PACKAGE BODY PKG_MAGO;
--
-- PKG_MAGO  (Package Body) 
--
CREATE OR REPLACE PACKAGE BODY PKG_MAGO  AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione Mago 1.8.a.1
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/************************************************************************************************************
 ------------------------------------------------------------------------------------------------------------
 Tipi, Variabili e Costanti Globali Private
 ------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

--------------------------------------------------------------------------------------------------------------
-- Statements SQL di uso generale
--------------------------------------------------------------------------------------------------------------


--------------------------------------------------------------------------------------------------------------
-- Variabili Globali
--------------------------------------------------------------------------------------------------------------

--------------------------------------------------------------------------------------------------------------
-- Costanti Globali
--------------------------------------------------------------------------------------------------------------


/************************************************************************************************************
 ------------------------------------------------------------------------------------------------------------
 Funzioni e Procedure Private
 ------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE PRINT (pTxt IN VARCHAR2, pLine IN BOOLEAN DEFAULT TRUE) AS
/*-----------------------------------------------------------------------------------------------------------
    UTILITA' - dbms_output
-----------------------------------------------------------------------------------------------------------*/
 BEGIN
    IF pLine THEN
        DBMS_OUTPUT.PUT_LINE(pTxt);
    ELSE
        DBMS_OUTPUT.PUT(pTxt);
    END IF;
 END PRINT;

-- ----------------------------------------------------------------------------------------------------------

/************************************************************************************************************
 ------------------------------------------------------------------------------------------------------------
 Funzioni e Procedure Pubbliche
 ------------------------------------------------------------------------------------------------------------
************************************************************************************************************/

-- ----------------------------------------------------------------------------------------------------------

FUNCTION GetRoundDate          (pDateFrom IN DATE,pCurrentDate IN DATE,pTemporalAggreg IN NUMBER) RETURN DATE AS
/*-----------------------------------------------------------------------------------------------------------
    Ritorna la pCurrentDate 'arrotondata' temporalmente a partire dalla data di partenza (DateFrom)
    in base alla aggregazione temporale (pTemporalAggreg) richiesta
-----------------------------------------------------------------------------------------------------------*/
    vUtcTs         TIMESTAMP;
    vUtcCurr       TIMESTAMP;
    vDiffDate      NUMBER;
    vTz            INTEGER; -- := PKG_Date_Java.getTimeOffset(pCurrentDate);

 BEGIN

    IF   pDateFrom       IS NULL
      OR pCurrentDate    IS NULL
      OR pTemporalAggreg IS NULL THEN
      RETURN NULL;
    END IF;

    BEGIN
        vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    EXCEPTION
        WHEN OTHERS THEN
             vTz := TO_NUMBER(SUBSTR(TO_CHAR(TO_TIMESTAMP_TZ(TO_CHAR(pCurrentDate + 1/24,'YYYY-MM-DD HH24:MI:SS')||' CET', 'YYYY-MM-DD HH24:MI:SS TZR'),'TZH'),2,2));
    END;

    --vUtcTs         := SYS_EXTRACT_UTC(TO_TIMESTAMP(TRUNC(pDateFrom)));
    -- set pDateFrom value into vUtcTs variable
    vUtcTs    := FROM_TZ(CAST(pDateFrom AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    vUtcCurr  := FROM_TZ(CAST(pCurrentDate AS TIMESTAMP), 'Europe/Rome') AT TIME ZONE 'UTC';

    SELECT EXTRACT (DAY FROM (vUtcCurr-vUtcTs))*24*60+
           EXTRACT (HOUR FROM (vUtcCurr-vUtcTs))*60+
           EXTRACT (MINUTE FROM (vUtcCurr-vUtcTs))
      INTO vDiffDate
      FROM DUAL;

    vDiffDate := TRUNC( vDiffDate / pTemporalAggreg );

    RETURN CAST(vUtcTs AS DATE) + (vDiffDate*pTemporalAggreg/60/24) + (vTz/24);

 EXCEPTION
    WHEN OTHERS THEN
        IF SQLCODE = -01878 THEN
            RETURN NULL;
        ELSE
            RAISE;
        END IF;

 END GetRoundDate;


-- ----------------------------------------------------------------------------------------------------------

PROCEDURE getMisureHB (pRefCurs       OUT PKG_Utl.t_query_cur ,
                       pCodGestList    IN VARCHAR2,
                       pDataDa         IN DATE,
                       pDataA          IN DATE,
                       pTipMis         IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                       pTipAgr         IN TIPI_AGGREGAZIONE.COD_TIPO_AGGREGAZIONE%TYPE,
                       pTipRis         IN TIPI_RISOLUZIONE.COD_TIPO_RISOLUZIONE%TYPE,
                       pTplMis         IN TIPOLOGIE_MISURE.COD_TIPOLOGIA_MISURA%TYPE,
                       pTipoEstrazione IN INTEGER,  -- 1=Trasformatori, 2=LineeMT, 0=Altro,                    
                       pAgrTemporale   IN INTEGER DEFAULT 0) AS
BEGIN
  PKG_MAGO.getMisure (pCodGestList,
                      pDataDa,
                      pDataA,
                      pTipMis,
                      pTipAgr,
                      pTipRis,
                      pTplMis,
                      pTipoEstrazione,
                      pRefCurs,
                      pAgrTemporale);
END GetMisureHB;

-- ----------------------------------------------------------------------------------------------------------

 PROCEDURE getMisure  (pCodGestList    IN VARCHAR2,
                       pDataDa         IN DATE,
                       pDataA          IN DATE,
                       pTipMis         IN TIPI_MISURA.COD_TIPO_MISURA%TYPE,
                       pTipAgr         IN TIPI_AGGREGAZIONE.COD_TIPO_AGGREGAZIONE%TYPE,
                       pTipRis         IN TIPI_RISOLUZIONE.COD_TIPO_RISOLUZIONE%TYPE,
                       pTplMis         IN TIPOLOGIE_MISURE.COD_TIPOLOGIA_MISURA%TYPE,
                       pTipoEstrazione IN INTEGER,  -- 1=Trasformatori, 2=LineeMT, 0=Altro
                       pRefCurs       OUT PKG_Utl.t_query_cur,
                       pAgrTemporale   IN INTEGER DEFAULT 0) AS
                              
    vAgrTemp    NUMBER   := pAgrTemporale;
    vInterval   NUMBER;

    vDataDa     DATE     := pDataDa;
    vDataA      DATE     := pDataA;
                              
    vTabKey     PKG_Utl.t_SplitTbl;
    vTipRec     GTTD_VALORI_TEMP.TIP%TYPE := 'MisureMago';
    vCodEle     ELEMENTI.COD_ELEMENTO%TYPE;
    vGstEle     VARCHAR2(100);
    vDirEle     VARCHAR2(100);
    vTrtEle     TRATTAMENTO_ELEMENTI.COD_TRATTAMENTO_ELEM%TYPE;

    vSql        VARCHAR2(920) := 
                  'SELECT DATA,VALORE,QUALITA '                                                                         || 
                    'FROM (SELECT PKG_Mago.GetRoundDate(:DtDa,DATA,:ag) DATA,'                                          || -- pDataDa, pAgrTemporale
                                 '#AGR#(VALORE) VALORE,'                                                                ||
                                 'PKG_Qualita.Raw_To_Binary(MAX(QUALITA), 2) QUALITA '                                  ||
                            'FROM (SELECT DATA + 1/86400 DATA, '                                                        ||
                                         'VALORE, QUALITA '                                                             ||
                                    'FROM (SELECT PKG_AppServ.NormalizzaData_Date(DATA) DATA, VALORE, QUALITA '         ||
                                            'FROM MISURE '                                                              ||
                                           'INNER JOIN (SELECT NUM COD_TRATTAMENTO_ELEM '                               ||
                                                         'FROM GTTD_VALORI_TEMP '                                       ||
                                                        'WHERE TIP = '''||vTipRec||''' ) USING (COD_TRATTAMENTO_ELEM) ' ||
                                           'WHERE DATA BETWEEN :DataDa AND :DataA '                                     || -- pDataDa, pDataA
                                         ') '                                                                           ||
                                 ') '                                                                                   ||
                           'GROUP BY PKG_Mago.GetRoundDate(:DtDa,DATA,:ag) '                                            || -- pDataDa, pAgrTemporale
                          ') '                                                                                          ||
                   'WHERE DATA IS NOT NULL '                                                                            ||
                   'ORDER BY 1 ';

 BEGIN

    IF vAgrTemp < 10 THEN
        vAgrTemp := 10;
    END IF;

    IF vAgrTemp > 1440 THEN
        vAgrTemp := 1440;
    END IF;
    
    vInterval := 1440 / vAgrTemp;

    IF vInterval < 1 THEN 
        vInterval := 1;
    END IF;

    vDataDa   := vDataDa;
    
    DELETE GTTD_VALORI_TEMP WHERE TIP = vTipRec;
    
    vTabKey := PKG_Utl.TokenizeString(pCodGestList,'|');
    IF vTabKey.LAST IS NOT NULL THEN
        FOR i IN vTabKey.FIRST .. vTabKey.LAST LOOP

            BEGIN 
               SELECT COD_ELEMENTO INTO vCodEle 
                 FROM ELEMENTI 
                WHERE COD_GEST_ELEMENTO = vTabKey(i)
                  AND (    DATA_DISATTIVAZIONE  > pDataDa
                       AND DATA_ATTIVAZIONE    <= pDataA);
            EXCEPTION
               WHEN NO_DATA_FOUND THEN
                    CASE pTipoEstrazione
                        WHEN gcElabTrasf THEN
                            NULL;
                        WHEN gcElabLinMT THEN
                            BEGIN
                                SELECT COD_GEST,COD_GEST_DIRMT 
                                  INTO vGstEle,vDirEle
                                  FROM MONTANTIMT_SA@PKG2_CORELE.IT
                                 WHERE COD_GEST_DIRMT = TRIM(vTabKey(i))||' ' 
                                   AND (    DATA_FINE    > pDataDa
                                        AND DATA_INIZIO <= pDataA);
                            EXCEPTION
                                WHEN NO_DATA_FOUND THEN
                                    vGstEle := NULL;
                                    vDirEle := NULL;
                                WHEN OTHERS THEN RAISE;
                            END;
                            IF (vGstEle IS NULL) AND (vDirEle IS NULL) THEN
                                BEGIN
                                    SELECT COD_GEST,COD_GEST_DIRMT 
                                      INTO vGstEle,vDirEle
                                      FROM MONTANTIMT_SA@PKG2_CORELE.IT
                                     WHERE COD_GEST = vTabKey(i) 
                                       AND (    DATA_FINE    > pDataDa
                                            AND DATA_INIZIO <= pDataA);
                                EXCEPTION
                                    WHEN NO_DATA_FOUND THEN
                                        vGstEle := NULL;
                                        vDirEle := NULL;
                                    WHEN OTHERS THEN RAISE;
                                END;
                            END IF;
                            IF (vGstEle IS NOT NULL) OR (vDirEle IS NOT NULL) THEN
                                BEGIN 
                                    SELECT COD_ELEMENTO INTO vCodEle
                                      FROM ELEMENTI
                                     WHERE COD_GEST_ELEMENTO = vDirEle
                                       AND (    DATA_DISATTIVAZIONE  > pDataDa
                                            AND DATA_ATTIVAZIONE    <= pDataA);
                                EXCEPTION
                                    WHEN NO_DATA_FOUND THEN
                                        vCodEle := NULL;
                                    WHEN OTHERS THEN RAISE;
                                END;
                                IF vCodEle IS NULL THEN
                                    BEGIN 
                                        SELECT COD_ELEMENTO INTO vCodEle
                                          FROM ELEMENTI
                                         WHERE COD_GEST_ELEMENTO = vGstEle
                                           AND (    DATA_DISATTIVAZIONE  > pDataDa
                                                AND DATA_ATTIVAZIONE    <= pDataA);
                                    EXCEPTION
                                        WHEN NO_DATA_FOUND THEN
                                            vCodEle := NULL;
                                        WHEN OTHERS THEN RAISE;
                                    END;
                                END IF;
                            END IF;
                        ELSE NULL;
                    END CASE;
               WHEN OTHERS THEN vCodEle := NULL;
            END;

            BEGIN
              SELECT COD_TRATTAMENTO_ELEM INTO vTrtEle
                FROM TRATTAMENTO_ELEMENTI             
               WHERE COD_ELEMENTO          = vCodEle
                 AND COD_TIPO_MISURA       = pTipMis
                 AND COD_TIPOLOGIA_MISURA  = pTplMis
                 AND COD_TIPO_AGGREGAZIONE = pTipAgr
                 AND COD_TIPO_RISOLUZIONE  = pTipRis;

              INSERT INTO GTTD_VALORI_TEMP (TIP, NUM, NUM2, ALF, ALF2) 
                                    VALUES (vTipRec, vTrtEle, vCodEle, vTabKey(i), vGstEle);

            EXCEPTION
                WHEN NO_DATA_FOUND THEN NULL;  --  verificare che fare se il trattamento non e' presente
            END;
        END LOOP;
    END IF;
    
    IF vAgrTemp <= 60 THEN
        vSql := REPLACE(vSql,'#AGR#','AVG');
    ELSE
        vSql := REPLACE(vSql,'#AGR#','MAX');
    END IF;
   
--    OPEN pRefCurs FOR vSql USING vInterval, vInterval, vDataDa, vDataA, vInterval, vInterval;
    OPEN pRefCurs FOR vSql USING pDataDa, pAgrTemporale, pDataDa, pDataA, pDataDa, pAgrTemporale;

    RETURN;

 END getMisure;

-- ----------------------------------------------------------------------------------------------------------

/************************************************************************************************************
                                                     F I N E
************************************************************************************************************/

END PKG_Mago;
/

SHOW ERRORS;


