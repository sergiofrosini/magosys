PROMPT Insert Table TIPI_MISURA;

UPDATE tipi_misura SET FLG_MIS_CARICO = 1
 WHERE cod_tipo_misura in ('PI', 'PCT-P','PCT-Q') 
       or cod_tipo_misura like 'PMP.%'
       or cod_tipo_misura like 'PMC.%';
 
UPDATE tipi_misura SET FLG_MIS_GENERAZIONE = 1
 WHERE cod_tipo_misura in ('PI')
       or cod_tipo_misura like 'PMP.%'
       or cod_tipo_misura like 'PMC.%';
          
COMMIT;
