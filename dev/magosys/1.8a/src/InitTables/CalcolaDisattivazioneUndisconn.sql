
PROMPT CalcolaDisattivazioneUndisconn

BEGIN

Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_GEO');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_AMM');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_ECP_SN');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_ECP_SA');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_ECS_SN');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('REL_ELEMENTI_ECS_SA');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_GEO');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_AMM');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_IMP_SN');
Pkg_aggregazioni.CalcolaDisattivazioneUndisconn('GERARCHIA_IMP_SA');

COMMIT;

END;
/
