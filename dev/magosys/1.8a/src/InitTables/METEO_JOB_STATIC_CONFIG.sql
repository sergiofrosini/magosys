PROMPT METEO_JOB_STATIC_CONFIG

MERGE INTO METEO_JOB_STATIC_CONFIG t
     USING (
            SELECT 'MFM.supplier.national.skip.elaboration.1' key , 'false' value
              FROM dual
             UNION ALL
            SELECT 'MFM.supplier.national.skip.elaboration.2' key , 'true' value
              FROM dual
            ) s
        ON (t.key = s.key)
      WHEN MATCHED THEN
    UPDATE SET 
              t.value = s.value
      WHEN NOT MATCHED THEN 
    INSERT   
          (key,value) 
    VALUES (s.key, s.value) 
;

COMMIT;
