PROMPT TABLE TIPI_MISURA_CONV_ORIG;


MERGE INTO TIPI_MISURA_CONV_ORIG A 
           USING (
                  SELECT 'NUM_IMP' ORIGINE,NULL COD_TIPO_ELEMENTO,'NRI' COD_TIPO_MISURA_IN,'NRI' COD_TIPO_MISURA_OUT,':val' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'POT_INST' ORIGINE,NULL COD_TIPO_ELEMENTO,'PI' COD_TIPO_MISURA_IN,'PI' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PMP' COD_TIPO_MISURA_IN,'PMP' COD_TIPO_MISURA_OUT,':val' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PMR' COD_TIPO_MISURA_IN,'PMR' COD_TIPO_MISURA_OUT,':val' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PMC' COD_TIPO_MISURA_IN,'PMC' COD_TIPO_MISURA_OUT,':val' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PAG' COD_TIPO_MISURA_IN,'PAG' COD_TIPO_MISURA_OUT,':val' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PCT-P' COD_TIPO_MISURA_IN,'PCT-P' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'METEO' ORIGINE,NULL COD_TIPO_ELEMENTO,'PCT-Q' COD_TIPO_MISURA_IN,'PCT-Q' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_A' ORIGINE,NULL COD_TIPO_ELEMENTO,'EAG' COD_TIPO_MISURA_IN,'PAS' COD_TIPO_MISURA_OUT,'(:val * 1000) * (60 / 15)' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_A' ORIGINE,NULL COD_TIPO_ELEMENTO,'ERI' COD_TIPO_MISURA_IN,'PRI' COD_TIPO_MISURA_OUT,'(:val * 1000) * (60 / 15)' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'EAG' COD_TIPO_MISURA_IN,'PAS' COD_TIPO_MISURA_OUT,'(:val * 1000) * (60 / 15)' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'ERI' COD_TIPO_MISURA_IN,'PRI' COD_TIPO_MISURA_OUT,'(:val * 1000) * (60 / 15)' FORMULA,1 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'CC-P' COD_TIPO_MISURA_IN,'CC-P' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'CC-Q' COD_TIPO_MISURA_IN,'CC-Q' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'PAAS' COD_TIPO_MISURA_IN,'PAAS' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                  UNION ALL
                  SELECT 'GME_M' ORIGINE,NULL COD_TIPO_ELEMENTO,'PAGS' COD_TIPO_MISURA_IN,'PAGS' COD_TIPO_MISURA_OUT,':val * 1000' FORMULA,0 FLG_SPLIT FROM DUAL
                 ) B ON (    A.ORIGINE = B.ORIGINE 
                         AND NVL(A.COD_TIPO_ELEMENTO,'null') = NVL(B.COD_TIPO_ELEMENTO,'null')
                         AND A.COD_TIPO_MISURA_IN = B.COD_TIPO_MISURA_IN
                        )
      WHEN NOT MATCHED THEN 
           INSERT (ORIGINE, COD_TIPO_ELEMENTO, COD_TIPO_MISURA_IN, COD_TIPO_MISURA_OUT, FORMULA,FLG_SPLIT)
           VALUES (B.ORIGINE,B.COD_TIPO_ELEMENTO,B.COD_TIPO_MISURA_IN,B.COD_TIPO_MISURA_OUT,B.FORMULA,B.FLG_SPLIT)
      WHEN MATCHED THEN
           UPDATE SET A.COD_TIPO_MISURA_OUT = B.COD_TIPO_MISURA_OUT,
                      A.FORMULA             = B.FORMULA,
                      A.FLG_SPLIT           = B.FLG_SPLIT;
COMMIT;                      
