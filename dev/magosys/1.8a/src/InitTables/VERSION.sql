PROMPT VERSION

MERGE INTO VERSION t
     USING
          ( SELECT sysdate data , '1' major , '8' minor , 'a' patch, 0 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 1 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 2 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 3 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 4 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 5 revision
              FROM DUAL
             UNION ALL
            SELECT sysdate data , '1' major , '8' minor , 'a' patch, 6 revision
              FROM DUAL
          ) s
        ON ( t.major = s.major AND t.minor = s.minor AND t.patch = s.patch AND t.revision = s.revision)
      WHEN MATCHED THEN 
    UPDATE SET
               t.data = s.data
      WHEN NOT MATCHED THEN
    INSERT (data, major, minor, patch, revision) 
    VALUES (s.data, s.major, s.minor, s.patch, s.revision) 
;

COMMIT;
