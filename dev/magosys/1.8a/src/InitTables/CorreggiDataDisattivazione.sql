MERGE INTO rel_elementi_ecp_sn  t
USING (
       SELECT dup.cod_elemento_figlio,ele.cod_tipo_elemento, elep.cod_elemento cod_elemento_padre , elep.cod_tipo_elemento tipo_p
             ,dup.data_attivazione, rel.data_disattivazione
         FROM (
               SELECT cod_elemento_figlio , data_attivazione 
                 FROM rel_elementi_ecp_sn 
                GROUP BY cod_elemento_figlio , data_attivazione 
               HAVING COUNT(*) > 1
              ) dup
             ,elementi ele 
             ,elementi elep
             ,rel_elementi_ecp_sn rel 
       WHERE rel.data_attivazione = dup.data_attivazione
         AND rel.cod_elemento_figlio = dup.cod_elemento_figlio
         AND ele.cod_elemento = dup.cod_elemento_figlio
         AND elep.cod_elemento = rel.cod_elemento_padre
         AND elep.cod_tipo_elemento ='CPR'
      ) s
   ON ( s.cod_elemento_figlio = t.cod_elemento_figlio AND s.data_attivazione = t.data_attivazione AND s.cod_elemento_padre = t.cod_elemento_padre )
 WHEN MATCHED THEN 
UPDATE SET
t.data_disattivazione = s.data_disattivazione;

MERGE INTO rel_elementi_ecp_sa  t
USING (
       SELECT dup.cod_elemento_figlio,ele.cod_tipo_elemento, elep.cod_elemento cod_elemento_padre , elep.cod_tipo_elemento tipo_p
             ,dup.data_attivazione, rel.data_disattivazione
         FROM (
               SELECT cod_elemento_figlio , data_attivazione 
                 FROM rel_elementi_ecp_sa
                GROUP BY cod_elemento_figlio , data_attivazione 
               HAVING COUNT(*) > 1
              ) dup
             ,elementi ele 
             ,elementi elep
             ,rel_elementi_ecp_sn rel 
       WHERE rel.data_attivazione = dup.data_attivazione
         AND rel.cod_elemento_figlio = dup.cod_elemento_figlio
         AND ele.cod_elemento = dup.cod_elemento_figlio
         AND elep.cod_elemento = rel.cod_elemento_padre
         AND elep.cod_tipo_elemento ='CPR'
      ) s
   ON ( s.cod_elemento_figlio = t.cod_elemento_figlio AND s.data_attivazione = t.data_attivazione AND s.cod_elemento_padre = t.cod_elemento_padre )
 WHEN MATCHED THEN 
UPDATE SET
t.data_disattivazione = s.data_disattivazione;


COMMIT;


