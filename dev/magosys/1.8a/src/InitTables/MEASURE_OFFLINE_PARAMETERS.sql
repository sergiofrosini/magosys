PROMPT MEASURE_OFFLINE_PARAMETERS
delete MEASURE_OFFLINE_PARAMETERS where key = 'STWebRegistry-WS.measure.offline.thread_timeout_factor';
insert into  MEASURE_OFFLINE_PARAMETERS (key,value) values ('STWebRegistry-WS.measure.offline.offline_thread_timeout_seconds','172800');
insert into  MEASURE_OFFLINE_PARAMETERS (key,value) values ('STWebRegistry-WS.measure.offline.subscribe_thread_timeout_seconds','172800');
insert into  MEASURE_OFFLINE_PARAMETERS (key,value) values ('STWebRegistry-WS.measure.offline.subscribe_max_concurrent_session_timestamp_threads','1');
COMMIT;

