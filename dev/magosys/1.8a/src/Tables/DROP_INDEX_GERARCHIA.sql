DECLARE
   CURSOR c_ind IS 
          select 'drop index ' || index_name ind
            from user_ind_columns
           where table_name in ('GERARCHIA_AMM','GERARCHIA_GEO','GERARCHIA_IMP_SN','GERARCHIA_IMP_SA')
             and column_name in ( 'L01','L02','L03','L04','L05','L06','L07','L08','L09','L10','L11','L12','L13');
BEGIN
	FOR rec_data in c_ind LOOP
	   execute immediate (rec_data.ind);
  END LOOP;
END;
/
