CREATE OR REPLACE VIEW v_punti_geo AS
SELECT cod_geo
      ,tipo_punto
      ,tipo_previsione
      ,coord_lat
      ,coord_long
      ,alt
      ,altezza_sul_terreno
  FROM 
      (
       SELECT /*+ ordered */
             ROW_NUMBER() OVER ( PARTITION BY an.cod_geo, tr.cod_tipo_fonte ORDER BY an.cod_geo) num
             ,an.cod_geo
             ,an.tipo_coord tipo_punto
             ,tr.cod_tipo_fonte tipo_previsione
             ,TRIM(TO_CHAR(TRUNC(an.coordinata_x) ,'999') || ' ' 
              || TRUNC((an.coordinata_x - TRUNC(an.coordinata_x)) * 60) || ' ' 
              || ROUND(((((an.coordinata_x - TRUNC(an.coordinata_x)) * 60) - TRUNC((an.coordinata_x - TRUNC(an.coordinata_x)) * 60)) *60),2)) coord_lat
             ,TRIM(TO_CHAR(TRUNC(an.coordinata_y) ,'999') || ' ' 
              || TRUNC((an.coordinata_y - TRUNC(an.coordinata_y)) * 60) || ' ' 
              || ROUND(((((an.coordinata_y - TRUNC(an.coordinata_y)) * 60) - TRUNC((an.coordinata_y - TRUNC(an.coordinata_y)) * 60)) *60),2)) coord_long
             ,an.coordinata_h alt
             ,CASE WHEN tr.cod_tipo_fonte ='E' THEN NVL(def.h_anem,80) ELSE 0 END altezza_sul_terreno
         FROM TRATTAMENTO_ELEMENTI tr 
             ,elementi_def def
             ,anagrafica_punti an
        WHERE an.cod_geo IN (def.cod_geo, def.cod_geo_a)
          AND def.data_disattivazione = to_date('01013000','ddmmyyyy')
          AND tr.cod_elemento = def.cod_elemento
          AND tr.COD_TIPO_MISURA = 'PI'
          AND tr.COD_TIPO_FONTE in ('S','E')
          AND an.coordinata_y != 0
          AND an.coordinata_x != 0
      ) 
 WHERE num = 1
/

--GRANT SELECT ON V_PUNTI_GEO TO MAGONAZ;
