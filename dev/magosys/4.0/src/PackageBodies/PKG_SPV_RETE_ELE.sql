PROMPT Package Body PKG_SPV_RETE_ELE

create or replace PACKAGE BODY      PKG_SPV_RETE_ELE AS


/* ***********************************************************************************************************
   NAME:       PKG_SPV_RETE_ELE
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
    NOTES:

*********************************************************************************************************** */
g_ApplXParam        VARCHAR2(50) := 'MAGO';

g_ParamMunic       VARCHAR2(50) := 'IS_MUNICIPALIZZATE';

g_DumDum_s          VARCHAR2(100); -- variabile generica stringa
g_DumDum_n          NUMBER; -- variabile generica numerica


    g_tipoMago_Client    CONSTANT elementi.cod_tipo_elemento%TYPE := 'CMT';
    g_tipoMago_Trasf     CONSTANT elementi.cod_tipo_elemento%TYPE := 'TRM';
    g_tipoMago_Montante  CONSTANT elementi.cod_tipo_elemento%TYPE := 'LMT';
    g_tipoMago_Comune    CONSTANT elementi.cod_tipo_elemento%TYPE := 'COM';
    g_tipoMago_CabinaP   CONSTANT elementi.cod_tipo_elemento%TYPE := 'CPR';
    g_tipoMago_sbarraSec CONSTANT elementi.cod_tipo_elemento%TYPE := 'SCS';


    g_ProdPuro_AUI CONSTANT VARCHAR2(2) := 'PP';
    g_AutoProd_AUI CONSTANT VARCHAR2(2) := 'AP';
    g_ProdAlia_AUI CONSTANT VARCHAR2(2) := 'PD';

    g_CollectorName         CONSTANT spv_collector_info.collector_name%type 	:='RETE_ELETTRICA';
	g_IdSistema             CONSTANT spv_dettaglio_parametri.ID_SISTEMA%type 	:= 5;

    g_isMunic   NUMBER := -1;
    g_DefaultMunic   NUMBER := 0;

	gID_CHIAVE_SPV_ENABLED  CONSTANT VARCHAR2(100 BYTE) := 'aui.supervision_enabled';

    g_SogliaLinea CONSTANT NUMBER := 6000; -- 6MW

    gMis_PI     CONSTANT VARCHAR2(10)  := 'PI';
    gMis_PAS    CONSTANT VARCHAR2(10)  := 'PAS';
    gMis_PPAGCT CONSTANT VARCHAR2(10)  := 'PMC';
    gMis_PRI    CONSTANT VARCHAR2(10)  := 'PRI';


    g_tit_CodGes constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.codGes';
    g_tit_TipCli constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipCli';
    g_tit_TipEle constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.tipEle';
    g_tit_PI     constant VARCHAR2(100) := 'mago.SPV.detail.anagAui.elenco.PI';

    g_sep_CSV_excel constant VARCHAR2(1) := ';';

    --mago.supervision.detail.anagraficaAui.elenco.title



    -- 01 err 1,'Controllo Codice gestionale esercizio abilitato
    -- 02 err 2,'CG duplicati
    -- 03 err 3,'CG  non definiti
    -- 04 err 4,'CG minuscoli in codice gestionale
    -- 05 err 5,'CG non standard
    -- 06 err 6,'Produttori MT e TR MT/BT definiti in AUI e non presenti in STM
    -- 07 err 7,'CG in STM non trovato in AUI
    -- 08 err 8, 'Produttori MT trasmessi da STM a MAGO che risultano �dismessi� in AUI	
    pID_DIZIONARIO_ALLARME_01 spv_dizionario_allarmi.id_dizionario_allarme%type := 1;
	pID_DIZIONARIO_ALLARME_02 spv_dizionario_allarmi.id_dizionario_allarme%type := 2;
	pID_DIZIONARIO_ALLARME_03 spv_dizionario_allarmi.id_dizionario_allarme%type := 3;
	pID_DIZIONARIO_ALLARME_04 spv_dizionario_allarmi.id_dizionario_allarme%type := 4;
	pID_DIZIONARIO_ALLARME_05 spv_dizionario_allarmi.id_dizionario_allarme%type := 5;
	pID_DIZIONARIO_ALLARME_06 spv_dizionario_allarmi.id_dizionario_allarme%type := 6;
	pID_DIZIONARIO_ALLARME_07 spv_dizionario_allarmi.id_dizionario_allarme%type := 7;
	pID_DIZIONARIO_ALLARME_08 spv_dizionario_allarmi.id_dizionario_allarme%type := 8;

    vPARAMETRI_DESCRIZIONE VARCHAR2(1000);

/*============================================================================*/

PROCEDURE printa(pSTR IN VARCHAR2) IS

BEGIn
   DBMS_OUTPUT.PUT_LINE(psTR);
   --	  PKG_Logs.TraceLog(psTR, PKG_UtlGlb.gcTrace_VRB);
END;

/*============================================================================*/
function f_IsMunic RETURN NUMBER IS 

  vRetVal NUMBER;
  v_num_par  number;

BEGIN
--  0 ENEL
--  1 MUNICIPALIZZATE
--
--

    IF g_IsMunic = -1 THEN
        SAR_ADMIN.PKG_UTL.sp_GetPargenVal(g_ApplXParam, g_ParamMunic, g_DumDum_s);
        v_num_par := NVL(g_DumDum_s,g_defaultMunic);   
    END IF;-- TODO - esiste un flag is_municip?
    
    RETURN g_IsMunic; 

END;

function f_StatoNormale RETURN VARCHAR2 IS BEGIN RETURN 'SN'; END;
function f_StatoAttuale RETURN VARCHAR2 IS BEGIN RETURN 'SA'; END;

function f_StatoNormaleSuffisso RETURN VARCHAR2 IS BEGIN RETURN '_' || f_StatoNormale; END;
function f_StatoAttualeSuffisso RETURN VARCHAR2 IS BEGIN RETURN '_' || f_StatoAttuale; END;

function f_TipoTrasfAUI  RETURN VARCHAR2 IS BEGIN RETURN 'T' ; END;
function f_TipoProdAUI  RETURN VARCHAR2 IS BEGIN RETURN 'U' ; END;
function f_TipoTrasfST  RETURN VARCHAR2 IS BEGIN RETURN 'TRM'; END;
function f_TipoProdST  RETURN VARCHAR2 IS BEGIN RETURN 'CMT'; END;

function f_DismessoAUI  RETURN VARCHAR2 IS BEGIN RETURN CASE f_IsMunic WHEN 0 THEN 'U' ELSE 'X' END; END; 

/*============================================================================*/

function f_Date2DettStr(pData in date) return varchar2 IS

BEGIN
 return to_char(pData,'dd/mm/yyyy')||' ore '||to_char(pData,'hh24:mi');
END;

/*============================================================================*/
function f_ClientAUISi(pAlias IN VARCHAR2 DEFAULT '666') RETURN VARCHAR2 IS


    vForse VARCHAR2(100) :=  CASE nvl(pAlias,'666') WHEN '666' THEN '' ELSE pAlias||'.' END;

BEGIN

    RETURN vForse||'TRATTAMENTO = 0 AND '||vForse||'STATO IN (''A'', ''E'') ';

END;

/*============================================================================*/

FUNCTION f_AppiccicaStato(pSql in varchar2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 default '_##') RETURN VARCHAR2 IS
    
BEGIN 

    RETURN
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN REPLACE(pSql,pDove,f_StatoAttualeSuffisso)
        WHEN PKG_Mago.gcStatoNormale THEN REPLACE(pSql,pDove,f_StatoNormaleSuffisso)
    END;

END;

/*============================================================================*/

PROCEDURE sp_AppiccicaStato(pSql in out varchar2,
                            pStato IN VARCHAR2,
                            pDove IN VARCHAR2 default '_##') IS
    
BEGIN 
    CASE pStato
        WHEN PKG_Mago.gcStatoAttuale THEN pSql := REPLACE(pSql,pDove,f_StatoAttualeSuffisso);
        WHEN PKG_Mago.gcStatoNormale THEN pSql := REPLACE(pSql,pDove,f_StatoNormaleSuffisso);
    END CASE;

END;

/*============================================================================*/

PROCEDURE sp_AddUnCollector(pErrore       IN INTEGER,
                            pGestElem     IN VARCHAR2,
                            pTipoElem     IN VARCHAR2 DEFAULT NULL,
                            pData         IN DATE DEFAULT SYSDATE) AS

BEGIN

   pkg_supervisione.AddSpvCollectorInfo(g_CollectorName,
                                        pData,
                                        g_IdSistema,
                                        pErrore,
                                        null,
                                        pGestElem,
                                        null,
                                        null,
                                        1,
                                        pTipoElem);

END sp_AddUnCollector;

/*============================================================================*/

PROCEDURE sp_CheckCO(pStato       IN INTEGER DEFAULT 1,
                     pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                     pData           IN DATE DEFAULT SYSDATE) AS

 vDummy VARCHAR2(100);
 
BEGIN

    SELECT COD_GEST_ELEMENTO
      INTO vDummy
      FROM DEFAULT_CO
     INNER JOIN ELEMENTI ON COD_ELEMENTO = COD_ELEMENTO_ESE
     LEFT OUTER JOIN ELEMENTI_DEF USING(COD_ELEMENTO)
     WHERE flag_primario = 1
       AND pData BETWEEN DATA_ATTIVAZIONE AND DATA_DISATTIVAZIONE;

EXCEPTION

    WHEN NO_DATA_FOUND THEN
   dbms_output.put_line(' sp_AddUnCollector 1 ');
             sp_AddUnCollector(pID_DIZIONARIO_ALLARME_01, NULL, NULL);
    
    WHEN OTHERS THEN RAISE;

END sp_CheckCO;

/*============================================================================*/

PROCEDURE sp_CheckCGEle(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


   vLst  PKG_UtlGlb.t_query_cur;

   
   vBlok    INTEGER;
   vTipErr  VARCHAR2(50);
   vCodSG   VARCHAR2(20);
   vCodST   VARCHAR2(20);
   vTipEle  VARCHAR2(10);
   vNomEle  VARCHAR2(90);
   vTipo    INTEGER;

   
    function f_DaTipoAMsg(ptipo in number) return NUMBER Is
    begin
        CASE pTipo
        WHEN    1 THEN RETURN pID_DIZIONARIO_ALLARME_02;
        WHEN    2 THEN RETURN pID_DIZIONARIO_ALLARME_03;
        WHEN    3 THEN RETURN pID_DIZIONARIO_ALLARME_05;
        WHEN    4 THEN RETURN pID_DIZIONARIO_ALLARME_04;
        ELSE RETURN -1; END CASE;
    END;
 
BEGIN

    pkg_anagrafiche.ElementsCheckReport(vLst,pStato,pData);

    LOOP
        FETCH vLst INTO   vBlok,vTipErr,vCodSG ,vCodST,vTipEle,vNomEle,vTipo;
        EXIT WHEN vLst%NOTFOUND;
                
       dbms_output.put_line(' sp_AddUnCollector '||f_DaTipoAMsg(vtipo)||' '||vTipEle||' - '||vCodSG);
    sp_AddUnCollector(f_DaTipoAMsg(vtipo), vCodSG, vTipEle);
 
   END LOOP;

END sp_CheckCGEle;

/*============================================================================*/

PROCEDURE sp_CheckProdAUI_NoSTM(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


    vSql VARCHAR2(3000) :=
            'WITH '||
            '    AUI_TRASF AS '||
            '    (SELECT '||
            '            t.COD_ORG_NODO, '||
            '            t.SER_NODO, '||
            '            t.NUM_NODO, '||
            '            t.ID_TRASF, '||
            '            t.TIPO_ELEMENTO '||
            '        FROM '||
            '            TRASFORMATORI_TLC@PKG1_STMAUI.IT t '||
            '        JOIN TRASF_PROD_BT_TLC@PKG1_STMAUI.IT p '||
            '        ON '||
            '            ( '||
            '                t.COD_ORG_NODO '||
            '                ||t.SER_NODO '||
            '                ||t.NUM_NODO '||
            '                ||t.TIPO_ELEMENTO '||
            '                ||t.ID_TRASF= p.COD_ORG_NODO '||
            '                ||p.SER_NODO '||
            '                ||p.NUM_NODO '||
            '                ||p.tipo_ele '||
            '                ||p.ID_TRASF '||
            '            ) '||
            '        WHERE '||
            '            ( '||
            PKG_SPV_RETE_ELe.f_ClientAUISi('t') ||
            '            ) '||
            '            AND '||
            '            ( '||
            PKG_SPV_RETE_ELe.f_ClientAUISi('p') ||
            '            ) '||
            '    ) '||
            'SELECT '||
            '     CASE  T.TIPO_ELEMENTO '||
            'WHEN PKG_SPV_RETE_ELE.f_TipoTrasfAUI THEN t.COD_ORG_NODO||t.SER_NODO||t.NUM_NODO||T.TIPO_ELEMENTO||t.ID_TRASF '||
            'ELSE  C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE END COD_GEST, '||
            '     CASE  T.TIPO_ELEMENTO '||
            'WHEN PKG_SPV_RETE_ELE.f_TipoTrasfAUI THEN PKG_SPV_RETE_ELE.f_TipoTrasfST '||
            'ELSE  PKG_SPV_RETE_ELE.f_TipoProdST END COD_TIPO_ELEMENTO '||
            'FROM '||
            '  (SELECT * FROM CLIENTI_TLC@PKG1_STMAUI.IT WHERE '|| PKG_SPV_RETE_ELe.f_ClientAUISi||' ) C '||
            'LEFT OUTER JOIN AUI_TRASF t '||
            'ON '||
            '    ( '||
            '        t.COD_ORG_NODO '||
            '        ||t.SER_NODO '||
            '        ||t.NUM_NODO '||
            '        ||t.ID_TRASF= C.COD_ORG_NODO '||
            '        ||C.SER_NODO '||
            '        ||C.NUM_NODO '||
            '        ||C.ID_CLIENTE '||
            '    ) '||
            'MINUS '||
            'SELECT '||
            '    COD_GEST, COD_TIPO_ELEMENTO '||
            'FROM '||
            '    ( '||
            '        SELECT '||
            '            COD_GEST, '||
            '            PKG_SPV_RETE_ELE.f_TipoProdST COD_TIPO_ELEMENTO '||
            '        FROM '||
            '            CORELE_CLIENTIMT_## '||
            '        WHERE '||
            '            :myData BETWEEN DATA_INIZIO AND DATA_FINE '||
            '        UNION ALL '||
            '        SELECT '||
            '            COD_GEST, '||
            '            PKG_SPV_RETE_ELE.f_TipoTrasfST COD_TIPO_ELEMENTO '||
            '        FROM '||
            '            CORELE_TRASFORMATORIBT_## '||
            '        WHERE '||
            '            :myData BETWEEN DATA_INIZIO AND DATA_FINE '||
            '            AND INSTR(UPPER(COD_GEST),''FIT'') = 0 '||
            '            AND INSTR(UPPER(NOME),''FIT'')     = 0 '||
            '    ) '||
            'order by COD_TIPO_ELEMENTO, COD_GEST';
      

 
    emp_refcur      SYS_REFCURSOR;
  
    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;    
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;    
  
  
BEGIN

--dbms_output.put_line(f_AppiccicaStato(vSql,pStato));
  OPEN emp_refcur FOR f_AppiccicaStato(vSql,pStato) USING pData,pData;
  LOOP 
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;
 
 dbms_output.put_line(' sp_AddUnCollector 6 '||vTipo||' - '||vGest);
    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_06, vGest,vTipo);

 
  END LOOP;
  CLOSE emp_refcur;

END sp_CheckProdAUI_NoSTM;

/*============================================================================*/

PROCEDURE sp_CheckCG_STM_NoAUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

/*

select base per costruire la sqlinAUI

set pagesize 10000


SELECT 
case WHEN table_name <> table_prev THEN 'SELECT ' else ' ' END,
column_name||(case WHEN table_name = table_succ THEN '||' ELSE ' COD_GEST FROM ' END),
case WHEN table_name <> table_succ THEN table_name||'@PKG1_STMAUI.IT where ###### union all ' else ' ' END
from (
select column_name, table_name, 
LAG(table_name, 1, 0) OVER (ORDER BY table_name,column_id) AS table_prev,
LEAD(table_name, 1, 0) OVER (ORDER BY table_name,column_id) AS table_succ
FROM user_tab_cols 
WHERE table_name like'%TLC'
and not table_name like 'V�_%' ESCAPE '�'
and column_id between 2 and 6
order by table_name, column_id)


*/

    vSql_inSTM VARCHAR(3000) :=
    'INSERT INTO GTTD_VALORI_TEMP '||
    '(TIP, ALF1, ALF2) ' ||
    'SELECT :tip, e_STM.COD_GEST, t.COD_TIPO_ELEMENTO '                                                    ||
                      'FROM (SELECT COD_ENTE,COD_GEST FROM CORELE_AVVOLGIMENTI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '     ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_IMPIANTIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_CLIENTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_CONGIUNTORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_MONTANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_PARALLELI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_RIFASATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_SBARRE_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '            ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_SEZIONATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '       ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASFORMATORIAT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASFORMATORIBT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '   ||
                                         'AND INSTR(UPPER(COD_GEST),''FIT'') = 0 AND INSTR(UPPER(NOME),''FIT'') = 0 '                                         ||
                            'UNION ALL SELECT COD_ENTE,COD_GEST FROM CORELE_TRASLATORI_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT DISTINCT COD_ENTE,COD_GEST FROM CORELE_IMPIANTIMT_SA WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '        ||
                            'UNION ALL SELECT ''SBCS'' COD_ENTE,COD_GEST_SBARRA COD_GEST FROM CORELE_IMPIANTIMT_## WHERE :dt BETWEEN DATA_INIZIO AND DATA_FINE '||
                            ') e_STM '||
			    ' JOIN TIPI_ELEMENTO T ON (t.CODIFICA_ST = e_STM.COD_ENTE AND e_STM.COD_ENTE <> ''CSMT'' ) '; -- escludo c.sec non presenti in elementi mago

    vSql_inAUI VARCHAR(5000) :=
    'INSERT INTO GTTD_VALORI_TEMP '||
    '(TIP, ALF1) ' ||
    'SELECT :tip, COD_GEST FROM ( '||
'SELECT COD_ORG_NODO||' ||
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_CLIENTE COD_GEST FROM APPO_CLIENTI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_TRASF COD_GEST FROM AVVOLGIMENTI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_TRASF COD_GEST FROM CLIENTI_BT_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_CLIENTE COD_GEST FROM CLIENTI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM COLL_ELE_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM COLL_RAMO_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM COLL_TRASF_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_COND COD_GEST FROM CONDENSATORI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM CONN_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||' UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_TRASF COD_GEST FROM COPPIA_AVVO_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_GENERATORE COD_GEST FROM GENERATORI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM MONTANTI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' DATA_AUI|| ' || 
' DATA_TLC COD_GEST FROM NODI_TLC@PKG1_STMAUI.IT where num_nodo<>2 AND '||f_ClientAUISi||'  UNION ALL ' || -- escludo c.sec non presenti in elementi mago
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_MONTANTE COD_GEST FROM PROTEZIONI_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_RTU|| ' || 
' DATA_AUI COD_GEST FROM RTU_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' ID_SIST_SBAR|| ' || 
' ID_SEZI_SBAR COD_GEST FROM SBARRE_SEZ_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELEMENTO|| ' || 
' ID_TRASF COD_GEST FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT  where '||f_ClientAUISi||'  UNION ALL ' || 
'SELECT COD_ORG_NODO|| ' || 
' SER_NODO|| ' || 
' NUM_NODO|| ' || 
' TIPO_ELE|| ' || 
' ID_TRASF COD_GEST FROM TRASF_PROD_BT_TLC@PKG1_STMAUI.IT where '||f_ClientAUISi||' '||
' ) '; 

   
BEGIN

--dbms_output.put_line(f_AppiccicaStato(vSql_inSTM,pStato)|| vSql_inAUI  );

  EXECUTE IMMEDIATE f_AppiccicaStato(vSql_inSTM,pStato)
  USING 'CGdiSTM',
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData,
  pData,pData;
  
  EXECUTE IMMEDIATE vSql_inAUI
  USING 'CGdiAUI';


  FOR riga in (
        SELECT  e_stm.ALF1,  e_stm.ALF2 FROM 
        (SELECT ALF1, ALF2 FROM GTTD_VALORI_TEMP WHERE TIP = 'CGdiSTM') e_stm
        LEFT JOIN
        (SELECT ALF1 FROM GTTD_VALORI_TEMP WHERE TIP = 'CGdiAUI') e_aui
        ON (e_stm.ALF1 = e_aui.ALF1)
        WHERE e_aui.ALF1 IS NULL
        ORDER BY ALF2,ALF1
  ) LOOP
 
 
  dbms_output.put_line(' sp_AddUnCollector 7 '||riga.ALF2||' - '||riga.ALF1);
   sp_AddUnCollector(pID_DIZIONARIO_ALLARME_07, riga.ALF2,riga.ALF1);

 
  END LOOP;


END sp_CheckCG_STM_NoAUI;


/*============================================================================*/

PROCEDURE sp_CheckCliDismessi_AUI(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


vSql VARCHAR2(2000) := 
 'SELECT c_stm.COD_GEST,t.COD_TIPO_ELEMENTO FROM CORELE_CLIENTIMT_## c_stm ' ||
 'JOIN CLIENTI_TLC@PKG1_STMAUI.IT C_aui '||
' ON (C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE = c_stm.COD_GEST ' ||
'  AND :dt BETWEEN c_stm.DATA_INIZIO AND c_stm.DATA_FINE '||
' AND c_aui.stato = '''||f_DismessoAUI||''' ) '||
' JOIN TIPI_ELEMENTO T ON (t.CODIFICA_ST = c_STM.COD_ENTE)';

    emp_refcur      SYS_REFCURSOR;
  
    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;    
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;    
  
  
BEGIN

  OPEN emp_refcur FOR f_AppiccicaStato(vSql,pStato) USING pData;
  LOOP 
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;
 
 dbms_output.put_line(' sp_AddUnCollector 8 AUI '||vTipo||' - '||vGest);
    sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, vGest,vTipo);

 
  END LOOP;
  CLOSE emp_refcur;

END sp_CheckCliDismessi_AUI;
                         
/*============================================================================*/

PROCEDURE sp_CheckCliDismessi_stm(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS


vSql VARCHAR2(2000) := 
' SELECT C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE, ''CMT'' FROM CLIENTI_TLC@PKG1_STMAUI.IT C_aui '||
' LEFT JOIN  (SELECT * FROM CORELE_CLIENTIMT_##) c_stm ' ||
' ON (C_aui.COD_ORG_NODO||C_aui.SER_NODO||C_aui.NUM_NODO||C_aui.TIPO_ELEMENTO||C_aui.ID_CLIENTE = c_stm.COD_GEST ' ||
'  AND :dt BETWEEN c_stm.DATA_INIZIO AND c_stm.DATA_FINE '||
' AND '|| f_ClientAUISi('c_aui') ||' ) '||
' WHERE c_stm.COD_GEST is null ';

    emp_refcur      SYS_REFCURSOR;
  
    vGest ELEMENTI.COD_GEST_ELEMENTO%TYPE;    
    vTipo ELEMENTI.COD_TIPO_ELEMENTO%TYPE;    
  
  
BEGIN

--dbms_output.put_line(f_AppiccicaStato(vSql,pStato));

  OPEN emp_refcur FOR f_AppiccicaStato(vSql,pStato) USING pData;
  LOOP 
    FETCH emp_refcur INTO vGest,vTipo;
    EXIT WHEN emp_refcur%NOTFOUND;
 
   dbms_output.put_line(' sp_AddUnCollector 8 stm '||vTipo||' - '||vGest);
  sp_AddUnCollector(pID_DIZIONARIO_ALLARME_08, vGest,vTipo);


  END LOOP;
  CLOSE emp_refcur;

END sp_CheckCliDismessi_stm;
                         
/*============================================================================*/

/*============================================================================*/

PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE) AS

    vOra TIMESTAMP;
    
BEGIN

    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCO ',40,' '));
     sp_CheckCO (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckCO ',40,' ')||to_char(current_timestamp-vOra));
    
    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCGEle ',40,' '));
     sp_CheckCGEle (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckCGEle ',40,' ')||to_char(current_timestamp-vOra));
    
    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckProdAUI_NoSTM ',40,' '));
     sp_CheckProdAUI_NoSTM (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckProdAUI_NoSTM ',40,' ')||to_char(current_timestamp-vOra));

    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCG_STM_NoAUI ',40,' '));
     sp_CheckCG_STM_NoAUI (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_Chsp_CheckCG_STM_NoAUIeckProdAUI_NoSTM ',40,' ')||to_char(current_timestamp-vOra));

    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCliDismessi_AUI ',40,' '));
     sp_CheckCliDismessi_AUI (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckCliDismessi_AUI ',40,' ')||to_char(current_timestamp-vOra));
    
    vOra := current_timestamp;
    printa(RPAD('in  sp_CheckCliDismessi_stm ',40,' '));
     sp_CheckCliDismessi_stm (pStato, pBlockOnly, pData );
    printa(RPAD('out sp_CheckCliDismessi_stm ',40,' ')||to_char(current_timestamp-vOra));
    
END sp_CheckAll;

/*============================================================================*/

procedure EleSpvCollector
	is
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvCollector',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvCollector - Start';
		printa(vStrLog);

		sp_CheckAll;

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvCollector - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
		EXCEPTION
		WHEN OTHERS THEN
		   printa ('PKG_SPV_RETE_ELE.EleSpvCollector error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));

end EleSpvCollector;

/*============================================================================*/

procedure EleSpvProcessor
	is
		PREFCURS PKG_UtlGlb.t_query_cur;

		p_cod_gest_elemento varchar2(100);
		p_cod_tipo_elemento varchar2(100);

		P_VALORE spv_dettaglio_parametri.VALORE%type;
		pID_DIZIONARIO_ALLARME number;
		pDATA_ALLARME date := sysdate;
		pDESCRIZIONE_TIPO VARCHAR2(100) := null;
		pDESCRIZIONE_ELEMENTO VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO1 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO2 VARCHAR2(100) := null;
		pDESCRIZIONE_ALTRO3 VARCHAR2(100) := null;
		pID_LIVELLO NUMBER;
		pINFO VARCHAR2(100) := null;
		pPARAMETRI_DESCRIZIONE VARCHAR2(100) := null;


		n_allarmi_1 	number;
		n_allarmi_2 	number;
		n_allarmi_3 	number;
		n_allarmi_3_GMT number;
		n_allarmi_3_TRM number;
		n_allarmi_4 	number;
		n_allarmi_5 	number;
		n_allarmi_6_CMT number;
		n_allarmi_6_TRM number;
		n_allarmi_7 	number;
		n_allarmi_8 	number;
		n_allarmi_9 	number;
		n_allarmi_10 	number;
		n_allarmi_11 	number;
		n_allarmi_12 	number;
		n_allarmi_13 	number;
		n_allarmi_14 	number;
		n_allarmi_15 	number;
		n_allarmi_16 	number;
		n_allarmi_17 	number;

		soglia_allarme_1 		number;
		soglia_allarme_2 		number;
		soglia_allarme_3_gmt 	number;
		soglia_allarme_3_trm 	number;
		soglia_allarme_4 		number;
		soglia_allarme_5 		number;
		soglia_allarme_6_cmt 	number;
		soglia_allarme_6_trm	number;
		soglia_allarme_7 		number;
		soglia_allarme_8 		number;
		soglia_allarme_9 		number;
		soglia_allarme_10 		number;
		soglia_allarme_11 		number;
		soglia_allarme_12 		number;
		soglia_allarme_13 		number;
		soglia_allarme_14 		number;
		soglia_allarme_15 		number;
		soglia_allarme_16 		number;
		soglia_allarme_17 		number;

		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvProcessor',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - Start';
		printa(vStrLog);

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_gen');
		soglia_allarme_1 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_nd');
		soglia_allarme_2 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_pi_lt_0');
		soglia_allarme_3_gmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_pi_lt_0');
		soglia_allarme_3_trm := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_pot_gruppi');
		soglia_allarme_4 := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_prod_no_fonte');
		soglia_allarme_6_cmt := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_tr_no_fonte');
		soglia_allarme_6_trm := TO_NUMBER(P_VALORE, '9999');

		P_VALORE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.n_gen_no_fonte');
		soglia_allarme_7 := TO_NUMBER(P_VALORE, '9999');

		--pPARAMETRI_DESCRIZIONE := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'smileGme.n_hour');

		--allarme 3:
		--			GENERATORI MT vs TR MT/bt
		--			GMT / TRM
		--allarme 6:
		--			PRODUTTORI MT vs TR MT/bt
		--			CMT / TRM


		with allarmi as (
		  select d.id_dizionario_allarme
		  , nvl(sum(i.valore), 0) n_allarmi
      , i.informazioni
		  from spv_rel_sistema_dizionario d
		  left join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = g_CollectorName
		  where d.id_sistema = g_IdSistema
		  and trunc(i.data_inserimento) = trunc(sysdate)
		  GROUP BY d.id_dizionario_allarme, i.informazioni
		  order BY d.id_dizionario_allarme, i.informazioni
		)
		select
		 sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_01  , a.n_allarmi, 0)) n_allarmi_1
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_02  , a.n_allarmi, 0)) n_allarmi_2
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_03  , a.n_allarmi, null)) n_allarmi_3
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 and a.informazioni = 'GMT') THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_GMT
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_03 and a.informazioni = g_tipoMago_Trasf) THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_3_TRM
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_04  , a.n_allarmi, 0)) n_allarmi_4
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_05  , a.n_allarmi, 0)) n_allarmi_5
		--,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_06  , a.n_allarmi, null)) n_allarmi_6
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 and a.informazioni = g_tipoMago_Client) THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_CMT
		,sum(
			CASE WHEN (a.id_dizionario_allarme = pID_DIZIONARIO_ALLARME_06 and a.informazioni = g_tipoMago_Trasf) THEN a.n_allarmi
				 ELSE 0
			END
		) n_allarmi_6_TRM
		,sum(DECODE(a.id_dizionario_allarme, pID_DIZIONARIO_ALLARME_07  , a.n_allarmi, 0)) n_allarmi_7
		into n_allarmi_1,n_allarmi_2,n_allarmi_3_GMT,n_allarmi_3_TRM,n_allarmi_4,n_allarmi_5,n_allarmi_6_CMT,n_allarmi_6_TRM,n_allarmi_7
		from allarmi a
		;

		OPEN pRefCurs FOR
			select d.id_dizionario_allarme, i.COD_GEST_ELEMENTO, e.cod_tipo_elemento, i.informazioni
			from spv_rel_sistema_dizionario d
			join spv_collector_info i on d.id_rel_sistema_dizionario = i.id_rel_sistema_dizionario and i.collector_name = g_CollectorName
			left join elementi e on e.COD_GEST_ELEMENTO = i.COD_GEST_ELEMENTO
			where d.id_sistema = g_IdSistema
			and trunc(i.data_inserimento) = trunc(sysdate)
			order BY d.id_dizionario_allarme
			;
		LOOP
			FETCH PREFCURS INTO pID_DIZIONARIO_ALLARME,p_cod_gest_elemento, p_cod_tipo_elemento, pPARAMETRI_DESCRIZIONE;
			EXIT WHEN PREFCURS%NOTFOUND;
		   -- UTL_FILE.PUT_LINE(v_outfile, TO_CHAR (vData,'dd/mm/yyyy hh24.mi.ss')||';'||vCodMis||';'||TO_CHAR(vValore), TRUE);


			--GREY(-1, "GREY"),
			--NESSUNA(1, "GREEN"),
			--MEDIO(2, "YELLOW"),
			--GRAVE(3, "RED");
			pID_LIVELLO := 2;

			-- Gli allarmi 5-12-16-17 hanno una soglia di tipo GIALLO sotto la quale non vengono segnalati
			IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_05  and n_allarmi_5  >= soglia_allarme_5 )
						)THEN
				vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'pPARAMETRI_DESCRIZIONE: ' || pPARAMETRI_DESCRIZIONE ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);

			ELSE
				-- tutti gli altri allarmi vengono gestiti cosi:
				-- da 0 a soglia --> allarme Giallo
				-- da soglia in poi --> allarme Rosso
				IF (   (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_01  and n_allarmi_1  >= soglia_allarme_1 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_02  and n_allarmi_2  >= soglia_allarme_2 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3  >= soglia_allarme_3 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3_GMT  >= soglia_allarme_3_gmt )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_03  and n_allarmi_3_TRM  >= soglia_allarme_3_trm )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_04  and n_allarmi_4  >= soglia_allarme_4 )
					--or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6  >= soglia_allarme_6 )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6_CMT  >= soglia_allarme_6_cmt )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_06  and n_allarmi_6_TRM  >= soglia_allarme_6_trm )
					or (pID_DIZIONARIO_ALLARME=pID_DIZIONARIO_ALLARME_07  and n_allarmi_7  >= soglia_allarme_7 )
					)THEN
					pID_LIVELLO := 3;
				END IF;

				vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - Calling AddSpvAllarmi ['||
				'pID_DIZIONARIO_ALLARME: ' || pID_DIZIONARIO_ALLARME ||' - '||
				'p_cod_gest_elemento: ' || p_cod_gest_elemento ||' - '||
				'p_cod_tipo_elemento: ' || p_cod_tipo_elemento ||' - '||
				'pID_LIVELLO: ' || pID_LIVELLO
				||']';
				printa(vStrLog);

				pkg_supervisione.AddSpvAllarmi(g_IdSistema, pID_DIZIONARIO_ALLARME, pDATA_ALLARME, p_cod_tipo_elemento, p_cod_gest_elemento, pDESCRIZIONE_ALTRO1, pDESCRIZIONE_ALTRO2, pDESCRIZIONE_ALTRO3, pID_LIVELLO, pINFO, pPARAMETRI_DESCRIZIONE);
			END IF;
		END LOOP;
		CLOSE PREFCURS;

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvProcessor - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_RETE_ELE.EleSpvProcessor error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end EleSpvProcessor;

/*============================================================================*/

function f_SetDettaglioLob(p_chiave  IN VARCHAR2,
                         p_query     IN VARCHAR2,
                         p_valforced IN NUMBER DEFAULT NULL) return number IS

l_cursor integer;
l_cursor_status integer;
l_col_count number;
l_desc_tbl sys.dbms_sql.desc_tab2;
l_col_val varchar2(32767);

l_report blob;
l_raw raw(32767);

l_Quanti NUMBER;

/*----------------------------------------------------------------*/

begin

    -- open BLOB to store CSV file
    dbms_lob.createtemporary( l_report, FALSE );
    dbms_lob.open( l_report, dbms_lob.lob_readwrite );

    -- parse query
    l_cursor := dbms_sql.open_cursor;
    dbms_sql.parse(l_cursor, p_query, dbms_sql.native);
    dbms_sql.describe_columns2(l_cursor, l_col_count, l_desc_tbl );

    -- define report columns
    for i in 1 .. l_col_count loop
        dbms_sql.define_column(l_cursor, i, l_col_val, 32767 );
    end loop;

    -- write column headings to CSV file
    for i in 1 .. l_col_count loop
        l_col_val := l_desc_tbl(i).col_name;
        IF upper(l_col_val) = upper('g_tit_CodGes') THEN
            l_col_val := g_tit_CodGes;
        ELSIF  upper(l_col_val) = upper('g_tit_TipCli') THEN
            l_col_val := g_tit_TipCli;
        ELSIF  upper(l_col_val) = upper('g_tit_TipEle') THEN
            l_col_val := g_tit_TipEle;
        END IF;

        if i = l_col_count then
--            l_col_val := '"'||l_col_val||'"'||chr(10);
            l_col_val := l_col_val||chr(10);
        else
--            l_col_val := '"'||l_col_val||'",';
            l_col_val := l_col_val||g_sep_CSV_excel;
        end if;
        l_raw := utl_raw.cast_to_raw( l_col_val );
        dbms_lob.writeappend( l_report, utl_raw.length( l_raw ), l_raw );
    end loop;

    l_cursor_status := sys.dbms_sql.execute(l_cursor);

    l_Quanti := 0;
    -- write result set to CSV file
    loop
    exit when dbms_sql.fetch_rows(l_cursor) <= 0;

        l_Quanti := l_Quanti + 1;

        for i in 1 .. l_col_count loop
            dbms_sql.column_value(l_cursor, i, l_col_val);
            if i = l_col_count then
         --       l_col_val := '"'||l_col_val||'"'||chr(10);
                l_col_val := l_col_val||chr(10);
            else
         --       l_col_val := '"'||l_col_val||'",';
                l_col_val := l_col_val||g_sep_CSV_excel;
            end if;
            l_raw := utl_raw.cast_to_raw( l_col_val );
            dbms_lob.writeappend( l_report, utl_raw.length( l_raw ), l_raw );
        end loop;
    end loop;

    dbms_sql.close_cursor(l_cursor);
    dbms_lob.close( l_report );

    IF p_valforced IS NULL THEN
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   l_Quanti,
                                                   l_report);
    ELSE
        pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                                   p_Chiave,
                                                   p_valforced,
                                                   l_report);
    END IF;

    return l_Quanti;
exception
when no_data_found then
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               0,
                                               null);
    return 0;

end f_SetDettaglioLob;

/*============================================================================*/

function f_SetDettaglioNum(p_chiave IN VARCHAR2,
                       p_query  IN VARCHAR2,
                       p_valforced IN NUMBER DEFAULT NULL) RETURN NUMBER IS

l_Quanti NUMBER;
v_Quanti VARCHAR2(200);

/*----------------------------------------------------------------*/

begin

    IF p_valforced IS NULL THEN
        EXECUTE IMMEDIATE p_query INTO l_Quanti;
    ELSE
        l_quanti := p_valforced;
    END IF;

    v_quanti := replace(to_char(l_quanti),',','.');
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               p_Chiave,
                                               v_Quanti,
                                               NULL);

printa('====================');
printa(p_chiave);
printa(v_Quanti);
--printa('--------------------');
--printa(p_query);
printa('====================');

    return l_quanti;

end f_SetDettaglioNum;

/*============================================================================*/
procedure EleSpvDetail
	is


		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(2000);

        v_sql_dummy VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL';
        v_sql_nihil VARCHAR(2000) := 'SELECT 0 QUANTI FROM DUAL WHERE ROWNUM<1';


        -- RICORDARSI: nelle select per la PI dividere il valore per 1000
        -- che i dati sono in kW ma sul video li vogliono in MW

    -- CONSISTENZA RETE _ AUI

       -- clienti MT
       v_sql_aui_consCliMT_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN '''||g_tipoMago_Trasf||''' ELSE '''||g_tipoMago_Client||''' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
            --    ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''','''||g_ProdPuro_AUI||''','''||g_ProdAlia_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN ) ';
        v_sql_aui_consCliMT_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_aui_consCliMT_from);
        v_sql_aui_consCliMT_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_consCliMT_from);

        -- clienti BT non reperibili
        v_sql_aui_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_aui_cliBT_PI VARCHAR2(2000) := v_sql_dummy;

        -- Trasformatori
        v_sql_aui_consTrMTBT_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) ';
        v_sql_aui_consTrMTBT_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_consTrMTBT_from);
        v_sql_aui_consTrMTBT_PI VARCHAR2(2000)  :=PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_consTrMTBT_from);

        -- Totale
        v_sql_aui_ConsTot_from  VARCHAR2(2000) := ' FROM ( select GEST_PROD COD_GEST_ELEMENTO, '
                ||'       TIPO_FORN COD_TIPO_CLIENTE, '
                ||'   CASE TIPO_ELEMENTO WHEN ''T'' THEN '''||g_tipoMago_Trasf||''' ELSE '''||g_tipoMago_Client||''' END COD_TIPO_ELEMENTO, '
                ||'       sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.TIPO_ELEMENTO,C.POT_GRUPPI,C.TIPO_FORN '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
            --    ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''','''||g_ProdPuro_AUI||''','''||g_ProdAlia_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.TIPO_ELEMENTO,A.POT_GRUPPI,A.TIPO_FORN  '
                ||'  UNION '
                ||'  select gest_prod COD_GEST_ELEMENTO, '
                ||'  '''||g_AutoProd_AUI||''' COD_TIPO_CLIENTE, '''||g_tipoMago_Trasf||''' COD_TIPO_ELEMENTO, '
                ||'      sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) ' ;
        v_sql_aui_ConsTot_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_aui_ConsTot_from);
        v_sql_aui_ConsTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_ConsTot_from);

    -- CONSISTENZA RETE _ MAGO

       -- clienti MT
       v_sql_mago_cliMT_from VARCHAR2(2000) := '   FROM V_ELEMENTI  '||
                ' WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||'''  '||
                ' AND COD_TIPO_CLIENTE IN (''A'',''B'',''C'') '||
                ' ';
        v_sql_mago_cliMT_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT  '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE g_tit_Tipele '
                ||v_sql_mago_cliMT_from);
        v_sql_mago_cliMT_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_cliMT_from);

        -- clienti BT non reperibili
        v_sql_mago_cliBT_N VARCHAR2(2000) :=  v_sql_nihil;
        v_sql_mago_cliBT_PI VARCHAR2(2000) := v_sql_dummy;

        -- Trasformatori
        v_sql_mago_TR_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||'''';
        v_sql_mago_TR_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO g_tit_CodGes'
                ||v_sql_mago_TR_from);
        v_sql_mago_TR_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_TR_from);

        -- Totali
        v_sql_mago_ConsTot_from  VARCHAR2(2000) :=
                ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''') '||
                ' OR (COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'',''C'')) ';
        v_sql_mago_ConsTot_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_ConsTot_from);
        v_sql_mago_ConsTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI  '
                || v_sql_mago_ConsTot_from);


    -- GENERAZIONE DISTRIBUITA _ AUI

       -- prod PURI
        v_sql_aui_prodPuri_from VARCHAR2(2000) :=' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN ('''||g_ProdPuro_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodPuri_N VARCHAR2(2000) :=PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodPuri_from);
        v_sql_aui_prodPuri_PI VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_prodPuri_from);

       -- prod AUTOPROD
       v_sql_aui_prodAuto_from VARCHAR2(2000) := ' FROM ( select gest_prod COD_GEST_ELEMENTO, sum(POT_GRUPPI) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_CLIENTE GEST_PROD, '
                ||'               C.POT_GRUPPI '
                ||'          FROM CLIENTI_TLC@PKG1_STMAUI.IT C
                                  WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'            AND C.TIPO_FORN IN ('''||g_AutoProd_AUI||''') '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_GRUPPI ) ';
        v_sql_aui_prodAuto_N VARCHAR2(2000)  :=PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_aui_prodAuto_from);
        v_sql_aui_prodAuto_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_prodAuto_from);

       -- trasf con Produzione
        v_sql_aui_TRprod_from  VARCHAR2(2000) :=' FROM ( select gest_prod, sum(POT_NOM_1) POTENZA_INSTALLATA '
                ||' FROM (SELECT C.COD_ORG_NODO||C.SER_NODO||C.NUM_NODO||C.TIPO_ELEMENTO||C.ID_TRASF GEST_PROD, '
                ||'               C.POT_NOM_1 '
                ||'          FROM TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'          WHERE C.TRATTAMENTO = 0 AND C.STATO = ''E'' '
                ||'         AND POT_NOM_1>0 '
                ||'       ) A '
                ||' GROUP BY A.GEST_PROD,A.POT_NOM_1 ) '
                ||' WHERE POTENZA_INSTALLATA>0 ';
        v_sql_aui_TRprod_N  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT GEST_PROD g_tit_CodGes'
                ||v_sql_aui_TRprod_from);
        v_sql_aui_TRprod_PI VARCHAR2(2000)  := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_aui_TRprod_from);

       -- totali
        v_sql_aui_GeneTot_from  VARCHAR2(2000) := ' FROM (   SELECT gest_prod COD_GEST_ELEMENTO, '
                ||' SUM(POT_GRUPPI)POTENZA_INSTALLATA, '
                ||''''||g_tipoMago_Client||''' COD_TIPO_ELEMENTO '
                ||'FROM ( SELECT '
                ||'C.COD_ORG_NODO '
                ||'||C.SER_NODO '
                ||'||C.NUM_NODO '
                ||'||C.TIPO_ELEMENTO '
                ||'||C.ID_CLIENTE GEST_PROD, '
                ||'C.POT_GRUPPI '
                ||'FROM '
                ||'CLIENTI_TLC@PKG1_STMAUI.IT C '
                ||'WHERE '
                ||'C.TRATTAMENTO    =0 '
                ||'AND C.STATO      =''E'' '
                ||' AND C.TIPO_FORN IN(''AP'',''PP'') '
                ||') '
                ||'A '
                ||'GROUP BY '
                ||'A.GEST_PROD, '
                ||'A.POT_GRUPPI '
                ||'UNION '
                ||'SELECT '
                ||'gest_prod COD_GEST_ELEMENTO, '
                ||'SUM(POT_NOM_1)POTENZA_INSTALLATA, '
                ||''''||g_tipoMago_Trasf||''' COD_TIPO_ELEMENTO '
                ||'FROM '
                ||'( '
                ||'SELECT '
                ||'C.COD_ORG_NODO '
                ||'||C.SER_NODO '
                ||'||C.NUM_NODO '
                ||'||C.TIPO_ELEMENTO '
                ||'||C.ID_TRASF GEST_PROD, '
                ||'C.POT_NOM_1 '
                ||'FROM '
                ||'TRASFORMATORI_TLC@PKG1_STMAUI.IT C '
                ||'WHERE '
                ||' C.TRATTAMENTO=0 '
                ||'AND C.STATO  =''E'' '
                ||' AND POT_NOM_1>0 '
                ||' ) '
                ||'A '
                ||' GROUP BY '
                ||'A.GEST_PROD, '
                ||'A.POT_NOM_1 '
                ||') ';
        v_sql_aui_GeneTot_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes '
                ||v_sql_aui_GeneTot_from);
        v_sql_aui_GeneTot_PI  VARCHAR2(2000) := (' SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI   '
                ||v_sql_aui_GeneTot_from);


    -- GENERAZIONE DISTRIBUITA _ MAGO

       -- prod PURI
        v_sql_mago_prodPuri_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                ' AND COD_TIPO_CLIENTE = ''A''';
        v_sql_mago_prodPuri_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodPuri_from);
        v_sql_mago_prodPuri_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_prodPuri_from);

       -- prod AUTOPROD
        v_sql_mago_prodAuto_from VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                ' AND COD_TIPO_CLIENTE = ''B''';
        v_sql_mago_prodAuto_N VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT COD_GEST_ELEMENTO  g_tit_CodGes'
                ||v_sql_mago_prodAuto_from);
        v_sql_mago_prodAuto_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_prodAuto_from);

       -- trasf con Produzione
        v_sql_mago_TRprod_from  VARCHAR2(2000) :=  ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''') '||
                '    AND POTENZA_INSTALLATA>0 '||
                ' ORDER BY COD_GEST_ELEMENTO';
        v_sql_mago_TRprod_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_GEST_ELEMENTO g_tit_CodGes '
                ||v_sql_mago_TRprod_from);
        v_sql_mago_TRprod_PI VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT  SUM(POTENZA_INSTALLATA)/1000 g_tit_PI '
                ||v_sql_mago_TRprod_from);

        -- totali
        v_sql_mago_GeneTot_from  VARCHAR2(2000) := ' FROM V_ELEMENTI WHERE (COD_TIPO_ELEMENTO = '''||g_tipoMago_Trasf||''' AND POTENZA_INSTALLATA>0) '||
                ' OR (COD_TIPO_ELEMENTO='''||g_tipoMago_Client||''' '||
                '    AND COD_TIPO_CLIENTE IN (''A'',''B'')) ';
        v_sql_mago_GeneTot_N  VARCHAR2(2000) :=  PKG_UTLGLB.CompattaSelect('SELECT '
                ||' COD_TIPO_ELEMENTO g_tit_TipEle, '
                ||' COD_GEST_ELEMENTO g_tit_CodGes, '
                ||' COD_TIPO_CLIENTE  g_tit_TipCli '
                ||v_sql_mago_GeneTot_from);
        v_sql_mago_GeneTot_PI  VARCHAR2(2000) := PKG_UTLGLB.CompattaSelect('SELECT SUM(POTENZA_INSTALLATA)/1000 g_tit_PI  '
                || v_sql_mago_GeneTot_from);



        v_dummyN            NUMBER;

        v_totN_auiCons      NUMBER := 0;
        v_totN_magoCons     NUMBER := 0;
        v_totPI_auiCons     NUMBER := 0;
        v_totPI_magoCons    NUMBER := 0;
        v_totN_auiGene      NUMBER := 0;
        v_totN_magoGene     NUMBER := 0;
        v_totPI_auiGene     NUMBER := 0;
        v_totPI_magoGene    NUMBER := 0;

        v_dataAUI VARCHAR2(100);
        v_dataMago VARCHAR2(100);

/*----------------------------------------------------------------------------*/
begin

    vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
                               pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvDetail',
                               pDataRif    => sysdate);

    vStrLog := 'PKG_SPV_RETE_ELE.EleSpvDetail - Start';
    printa(vStrLog);


    -- date ultimi aggiornamenti
    select f_Date2DettStr(max(data_import))
    into v_dataAUI
    from STORICO_IMPORT
    where TIPO = 'SN' AND ORIGINE = 'AUI';

   select f_Date2DettStr(max(data_import))
    into v_dataMago
    from STORICO_IMPORT
    where TIPO = 'SN' AND ORIGINE = 'CORELE';


    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.aui.ultimoAggiornamento',
                                               v_dataAUI,
                                               NULL);
    pkg_supervisione.SetSpvDettaglioParametri( g_IdSistema,
                                               'aggiornamento.mago.ultimoAggiornamento',
                                               v_dataMago,
                                               NULL);

    -- consistenza rete AUI
        v_totN_auiCons       := 0;
        v_totPI_auiCons      := 0;


    printa('consistenzaRete.aui.trMTBT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.trMTBT_N',
                                        v_sql_aui_consTrMTBT_N);
    printa('consistenzaRete.aui.trMTBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.trMTBT_PI',
                                        v_sql_aui_consTrMTBT_PI);

    printa('consistenzaRete.aui.clientiMT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiMT_N',
                                        v_sql_aui_consCliMT_N);
    printa('consistenzaRete.aui.clientiMT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiMT_PI',
                                        v_sql_aui_consCliMT_PI);
    -- clienti BT non reperibili
    printa('consistenzaRete.aui.clientiBT_N');
    v_totN_auiCons := v_totN_auiCons + f_SetDettaglioLob('consistenzaRete.aui.clientiBT_N',
                                        v_sql_aui_cliBT_N);
    printa('consistenzaRete.aui.clientiBT_PI');
    v_totPI_auiCons := v_totPI_auiCons + f_SetDettaglioNum('consistenzaRete.aui.clientiBT_PI',
                                        v_sql_aui_cliBT_PI);

    -- totali cons AUI
    printa('consistenzaRete.aui.totale_N');
    v_dummyN := f_SetDettaglioLob('consistenzaRete.aui.totale_N',
                                        v_sql_aui_ConsTot_N,
                                        v_totN_auiCons);

    v_dummyN := f_SetDettaglioNum('consistenzaRete.aui.totale_PI',
                                        v_sql_aui_ConsTot_PI,
                                        v_totPI_auiCons);


    -- consistenza rete Mago
        v_totN_magoCons      := 0;
        v_totPI_magoCons     := 0;

    printa('consistenzaRete.mago.trMTBT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.trMTBT_N',
                                        v_sql_mago_TR_N);
    printa('consistenzaRete.mago.trMTBT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.trMTBT_PI',
                                        v_sql_mago_TR_PI);

    printa('consistenzaRete.mago.clientiMT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiMT_N',
                                        v_sql_mago_cliMT_N);
    printa('consistenzaRete.mago.clientiMT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiMT_PI',
                                        v_sql_mago_cliMT_PI);

    -- clienti BT non reperibili
    printa('consistenzaRete.mago.clientiBT_N');
    v_totN_magoCons := v_totN_magoCons + f_SetDettaglioLob('consistenzaRete.mago.clientiBT_N',
                                        v_sql_mago_cliBT_N);
    printa('consistenzaRete.mago.clientiBT_PI');
    v_totPI_magoCons := v_totPI_magoCons + f_SetDettaglioNum('consistenzaRete.mago.clientiBT_PI',
                                        v_sql_mago_cliBT_PI);

    -- totali cons Mago
    printa('consistenzaRete.mago.totale_N');
    v_dummyN := f_SetDettaglioLob('consistenzaRete.mago.totale_N',
                                        v_sql_mago_ConsTot_N,
                                        v_totN_magoCons);

    v_dummyN := f_SetDettaglioNum('consistenzaRete.mago.totale_PI',
                                        v_sql_mago_ConsTot_PI);



    --  generazione distribuitaAUI
        v_totN_auiGene       := 0;
        v_totPI_auiGene      := 0;

    printa('genDistribuita.aui.trMTBT_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.trMTBT_N',
                                        v_sql_aui_TRprod_N);
    printa(v_totN_auiGene);

    printa('genDistribuita.aui.trMTBT_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.trMTBT_PI',
                                        v_sql_aui_TRprod_PI);
--    printa(v_totPI_auiGene);

    printa('genDistribuita.aui.prodMTPuri_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTPuri_N',
                                        v_sql_aui_prodPuri_N);
--    printa(v_totN_auiGene);

    printa('genDistribuita.aui.prodMTPuri_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTPuri_PI',
                                        v_sql_aui_prodPuri_PI);
--    printa(v_totPI_auiGene);

    printa('genDistribuita.aui.prodMTAutoproduttori_N');
    v_totN_auiGene := v_totN_auiGene + f_SetDettaglioLob('genDistribuita.aui.prodMTAutoproduttori_N',
                                        v_sql_aui_prodAuto_N);
--    printa(v_totN_auiGene);

    printa('genDistribuita.aui.prodMTAutoproduttori_PI');
    v_totPI_auiGene := v_totPI_auiGene + f_SetDettaglioNum('genDistribuita.aui.prodMTAutoproduttori_PI',
                                        v_sql_aui_prodAuto_PI);
--    printa(v_totPI_auiGene);

    -- totali gene dist AUI
--    printa('genDistribuita.aui.totale_N');
--    printa('-----------------------------');
--    printa(v_sql_aui_GeneTot_N);
 --   printa('-----------------------------');
--    printa(v_sql_aui_GeneTot_PI);
--    printa('-----------------------------');
    v_dummyN := f_SetDettaglioLob('genDistribuita.aui.totale_N',
                                        v_sql_aui_GeneTot_N,
                                        v_totN_auiGene);

    v_dummyN := f_SetDettaglioNum('genDistribuita.aui.totale_PI',
                                        v_sql_aui_GeneTot_PI,
                                        v_totPI_auiGene);
--    printa(v_totPI_auiGene);



    -- generazione distribuita Mago
        v_totN_magoGene      := 0;
        v_totPI_magoGene     := 0;

    printa('genDistribuita.mago.trMTBT_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.trMTBT_N',
                                        v_sql_mago_TRprod_N);
    printa('genDistribuita.mago.trMTBT_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.trMTBT_PI',
                                        v_sql_mago_TRprod_PI);

    printa('genDistribuita.mago.prodMTPuri_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTPuri_N',
                                        v_sql_mago_prodPuri_N);
    printa('genDistribuita.mago.prodMTPuri_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTPuri_PI',
                                        v_sql_mago_prodPuri_PI);

    printa('genDistribuita.mago.prodMTAutoproduttori_N');
    v_totN_magoGene := v_totN_magoGene + f_SetDettaglioLob('genDistribuita.mago.prodMTAutoproduttori_N',
                                        v_sql_mago_prodAuto_N);
    printa('genDistribuita.mago.prodMTAutoproduttori_PI');
    v_totPI_magoGene := v_totPI_magoGene + f_SetDettaglioNum('genDistribuita.mago.prodMTAutoproduttori_PI',
                                        v_sql_mago_prodAuto_PI);



    -- totali gene dist Mago
    printa('genDistribuita.mago.totale_N');
    v_dummyN := f_SetDettaglioLob('genDistribuita.mago.totale_N',
                                        v_sql_mago_GeneTot_N);

    v_dummyN := f_SetDettaglioNum('genDistribuita.mago.totale_PI',
                                        v_sql_mago_GeneTot_PI,
                                        v_totPI_magoGene);





    vStrLog := 'PKG_SPV_RETE_ELE.EleSpvDetail - End';
    PKG_Logs.TraceLog(vStrLog,PKG_UtlGlb.gcTrace_VRB);
    printa(vStrLog);

    PKG_Logs.StdLogPrint (vLog);

EXCEPTION
WHEN OTHERS THEN
    printa('PKG_SPV_RETE_ELE.EleSpvDetail error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end EleSpvDetail;
/*============================================================================*/

procedure EleSpvMain(	 pDoDeleteOldSpvInfo in number default 0
							,pDoDeleteSpvAlarm in number default 1
							,pDoDeleteTodaySpvInfo in number default 1
							,pDoCollector in number default 1
							,pDoProcessor in number default 1
							,pDoDetail in number default 1
							)
	is
    vIsSupervisionEnabled varchar2(100);
		vLog  PKG_Logs.t_StandardLog;
		vStrLog VARCHAR2(1000);
	begin
		vLog := PKG_Logs.StdLogInit(pClasseFunz => PKG_Mago.gcJobClassMIS,
								   pFunzione   => 'PKG_SPV_RETE_ELE.EleSpvMain',
								   pDataRif    => sysdate);

		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Start';
		printa(vStrLog);

    vIsSupervisionEnabled := pkg_supervisione.F_getSpvSoglie(g_IdSistema, 'aui.supervision_enabled');
    	vStrLog :=  'PKG_SPV_RETE_ELE --> ' ||
			'pDoDeleteOldSpvInfo: ' || pDoDeleteOldSpvInfo ||' - '||
			'pDoDeleteSpvAlarm: ' || pDoDeleteSpvAlarm ||' - '||
			'pDoDeleteTodaySpvInfo: ' || pDoDeleteTodaySpvInfo ||' - '||
			'pDoCollector: ' || pDoCollector ||' - '||
			'pDoProcessor: ' || pDoProcessor ||' - '||
      'vIsSupervisionEnabled: ' || vIsSupervisionEnabled
			;
     printa(vStrLog);


vIsSupervisionEnabled:='true';
   if vIsSupervisionEnabled = 'true' or vIsSupervisionEnabled = 'TRUE' then
        -- cancello le info generate da run precedenti di smileGmeSpvMain. non vengono elminitate le info generate dal BE c++
      if pDoDeleteTodaySpvInfo = 1 then
          vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - delete Today spv_collector_info ';
           printa(vStrLog);
        delete from spv_collector_info
        where collector_name = g_CollectorName
        and trunc(data_inserimento) = trunc(sysdate)
        ;
      end if;

      -- cancello TUTTE le info generate dal collector ANAGRAFICA_AUI prima di gc-48h
      if pDoDeleteOldSpvInfo = 1 then
          vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - delete Old spv_collector_info ';
          printa(vStrLog);
        delete from spv_collector_info
        where collector_name = g_CollectorName
        and data_inserimento < trunc(sysdate)
        and id_rel_sistema_dizionario in (
          select id_rel_sistema_dizionario
          from spv_rel_sistema_dizionario
          where id_sistema = g_IdSistema
        );
      end if;

      -- eseguo il collector e creo le spv_collector_info
      if pDoCollector = 1 then
        vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvCollector ';
        printa(vStrLog);
        EleSpvCollector();
      end if;

      -- cancello TUTTI gli allarmi generati dal collector ANAGRAFICA_AUI
      if pDoDeleteSpvAlarm = 1 then
        vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain -  delete from spv_allarmi ';
         printa(vStrLog);
        delete from spv_allarmi where id_rel_sistema_dizionario in (select id_rel_sistema_dizionario from spv_rel_sistema_dizionario where id_sistema = g_IdSistema);
      end if;

      -- eseguo il processor e creo le spv_allarmi
      if pDoProcessor = 1 then
        vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvProcessor ';
        printa(vStrLog);

        EleSpvProcessor();
      end if;

      -- valorizzo il pannello di dettaglio
      if pDoDetail = 1 then
        vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Calling EleSpvDetail ';
        printa(vStrLog);

        EleSpvDetail();
      end if;
    else
      vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - Supervision AUI is NOT enabled';
      printa(vStrLog);
    end if;



		vStrLog := 'PKG_SPV_RETE_ELE.EleSpvMain - End';
		printa(vStrLog);

		PKG_Logs.StdLogPrint (vLog);
	EXCEPTION
	WHEN OTHERS THEN
		printa('PKG_SPV_RETE_ELE.EleSpvMain error ' || SQLCODE || '- ' || SUBSTR(SQLERRM, 1 , 64));
end EleSpvMain;

/*============================================================================*/

END PKG_SPV_RETE_ELE;
/

show errors;