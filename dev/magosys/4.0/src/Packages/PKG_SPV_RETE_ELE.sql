PROMPT PACKAGE        "PKG_SPV_RETE_ELE" AS

create or replace PACKAGE        PKG_SPV_RETE_ELE AS

/*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=
                                             versione 2.2.1x
=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*=*/

/* ***********************************************************************************************************
   NAME:       PKG_SPV_RETE_ELE
   PURPOSE:    Servizi la raccolta delle info per gli allarmi relativi alla Supervisione Anagrafica AUI

   REVISIONS:
   Ver         Date        Author          Description
   ---------  ----------  ---------------  --------------------------------------------------------------
   2.2.1x      17/10/2017  Frosini S.       Created this package                -- Spec. Version 2.2.1x
    NOTES:

*********************************************************************************************************** */

/* ***********************************************************************************************************
 Tipi, Costanti e Variabili Globali Pubbliche
*********************************************************************************************************** */

/* ***********************************************************************************************************
 Funzioni e Procedure Pubbliche
*********************************************************************************************************** */

    function f_StatoNormale RETURN VARCHAR2;
    function f_StatoAttuale RETURN VARCHAR2;

    function f_StatoNormaleSuffisso RETURN VARCHAR2;
    function f_StatoAttualeSuffisso RETURN VARCHAR2;

    function f_TipoTrasfAUI  RETURN VARCHAR2;
    function f_TipoProdAUI  RETURN VARCHAR2;
    function f_TipoTrasfST  RETURN VARCHAR2;
    function f_TipoProdST  RETURN VARCHAR2;


    function f_Date2DettStr(pData in date) return varchar2;
    
    
    PROCEDURE sp_CheckCliDismessi_stm(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);
    
    PROCEDURE sp_CheckCliDismessi_AUI(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);
    
    PROCEDURE sp_CheckCG_STM_NoAUI(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);
    
    PROCEDURE sp_CheckProdAUI_NoSTM(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);
    
    PROCEDURE sp_CheckCGEle(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);
                             
    PROCEDURE sp_CheckCO(pStato       IN INTEGER DEFAULT 1,
                         pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                         pData           IN DATE DEFAULT SYSDATE);
                         
    PROCEDURE sp_CheckAll(pStato       IN INTEGER DEFAULT 1,
                             pBlockOnly      IN BOOLEAN DEFAULT FALSE,
                             pData           IN DATE DEFAULT SYSDATE);



    procedure EleSpvCollector;
    procedure EleSpvProcessor;
    procedure EleSpvDetail;
    procedure EleSpvMain(	 pDoDeleteOldSpvInfo in number default 0
                            ,pDoDeleteSpvAlarm in number default 1
                            ,pDoDeleteTodaySpvInfo in number default 1
                            ,pDoCollector in number default 1
                            ,pDoProcessor in number default 1
                            ,pDoDetail in number default 1
                            );


END PKG_SPV_RETE_ELE;
/