PROMPT Insert parametri MAGO/SPV ELE in SAR_ADMIN.PARAMETRI_GENERALI

MERGE INTO PARAMETRI_GENERALI t 
     USING (
            SELECT 'MAGO' COD_APPLICAZIONE , 'IS_MUNICIPALIZZATE' NOME_PARAMETRO, '0' VALORE_PARAMETRO   FROM dual
            ) s        
        ON (t.COD_APPLICAZIONE = s.COD_APPLICAZIONE and t.NOME_PARAMETRO = s.NOME_PARAMETRO)
      WHEN MATCHED THEN
         UPDATE SET 
              t.VALORE_PARAMETRO = s.VALORE_PARAMETRO
      WHEN NOT MATCHED THEN 
        INSERT   
          (COD_APPLICAZIONE,NOME_PARAMETRO,VALORE_PARAMETRO) 
        VALUES (s.COD_APPLICAZIONE, s.NOME_PARAMETRO , s.VALORE_PARAMETRO);

COMMIT;
/