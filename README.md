## Comandi per repository con submodules

Clonare:

    git clone --recursive <PROJECT_URL>
    
Update submodule:

    git submodule update --remote
    
Inizializazione repository

Creare la cartella /dev/\<schema\> al cui interno verranno comittate le revisioni dello schema

----------------------------------------------------

## Ant Usage

	Available commnads:    
		* ant create-migration params
		* ant create-rev params
		* ant create-sar params
		* ant db-inventory

-------------------------------------------------

	Usage:    ant create-migration params
	Description: create migration for a schema or multiple schemas using the migration-file
	params:
		* -Dgui=true|false (start the buil with GUI support)
		* -Dmigration="**schema**|**version**|**list-of-revisions**"
		* -Dmigration-file=migration.list (use the specified file for composing the build)
		* -Dclean=true|false (decide to clean the dist folder | default false)
		* -Dnotes="Note da inserire nella migrazione"
	Example:
		- ant create-migration -Dgui=true
		- ant create-migration -Dgui=true -Dclean=true
		- ant create-migration -Dmigration="retim|2.0|rev-001,rev-002,rev-003" 
		- ant create-migration -Dmigration-file=example/migration.list
		- ant create-migration -Dmigration-file=example/migration.list -Dnotes="Note..." -Dclean=true

-------------------------------------------------

	Usage:    create-rev params
    Description: create a new revision for a schema using a common template
    params:
		* -Dschema=**schema** (the directory where to create the new rev)
		* -Drev=X.Y.Z (the rev to create, n.b. can include letters like 2.1.15a)
    Example:
		- ant create-rev -Dschema=crel -Drev=2.6.4a

-------------------------------------------------

	Usage:    create-sar params
    Description: create a new sar using a template
    params:
		* -Dmigration="**schema**|**version**|**list-of-revisions**"
		* -Dmigration-file=migration.list (use the specified file for composing the build)
		* -Dnotes="Note da inserire nella migrazione" (optional)
		* -Dname=SAR_XXX_YYY (Nome da dare al SAR)
    Example:
		- ant create-sar -Dmigration="retim|2.0|rev-001,rev-002,rev-003" -Dname=SAR_CP_1023
		- ant create-sar -Dmigration-file=example/migration.list
		- ant create-sar -Dmigration-file=example/migration.list -Dnotes="Note..." 

-------------------------------------------------

	Usage:    ant db-inventory
	Description: create a file inventory.list containing all the schemas and relative versions and revisions.
	Example:
		- ant ant db-inventory

Per Un maggiore dettaglio fare riferimento alla Wiki del progetto Ant:

[Wiki Ant](https://gitlab.digitalgrid.it/stweb/enel/dr-database/ant/wikis/home)
	